<?php

return [
    "wsdl" => env("ID3GLOBAL_WSDL", "https://pilot.id3global.com/ID3gWS/ID3global.svc?wsdl"),
    "auth" => [
        "username" => env("ID3GLOBAL_USERNAME", ""),
        "password" => env("ID3GLOBAL_PASSWORD", "")
    ]
];
