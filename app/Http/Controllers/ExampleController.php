<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ID3Global\Services\Check;
use ID3Global\Models\CheckCredentials;

class ExampleController extends Controller
{
    /**
     * The ID3Global Check service is available from the service container
     * all ready and configured with WsSecurity. (see app/Providers/ID3GlobalProvider)
     * So all we have to do is grab it with dependency injection
     */
    public function checkCredentials(Check $checkService)
    {
        // Let's just check our own credentials.
        $credentials = new CheckCredentials(config("id3global.auth.username"), config("id3global.auth.password"));

        if ($checkService->CheckCredentials($credentials) !== false) {
            return response()->json($checkService->getResult());
        } else {
            return response()->json($checkService->getLastError());
        }
    }
}
