<?php

namespace App\Providers;

use ID3Global\Services\Add;
use Illuminate\Support\ServiceProvider;
use SoapClient;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;
use WsdlToPhp\WsSecurity\WsSecurity;
use HaydenPierce\ClassFinder\ClassFinder;

class ID3GlobalServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $options = [
            \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => config("id3global.wsdl"),
            \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \ID3Global\ClassMap::get(),
        ];

        $this->app->bind("wsSoapClient", function () {
            $soapClient = new SoapClient(config("id3global.wsdl"));
            $soapClient->__setSoapHeaders(WsSecurity::createWsSecuritySoapHeader(
                config("id3global.auth.username"),
                config("id3global.auth.password")
            ));
            return $soapClient;
        });
        // Loop through all the Service definitions and bind them to the service containers
        // so we can dependency inject them preconfigured with WsSecurity applied in the soap header
        $services = ClassFinder::getClassesInNamespace("ID3Global\Services");
        foreach ($services as $service) {
            $this->app->bind($service, function () use ($service, $options) {
                $client = new $service($options);
                $client->setSoapClient(app("wsSoapClient"));
                return $client;
            });
        }
    }
}
