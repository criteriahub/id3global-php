<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = [
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://pilot.id3global.com/ID3gWS/ID3global.svc?wsdl',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * ];
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = [
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://pilot.id3global.com/ID3gWS/ID3global.svc?wsdl',
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \ID3Global\ClassMap::get(),
];
/**
 * Samples for Check ServiceType
 */
$check = new \ID3Global\Services\Check($options);
/**
 * Sample call for CheckCredentials operation/method
 */
if ($check->CheckCredentials(new \ID3Global\Models\CheckCredentials()) !== false) {
    print_r($check->getResult());
} else {
    print_r($check->getLastError());
}
/**
 * Sample call for CheckNonce operation/method
 */
if ($check->CheckNonce(new \ID3Global\Models\CheckNonce()) !== false) {
    print_r($check->getResult());
} else {
    print_r($check->getLastError());
}
/**
 * Sample call for CheckCredentialsWithPIN operation/method
 */
if ($check->CheckCredentialsWithPIN(new \ID3Global\Models\CheckCredentialsWithPIN()) !== false) {
    print_r($check->getResult());
} else {
    print_r($check->getLastError());
}
/**
 * Sample call for CheckNewAccountAgainstPasswordPolicies operation/method
 */
if ($check->CheckNewAccountAgainstPasswordPolicies(new \ID3Global\Models\CheckNewAccountAgainstPasswordPolicies()) !== false) {
    print_r($check->getResult());
} else {
    print_r($check->getLastError());
}
/**
 * Samples for Get ServiceType
 */
$get = new \ID3Global\Services\Get($options);
/**
 * Sample call for GetPINSequence operation/method
 */
if ($get->GetPINSequence(new \ID3Global\Models\GetPINSequence()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetPasswordPolicyForUser operation/method
 */
if ($get->GetPasswordPolicyForUser(new \ID3Global\Models\GetPasswordPolicyForUser()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDocImageMinSizeKBytes operation/method
 */
if ($get->GetDocImageMinSizeKBytes(new \ID3Global\Models\GetDocImageMinSizeKBytes()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDocImageMaxSizeKBytes operation/method
 */
if ($get->GetDocImageMaxSizeKBytes(new \ID3Global\Models\GetDocImageMaxSizeKBytes()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDocImageMinWidthResolution operation/method
 */
if ($get->GetDocImageMinWidthResolution(new \ID3Global\Models\GetDocImageMinWidthResolution()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDocImageMinHeightResolution operation/method
 */
if ($get->GetDocImageMinHeightResolution(new \ID3Global\Models\GetDocImageMinHeightResolution()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDocImageSupportedFileTypes operation/method
 */
if ($get->GetDocImageSupportedFileTypes(new \ID3Global\Models\GetDocImageSupportedFileTypes()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetPassportCountries operation/method
 */
if ($get->GetPassportCountries(new \ID3Global\Models\GetPassportCountries()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetIdentityCardIssuingCountries operation/method
 */
if ($get->GetIdentityCardIssuingCountries(new \ID3Global\Models\GetIdentityCardIssuingCountries()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetIdentityCardNationalityCountries operation/method
 */
if ($get->GetIdentityCardNationalityCountries(new \ID3Global\Models\GetIdentityCardNationalityCountries()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetLocationCountries operation/method
 */
if ($get->GetLocationCountries(new \ID3Global\Models\GetLocationCountries()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetLocationStates operation/method
 */
if ($get->GetLocationStates(new \ID3Global\Models\GetLocationStates()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetItalyProvinceOfBirth operation/method
 */
if ($get->GetItalyProvinceOfBirth(new \ID3Global\Models\GetItalyProvinceOfBirth()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetItemCheckResultCodes operation/method
 */
if ($get->GetItemCheckResultCodes(new \ID3Global\Models\GetItemCheckResultCodes()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSanctionsIssuers operation/method
 */
if ($get->GetSanctionsIssuers(new \ID3Global\Models\GetSanctionsIssuers()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSearchPurposes operation/method
 */
if ($get->GetSearchPurposes(new \ID3Global\Models\GetSearchPurposes()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetFCRASanctionsIssuers operation/method
 */
if ($get->GetFCRASanctionsIssuers(new \ID3Global\Models\GetFCRASanctionsIssuers()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCardName operation/method
 */
if ($get->GetCardName(new \ID3Global\Models\GetCardName()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDocumentTypes operation/method
 */
if ($get->GetDocumentTypes(new \ID3Global\Models\GetDocumentTypes()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetExtractVersions operation/method
 */
if ($get->GetExtractVersions(new \ID3Global\Models\GetExtractVersions()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetExtractFormats operation/method
 */
if ($get->GetExtractFormats(new \ID3Global\Models\GetExtractFormats()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSouthAfricaCreditSuppliers operation/method
 */
if ($get->GetSouthAfricaCreditSuppliers(new \ID3Global\Models\GetSouthAfricaCreditSuppliers()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDVLADrivingLicenceEndorsements operation/method
 */
if ($get->GetDVLADrivingLicenceEndorsements(new \ID3Global\Models\GetDVLADrivingLicenceEndorsements()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetPEPTiers operation/method
 */
if ($get->GetPEPTiers(new \ID3Global\Models\GetPEPTiers()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCIFASProductCode operation/method
 */
if ($get->GetCIFASProductCode(new \ID3Global\Models\GetCIFASProductCode()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCreditHeaderAccountTypes operation/method
 */
if ($get->GetCreditHeaderAccountTypes(new \ID3Global\Models\GetCreditHeaderAccountTypes()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetImageResults operation/method
 */
if ($get->GetImageResults(new \ID3Global\Models\GetImageResults()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetImages operation/method
 */
if ($get->GetImages(new \ID3Global\Models\GetImages()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetImageDetails operation/method
 */
if ($get->GetImageDetails(new \ID3Global\Models\GetImageDetails()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDocumentExtract operation/method
 */
if ($get->GetDocumentExtract(new \ID3Global\Models\GetDocumentExtract()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetProfileVersions operation/method
 */
if ($get->GetProfileVersions(new \ID3Global\Models\GetProfileVersions()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetProfileVersionFlags operation/method
 */
if ($get->GetProfileVersionFlags(new \ID3Global\Models\GetProfileVersionFlags()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetProfileVersionItemIDs operation/method
 */
if ($get->GetProfileVersionItemIDs(new \ID3Global\Models\GetProfileVersionItemIDs()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCardTypes operation/method
 */
if ($get->GetCardTypes(new \ID3Global\Models\GetCardTypes()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCardTypesMP operation/method
 */
if ($get->GetCardTypesMP(new \ID3Global\Models\GetCardTypesMP()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetProfiles operation/method
 */
if ($get->GetProfiles(new \ID3Global\Models\GetProfiles()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetProfileChain operation/method
 */
if ($get->GetProfileChain(new \ID3Global\Models\GetProfileChain()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetProfileDetails operation/method
 */
if ($get->GetProfileDetails(new \ID3Global\Models\GetProfileDetails()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetProfileDetailsWithLatestRevision operation/method
 */
if ($get->GetProfileDetailsWithLatestRevision(new \ID3Global\Models\GetProfileDetailsWithLatestRevision()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetLicence operation/method
 */
if ($get->GetLicence(new \ID3Global\Models\GetLicence()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetItemCheckProperties operation/method
 */
if ($get->GetItemCheckProperties(new \ID3Global\Models\GetItemCheckProperties()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetItemChecks operation/method
 */
if ($get->GetItemChecks(new \ID3Global\Models\GetItemChecks()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetInputFieldByItemCheckId operation/method
 */
if ($get->GetInputFieldByItemCheckId(new \ID3Global\Models\GetInputFieldByItemCheckId()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetRetiringProfiles operation/method
 */
if ($get->GetRetiringProfiles(new \ID3Global\Models\GetRetiringProfiles()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetAuthenticationDetails operation/method
 */
if ($get->GetAuthenticationDetails(new \ID3Global\Models\GetAuthenticationDetails()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetLatestAuthenticationDetails operation/method
 */
if ($get->GetLatestAuthenticationDetails(new \ID3Global\Models\GetLatestAuthenticationDetails()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetAuthentications operation/method
 */
if ($get->GetAuthentications(new \ID3Global\Models\GetAuthentications()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSanctionsEnforcementsData operation/method
 */
if ($get->GetSanctionsEnforcementsData(new \ID3Global\Models\GetSanctionsEnforcementsData()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetPEPIntelligenceData operation/method
 */
if ($get->GetPEPIntelligenceData(new \ID3Global\Models\GetPEPIntelligenceData()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDataExtract operation/method
 */
if ($get->GetDataExtract(new \ID3Global\Models\GetDataExtract()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDataExtracts operation/method
 */
if ($get->GetDataExtracts(new \ID3Global\Models\GetDataExtracts()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDataExtractHistory operation/method
 */
if ($get->GetDataExtractHistory(new \ID3Global\Models\GetDataExtractHistory()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetReports operation/method
 */
if ($get->GetReports(new \ID3Global\Models\GetReports()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetReportParameters operation/method
 */
if ($get->GetReportParameters(new \ID3Global\Models\GetReportParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetReport operation/method
 */
if ($get->GetReport(new \ID3Global\Models\GetReport()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetReportImage operation/method
 */
if ($get->GetReportImage(new \ID3Global\Models\GetReportImage()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDetailedInvestigation operation/method
 */
if ($get->GetDetailedInvestigation(new \ID3Global\Models\GetDetailedInvestigation()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetFCRAMatchDetails operation/method
 */
if ($get->GetFCRAMatchDetails(new \ID3Global\Models\GetFCRAMatchDetails()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSupplierExtendedStatusInformation operation/method
 */
if ($get->GetSupplierExtendedStatusInformation(new \ID3Global\Models\GetSupplierExtendedStatusInformation()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetAccountExtracts operation/method
 */
if ($get->GetAccountExtracts(new \ID3Global\Models\GetAccountExtracts()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetUserAccountExtractHistory operation/method
 */
if ($get->GetUserAccountExtractHistory(new \ID3Global\Models\GetUserAccountExtractHistory()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetDetailedInvestigationPDF operation/method
 */
if ($get->GetDetailedInvestigationPDF(new \ID3Global\Models\GetDetailedInvestigationPDF()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetOrganisations operation/method
 */
if ($get->GetOrganisations(new \ID3Global\Models\GetOrganisations()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetOrganisationDetails operation/method
 */
if ($get->GetOrganisationDetails(new \ID3Global\Models\GetOrganisationDetails()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetHome operation/method
 */
if ($get->GetHome(new \ID3Global\Models\GetHome()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetAccounts operation/method
 */
if ($get->GetAccounts(new \ID3Global\Models\GetAccounts()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetAccountDetails operation/method
 */
if ($get->GetAccountDetails(new \ID3Global\Models\GetAccountDetails()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetOrgAdminAccountID operation/method
 */
if ($get->GetOrgAdminAccountID(new \ID3Global\Models\GetOrgAdminAccountID()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetAccountRoles operation/method
 */
if ($get->GetAccountRoles(new \ID3Global\Models\GetAccountRoles()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetAccountPermission operation/method
 */
if ($get->GetAccountPermission(new \ID3Global\Models\GetAccountPermission()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetRoles operation/method
 */
if ($get->GetRoles(new \ID3Global\Models\GetRoles()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetRoleMembers operation/method
 */
if ($get->GetRoleMembers(new \ID3Global\Models\GetRoleMembers()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetRolePermissions operation/method
 */
if ($get->GetRolePermissions(new \ID3Global\Models\GetRolePermissions()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetPermissions operation/method
 */
if ($get->GetPermissions(new \ID3Global\Models\GetPermissions()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSuppliers operation/method
 */
if ($get->GetSuppliers(new \ID3Global\Models\GetSuppliers()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSupplierFlags operation/method
 */
if ($get->GetSupplierFlags(new \ID3Global\Models\GetSupplierFlags()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSupplierAccounts operation/method
 */
if ($get->GetSupplierAccounts(new \ID3Global\Models\GetSupplierAccounts()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSupplierAccount operation/method
 */
if ($get->GetSupplierAccount(new \ID3Global\Models\GetSupplierAccount()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSupplierFieldsBySupplierId operation/method
 */
if ($get->GetSupplierFieldsBySupplierId(new \ID3Global\Models\GetSupplierFieldsBySupplierId()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetSupplierConfig operation/method
 */
if ($get->GetSupplierConfig(new \ID3Global\Models\GetSupplierConfig()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetGlobalCaseProfileVersions operation/method
 */
if ($get->GetGlobalCaseProfileVersions(new \ID3Global\Models\GetGlobalCaseProfileVersions()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCaseNotifications operation/method
 */
if ($get->GetCaseNotifications(new \ID3Global\Models\GetCaseNotifications()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCaseNotification operation/method
 */
if ($get->GetCaseNotification(new \ID3Global\Models\GetCaseNotification()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCustomerSettings operation/method
 */
if ($get->GetCustomerSettings(new \ID3Global\Models\GetCustomerSettings()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCases operation/method
 */
if ($get->GetCases(new \ID3Global\Models\GetCases()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCaseDetails operation/method
 */
if ($get->GetCaseDetails(new \ID3Global\Models\GetCaseDetails()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCaseInputData operation/method
 */
if ($get->GetCaseInputData(new \ID3Global\Models\GetCaseInputData()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCaseAuditRecord operation/method
 */
if ($get->GetCaseAuditRecord(new \ID3Global\Models\GetCaseAuditRecord()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for Change ServiceType
 */
$change = new \ID3Global\Services\Change($options);
/**
 * Sample call for ChangePassword operation/method
 */
if ($change->ChangePassword(new \ID3Global\Models\ChangePassword()) !== false) {
    print_r($change->getResult());
} else {
    print_r($change->getLastError());
}
/**
 * Sample call for ChangePIN operation/method
 */
if ($change->ChangePIN(new \ID3Global\Models\ChangePIN()) !== false) {
    print_r($change->getResult());
} else {
    print_r($change->getLastError());
}
/**
 * Sample call for ChangePINWithReminder operation/method
 */
if ($change->ChangePINWithReminder(new \ID3Global\Models\ChangePINWithReminder()) !== false) {
    print_r($change->getResult());
} else {
    print_r($change->getLastError());
}
/**
 * Samples for Forgot ServiceType
 */
$forgot = new \ID3Global\Services\Forgot($options);
/**
 * Sample call for ForgotAccount operation/method
 */
if ($forgot->ForgotAccount(new \ID3Global\Models\ForgotAccount()) !== false) {
    print_r($forgot->getResult());
} else {
    print_r($forgot->getLastError());
}
/**
 * Sample call for ForgotAccountForProduct operation/method
 */
if ($forgot->ForgotAccountForProduct(new \ID3Global\Models\ForgotAccountForProduct()) !== false) {
    print_r($forgot->getResult());
} else {
    print_r($forgot->getLastError());
}
/**
 * Sample call for ForgotPassword operation/method
 */
if ($forgot->ForgotPassword(new \ID3Global\Models\ForgotPassword()) !== false) {
    print_r($forgot->getResult());
} else {
    print_r($forgot->getLastError());
}
/**
 * Samples for Request ServiceType
 */
$request = new \ID3Global\Services\Request($options);
/**
 * Sample call for RequestMemorableWordReminder operation/method
 */
if ($request->RequestMemorableWordReminder(new \ID3Global\Models\RequestMemorableWordReminder()) !== false) {
    print_r($request->getResult());
} else {
    print_r($request->getLastError());
}
/**
 * Samples for Supplier ServiceType
 */
$supplier = new \ID3Global\Services\Supplier($options);
/**
 * Sample call for SupplierUpdate operation/method
 */
if ($supplier->SupplierUpdate(new \ID3Global\Models\SupplierUpdate()) !== false) {
    print_r($supplier->getResult());
} else {
    print_r($supplier->getLastError());
}
/**
 * Samples for Upload ServiceType
 */
$upload = new \ID3Global\Services\Upload($options);
/**
 * Sample call for Upload operation/method
 */
if ($upload->Upload(new \ID3Global\Models\Upload()) !== false) {
    print_r($upload->getResult());
} else {
    print_r($upload->getLastError());
}
/**
 * Sample call for UploadAndProcess operation/method
 */
if ($upload->UploadAndProcess(new \ID3Global\Models\UploadAndProcess()) !== false) {
    print_r($upload->getResult());
} else {
    print_r($upload->getLastError());
}
/**
 * Sample call for UploadAndProcessByProfileProperties operation/method
 */
if ($upload->UploadAndProcessByProfileProperties(new \ID3Global\Models\UploadAndProcessByProfileProperties()) !== false) {
    print_r($upload->getResult());
} else {
    print_r($upload->getLastError());
}
/**
 * Samples for Process ServiceType
 */
$process = new \ID3Global\Services\Process($options);
/**
 * Sample call for ProcessStoredImage operation/method
 */
if ($process->ProcessStoredImage(new \ID3Global\Models\ProcessStoredImage()) !== false) {
    print_r($process->getResult());
} else {
    print_r($process->getLastError());
}
/**
 * Samples for Download ServiceType
 */
$download = new \ID3Global\Services\Download($options);
/**
 * Sample call for Download operation/method
 */
if ($download->Download(new \ID3Global\Models\Download()) !== false) {
    print_r($download->getResult());
} else {
    print_r($download->getLastError());
}
/**
 * Sample call for DownloadStoredImage operation/method
 */
if ($download->DownloadStoredImage(new \ID3Global\Models\DownloadStoredImage()) !== false) {
    print_r($download->getResult());
} else {
    print_r($download->getLastError());
}
/**
 * Sample call for DownloadDataExtract operation/method
 */
if ($download->DownloadDataExtract(new \ID3Global\Models\DownloadDataExtract()) !== false) {
    print_r($download->getResult());
} else {
    print_r($download->getLastError());
}
/**
 * Sample call for DownloadExtract operation/method
 */
if ($download->DownloadExtract(new \ID3Global\Models\DownloadExtract()) !== false) {
    print_r($download->getResult());
} else {
    print_r($download->getLastError());
}
/**
 * Samples for Store ServiceType
 */
$store = new \ID3Global\Services\Store($options);
/**
 * Sample call for StoreDocumentImage operation/method
 */
if ($store->StoreDocumentImage(new \ID3Global\Models\StoreDocumentImage()) !== false) {
    print_r($store->getResult());
} else {
    print_r($store->getLastError());
}
/**
 * Samples for Supported ServiceType
 */
$supported = new \ID3Global\Services\Supported($options);
/**
 * Sample call for SupportedFields operation/method
 */
if ($supported->SupportedFields(new \ID3Global\Models\SupportedFields()) !== false) {
    print_r($supported->getResult());
} else {
    print_r($supported->getLastError());
}
/**
 * Samples for Create ServiceType
 */
$create = new \ID3Global\Services\Create($options);
/**
 * Sample call for CreateProfile operation/method
 */
if ($create->CreateProfile(new \ID3Global\Models\CreateProfile()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for CreateProfileWithScoringMethod operation/method
 */
if ($create->CreateProfileWithScoringMethod(new \ID3Global\Models\CreateProfileWithScoringMethod()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for CreateDataExtract operation/method
 */
if ($create->CreateDataExtract(new \ID3Global\Models\CreateDataExtract()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for CreateUserAccountExtract operation/method
 */
if ($create->CreateUserAccountExtract(new \ID3Global\Models\CreateUserAccountExtract()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for CreateOrganisation operation/method
 */
if ($create->CreateOrganisation(new \ID3Global\Models\CreateOrganisation()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for CreateAccount operation/method
 */
if ($create->CreateAccount(new \ID3Global\Models\CreateAccount()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for CreateRole operation/method
 */
if ($create->CreateRole(new \ID3Global\Models\CreateRole()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for CreateCase operation/method
 */
if ($create->CreateCase(new \ID3Global\Models\CreateCase()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for CreateFullCase operation/method
 */
if ($create->CreateFullCase(new \ID3Global\Models\CreateFullCase()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Samples for Copy ServiceType
 */
$copy = new \ID3Global\Services\Copy($options);
/**
 * Sample call for CopyProfile operation/method
 */
if ($copy->CopyProfile(new \ID3Global\Models\CopyProfile()) !== false) {
    print_r($copy->getResult());
} else {
    print_r($copy->getLastError());
}
/**
 * Samples for Update ServiceType
 */
$update = new \ID3Global\Services\Update($options);
/**
 * Sample call for UpdateProfile operation/method
 */
if ($update->UpdateProfile(new \ID3Global\Models\UpdateProfile()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateProfileState operation/method
 */
if ($update->UpdateProfileState(new \ID3Global\Models\UpdateProfileState()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateProfileDetails operation/method
 */
if ($update->UpdateProfileDetails(new \ID3Global\Models\UpdateProfileDetails()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateLicence operation/method
 */
if ($update->UpdateLicence(new \ID3Global\Models\UpdateLicence()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateLicenceItemToPending operation/method
 */
if ($update->UpdateLicenceItemToPending(new \ID3Global\Models\UpdateLicenceItemToPending()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateAuthenticationHelpdeskAccess operation/method
 */
if ($update->UpdateAuthenticationHelpdeskAccess(new \ID3Global\Models\UpdateAuthenticationHelpdeskAccess()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateAuthenticationDataDeletion operation/method
 */
if ($update->UpdateAuthenticationDataDeletion(new \ID3Global\Models\UpdateAuthenticationDataDeletion()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateDIVDataDeletion operation/method
 */
if ($update->UpdateDIVDataDeletion(new \ID3Global\Models\UpdateDIVDataDeletion()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateOrganisationDetails operation/method
 */
if ($update->UpdateOrganisationDetails(new \ID3Global\Models\UpdateOrganisationDetails()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateAccountDetails operation/method
 */
if ($update->UpdateAccountDetails(new \ID3Global\Models\UpdateAccountDetails()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateRole operation/method
 */
if ($update->UpdateRole(new \ID3Global\Models\UpdateRole()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateRoleMembers operation/method
 */
if ($update->UpdateRoleMembers(new \ID3Global\Models\UpdateRoleMembers()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdatePermissions operation/method
 */
if ($update->UpdatePermissions(new \ID3Global\Models\UpdatePermissions()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateSupplier operation/method
 */
if ($update->UpdateSupplier(new \ID3Global\Models\UpdateSupplier()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateSupplierAccount operation/method
 */
if ($update->UpdateSupplierAccount(new \ID3Global\Models\UpdateSupplierAccount()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateCaseNotification operation/method
 */
if ($update->UpdateCaseNotification(new \ID3Global\Models\UpdateCaseNotification()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateCustomerSettings operation/method
 */
if ($update->UpdateCustomerSettings(new \ID3Global\Models\UpdateCustomerSettings()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateState operation/method
 */
if ($update->UpdateState(new \ID3Global\Models\UpdateState()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateCase operation/method
 */
if ($update->UpdateCase(new \ID3Global\Models\UpdateCase()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateCaseEmail operation/method
 */
if ($update->UpdateCaseEmail(new \ID3Global\Models\UpdateCaseEmail()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for UpdateDispatchStatus operation/method
 */
if ($update->UpdateDispatchStatus(new \ID3Global\Models\UpdateDispatchStatus()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Samples for Authenticate ServiceType
 */
$authenticate = new \ID3Global\Services\Authenticate($options);
/**
 * Sample call for AuthenticateSP operation/method
 */
if ($authenticate->AuthenticateSP(new \ID3Global\Models\AuthenticateSP()) !== false) {
    print_r($authenticate->getResult());
} else {
    print_r($authenticate->getLastError());
}
/**
 * Sample call for AuthenticateMP operation/method
 */
if ($authenticate->AuthenticateMP(new \ID3Global\Models\AuthenticateMP()) !== false) {
    print_r($authenticate->getResult());
} else {
    print_r($authenticate->getLastError());
}
/**
 * Samples for Incremental ServiceType
 */
$incremental = new \ID3Global\Services\Incremental($options);
/**
 * Sample call for IncrementalVerification operation/method
 */
if ($incremental->IncrementalVerification(new \ID3Global\Models\IncrementalVerification()) !== false) {
    print_r($incremental->getResult());
} else {
    print_r($incremental->getLastError());
}
/**
 * Samples for Force ServiceType
 */
$force = new \ID3Global\Services\Force($options);
/**
 * Sample call for ForceOverride operation/method
 */
if ($force->ForceOverride(new \ID3Global\Models\ForceOverride()) !== false) {
    print_r($force->getResult());
} else {
    print_r($force->getLastError());
}
/**
 * Samples for Address ServiceType
 */
$address = new \ID3Global\Services\Address($options);
/**
 * Sample call for AddressLookup operation/method
 */
if ($address->AddressLookup(new \ID3Global\Models\AddressLookup()) !== false) {
    print_r($address->getResult());
} else {
    print_r($address->getLastError());
}
/**
 * Samples for Add ServiceType
 */
$add = new \ID3Global\Services\Add($options);
/**
 * Sample call for AddRemoveOGMRecord operation/method
 */
if ($add->AddRemoveOGMRecord(new \ID3Global\Models\AddRemoveOGMRecord()) !== false) {
    print_r($add->getResult());
} else {
    print_r($add->getLastError());
}
/**
 * Samples for Cancel ServiceType
 */
$cancel = new \ID3Global\Services\Cancel($options);
/**
 * Sample call for CancelDataExtract operation/method
 */
if ($cancel->CancelDataExtract(new \ID3Global\Models\CancelDataExtract()) !== false) {
    print_r($cancel->getResult());
} else {
    print_r($cancel->getLastError());
}
/**
 * Sample call for CancelExtract operation/method
 */
if ($cancel->CancelExtract(new \ID3Global\Models\CancelExtract()) !== false) {
    print_r($cancel->getResult());
} else {
    print_r($cancel->getLastError());
}
/**
 * Samples for Reset ServiceType
 */
$reset = new \ID3Global\Services\Reset($options);
/**
 * Sample call for ResetPassword operation/method
 */
if ($reset->ResetPassword(new \ID3Global\Models\ResetPassword()) !== false) {
    print_r($reset->getResult());
} else {
    print_r($reset->getLastError());
}
/**
 * Samples for Send ServiceType
 */
$send = new \ID3Global\Services\Send($options);
/**
 * Sample call for SendWelcomeEmail operation/method
 */
if ($send->SendWelcomeEmail(new \ID3Global\Models\SendWelcomeEmail()) !== false) {
    print_r($send->getResult());
} else {
    print_r($send->getLastError());
}
/**
 * Samples for Test ServiceType
 */
$test = new \ID3Global\Services\Test($options);
/**
 * Sample call for TestSupplierAccountCredentials operation/method
 */
if ($test->TestSupplierAccountCredentials(new \ID3Global\Models\TestSupplierAccountCredentials()) !== false) {
    print_r($test->getResult());
} else {
    print_r($test->getLastError());
}
/**
 * Samples for Raise ServiceType
 */
$raise = new \ID3Global\Services\Raise($options);
/**
 * Sample call for RaiseChargingPoint operation/method
 */
if ($raise->RaiseChargingPoint(new \ID3Global\Models\RaiseChargingPoint()) !== false) {
    print_r($raise->getResult());
} else {
    print_r($raise->getLastError());
}
/**
 * Samples for Save ServiceType
 */
$save = new \ID3Global\Services\Save($options);
/**
 * Sample call for SaveCaseDocuments operation/method
 */
if ($save->SaveCaseDocuments(new \ID3Global\Models\SaveCaseDocuments()) !== false) {
    print_r($save->getResult());
} else {
    print_r($save->getLastError());
}
/**
 * Sample call for SaveDocumentResult operation/method
 */
if ($save->SaveDocumentResult(new \ID3Global\Models\SaveDocumentResult()) !== false) {
    print_r($save->getResult());
} else {
    print_r($save->getLastError());
}
/**
 * Sample call for SaveExternalDataResult operation/method
 */
if ($save->SaveExternalDataResult(new \ID3Global\Models\SaveExternalDataResult()) !== false) {
    print_r($save->getResult());
} else {
    print_r($save->getLastError());
}
/**
 * Sample call for SaveResult operation/method
 */
if ($save->SaveResult(new \ID3Global\Models\SaveResult()) !== false) {
    print_r($save->getResult());
} else {
    print_r($save->getLastError());
}
/**
 * Sample call for SaveManualDispatchResult operation/method
 */
if ($save->SaveManualDispatchResult(new \ID3Global\Models\SaveManualDispatchResult()) !== false) {
    print_r($save->getResult());
} else {
    print_r($save->getLastError());
}
/**
 * Samples for Dispatch ServiceType
 */
$dispatch = new \ID3Global\Services\Dispatch($options);
/**
 * Sample call for DispatchCase operation/method
 */
if ($dispatch->DispatchCase(new \ID3Global\Models\DispatchCase()) !== false) {
    print_r($dispatch->getResult());
} else {
    print_r($dispatch->getLastError());
}
/**
 * Samples for Retrieve ServiceType
 */
$retrieve = new \ID3Global\Services\Retrieve($options);
/**
 * Sample call for RetrieveDispatch operation/method
 */
if ($retrieve->RetrieveDispatch(new \ID3Global\Models\RetrieveDispatch()) !== false) {
    print_r($retrieve->getResult());
} else {
    print_r($retrieve->getLastError());
}
