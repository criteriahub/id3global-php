<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalItemCheckDecisionBand Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q253:ArrayOfGlobalItemCheckDecisionBand
 * @subpackage Arrays
 */
class ArrayOfGlobalItemCheckDecisionBand extends AbstractStructArrayBase
{
    /**
     * The GlobalItemCheckDecisionBand
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalItemCheckDecisionBand[]
     */
    protected ?array $GlobalItemCheckDecisionBand = null;
    /**
     * Constructor method for ArrayOfGlobalItemCheckDecisionBand
     * @uses ArrayOfGlobalItemCheckDecisionBand::setGlobalItemCheckDecisionBand()
     * @param \ID3Global\Models\GlobalItemCheckDecisionBand[] $globalItemCheckDecisionBand
     */
    public function __construct(?array $globalItemCheckDecisionBand = null)
    {
        $this
            ->setGlobalItemCheckDecisionBand($globalItemCheckDecisionBand);
    }
    /**
     * Get GlobalItemCheckDecisionBand value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalItemCheckDecisionBand[]
     */
    public function getGlobalItemCheckDecisionBand(): ?array
    {
        return isset($this->GlobalItemCheckDecisionBand) ? $this->GlobalItemCheckDecisionBand : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalItemCheckDecisionBand method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalItemCheckDecisionBand method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalItemCheckDecisionBandForArrayConstraintsFromSetGlobalItemCheckDecisionBand(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalItemCheckDecisionBandGlobalItemCheckDecisionBandItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalItemCheckDecisionBandGlobalItemCheckDecisionBandItem instanceof \ID3Global\Models\GlobalItemCheckDecisionBand) {
                $invalidValues[] = is_object($arrayOfGlobalItemCheckDecisionBandGlobalItemCheckDecisionBandItem) ? get_class($arrayOfGlobalItemCheckDecisionBandGlobalItemCheckDecisionBandItem) : sprintf('%s(%s)', gettype($arrayOfGlobalItemCheckDecisionBandGlobalItemCheckDecisionBandItem), var_export($arrayOfGlobalItemCheckDecisionBandGlobalItemCheckDecisionBandItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalItemCheckDecisionBand property can only contain items of type \ID3Global\Models\GlobalItemCheckDecisionBand, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalItemCheckDecisionBand value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckDecisionBand[] $globalItemCheckDecisionBand
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBand
     */
    public function setGlobalItemCheckDecisionBand(?array $globalItemCheckDecisionBand = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalItemCheckDecisionBandArrayErrorMessage = self::validateGlobalItemCheckDecisionBandForArrayConstraintsFromSetGlobalItemCheckDecisionBand($globalItemCheckDecisionBand))) {
            throw new InvalidArgumentException($globalItemCheckDecisionBandArrayErrorMessage, __LINE__);
        }
        if (is_null($globalItemCheckDecisionBand) || (is_array($globalItemCheckDecisionBand) && empty($globalItemCheckDecisionBand))) {
            unset($this->GlobalItemCheckDecisionBand);
        } else {
            $this->GlobalItemCheckDecisionBand = $globalItemCheckDecisionBand;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalItemCheckDecisionBand|null
     */
    public function current(): ?\ID3Global\Models\GlobalItemCheckDecisionBand
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalItemCheckDecisionBand|null
     */
    public function item($index): ?\ID3Global\Models\GlobalItemCheckDecisionBand
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalItemCheckDecisionBand|null
     */
    public function first(): ?\ID3Global\Models\GlobalItemCheckDecisionBand
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalItemCheckDecisionBand|null
     */
    public function last(): ?\ID3Global\Models\GlobalItemCheckDecisionBand
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalItemCheckDecisionBand|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalItemCheckDecisionBand
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckDecisionBand $item
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBand
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalItemCheckDecisionBand) {
            throw new InvalidArgumentException(sprintf('The GlobalItemCheckDecisionBand property can only contain items of type \ID3Global\Models\GlobalItemCheckDecisionBand, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalItemCheckDecisionBand
     */
    public function getAttributeName(): string
    {
        return 'GlobalItemCheckDecisionBand';
    }
}
