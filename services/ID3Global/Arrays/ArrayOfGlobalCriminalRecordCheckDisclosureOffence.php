<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCriminalRecordCheckDisclosureOffence Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q793:ArrayOfGlobalCriminalRecordCheckDisclosureOffence
 * @subpackage Arrays
 */
class ArrayOfGlobalCriminalRecordCheckDisclosureOffence extends AbstractStructArrayBase
{
    /**
     * The GlobalCriminalRecordCheckDisclosureOffence
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence[]
     */
    protected ?array $GlobalCriminalRecordCheckDisclosureOffence = null;
    /**
     * Constructor method for ArrayOfGlobalCriminalRecordCheckDisclosureOffence
     * @uses ArrayOfGlobalCriminalRecordCheckDisclosureOffence::setGlobalCriminalRecordCheckDisclosureOffence()
     * @param \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence[] $globalCriminalRecordCheckDisclosureOffence
     */
    public function __construct(?array $globalCriminalRecordCheckDisclosureOffence = null)
    {
        $this
            ->setGlobalCriminalRecordCheckDisclosureOffence($globalCriminalRecordCheckDisclosureOffence);
    }
    /**
     * Get GlobalCriminalRecordCheckDisclosureOffence value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence[]
     */
    public function getGlobalCriminalRecordCheckDisclosureOffence(): ?array
    {
        return isset($this->GlobalCriminalRecordCheckDisclosureOffence) ? $this->GlobalCriminalRecordCheckDisclosureOffence : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCriminalRecordCheckDisclosureOffence method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCriminalRecordCheckDisclosureOffence method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCriminalRecordCheckDisclosureOffenceForArrayConstraintsFromSetGlobalCriminalRecordCheckDisclosureOffence(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCriminalRecordCheckDisclosureOffenceGlobalCriminalRecordCheckDisclosureOffenceItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCriminalRecordCheckDisclosureOffenceGlobalCriminalRecordCheckDisclosureOffenceItem instanceof \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence) {
                $invalidValues[] = is_object($arrayOfGlobalCriminalRecordCheckDisclosureOffenceGlobalCriminalRecordCheckDisclosureOffenceItem) ? get_class($arrayOfGlobalCriminalRecordCheckDisclosureOffenceGlobalCriminalRecordCheckDisclosureOffenceItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCriminalRecordCheckDisclosureOffenceGlobalCriminalRecordCheckDisclosureOffenceItem), var_export($arrayOfGlobalCriminalRecordCheckDisclosureOffenceGlobalCriminalRecordCheckDisclosureOffenceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCriminalRecordCheckDisclosureOffence property can only contain items of type \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCriminalRecordCheckDisclosureOffence value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence[] $globalCriminalRecordCheckDisclosureOffence
     * @return \ID3Global\Arrays\ArrayOfGlobalCriminalRecordCheckDisclosureOffence
     */
    public function setGlobalCriminalRecordCheckDisclosureOffence(?array $globalCriminalRecordCheckDisclosureOffence = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCriminalRecordCheckDisclosureOffenceArrayErrorMessage = self::validateGlobalCriminalRecordCheckDisclosureOffenceForArrayConstraintsFromSetGlobalCriminalRecordCheckDisclosureOffence($globalCriminalRecordCheckDisclosureOffence))) {
            throw new InvalidArgumentException($globalCriminalRecordCheckDisclosureOffenceArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCriminalRecordCheckDisclosureOffence) || (is_array($globalCriminalRecordCheckDisclosureOffence) && empty($globalCriminalRecordCheckDisclosureOffence))) {
            unset($this->GlobalCriminalRecordCheckDisclosureOffence);
        } else {
            $this->GlobalCriminalRecordCheckDisclosureOffence = $globalCriminalRecordCheckDisclosureOffence;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence|null
     */
    public function current(): ?\ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence|null
     */
    public function first(): ?\ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence|null
     */
    public function last(): ?\ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCriminalRecordCheckDisclosureOffence
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence) {
            throw new InvalidArgumentException(sprintf('The GlobalCriminalRecordCheckDisclosureOffence property can only contain items of type \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCriminalRecordCheckDisclosureOffence
     */
    public function getAttributeName(): string
    {
        return 'GlobalCriminalRecordCheckDisclosureOffence';
    }
}
