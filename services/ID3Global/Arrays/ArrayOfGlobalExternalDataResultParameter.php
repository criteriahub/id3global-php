<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalExternalDataResultParameter Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q880:ArrayOfGlobalExternalDataResultParameter
 * @subpackage Arrays
 */
class ArrayOfGlobalExternalDataResultParameter extends AbstractStructArrayBase
{
    /**
     * The GlobalExternalDataResultParameter
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalExternalDataResultParameter[]
     */
    protected ?array $GlobalExternalDataResultParameter = null;
    /**
     * Constructor method for ArrayOfGlobalExternalDataResultParameter
     * @uses ArrayOfGlobalExternalDataResultParameter::setGlobalExternalDataResultParameter()
     * @param \ID3Global\Models\GlobalExternalDataResultParameter[] $globalExternalDataResultParameter
     */
    public function __construct(?array $globalExternalDataResultParameter = null)
    {
        $this
            ->setGlobalExternalDataResultParameter($globalExternalDataResultParameter);
    }
    /**
     * Get GlobalExternalDataResultParameter value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalExternalDataResultParameter[]
     */
    public function getGlobalExternalDataResultParameter(): ?array
    {
        return isset($this->GlobalExternalDataResultParameter) ? $this->GlobalExternalDataResultParameter : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalExternalDataResultParameter method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalExternalDataResultParameter method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalExternalDataResultParameterForArrayConstraintsFromSetGlobalExternalDataResultParameter(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalExternalDataResultParameterGlobalExternalDataResultParameterItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalExternalDataResultParameterGlobalExternalDataResultParameterItem instanceof \ID3Global\Models\GlobalExternalDataResultParameter) {
                $invalidValues[] = is_object($arrayOfGlobalExternalDataResultParameterGlobalExternalDataResultParameterItem) ? get_class($arrayOfGlobalExternalDataResultParameterGlobalExternalDataResultParameterItem) : sprintf('%s(%s)', gettype($arrayOfGlobalExternalDataResultParameterGlobalExternalDataResultParameterItem), var_export($arrayOfGlobalExternalDataResultParameterGlobalExternalDataResultParameterItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalExternalDataResultParameter property can only contain items of type \ID3Global\Models\GlobalExternalDataResultParameter, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalExternalDataResultParameter value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalExternalDataResultParameter[] $globalExternalDataResultParameter
     * @return \ID3Global\Arrays\ArrayOfGlobalExternalDataResultParameter
     */
    public function setGlobalExternalDataResultParameter(?array $globalExternalDataResultParameter = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalExternalDataResultParameterArrayErrorMessage = self::validateGlobalExternalDataResultParameterForArrayConstraintsFromSetGlobalExternalDataResultParameter($globalExternalDataResultParameter))) {
            throw new InvalidArgumentException($globalExternalDataResultParameterArrayErrorMessage, __LINE__);
        }
        if (is_null($globalExternalDataResultParameter) || (is_array($globalExternalDataResultParameter) && empty($globalExternalDataResultParameter))) {
            unset($this->GlobalExternalDataResultParameter);
        } else {
            $this->GlobalExternalDataResultParameter = $globalExternalDataResultParameter;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalExternalDataResultParameter|null
     */
    public function current(): ?\ID3Global\Models\GlobalExternalDataResultParameter
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalExternalDataResultParameter|null
     */
    public function item($index): ?\ID3Global\Models\GlobalExternalDataResultParameter
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalExternalDataResultParameter|null
     */
    public function first(): ?\ID3Global\Models\GlobalExternalDataResultParameter
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalExternalDataResultParameter|null
     */
    public function last(): ?\ID3Global\Models\GlobalExternalDataResultParameter
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalExternalDataResultParameter|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalExternalDataResultParameter
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalExternalDataResultParameter $item
     * @return \ID3Global\Arrays\ArrayOfGlobalExternalDataResultParameter
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalExternalDataResultParameter) {
            throw new InvalidArgumentException(sprintf('The GlobalExternalDataResultParameter property can only contain items of type \ID3Global\Models\GlobalExternalDataResultParameter, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalExternalDataResultParameter
     */
    public function getAttributeName(): string
    {
        return 'GlobalExternalDataResultParameter';
    }
}
