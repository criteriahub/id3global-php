<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseNotificationJob Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q740:ArrayOfGlobalCaseNotificationJob
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseNotificationJob extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseNotificationJob
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseNotificationJob[]
     */
    protected ?array $GlobalCaseNotificationJob = null;
    /**
     * Constructor method for ArrayOfGlobalCaseNotificationJob
     * @uses ArrayOfGlobalCaseNotificationJob::setGlobalCaseNotificationJob()
     * @param \ID3Global\Models\GlobalCaseNotificationJob[] $globalCaseNotificationJob
     */
    public function __construct(?array $globalCaseNotificationJob = null)
    {
        $this
            ->setGlobalCaseNotificationJob($globalCaseNotificationJob);
    }
    /**
     * Get GlobalCaseNotificationJob value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseNotificationJob[]
     */
    public function getGlobalCaseNotificationJob(): ?array
    {
        return isset($this->GlobalCaseNotificationJob) ? $this->GlobalCaseNotificationJob : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseNotificationJob method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseNotificationJob method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseNotificationJobForArrayConstraintsFromSetGlobalCaseNotificationJob(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseNotificationJobGlobalCaseNotificationJobItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseNotificationJobGlobalCaseNotificationJobItem instanceof \ID3Global\Models\GlobalCaseNotificationJob) {
                $invalidValues[] = is_object($arrayOfGlobalCaseNotificationJobGlobalCaseNotificationJobItem) ? get_class($arrayOfGlobalCaseNotificationJobGlobalCaseNotificationJobItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseNotificationJobGlobalCaseNotificationJobItem), var_export($arrayOfGlobalCaseNotificationJobGlobalCaseNotificationJobItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseNotificationJob property can only contain items of type \ID3Global\Models\GlobalCaseNotificationJob, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseNotificationJob value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseNotificationJob[] $globalCaseNotificationJob
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseNotificationJob
     */
    public function setGlobalCaseNotificationJob(?array $globalCaseNotificationJob = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseNotificationJobArrayErrorMessage = self::validateGlobalCaseNotificationJobForArrayConstraintsFromSetGlobalCaseNotificationJob($globalCaseNotificationJob))) {
            throw new InvalidArgumentException($globalCaseNotificationJobArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseNotificationJob) || (is_array($globalCaseNotificationJob) && empty($globalCaseNotificationJob))) {
            unset($this->GlobalCaseNotificationJob);
        } else {
            $this->GlobalCaseNotificationJob = $globalCaseNotificationJob;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseNotificationJob|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseNotificationJob
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseNotificationJob|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseNotificationJob
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseNotificationJob|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseNotificationJob
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseNotificationJob|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseNotificationJob
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseNotificationJob|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseNotificationJob
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseNotificationJob $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseNotificationJob
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseNotificationJob) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseNotificationJob property can only contain items of type \ID3Global\Models\GlobalCaseNotificationJob, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseNotificationJob
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseNotificationJob';
    }
}
