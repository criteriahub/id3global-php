<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCIFASMatch Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q65:ArrayOfGlobalCIFASMatch
 * @subpackage Arrays
 */
class ArrayOfGlobalCIFASMatch extends AbstractStructArrayBase
{
    /**
     * The GlobalCIFASMatch
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCIFASMatch[]
     */
    protected ?array $GlobalCIFASMatch = null;
    /**
     * Constructor method for ArrayOfGlobalCIFASMatch
     * @uses ArrayOfGlobalCIFASMatch::setGlobalCIFASMatch()
     * @param \ID3Global\Models\GlobalCIFASMatch[] $globalCIFASMatch
     */
    public function __construct(?array $globalCIFASMatch = null)
    {
        $this
            ->setGlobalCIFASMatch($globalCIFASMatch);
    }
    /**
     * Get GlobalCIFASMatch value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCIFASMatch[]
     */
    public function getGlobalCIFASMatch(): ?array
    {
        return isset($this->GlobalCIFASMatch) ? $this->GlobalCIFASMatch : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCIFASMatch method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCIFASMatch method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCIFASMatchForArrayConstraintsFromSetGlobalCIFASMatch(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCIFASMatchGlobalCIFASMatchItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCIFASMatchGlobalCIFASMatchItem instanceof \ID3Global\Models\GlobalCIFASMatch) {
                $invalidValues[] = is_object($arrayOfGlobalCIFASMatchGlobalCIFASMatchItem) ? get_class($arrayOfGlobalCIFASMatchGlobalCIFASMatchItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCIFASMatchGlobalCIFASMatchItem), var_export($arrayOfGlobalCIFASMatchGlobalCIFASMatchItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCIFASMatch property can only contain items of type \ID3Global\Models\GlobalCIFASMatch, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCIFASMatch value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCIFASMatch[] $globalCIFASMatch
     * @return \ID3Global\Arrays\ArrayOfGlobalCIFASMatch
     */
    public function setGlobalCIFASMatch(?array $globalCIFASMatch = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCIFASMatchArrayErrorMessage = self::validateGlobalCIFASMatchForArrayConstraintsFromSetGlobalCIFASMatch($globalCIFASMatch))) {
            throw new InvalidArgumentException($globalCIFASMatchArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCIFASMatch) || (is_array($globalCIFASMatch) && empty($globalCIFASMatch))) {
            unset($this->GlobalCIFASMatch);
        } else {
            $this->GlobalCIFASMatch = $globalCIFASMatch;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCIFASMatch|null
     */
    public function current(): ?\ID3Global\Models\GlobalCIFASMatch
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCIFASMatch|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCIFASMatch
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCIFASMatch|null
     */
    public function first(): ?\ID3Global\Models\GlobalCIFASMatch
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCIFASMatch|null
     */
    public function last(): ?\ID3Global\Models\GlobalCIFASMatch
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCIFASMatch|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCIFASMatch
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCIFASMatch $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCIFASMatch
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCIFASMatch) {
            throw new InvalidArgumentException(sprintf('The GlobalCIFASMatch property can only contain items of type \ID3Global\Models\GlobalCIFASMatch, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCIFASMatch
     */
    public function getAttributeName(): string
    {
        return 'GlobalCIFASMatch';
    }
}
