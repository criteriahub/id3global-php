<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalFCRAMatchDetails Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q531:ArrayOfGlobalFCRAMatchDetails
 * @subpackage Arrays
 */
class ArrayOfGlobalFCRAMatchDetails extends AbstractStructArrayBase
{
    /**
     * The GlobalFCRAMatchDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalFCRAMatchDetails[]
     */
    protected ?array $GlobalFCRAMatchDetails = null;
    /**
     * Constructor method for ArrayOfGlobalFCRAMatchDetails
     * @uses ArrayOfGlobalFCRAMatchDetails::setGlobalFCRAMatchDetails()
     * @param \ID3Global\Models\GlobalFCRAMatchDetails[] $globalFCRAMatchDetails
     */
    public function __construct(?array $globalFCRAMatchDetails = null)
    {
        $this
            ->setGlobalFCRAMatchDetails($globalFCRAMatchDetails);
    }
    /**
     * Get GlobalFCRAMatchDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalFCRAMatchDetails[]
     */
    public function getGlobalFCRAMatchDetails(): ?array
    {
        return isset($this->GlobalFCRAMatchDetails) ? $this->GlobalFCRAMatchDetails : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalFCRAMatchDetails method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalFCRAMatchDetails method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalFCRAMatchDetailsForArrayConstraintsFromSetGlobalFCRAMatchDetails(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalFCRAMatchDetailsGlobalFCRAMatchDetailsItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalFCRAMatchDetailsGlobalFCRAMatchDetailsItem instanceof \ID3Global\Models\GlobalFCRAMatchDetails) {
                $invalidValues[] = is_object($arrayOfGlobalFCRAMatchDetailsGlobalFCRAMatchDetailsItem) ? get_class($arrayOfGlobalFCRAMatchDetailsGlobalFCRAMatchDetailsItem) : sprintf('%s(%s)', gettype($arrayOfGlobalFCRAMatchDetailsGlobalFCRAMatchDetailsItem), var_export($arrayOfGlobalFCRAMatchDetailsGlobalFCRAMatchDetailsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalFCRAMatchDetails property can only contain items of type \ID3Global\Models\GlobalFCRAMatchDetails, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalFCRAMatchDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalFCRAMatchDetails[] $globalFCRAMatchDetails
     * @return \ID3Global\Arrays\ArrayOfGlobalFCRAMatchDetails
     */
    public function setGlobalFCRAMatchDetails(?array $globalFCRAMatchDetails = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalFCRAMatchDetailsArrayErrorMessage = self::validateGlobalFCRAMatchDetailsForArrayConstraintsFromSetGlobalFCRAMatchDetails($globalFCRAMatchDetails))) {
            throw new InvalidArgumentException($globalFCRAMatchDetailsArrayErrorMessage, __LINE__);
        }
        if (is_null($globalFCRAMatchDetails) || (is_array($globalFCRAMatchDetails) && empty($globalFCRAMatchDetails))) {
            unset($this->GlobalFCRAMatchDetails);
        } else {
            $this->GlobalFCRAMatchDetails = $globalFCRAMatchDetails;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalFCRAMatchDetails|null
     */
    public function current(): ?\ID3Global\Models\GlobalFCRAMatchDetails
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalFCRAMatchDetails|null
     */
    public function item($index): ?\ID3Global\Models\GlobalFCRAMatchDetails
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalFCRAMatchDetails|null
     */
    public function first(): ?\ID3Global\Models\GlobalFCRAMatchDetails
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalFCRAMatchDetails|null
     */
    public function last(): ?\ID3Global\Models\GlobalFCRAMatchDetails
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalFCRAMatchDetails|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalFCRAMatchDetails
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalFCRAMatchDetails $item
     * @return \ID3Global\Arrays\ArrayOfGlobalFCRAMatchDetails
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalFCRAMatchDetails) {
            throw new InvalidArgumentException(sprintf('The GlobalFCRAMatchDetails property can only contain items of type \ID3Global\Models\GlobalFCRAMatchDetails, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalFCRAMatchDetails
     */
    public function getAttributeName(): string
    {
        return 'GlobalFCRAMatchDetails';
    }
}
