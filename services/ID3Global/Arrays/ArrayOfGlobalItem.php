<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalItem Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q211:ArrayOfGlobalItem
 * @subpackage Arrays
 */
class ArrayOfGlobalItem extends AbstractStructArrayBase
{
    /**
     * The GlobalItem
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalItem[]
     */
    protected ?array $GlobalItem = null;
    /**
     * Constructor method for ArrayOfGlobalItem
     * @uses ArrayOfGlobalItem::setGlobalItem()
     * @param \ID3Global\Models\GlobalItem[] $globalItem
     */
    public function __construct(?array $globalItem = null)
    {
        $this
            ->setGlobalItem($globalItem);
    }
    /**
     * Get GlobalItem value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalItem[]
     */
    public function getGlobalItem(): ?array
    {
        return isset($this->GlobalItem) ? $this->GlobalItem : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalItem method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalItemForArrayConstraintsFromSetGlobalItem(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalItemGlobalItemItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalItemGlobalItemItem instanceof \ID3Global\Models\GlobalItem) {
                $invalidValues[] = is_object($arrayOfGlobalItemGlobalItemItem) ? get_class($arrayOfGlobalItemGlobalItemItem) : sprintf('%s(%s)', gettype($arrayOfGlobalItemGlobalItemItem), var_export($arrayOfGlobalItemGlobalItemItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalItem property can only contain items of type \ID3Global\Models\GlobalItem, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalItem value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItem[] $globalItem
     * @return \ID3Global\Arrays\ArrayOfGlobalItem
     */
    public function setGlobalItem(?array $globalItem = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalItemArrayErrorMessage = self::validateGlobalItemForArrayConstraintsFromSetGlobalItem($globalItem))) {
            throw new InvalidArgumentException($globalItemArrayErrorMessage, __LINE__);
        }
        if (is_null($globalItem) || (is_array($globalItem) && empty($globalItem))) {
            unset($this->GlobalItem);
        } else {
            $this->GlobalItem = $globalItem;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalItem|null
     */
    public function current(): ?\ID3Global\Models\GlobalItem
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalItem|null
     */
    public function item($index): ?\ID3Global\Models\GlobalItem
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalItem|null
     */
    public function first(): ?\ID3Global\Models\GlobalItem
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalItem|null
     */
    public function last(): ?\ID3Global\Models\GlobalItem
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalItem|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalItem
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItem $item
     * @return \ID3Global\Arrays\ArrayOfGlobalItem
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalItem) {
            throw new InvalidArgumentException(sprintf('The GlobalItem property can only contain items of type \ID3Global\Models\GlobalItem, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalItem
     */
    public function getAttributeName(): string
    {
        return 'GlobalItem';
    }
}
