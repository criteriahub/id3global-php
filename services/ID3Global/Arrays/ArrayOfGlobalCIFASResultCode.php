<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCIFASResultCode Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q69:ArrayOfGlobalCIFASResultCode
 * @subpackage Arrays
 */
class ArrayOfGlobalCIFASResultCode extends AbstractStructArrayBase
{
    /**
     * The GlobalCIFASResultCode
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCIFASResultCode[]
     */
    protected ?array $GlobalCIFASResultCode = null;
    /**
     * Constructor method for ArrayOfGlobalCIFASResultCode
     * @uses ArrayOfGlobalCIFASResultCode::setGlobalCIFASResultCode()
     * @param \ID3Global\Models\GlobalCIFASResultCode[] $globalCIFASResultCode
     */
    public function __construct(?array $globalCIFASResultCode = null)
    {
        $this
            ->setGlobalCIFASResultCode($globalCIFASResultCode);
    }
    /**
     * Get GlobalCIFASResultCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCIFASResultCode[]
     */
    public function getGlobalCIFASResultCode(): ?array
    {
        return isset($this->GlobalCIFASResultCode) ? $this->GlobalCIFASResultCode : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCIFASResultCode method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCIFASResultCode method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCIFASResultCodeForArrayConstraintsFromSetGlobalCIFASResultCode(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCIFASResultCodeGlobalCIFASResultCodeItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCIFASResultCodeGlobalCIFASResultCodeItem instanceof \ID3Global\Models\GlobalCIFASResultCode) {
                $invalidValues[] = is_object($arrayOfGlobalCIFASResultCodeGlobalCIFASResultCodeItem) ? get_class($arrayOfGlobalCIFASResultCodeGlobalCIFASResultCodeItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCIFASResultCodeGlobalCIFASResultCodeItem), var_export($arrayOfGlobalCIFASResultCodeGlobalCIFASResultCodeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCIFASResultCode property can only contain items of type \ID3Global\Models\GlobalCIFASResultCode, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCIFASResultCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCIFASResultCode[] $globalCIFASResultCode
     * @return \ID3Global\Arrays\ArrayOfGlobalCIFASResultCode
     */
    public function setGlobalCIFASResultCode(?array $globalCIFASResultCode = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCIFASResultCodeArrayErrorMessage = self::validateGlobalCIFASResultCodeForArrayConstraintsFromSetGlobalCIFASResultCode($globalCIFASResultCode))) {
            throw new InvalidArgumentException($globalCIFASResultCodeArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCIFASResultCode) || (is_array($globalCIFASResultCode) && empty($globalCIFASResultCode))) {
            unset($this->GlobalCIFASResultCode);
        } else {
            $this->GlobalCIFASResultCode = $globalCIFASResultCode;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCIFASResultCode|null
     */
    public function current(): ?\ID3Global\Models\GlobalCIFASResultCode
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCIFASResultCode|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCIFASResultCode
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCIFASResultCode|null
     */
    public function first(): ?\ID3Global\Models\GlobalCIFASResultCode
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCIFASResultCode|null
     */
    public function last(): ?\ID3Global\Models\GlobalCIFASResultCode
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCIFASResultCode|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCIFASResultCode
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCIFASResultCode $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCIFASResultCode
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCIFASResultCode) {
            throw new InvalidArgumentException(sprintf('The GlobalCIFASResultCode property can only contain items of type \ID3Global\Models\GlobalCIFASResultCode, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCIFASResultCode
     */
    public function getAttributeName(): string
    {
        return 'GlobalCIFASResultCode';
    }
}
