<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSanctionsMatch Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q47:ArrayOfGlobalSanctionsMatch
 * @subpackage Arrays
 */
class ArrayOfGlobalSanctionsMatch extends AbstractStructArrayBase
{
    /**
     * The GlobalSanctionsMatch
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSanctionsMatch[]
     */
    protected ?array $GlobalSanctionsMatch = null;
    /**
     * Constructor method for ArrayOfGlobalSanctionsMatch
     * @uses ArrayOfGlobalSanctionsMatch::setGlobalSanctionsMatch()
     * @param \ID3Global\Models\GlobalSanctionsMatch[] $globalSanctionsMatch
     */
    public function __construct(?array $globalSanctionsMatch = null)
    {
        $this
            ->setGlobalSanctionsMatch($globalSanctionsMatch);
    }
    /**
     * Get GlobalSanctionsMatch value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSanctionsMatch[]
     */
    public function getGlobalSanctionsMatch(): ?array
    {
        return isset($this->GlobalSanctionsMatch) ? $this->GlobalSanctionsMatch : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSanctionsMatch method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSanctionsMatch method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSanctionsMatchForArrayConstraintsFromSetGlobalSanctionsMatch(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSanctionsMatchGlobalSanctionsMatchItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSanctionsMatchGlobalSanctionsMatchItem instanceof \ID3Global\Models\GlobalSanctionsMatch) {
                $invalidValues[] = is_object($arrayOfGlobalSanctionsMatchGlobalSanctionsMatchItem) ? get_class($arrayOfGlobalSanctionsMatchGlobalSanctionsMatchItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSanctionsMatchGlobalSanctionsMatchItem), var_export($arrayOfGlobalSanctionsMatchGlobalSanctionsMatchItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSanctionsMatch property can only contain items of type \ID3Global\Models\GlobalSanctionsMatch, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSanctionsMatch value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSanctionsMatch[] $globalSanctionsMatch
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsMatch
     */
    public function setGlobalSanctionsMatch(?array $globalSanctionsMatch = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSanctionsMatchArrayErrorMessage = self::validateGlobalSanctionsMatchForArrayConstraintsFromSetGlobalSanctionsMatch($globalSanctionsMatch))) {
            throw new InvalidArgumentException($globalSanctionsMatchArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSanctionsMatch) || (is_array($globalSanctionsMatch) && empty($globalSanctionsMatch))) {
            unset($this->GlobalSanctionsMatch);
        } else {
            $this->GlobalSanctionsMatch = $globalSanctionsMatch;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSanctionsMatch|null
     */
    public function current(): ?\ID3Global\Models\GlobalSanctionsMatch
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSanctionsMatch|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSanctionsMatch
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSanctionsMatch|null
     */
    public function first(): ?\ID3Global\Models\GlobalSanctionsMatch
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSanctionsMatch|null
     */
    public function last(): ?\ID3Global\Models\GlobalSanctionsMatch
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSanctionsMatch|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSanctionsMatch
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSanctionsMatch $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsMatch
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSanctionsMatch) {
            throw new InvalidArgumentException(sprintf('The GlobalSanctionsMatch property can only contain items of type \ID3Global\Models\GlobalSanctionsMatch, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSanctionsMatch
     */
    public function getAttributeName(): string
    {
        return 'GlobalSanctionsMatch';
    }
}
