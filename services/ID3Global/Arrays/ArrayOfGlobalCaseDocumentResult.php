<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseDocumentResult Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q865:ArrayOfGlobalCaseDocumentResult
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseDocumentResult extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseDocumentResult
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseDocumentResult[]
     */
    protected ?array $GlobalCaseDocumentResult = null;
    /**
     * Constructor method for ArrayOfGlobalCaseDocumentResult
     * @uses ArrayOfGlobalCaseDocumentResult::setGlobalCaseDocumentResult()
     * @param \ID3Global\Models\GlobalCaseDocumentResult[] $globalCaseDocumentResult
     */
    public function __construct(?array $globalCaseDocumentResult = null)
    {
        $this
            ->setGlobalCaseDocumentResult($globalCaseDocumentResult);
    }
    /**
     * Get GlobalCaseDocumentResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseDocumentResult[]
     */
    public function getGlobalCaseDocumentResult(): ?array
    {
        return isset($this->GlobalCaseDocumentResult) ? $this->GlobalCaseDocumentResult : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseDocumentResult method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseDocumentResult method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseDocumentResultForArrayConstraintsFromSetGlobalCaseDocumentResult(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseDocumentResultGlobalCaseDocumentResultItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseDocumentResultGlobalCaseDocumentResultItem instanceof \ID3Global\Models\GlobalCaseDocumentResult) {
                $invalidValues[] = is_object($arrayOfGlobalCaseDocumentResultGlobalCaseDocumentResultItem) ? get_class($arrayOfGlobalCaseDocumentResultGlobalCaseDocumentResultItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseDocumentResultGlobalCaseDocumentResultItem), var_export($arrayOfGlobalCaseDocumentResultGlobalCaseDocumentResultItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseDocumentResult property can only contain items of type \ID3Global\Models\GlobalCaseDocumentResult, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseDocumentResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseDocumentResult[] $globalCaseDocumentResult
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDocumentResult
     */
    public function setGlobalCaseDocumentResult(?array $globalCaseDocumentResult = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseDocumentResultArrayErrorMessage = self::validateGlobalCaseDocumentResultForArrayConstraintsFromSetGlobalCaseDocumentResult($globalCaseDocumentResult))) {
            throw new InvalidArgumentException($globalCaseDocumentResultArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseDocumentResult) || (is_array($globalCaseDocumentResult) && empty($globalCaseDocumentResult))) {
            unset($this->GlobalCaseDocumentResult);
        } else {
            $this->GlobalCaseDocumentResult = $globalCaseDocumentResult;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseDocumentResult|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseDocumentResult
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseDocumentResult|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseDocumentResult
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseDocumentResult|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseDocumentResult
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseDocumentResult|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseDocumentResult
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseDocumentResult|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseDocumentResult
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseDocumentResult $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDocumentResult
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseDocumentResult) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseDocumentResult property can only contain items of type \ID3Global\Models\GlobalCaseDocumentResult, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseDocumentResult
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseDocumentResult';
    }
}
