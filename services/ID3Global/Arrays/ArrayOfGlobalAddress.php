<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalAddress Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q333:ArrayOfGlobalAddress
 * @subpackage Arrays
 */
class ArrayOfGlobalAddress extends AbstractStructArrayBase
{
    /**
     * The GlobalAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAddress[]
     */
    protected ?array $GlobalAddress = null;
    /**
     * Constructor method for ArrayOfGlobalAddress
     * @uses ArrayOfGlobalAddress::setGlobalAddress()
     * @param \ID3Global\Models\GlobalAddress[] $globalAddress
     */
    public function __construct(?array $globalAddress = null)
    {
        $this
            ->setGlobalAddress($globalAddress);
    }
    /**
     * Get GlobalAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAddress[]
     */
    public function getGlobalAddress(): ?array
    {
        return isset($this->GlobalAddress) ? $this->GlobalAddress : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalAddress method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalAddress method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalAddressForArrayConstraintsFromSetGlobalAddress(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalAddressGlobalAddressItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalAddressGlobalAddressItem instanceof \ID3Global\Models\GlobalAddress) {
                $invalidValues[] = is_object($arrayOfGlobalAddressGlobalAddressItem) ? get_class($arrayOfGlobalAddressGlobalAddressItem) : sprintf('%s(%s)', gettype($arrayOfGlobalAddressGlobalAddressItem), var_export($arrayOfGlobalAddressGlobalAddressItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalAddress property can only contain items of type \ID3Global\Models\GlobalAddress, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalAddress[] $globalAddress
     * @return \ID3Global\Arrays\ArrayOfGlobalAddress
     */
    public function setGlobalAddress(?array $globalAddress = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalAddressArrayErrorMessage = self::validateGlobalAddressForArrayConstraintsFromSetGlobalAddress($globalAddress))) {
            throw new InvalidArgumentException($globalAddressArrayErrorMessage, __LINE__);
        }
        if (is_null($globalAddress) || (is_array($globalAddress) && empty($globalAddress))) {
            unset($this->GlobalAddress);
        } else {
            $this->GlobalAddress = $globalAddress;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalAddress|null
     */
    public function current(): ?\ID3Global\Models\GlobalAddress
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalAddress|null
     */
    public function item($index): ?\ID3Global\Models\GlobalAddress
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalAddress|null
     */
    public function first(): ?\ID3Global\Models\GlobalAddress
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalAddress|null
     */
    public function last(): ?\ID3Global\Models\GlobalAddress
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalAddress|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalAddress
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalAddress $item
     * @return \ID3Global\Arrays\ArrayOfGlobalAddress
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalAddress) {
            throw new InvalidArgumentException(sprintf('The GlobalAddress property can only contain items of type \ID3Global\Models\GlobalAddress, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalAddress
     */
    public function getAttributeName(): string
    {
        return 'GlobalAddress';
    }
}
