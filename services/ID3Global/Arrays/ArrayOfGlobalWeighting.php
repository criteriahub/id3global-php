<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalWeighting Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q244:ArrayOfGlobalWeighting
 * @subpackage Arrays
 */
class ArrayOfGlobalWeighting extends AbstractStructArrayBase
{
    /**
     * The GlobalWeighting
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalWeighting[]
     */
    protected ?array $GlobalWeighting = null;
    /**
     * Constructor method for ArrayOfGlobalWeighting
     * @uses ArrayOfGlobalWeighting::setGlobalWeighting()
     * @param \ID3Global\Models\GlobalWeighting[] $globalWeighting
     */
    public function __construct(?array $globalWeighting = null)
    {
        $this
            ->setGlobalWeighting($globalWeighting);
    }
    /**
     * Get GlobalWeighting value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalWeighting[]
     */
    public function getGlobalWeighting(): ?array
    {
        return isset($this->GlobalWeighting) ? $this->GlobalWeighting : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalWeighting method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalWeighting method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalWeightingForArrayConstraintsFromSetGlobalWeighting(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalWeightingGlobalWeightingItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalWeightingGlobalWeightingItem instanceof \ID3Global\Models\GlobalWeighting) {
                $invalidValues[] = is_object($arrayOfGlobalWeightingGlobalWeightingItem) ? get_class($arrayOfGlobalWeightingGlobalWeightingItem) : sprintf('%s(%s)', gettype($arrayOfGlobalWeightingGlobalWeightingItem), var_export($arrayOfGlobalWeightingGlobalWeightingItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalWeighting property can only contain items of type \ID3Global\Models\GlobalWeighting, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalWeighting value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalWeighting[] $globalWeighting
     * @return \ID3Global\Arrays\ArrayOfGlobalWeighting
     */
    public function setGlobalWeighting(?array $globalWeighting = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalWeightingArrayErrorMessage = self::validateGlobalWeightingForArrayConstraintsFromSetGlobalWeighting($globalWeighting))) {
            throw new InvalidArgumentException($globalWeightingArrayErrorMessage, __LINE__);
        }
        if (is_null($globalWeighting) || (is_array($globalWeighting) && empty($globalWeighting))) {
            unset($this->GlobalWeighting);
        } else {
            $this->GlobalWeighting = $globalWeighting;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalWeighting|null
     */
    public function current(): ?\ID3Global\Models\GlobalWeighting
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalWeighting|null
     */
    public function item($index): ?\ID3Global\Models\GlobalWeighting
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalWeighting|null
     */
    public function first(): ?\ID3Global\Models\GlobalWeighting
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalWeighting|null
     */
    public function last(): ?\ID3Global\Models\GlobalWeighting
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalWeighting|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalWeighting
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalWeighting $item
     * @return \ID3Global\Arrays\ArrayOfGlobalWeighting
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalWeighting) {
            throw new InvalidArgumentException(sprintf('The GlobalWeighting property can only contain items of type \ID3Global\Models\GlobalWeighting, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalWeighting
     */
    public function getAttributeName(): string
    {
        return 'GlobalWeighting';
    }
}
