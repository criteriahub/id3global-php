<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalItemCheckInputField Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q287:ArrayOfGlobalItemCheckInputField
 * @subpackage Arrays
 */
class ArrayOfGlobalItemCheckInputField extends AbstractStructArrayBase
{
    /**
     * The GlobalItemCheckInputField
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalItemCheckInputField[]
     */
    protected ?array $GlobalItemCheckInputField = null;
    /**
     * Constructor method for ArrayOfGlobalItemCheckInputField
     * @uses ArrayOfGlobalItemCheckInputField::setGlobalItemCheckInputField()
     * @param \ID3Global\Models\GlobalItemCheckInputField[] $globalItemCheckInputField
     */
    public function __construct(?array $globalItemCheckInputField = null)
    {
        $this
            ->setGlobalItemCheckInputField($globalItemCheckInputField);
    }
    /**
     * Get GlobalItemCheckInputField value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalItemCheckInputField[]
     */
    public function getGlobalItemCheckInputField(): ?array
    {
        return isset($this->GlobalItemCheckInputField) ? $this->GlobalItemCheckInputField : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalItemCheckInputField method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalItemCheckInputField method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalItemCheckInputFieldForArrayConstraintsFromSetGlobalItemCheckInputField(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalItemCheckInputFieldGlobalItemCheckInputFieldItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalItemCheckInputFieldGlobalItemCheckInputFieldItem instanceof \ID3Global\Models\GlobalItemCheckInputField) {
                $invalidValues[] = is_object($arrayOfGlobalItemCheckInputFieldGlobalItemCheckInputFieldItem) ? get_class($arrayOfGlobalItemCheckInputFieldGlobalItemCheckInputFieldItem) : sprintf('%s(%s)', gettype($arrayOfGlobalItemCheckInputFieldGlobalItemCheckInputFieldItem), var_export($arrayOfGlobalItemCheckInputFieldGlobalItemCheckInputFieldItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalItemCheckInputField property can only contain items of type \ID3Global\Models\GlobalItemCheckInputField, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalItemCheckInputField value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckInputField[] $globalItemCheckInputField
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckInputField
     */
    public function setGlobalItemCheckInputField(?array $globalItemCheckInputField = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalItemCheckInputFieldArrayErrorMessage = self::validateGlobalItemCheckInputFieldForArrayConstraintsFromSetGlobalItemCheckInputField($globalItemCheckInputField))) {
            throw new InvalidArgumentException($globalItemCheckInputFieldArrayErrorMessage, __LINE__);
        }
        if (is_null($globalItemCheckInputField) || (is_array($globalItemCheckInputField) && empty($globalItemCheckInputField))) {
            unset($this->GlobalItemCheckInputField);
        } else {
            $this->GlobalItemCheckInputField = $globalItemCheckInputField;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalItemCheckInputField|null
     */
    public function current(): ?\ID3Global\Models\GlobalItemCheckInputField
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalItemCheckInputField|null
     */
    public function item($index): ?\ID3Global\Models\GlobalItemCheckInputField
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalItemCheckInputField|null
     */
    public function first(): ?\ID3Global\Models\GlobalItemCheckInputField
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalItemCheckInputField|null
     */
    public function last(): ?\ID3Global\Models\GlobalItemCheckInputField
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalItemCheckInputField|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalItemCheckInputField
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckInputField $item
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckInputField
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalItemCheckInputField) {
            throw new InvalidArgumentException(sprintf('The GlobalItemCheckInputField property can only contain items of type \ID3Global\Models\GlobalItemCheckInputField, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalItemCheckInputField
     */
    public function getAttributeName(): string
    {
        return 'GlobalItemCheckInputField';
    }
}
