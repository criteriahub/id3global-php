<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalOrganisation Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q566:ArrayOfGlobalOrganisation
 * @subpackage Arrays
 */
class ArrayOfGlobalOrganisation extends AbstractStructArrayBase
{
    /**
     * The GlobalOrganisation
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalOrganisation[]
     */
    protected ?array $GlobalOrganisation = null;
    /**
     * Constructor method for ArrayOfGlobalOrganisation
     * @uses ArrayOfGlobalOrganisation::setGlobalOrganisation()
     * @param \ID3Global\Models\GlobalOrganisation[] $globalOrganisation
     */
    public function __construct(?array $globalOrganisation = null)
    {
        $this
            ->setGlobalOrganisation($globalOrganisation);
    }
    /**
     * Get GlobalOrganisation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalOrganisation[]
     */
    public function getGlobalOrganisation(): ?array
    {
        return isset($this->GlobalOrganisation) ? $this->GlobalOrganisation : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalOrganisation method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalOrganisation method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalOrganisationForArrayConstraintsFromSetGlobalOrganisation(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalOrganisationGlobalOrganisationItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalOrganisationGlobalOrganisationItem instanceof \ID3Global\Models\GlobalOrganisation) {
                $invalidValues[] = is_object($arrayOfGlobalOrganisationGlobalOrganisationItem) ? get_class($arrayOfGlobalOrganisationGlobalOrganisationItem) : sprintf('%s(%s)', gettype($arrayOfGlobalOrganisationGlobalOrganisationItem), var_export($arrayOfGlobalOrganisationGlobalOrganisationItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalOrganisation property can only contain items of type \ID3Global\Models\GlobalOrganisation, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalOrganisation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalOrganisation[] $globalOrganisation
     * @return \ID3Global\Arrays\ArrayOfGlobalOrganisation
     */
    public function setGlobalOrganisation(?array $globalOrganisation = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalOrganisationArrayErrorMessage = self::validateGlobalOrganisationForArrayConstraintsFromSetGlobalOrganisation($globalOrganisation))) {
            throw new InvalidArgumentException($globalOrganisationArrayErrorMessage, __LINE__);
        }
        if (is_null($globalOrganisation) || (is_array($globalOrganisation) && empty($globalOrganisation))) {
            unset($this->GlobalOrganisation);
        } else {
            $this->GlobalOrganisation = $globalOrganisation;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalOrganisation|null
     */
    public function current(): ?\ID3Global\Models\GlobalOrganisation
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalOrganisation|null
     */
    public function item($index): ?\ID3Global\Models\GlobalOrganisation
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalOrganisation|null
     */
    public function first(): ?\ID3Global\Models\GlobalOrganisation
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalOrganisation|null
     */
    public function last(): ?\ID3Global\Models\GlobalOrganisation
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalOrganisation|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalOrganisation
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalOrganisation $item
     * @return \ID3Global\Arrays\ArrayOfGlobalOrganisation
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalOrganisation) {
            throw new InvalidArgumentException(sprintf('The GlobalOrganisation property can only contain items of type \ID3Global\Models\GlobalOrganisation, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalOrganisation
     */
    public function getAttributeName(): string
    {
        return 'GlobalOrganisation';
    }
}
