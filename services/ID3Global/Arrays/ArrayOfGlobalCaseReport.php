<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseReport Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q798:ArrayOfGlobalCaseReport
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseReport extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseReport
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseReport[]
     */
    protected ?array $GlobalCaseReport = null;
    /**
     * Constructor method for ArrayOfGlobalCaseReport
     * @uses ArrayOfGlobalCaseReport::setGlobalCaseReport()
     * @param \ID3Global\Models\GlobalCaseReport[] $globalCaseReport
     */
    public function __construct(?array $globalCaseReport = null)
    {
        $this
            ->setGlobalCaseReport($globalCaseReport);
    }
    /**
     * Get GlobalCaseReport value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseReport[]
     */
    public function getGlobalCaseReport(): ?array
    {
        return isset($this->GlobalCaseReport) ? $this->GlobalCaseReport : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseReport method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseReport method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseReportForArrayConstraintsFromSetGlobalCaseReport(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseReportGlobalCaseReportItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseReportGlobalCaseReportItem instanceof \ID3Global\Models\GlobalCaseReport) {
                $invalidValues[] = is_object($arrayOfGlobalCaseReportGlobalCaseReportItem) ? get_class($arrayOfGlobalCaseReportGlobalCaseReportItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseReportGlobalCaseReportItem), var_export($arrayOfGlobalCaseReportGlobalCaseReportItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseReport property can only contain items of type \ID3Global\Models\GlobalCaseReport, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseReport value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseReport[] $globalCaseReport
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseReport
     */
    public function setGlobalCaseReport(?array $globalCaseReport = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseReportArrayErrorMessage = self::validateGlobalCaseReportForArrayConstraintsFromSetGlobalCaseReport($globalCaseReport))) {
            throw new InvalidArgumentException($globalCaseReportArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseReport) || (is_array($globalCaseReport) && empty($globalCaseReport))) {
            unset($this->GlobalCaseReport);
        } else {
            $this->GlobalCaseReport = $globalCaseReport;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseReport|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseReport
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseReport|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseReport
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseReport|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseReport
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseReport|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseReport
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseReport|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseReport
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseReport $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseReport
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseReport) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseReport property can only contain items of type \ID3Global\Models\GlobalCaseReport, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseReport
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseReport';
    }
}
