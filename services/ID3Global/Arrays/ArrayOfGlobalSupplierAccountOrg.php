<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSupplierAccountOrg Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q641:ArrayOfGlobalSupplierAccountOrg
 * @subpackage Arrays
 */
class ArrayOfGlobalSupplierAccountOrg extends AbstractStructArrayBase
{
    /**
     * The GlobalSupplierAccountOrg
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierAccountOrg[]
     */
    protected ?array $GlobalSupplierAccountOrg = null;
    /**
     * Constructor method for ArrayOfGlobalSupplierAccountOrg
     * @uses ArrayOfGlobalSupplierAccountOrg::setGlobalSupplierAccountOrg()
     * @param \ID3Global\Models\GlobalSupplierAccountOrg[] $globalSupplierAccountOrg
     */
    public function __construct(?array $globalSupplierAccountOrg = null)
    {
        $this
            ->setGlobalSupplierAccountOrg($globalSupplierAccountOrg);
    }
    /**
     * Get GlobalSupplierAccountOrg value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierAccountOrg[]
     */
    public function getGlobalSupplierAccountOrg(): ?array
    {
        return isset($this->GlobalSupplierAccountOrg) ? $this->GlobalSupplierAccountOrg : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSupplierAccountOrg method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSupplierAccountOrg method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSupplierAccountOrgForArrayConstraintsFromSetGlobalSupplierAccountOrg(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSupplierAccountOrgGlobalSupplierAccountOrgItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSupplierAccountOrgGlobalSupplierAccountOrgItem instanceof \ID3Global\Models\GlobalSupplierAccountOrg) {
                $invalidValues[] = is_object($arrayOfGlobalSupplierAccountOrgGlobalSupplierAccountOrgItem) ? get_class($arrayOfGlobalSupplierAccountOrgGlobalSupplierAccountOrgItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSupplierAccountOrgGlobalSupplierAccountOrgItem), var_export($arrayOfGlobalSupplierAccountOrgGlobalSupplierAccountOrgItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSupplierAccountOrg property can only contain items of type \ID3Global\Models\GlobalSupplierAccountOrg, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSupplierAccountOrg value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupplierAccountOrg[] $globalSupplierAccountOrg
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierAccountOrg
     */
    public function setGlobalSupplierAccountOrg(?array $globalSupplierAccountOrg = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSupplierAccountOrgArrayErrorMessage = self::validateGlobalSupplierAccountOrgForArrayConstraintsFromSetGlobalSupplierAccountOrg($globalSupplierAccountOrg))) {
            throw new InvalidArgumentException($globalSupplierAccountOrgArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSupplierAccountOrg) || (is_array($globalSupplierAccountOrg) && empty($globalSupplierAccountOrg))) {
            unset($this->GlobalSupplierAccountOrg);
        } else {
            $this->GlobalSupplierAccountOrg = $globalSupplierAccountOrg;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSupplierAccountOrg|null
     */
    public function current(): ?\ID3Global\Models\GlobalSupplierAccountOrg
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSupplierAccountOrg|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSupplierAccountOrg
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSupplierAccountOrg|null
     */
    public function first(): ?\ID3Global\Models\GlobalSupplierAccountOrg
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSupplierAccountOrg|null
     */
    public function last(): ?\ID3Global\Models\GlobalSupplierAccountOrg
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSupplierAccountOrg|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSupplierAccountOrg
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupplierAccountOrg $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierAccountOrg
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSupplierAccountOrg) {
            throw new InvalidArgumentException(sprintf('The GlobalSupplierAccountOrg property can only contain items of type \ID3Global\Models\GlobalSupplierAccountOrg, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSupplierAccountOrg
     */
    public function getAttributeName(): string
    {
        return 'GlobalSupplierAccountOrg';
    }
}
