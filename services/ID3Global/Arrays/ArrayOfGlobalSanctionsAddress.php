<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSanctionsAddress Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q479:ArrayOfGlobalSanctionsAddress
 * @subpackage Arrays
 */
class ArrayOfGlobalSanctionsAddress extends AbstractStructArrayBase
{
    /**
     * The GlobalSanctionsAddress
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSanctionsAddress[]
     */
    protected ?array $GlobalSanctionsAddress = null;
    /**
     * Constructor method for ArrayOfGlobalSanctionsAddress
     * @uses ArrayOfGlobalSanctionsAddress::setGlobalSanctionsAddress()
     * @param \ID3Global\Models\GlobalSanctionsAddress[] $globalSanctionsAddress
     */
    public function __construct(?array $globalSanctionsAddress = null)
    {
        $this
            ->setGlobalSanctionsAddress($globalSanctionsAddress);
    }
    /**
     * Get GlobalSanctionsAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSanctionsAddress[]
     */
    public function getGlobalSanctionsAddress(): ?array
    {
        return isset($this->GlobalSanctionsAddress) ? $this->GlobalSanctionsAddress : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSanctionsAddress method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSanctionsAddress method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSanctionsAddressForArrayConstraintsFromSetGlobalSanctionsAddress(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSanctionsAddressGlobalSanctionsAddressItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSanctionsAddressGlobalSanctionsAddressItem instanceof \ID3Global\Models\GlobalSanctionsAddress) {
                $invalidValues[] = is_object($arrayOfGlobalSanctionsAddressGlobalSanctionsAddressItem) ? get_class($arrayOfGlobalSanctionsAddressGlobalSanctionsAddressItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSanctionsAddressGlobalSanctionsAddressItem), var_export($arrayOfGlobalSanctionsAddressGlobalSanctionsAddressItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSanctionsAddress property can only contain items of type \ID3Global\Models\GlobalSanctionsAddress, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSanctionsAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSanctionsAddress[] $globalSanctionsAddress
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsAddress
     */
    public function setGlobalSanctionsAddress(?array $globalSanctionsAddress = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSanctionsAddressArrayErrorMessage = self::validateGlobalSanctionsAddressForArrayConstraintsFromSetGlobalSanctionsAddress($globalSanctionsAddress))) {
            throw new InvalidArgumentException($globalSanctionsAddressArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSanctionsAddress) || (is_array($globalSanctionsAddress) && empty($globalSanctionsAddress))) {
            unset($this->GlobalSanctionsAddress);
        } else {
            $this->GlobalSanctionsAddress = $globalSanctionsAddress;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSanctionsAddress|null
     */
    public function current(): ?\ID3Global\Models\GlobalSanctionsAddress
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSanctionsAddress|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSanctionsAddress
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSanctionsAddress|null
     */
    public function first(): ?\ID3Global\Models\GlobalSanctionsAddress
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSanctionsAddress|null
     */
    public function last(): ?\ID3Global\Models\GlobalSanctionsAddress
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSanctionsAddress|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSanctionsAddress
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSanctionsAddress $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsAddress
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSanctionsAddress) {
            throw new InvalidArgumentException(sprintf('The GlobalSanctionsAddress property can only contain items of type \ID3Global\Models\GlobalSanctionsAddress, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSanctionsAddress
     */
    public function getAttributeName(): string
    {
        return 'GlobalSanctionsAddress';
    }
}
