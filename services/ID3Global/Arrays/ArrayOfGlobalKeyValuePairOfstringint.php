<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalKeyValuePairOfstringint Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q826:ArrayOfGlobalKeyValuePairOfstringint
 * @subpackage Arrays
 */
class ArrayOfGlobalKeyValuePairOfstringint extends AbstractStructArrayBase
{
    /**
     * The GlobalKeyValuePairOfstringint
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \ID3Global\Models\GlobalKeyValuePairOfstringint[]
     */
    protected ?array $GlobalKeyValuePairOfstringint = null;
    /**
     * Constructor method for ArrayOfGlobalKeyValuePairOfstringint
     * @uses ArrayOfGlobalKeyValuePairOfstringint::setGlobalKeyValuePairOfstringint()
     * @param \ID3Global\Models\GlobalKeyValuePairOfstringint[] $globalKeyValuePairOfstringint
     */
    public function __construct(?array $globalKeyValuePairOfstringint = null)
    {
        $this
            ->setGlobalKeyValuePairOfstringint($globalKeyValuePairOfstringint);
    }
    /**
     * Get GlobalKeyValuePairOfstringint value
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringint[]
     */
    public function getGlobalKeyValuePairOfstringint(): ?array
    {
        return $this->GlobalKeyValuePairOfstringint;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalKeyValuePairOfstringint method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalKeyValuePairOfstringint method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalKeyValuePairOfstringintForArrayConstraintsFromSetGlobalKeyValuePairOfstringint(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalKeyValuePairOfstringintGlobalKeyValuePairOfstringintItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalKeyValuePairOfstringintGlobalKeyValuePairOfstringintItem instanceof \ID3Global\Models\GlobalKeyValuePairOfstringint) {
                $invalidValues[] = is_object($arrayOfGlobalKeyValuePairOfstringintGlobalKeyValuePairOfstringintItem) ? get_class($arrayOfGlobalKeyValuePairOfstringintGlobalKeyValuePairOfstringintItem) : sprintf('%s(%s)', gettype($arrayOfGlobalKeyValuePairOfstringintGlobalKeyValuePairOfstringintItem), var_export($arrayOfGlobalKeyValuePairOfstringintGlobalKeyValuePairOfstringintItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalKeyValuePairOfstringint property can only contain items of type \ID3Global\Models\GlobalKeyValuePairOfstringint, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalKeyValuePairOfstringint value
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalKeyValuePairOfstringint[] $globalKeyValuePairOfstringint
     * @return \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint
     */
    public function setGlobalKeyValuePairOfstringint(?array $globalKeyValuePairOfstringint = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalKeyValuePairOfstringintArrayErrorMessage = self::validateGlobalKeyValuePairOfstringintForArrayConstraintsFromSetGlobalKeyValuePairOfstringint($globalKeyValuePairOfstringint))) {
            throw new InvalidArgumentException($globalKeyValuePairOfstringintArrayErrorMessage, __LINE__);
        }
        $this->GlobalKeyValuePairOfstringint = $globalKeyValuePairOfstringint;
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringint|null
     */
    public function current(): ?\ID3Global\Models\GlobalKeyValuePairOfstringint
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringint|null
     */
    public function item($index): ?\ID3Global\Models\GlobalKeyValuePairOfstringint
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringint|null
     */
    public function first(): ?\ID3Global\Models\GlobalKeyValuePairOfstringint
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringint|null
     */
    public function last(): ?\ID3Global\Models\GlobalKeyValuePairOfstringint
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringint|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalKeyValuePairOfstringint
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalKeyValuePairOfstringint $item
     * @return \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalKeyValuePairOfstringint) {
            throw new InvalidArgumentException(sprintf('The GlobalKeyValuePairOfstringint property can only contain items of type \ID3Global\Models\GlobalKeyValuePairOfstringint, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalKeyValuePairOfstringint
     */
    public function getAttributeName(): string
    {
        return 'GlobalKeyValuePairOfstringint';
    }
}
