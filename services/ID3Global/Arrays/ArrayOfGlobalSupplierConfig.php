<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSupplierConfig Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q725:ArrayOfGlobalSupplierConfig
 * @subpackage Arrays
 */
class ArrayOfGlobalSupplierConfig extends AbstractStructArrayBase
{
    /**
     * The GlobalSupplierConfig
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierConfig[]
     */
    protected ?array $GlobalSupplierConfig = null;
    /**
     * Constructor method for ArrayOfGlobalSupplierConfig
     * @uses ArrayOfGlobalSupplierConfig::setGlobalSupplierConfig()
     * @param \ID3Global\Models\GlobalSupplierConfig[] $globalSupplierConfig
     */
    public function __construct(?array $globalSupplierConfig = null)
    {
        $this
            ->setGlobalSupplierConfig($globalSupplierConfig);
    }
    /**
     * Get GlobalSupplierConfig value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierConfig[]
     */
    public function getGlobalSupplierConfig(): ?array
    {
        return isset($this->GlobalSupplierConfig) ? $this->GlobalSupplierConfig : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSupplierConfig method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSupplierConfig method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSupplierConfigForArrayConstraintsFromSetGlobalSupplierConfig(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSupplierConfigGlobalSupplierConfigItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSupplierConfigGlobalSupplierConfigItem instanceof \ID3Global\Models\GlobalSupplierConfig) {
                $invalidValues[] = is_object($arrayOfGlobalSupplierConfigGlobalSupplierConfigItem) ? get_class($arrayOfGlobalSupplierConfigGlobalSupplierConfigItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSupplierConfigGlobalSupplierConfigItem), var_export($arrayOfGlobalSupplierConfigGlobalSupplierConfigItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSupplierConfig property can only contain items of type \ID3Global\Models\GlobalSupplierConfig, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSupplierConfig value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupplierConfig[] $globalSupplierConfig
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierConfig
     */
    public function setGlobalSupplierConfig(?array $globalSupplierConfig = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSupplierConfigArrayErrorMessage = self::validateGlobalSupplierConfigForArrayConstraintsFromSetGlobalSupplierConfig($globalSupplierConfig))) {
            throw new InvalidArgumentException($globalSupplierConfigArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSupplierConfig) || (is_array($globalSupplierConfig) && empty($globalSupplierConfig))) {
            unset($this->GlobalSupplierConfig);
        } else {
            $this->GlobalSupplierConfig = $globalSupplierConfig;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSupplierConfig|null
     */
    public function current(): ?\ID3Global\Models\GlobalSupplierConfig
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSupplierConfig|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSupplierConfig
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSupplierConfig|null
     */
    public function first(): ?\ID3Global\Models\GlobalSupplierConfig
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSupplierConfig|null
     */
    public function last(): ?\ID3Global\Models\GlobalSupplierConfig
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSupplierConfig|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSupplierConfig
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupplierConfig $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierConfig
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSupplierConfig) {
            throw new InvalidArgumentException(sprintf('The GlobalSupplierConfig property can only contain items of type \ID3Global\Models\GlobalSupplierConfig, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSupplierConfig
     */
    public function getAttributeName(): string
    {
        return 'GlobalSupplierConfig';
    }
}
