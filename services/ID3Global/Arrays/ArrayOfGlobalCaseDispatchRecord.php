<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseDispatchRecord Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q855:ArrayOfGlobalCaseDispatchRecord
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseDispatchRecord extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseDispatchRecord
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseDispatchRecord[]
     */
    protected ?array $GlobalCaseDispatchRecord = null;
    /**
     * Constructor method for ArrayOfGlobalCaseDispatchRecord
     * @uses ArrayOfGlobalCaseDispatchRecord::setGlobalCaseDispatchRecord()
     * @param \ID3Global\Models\GlobalCaseDispatchRecord[] $globalCaseDispatchRecord
     */
    public function __construct(?array $globalCaseDispatchRecord = null)
    {
        $this
            ->setGlobalCaseDispatchRecord($globalCaseDispatchRecord);
    }
    /**
     * Get GlobalCaseDispatchRecord value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseDispatchRecord[]
     */
    public function getGlobalCaseDispatchRecord(): ?array
    {
        return isset($this->GlobalCaseDispatchRecord) ? $this->GlobalCaseDispatchRecord : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseDispatchRecord method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseDispatchRecord method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseDispatchRecordForArrayConstraintsFromSetGlobalCaseDispatchRecord(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseDispatchRecordGlobalCaseDispatchRecordItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseDispatchRecordGlobalCaseDispatchRecordItem instanceof \ID3Global\Models\GlobalCaseDispatchRecord) {
                $invalidValues[] = is_object($arrayOfGlobalCaseDispatchRecordGlobalCaseDispatchRecordItem) ? get_class($arrayOfGlobalCaseDispatchRecordGlobalCaseDispatchRecordItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseDispatchRecordGlobalCaseDispatchRecordItem), var_export($arrayOfGlobalCaseDispatchRecordGlobalCaseDispatchRecordItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseDispatchRecord property can only contain items of type \ID3Global\Models\GlobalCaseDispatchRecord, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseDispatchRecord value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseDispatchRecord[] $globalCaseDispatchRecord
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDispatchRecord
     */
    public function setGlobalCaseDispatchRecord(?array $globalCaseDispatchRecord = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseDispatchRecordArrayErrorMessage = self::validateGlobalCaseDispatchRecordForArrayConstraintsFromSetGlobalCaseDispatchRecord($globalCaseDispatchRecord))) {
            throw new InvalidArgumentException($globalCaseDispatchRecordArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseDispatchRecord) || (is_array($globalCaseDispatchRecord) && empty($globalCaseDispatchRecord))) {
            unset($this->GlobalCaseDispatchRecord);
        } else {
            $this->GlobalCaseDispatchRecord = $globalCaseDispatchRecord;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseDispatchRecord|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseDispatchRecord
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseDispatchRecord|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseDispatchRecord
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseDispatchRecord|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseDispatchRecord
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseDispatchRecord|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseDispatchRecord
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseDispatchRecord|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseDispatchRecord
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseDispatchRecord $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDispatchRecord
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseDispatchRecord) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseDispatchRecord property can only contain items of type \ID3Global\Models\GlobalCaseDispatchRecord, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseDispatchRecord
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseDispatchRecord';
    }
}
