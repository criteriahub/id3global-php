<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalItemCheckResultCodes Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q439:ArrayOfGlobalItemCheckResultCodes
 * @subpackage Arrays
 */
class ArrayOfGlobalItemCheckResultCodes extends AbstractStructArrayBase
{
    /**
     * The GlobalItemCheckResultCodes
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalItemCheckResultCodes[]
     */
    protected ?array $GlobalItemCheckResultCodes = null;
    /**
     * Constructor method for ArrayOfGlobalItemCheckResultCodes
     * @uses ArrayOfGlobalItemCheckResultCodes::setGlobalItemCheckResultCodes()
     * @param \ID3Global\Models\GlobalItemCheckResultCodes[] $globalItemCheckResultCodes
     */
    public function __construct(?array $globalItemCheckResultCodes = null)
    {
        $this
            ->setGlobalItemCheckResultCodes($globalItemCheckResultCodes);
    }
    /**
     * Get GlobalItemCheckResultCodes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalItemCheckResultCodes[]
     */
    public function getGlobalItemCheckResultCodes(): ?array
    {
        return isset($this->GlobalItemCheckResultCodes) ? $this->GlobalItemCheckResultCodes : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalItemCheckResultCodes method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalItemCheckResultCodes method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalItemCheckResultCodesForArrayConstraintsFromSetGlobalItemCheckResultCodes(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalItemCheckResultCodesGlobalItemCheckResultCodesItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalItemCheckResultCodesGlobalItemCheckResultCodesItem instanceof \ID3Global\Models\GlobalItemCheckResultCodes) {
                $invalidValues[] = is_object($arrayOfGlobalItemCheckResultCodesGlobalItemCheckResultCodesItem) ? get_class($arrayOfGlobalItemCheckResultCodesGlobalItemCheckResultCodesItem) : sprintf('%s(%s)', gettype($arrayOfGlobalItemCheckResultCodesGlobalItemCheckResultCodesItem), var_export($arrayOfGlobalItemCheckResultCodesGlobalItemCheckResultCodesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalItemCheckResultCodes property can only contain items of type \ID3Global\Models\GlobalItemCheckResultCodes, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalItemCheckResultCodes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckResultCodes[] $globalItemCheckResultCodes
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes
     */
    public function setGlobalItemCheckResultCodes(?array $globalItemCheckResultCodes = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalItemCheckResultCodesArrayErrorMessage = self::validateGlobalItemCheckResultCodesForArrayConstraintsFromSetGlobalItemCheckResultCodes($globalItemCheckResultCodes))) {
            throw new InvalidArgumentException($globalItemCheckResultCodesArrayErrorMessage, __LINE__);
        }
        if (is_null($globalItemCheckResultCodes) || (is_array($globalItemCheckResultCodes) && empty($globalItemCheckResultCodes))) {
            unset($this->GlobalItemCheckResultCodes);
        } else {
            $this->GlobalItemCheckResultCodes = $globalItemCheckResultCodes;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalItemCheckResultCodes|null
     */
    public function current(): ?\ID3Global\Models\GlobalItemCheckResultCodes
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalItemCheckResultCodes|null
     */
    public function item($index): ?\ID3Global\Models\GlobalItemCheckResultCodes
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalItemCheckResultCodes|null
     */
    public function first(): ?\ID3Global\Models\GlobalItemCheckResultCodes
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalItemCheckResultCodes|null
     */
    public function last(): ?\ID3Global\Models\GlobalItemCheckResultCodes
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalItemCheckResultCodes|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalItemCheckResultCodes
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckResultCodes $item
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalItemCheckResultCodes) {
            throw new InvalidArgumentException(sprintf('The GlobalItemCheckResultCodes property can only contain items of type \ID3Global\Models\GlobalItemCheckResultCodes, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalItemCheckResultCodes
     */
    public function getAttributeName(): string
    {
        return 'GlobalItemCheckResultCodes';
    }
}
