<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfunsignedInt Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfunsignedInt
 * @subpackage Arrays
 */
class ArrayOfunsignedInt extends AbstractStructArrayBase
{
    /**
     * The unsignedInt
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var int[]
     */
    protected ?array $unsignedInt = null;
    /**
     * Constructor method for ArrayOfunsignedInt
     * @uses ArrayOfunsignedInt::setUnsignedInt()
     * @param int[] $unsignedInt
     */
    public function __construct(?array $unsignedInt = null)
    {
        $this
            ->setUnsignedInt($unsignedInt);
    }
    /**
     * Get unsignedInt value
     * @return int[]
     */
    public function getUnsignedInt(): ?array
    {
        return $this->unsignedInt;
    }
    /**
     * This method is responsible for validating the values passed to the setUnsignedInt method
     * This method is willingly generated in order to preserve the one-line inline validation within the setUnsignedInt method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateUnsignedIntForArrayConstraintsFromSetUnsignedInt(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfunsignedIntUnsignedIntItem) {
            // validation for constraint: itemType
            if (!(is_int($arrayOfunsignedIntUnsignedIntItem) || ctype_digit($arrayOfunsignedIntUnsignedIntItem))) {
                $invalidValues[] = is_object($arrayOfunsignedIntUnsignedIntItem) ? get_class($arrayOfunsignedIntUnsignedIntItem) : sprintf('%s(%s)', gettype($arrayOfunsignedIntUnsignedIntItem), var_export($arrayOfunsignedIntUnsignedIntItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The unsignedInt property can only contain items of type int, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set unsignedInt value
     * @throws InvalidArgumentException
     * @param int[] $unsignedInt
     * @return \ID3Global\Arrays\ArrayOfunsignedInt
     */
    public function setUnsignedInt(?array $unsignedInt = null): self
    {
        // validation for constraint: array
        if ('' !== ($unsignedIntArrayErrorMessage = self::validateUnsignedIntForArrayConstraintsFromSetUnsignedInt($unsignedInt))) {
            throw new InvalidArgumentException($unsignedIntArrayErrorMessage, __LINE__);
        }
        $this->unsignedInt = $unsignedInt;
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return int|null
     */
    public function current(): ?int
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return int|null
     */
    public function item($index): ?int
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return int|null
     */
    public function first(): ?int
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return int|null
     */
    public function last(): ?int
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return int|null
     */
    public function offsetGet($offset): ?int
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string unsignedInt
     */
    public function getAttributeName(): string
    {
        return 'unsignedInt';
    }
}
