<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalResource Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q573:ArrayOfGlobalResource
 * @subpackage Arrays
 */
class ArrayOfGlobalResource extends AbstractStructArrayBase
{
    /**
     * The GlobalResource
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalResource[]
     */
    protected ?array $GlobalResource = null;
    /**
     * Constructor method for ArrayOfGlobalResource
     * @uses ArrayOfGlobalResource::setGlobalResource()
     * @param \ID3Global\Models\GlobalResource[] $globalResource
     */
    public function __construct(?array $globalResource = null)
    {
        $this
            ->setGlobalResource($globalResource);
    }
    /**
     * Get GlobalResource value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalResource[]
     */
    public function getGlobalResource(): ?array
    {
        return isset($this->GlobalResource) ? $this->GlobalResource : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalResource method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalResource method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalResourceForArrayConstraintsFromSetGlobalResource(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalResourceGlobalResourceItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalResourceGlobalResourceItem instanceof \ID3Global\Models\GlobalResource) {
                $invalidValues[] = is_object($arrayOfGlobalResourceGlobalResourceItem) ? get_class($arrayOfGlobalResourceGlobalResourceItem) : sprintf('%s(%s)', gettype($arrayOfGlobalResourceGlobalResourceItem), var_export($arrayOfGlobalResourceGlobalResourceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalResource property can only contain items of type \ID3Global\Models\GlobalResource, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalResource value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalResource[] $globalResource
     * @return \ID3Global\Arrays\ArrayOfGlobalResource
     */
    public function setGlobalResource(?array $globalResource = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalResourceArrayErrorMessage = self::validateGlobalResourceForArrayConstraintsFromSetGlobalResource($globalResource))) {
            throw new InvalidArgumentException($globalResourceArrayErrorMessage, __LINE__);
        }
        if (is_null($globalResource) || (is_array($globalResource) && empty($globalResource))) {
            unset($this->GlobalResource);
        } else {
            $this->GlobalResource = $globalResource;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalResource|null
     */
    public function current(): ?\ID3Global\Models\GlobalResource
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalResource|null
     */
    public function item($index): ?\ID3Global\Models\GlobalResource
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalResource|null
     */
    public function first(): ?\ID3Global\Models\GlobalResource
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalResource|null
     */
    public function last(): ?\ID3Global\Models\GlobalResource
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalResource|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalResource
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalResource $item
     * @return \ID3Global\Arrays\ArrayOfGlobalResource
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalResource) {
            throw new InvalidArgumentException(sprintf('The GlobalResource property can only contain items of type \ID3Global\Models\GlobalResource, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalResource
     */
    public function getAttributeName(): string
    {
        return 'GlobalResource';
    }
}
