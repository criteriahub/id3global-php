<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalRolePermission Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q609:ArrayOfGlobalRolePermission
 * @subpackage Arrays
 */
class ArrayOfGlobalRolePermission extends AbstractStructArrayBase
{
    /**
     * The GlobalRolePermission
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalRolePermission[]
     */
    protected ?array $GlobalRolePermission = null;
    /**
     * Constructor method for ArrayOfGlobalRolePermission
     * @uses ArrayOfGlobalRolePermission::setGlobalRolePermission()
     * @param \ID3Global\Models\GlobalRolePermission[] $globalRolePermission
     */
    public function __construct(?array $globalRolePermission = null)
    {
        $this
            ->setGlobalRolePermission($globalRolePermission);
    }
    /**
     * Get GlobalRolePermission value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalRolePermission[]
     */
    public function getGlobalRolePermission(): ?array
    {
        return isset($this->GlobalRolePermission) ? $this->GlobalRolePermission : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalRolePermission method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalRolePermission method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalRolePermissionForArrayConstraintsFromSetGlobalRolePermission(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalRolePermissionGlobalRolePermissionItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalRolePermissionGlobalRolePermissionItem instanceof \ID3Global\Models\GlobalRolePermission) {
                $invalidValues[] = is_object($arrayOfGlobalRolePermissionGlobalRolePermissionItem) ? get_class($arrayOfGlobalRolePermissionGlobalRolePermissionItem) : sprintf('%s(%s)', gettype($arrayOfGlobalRolePermissionGlobalRolePermissionItem), var_export($arrayOfGlobalRolePermissionGlobalRolePermissionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalRolePermission property can only contain items of type \ID3Global\Models\GlobalRolePermission, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalRolePermission value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalRolePermission[] $globalRolePermission
     * @return \ID3Global\Arrays\ArrayOfGlobalRolePermission
     */
    public function setGlobalRolePermission(?array $globalRolePermission = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalRolePermissionArrayErrorMessage = self::validateGlobalRolePermissionForArrayConstraintsFromSetGlobalRolePermission($globalRolePermission))) {
            throw new InvalidArgumentException($globalRolePermissionArrayErrorMessage, __LINE__);
        }
        if (is_null($globalRolePermission) || (is_array($globalRolePermission) && empty($globalRolePermission))) {
            unset($this->GlobalRolePermission);
        } else {
            $this->GlobalRolePermission = $globalRolePermission;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalRolePermission|null
     */
    public function current(): ?\ID3Global\Models\GlobalRolePermission
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalRolePermission|null
     */
    public function item($index): ?\ID3Global\Models\GlobalRolePermission
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalRolePermission|null
     */
    public function first(): ?\ID3Global\Models\GlobalRolePermission
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalRolePermission|null
     */
    public function last(): ?\ID3Global\Models\GlobalRolePermission
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalRolePermission|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalRolePermission
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalRolePermission $item
     * @return \ID3Global\Arrays\ArrayOfGlobalRolePermission
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalRolePermission) {
            throw new InvalidArgumentException(sprintf('The GlobalRolePermission property can only contain items of type \ID3Global\Models\GlobalRolePermission, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalRolePermission
     */
    public function getAttributeName(): string
    {
        return 'GlobalRolePermission';
    }
}
