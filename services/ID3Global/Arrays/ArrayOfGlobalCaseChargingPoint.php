<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseChargingPoint Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q764:ArrayOfGlobalCaseChargingPoint
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseChargingPoint extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseChargingPoint
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseChargingPoint[]
     */
    protected ?array $GlobalCaseChargingPoint = null;
    /**
     * Constructor method for ArrayOfGlobalCaseChargingPoint
     * @uses ArrayOfGlobalCaseChargingPoint::setGlobalCaseChargingPoint()
     * @param \ID3Global\Models\GlobalCaseChargingPoint[] $globalCaseChargingPoint
     */
    public function __construct(?array $globalCaseChargingPoint = null)
    {
        $this
            ->setGlobalCaseChargingPoint($globalCaseChargingPoint);
    }
    /**
     * Get GlobalCaseChargingPoint value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseChargingPoint[]
     */
    public function getGlobalCaseChargingPoint(): ?array
    {
        return isset($this->GlobalCaseChargingPoint) ? $this->GlobalCaseChargingPoint : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseChargingPoint method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseChargingPoint method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseChargingPointForArrayConstraintsFromSetGlobalCaseChargingPoint(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseChargingPointGlobalCaseChargingPointItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseChargingPointGlobalCaseChargingPointItem instanceof \ID3Global\Models\GlobalCaseChargingPoint) {
                $invalidValues[] = is_object($arrayOfGlobalCaseChargingPointGlobalCaseChargingPointItem) ? get_class($arrayOfGlobalCaseChargingPointGlobalCaseChargingPointItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseChargingPointGlobalCaseChargingPointItem), var_export($arrayOfGlobalCaseChargingPointGlobalCaseChargingPointItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseChargingPoint property can only contain items of type \ID3Global\Models\GlobalCaseChargingPoint, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseChargingPoint value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseChargingPoint[] $globalCaseChargingPoint
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseChargingPoint
     */
    public function setGlobalCaseChargingPoint(?array $globalCaseChargingPoint = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseChargingPointArrayErrorMessage = self::validateGlobalCaseChargingPointForArrayConstraintsFromSetGlobalCaseChargingPoint($globalCaseChargingPoint))) {
            throw new InvalidArgumentException($globalCaseChargingPointArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseChargingPoint) || (is_array($globalCaseChargingPoint) && empty($globalCaseChargingPoint))) {
            unset($this->GlobalCaseChargingPoint);
        } else {
            $this->GlobalCaseChargingPoint = $globalCaseChargingPoint;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseChargingPoint|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseChargingPoint
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseChargingPoint|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseChargingPoint
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseChargingPoint|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseChargingPoint
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseChargingPoint|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseChargingPoint
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseChargingPoint|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseChargingPoint
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseChargingPoint $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseChargingPoint
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseChargingPoint) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseChargingPoint property can only contain items of type \ID3Global\Models\GlobalCaseChargingPoint, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseChargingPoint
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseChargingPoint';
    }
}
