<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalProfile Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q197:ArrayOfGlobalProfile
 * @subpackage Arrays
 */
class ArrayOfGlobalProfile extends AbstractStructArrayBase
{
    /**
     * The GlobalProfile
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfile[]
     */
    protected ?array $GlobalProfile = null;
    /**
     * Constructor method for ArrayOfGlobalProfile
     * @uses ArrayOfGlobalProfile::setGlobalProfile()
     * @param \ID3Global\Models\GlobalProfile[] $globalProfile
     */
    public function __construct(?array $globalProfile = null)
    {
        $this
            ->setGlobalProfile($globalProfile);
    }
    /**
     * Get GlobalProfile value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfile[]
     */
    public function getGlobalProfile(): ?array
    {
        return isset($this->GlobalProfile) ? $this->GlobalProfile : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalProfile method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalProfile method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalProfileForArrayConstraintsFromSetGlobalProfile(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalProfileGlobalProfileItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalProfileGlobalProfileItem instanceof \ID3Global\Models\GlobalProfile) {
                $invalidValues[] = is_object($arrayOfGlobalProfileGlobalProfileItem) ? get_class($arrayOfGlobalProfileGlobalProfileItem) : sprintf('%s(%s)', gettype($arrayOfGlobalProfileGlobalProfileItem), var_export($arrayOfGlobalProfileGlobalProfileItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalProfile property can only contain items of type \ID3Global\Models\GlobalProfile, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalProfile value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProfile[] $globalProfile
     * @return \ID3Global\Arrays\ArrayOfGlobalProfile
     */
    public function setGlobalProfile(?array $globalProfile = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalProfileArrayErrorMessage = self::validateGlobalProfileForArrayConstraintsFromSetGlobalProfile($globalProfile))) {
            throw new InvalidArgumentException($globalProfileArrayErrorMessage, __LINE__);
        }
        if (is_null($globalProfile) || (is_array($globalProfile) && empty($globalProfile))) {
            unset($this->GlobalProfile);
        } else {
            $this->GlobalProfile = $globalProfile;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalProfile|null
     */
    public function current(): ?\ID3Global\Models\GlobalProfile
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalProfile|null
     */
    public function item($index): ?\ID3Global\Models\GlobalProfile
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalProfile|null
     */
    public function first(): ?\ID3Global\Models\GlobalProfile
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalProfile|null
     */
    public function last(): ?\ID3Global\Models\GlobalProfile
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalProfile|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalProfile
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProfile $item
     * @return \ID3Global\Arrays\ArrayOfGlobalProfile
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalProfile) {
            throw new InvalidArgumentException(sprintf('The GlobalProfile property can only contain items of type \ID3Global\Models\GlobalProfile, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalProfile
     */
    public function getAttributeName(): string
    {
        return 'GlobalProfile';
    }
}
