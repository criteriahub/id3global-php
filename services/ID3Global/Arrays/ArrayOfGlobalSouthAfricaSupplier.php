<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSouthAfricaSupplier Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q99:ArrayOfGlobalSouthAfricaSupplier
 * @subpackage Arrays
 */
class ArrayOfGlobalSouthAfricaSupplier extends AbstractStructArrayBase
{
    /**
     * The GlobalSouthAfricaSupplier
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSouthAfricaSupplier[]
     */
    protected ?array $GlobalSouthAfricaSupplier = null;
    /**
     * Constructor method for ArrayOfGlobalSouthAfricaSupplier
     * @uses ArrayOfGlobalSouthAfricaSupplier::setGlobalSouthAfricaSupplier()
     * @param \ID3Global\Models\GlobalSouthAfricaSupplier[] $globalSouthAfricaSupplier
     */
    public function __construct(?array $globalSouthAfricaSupplier = null)
    {
        $this
            ->setGlobalSouthAfricaSupplier($globalSouthAfricaSupplier);
    }
    /**
     * Get GlobalSouthAfricaSupplier value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSouthAfricaSupplier[]
     */
    public function getGlobalSouthAfricaSupplier(): ?array
    {
        return isset($this->GlobalSouthAfricaSupplier) ? $this->GlobalSouthAfricaSupplier : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSouthAfricaSupplier method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSouthAfricaSupplier method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSouthAfricaSupplierForArrayConstraintsFromSetGlobalSouthAfricaSupplier(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSouthAfricaSupplierGlobalSouthAfricaSupplierItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSouthAfricaSupplierGlobalSouthAfricaSupplierItem instanceof \ID3Global\Models\GlobalSouthAfricaSupplier) {
                $invalidValues[] = is_object($arrayOfGlobalSouthAfricaSupplierGlobalSouthAfricaSupplierItem) ? get_class($arrayOfGlobalSouthAfricaSupplierGlobalSouthAfricaSupplierItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSouthAfricaSupplierGlobalSouthAfricaSupplierItem), var_export($arrayOfGlobalSouthAfricaSupplierGlobalSouthAfricaSupplierItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSouthAfricaSupplier property can only contain items of type \ID3Global\Models\GlobalSouthAfricaSupplier, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSouthAfricaSupplier value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSouthAfricaSupplier[] $globalSouthAfricaSupplier
     * @return \ID3Global\Arrays\ArrayOfGlobalSouthAfricaSupplier
     */
    public function setGlobalSouthAfricaSupplier(?array $globalSouthAfricaSupplier = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSouthAfricaSupplierArrayErrorMessage = self::validateGlobalSouthAfricaSupplierForArrayConstraintsFromSetGlobalSouthAfricaSupplier($globalSouthAfricaSupplier))) {
            throw new InvalidArgumentException($globalSouthAfricaSupplierArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSouthAfricaSupplier) || (is_array($globalSouthAfricaSupplier) && empty($globalSouthAfricaSupplier))) {
            unset($this->GlobalSouthAfricaSupplier);
        } else {
            $this->GlobalSouthAfricaSupplier = $globalSouthAfricaSupplier;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSouthAfricaSupplier|null
     */
    public function current(): ?\ID3Global\Models\GlobalSouthAfricaSupplier
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSouthAfricaSupplier|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSouthAfricaSupplier
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSouthAfricaSupplier|null
     */
    public function first(): ?\ID3Global\Models\GlobalSouthAfricaSupplier
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSouthAfricaSupplier|null
     */
    public function last(): ?\ID3Global\Models\GlobalSouthAfricaSupplier
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSouthAfricaSupplier|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSouthAfricaSupplier
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSouthAfricaSupplier $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSouthAfricaSupplier
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSouthAfricaSupplier) {
            throw new InvalidArgumentException(sprintf('The GlobalSouthAfricaSupplier property can only contain items of type \ID3Global\Models\GlobalSouthAfricaSupplier, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSouthAfricaSupplier
     */
    public function getAttributeName(): string
    {
        return 'GlobalSouthAfricaSupplier';
    }
}
