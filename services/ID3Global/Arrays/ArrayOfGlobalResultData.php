<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalResultData Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q450:ArrayOfGlobalResultData
 * @subpackage Arrays
 */
class ArrayOfGlobalResultData extends AbstractStructArrayBase
{
    /**
     * The GlobalResultData
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalResultData[]
     */
    protected ?array $GlobalResultData = null;
    /**
     * Constructor method for ArrayOfGlobalResultData
     * @uses ArrayOfGlobalResultData::setGlobalResultData()
     * @param \ID3Global\Models\GlobalResultData[] $globalResultData
     */
    public function __construct(?array $globalResultData = null)
    {
        $this
            ->setGlobalResultData($globalResultData);
    }
    /**
     * Get GlobalResultData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalResultData[]
     */
    public function getGlobalResultData(): ?array
    {
        return isset($this->GlobalResultData) ? $this->GlobalResultData : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalResultData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalResultData method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalResultDataForArrayConstraintsFromSetGlobalResultData(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalResultDataGlobalResultDataItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalResultDataGlobalResultDataItem instanceof \ID3Global\Models\GlobalResultData) {
                $invalidValues[] = is_object($arrayOfGlobalResultDataGlobalResultDataItem) ? get_class($arrayOfGlobalResultDataGlobalResultDataItem) : sprintf('%s(%s)', gettype($arrayOfGlobalResultDataGlobalResultDataItem), var_export($arrayOfGlobalResultDataGlobalResultDataItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalResultData property can only contain items of type \ID3Global\Models\GlobalResultData, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalResultData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalResultData[] $globalResultData
     * @return \ID3Global\Arrays\ArrayOfGlobalResultData
     */
    public function setGlobalResultData(?array $globalResultData = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalResultDataArrayErrorMessage = self::validateGlobalResultDataForArrayConstraintsFromSetGlobalResultData($globalResultData))) {
            throw new InvalidArgumentException($globalResultDataArrayErrorMessage, __LINE__);
        }
        if (is_null($globalResultData) || (is_array($globalResultData) && empty($globalResultData))) {
            unset($this->GlobalResultData);
        } else {
            $this->GlobalResultData = $globalResultData;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalResultData|null
     */
    public function current(): ?\ID3Global\Models\GlobalResultData
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalResultData|null
     */
    public function item($index): ?\ID3Global\Models\GlobalResultData
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalResultData|null
     */
    public function first(): ?\ID3Global\Models\GlobalResultData
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalResultData|null
     */
    public function last(): ?\ID3Global\Models\GlobalResultData
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalResultData|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalResultData
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalResultData $item
     * @return \ID3Global\Arrays\ArrayOfGlobalResultData
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalResultData) {
            throw new InvalidArgumentException(sprintf('The GlobalResultData property can only contain items of type \ID3Global\Models\GlobalResultData, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalResultData
     */
    public function getAttributeName(): string
    {
        return 'GlobalResultData';
    }
}
