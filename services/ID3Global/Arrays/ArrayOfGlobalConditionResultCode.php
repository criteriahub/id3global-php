<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalConditionResultCode Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q224:ArrayOfGlobalConditionResultCode
 * @subpackage Arrays
 */
class ArrayOfGlobalConditionResultCode extends AbstractStructArrayBase
{
    /**
     * The GlobalConditionResultCode
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalConditionResultCode[]
     */
    protected ?array $GlobalConditionResultCode = null;
    /**
     * Constructor method for ArrayOfGlobalConditionResultCode
     * @uses ArrayOfGlobalConditionResultCode::setGlobalConditionResultCode()
     * @param \ID3Global\Models\GlobalConditionResultCode[] $globalConditionResultCode
     */
    public function __construct(?array $globalConditionResultCode = null)
    {
        $this
            ->setGlobalConditionResultCode($globalConditionResultCode);
    }
    /**
     * Get GlobalConditionResultCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalConditionResultCode[]
     */
    public function getGlobalConditionResultCode(): ?array
    {
        return isset($this->GlobalConditionResultCode) ? $this->GlobalConditionResultCode : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalConditionResultCode method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalConditionResultCode method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalConditionResultCodeForArrayConstraintsFromSetGlobalConditionResultCode(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalConditionResultCodeGlobalConditionResultCodeItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalConditionResultCodeGlobalConditionResultCodeItem instanceof \ID3Global\Models\GlobalConditionResultCode) {
                $invalidValues[] = is_object($arrayOfGlobalConditionResultCodeGlobalConditionResultCodeItem) ? get_class($arrayOfGlobalConditionResultCodeGlobalConditionResultCodeItem) : sprintf('%s(%s)', gettype($arrayOfGlobalConditionResultCodeGlobalConditionResultCodeItem), var_export($arrayOfGlobalConditionResultCodeGlobalConditionResultCodeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalConditionResultCode property can only contain items of type \ID3Global\Models\GlobalConditionResultCode, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalConditionResultCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalConditionResultCode[] $globalConditionResultCode
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionResultCode
     */
    public function setGlobalConditionResultCode(?array $globalConditionResultCode = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalConditionResultCodeArrayErrorMessage = self::validateGlobalConditionResultCodeForArrayConstraintsFromSetGlobalConditionResultCode($globalConditionResultCode))) {
            throw new InvalidArgumentException($globalConditionResultCodeArrayErrorMessage, __LINE__);
        }
        if (is_null($globalConditionResultCode) || (is_array($globalConditionResultCode) && empty($globalConditionResultCode))) {
            unset($this->GlobalConditionResultCode);
        } else {
            $this->GlobalConditionResultCode = $globalConditionResultCode;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalConditionResultCode|null
     */
    public function current(): ?\ID3Global\Models\GlobalConditionResultCode
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalConditionResultCode|null
     */
    public function item($index): ?\ID3Global\Models\GlobalConditionResultCode
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalConditionResultCode|null
     */
    public function first(): ?\ID3Global\Models\GlobalConditionResultCode
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalConditionResultCode|null
     */
    public function last(): ?\ID3Global\Models\GlobalConditionResultCode
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalConditionResultCode|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalConditionResultCode
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalConditionResultCode $item
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionResultCode
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalConditionResultCode) {
            throw new InvalidArgumentException(sprintf('The GlobalConditionResultCode property can only contain items of type \ID3Global\Models\GlobalConditionResultCode, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalConditionResultCode
     */
    public function getAttributeName(): string
    {
        return 'GlobalConditionResultCode';
    }
}
