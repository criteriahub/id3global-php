<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalOverride Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q454:ArrayOfGlobalOverride
 * @subpackage Arrays
 */
class ArrayOfGlobalOverride extends AbstractStructArrayBase
{
    /**
     * The GlobalOverride
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalOverride[]
     */
    protected ?array $GlobalOverride = null;
    /**
     * Constructor method for ArrayOfGlobalOverride
     * @uses ArrayOfGlobalOverride::setGlobalOverride()
     * @param \ID3Global\Models\GlobalOverride[] $globalOverride
     */
    public function __construct(?array $globalOverride = null)
    {
        $this
            ->setGlobalOverride($globalOverride);
    }
    /**
     * Get GlobalOverride value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalOverride[]
     */
    public function getGlobalOverride(): ?array
    {
        return isset($this->GlobalOverride) ? $this->GlobalOverride : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalOverride method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalOverride method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalOverrideForArrayConstraintsFromSetGlobalOverride(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalOverrideGlobalOverrideItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalOverrideGlobalOverrideItem instanceof \ID3Global\Models\GlobalOverride) {
                $invalidValues[] = is_object($arrayOfGlobalOverrideGlobalOverrideItem) ? get_class($arrayOfGlobalOverrideGlobalOverrideItem) : sprintf('%s(%s)', gettype($arrayOfGlobalOverrideGlobalOverrideItem), var_export($arrayOfGlobalOverrideGlobalOverrideItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalOverride property can only contain items of type \ID3Global\Models\GlobalOverride, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalOverride value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalOverride[] $globalOverride
     * @return \ID3Global\Arrays\ArrayOfGlobalOverride
     */
    public function setGlobalOverride(?array $globalOverride = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalOverrideArrayErrorMessage = self::validateGlobalOverrideForArrayConstraintsFromSetGlobalOverride($globalOverride))) {
            throw new InvalidArgumentException($globalOverrideArrayErrorMessage, __LINE__);
        }
        if (is_null($globalOverride) || (is_array($globalOverride) && empty($globalOverride))) {
            unset($this->GlobalOverride);
        } else {
            $this->GlobalOverride = $globalOverride;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalOverride|null
     */
    public function current(): ?\ID3Global\Models\GlobalOverride
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalOverride|null
     */
    public function item($index): ?\ID3Global\Models\GlobalOverride
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalOverride|null
     */
    public function first(): ?\ID3Global\Models\GlobalOverride
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalOverride|null
     */
    public function last(): ?\ID3Global\Models\GlobalOverride
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalOverride|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalOverride
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalOverride $item
     * @return \ID3Global\Arrays\ArrayOfGlobalOverride
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalOverride) {
            throw new InvalidArgumentException(sprintf('The GlobalOverride property can only contain items of type \ID3Global\Models\GlobalOverride, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalOverride
     */
    public function getAttributeName(): string
    {
        return 'GlobalOverride';
    }
}
