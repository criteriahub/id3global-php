<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfDocumentForgeryTests Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q139:ArrayOfDocumentForgeryTests
 * @subpackage Arrays
 */
class ArrayOfDocumentForgeryTests extends AbstractStructArrayBase
{
    /**
     * The DocumentForgeryTests
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\DocumentForgeryTests[]
     */
    protected ?array $DocumentForgeryTests = null;
    /**
     * Constructor method for ArrayOfDocumentForgeryTests
     * @uses ArrayOfDocumentForgeryTests::setDocumentForgeryTests()
     * @param \ID3Global\Models\DocumentForgeryTests[] $documentForgeryTests
     */
    public function __construct(?array $documentForgeryTests = null)
    {
        $this
            ->setDocumentForgeryTests($documentForgeryTests);
    }
    /**
     * Get DocumentForgeryTests value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\DocumentForgeryTests[]
     */
    public function getDocumentForgeryTests(): ?array
    {
        return isset($this->DocumentForgeryTests) ? $this->DocumentForgeryTests : null;
    }
    /**
     * This method is responsible for validating the values passed to the setDocumentForgeryTests method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDocumentForgeryTests method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDocumentForgeryTestsForArrayConstraintsFromSetDocumentForgeryTests(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfDocumentForgeryTestsDocumentForgeryTestsItem) {
            // validation for constraint: itemType
            if (!$arrayOfDocumentForgeryTestsDocumentForgeryTestsItem instanceof \ID3Global\Models\DocumentForgeryTests) {
                $invalidValues[] = is_object($arrayOfDocumentForgeryTestsDocumentForgeryTestsItem) ? get_class($arrayOfDocumentForgeryTestsDocumentForgeryTestsItem) : sprintf('%s(%s)', gettype($arrayOfDocumentForgeryTestsDocumentForgeryTestsItem), var_export($arrayOfDocumentForgeryTestsDocumentForgeryTestsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The DocumentForgeryTests property can only contain items of type \ID3Global\Models\DocumentForgeryTests, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set DocumentForgeryTests value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\DocumentForgeryTests[] $documentForgeryTests
     * @return \ID3Global\Arrays\ArrayOfDocumentForgeryTests
     */
    public function setDocumentForgeryTests(?array $documentForgeryTests = null): self
    {
        // validation for constraint: array
        if ('' !== ($documentForgeryTestsArrayErrorMessage = self::validateDocumentForgeryTestsForArrayConstraintsFromSetDocumentForgeryTests($documentForgeryTests))) {
            throw new InvalidArgumentException($documentForgeryTestsArrayErrorMessage, __LINE__);
        }
        if (is_null($documentForgeryTests) || (is_array($documentForgeryTests) && empty($documentForgeryTests))) {
            unset($this->DocumentForgeryTests);
        } else {
            $this->DocumentForgeryTests = $documentForgeryTests;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\DocumentForgeryTests|null
     */
    public function current(): ?\ID3Global\Models\DocumentForgeryTests
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\DocumentForgeryTests|null
     */
    public function item($index): ?\ID3Global\Models\DocumentForgeryTests
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\DocumentForgeryTests|null
     */
    public function first(): ?\ID3Global\Models\DocumentForgeryTests
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\DocumentForgeryTests|null
     */
    public function last(): ?\ID3Global\Models\DocumentForgeryTests
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\DocumentForgeryTests|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\DocumentForgeryTests
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\DocumentForgeryTests $item
     * @return \ID3Global\Arrays\ArrayOfDocumentForgeryTests
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\DocumentForgeryTests) {
            throw new InvalidArgumentException(sprintf('The DocumentForgeryTests property can only contain items of type \ID3Global\Models\DocumentForgeryTests, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string DocumentForgeryTests
     */
    public function getAttributeName(): string
    {
        return 'DocumentForgeryTests';
    }
}
