<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseVerdict Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q761:ArrayOfGlobalCaseVerdict
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseVerdict extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseVerdict
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseVerdict[]
     */
    protected ?array $GlobalCaseVerdict = null;
    /**
     * Constructor method for ArrayOfGlobalCaseVerdict
     * @uses ArrayOfGlobalCaseVerdict::setGlobalCaseVerdict()
     * @param \ID3Global\Models\GlobalCaseVerdict[] $globalCaseVerdict
     */
    public function __construct(?array $globalCaseVerdict = null)
    {
        $this
            ->setGlobalCaseVerdict($globalCaseVerdict);
    }
    /**
     * Get GlobalCaseVerdict value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseVerdict[]
     */
    public function getGlobalCaseVerdict(): ?array
    {
        return isset($this->GlobalCaseVerdict) ? $this->GlobalCaseVerdict : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseVerdict method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseVerdict method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseVerdictForArrayConstraintsFromSetGlobalCaseVerdict(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseVerdictGlobalCaseVerdictItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseVerdictGlobalCaseVerdictItem instanceof \ID3Global\Models\GlobalCaseVerdict) {
                $invalidValues[] = is_object($arrayOfGlobalCaseVerdictGlobalCaseVerdictItem) ? get_class($arrayOfGlobalCaseVerdictGlobalCaseVerdictItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseVerdictGlobalCaseVerdictItem), var_export($arrayOfGlobalCaseVerdictGlobalCaseVerdictItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseVerdict property can only contain items of type \ID3Global\Models\GlobalCaseVerdict, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseVerdict value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseVerdict[] $globalCaseVerdict
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseVerdict
     */
    public function setGlobalCaseVerdict(?array $globalCaseVerdict = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseVerdictArrayErrorMessage = self::validateGlobalCaseVerdictForArrayConstraintsFromSetGlobalCaseVerdict($globalCaseVerdict))) {
            throw new InvalidArgumentException($globalCaseVerdictArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseVerdict) || (is_array($globalCaseVerdict) && empty($globalCaseVerdict))) {
            unset($this->GlobalCaseVerdict);
        } else {
            $this->GlobalCaseVerdict = $globalCaseVerdict;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseVerdict|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseVerdict
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseVerdict|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseVerdict
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseVerdict|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseVerdict
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseVerdict|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseVerdict
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseVerdict|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseVerdict
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseVerdict $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseVerdict
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseVerdict) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseVerdict property can only contain items of type \ID3Global\Models\GlobalCaseVerdict, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseVerdict
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseVerdict';
    }
}
