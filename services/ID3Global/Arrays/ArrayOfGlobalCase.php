<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCase Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q915:ArrayOfGlobalCase
 * @subpackage Arrays
 */
class ArrayOfGlobalCase extends AbstractStructArrayBase
{
    /**
     * The GlobalCase
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCase[]
     */
    protected ?array $GlobalCase = null;
    /**
     * Constructor method for ArrayOfGlobalCase
     * @uses ArrayOfGlobalCase::setGlobalCase()
     * @param \ID3Global\Models\GlobalCase[] $globalCase
     */
    public function __construct(?array $globalCase = null)
    {
        $this
            ->setGlobalCase($globalCase);
    }
    /**
     * Get GlobalCase value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCase[]
     */
    public function getGlobalCase(): ?array
    {
        return isset($this->GlobalCase) ? $this->GlobalCase : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCase method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCase method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseForArrayConstraintsFromSetGlobalCase(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseGlobalCaseItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseGlobalCaseItem instanceof \ID3Global\Models\GlobalCase) {
                $invalidValues[] = is_object($arrayOfGlobalCaseGlobalCaseItem) ? get_class($arrayOfGlobalCaseGlobalCaseItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseGlobalCaseItem), var_export($arrayOfGlobalCaseGlobalCaseItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCase property can only contain items of type \ID3Global\Models\GlobalCase, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCase value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCase[] $globalCase
     * @return \ID3Global\Arrays\ArrayOfGlobalCase
     */
    public function setGlobalCase(?array $globalCase = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseArrayErrorMessage = self::validateGlobalCaseForArrayConstraintsFromSetGlobalCase($globalCase))) {
            throw new InvalidArgumentException($globalCaseArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCase) || (is_array($globalCase) && empty($globalCase))) {
            unset($this->GlobalCase);
        } else {
            $this->GlobalCase = $globalCase;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCase|null
     */
    public function current(): ?\ID3Global\Models\GlobalCase
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCase|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCase
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCase|null
     */
    public function first(): ?\ID3Global\Models\GlobalCase
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCase|null
     */
    public function last(): ?\ID3Global\Models\GlobalCase
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCase|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCase
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCase $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCase
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCase) {
            throw new InvalidArgumentException(sprintf('The GlobalCase property can only contain items of type \ID3Global\Models\GlobalCase, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCase
     */
    public function getAttributeName(): string
    {
        return 'GlobalCase';
    }
}
