<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalConditionDecision Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q230:ArrayOfGlobalConditionDecision
 * @subpackage Arrays
 */
class ArrayOfGlobalConditionDecision extends AbstractStructArrayBase
{
    /**
     * The GlobalConditionDecision
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalConditionDecision[]
     */
    protected ?array $GlobalConditionDecision = null;
    /**
     * Constructor method for ArrayOfGlobalConditionDecision
     * @uses ArrayOfGlobalConditionDecision::setGlobalConditionDecision()
     * @param \ID3Global\Models\GlobalConditionDecision[] $globalConditionDecision
     */
    public function __construct(?array $globalConditionDecision = null)
    {
        $this
            ->setGlobalConditionDecision($globalConditionDecision);
    }
    /**
     * Get GlobalConditionDecision value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalConditionDecision[]
     */
    public function getGlobalConditionDecision(): ?array
    {
        return isset($this->GlobalConditionDecision) ? $this->GlobalConditionDecision : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalConditionDecision method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalConditionDecision method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalConditionDecisionForArrayConstraintsFromSetGlobalConditionDecision(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalConditionDecisionGlobalConditionDecisionItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalConditionDecisionGlobalConditionDecisionItem instanceof \ID3Global\Models\GlobalConditionDecision) {
                $invalidValues[] = is_object($arrayOfGlobalConditionDecisionGlobalConditionDecisionItem) ? get_class($arrayOfGlobalConditionDecisionGlobalConditionDecisionItem) : sprintf('%s(%s)', gettype($arrayOfGlobalConditionDecisionGlobalConditionDecisionItem), var_export($arrayOfGlobalConditionDecisionGlobalConditionDecisionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalConditionDecision property can only contain items of type \ID3Global\Models\GlobalConditionDecision, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalConditionDecision value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalConditionDecision[] $globalConditionDecision
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionDecision
     */
    public function setGlobalConditionDecision(?array $globalConditionDecision = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalConditionDecisionArrayErrorMessage = self::validateGlobalConditionDecisionForArrayConstraintsFromSetGlobalConditionDecision($globalConditionDecision))) {
            throw new InvalidArgumentException($globalConditionDecisionArrayErrorMessage, __LINE__);
        }
        if (is_null($globalConditionDecision) || (is_array($globalConditionDecision) && empty($globalConditionDecision))) {
            unset($this->GlobalConditionDecision);
        } else {
            $this->GlobalConditionDecision = $globalConditionDecision;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalConditionDecision|null
     */
    public function current(): ?\ID3Global\Models\GlobalConditionDecision
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalConditionDecision|null
     */
    public function item($index): ?\ID3Global\Models\GlobalConditionDecision
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalConditionDecision|null
     */
    public function first(): ?\ID3Global\Models\GlobalConditionDecision
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalConditionDecision|null
     */
    public function last(): ?\ID3Global\Models\GlobalConditionDecision
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalConditionDecision|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalConditionDecision
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalConditionDecision $item
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionDecision
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalConditionDecision) {
            throw new InvalidArgumentException(sprintf('The GlobalConditionDecision property can only contain items of type \ID3Global\Models\GlobalConditionDecision, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalConditionDecision
     */
    public function getAttributeName(): string
    {
        return 'GlobalConditionDecision';
    }
}
