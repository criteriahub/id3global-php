<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalReportDetails Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q518:ArrayOfGlobalReportDetails
 * @subpackage Arrays
 */
class ArrayOfGlobalReportDetails extends AbstractStructArrayBase
{
    /**
     * The GlobalReportDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalReportDetails[]
     */
    protected ?array $GlobalReportDetails = null;
    /**
     * Constructor method for ArrayOfGlobalReportDetails
     * @uses ArrayOfGlobalReportDetails::setGlobalReportDetails()
     * @param \ID3Global\Models\GlobalReportDetails[] $globalReportDetails
     */
    public function __construct(?array $globalReportDetails = null)
    {
        $this
            ->setGlobalReportDetails($globalReportDetails);
    }
    /**
     * Get GlobalReportDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalReportDetails[]
     */
    public function getGlobalReportDetails(): ?array
    {
        return isset($this->GlobalReportDetails) ? $this->GlobalReportDetails : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalReportDetails method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalReportDetails method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalReportDetailsForArrayConstraintsFromSetGlobalReportDetails(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalReportDetailsGlobalReportDetailsItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalReportDetailsGlobalReportDetailsItem instanceof \ID3Global\Models\GlobalReportDetails) {
                $invalidValues[] = is_object($arrayOfGlobalReportDetailsGlobalReportDetailsItem) ? get_class($arrayOfGlobalReportDetailsGlobalReportDetailsItem) : sprintf('%s(%s)', gettype($arrayOfGlobalReportDetailsGlobalReportDetailsItem), var_export($arrayOfGlobalReportDetailsGlobalReportDetailsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalReportDetails property can only contain items of type \ID3Global\Models\GlobalReportDetails, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalReportDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalReportDetails[] $globalReportDetails
     * @return \ID3Global\Arrays\ArrayOfGlobalReportDetails
     */
    public function setGlobalReportDetails(?array $globalReportDetails = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalReportDetailsArrayErrorMessage = self::validateGlobalReportDetailsForArrayConstraintsFromSetGlobalReportDetails($globalReportDetails))) {
            throw new InvalidArgumentException($globalReportDetailsArrayErrorMessage, __LINE__);
        }
        if (is_null($globalReportDetails) || (is_array($globalReportDetails) && empty($globalReportDetails))) {
            unset($this->GlobalReportDetails);
        } else {
            $this->GlobalReportDetails = $globalReportDetails;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalReportDetails|null
     */
    public function current(): ?\ID3Global\Models\GlobalReportDetails
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalReportDetails|null
     */
    public function item($index): ?\ID3Global\Models\GlobalReportDetails
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalReportDetails|null
     */
    public function first(): ?\ID3Global\Models\GlobalReportDetails
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalReportDetails|null
     */
    public function last(): ?\ID3Global\Models\GlobalReportDetails
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalReportDetails|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalReportDetails
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalReportDetails $item
     * @return \ID3Global\Arrays\ArrayOfGlobalReportDetails
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalReportDetails) {
            throw new InvalidArgumentException(sprintf('The GlobalReportDetails property can only contain items of type \ID3Global\Models\GlobalReportDetails, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalReportDetails
     */
    public function getAttributeName(): string
    {
        return 'GlobalReportDetails';
    }
}
