<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCountry Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q14:ArrayOfGlobalCountry
 * @subpackage Arrays
 */
class ArrayOfGlobalCountry extends AbstractStructArrayBase
{
    /**
     * The GlobalCountry
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCountry[]
     */
    protected ?array $GlobalCountry = null;
    /**
     * Constructor method for ArrayOfGlobalCountry
     * @uses ArrayOfGlobalCountry::setGlobalCountry()
     * @param \ID3Global\Models\GlobalCountry[] $globalCountry
     */
    public function __construct(?array $globalCountry = null)
    {
        $this
            ->setGlobalCountry($globalCountry);
    }
    /**
     * Get GlobalCountry value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCountry[]
     */
    public function getGlobalCountry(): ?array
    {
        return isset($this->GlobalCountry) ? $this->GlobalCountry : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCountry method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCountry method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCountryForArrayConstraintsFromSetGlobalCountry(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCountryGlobalCountryItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCountryGlobalCountryItem instanceof \ID3Global\Models\GlobalCountry) {
                $invalidValues[] = is_object($arrayOfGlobalCountryGlobalCountryItem) ? get_class($arrayOfGlobalCountryGlobalCountryItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCountryGlobalCountryItem), var_export($arrayOfGlobalCountryGlobalCountryItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCountry property can only contain items of type \ID3Global\Models\GlobalCountry, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCountry value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCountry[] $globalCountry
     * @return \ID3Global\Arrays\ArrayOfGlobalCountry
     */
    public function setGlobalCountry(?array $globalCountry = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCountryArrayErrorMessage = self::validateGlobalCountryForArrayConstraintsFromSetGlobalCountry($globalCountry))) {
            throw new InvalidArgumentException($globalCountryArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCountry) || (is_array($globalCountry) && empty($globalCountry))) {
            unset($this->GlobalCountry);
        } else {
            $this->GlobalCountry = $globalCountry;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCountry|null
     */
    public function current(): ?\ID3Global\Models\GlobalCountry
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCountry|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCountry
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCountry|null
     */
    public function first(): ?\ID3Global\Models\GlobalCountry
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCountry|null
     */
    public function last(): ?\ID3Global\Models\GlobalCountry
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCountry|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCountry
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCountry $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCountry
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCountry) {
            throw new InvalidArgumentException(sprintf('The GlobalCountry property can only contain items of type \ID3Global\Models\GlobalCountry, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCountry
     */
    public function getAttributeName(): string
    {
        return 'GlobalCountry';
    }
}
