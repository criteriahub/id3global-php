<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalMatrixResultField Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q817:ArrayOfGlobalMatrixResultField
 * @subpackage Arrays
 */
class ArrayOfGlobalMatrixResultField extends AbstractStructArrayBase
{
    /**
     * The GlobalMatrixResultField
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalMatrixResultField[]
     */
    protected ?array $GlobalMatrixResultField = null;
    /**
     * Constructor method for ArrayOfGlobalMatrixResultField
     * @uses ArrayOfGlobalMatrixResultField::setGlobalMatrixResultField()
     * @param \ID3Global\Models\GlobalMatrixResultField[] $globalMatrixResultField
     */
    public function __construct(?array $globalMatrixResultField = null)
    {
        $this
            ->setGlobalMatrixResultField($globalMatrixResultField);
    }
    /**
     * Get GlobalMatrixResultField value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalMatrixResultField[]
     */
    public function getGlobalMatrixResultField(): ?array
    {
        return isset($this->GlobalMatrixResultField) ? $this->GlobalMatrixResultField : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalMatrixResultField method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalMatrixResultField method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalMatrixResultFieldForArrayConstraintsFromSetGlobalMatrixResultField(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalMatrixResultFieldGlobalMatrixResultFieldItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalMatrixResultFieldGlobalMatrixResultFieldItem instanceof \ID3Global\Models\GlobalMatrixResultField) {
                $invalidValues[] = is_object($arrayOfGlobalMatrixResultFieldGlobalMatrixResultFieldItem) ? get_class($arrayOfGlobalMatrixResultFieldGlobalMatrixResultFieldItem) : sprintf('%s(%s)', gettype($arrayOfGlobalMatrixResultFieldGlobalMatrixResultFieldItem), var_export($arrayOfGlobalMatrixResultFieldGlobalMatrixResultFieldItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalMatrixResultField property can only contain items of type \ID3Global\Models\GlobalMatrixResultField, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalMatrixResultField value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalMatrixResultField[] $globalMatrixResultField
     * @return \ID3Global\Arrays\ArrayOfGlobalMatrixResultField
     */
    public function setGlobalMatrixResultField(?array $globalMatrixResultField = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalMatrixResultFieldArrayErrorMessage = self::validateGlobalMatrixResultFieldForArrayConstraintsFromSetGlobalMatrixResultField($globalMatrixResultField))) {
            throw new InvalidArgumentException($globalMatrixResultFieldArrayErrorMessage, __LINE__);
        }
        if (is_null($globalMatrixResultField) || (is_array($globalMatrixResultField) && empty($globalMatrixResultField))) {
            unset($this->GlobalMatrixResultField);
        } else {
            $this->GlobalMatrixResultField = $globalMatrixResultField;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalMatrixResultField|null
     */
    public function current(): ?\ID3Global\Models\GlobalMatrixResultField
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalMatrixResultField|null
     */
    public function item($index): ?\ID3Global\Models\GlobalMatrixResultField
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalMatrixResultField|null
     */
    public function first(): ?\ID3Global\Models\GlobalMatrixResultField
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalMatrixResultField|null
     */
    public function last(): ?\ID3Global\Models\GlobalMatrixResultField
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalMatrixResultField|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalMatrixResultField
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalMatrixResultField $item
     * @return \ID3Global\Arrays\ArrayOfGlobalMatrixResultField
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalMatrixResultField) {
            throw new InvalidArgumentException(sprintf('The GlobalMatrixResultField property can only contain items of type \ID3Global\Models\GlobalMatrixResultField, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalMatrixResultField
     */
    public function getAttributeName(): string
    {
        return 'GlobalMatrixResultField';
    }
}
