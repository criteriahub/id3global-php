<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalStatus Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfGlobalStatus
 * @subpackage Arrays
 */
class ArrayOfGlobalStatus extends AbstractStructArrayBase
{
    /**
     * The GlobalStatus
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalStatus[]
     */
    protected ?array $GlobalStatus = null;
    /**
     * Constructor method for ArrayOfGlobalStatus
     * @uses ArrayOfGlobalStatus::setGlobalStatus()
     * @param \ID3Global\Models\GlobalStatus[] $globalStatus
     */
    public function __construct(?array $globalStatus = null)
    {
        $this
            ->setGlobalStatus($globalStatus);
    }
    /**
     * Get GlobalStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalStatus[]
     */
    public function getGlobalStatus(): ?array
    {
        return isset($this->GlobalStatus) ? $this->GlobalStatus : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalStatus method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalStatus method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalStatusForArrayConstraintsFromSetGlobalStatus(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalStatusGlobalStatusItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalStatusGlobalStatusItem instanceof \ID3Global\Models\GlobalStatus) {
                $invalidValues[] = is_object($arrayOfGlobalStatusGlobalStatusItem) ? get_class($arrayOfGlobalStatusGlobalStatusItem) : sprintf('%s(%s)', gettype($arrayOfGlobalStatusGlobalStatusItem), var_export($arrayOfGlobalStatusGlobalStatusItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalStatus property can only contain items of type \ID3Global\Models\GlobalStatus, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalStatus[] $globalStatus
     * @return \ID3Global\Arrays\ArrayOfGlobalStatus
     */
    public function setGlobalStatus(?array $globalStatus = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalStatusArrayErrorMessage = self::validateGlobalStatusForArrayConstraintsFromSetGlobalStatus($globalStatus))) {
            throw new InvalidArgumentException($globalStatusArrayErrorMessage, __LINE__);
        }
        if (is_null($globalStatus) || (is_array($globalStatus) && empty($globalStatus))) {
            unset($this->GlobalStatus);
        } else {
            $this->GlobalStatus = $globalStatus;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalStatus|null
     */
    public function current(): ?\ID3Global\Models\GlobalStatus
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalStatus|null
     */
    public function item($index): ?\ID3Global\Models\GlobalStatus
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalStatus|null
     */
    public function first(): ?\ID3Global\Models\GlobalStatus
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalStatus|null
     */
    public function last(): ?\ID3Global\Models\GlobalStatus
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalStatus|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalStatus
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalStatus $item
     * @return \ID3Global\Arrays\ArrayOfGlobalStatus
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalStatus) {
            throw new InvalidArgumentException(sprintf('The GlobalStatus property can only contain items of type \ID3Global\Models\GlobalStatus, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalStatus
     */
    public function getAttributeName(): string
    {
        return 'GlobalStatus';
    }
}
