<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseConsent Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q849:ArrayOfGlobalCaseConsent
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseConsent extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseConsent
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseConsent[]
     */
    protected ?array $GlobalCaseConsent = null;
    /**
     * Constructor method for ArrayOfGlobalCaseConsent
     * @uses ArrayOfGlobalCaseConsent::setGlobalCaseConsent()
     * @param \ID3Global\Models\GlobalCaseConsent[] $globalCaseConsent
     */
    public function __construct(?array $globalCaseConsent = null)
    {
        $this
            ->setGlobalCaseConsent($globalCaseConsent);
    }
    /**
     * Get GlobalCaseConsent value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseConsent[]
     */
    public function getGlobalCaseConsent(): ?array
    {
        return isset($this->GlobalCaseConsent) ? $this->GlobalCaseConsent : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseConsent method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseConsent method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseConsentForArrayConstraintsFromSetGlobalCaseConsent(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseConsentGlobalCaseConsentItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseConsentGlobalCaseConsentItem instanceof \ID3Global\Models\GlobalCaseConsent) {
                $invalidValues[] = is_object($arrayOfGlobalCaseConsentGlobalCaseConsentItem) ? get_class($arrayOfGlobalCaseConsentGlobalCaseConsentItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseConsentGlobalCaseConsentItem), var_export($arrayOfGlobalCaseConsentGlobalCaseConsentItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseConsent property can only contain items of type \ID3Global\Models\GlobalCaseConsent, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseConsent value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseConsent[] $globalCaseConsent
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseConsent
     */
    public function setGlobalCaseConsent(?array $globalCaseConsent = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseConsentArrayErrorMessage = self::validateGlobalCaseConsentForArrayConstraintsFromSetGlobalCaseConsent($globalCaseConsent))) {
            throw new InvalidArgumentException($globalCaseConsentArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseConsent) || (is_array($globalCaseConsent) && empty($globalCaseConsent))) {
            unset($this->GlobalCaseConsent);
        } else {
            $this->GlobalCaseConsent = $globalCaseConsent;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseConsent|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseConsent
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseConsent|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseConsent
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseConsent|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseConsent
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseConsent|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseConsent
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseConsent|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseConsent
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseConsent $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseConsent
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseConsent) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseConsent property can only contain items of type \ID3Global\Models\GlobalCaseConsent, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseConsent
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseConsent';
    }
}
