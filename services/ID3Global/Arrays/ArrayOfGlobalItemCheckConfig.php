<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalItemCheckConfig Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q280:ArrayOfGlobalItemCheckConfig
 * @subpackage Arrays
 */
class ArrayOfGlobalItemCheckConfig extends AbstractStructArrayBase
{
    /**
     * The GlobalItemCheckConfig
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalItemCheckConfig[]
     */
    protected ?array $GlobalItemCheckConfig = null;
    /**
     * Constructor method for ArrayOfGlobalItemCheckConfig
     * @uses ArrayOfGlobalItemCheckConfig::setGlobalItemCheckConfig()
     * @param \ID3Global\Models\GlobalItemCheckConfig[] $globalItemCheckConfig
     */
    public function __construct(?array $globalItemCheckConfig = null)
    {
        $this
            ->setGlobalItemCheckConfig($globalItemCheckConfig);
    }
    /**
     * Get GlobalItemCheckConfig value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalItemCheckConfig[]
     */
    public function getGlobalItemCheckConfig(): ?array
    {
        return isset($this->GlobalItemCheckConfig) ? $this->GlobalItemCheckConfig : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalItemCheckConfig method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalItemCheckConfig method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalItemCheckConfigForArrayConstraintsFromSetGlobalItemCheckConfig(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalItemCheckConfigGlobalItemCheckConfigItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalItemCheckConfigGlobalItemCheckConfigItem instanceof \ID3Global\Models\GlobalItemCheckConfig) {
                $invalidValues[] = is_object($arrayOfGlobalItemCheckConfigGlobalItemCheckConfigItem) ? get_class($arrayOfGlobalItemCheckConfigGlobalItemCheckConfigItem) : sprintf('%s(%s)', gettype($arrayOfGlobalItemCheckConfigGlobalItemCheckConfigItem), var_export($arrayOfGlobalItemCheckConfigGlobalItemCheckConfigItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalItemCheckConfig property can only contain items of type \ID3Global\Models\GlobalItemCheckConfig, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalItemCheckConfig value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckConfig[] $globalItemCheckConfig
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckConfig
     */
    public function setGlobalItemCheckConfig(?array $globalItemCheckConfig = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalItemCheckConfigArrayErrorMessage = self::validateGlobalItemCheckConfigForArrayConstraintsFromSetGlobalItemCheckConfig($globalItemCheckConfig))) {
            throw new InvalidArgumentException($globalItemCheckConfigArrayErrorMessage, __LINE__);
        }
        if (is_null($globalItemCheckConfig) || (is_array($globalItemCheckConfig) && empty($globalItemCheckConfig))) {
            unset($this->GlobalItemCheckConfig);
        } else {
            $this->GlobalItemCheckConfig = $globalItemCheckConfig;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalItemCheckConfig|null
     */
    public function current(): ?\ID3Global\Models\GlobalItemCheckConfig
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalItemCheckConfig|null
     */
    public function item($index): ?\ID3Global\Models\GlobalItemCheckConfig
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalItemCheckConfig|null
     */
    public function first(): ?\ID3Global\Models\GlobalItemCheckConfig
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalItemCheckConfig|null
     */
    public function last(): ?\ID3Global\Models\GlobalItemCheckConfig
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalItemCheckConfig|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalItemCheckConfig
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckConfig $item
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckConfig
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalItemCheckConfig) {
            throw new InvalidArgumentException(sprintf('The GlobalItemCheckConfig property can only contain items of type \ID3Global\Models\GlobalItemCheckConfig, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalItemCheckConfig
     */
    public function getAttributeName(): string
    {
        return 'GlobalItemCheckConfig';
    }
}
