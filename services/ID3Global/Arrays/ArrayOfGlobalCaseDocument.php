<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseDocument Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q834:ArrayOfGlobalCaseDocument
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseDocument extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseDocument
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseDocument[]
     */
    protected ?array $GlobalCaseDocument = null;
    /**
     * Constructor method for ArrayOfGlobalCaseDocument
     * @uses ArrayOfGlobalCaseDocument::setGlobalCaseDocument()
     * @param \ID3Global\Models\GlobalCaseDocument[] $globalCaseDocument
     */
    public function __construct(?array $globalCaseDocument = null)
    {
        $this
            ->setGlobalCaseDocument($globalCaseDocument);
    }
    /**
     * Get GlobalCaseDocument value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseDocument[]
     */
    public function getGlobalCaseDocument(): ?array
    {
        return isset($this->GlobalCaseDocument) ? $this->GlobalCaseDocument : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseDocument method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseDocument method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseDocumentForArrayConstraintsFromSetGlobalCaseDocument(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseDocumentGlobalCaseDocumentItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseDocumentGlobalCaseDocumentItem instanceof \ID3Global\Models\GlobalCaseDocument) {
                $invalidValues[] = is_object($arrayOfGlobalCaseDocumentGlobalCaseDocumentItem) ? get_class($arrayOfGlobalCaseDocumentGlobalCaseDocumentItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseDocumentGlobalCaseDocumentItem), var_export($arrayOfGlobalCaseDocumentGlobalCaseDocumentItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseDocument property can only contain items of type \ID3Global\Models\GlobalCaseDocument, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseDocument value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseDocument[] $globalCaseDocument
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDocument
     */
    public function setGlobalCaseDocument(?array $globalCaseDocument = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseDocumentArrayErrorMessage = self::validateGlobalCaseDocumentForArrayConstraintsFromSetGlobalCaseDocument($globalCaseDocument))) {
            throw new InvalidArgumentException($globalCaseDocumentArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseDocument) || (is_array($globalCaseDocument) && empty($globalCaseDocument))) {
            unset($this->GlobalCaseDocument);
        } else {
            $this->GlobalCaseDocument = $globalCaseDocument;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseDocument|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseDocument
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseDocument|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseDocument
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseDocument|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseDocument
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseDocument|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseDocument
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseDocument|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseDocument
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseDocument $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDocument
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseDocument) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseDocument property can only contain items of type \ID3Global\Models\GlobalCaseDocument, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseDocument
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseDocument';
    }
}
