<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalBand Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q207:ArrayOfGlobalBand
 * @subpackage Arrays
 */
class ArrayOfGlobalBand extends AbstractStructArrayBase
{
    /**
     * The GlobalBand
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalBand[]
     */
    protected ?array $GlobalBand = null;
    /**
     * Constructor method for ArrayOfGlobalBand
     * @uses ArrayOfGlobalBand::setGlobalBand()
     * @param \ID3Global\Models\GlobalBand[] $globalBand
     */
    public function __construct(?array $globalBand = null)
    {
        $this
            ->setGlobalBand($globalBand);
    }
    /**
     * Get GlobalBand value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalBand[]
     */
    public function getGlobalBand(): ?array
    {
        return isset($this->GlobalBand) ? $this->GlobalBand : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalBand method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalBand method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalBandForArrayConstraintsFromSetGlobalBand(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalBandGlobalBandItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalBandGlobalBandItem instanceof \ID3Global\Models\GlobalBand) {
                $invalidValues[] = is_object($arrayOfGlobalBandGlobalBandItem) ? get_class($arrayOfGlobalBandGlobalBandItem) : sprintf('%s(%s)', gettype($arrayOfGlobalBandGlobalBandItem), var_export($arrayOfGlobalBandGlobalBandItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalBand property can only contain items of type \ID3Global\Models\GlobalBand, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalBand value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalBand[] $globalBand
     * @return \ID3Global\Arrays\ArrayOfGlobalBand
     */
    public function setGlobalBand(?array $globalBand = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalBandArrayErrorMessage = self::validateGlobalBandForArrayConstraintsFromSetGlobalBand($globalBand))) {
            throw new InvalidArgumentException($globalBandArrayErrorMessage, __LINE__);
        }
        if (is_null($globalBand) || (is_array($globalBand) && empty($globalBand))) {
            unset($this->GlobalBand);
        } else {
            $this->GlobalBand = $globalBand;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalBand|null
     */
    public function current(): ?\ID3Global\Models\GlobalBand
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalBand|null
     */
    public function item($index): ?\ID3Global\Models\GlobalBand
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalBand|null
     */
    public function first(): ?\ID3Global\Models\GlobalBand
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalBand|null
     */
    public function last(): ?\ID3Global\Models\GlobalBand
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalBand|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalBand
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalBand $item
     * @return \ID3Global\Arrays\ArrayOfGlobalBand
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalBand) {
            throw new InvalidArgumentException(sprintf('The GlobalBand property can only contain items of type \ID3Global\Models\GlobalBand, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalBand
     */
    public function getAttributeName(): string
    {
        return 'GlobalBand';
    }
}
