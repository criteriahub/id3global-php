<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalProvince Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q21:ArrayOfGlobalProvince
 * @subpackage Arrays
 */
class ArrayOfGlobalProvince extends AbstractStructArrayBase
{
    /**
     * The GlobalProvince
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProvince[]
     */
    protected ?array $GlobalProvince = null;
    /**
     * Constructor method for ArrayOfGlobalProvince
     * @uses ArrayOfGlobalProvince::setGlobalProvince()
     * @param \ID3Global\Models\GlobalProvince[] $globalProvince
     */
    public function __construct(?array $globalProvince = null)
    {
        $this
            ->setGlobalProvince($globalProvince);
    }
    /**
     * Get GlobalProvince value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProvince[]
     */
    public function getGlobalProvince(): ?array
    {
        return isset($this->GlobalProvince) ? $this->GlobalProvince : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalProvince method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalProvince method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalProvinceForArrayConstraintsFromSetGlobalProvince(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalProvinceGlobalProvinceItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalProvinceGlobalProvinceItem instanceof \ID3Global\Models\GlobalProvince) {
                $invalidValues[] = is_object($arrayOfGlobalProvinceGlobalProvinceItem) ? get_class($arrayOfGlobalProvinceGlobalProvinceItem) : sprintf('%s(%s)', gettype($arrayOfGlobalProvinceGlobalProvinceItem), var_export($arrayOfGlobalProvinceGlobalProvinceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalProvince property can only contain items of type \ID3Global\Models\GlobalProvince, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalProvince value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProvince[] $globalProvince
     * @return \ID3Global\Arrays\ArrayOfGlobalProvince
     */
    public function setGlobalProvince(?array $globalProvince = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalProvinceArrayErrorMessage = self::validateGlobalProvinceForArrayConstraintsFromSetGlobalProvince($globalProvince))) {
            throw new InvalidArgumentException($globalProvinceArrayErrorMessage, __LINE__);
        }
        if (is_null($globalProvince) || (is_array($globalProvince) && empty($globalProvince))) {
            unset($this->GlobalProvince);
        } else {
            $this->GlobalProvince = $globalProvince;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalProvince|null
     */
    public function current(): ?\ID3Global\Models\GlobalProvince
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalProvince|null
     */
    public function item($index): ?\ID3Global\Models\GlobalProvince
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalProvince|null
     */
    public function first(): ?\ID3Global\Models\GlobalProvince
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalProvince|null
     */
    public function last(): ?\ID3Global\Models\GlobalProvince
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalProvince|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalProvince
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProvince $item
     * @return \ID3Global\Arrays\ArrayOfGlobalProvince
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalProvince) {
            throw new InvalidArgumentException(sprintf('The GlobalProvince property can only contain items of type \ID3Global\Models\GlobalProvince, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalProvince
     */
    public function getAttributeName(): string
    {
        return 'GlobalProvince';
    }
}
