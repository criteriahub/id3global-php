<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalProfileVersionsFlag Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q173:ArrayOfGlobalProfileVersionsFlag
 * @subpackage Arrays
 */
class ArrayOfGlobalProfileVersionsFlag extends AbstractStructArrayBase
{
    /**
     * The GlobalProfileVersionsFlag
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileVersionsFlag[]
     */
    protected ?array $GlobalProfileVersionsFlag = null;
    /**
     * Constructor method for ArrayOfGlobalProfileVersionsFlag
     * @uses ArrayOfGlobalProfileVersionsFlag::setGlobalProfileVersionsFlag()
     * @param \ID3Global\Models\GlobalProfileVersionsFlag[] $globalProfileVersionsFlag
     */
    public function __construct(?array $globalProfileVersionsFlag = null)
    {
        $this
            ->setGlobalProfileVersionsFlag($globalProfileVersionsFlag);
    }
    /**
     * Get GlobalProfileVersionsFlag value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileVersionsFlag[]
     */
    public function getGlobalProfileVersionsFlag(): ?array
    {
        return isset($this->GlobalProfileVersionsFlag) ? $this->GlobalProfileVersionsFlag : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalProfileVersionsFlag method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalProfileVersionsFlag method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalProfileVersionsFlagForArrayConstraintsFromSetGlobalProfileVersionsFlag(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalProfileVersionsFlagGlobalProfileVersionsFlagItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalProfileVersionsFlagGlobalProfileVersionsFlagItem instanceof \ID3Global\Models\GlobalProfileVersionsFlag) {
                $invalidValues[] = is_object($arrayOfGlobalProfileVersionsFlagGlobalProfileVersionsFlagItem) ? get_class($arrayOfGlobalProfileVersionsFlagGlobalProfileVersionsFlagItem) : sprintf('%s(%s)', gettype($arrayOfGlobalProfileVersionsFlagGlobalProfileVersionsFlagItem), var_export($arrayOfGlobalProfileVersionsFlagGlobalProfileVersionsFlagItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalProfileVersionsFlag property can only contain items of type \ID3Global\Models\GlobalProfileVersionsFlag, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalProfileVersionsFlag value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProfileVersionsFlag[] $globalProfileVersionsFlag
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileVersionsFlag
     */
    public function setGlobalProfileVersionsFlag(?array $globalProfileVersionsFlag = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalProfileVersionsFlagArrayErrorMessage = self::validateGlobalProfileVersionsFlagForArrayConstraintsFromSetGlobalProfileVersionsFlag($globalProfileVersionsFlag))) {
            throw new InvalidArgumentException($globalProfileVersionsFlagArrayErrorMessage, __LINE__);
        }
        if (is_null($globalProfileVersionsFlag) || (is_array($globalProfileVersionsFlag) && empty($globalProfileVersionsFlag))) {
            unset($this->GlobalProfileVersionsFlag);
        } else {
            $this->GlobalProfileVersionsFlag = $globalProfileVersionsFlag;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalProfileVersionsFlag|null
     */
    public function current(): ?\ID3Global\Models\GlobalProfileVersionsFlag
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalProfileVersionsFlag|null
     */
    public function item($index): ?\ID3Global\Models\GlobalProfileVersionsFlag
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalProfileVersionsFlag|null
     */
    public function first(): ?\ID3Global\Models\GlobalProfileVersionsFlag
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalProfileVersionsFlag|null
     */
    public function last(): ?\ID3Global\Models\GlobalProfileVersionsFlag
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalProfileVersionsFlag|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalProfileVersionsFlag
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProfileVersionsFlag $item
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileVersionsFlag
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalProfileVersionsFlag) {
            throw new InvalidArgumentException(sprintf('The GlobalProfileVersionsFlag property can only contain items of type \ID3Global\Models\GlobalProfileVersionsFlag, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalProfileVersionsFlag
     */
    public function getAttributeName(): string
    {
        return 'GlobalProfileVersionsFlag';
    }
}
