<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalDocumentCategorySubmissionType Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q829:ArrayOfGlobalDocumentCategorySubmissionType
 * @subpackage Arrays
 */
class ArrayOfGlobalDocumentCategorySubmissionType extends AbstractStructArrayBase
{
    /**
     * The GlobalDocumentCategorySubmissionType
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDocumentCategorySubmissionType[]
     */
    protected ?array $GlobalDocumentCategorySubmissionType = null;
    /**
     * Constructor method for ArrayOfGlobalDocumentCategorySubmissionType
     * @uses ArrayOfGlobalDocumentCategorySubmissionType::setGlobalDocumentCategorySubmissionType()
     * @param \ID3Global\Models\GlobalDocumentCategorySubmissionType[] $globalDocumentCategorySubmissionType
     */
    public function __construct(?array $globalDocumentCategorySubmissionType = null)
    {
        $this
            ->setGlobalDocumentCategorySubmissionType($globalDocumentCategorySubmissionType);
    }
    /**
     * Get GlobalDocumentCategorySubmissionType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDocumentCategorySubmissionType[]
     */
    public function getGlobalDocumentCategorySubmissionType(): ?array
    {
        return isset($this->GlobalDocumentCategorySubmissionType) ? $this->GlobalDocumentCategorySubmissionType : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalDocumentCategorySubmissionType method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalDocumentCategorySubmissionType method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalDocumentCategorySubmissionTypeForArrayConstraintsFromSetGlobalDocumentCategorySubmissionType(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalDocumentCategorySubmissionTypeGlobalDocumentCategorySubmissionTypeItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalDocumentCategorySubmissionTypeGlobalDocumentCategorySubmissionTypeItem instanceof \ID3Global\Models\GlobalDocumentCategorySubmissionType) {
                $invalidValues[] = is_object($arrayOfGlobalDocumentCategorySubmissionTypeGlobalDocumentCategorySubmissionTypeItem) ? get_class($arrayOfGlobalDocumentCategorySubmissionTypeGlobalDocumentCategorySubmissionTypeItem) : sprintf('%s(%s)', gettype($arrayOfGlobalDocumentCategorySubmissionTypeGlobalDocumentCategorySubmissionTypeItem), var_export($arrayOfGlobalDocumentCategorySubmissionTypeGlobalDocumentCategorySubmissionTypeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalDocumentCategorySubmissionType property can only contain items of type \ID3Global\Models\GlobalDocumentCategorySubmissionType, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalDocumentCategorySubmissionType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalDocumentCategorySubmissionType[] $globalDocumentCategorySubmissionType
     * @return \ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType
     */
    public function setGlobalDocumentCategorySubmissionType(?array $globalDocumentCategorySubmissionType = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalDocumentCategorySubmissionTypeArrayErrorMessage = self::validateGlobalDocumentCategorySubmissionTypeForArrayConstraintsFromSetGlobalDocumentCategorySubmissionType($globalDocumentCategorySubmissionType))) {
            throw new InvalidArgumentException($globalDocumentCategorySubmissionTypeArrayErrorMessage, __LINE__);
        }
        if (is_null($globalDocumentCategorySubmissionType) || (is_array($globalDocumentCategorySubmissionType) && empty($globalDocumentCategorySubmissionType))) {
            unset($this->GlobalDocumentCategorySubmissionType);
        } else {
            $this->GlobalDocumentCategorySubmissionType = $globalDocumentCategorySubmissionType;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalDocumentCategorySubmissionType|null
     */
    public function current(): ?\ID3Global\Models\GlobalDocumentCategorySubmissionType
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalDocumentCategorySubmissionType|null
     */
    public function item($index): ?\ID3Global\Models\GlobalDocumentCategorySubmissionType
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalDocumentCategorySubmissionType|null
     */
    public function first(): ?\ID3Global\Models\GlobalDocumentCategorySubmissionType
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalDocumentCategorySubmissionType|null
     */
    public function last(): ?\ID3Global\Models\GlobalDocumentCategorySubmissionType
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalDocumentCategorySubmissionType|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalDocumentCategorySubmissionType
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalDocumentCategorySubmissionType $item
     * @return \ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalDocumentCategorySubmissionType) {
            throw new InvalidArgumentException(sprintf('The GlobalDocumentCategorySubmissionType property can only contain items of type \ID3Global\Models\GlobalDocumentCategorySubmissionType, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalDocumentCategorySubmissionType
     */
    public function getAttributeName(): string
    {
        return 'GlobalDocumentCategorySubmissionType';
    }
}
