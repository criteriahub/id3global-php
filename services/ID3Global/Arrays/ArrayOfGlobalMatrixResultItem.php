<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalMatrixResultItem Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q815:ArrayOfGlobalMatrixResultItem
 * @subpackage Arrays
 */
class ArrayOfGlobalMatrixResultItem extends AbstractStructArrayBase
{
    /**
     * The GlobalMatrixResultItem
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalMatrixResultItem[]
     */
    protected ?array $GlobalMatrixResultItem = null;
    /**
     * Constructor method for ArrayOfGlobalMatrixResultItem
     * @uses ArrayOfGlobalMatrixResultItem::setGlobalMatrixResultItem()
     * @param \ID3Global\Models\GlobalMatrixResultItem[] $globalMatrixResultItem
     */
    public function __construct(?array $globalMatrixResultItem = null)
    {
        $this
            ->setGlobalMatrixResultItem($globalMatrixResultItem);
    }
    /**
     * Get GlobalMatrixResultItem value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalMatrixResultItem[]
     */
    public function getGlobalMatrixResultItem(): ?array
    {
        return isset($this->GlobalMatrixResultItem) ? $this->GlobalMatrixResultItem : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalMatrixResultItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalMatrixResultItem method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalMatrixResultItemForArrayConstraintsFromSetGlobalMatrixResultItem(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalMatrixResultItemGlobalMatrixResultItemItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalMatrixResultItemGlobalMatrixResultItemItem instanceof \ID3Global\Models\GlobalMatrixResultItem) {
                $invalidValues[] = is_object($arrayOfGlobalMatrixResultItemGlobalMatrixResultItemItem) ? get_class($arrayOfGlobalMatrixResultItemGlobalMatrixResultItemItem) : sprintf('%s(%s)', gettype($arrayOfGlobalMatrixResultItemGlobalMatrixResultItemItem), var_export($arrayOfGlobalMatrixResultItemGlobalMatrixResultItemItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalMatrixResultItem property can only contain items of type \ID3Global\Models\GlobalMatrixResultItem, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalMatrixResultItem value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalMatrixResultItem[] $globalMatrixResultItem
     * @return \ID3Global\Arrays\ArrayOfGlobalMatrixResultItem
     */
    public function setGlobalMatrixResultItem(?array $globalMatrixResultItem = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalMatrixResultItemArrayErrorMessage = self::validateGlobalMatrixResultItemForArrayConstraintsFromSetGlobalMatrixResultItem($globalMatrixResultItem))) {
            throw new InvalidArgumentException($globalMatrixResultItemArrayErrorMessage, __LINE__);
        }
        if (is_null($globalMatrixResultItem) || (is_array($globalMatrixResultItem) && empty($globalMatrixResultItem))) {
            unset($this->GlobalMatrixResultItem);
        } else {
            $this->GlobalMatrixResultItem = $globalMatrixResultItem;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalMatrixResultItem|null
     */
    public function current(): ?\ID3Global\Models\GlobalMatrixResultItem
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalMatrixResultItem|null
     */
    public function item($index): ?\ID3Global\Models\GlobalMatrixResultItem
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalMatrixResultItem|null
     */
    public function first(): ?\ID3Global\Models\GlobalMatrixResultItem
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalMatrixResultItem|null
     */
    public function last(): ?\ID3Global\Models\GlobalMatrixResultItem
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalMatrixResultItem|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalMatrixResultItem
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalMatrixResultItem $item
     * @return \ID3Global\Arrays\ArrayOfGlobalMatrixResultItem
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalMatrixResultItem) {
            throw new InvalidArgumentException(sprintf('The GlobalMatrixResultItem property can only contain items of type \ID3Global\Models\GlobalMatrixResultItem, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalMatrixResultItem
     */
    public function getAttributeName(): string
    {
        return 'GlobalMatrixResultItem';
    }
}
