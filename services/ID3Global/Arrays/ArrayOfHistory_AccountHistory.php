<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfHistory.AccountHistory Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q548:ArrayOfHistory.AccountHistory
 * @subpackage Arrays
 */
class ArrayOfHistory_AccountHistory extends AbstractStructArrayBase
{
    /**
     * The History_AccountHistory
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\History_AccountHistory[]
     */
    protected ?array $History_AccountHistory = null;
    /**
     * Constructor method for ArrayOfHistory.AccountHistory
     * @uses ArrayOfHistory_AccountHistory::setHistory_AccountHistory()
     * @param \ID3Global\Models\History_AccountHistory[] $history_AccountHistory
     */
    public function __construct(?array $history_AccountHistory = null)
    {
        $this
            ->setHistory_AccountHistory($history_AccountHistory);
    }
    /**
     * Get History_AccountHistory value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\History_AccountHistory[]
     */
    public function getHistory_AccountHistory(): ?array
    {
        return isset($this->{'History.AccountHistory'}) ? $this->{'History.AccountHistory'} : null;
    }
    /**
     * This method is responsible for validating the values passed to the setHistory_AccountHistory method
     * This method is willingly generated in order to preserve the one-line inline validation within the setHistory_AccountHistory method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateHistory_AccountHistoryForArrayConstraintsFromSetHistory_AccountHistory(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfHistory_AccountHistoryHistory_AccountHistoryItem) {
            // validation for constraint: itemType
            if (!$arrayOfHistory_AccountHistoryHistory_AccountHistoryItem instanceof \ID3Global\Models\History_AccountHistory) {
                $invalidValues[] = is_object($arrayOfHistory_AccountHistoryHistory_AccountHistoryItem) ? get_class($arrayOfHistory_AccountHistoryHistory_AccountHistoryItem) : sprintf('%s(%s)', gettype($arrayOfHistory_AccountHistoryHistory_AccountHistoryItem), var_export($arrayOfHistory_AccountHistoryHistory_AccountHistoryItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The History_AccountHistory property can only contain items of type \ID3Global\Models\History_AccountHistory, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set History_AccountHistory value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\History_AccountHistory[] $history_AccountHistory
     * @return \ID3Global\Arrays\ArrayOfHistory_AccountHistory
     */
    public function setHistory_AccountHistory(?array $history_AccountHistory = null): self
    {
        // validation for constraint: array
        if ('' !== ($history_AccountHistoryArrayErrorMessage = self::validateHistory_AccountHistoryForArrayConstraintsFromSetHistory_AccountHistory($history_AccountHistory))) {
            throw new InvalidArgumentException($history_AccountHistoryArrayErrorMessage, __LINE__);
        }
        if (is_null($history_AccountHistory) || (is_array($history_AccountHistory) && empty($history_AccountHistory))) {
            unset($this->History_AccountHistory, $this->{'History.AccountHistory'});
        } else {
            $this->History_AccountHistory = $this->{'History.AccountHistory'} = $history_AccountHistory;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\History_AccountHistory|null
     */
    public function current(): ?\ID3Global\Models\History_AccountHistory
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\History_AccountHistory|null
     */
    public function item($index): ?\ID3Global\Models\History_AccountHistory
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\History_AccountHistory|null
     */
    public function first(): ?\ID3Global\Models\History_AccountHistory
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\History_AccountHistory|null
     */
    public function last(): ?\ID3Global\Models\History_AccountHistory
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\History_AccountHistory|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\History_AccountHistory
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\History_AccountHistory $item
     * @return \ID3Global\Arrays\ArrayOfHistory_AccountHistory
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\History_AccountHistory) {
            throw new InvalidArgumentException(sprintf('The History_AccountHistory property can only contain items of type \ID3Global\Models\History_AccountHistory, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string History.AccountHistory
     */
    public function getAttributeName(): string
    {
        return 'History.AccountHistory';
    }
}
