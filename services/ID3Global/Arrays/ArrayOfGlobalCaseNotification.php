<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseNotification Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q736:ArrayOfGlobalCaseNotification
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseNotification extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseNotification
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseNotification[]
     */
    protected ?array $GlobalCaseNotification = null;
    /**
     * Constructor method for ArrayOfGlobalCaseNotification
     * @uses ArrayOfGlobalCaseNotification::setGlobalCaseNotification()
     * @param \ID3Global\Models\GlobalCaseNotification[] $globalCaseNotification
     */
    public function __construct(?array $globalCaseNotification = null)
    {
        $this
            ->setGlobalCaseNotification($globalCaseNotification);
    }
    /**
     * Get GlobalCaseNotification value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseNotification[]
     */
    public function getGlobalCaseNotification(): ?array
    {
        return isset($this->GlobalCaseNotification) ? $this->GlobalCaseNotification : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseNotification method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseNotification method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseNotificationForArrayConstraintsFromSetGlobalCaseNotification(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseNotificationGlobalCaseNotificationItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseNotificationGlobalCaseNotificationItem instanceof \ID3Global\Models\GlobalCaseNotification) {
                $invalidValues[] = is_object($arrayOfGlobalCaseNotificationGlobalCaseNotificationItem) ? get_class($arrayOfGlobalCaseNotificationGlobalCaseNotificationItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseNotificationGlobalCaseNotificationItem), var_export($arrayOfGlobalCaseNotificationGlobalCaseNotificationItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseNotification property can only contain items of type \ID3Global\Models\GlobalCaseNotification, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseNotification value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseNotification[] $globalCaseNotification
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseNotification
     */
    public function setGlobalCaseNotification(?array $globalCaseNotification = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseNotificationArrayErrorMessage = self::validateGlobalCaseNotificationForArrayConstraintsFromSetGlobalCaseNotification($globalCaseNotification))) {
            throw new InvalidArgumentException($globalCaseNotificationArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseNotification) || (is_array($globalCaseNotification) && empty($globalCaseNotification))) {
            unset($this->GlobalCaseNotification);
        } else {
            $this->GlobalCaseNotification = $globalCaseNotification;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseNotification|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseNotification
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseNotification|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseNotification
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseNotification|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseNotification
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseNotification|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseNotification
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseNotification|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseNotification
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseNotification $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseNotification
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseNotification) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseNotification property can only contain items of type \ID3Global\Models\GlobalCaseNotification, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseNotification
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseNotification';
    }
}
