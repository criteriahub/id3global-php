<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCountryFlag Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q626:ArrayOfGlobalCountryFlag
 * @subpackage Arrays
 */
class ArrayOfGlobalCountryFlag extends AbstractStructArrayBase
{
    /**
     * The GlobalCountryFlag
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCountryFlag[]
     */
    protected ?array $GlobalCountryFlag = null;
    /**
     * Constructor method for ArrayOfGlobalCountryFlag
     * @uses ArrayOfGlobalCountryFlag::setGlobalCountryFlag()
     * @param \ID3Global\Models\GlobalCountryFlag[] $globalCountryFlag
     */
    public function __construct(?array $globalCountryFlag = null)
    {
        $this
            ->setGlobalCountryFlag($globalCountryFlag);
    }
    /**
     * Get GlobalCountryFlag value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCountryFlag[]
     */
    public function getGlobalCountryFlag(): ?array
    {
        return isset($this->GlobalCountryFlag) ? $this->GlobalCountryFlag : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCountryFlag method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCountryFlag method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCountryFlagForArrayConstraintsFromSetGlobalCountryFlag(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCountryFlagGlobalCountryFlagItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCountryFlagGlobalCountryFlagItem instanceof \ID3Global\Models\GlobalCountryFlag) {
                $invalidValues[] = is_object($arrayOfGlobalCountryFlagGlobalCountryFlagItem) ? get_class($arrayOfGlobalCountryFlagGlobalCountryFlagItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCountryFlagGlobalCountryFlagItem), var_export($arrayOfGlobalCountryFlagGlobalCountryFlagItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCountryFlag property can only contain items of type \ID3Global\Models\GlobalCountryFlag, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCountryFlag value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCountryFlag[] $globalCountryFlag
     * @return \ID3Global\Arrays\ArrayOfGlobalCountryFlag
     */
    public function setGlobalCountryFlag(?array $globalCountryFlag = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCountryFlagArrayErrorMessage = self::validateGlobalCountryFlagForArrayConstraintsFromSetGlobalCountryFlag($globalCountryFlag))) {
            throw new InvalidArgumentException($globalCountryFlagArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCountryFlag) || (is_array($globalCountryFlag) && empty($globalCountryFlag))) {
            unset($this->GlobalCountryFlag);
        } else {
            $this->GlobalCountryFlag = $globalCountryFlag;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCountryFlag|null
     */
    public function current(): ?\ID3Global\Models\GlobalCountryFlag
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCountryFlag|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCountryFlag
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCountryFlag|null
     */
    public function first(): ?\ID3Global\Models\GlobalCountryFlag
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCountryFlag|null
     */
    public function last(): ?\ID3Global\Models\GlobalCountryFlag
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCountryFlag|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCountryFlag
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCountryFlag $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCountryFlag
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCountryFlag) {
            throw new InvalidArgumentException(sprintf('The GlobalCountryFlag property can only contain items of type \ID3Global\Models\GlobalCountryFlag, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCountryFlag
     */
    public function getAttributeName(): string
    {
        return 'GlobalCountryFlag';
    }
}
