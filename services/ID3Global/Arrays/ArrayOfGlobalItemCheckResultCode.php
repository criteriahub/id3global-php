<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalItemCheckResultCode Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q37:ArrayOfGlobalItemCheckResultCode
 * @subpackage Arrays
 */
class ArrayOfGlobalItemCheckResultCode extends AbstractStructArrayBase
{
    /**
     * The GlobalItemCheckResultCode
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalItemCheckResultCode[]
     */
    protected ?array $GlobalItemCheckResultCode = null;
    /**
     * Constructor method for ArrayOfGlobalItemCheckResultCode
     * @uses ArrayOfGlobalItemCheckResultCode::setGlobalItemCheckResultCode()
     * @param \ID3Global\Models\GlobalItemCheckResultCode[] $globalItemCheckResultCode
     */
    public function __construct(?array $globalItemCheckResultCode = null)
    {
        $this
            ->setGlobalItemCheckResultCode($globalItemCheckResultCode);
    }
    /**
     * Get GlobalItemCheckResultCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalItemCheckResultCode[]
     */
    public function getGlobalItemCheckResultCode(): ?array
    {
        return isset($this->GlobalItemCheckResultCode) ? $this->GlobalItemCheckResultCode : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalItemCheckResultCode method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalItemCheckResultCode method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalItemCheckResultCodeForArrayConstraintsFromSetGlobalItemCheckResultCode(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalItemCheckResultCodeGlobalItemCheckResultCodeItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalItemCheckResultCodeGlobalItemCheckResultCodeItem instanceof \ID3Global\Models\GlobalItemCheckResultCode) {
                $invalidValues[] = is_object($arrayOfGlobalItemCheckResultCodeGlobalItemCheckResultCodeItem) ? get_class($arrayOfGlobalItemCheckResultCodeGlobalItemCheckResultCodeItem) : sprintf('%s(%s)', gettype($arrayOfGlobalItemCheckResultCodeGlobalItemCheckResultCodeItem), var_export($arrayOfGlobalItemCheckResultCodeGlobalItemCheckResultCodeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalItemCheckResultCode property can only contain items of type \ID3Global\Models\GlobalItemCheckResultCode, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalItemCheckResultCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckResultCode[] $globalItemCheckResultCode
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode
     */
    public function setGlobalItemCheckResultCode(?array $globalItemCheckResultCode = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalItemCheckResultCodeArrayErrorMessage = self::validateGlobalItemCheckResultCodeForArrayConstraintsFromSetGlobalItemCheckResultCode($globalItemCheckResultCode))) {
            throw new InvalidArgumentException($globalItemCheckResultCodeArrayErrorMessage, __LINE__);
        }
        if (is_null($globalItemCheckResultCode) || (is_array($globalItemCheckResultCode) && empty($globalItemCheckResultCode))) {
            unset($this->GlobalItemCheckResultCode);
        } else {
            $this->GlobalItemCheckResultCode = $globalItemCheckResultCode;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalItemCheckResultCode|null
     */
    public function current(): ?\ID3Global\Models\GlobalItemCheckResultCode
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalItemCheckResultCode|null
     */
    public function item($index): ?\ID3Global\Models\GlobalItemCheckResultCode
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalItemCheckResultCode|null
     */
    public function first(): ?\ID3Global\Models\GlobalItemCheckResultCode
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalItemCheckResultCode|null
     */
    public function last(): ?\ID3Global\Models\GlobalItemCheckResultCode
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalItemCheckResultCode|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalItemCheckResultCode
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckResultCode $item
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalItemCheckResultCode) {
            throw new InvalidArgumentException(sprintf('The GlobalItemCheckResultCode property can only contain items of type \ID3Global\Models\GlobalItemCheckResultCode, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalItemCheckResultCode
     */
    public function getAttributeName(): string
    {
        return 'GlobalItemCheckResultCode';
    }
}
