<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalAlternateName Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q325:ArrayOfGlobalAlternateName
 * @subpackage Arrays
 */
class ArrayOfGlobalAlternateName extends AbstractStructArrayBase
{
    /**
     * The GlobalAlternateName
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAlternateName[]
     */
    protected ?array $GlobalAlternateName = null;
    /**
     * Constructor method for ArrayOfGlobalAlternateName
     * @uses ArrayOfGlobalAlternateName::setGlobalAlternateName()
     * @param \ID3Global\Models\GlobalAlternateName[] $globalAlternateName
     */
    public function __construct(?array $globalAlternateName = null)
    {
        $this
            ->setGlobalAlternateName($globalAlternateName);
    }
    /**
     * Get GlobalAlternateName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAlternateName[]
     */
    public function getGlobalAlternateName(): ?array
    {
        return isset($this->GlobalAlternateName) ? $this->GlobalAlternateName : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalAlternateName method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalAlternateName method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalAlternateNameForArrayConstraintsFromSetGlobalAlternateName(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalAlternateNameGlobalAlternateNameItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalAlternateNameGlobalAlternateNameItem instanceof \ID3Global\Models\GlobalAlternateName) {
                $invalidValues[] = is_object($arrayOfGlobalAlternateNameGlobalAlternateNameItem) ? get_class($arrayOfGlobalAlternateNameGlobalAlternateNameItem) : sprintf('%s(%s)', gettype($arrayOfGlobalAlternateNameGlobalAlternateNameItem), var_export($arrayOfGlobalAlternateNameGlobalAlternateNameItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalAlternateName property can only contain items of type \ID3Global\Models\GlobalAlternateName, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalAlternateName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalAlternateName[] $globalAlternateName
     * @return \ID3Global\Arrays\ArrayOfGlobalAlternateName
     */
    public function setGlobalAlternateName(?array $globalAlternateName = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalAlternateNameArrayErrorMessage = self::validateGlobalAlternateNameForArrayConstraintsFromSetGlobalAlternateName($globalAlternateName))) {
            throw new InvalidArgumentException($globalAlternateNameArrayErrorMessage, __LINE__);
        }
        if (is_null($globalAlternateName) || (is_array($globalAlternateName) && empty($globalAlternateName))) {
            unset($this->GlobalAlternateName);
        } else {
            $this->GlobalAlternateName = $globalAlternateName;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalAlternateName|null
     */
    public function current(): ?\ID3Global\Models\GlobalAlternateName
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalAlternateName|null
     */
    public function item($index): ?\ID3Global\Models\GlobalAlternateName
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalAlternateName|null
     */
    public function first(): ?\ID3Global\Models\GlobalAlternateName
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalAlternateName|null
     */
    public function last(): ?\ID3Global\Models\GlobalAlternateName
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalAlternateName|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalAlternateName
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalAlternateName $item
     * @return \ID3Global\Arrays\ArrayOfGlobalAlternateName
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalAlternateName) {
            throw new InvalidArgumentException(sprintf('The GlobalAlternateName property can only contain items of type \ID3Global\Models\GlobalAlternateName, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalAlternateName
     */
    public function getAttributeName(): string
    {
        return 'GlobalAlternateName';
    }
}
