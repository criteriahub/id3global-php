<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSanctionsPosition Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q494:ArrayOfGlobalSanctionsPosition
 * @subpackage Arrays
 */
class ArrayOfGlobalSanctionsPosition extends AbstractStructArrayBase
{
    /**
     * The GlobalSanctionsPosition
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSanctionsPosition[]
     */
    protected ?array $GlobalSanctionsPosition = null;
    /**
     * Constructor method for ArrayOfGlobalSanctionsPosition
     * @uses ArrayOfGlobalSanctionsPosition::setGlobalSanctionsPosition()
     * @param \ID3Global\Models\GlobalSanctionsPosition[] $globalSanctionsPosition
     */
    public function __construct(?array $globalSanctionsPosition = null)
    {
        $this
            ->setGlobalSanctionsPosition($globalSanctionsPosition);
    }
    /**
     * Get GlobalSanctionsPosition value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSanctionsPosition[]
     */
    public function getGlobalSanctionsPosition(): ?array
    {
        return isset($this->GlobalSanctionsPosition) ? $this->GlobalSanctionsPosition : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSanctionsPosition method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSanctionsPosition method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSanctionsPositionForArrayConstraintsFromSetGlobalSanctionsPosition(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSanctionsPositionGlobalSanctionsPositionItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSanctionsPositionGlobalSanctionsPositionItem instanceof \ID3Global\Models\GlobalSanctionsPosition) {
                $invalidValues[] = is_object($arrayOfGlobalSanctionsPositionGlobalSanctionsPositionItem) ? get_class($arrayOfGlobalSanctionsPositionGlobalSanctionsPositionItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSanctionsPositionGlobalSanctionsPositionItem), var_export($arrayOfGlobalSanctionsPositionGlobalSanctionsPositionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSanctionsPosition property can only contain items of type \ID3Global\Models\GlobalSanctionsPosition, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSanctionsPosition value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSanctionsPosition[] $globalSanctionsPosition
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsPosition
     */
    public function setGlobalSanctionsPosition(?array $globalSanctionsPosition = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSanctionsPositionArrayErrorMessage = self::validateGlobalSanctionsPositionForArrayConstraintsFromSetGlobalSanctionsPosition($globalSanctionsPosition))) {
            throw new InvalidArgumentException($globalSanctionsPositionArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSanctionsPosition) || (is_array($globalSanctionsPosition) && empty($globalSanctionsPosition))) {
            unset($this->GlobalSanctionsPosition);
        } else {
            $this->GlobalSanctionsPosition = $globalSanctionsPosition;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSanctionsPosition|null
     */
    public function current(): ?\ID3Global\Models\GlobalSanctionsPosition
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSanctionsPosition|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSanctionsPosition
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSanctionsPosition|null
     */
    public function first(): ?\ID3Global\Models\GlobalSanctionsPosition
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSanctionsPosition|null
     */
    public function last(): ?\ID3Global\Models\GlobalSanctionsPosition
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSanctionsPosition|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSanctionsPosition
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSanctionsPosition $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsPosition
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSanctionsPosition) {
            throw new InvalidArgumentException(sprintf('The GlobalSanctionsPosition property can only contain items of type \ID3Global\Models\GlobalSanctionsPosition, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSanctionsPosition
     */
    public function getAttributeName(): string
    {
        return 'GlobalSanctionsPosition';
    }
}
