<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCIFASProductCode Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q112:ArrayOfGlobalCIFASProductCode
 * @subpackage Arrays
 */
class ArrayOfGlobalCIFASProductCode extends AbstractStructArrayBase
{
    /**
     * The GlobalCIFASProductCode
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCIFASProductCode[]
     */
    protected ?array $GlobalCIFASProductCode = null;
    /**
     * Constructor method for ArrayOfGlobalCIFASProductCode
     * @uses ArrayOfGlobalCIFASProductCode::setGlobalCIFASProductCode()
     * @param \ID3Global\Models\GlobalCIFASProductCode[] $globalCIFASProductCode
     */
    public function __construct(?array $globalCIFASProductCode = null)
    {
        $this
            ->setGlobalCIFASProductCode($globalCIFASProductCode);
    }
    /**
     * Get GlobalCIFASProductCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCIFASProductCode[]
     */
    public function getGlobalCIFASProductCode(): ?array
    {
        return isset($this->GlobalCIFASProductCode) ? $this->GlobalCIFASProductCode : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCIFASProductCode method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCIFASProductCode method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCIFASProductCodeForArrayConstraintsFromSetGlobalCIFASProductCode(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCIFASProductCodeGlobalCIFASProductCodeItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCIFASProductCodeGlobalCIFASProductCodeItem instanceof \ID3Global\Models\GlobalCIFASProductCode) {
                $invalidValues[] = is_object($arrayOfGlobalCIFASProductCodeGlobalCIFASProductCodeItem) ? get_class($arrayOfGlobalCIFASProductCodeGlobalCIFASProductCodeItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCIFASProductCodeGlobalCIFASProductCodeItem), var_export($arrayOfGlobalCIFASProductCodeGlobalCIFASProductCodeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCIFASProductCode property can only contain items of type \ID3Global\Models\GlobalCIFASProductCode, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCIFASProductCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCIFASProductCode[] $globalCIFASProductCode
     * @return \ID3Global\Arrays\ArrayOfGlobalCIFASProductCode
     */
    public function setGlobalCIFASProductCode(?array $globalCIFASProductCode = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCIFASProductCodeArrayErrorMessage = self::validateGlobalCIFASProductCodeForArrayConstraintsFromSetGlobalCIFASProductCode($globalCIFASProductCode))) {
            throw new InvalidArgumentException($globalCIFASProductCodeArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCIFASProductCode) || (is_array($globalCIFASProductCode) && empty($globalCIFASProductCode))) {
            unset($this->GlobalCIFASProductCode);
        } else {
            $this->GlobalCIFASProductCode = $globalCIFASProductCode;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCIFASProductCode|null
     */
    public function current(): ?\ID3Global\Models\GlobalCIFASProductCode
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCIFASProductCode|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCIFASProductCode
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCIFASProductCode|null
     */
    public function first(): ?\ID3Global\Models\GlobalCIFASProductCode
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCIFASProductCode|null
     */
    public function last(): ?\ID3Global\Models\GlobalCIFASProductCode
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCIFASProductCode|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCIFASProductCode
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCIFASProductCode $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCIFASProductCode
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCIFASProductCode) {
            throw new InvalidArgumentException(sprintf('The GlobalCIFASProductCode property can only contain items of type \ID3Global\Models\GlobalCIFASProductCode, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCIFASProductCode
     */
    public function getAttributeName(): string
    {
        return 'GlobalCIFASProductCode';
    }
}
