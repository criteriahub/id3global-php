<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseState Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q920:ArrayOfGlobalCaseState
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseState extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseState
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseState[]
     */
    protected ?array $GlobalCaseState = null;
    /**
     * Constructor method for ArrayOfGlobalCaseState
     * @uses ArrayOfGlobalCaseState::setGlobalCaseState()
     * @param \ID3Global\Models\GlobalCaseState[] $globalCaseState
     */
    public function __construct(?array $globalCaseState = null)
    {
        $this
            ->setGlobalCaseState($globalCaseState);
    }
    /**
     * Get GlobalCaseState value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseState[]
     */
    public function getGlobalCaseState(): ?array
    {
        return isset($this->GlobalCaseState) ? $this->GlobalCaseState : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseState method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseState method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseStateForArrayConstraintsFromSetGlobalCaseState(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseStateGlobalCaseStateItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseStateGlobalCaseStateItem instanceof \ID3Global\Models\GlobalCaseState) {
                $invalidValues[] = is_object($arrayOfGlobalCaseStateGlobalCaseStateItem) ? get_class($arrayOfGlobalCaseStateGlobalCaseStateItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseStateGlobalCaseStateItem), var_export($arrayOfGlobalCaseStateGlobalCaseStateItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseState property can only contain items of type \ID3Global\Models\GlobalCaseState, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseState value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseState[] $globalCaseState
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseState
     */
    public function setGlobalCaseState(?array $globalCaseState = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseStateArrayErrorMessage = self::validateGlobalCaseStateForArrayConstraintsFromSetGlobalCaseState($globalCaseState))) {
            throw new InvalidArgumentException($globalCaseStateArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseState) || (is_array($globalCaseState) && empty($globalCaseState))) {
            unset($this->GlobalCaseState);
        } else {
            $this->GlobalCaseState = $globalCaseState;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseState|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseState
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseState|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseState
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseState|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseState
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseState|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseState
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseState|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseState
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseState $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseState
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseState) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseState property can only contain items of type \ID3Global\Models\GlobalCaseState, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseState
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseState';
    }
}
