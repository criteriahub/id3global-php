<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalKeyValuePairOfstringstring Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q767:ArrayOfGlobalKeyValuePairOfstringstring
 * @subpackage Arrays
 */
class ArrayOfGlobalKeyValuePairOfstringstring extends AbstractStructArrayBase
{
    /**
     * The GlobalKeyValuePairOfstringstring
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \ID3Global\Models\GlobalKeyValuePairOfstringstring[]
     */
    protected ?array $GlobalKeyValuePairOfstringstring = null;
    /**
     * Constructor method for ArrayOfGlobalKeyValuePairOfstringstring
     * @uses ArrayOfGlobalKeyValuePairOfstringstring::setGlobalKeyValuePairOfstringstring()
     * @param \ID3Global\Models\GlobalKeyValuePairOfstringstring[] $globalKeyValuePairOfstringstring
     */
    public function __construct(?array $globalKeyValuePairOfstringstring = null)
    {
        $this
            ->setGlobalKeyValuePairOfstringstring($globalKeyValuePairOfstringstring);
    }
    /**
     * Get GlobalKeyValuePairOfstringstring value
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringstring[]
     */
    public function getGlobalKeyValuePairOfstringstring(): ?array
    {
        return $this->GlobalKeyValuePairOfstringstring;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalKeyValuePairOfstringstring method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalKeyValuePairOfstringstring method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalKeyValuePairOfstringstringForArrayConstraintsFromSetGlobalKeyValuePairOfstringstring(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalKeyValuePairOfstringstringGlobalKeyValuePairOfstringstringItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalKeyValuePairOfstringstringGlobalKeyValuePairOfstringstringItem instanceof \ID3Global\Models\GlobalKeyValuePairOfstringstring) {
                $invalidValues[] = is_object($arrayOfGlobalKeyValuePairOfstringstringGlobalKeyValuePairOfstringstringItem) ? get_class($arrayOfGlobalKeyValuePairOfstringstringGlobalKeyValuePairOfstringstringItem) : sprintf('%s(%s)', gettype($arrayOfGlobalKeyValuePairOfstringstringGlobalKeyValuePairOfstringstringItem), var_export($arrayOfGlobalKeyValuePairOfstringstringGlobalKeyValuePairOfstringstringItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalKeyValuePairOfstringstring property can only contain items of type \ID3Global\Models\GlobalKeyValuePairOfstringstring, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalKeyValuePairOfstringstring value
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalKeyValuePairOfstringstring[] $globalKeyValuePairOfstringstring
     * @return \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring
     */
    public function setGlobalKeyValuePairOfstringstring(?array $globalKeyValuePairOfstringstring = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalKeyValuePairOfstringstringArrayErrorMessage = self::validateGlobalKeyValuePairOfstringstringForArrayConstraintsFromSetGlobalKeyValuePairOfstringstring($globalKeyValuePairOfstringstring))) {
            throw new InvalidArgumentException($globalKeyValuePairOfstringstringArrayErrorMessage, __LINE__);
        }
        $this->GlobalKeyValuePairOfstringstring = $globalKeyValuePairOfstringstring;
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringstring|null
     */
    public function current(): ?\ID3Global\Models\GlobalKeyValuePairOfstringstring
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringstring|null
     */
    public function item($index): ?\ID3Global\Models\GlobalKeyValuePairOfstringstring
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringstring|null
     */
    public function first(): ?\ID3Global\Models\GlobalKeyValuePairOfstringstring
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringstring|null
     */
    public function last(): ?\ID3Global\Models\GlobalKeyValuePairOfstringstring
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringstring|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalKeyValuePairOfstringstring
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalKeyValuePairOfstringstring $item
     * @return \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalKeyValuePairOfstringstring) {
            throw new InvalidArgumentException(sprintf('The GlobalKeyValuePairOfstringstring property can only contain items of type \ID3Global\Models\GlobalKeyValuePairOfstringstring, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalKeyValuePairOfstringstring
     */
    public function getAttributeName(): string
    {
        return 'GlobalKeyValuePairOfstringstring';
    }
}
