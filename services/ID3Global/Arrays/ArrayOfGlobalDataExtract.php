<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalDataExtract Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q505:ArrayOfGlobalDataExtract
 * @subpackage Arrays
 */
class ArrayOfGlobalDataExtract extends AbstractStructArrayBase
{
    /**
     * The GlobalDataExtract
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDataExtract[]
     */
    protected ?array $GlobalDataExtract = null;
    /**
     * Constructor method for ArrayOfGlobalDataExtract
     * @uses ArrayOfGlobalDataExtract::setGlobalDataExtract()
     * @param \ID3Global\Models\GlobalDataExtract[] $globalDataExtract
     */
    public function __construct(?array $globalDataExtract = null)
    {
        $this
            ->setGlobalDataExtract($globalDataExtract);
    }
    /**
     * Get GlobalDataExtract value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDataExtract[]
     */
    public function getGlobalDataExtract(): ?array
    {
        return isset($this->GlobalDataExtract) ? $this->GlobalDataExtract : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalDataExtract method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalDataExtract method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalDataExtractForArrayConstraintsFromSetGlobalDataExtract(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalDataExtractGlobalDataExtractItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalDataExtractGlobalDataExtractItem instanceof \ID3Global\Models\GlobalDataExtract) {
                $invalidValues[] = is_object($arrayOfGlobalDataExtractGlobalDataExtractItem) ? get_class($arrayOfGlobalDataExtractGlobalDataExtractItem) : sprintf('%s(%s)', gettype($arrayOfGlobalDataExtractGlobalDataExtractItem), var_export($arrayOfGlobalDataExtractGlobalDataExtractItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalDataExtract property can only contain items of type \ID3Global\Models\GlobalDataExtract, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalDataExtract value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalDataExtract[] $globalDataExtract
     * @return \ID3Global\Arrays\ArrayOfGlobalDataExtract
     */
    public function setGlobalDataExtract(?array $globalDataExtract = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalDataExtractArrayErrorMessage = self::validateGlobalDataExtractForArrayConstraintsFromSetGlobalDataExtract($globalDataExtract))) {
            throw new InvalidArgumentException($globalDataExtractArrayErrorMessage, __LINE__);
        }
        if (is_null($globalDataExtract) || (is_array($globalDataExtract) && empty($globalDataExtract))) {
            unset($this->GlobalDataExtract);
        } else {
            $this->GlobalDataExtract = $globalDataExtract;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalDataExtract|null
     */
    public function current(): ?\ID3Global\Models\GlobalDataExtract
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalDataExtract|null
     */
    public function item($index): ?\ID3Global\Models\GlobalDataExtract
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalDataExtract|null
     */
    public function first(): ?\ID3Global\Models\GlobalDataExtract
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalDataExtract|null
     */
    public function last(): ?\ID3Global\Models\GlobalDataExtract
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalDataExtract|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalDataExtract
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalDataExtract $item
     * @return \ID3Global\Arrays\ArrayOfGlobalDataExtract
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalDataExtract) {
            throw new InvalidArgumentException(sprintf('The GlobalDataExtract property can only contain items of type \ID3Global\Models\GlobalDataExtract, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalDataExtract
     */
    public function getAttributeName(): string
    {
        return 'GlobalDataExtract';
    }
}
