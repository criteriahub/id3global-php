<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalItemCheckDecisionBands Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q441:ArrayOfGlobalItemCheckDecisionBands
 * @subpackage Arrays
 */
class ArrayOfGlobalItemCheckDecisionBands extends AbstractStructArrayBase
{
    /**
     * The GlobalItemCheckDecisionBands
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalItemCheckDecisionBands[]
     */
    protected ?array $GlobalItemCheckDecisionBands = null;
    /**
     * Constructor method for ArrayOfGlobalItemCheckDecisionBands
     * @uses ArrayOfGlobalItemCheckDecisionBands::setGlobalItemCheckDecisionBands()
     * @param \ID3Global\Models\GlobalItemCheckDecisionBands[] $globalItemCheckDecisionBands
     */
    public function __construct(?array $globalItemCheckDecisionBands = null)
    {
        $this
            ->setGlobalItemCheckDecisionBands($globalItemCheckDecisionBands);
    }
    /**
     * Get GlobalItemCheckDecisionBands value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalItemCheckDecisionBands[]
     */
    public function getGlobalItemCheckDecisionBands(): ?array
    {
        return isset($this->GlobalItemCheckDecisionBands) ? $this->GlobalItemCheckDecisionBands : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalItemCheckDecisionBands method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalItemCheckDecisionBands method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalItemCheckDecisionBandsForArrayConstraintsFromSetGlobalItemCheckDecisionBands(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalItemCheckDecisionBandsGlobalItemCheckDecisionBandsItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalItemCheckDecisionBandsGlobalItemCheckDecisionBandsItem instanceof \ID3Global\Models\GlobalItemCheckDecisionBands) {
                $invalidValues[] = is_object($arrayOfGlobalItemCheckDecisionBandsGlobalItemCheckDecisionBandsItem) ? get_class($arrayOfGlobalItemCheckDecisionBandsGlobalItemCheckDecisionBandsItem) : sprintf('%s(%s)', gettype($arrayOfGlobalItemCheckDecisionBandsGlobalItemCheckDecisionBandsItem), var_export($arrayOfGlobalItemCheckDecisionBandsGlobalItemCheckDecisionBandsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalItemCheckDecisionBands property can only contain items of type \ID3Global\Models\GlobalItemCheckDecisionBands, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalItemCheckDecisionBands value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckDecisionBands[] $globalItemCheckDecisionBands
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands
     */
    public function setGlobalItemCheckDecisionBands(?array $globalItemCheckDecisionBands = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalItemCheckDecisionBandsArrayErrorMessage = self::validateGlobalItemCheckDecisionBandsForArrayConstraintsFromSetGlobalItemCheckDecisionBands($globalItemCheckDecisionBands))) {
            throw new InvalidArgumentException($globalItemCheckDecisionBandsArrayErrorMessage, __LINE__);
        }
        if (is_null($globalItemCheckDecisionBands) || (is_array($globalItemCheckDecisionBands) && empty($globalItemCheckDecisionBands))) {
            unset($this->GlobalItemCheckDecisionBands);
        } else {
            $this->GlobalItemCheckDecisionBands = $globalItemCheckDecisionBands;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalItemCheckDecisionBands|null
     */
    public function current(): ?\ID3Global\Models\GlobalItemCheckDecisionBands
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalItemCheckDecisionBands|null
     */
    public function item($index): ?\ID3Global\Models\GlobalItemCheckDecisionBands
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalItemCheckDecisionBands|null
     */
    public function first(): ?\ID3Global\Models\GlobalItemCheckDecisionBands
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalItemCheckDecisionBands|null
     */
    public function last(): ?\ID3Global\Models\GlobalItemCheckDecisionBands
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalItemCheckDecisionBands|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalItemCheckDecisionBands
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalItemCheckDecisionBands $item
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalItemCheckDecisionBands) {
            throw new InvalidArgumentException(sprintf('The GlobalItemCheckDecisionBands property can only contain items of type \ID3Global\Models\GlobalItemCheckDecisionBands, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalItemCheckDecisionBands
     */
    public function getAttributeName(): string
    {
        return 'GlobalItemCheckDecisionBands';
    }
}
