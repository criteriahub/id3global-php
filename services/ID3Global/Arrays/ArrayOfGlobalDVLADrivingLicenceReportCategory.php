<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalDVLADrivingLicenceReportCategory Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q782:ArrayOfGlobalDVLADrivingLicenceReportCategory
 * @subpackage Arrays
 */
class ArrayOfGlobalDVLADrivingLicenceReportCategory extends AbstractStructArrayBase
{
    /**
     * The GlobalDVLADrivingLicenceReportCategory
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory[]
     */
    protected ?array $GlobalDVLADrivingLicenceReportCategory = null;
    /**
     * Constructor method for ArrayOfGlobalDVLADrivingLicenceReportCategory
     * @uses ArrayOfGlobalDVLADrivingLicenceReportCategory::setGlobalDVLADrivingLicenceReportCategory()
     * @param \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory[] $globalDVLADrivingLicenceReportCategory
     */
    public function __construct(?array $globalDVLADrivingLicenceReportCategory = null)
    {
        $this
            ->setGlobalDVLADrivingLicenceReportCategory($globalDVLADrivingLicenceReportCategory);
    }
    /**
     * Get GlobalDVLADrivingLicenceReportCategory value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory[]
     */
    public function getGlobalDVLADrivingLicenceReportCategory(): ?array
    {
        return isset($this->GlobalDVLADrivingLicenceReportCategory) ? $this->GlobalDVLADrivingLicenceReportCategory : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalDVLADrivingLicenceReportCategory method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalDVLADrivingLicenceReportCategory method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalDVLADrivingLicenceReportCategoryForArrayConstraintsFromSetGlobalDVLADrivingLicenceReportCategory(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalDVLADrivingLicenceReportCategoryGlobalDVLADrivingLicenceReportCategoryItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalDVLADrivingLicenceReportCategoryGlobalDVLADrivingLicenceReportCategoryItem instanceof \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory) {
                $invalidValues[] = is_object($arrayOfGlobalDVLADrivingLicenceReportCategoryGlobalDVLADrivingLicenceReportCategoryItem) ? get_class($arrayOfGlobalDVLADrivingLicenceReportCategoryGlobalDVLADrivingLicenceReportCategoryItem) : sprintf('%s(%s)', gettype($arrayOfGlobalDVLADrivingLicenceReportCategoryGlobalDVLADrivingLicenceReportCategoryItem), var_export($arrayOfGlobalDVLADrivingLicenceReportCategoryGlobalDVLADrivingLicenceReportCategoryItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalDVLADrivingLicenceReportCategory property can only contain items of type \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalDVLADrivingLicenceReportCategory value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory[] $globalDVLADrivingLicenceReportCategory
     * @return \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportCategory
     */
    public function setGlobalDVLADrivingLicenceReportCategory(?array $globalDVLADrivingLicenceReportCategory = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalDVLADrivingLicenceReportCategoryArrayErrorMessage = self::validateGlobalDVLADrivingLicenceReportCategoryForArrayConstraintsFromSetGlobalDVLADrivingLicenceReportCategory($globalDVLADrivingLicenceReportCategory))) {
            throw new InvalidArgumentException($globalDVLADrivingLicenceReportCategoryArrayErrorMessage, __LINE__);
        }
        if (is_null($globalDVLADrivingLicenceReportCategory) || (is_array($globalDVLADrivingLicenceReportCategory) && empty($globalDVLADrivingLicenceReportCategory))) {
            unset($this->GlobalDVLADrivingLicenceReportCategory);
        } else {
            $this->GlobalDVLADrivingLicenceReportCategory = $globalDVLADrivingLicenceReportCategory;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory|null
     */
    public function current(): ?\ID3Global\Models\GlobalDVLADrivingLicenceReportCategory
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory|null
     */
    public function item($index): ?\ID3Global\Models\GlobalDVLADrivingLicenceReportCategory
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory|null
     */
    public function first(): ?\ID3Global\Models\GlobalDVLADrivingLicenceReportCategory
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory|null
     */
    public function last(): ?\ID3Global\Models\GlobalDVLADrivingLicenceReportCategory
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalDVLADrivingLicenceReportCategory
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory $item
     * @return \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportCategory
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory) {
            throw new InvalidArgumentException(sprintf('The GlobalDVLADrivingLicenceReportCategory property can only contain items of type \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalDVLADrivingLicenceReportCategory
     */
    public function getAttributeName(): string
    {
        return 'GlobalDVLADrivingLicenceReportCategory';
    }
}
