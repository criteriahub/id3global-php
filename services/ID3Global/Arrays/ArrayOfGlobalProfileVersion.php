<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalProfileVersion Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q164:ArrayOfGlobalProfileVersion
 * @subpackage Arrays
 */
class ArrayOfGlobalProfileVersion extends AbstractStructArrayBase
{
    /**
     * The GlobalProfileVersion
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileVersion[]
     */
    protected ?array $GlobalProfileVersion = null;
    /**
     * Constructor method for ArrayOfGlobalProfileVersion
     * @uses ArrayOfGlobalProfileVersion::setGlobalProfileVersion()
     * @param \ID3Global\Models\GlobalProfileVersion[] $globalProfileVersion
     */
    public function __construct(?array $globalProfileVersion = null)
    {
        $this
            ->setGlobalProfileVersion($globalProfileVersion);
    }
    /**
     * Get GlobalProfileVersion value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileVersion[]
     */
    public function getGlobalProfileVersion(): ?array
    {
        return isset($this->GlobalProfileVersion) ? $this->GlobalProfileVersion : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalProfileVersion method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalProfileVersion method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalProfileVersionForArrayConstraintsFromSetGlobalProfileVersion(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalProfileVersionGlobalProfileVersionItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalProfileVersionGlobalProfileVersionItem instanceof \ID3Global\Models\GlobalProfileVersion) {
                $invalidValues[] = is_object($arrayOfGlobalProfileVersionGlobalProfileVersionItem) ? get_class($arrayOfGlobalProfileVersionGlobalProfileVersionItem) : sprintf('%s(%s)', gettype($arrayOfGlobalProfileVersionGlobalProfileVersionItem), var_export($arrayOfGlobalProfileVersionGlobalProfileVersionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalProfileVersion property can only contain items of type \ID3Global\Models\GlobalProfileVersion, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalProfileVersion value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProfileVersion[] $globalProfileVersion
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileVersion
     */
    public function setGlobalProfileVersion(?array $globalProfileVersion = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalProfileVersionArrayErrorMessage = self::validateGlobalProfileVersionForArrayConstraintsFromSetGlobalProfileVersion($globalProfileVersion))) {
            throw new InvalidArgumentException($globalProfileVersionArrayErrorMessage, __LINE__);
        }
        if (is_null($globalProfileVersion) || (is_array($globalProfileVersion) && empty($globalProfileVersion))) {
            unset($this->GlobalProfileVersion);
        } else {
            $this->GlobalProfileVersion = $globalProfileVersion;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalProfileVersion|null
     */
    public function current(): ?\ID3Global\Models\GlobalProfileVersion
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalProfileVersion|null
     */
    public function item($index): ?\ID3Global\Models\GlobalProfileVersion
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalProfileVersion|null
     */
    public function first(): ?\ID3Global\Models\GlobalProfileVersion
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalProfileVersion|null
     */
    public function last(): ?\ID3Global\Models\GlobalProfileVersion
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalProfileVersion|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalProfileVersion
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProfileVersion $item
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileVersion
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalProfileVersion) {
            throw new InvalidArgumentException(sprintf('The GlobalProfileVersion property can only contain items of type \ID3Global\Models\GlobalProfileVersion, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalProfileVersion
     */
    public function getAttributeName(): string
    {
        return 'GlobalProfileVersion';
    }
}
