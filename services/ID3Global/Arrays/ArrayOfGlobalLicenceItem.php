<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalLicenceItem Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q274:ArrayOfGlobalLicenceItem
 * @subpackage Arrays
 */
class ArrayOfGlobalLicenceItem extends AbstractStructArrayBase
{
    /**
     * The GlobalLicenceItem
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalLicenceItem[]
     */
    protected ?array $GlobalLicenceItem = null;
    /**
     * Constructor method for ArrayOfGlobalLicenceItem
     * @uses ArrayOfGlobalLicenceItem::setGlobalLicenceItem()
     * @param \ID3Global\Models\GlobalLicenceItem[] $globalLicenceItem
     */
    public function __construct(?array $globalLicenceItem = null)
    {
        $this
            ->setGlobalLicenceItem($globalLicenceItem);
    }
    /**
     * Get GlobalLicenceItem value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalLicenceItem[]
     */
    public function getGlobalLicenceItem(): ?array
    {
        return isset($this->GlobalLicenceItem) ? $this->GlobalLicenceItem : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalLicenceItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalLicenceItem method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalLicenceItemForArrayConstraintsFromSetGlobalLicenceItem(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalLicenceItemGlobalLicenceItemItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalLicenceItemGlobalLicenceItemItem instanceof \ID3Global\Models\GlobalLicenceItem) {
                $invalidValues[] = is_object($arrayOfGlobalLicenceItemGlobalLicenceItemItem) ? get_class($arrayOfGlobalLicenceItemGlobalLicenceItemItem) : sprintf('%s(%s)', gettype($arrayOfGlobalLicenceItemGlobalLicenceItemItem), var_export($arrayOfGlobalLicenceItemGlobalLicenceItemItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalLicenceItem property can only contain items of type \ID3Global\Models\GlobalLicenceItem, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalLicenceItem value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalLicenceItem[] $globalLicenceItem
     * @return \ID3Global\Arrays\ArrayOfGlobalLicenceItem
     */
    public function setGlobalLicenceItem(?array $globalLicenceItem = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalLicenceItemArrayErrorMessage = self::validateGlobalLicenceItemForArrayConstraintsFromSetGlobalLicenceItem($globalLicenceItem))) {
            throw new InvalidArgumentException($globalLicenceItemArrayErrorMessage, __LINE__);
        }
        if (is_null($globalLicenceItem) || (is_array($globalLicenceItem) && empty($globalLicenceItem))) {
            unset($this->GlobalLicenceItem);
        } else {
            $this->GlobalLicenceItem = $globalLicenceItem;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalLicenceItem|null
     */
    public function current(): ?\ID3Global\Models\GlobalLicenceItem
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalLicenceItem|null
     */
    public function item($index): ?\ID3Global\Models\GlobalLicenceItem
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalLicenceItem|null
     */
    public function first(): ?\ID3Global\Models\GlobalLicenceItem
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalLicenceItem|null
     */
    public function last(): ?\ID3Global\Models\GlobalLicenceItem
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalLicenceItem|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalLicenceItem
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalLicenceItem $item
     * @return \ID3Global\Arrays\ArrayOfGlobalLicenceItem
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalLicenceItem) {
            throw new InvalidArgumentException(sprintf('The GlobalLicenceItem property can only contain items of type \ID3Global\Models\GlobalLicenceItem, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalLicenceItem
     */
    public function getAttributeName(): string
    {
        return 'GlobalLicenceItem';
    }
}
