<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSupplierField Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q721:ArrayOfGlobalSupplierField
 * @subpackage Arrays
 */
class ArrayOfGlobalSupplierField extends AbstractStructArrayBase
{
    /**
     * The GlobalSupplierField
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierField[]
     */
    protected ?array $GlobalSupplierField = null;
    /**
     * Constructor method for ArrayOfGlobalSupplierField
     * @uses ArrayOfGlobalSupplierField::setGlobalSupplierField()
     * @param \ID3Global\Models\GlobalSupplierField[] $globalSupplierField
     */
    public function __construct(?array $globalSupplierField = null)
    {
        $this
            ->setGlobalSupplierField($globalSupplierField);
    }
    /**
     * Get GlobalSupplierField value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierField[]
     */
    public function getGlobalSupplierField(): ?array
    {
        return isset($this->GlobalSupplierField) ? $this->GlobalSupplierField : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSupplierField method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSupplierField method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSupplierFieldForArrayConstraintsFromSetGlobalSupplierField(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSupplierFieldGlobalSupplierFieldItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSupplierFieldGlobalSupplierFieldItem instanceof \ID3Global\Models\GlobalSupplierField) {
                $invalidValues[] = is_object($arrayOfGlobalSupplierFieldGlobalSupplierFieldItem) ? get_class($arrayOfGlobalSupplierFieldGlobalSupplierFieldItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSupplierFieldGlobalSupplierFieldItem), var_export($arrayOfGlobalSupplierFieldGlobalSupplierFieldItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSupplierField property can only contain items of type \ID3Global\Models\GlobalSupplierField, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSupplierField value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupplierField[] $globalSupplierField
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierField
     */
    public function setGlobalSupplierField(?array $globalSupplierField = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSupplierFieldArrayErrorMessage = self::validateGlobalSupplierFieldForArrayConstraintsFromSetGlobalSupplierField($globalSupplierField))) {
            throw new InvalidArgumentException($globalSupplierFieldArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSupplierField) || (is_array($globalSupplierField) && empty($globalSupplierField))) {
            unset($this->GlobalSupplierField);
        } else {
            $this->GlobalSupplierField = $globalSupplierField;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSupplierField|null
     */
    public function current(): ?\ID3Global\Models\GlobalSupplierField
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSupplierField|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSupplierField
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSupplierField|null
     */
    public function first(): ?\ID3Global\Models\GlobalSupplierField
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSupplierField|null
     */
    public function last(): ?\ID3Global\Models\GlobalSupplierField
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSupplierField|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSupplierField
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupplierField $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierField
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSupplierField) {
            throw new InvalidArgumentException(sprintf('The GlobalSupplierField property can only contain items of type \ID3Global\Models\GlobalSupplierField, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSupplierField
     */
    public function getAttributeName(): string
    {
        return 'GlobalSupplierField';
    }
}
