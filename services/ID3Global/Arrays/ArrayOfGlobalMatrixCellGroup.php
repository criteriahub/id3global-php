<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalMatrixCellGroup Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q821:ArrayOfGlobalMatrixCellGroup
 * @subpackage Arrays
 */
class ArrayOfGlobalMatrixCellGroup extends AbstractStructArrayBase
{
    /**
     * The GlobalMatrixCellGroup
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalMatrixCellGroup[]
     */
    protected ?array $GlobalMatrixCellGroup = null;
    /**
     * Constructor method for ArrayOfGlobalMatrixCellGroup
     * @uses ArrayOfGlobalMatrixCellGroup::setGlobalMatrixCellGroup()
     * @param \ID3Global\Models\GlobalMatrixCellGroup[] $globalMatrixCellGroup
     */
    public function __construct(?array $globalMatrixCellGroup = null)
    {
        $this
            ->setGlobalMatrixCellGroup($globalMatrixCellGroup);
    }
    /**
     * Get GlobalMatrixCellGroup value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalMatrixCellGroup[]
     */
    public function getGlobalMatrixCellGroup(): ?array
    {
        return isset($this->GlobalMatrixCellGroup) ? $this->GlobalMatrixCellGroup : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalMatrixCellGroup method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalMatrixCellGroup method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalMatrixCellGroupForArrayConstraintsFromSetGlobalMatrixCellGroup(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalMatrixCellGroupGlobalMatrixCellGroupItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalMatrixCellGroupGlobalMatrixCellGroupItem instanceof \ID3Global\Models\GlobalMatrixCellGroup) {
                $invalidValues[] = is_object($arrayOfGlobalMatrixCellGroupGlobalMatrixCellGroupItem) ? get_class($arrayOfGlobalMatrixCellGroupGlobalMatrixCellGroupItem) : sprintf('%s(%s)', gettype($arrayOfGlobalMatrixCellGroupGlobalMatrixCellGroupItem), var_export($arrayOfGlobalMatrixCellGroupGlobalMatrixCellGroupItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalMatrixCellGroup property can only contain items of type \ID3Global\Models\GlobalMatrixCellGroup, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalMatrixCellGroup value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalMatrixCellGroup[] $globalMatrixCellGroup
     * @return \ID3Global\Arrays\ArrayOfGlobalMatrixCellGroup
     */
    public function setGlobalMatrixCellGroup(?array $globalMatrixCellGroup = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalMatrixCellGroupArrayErrorMessage = self::validateGlobalMatrixCellGroupForArrayConstraintsFromSetGlobalMatrixCellGroup($globalMatrixCellGroup))) {
            throw new InvalidArgumentException($globalMatrixCellGroupArrayErrorMessage, __LINE__);
        }
        if (is_null($globalMatrixCellGroup) || (is_array($globalMatrixCellGroup) && empty($globalMatrixCellGroup))) {
            unset($this->GlobalMatrixCellGroup);
        } else {
            $this->GlobalMatrixCellGroup = $globalMatrixCellGroup;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalMatrixCellGroup|null
     */
    public function current(): ?\ID3Global\Models\GlobalMatrixCellGroup
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalMatrixCellGroup|null
     */
    public function item($index): ?\ID3Global\Models\GlobalMatrixCellGroup
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalMatrixCellGroup|null
     */
    public function first(): ?\ID3Global\Models\GlobalMatrixCellGroup
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalMatrixCellGroup|null
     */
    public function last(): ?\ID3Global\Models\GlobalMatrixCellGroup
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalMatrixCellGroup|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalMatrixCellGroup
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalMatrixCellGroup $item
     * @return \ID3Global\Arrays\ArrayOfGlobalMatrixCellGroup
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalMatrixCellGroup) {
            throw new InvalidArgumentException(sprintf('The GlobalMatrixCellGroup property can only contain items of type \ID3Global\Models\GlobalMatrixCellGroup, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalMatrixCellGroup
     */
    public function getAttributeName(): string
    {
        return 'GlobalMatrixCellGroup';
    }
}
