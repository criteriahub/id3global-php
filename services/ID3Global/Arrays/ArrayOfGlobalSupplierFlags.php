<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSupplierFlags Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q622:ArrayOfGlobalSupplierFlags
 * @subpackage Arrays
 */
class ArrayOfGlobalSupplierFlags extends AbstractStructArrayBase
{
    /**
     * The GlobalSupplierFlags
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierFlags[]
     */
    protected ?array $GlobalSupplierFlags = null;
    /**
     * Constructor method for ArrayOfGlobalSupplierFlags
     * @uses ArrayOfGlobalSupplierFlags::setGlobalSupplierFlags()
     * @param \ID3Global\Models\GlobalSupplierFlags[] $globalSupplierFlags
     */
    public function __construct(?array $globalSupplierFlags = null)
    {
        $this
            ->setGlobalSupplierFlags($globalSupplierFlags);
    }
    /**
     * Get GlobalSupplierFlags value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierFlags[]
     */
    public function getGlobalSupplierFlags(): ?array
    {
        return isset($this->GlobalSupplierFlags) ? $this->GlobalSupplierFlags : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSupplierFlags method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSupplierFlags method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSupplierFlagsForArrayConstraintsFromSetGlobalSupplierFlags(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSupplierFlagsGlobalSupplierFlagsItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSupplierFlagsGlobalSupplierFlagsItem instanceof \ID3Global\Models\GlobalSupplierFlags) {
                $invalidValues[] = is_object($arrayOfGlobalSupplierFlagsGlobalSupplierFlagsItem) ? get_class($arrayOfGlobalSupplierFlagsGlobalSupplierFlagsItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSupplierFlagsGlobalSupplierFlagsItem), var_export($arrayOfGlobalSupplierFlagsGlobalSupplierFlagsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSupplierFlags property can only contain items of type \ID3Global\Models\GlobalSupplierFlags, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSupplierFlags value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupplierFlags[] $globalSupplierFlags
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierFlags
     */
    public function setGlobalSupplierFlags(?array $globalSupplierFlags = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSupplierFlagsArrayErrorMessage = self::validateGlobalSupplierFlagsForArrayConstraintsFromSetGlobalSupplierFlags($globalSupplierFlags))) {
            throw new InvalidArgumentException($globalSupplierFlagsArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSupplierFlags) || (is_array($globalSupplierFlags) && empty($globalSupplierFlags))) {
            unset($this->GlobalSupplierFlags);
        } else {
            $this->GlobalSupplierFlags = $globalSupplierFlags;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSupplierFlags|null
     */
    public function current(): ?\ID3Global\Models\GlobalSupplierFlags
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSupplierFlags|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSupplierFlags
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSupplierFlags|null
     */
    public function first(): ?\ID3Global\Models\GlobalSupplierFlags
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSupplierFlags|null
     */
    public function last(): ?\ID3Global\Models\GlobalSupplierFlags
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSupplierFlags|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSupplierFlags
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupplierFlags $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierFlags
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSupplierFlags) {
            throw new InvalidArgumentException(sprintf('The GlobalSupplierFlags property can only contain items of type \ID3Global\Models\GlobalSupplierFlags, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSupplierFlags
     */
    public function getAttributeName(): string
    {
        return 'GlobalSupplierFlags';
    }
}
