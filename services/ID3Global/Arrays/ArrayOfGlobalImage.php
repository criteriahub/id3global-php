<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalImage Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q310:ArrayOfGlobalImage
 * @subpackage Arrays
 */
class ArrayOfGlobalImage extends AbstractStructArrayBase
{
    /**
     * The GlobalImage
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalImage[]
     */
    protected ?array $GlobalImage = null;
    /**
     * Constructor method for ArrayOfGlobalImage
     * @uses ArrayOfGlobalImage::setGlobalImage()
     * @param \ID3Global\Models\GlobalImage[] $globalImage
     */
    public function __construct(?array $globalImage = null)
    {
        $this
            ->setGlobalImage($globalImage);
    }
    /**
     * Get GlobalImage value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalImage[]
     */
    public function getGlobalImage(): ?array
    {
        return isset($this->GlobalImage) ? $this->GlobalImage : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalImage method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalImage method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalImageForArrayConstraintsFromSetGlobalImage(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalImageGlobalImageItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalImageGlobalImageItem instanceof \ID3Global\Models\GlobalImage) {
                $invalidValues[] = is_object($arrayOfGlobalImageGlobalImageItem) ? get_class($arrayOfGlobalImageGlobalImageItem) : sprintf('%s(%s)', gettype($arrayOfGlobalImageGlobalImageItem), var_export($arrayOfGlobalImageGlobalImageItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalImage property can only contain items of type \ID3Global\Models\GlobalImage, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalImage value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalImage[] $globalImage
     * @return \ID3Global\Arrays\ArrayOfGlobalImage
     */
    public function setGlobalImage(?array $globalImage = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalImageArrayErrorMessage = self::validateGlobalImageForArrayConstraintsFromSetGlobalImage($globalImage))) {
            throw new InvalidArgumentException($globalImageArrayErrorMessage, __LINE__);
        }
        if (is_null($globalImage) || (is_array($globalImage) && empty($globalImage))) {
            unset($this->GlobalImage);
        } else {
            $this->GlobalImage = $globalImage;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalImage|null
     */
    public function current(): ?\ID3Global\Models\GlobalImage
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalImage|null
     */
    public function item($index): ?\ID3Global\Models\GlobalImage
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalImage|null
     */
    public function first(): ?\ID3Global\Models\GlobalImage
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalImage|null
     */
    public function last(): ?\ID3Global\Models\GlobalImage
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalImage|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalImage
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalImage $item
     * @return \ID3Global\Arrays\ArrayOfGlobalImage
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalImage) {
            throw new InvalidArgumentException(sprintf('The GlobalImage property can only contain items of type \ID3Global\Models\GlobalImage, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalImage
     */
    public function getAttributeName(): string
    {
        return 'GlobalImage';
    }
}
