<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSanctionsIssuer Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q82:ArrayOfGlobalSanctionsIssuer
 * @subpackage Arrays
 */
class ArrayOfGlobalSanctionsIssuer extends AbstractStructArrayBase
{
    /**
     * The GlobalSanctionsIssuer
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSanctionsIssuer[]
     */
    protected ?array $GlobalSanctionsIssuer = null;
    /**
     * Constructor method for ArrayOfGlobalSanctionsIssuer
     * @uses ArrayOfGlobalSanctionsIssuer::setGlobalSanctionsIssuer()
     * @param \ID3Global\Models\GlobalSanctionsIssuer[] $globalSanctionsIssuer
     */
    public function __construct(?array $globalSanctionsIssuer = null)
    {
        $this
            ->setGlobalSanctionsIssuer($globalSanctionsIssuer);
    }
    /**
     * Get GlobalSanctionsIssuer value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSanctionsIssuer[]
     */
    public function getGlobalSanctionsIssuer(): ?array
    {
        return isset($this->GlobalSanctionsIssuer) ? $this->GlobalSanctionsIssuer : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSanctionsIssuer method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSanctionsIssuer method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSanctionsIssuerForArrayConstraintsFromSetGlobalSanctionsIssuer(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSanctionsIssuerGlobalSanctionsIssuerItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSanctionsIssuerGlobalSanctionsIssuerItem instanceof \ID3Global\Models\GlobalSanctionsIssuer) {
                $invalidValues[] = is_object($arrayOfGlobalSanctionsIssuerGlobalSanctionsIssuerItem) ? get_class($arrayOfGlobalSanctionsIssuerGlobalSanctionsIssuerItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSanctionsIssuerGlobalSanctionsIssuerItem), var_export($arrayOfGlobalSanctionsIssuerGlobalSanctionsIssuerItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSanctionsIssuer property can only contain items of type \ID3Global\Models\GlobalSanctionsIssuer, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSanctionsIssuer value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSanctionsIssuer[] $globalSanctionsIssuer
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer
     */
    public function setGlobalSanctionsIssuer(?array $globalSanctionsIssuer = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSanctionsIssuerArrayErrorMessage = self::validateGlobalSanctionsIssuerForArrayConstraintsFromSetGlobalSanctionsIssuer($globalSanctionsIssuer))) {
            throw new InvalidArgumentException($globalSanctionsIssuerArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSanctionsIssuer) || (is_array($globalSanctionsIssuer) && empty($globalSanctionsIssuer))) {
            unset($this->GlobalSanctionsIssuer);
        } else {
            $this->GlobalSanctionsIssuer = $globalSanctionsIssuer;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSanctionsIssuer|null
     */
    public function current(): ?\ID3Global\Models\GlobalSanctionsIssuer
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSanctionsIssuer|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSanctionsIssuer
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSanctionsIssuer|null
     */
    public function first(): ?\ID3Global\Models\GlobalSanctionsIssuer
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSanctionsIssuer|null
     */
    public function last(): ?\ID3Global\Models\GlobalSanctionsIssuer
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSanctionsIssuer|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSanctionsIssuer
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSanctionsIssuer $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSanctionsIssuer) {
            throw new InvalidArgumentException(sprintf('The GlobalSanctionsIssuer property can only contain items of type \ID3Global\Models\GlobalSanctionsIssuer, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSanctionsIssuer
     */
    public function getAttributeName(): string
    {
        return 'GlobalSanctionsIssuer';
    }
}
