<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalPEPTier Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q107:ArrayOfGlobalPEPTier
 * @subpackage Arrays
 */
class ArrayOfGlobalPEPTier extends AbstractStructArrayBase
{
    /**
     * The GlobalPEPTier
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalPEPTier[]
     */
    protected ?array $GlobalPEPTier = null;
    /**
     * Constructor method for ArrayOfGlobalPEPTier
     * @uses ArrayOfGlobalPEPTier::setGlobalPEPTier()
     * @param \ID3Global\Models\GlobalPEPTier[] $globalPEPTier
     */
    public function __construct(?array $globalPEPTier = null)
    {
        $this
            ->setGlobalPEPTier($globalPEPTier);
    }
    /**
     * Get GlobalPEPTier value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalPEPTier[]
     */
    public function getGlobalPEPTier(): ?array
    {
        return isset($this->GlobalPEPTier) ? $this->GlobalPEPTier : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalPEPTier method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalPEPTier method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalPEPTierForArrayConstraintsFromSetGlobalPEPTier(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalPEPTierGlobalPEPTierItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalPEPTierGlobalPEPTierItem instanceof \ID3Global\Models\GlobalPEPTier) {
                $invalidValues[] = is_object($arrayOfGlobalPEPTierGlobalPEPTierItem) ? get_class($arrayOfGlobalPEPTierGlobalPEPTierItem) : sprintf('%s(%s)', gettype($arrayOfGlobalPEPTierGlobalPEPTierItem), var_export($arrayOfGlobalPEPTierGlobalPEPTierItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalPEPTier property can only contain items of type \ID3Global\Models\GlobalPEPTier, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalPEPTier value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalPEPTier[] $globalPEPTier
     * @return \ID3Global\Arrays\ArrayOfGlobalPEPTier
     */
    public function setGlobalPEPTier(?array $globalPEPTier = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalPEPTierArrayErrorMessage = self::validateGlobalPEPTierForArrayConstraintsFromSetGlobalPEPTier($globalPEPTier))) {
            throw new InvalidArgumentException($globalPEPTierArrayErrorMessage, __LINE__);
        }
        if (is_null($globalPEPTier) || (is_array($globalPEPTier) && empty($globalPEPTier))) {
            unset($this->GlobalPEPTier);
        } else {
            $this->GlobalPEPTier = $globalPEPTier;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalPEPTier|null
     */
    public function current(): ?\ID3Global\Models\GlobalPEPTier
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalPEPTier|null
     */
    public function item($index): ?\ID3Global\Models\GlobalPEPTier
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalPEPTier|null
     */
    public function first(): ?\ID3Global\Models\GlobalPEPTier
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalPEPTier|null
     */
    public function last(): ?\ID3Global\Models\GlobalPEPTier
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalPEPTier|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalPEPTier
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalPEPTier $item
     * @return \ID3Global\Arrays\ArrayOfGlobalPEPTier
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalPEPTier) {
            throw new InvalidArgumentException(sprintf('The GlobalPEPTier property can only contain items of type \ID3Global\Models\GlobalPEPTier, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalPEPTier
     */
    public function getAttributeName(): string
    {
        return 'GlobalPEPTier';
    }
}
