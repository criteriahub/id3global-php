<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseReference Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q770:ArrayOfGlobalCaseReference
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseReference extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseReference
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseReference[]
     */
    protected ?array $GlobalCaseReference = null;
    /**
     * Constructor method for ArrayOfGlobalCaseReference
     * @uses ArrayOfGlobalCaseReference::setGlobalCaseReference()
     * @param \ID3Global\Models\GlobalCaseReference[] $globalCaseReference
     */
    public function __construct(?array $globalCaseReference = null)
    {
        $this
            ->setGlobalCaseReference($globalCaseReference);
    }
    /**
     * Get GlobalCaseReference value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseReference[]
     */
    public function getGlobalCaseReference(): ?array
    {
        return isset($this->GlobalCaseReference) ? $this->GlobalCaseReference : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseReference method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseReference method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseReferenceForArrayConstraintsFromSetGlobalCaseReference(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseReferenceGlobalCaseReferenceItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseReferenceGlobalCaseReferenceItem instanceof \ID3Global\Models\GlobalCaseReference) {
                $invalidValues[] = is_object($arrayOfGlobalCaseReferenceGlobalCaseReferenceItem) ? get_class($arrayOfGlobalCaseReferenceGlobalCaseReferenceItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseReferenceGlobalCaseReferenceItem), var_export($arrayOfGlobalCaseReferenceGlobalCaseReferenceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseReference property can only contain items of type \ID3Global\Models\GlobalCaseReference, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseReference value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseReference[] $globalCaseReference
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseReference
     */
    public function setGlobalCaseReference(?array $globalCaseReference = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseReferenceArrayErrorMessage = self::validateGlobalCaseReferenceForArrayConstraintsFromSetGlobalCaseReference($globalCaseReference))) {
            throw new InvalidArgumentException($globalCaseReferenceArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseReference) || (is_array($globalCaseReference) && empty($globalCaseReference))) {
            unset($this->GlobalCaseReference);
        } else {
            $this->GlobalCaseReference = $globalCaseReference;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseReference|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseReference
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseReference|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseReference
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseReference|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseReference
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseReference|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseReference
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseReference|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseReference
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseReference $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseReference
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseReference) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseReference property can only contain items of type \ID3Global\Models\GlobalCaseReference, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseReference
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseReference';
    }
}
