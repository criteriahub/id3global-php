<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalImageDetails Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q156:ArrayOfGlobalImageDetails
 * @subpackage Arrays
 */
class ArrayOfGlobalImageDetails extends AbstractStructArrayBase
{
    /**
     * The GlobalImageDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalImageDetails[]
     */
    protected ?array $GlobalImageDetails = null;
    /**
     * Constructor method for ArrayOfGlobalImageDetails
     * @uses ArrayOfGlobalImageDetails::setGlobalImageDetails()
     * @param \ID3Global\Models\GlobalImageDetails[] $globalImageDetails
     */
    public function __construct(?array $globalImageDetails = null)
    {
        $this
            ->setGlobalImageDetails($globalImageDetails);
    }
    /**
     * Get GlobalImageDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalImageDetails[]
     */
    public function getGlobalImageDetails(): ?array
    {
        return isset($this->GlobalImageDetails) ? $this->GlobalImageDetails : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalImageDetails method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalImageDetails method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalImageDetailsForArrayConstraintsFromSetGlobalImageDetails(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalImageDetailsGlobalImageDetailsItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalImageDetailsGlobalImageDetailsItem instanceof \ID3Global\Models\GlobalImageDetails) {
                $invalidValues[] = is_object($arrayOfGlobalImageDetailsGlobalImageDetailsItem) ? get_class($arrayOfGlobalImageDetailsGlobalImageDetailsItem) : sprintf('%s(%s)', gettype($arrayOfGlobalImageDetailsGlobalImageDetailsItem), var_export($arrayOfGlobalImageDetailsGlobalImageDetailsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalImageDetails property can only contain items of type \ID3Global\Models\GlobalImageDetails, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalImageDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalImageDetails[] $globalImageDetails
     * @return \ID3Global\Arrays\ArrayOfGlobalImageDetails
     */
    public function setGlobalImageDetails(?array $globalImageDetails = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalImageDetailsArrayErrorMessage = self::validateGlobalImageDetailsForArrayConstraintsFromSetGlobalImageDetails($globalImageDetails))) {
            throw new InvalidArgumentException($globalImageDetailsArrayErrorMessage, __LINE__);
        }
        if (is_null($globalImageDetails) || (is_array($globalImageDetails) && empty($globalImageDetails))) {
            unset($this->GlobalImageDetails);
        } else {
            $this->GlobalImageDetails = $globalImageDetails;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalImageDetails|null
     */
    public function current(): ?\ID3Global\Models\GlobalImageDetails
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalImageDetails|null
     */
    public function item($index): ?\ID3Global\Models\GlobalImageDetails
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalImageDetails|null
     */
    public function first(): ?\ID3Global\Models\GlobalImageDetails
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalImageDetails|null
     */
    public function last(): ?\ID3Global\Models\GlobalImageDetails
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalImageDetails|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalImageDetails
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalImageDetails $item
     * @return \ID3Global\Arrays\ArrayOfGlobalImageDetails
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalImageDetails) {
            throw new InvalidArgumentException(sprintf('The GlobalImageDetails property can only contain items of type \ID3Global\Models\GlobalImageDetails, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalImageDetails
     */
    public function getAttributeName(): string
    {
        return 'GlobalImageDetails';
    }
}
