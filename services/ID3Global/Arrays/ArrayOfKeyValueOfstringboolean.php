<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfKeyValueOfstringboolean Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfKeyValueOfstringboolean
 * @subpackage Arrays
 */
class ArrayOfKeyValueOfstringboolean extends AbstractStructArrayBase
{
    /**
     * The KeyValueOfstringboolean
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \ID3Global\Models\KeyValueOfstringboolean[]
     */
    protected ?array $KeyValueOfstringboolean = null;
    /**
     * Constructor method for ArrayOfKeyValueOfstringboolean
     * @uses ArrayOfKeyValueOfstringboolean::setKeyValueOfstringboolean()
     * @param \ID3Global\Models\KeyValueOfstringboolean[] $keyValueOfstringboolean
     */
    public function __construct(?array $keyValueOfstringboolean = null)
    {
        $this
            ->setKeyValueOfstringboolean($keyValueOfstringboolean);
    }
    /**
     * Get KeyValueOfstringboolean value
     * @return \ID3Global\Models\KeyValueOfstringboolean[]
     */
    public function getKeyValueOfstringboolean(): ?array
    {
        return $this->KeyValueOfstringboolean;
    }
    /**
     * This method is responsible for validating the values passed to the setKeyValueOfstringboolean method
     * This method is willingly generated in order to preserve the one-line inline validation within the setKeyValueOfstringboolean method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateKeyValueOfstringbooleanForArrayConstraintsFromSetKeyValueOfstringboolean(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfKeyValueOfstringbooleanKeyValueOfstringbooleanItem) {
            // validation for constraint: itemType
            if (!$arrayOfKeyValueOfstringbooleanKeyValueOfstringbooleanItem instanceof \ID3Global\Models\KeyValueOfstringboolean) {
                $invalidValues[] = is_object($arrayOfKeyValueOfstringbooleanKeyValueOfstringbooleanItem) ? get_class($arrayOfKeyValueOfstringbooleanKeyValueOfstringbooleanItem) : sprintf('%s(%s)', gettype($arrayOfKeyValueOfstringbooleanKeyValueOfstringbooleanItem), var_export($arrayOfKeyValueOfstringbooleanKeyValueOfstringbooleanItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The KeyValueOfstringboolean property can only contain items of type \ID3Global\Models\KeyValueOfstringboolean, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set KeyValueOfstringboolean value
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\KeyValueOfstringboolean[] $keyValueOfstringboolean
     * @return \ID3Global\Arrays\ArrayOfKeyValueOfstringboolean
     */
    public function setKeyValueOfstringboolean(?array $keyValueOfstringboolean = null): self
    {
        // validation for constraint: array
        if ('' !== ($keyValueOfstringbooleanArrayErrorMessage = self::validateKeyValueOfstringbooleanForArrayConstraintsFromSetKeyValueOfstringboolean($keyValueOfstringboolean))) {
            throw new InvalidArgumentException($keyValueOfstringbooleanArrayErrorMessage, __LINE__);
        }
        $this->KeyValueOfstringboolean = $keyValueOfstringboolean;
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\KeyValueOfstringboolean|null
     */
    public function current(): ?\ID3Global\Models\KeyValueOfstringboolean
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\KeyValueOfstringboolean|null
     */
    public function item($index): ?\ID3Global\Models\KeyValueOfstringboolean
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\KeyValueOfstringboolean|null
     */
    public function first(): ?\ID3Global\Models\KeyValueOfstringboolean
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\KeyValueOfstringboolean|null
     */
    public function last(): ?\ID3Global\Models\KeyValueOfstringboolean
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\KeyValueOfstringboolean|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\KeyValueOfstringboolean
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\KeyValueOfstringboolean $item
     * @return \ID3Global\Arrays\ArrayOfKeyValueOfstringboolean
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\KeyValueOfstringboolean) {
            throw new InvalidArgumentException(sprintf('The KeyValueOfstringboolean property can only contain items of type \ID3Global\Models\KeyValueOfstringboolean, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string KeyValueOfstringboolean
     */
    public function getAttributeName(): string
    {
        return 'KeyValueOfstringboolean';
    }
}
