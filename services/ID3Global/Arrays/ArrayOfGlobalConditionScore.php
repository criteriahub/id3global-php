<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalConditionScore Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q227:ArrayOfGlobalConditionScore
 * @subpackage Arrays
 */
class ArrayOfGlobalConditionScore extends AbstractStructArrayBase
{
    /**
     * The GlobalConditionScore
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalConditionScore[]
     */
    protected ?array $GlobalConditionScore = null;
    /**
     * Constructor method for ArrayOfGlobalConditionScore
     * @uses ArrayOfGlobalConditionScore::setGlobalConditionScore()
     * @param \ID3Global\Models\GlobalConditionScore[] $globalConditionScore
     */
    public function __construct(?array $globalConditionScore = null)
    {
        $this
            ->setGlobalConditionScore($globalConditionScore);
    }
    /**
     * Get GlobalConditionScore value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalConditionScore[]
     */
    public function getGlobalConditionScore(): ?array
    {
        return isset($this->GlobalConditionScore) ? $this->GlobalConditionScore : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalConditionScore method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalConditionScore method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalConditionScoreForArrayConstraintsFromSetGlobalConditionScore(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalConditionScoreGlobalConditionScoreItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalConditionScoreGlobalConditionScoreItem instanceof \ID3Global\Models\GlobalConditionScore) {
                $invalidValues[] = is_object($arrayOfGlobalConditionScoreGlobalConditionScoreItem) ? get_class($arrayOfGlobalConditionScoreGlobalConditionScoreItem) : sprintf('%s(%s)', gettype($arrayOfGlobalConditionScoreGlobalConditionScoreItem), var_export($arrayOfGlobalConditionScoreGlobalConditionScoreItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalConditionScore property can only contain items of type \ID3Global\Models\GlobalConditionScore, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalConditionScore value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalConditionScore[] $globalConditionScore
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionScore
     */
    public function setGlobalConditionScore(?array $globalConditionScore = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalConditionScoreArrayErrorMessage = self::validateGlobalConditionScoreForArrayConstraintsFromSetGlobalConditionScore($globalConditionScore))) {
            throw new InvalidArgumentException($globalConditionScoreArrayErrorMessage, __LINE__);
        }
        if (is_null($globalConditionScore) || (is_array($globalConditionScore) && empty($globalConditionScore))) {
            unset($this->GlobalConditionScore);
        } else {
            $this->GlobalConditionScore = $globalConditionScore;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalConditionScore|null
     */
    public function current(): ?\ID3Global\Models\GlobalConditionScore
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalConditionScore|null
     */
    public function item($index): ?\ID3Global\Models\GlobalConditionScore
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalConditionScore|null
     */
    public function first(): ?\ID3Global\Models\GlobalConditionScore
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalConditionScore|null
     */
    public function last(): ?\ID3Global\Models\GlobalConditionScore
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalConditionScore|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalConditionScore
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalConditionScore $item
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionScore
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalConditionScore) {
            throw new InvalidArgumentException(sprintf('The GlobalConditionScore property can only contain items of type \ID3Global\Models\GlobalConditionScore, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalConditionScore
     */
    public function getAttributeName(): string
    {
        return 'GlobalConditionScore';
    }
}
