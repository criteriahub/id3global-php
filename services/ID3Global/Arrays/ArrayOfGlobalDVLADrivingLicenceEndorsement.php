<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalDVLADrivingLicenceEndorsement Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q103:ArrayOfGlobalDVLADrivingLicenceEndorsement
 * @subpackage Arrays
 */
class ArrayOfGlobalDVLADrivingLicenceEndorsement extends AbstractStructArrayBase
{
    /**
     * The GlobalDVLADrivingLicenceEndorsement
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement[]
     */
    protected ?array $GlobalDVLADrivingLicenceEndorsement = null;
    /**
     * Constructor method for ArrayOfGlobalDVLADrivingLicenceEndorsement
     * @uses ArrayOfGlobalDVLADrivingLicenceEndorsement::setGlobalDVLADrivingLicenceEndorsement()
     * @param \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement[] $globalDVLADrivingLicenceEndorsement
     */
    public function __construct(?array $globalDVLADrivingLicenceEndorsement = null)
    {
        $this
            ->setGlobalDVLADrivingLicenceEndorsement($globalDVLADrivingLicenceEndorsement);
    }
    /**
     * Get GlobalDVLADrivingLicenceEndorsement value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement[]
     */
    public function getGlobalDVLADrivingLicenceEndorsement(): ?array
    {
        return isset($this->GlobalDVLADrivingLicenceEndorsement) ? $this->GlobalDVLADrivingLicenceEndorsement : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalDVLADrivingLicenceEndorsement method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalDVLADrivingLicenceEndorsement method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalDVLADrivingLicenceEndorsementForArrayConstraintsFromSetGlobalDVLADrivingLicenceEndorsement(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalDVLADrivingLicenceEndorsementGlobalDVLADrivingLicenceEndorsementItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalDVLADrivingLicenceEndorsementGlobalDVLADrivingLicenceEndorsementItem instanceof \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement) {
                $invalidValues[] = is_object($arrayOfGlobalDVLADrivingLicenceEndorsementGlobalDVLADrivingLicenceEndorsementItem) ? get_class($arrayOfGlobalDVLADrivingLicenceEndorsementGlobalDVLADrivingLicenceEndorsementItem) : sprintf('%s(%s)', gettype($arrayOfGlobalDVLADrivingLicenceEndorsementGlobalDVLADrivingLicenceEndorsementItem), var_export($arrayOfGlobalDVLADrivingLicenceEndorsementGlobalDVLADrivingLicenceEndorsementItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalDVLADrivingLicenceEndorsement property can only contain items of type \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalDVLADrivingLicenceEndorsement value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement[] $globalDVLADrivingLicenceEndorsement
     * @return \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceEndorsement
     */
    public function setGlobalDVLADrivingLicenceEndorsement(?array $globalDVLADrivingLicenceEndorsement = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalDVLADrivingLicenceEndorsementArrayErrorMessage = self::validateGlobalDVLADrivingLicenceEndorsementForArrayConstraintsFromSetGlobalDVLADrivingLicenceEndorsement($globalDVLADrivingLicenceEndorsement))) {
            throw new InvalidArgumentException($globalDVLADrivingLicenceEndorsementArrayErrorMessage, __LINE__);
        }
        if (is_null($globalDVLADrivingLicenceEndorsement) || (is_array($globalDVLADrivingLicenceEndorsement) && empty($globalDVLADrivingLicenceEndorsement))) {
            unset($this->GlobalDVLADrivingLicenceEndorsement);
        } else {
            $this->GlobalDVLADrivingLicenceEndorsement = $globalDVLADrivingLicenceEndorsement;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement|null
     */
    public function current(): ?\ID3Global\Models\GlobalDVLADrivingLicenceEndorsement
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement|null
     */
    public function item($index): ?\ID3Global\Models\GlobalDVLADrivingLicenceEndorsement
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement|null
     */
    public function first(): ?\ID3Global\Models\GlobalDVLADrivingLicenceEndorsement
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement|null
     */
    public function last(): ?\ID3Global\Models\GlobalDVLADrivingLicenceEndorsement
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalDVLADrivingLicenceEndorsement
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement $item
     * @return \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceEndorsement
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement) {
            throw new InvalidArgumentException(sprintf('The GlobalDVLADrivingLicenceEndorsement property can only contain items of type \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalDVLADrivingLicenceEndorsement
     */
    public function getAttributeName(): string
    {
        return 'GlobalDVLADrivingLicenceEndorsement';
    }
}
