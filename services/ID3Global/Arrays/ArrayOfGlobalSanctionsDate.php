<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSanctionsDate Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q482:ArrayOfGlobalSanctionsDate
 * @subpackage Arrays
 */
class ArrayOfGlobalSanctionsDate extends AbstractStructArrayBase
{
    /**
     * The GlobalSanctionsDate
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSanctionsDate[]
     */
    protected ?array $GlobalSanctionsDate = null;
    /**
     * Constructor method for ArrayOfGlobalSanctionsDate
     * @uses ArrayOfGlobalSanctionsDate::setGlobalSanctionsDate()
     * @param \ID3Global\Models\GlobalSanctionsDate[] $globalSanctionsDate
     */
    public function __construct(?array $globalSanctionsDate = null)
    {
        $this
            ->setGlobalSanctionsDate($globalSanctionsDate);
    }
    /**
     * Get GlobalSanctionsDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSanctionsDate[]
     */
    public function getGlobalSanctionsDate(): ?array
    {
        return isset($this->GlobalSanctionsDate) ? $this->GlobalSanctionsDate : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSanctionsDate method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSanctionsDate method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSanctionsDateForArrayConstraintsFromSetGlobalSanctionsDate(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSanctionsDateGlobalSanctionsDateItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSanctionsDateGlobalSanctionsDateItem instanceof \ID3Global\Models\GlobalSanctionsDate) {
                $invalidValues[] = is_object($arrayOfGlobalSanctionsDateGlobalSanctionsDateItem) ? get_class($arrayOfGlobalSanctionsDateGlobalSanctionsDateItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSanctionsDateGlobalSanctionsDateItem), var_export($arrayOfGlobalSanctionsDateGlobalSanctionsDateItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSanctionsDate property can only contain items of type \ID3Global\Models\GlobalSanctionsDate, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSanctionsDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSanctionsDate[] $globalSanctionsDate
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsDate
     */
    public function setGlobalSanctionsDate(?array $globalSanctionsDate = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSanctionsDateArrayErrorMessage = self::validateGlobalSanctionsDateForArrayConstraintsFromSetGlobalSanctionsDate($globalSanctionsDate))) {
            throw new InvalidArgumentException($globalSanctionsDateArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSanctionsDate) || (is_array($globalSanctionsDate) && empty($globalSanctionsDate))) {
            unset($this->GlobalSanctionsDate);
        } else {
            $this->GlobalSanctionsDate = $globalSanctionsDate;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSanctionsDate|null
     */
    public function current(): ?\ID3Global\Models\GlobalSanctionsDate
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSanctionsDate|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSanctionsDate
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSanctionsDate|null
     */
    public function first(): ?\ID3Global\Models\GlobalSanctionsDate
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSanctionsDate|null
     */
    public function last(): ?\ID3Global\Models\GlobalSanctionsDate
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSanctionsDate|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSanctionsDate
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSanctionsDate $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsDate
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSanctionsDate) {
            throw new InvalidArgumentException(sprintf('The GlobalSanctionsDate property can only contain items of type \ID3Global\Models\GlobalSanctionsDate, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSanctionsDate
     */
    public function getAttributeName(): string
    {
        return 'GlobalSanctionsDate';
    }
}
