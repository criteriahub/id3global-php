<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfdouble Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfdouble
 * @subpackage Arrays
 */
class ArrayOfdouble extends AbstractStructArrayBase
{
    /**
     * The double
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var float[]
     */
    protected ?array $double = null;
    /**
     * Constructor method for ArrayOfdouble
     * @uses ArrayOfdouble::setDouble()
     * @param float[] $double
     */
    public function __construct(?array $double = null)
    {
        $this
            ->setDouble($double);
    }
    /**
     * Get double value
     * @return float[]
     */
    public function getDouble(): ?array
    {
        return $this->double;
    }
    /**
     * This method is responsible for validating the values passed to the setDouble method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDouble method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDoubleForArrayConstraintsFromSetDouble(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfdoubleDoubleItem) {
            // validation for constraint: itemType
            if (!(is_float($arrayOfdoubleDoubleItem) || is_numeric($arrayOfdoubleDoubleItem))) {
                $invalidValues[] = is_object($arrayOfdoubleDoubleItem) ? get_class($arrayOfdoubleDoubleItem) : sprintf('%s(%s)', gettype($arrayOfdoubleDoubleItem), var_export($arrayOfdoubleDoubleItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The double property can only contain items of type float, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set double value
     * @throws InvalidArgumentException
     * @param float[] $double
     * @return \ID3Global\Arrays\ArrayOfdouble
     */
    public function setDouble(?array $double = null): self
    {
        // validation for constraint: array
        if ('' !== ($doubleArrayErrorMessage = self::validateDoubleForArrayConstraintsFromSetDouble($double))) {
            throw new InvalidArgumentException($doubleArrayErrorMessage, __LINE__);
        }
        $this->double = $double;
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return float|null
     */
    public function current(): ?float
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return float|null
     */
    public function item($index): ?float
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return float|null
     */
    public function first(): ?float
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return float|null
     */
    public function last(): ?float
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return float|null
     */
    public function offsetGet($offset): ?float
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string double
     */
    public function getAttributeName(): string
    {
        return 'double';
    }
}
