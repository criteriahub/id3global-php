<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalProfileIDVersion Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q185:ArrayOfGlobalProfileIDVersion
 * @subpackage Arrays
 */
class ArrayOfGlobalProfileIDVersion extends AbstractStructArrayBase
{
    /**
     * The GlobalProfileIDVersion
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileIDVersion[]
     */
    protected ?array $GlobalProfileIDVersion = null;
    /**
     * Constructor method for ArrayOfGlobalProfileIDVersion
     * @uses ArrayOfGlobalProfileIDVersion::setGlobalProfileIDVersion()
     * @param \ID3Global\Models\GlobalProfileIDVersion[] $globalProfileIDVersion
     */
    public function __construct(?array $globalProfileIDVersion = null)
    {
        $this
            ->setGlobalProfileIDVersion($globalProfileIDVersion);
    }
    /**
     * Get GlobalProfileIDVersion value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileIDVersion[]
     */
    public function getGlobalProfileIDVersion(): ?array
    {
        return isset($this->GlobalProfileIDVersion) ? $this->GlobalProfileIDVersion : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalProfileIDVersion method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalProfileIDVersion method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalProfileIDVersionForArrayConstraintsFromSetGlobalProfileIDVersion(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalProfileIDVersionGlobalProfileIDVersionItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalProfileIDVersionGlobalProfileIDVersionItem instanceof \ID3Global\Models\GlobalProfileIDVersion) {
                $invalidValues[] = is_object($arrayOfGlobalProfileIDVersionGlobalProfileIDVersionItem) ? get_class($arrayOfGlobalProfileIDVersionGlobalProfileIDVersionItem) : sprintf('%s(%s)', gettype($arrayOfGlobalProfileIDVersionGlobalProfileIDVersionItem), var_export($arrayOfGlobalProfileIDVersionGlobalProfileIDVersionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalProfileIDVersion property can only contain items of type \ID3Global\Models\GlobalProfileIDVersion, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalProfileIDVersion value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProfileIDVersion[] $globalProfileIDVersion
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileIDVersion
     */
    public function setGlobalProfileIDVersion(?array $globalProfileIDVersion = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalProfileIDVersionArrayErrorMessage = self::validateGlobalProfileIDVersionForArrayConstraintsFromSetGlobalProfileIDVersion($globalProfileIDVersion))) {
            throw new InvalidArgumentException($globalProfileIDVersionArrayErrorMessage, __LINE__);
        }
        if (is_null($globalProfileIDVersion) || (is_array($globalProfileIDVersion) && empty($globalProfileIDVersion))) {
            unset($this->GlobalProfileIDVersion);
        } else {
            $this->GlobalProfileIDVersion = $globalProfileIDVersion;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalProfileIDVersion|null
     */
    public function current(): ?\ID3Global\Models\GlobalProfileIDVersion
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalProfileIDVersion|null
     */
    public function item($index): ?\ID3Global\Models\GlobalProfileIDVersion
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalProfileIDVersion|null
     */
    public function first(): ?\ID3Global\Models\GlobalProfileIDVersion
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalProfileIDVersion|null
     */
    public function last(): ?\ID3Global\Models\GlobalProfileIDVersion
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalProfileIDVersion|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalProfileIDVersion
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProfileIDVersion $item
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileIDVersion
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalProfileIDVersion) {
            throw new InvalidArgumentException(sprintf('The GlobalProfileIDVersion property can only contain items of type \ID3Global\Models\GlobalProfileIDVersion, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalProfileIDVersion
     */
    public function getAttributeName(): string
    {
        return 'GlobalProfileIDVersion';
    }
}
