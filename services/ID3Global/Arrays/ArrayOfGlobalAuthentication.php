<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalAuthentication Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q469:ArrayOfGlobalAuthentication
 * @subpackage Arrays
 */
class ArrayOfGlobalAuthentication extends AbstractStructArrayBase
{
    /**
     * The GlobalAuthentication
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAuthentication[]
     */
    protected ?array $GlobalAuthentication = null;
    /**
     * Constructor method for ArrayOfGlobalAuthentication
     * @uses ArrayOfGlobalAuthentication::setGlobalAuthentication()
     * @param \ID3Global\Models\GlobalAuthentication[] $globalAuthentication
     */
    public function __construct(?array $globalAuthentication = null)
    {
        $this
            ->setGlobalAuthentication($globalAuthentication);
    }
    /**
     * Get GlobalAuthentication value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAuthentication[]
     */
    public function getGlobalAuthentication(): ?array
    {
        return isset($this->GlobalAuthentication) ? $this->GlobalAuthentication : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalAuthentication method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalAuthentication method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalAuthenticationForArrayConstraintsFromSetGlobalAuthentication(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalAuthenticationGlobalAuthenticationItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalAuthenticationGlobalAuthenticationItem instanceof \ID3Global\Models\GlobalAuthentication) {
                $invalidValues[] = is_object($arrayOfGlobalAuthenticationGlobalAuthenticationItem) ? get_class($arrayOfGlobalAuthenticationGlobalAuthenticationItem) : sprintf('%s(%s)', gettype($arrayOfGlobalAuthenticationGlobalAuthenticationItem), var_export($arrayOfGlobalAuthenticationGlobalAuthenticationItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalAuthentication property can only contain items of type \ID3Global\Models\GlobalAuthentication, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalAuthentication value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalAuthentication[] $globalAuthentication
     * @return \ID3Global\Arrays\ArrayOfGlobalAuthentication
     */
    public function setGlobalAuthentication(?array $globalAuthentication = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalAuthenticationArrayErrorMessage = self::validateGlobalAuthenticationForArrayConstraintsFromSetGlobalAuthentication($globalAuthentication))) {
            throw new InvalidArgumentException($globalAuthenticationArrayErrorMessage, __LINE__);
        }
        if (is_null($globalAuthentication) || (is_array($globalAuthentication) && empty($globalAuthentication))) {
            unset($this->GlobalAuthentication);
        } else {
            $this->GlobalAuthentication = $globalAuthentication;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalAuthentication|null
     */
    public function current(): ?\ID3Global\Models\GlobalAuthentication
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalAuthentication|null
     */
    public function item($index): ?\ID3Global\Models\GlobalAuthentication
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalAuthentication|null
     */
    public function first(): ?\ID3Global\Models\GlobalAuthentication
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalAuthentication|null
     */
    public function last(): ?\ID3Global\Models\GlobalAuthentication
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalAuthentication|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalAuthentication
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalAuthentication $item
     * @return \ID3Global\Arrays\ArrayOfGlobalAuthentication
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalAuthentication) {
            throw new InvalidArgumentException(sprintf('The GlobalAuthentication property can only contain items of type \ID3Global\Models\GlobalAuthentication, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalAuthentication
     */
    public function getAttributeName(): string
    {
        return 'GlobalAuthentication';
    }
}
