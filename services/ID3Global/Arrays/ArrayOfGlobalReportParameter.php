<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalReportParameter Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q522:ArrayOfGlobalReportParameter
 * @subpackage Arrays
 */
class ArrayOfGlobalReportParameter extends AbstractStructArrayBase
{
    /**
     * The GlobalReportParameter
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalReportParameter[]
     */
    protected ?array $GlobalReportParameter = null;
    /**
     * Constructor method for ArrayOfGlobalReportParameter
     * @uses ArrayOfGlobalReportParameter::setGlobalReportParameter()
     * @param \ID3Global\Models\GlobalReportParameter[] $globalReportParameter
     */
    public function __construct(?array $globalReportParameter = null)
    {
        $this
            ->setGlobalReportParameter($globalReportParameter);
    }
    /**
     * Get GlobalReportParameter value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalReportParameter[]
     */
    public function getGlobalReportParameter(): ?array
    {
        return isset($this->GlobalReportParameter) ? $this->GlobalReportParameter : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalReportParameter method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalReportParameter method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalReportParameterForArrayConstraintsFromSetGlobalReportParameter(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalReportParameterGlobalReportParameterItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalReportParameterGlobalReportParameterItem instanceof \ID3Global\Models\GlobalReportParameter) {
                $invalidValues[] = is_object($arrayOfGlobalReportParameterGlobalReportParameterItem) ? get_class($arrayOfGlobalReportParameterGlobalReportParameterItem) : sprintf('%s(%s)', gettype($arrayOfGlobalReportParameterGlobalReportParameterItem), var_export($arrayOfGlobalReportParameterGlobalReportParameterItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalReportParameter property can only contain items of type \ID3Global\Models\GlobalReportParameter, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalReportParameter value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalReportParameter[] $globalReportParameter
     * @return \ID3Global\Arrays\ArrayOfGlobalReportParameter
     */
    public function setGlobalReportParameter(?array $globalReportParameter = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalReportParameterArrayErrorMessage = self::validateGlobalReportParameterForArrayConstraintsFromSetGlobalReportParameter($globalReportParameter))) {
            throw new InvalidArgumentException($globalReportParameterArrayErrorMessage, __LINE__);
        }
        if (is_null($globalReportParameter) || (is_array($globalReportParameter) && empty($globalReportParameter))) {
            unset($this->GlobalReportParameter);
        } else {
            $this->GlobalReportParameter = $globalReportParameter;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalReportParameter|null
     */
    public function current(): ?\ID3Global\Models\GlobalReportParameter
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalReportParameter|null
     */
    public function item($index): ?\ID3Global\Models\GlobalReportParameter
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalReportParameter|null
     */
    public function first(): ?\ID3Global\Models\GlobalReportParameter
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalReportParameter|null
     */
    public function last(): ?\ID3Global\Models\GlobalReportParameter
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalReportParameter|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalReportParameter
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalReportParameter $item
     * @return \ID3Global\Arrays\ArrayOfGlobalReportParameter
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalReportParameter) {
            throw new InvalidArgumentException(sprintf('The GlobalReportParameter property can only contain items of type \ID3Global\Models\GlobalReportParameter, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalReportParameter
     */
    public function getAttributeName(): string
    {
        return 'GlobalReportParameter';
    }
}
