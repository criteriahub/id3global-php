<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseProfileVersion Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q728:ArrayOfGlobalCaseProfileVersion
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseProfileVersion extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseProfileVersion
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseProfileVersion[]
     */
    protected ?array $GlobalCaseProfileVersion = null;
    /**
     * Constructor method for ArrayOfGlobalCaseProfileVersion
     * @uses ArrayOfGlobalCaseProfileVersion::setGlobalCaseProfileVersion()
     * @param \ID3Global\Models\GlobalCaseProfileVersion[] $globalCaseProfileVersion
     */
    public function __construct(?array $globalCaseProfileVersion = null)
    {
        $this
            ->setGlobalCaseProfileVersion($globalCaseProfileVersion);
    }
    /**
     * Get GlobalCaseProfileVersion value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseProfileVersion[]
     */
    public function getGlobalCaseProfileVersion(): ?array
    {
        return isset($this->GlobalCaseProfileVersion) ? $this->GlobalCaseProfileVersion : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseProfileVersion method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseProfileVersion method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseProfileVersionForArrayConstraintsFromSetGlobalCaseProfileVersion(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseProfileVersionGlobalCaseProfileVersionItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseProfileVersionGlobalCaseProfileVersionItem instanceof \ID3Global\Models\GlobalCaseProfileVersion) {
                $invalidValues[] = is_object($arrayOfGlobalCaseProfileVersionGlobalCaseProfileVersionItem) ? get_class($arrayOfGlobalCaseProfileVersionGlobalCaseProfileVersionItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseProfileVersionGlobalCaseProfileVersionItem), var_export($arrayOfGlobalCaseProfileVersionGlobalCaseProfileVersionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseProfileVersion property can only contain items of type \ID3Global\Models\GlobalCaseProfileVersion, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseProfileVersion value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseProfileVersion[] $globalCaseProfileVersion
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseProfileVersion
     */
    public function setGlobalCaseProfileVersion(?array $globalCaseProfileVersion = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseProfileVersionArrayErrorMessage = self::validateGlobalCaseProfileVersionForArrayConstraintsFromSetGlobalCaseProfileVersion($globalCaseProfileVersion))) {
            throw new InvalidArgumentException($globalCaseProfileVersionArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseProfileVersion) || (is_array($globalCaseProfileVersion) && empty($globalCaseProfileVersion))) {
            unset($this->GlobalCaseProfileVersion);
        } else {
            $this->GlobalCaseProfileVersion = $globalCaseProfileVersion;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseProfileVersion|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseProfileVersion
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseProfileVersion|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseProfileVersion
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseProfileVersion|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseProfileVersion
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseProfileVersion|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseProfileVersion
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseProfileVersion|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseProfileVersion
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseProfileVersion $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseProfileVersion
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseProfileVersion) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseProfileVersion property can only contain items of type \ID3Global\Models\GlobalCaseProfileVersion, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseProfileVersion
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseProfileVersion';
    }
}
