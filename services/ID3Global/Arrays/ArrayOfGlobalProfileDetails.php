<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalProfileDetails Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q294:ArrayOfGlobalProfileDetails
 * @subpackage Arrays
 */
class ArrayOfGlobalProfileDetails extends AbstractStructArrayBase
{
    /**
     * The GlobalProfileDetails
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileDetails[]
     */
    protected ?array $GlobalProfileDetails = null;
    /**
     * Constructor method for ArrayOfGlobalProfileDetails
     * @uses ArrayOfGlobalProfileDetails::setGlobalProfileDetails()
     * @param \ID3Global\Models\GlobalProfileDetails[] $globalProfileDetails
     */
    public function __construct(?array $globalProfileDetails = null)
    {
        $this
            ->setGlobalProfileDetails($globalProfileDetails);
    }
    /**
     * Get GlobalProfileDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileDetails[]
     */
    public function getGlobalProfileDetails(): ?array
    {
        return isset($this->GlobalProfileDetails) ? $this->GlobalProfileDetails : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalProfileDetails method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalProfileDetails method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalProfileDetailsForArrayConstraintsFromSetGlobalProfileDetails(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalProfileDetailsGlobalProfileDetailsItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalProfileDetailsGlobalProfileDetailsItem instanceof \ID3Global\Models\GlobalProfileDetails) {
                $invalidValues[] = is_object($arrayOfGlobalProfileDetailsGlobalProfileDetailsItem) ? get_class($arrayOfGlobalProfileDetailsGlobalProfileDetailsItem) : sprintf('%s(%s)', gettype($arrayOfGlobalProfileDetailsGlobalProfileDetailsItem), var_export($arrayOfGlobalProfileDetailsGlobalProfileDetailsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalProfileDetails property can only contain items of type \ID3Global\Models\GlobalProfileDetails, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalProfileDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProfileDetails[] $globalProfileDetails
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileDetails
     */
    public function setGlobalProfileDetails(?array $globalProfileDetails = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalProfileDetailsArrayErrorMessage = self::validateGlobalProfileDetailsForArrayConstraintsFromSetGlobalProfileDetails($globalProfileDetails))) {
            throw new InvalidArgumentException($globalProfileDetailsArrayErrorMessage, __LINE__);
        }
        if (is_null($globalProfileDetails) || (is_array($globalProfileDetails) && empty($globalProfileDetails))) {
            unset($this->GlobalProfileDetails);
        } else {
            $this->GlobalProfileDetails = $globalProfileDetails;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalProfileDetails|null
     */
    public function current(): ?\ID3Global\Models\GlobalProfileDetails
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalProfileDetails|null
     */
    public function item($index): ?\ID3Global\Models\GlobalProfileDetails
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalProfileDetails|null
     */
    public function first(): ?\ID3Global\Models\GlobalProfileDetails
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalProfileDetails|null
     */
    public function last(): ?\ID3Global\Models\GlobalProfileDetails
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalProfileDetails|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalProfileDetails
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProfileDetails $item
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileDetails
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalProfileDetails) {
            throw new InvalidArgumentException(sprintf('The GlobalProfileDetails property can only contain items of type \ID3Global\Models\GlobalProfileDetails, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalProfileDetails
     */
    public function getAttributeName(): string
    {
        return 'GlobalProfileDetails';
    }
}
