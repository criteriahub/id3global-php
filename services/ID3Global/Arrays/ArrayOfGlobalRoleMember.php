<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalRoleMember Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q602:ArrayOfGlobalRoleMember
 * @subpackage Arrays
 */
class ArrayOfGlobalRoleMember extends AbstractStructArrayBase
{
    /**
     * The GlobalRoleMember
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalRoleMember[]
     */
    protected ?array $GlobalRoleMember = null;
    /**
     * Constructor method for ArrayOfGlobalRoleMember
     * @uses ArrayOfGlobalRoleMember::setGlobalRoleMember()
     * @param \ID3Global\Models\GlobalRoleMember[] $globalRoleMember
     */
    public function __construct(?array $globalRoleMember = null)
    {
        $this
            ->setGlobalRoleMember($globalRoleMember);
    }
    /**
     * Get GlobalRoleMember value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalRoleMember[]
     */
    public function getGlobalRoleMember(): ?array
    {
        return isset($this->GlobalRoleMember) ? $this->GlobalRoleMember : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalRoleMember method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalRoleMember method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalRoleMemberForArrayConstraintsFromSetGlobalRoleMember(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalRoleMemberGlobalRoleMemberItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalRoleMemberGlobalRoleMemberItem instanceof \ID3Global\Models\GlobalRoleMember) {
                $invalidValues[] = is_object($arrayOfGlobalRoleMemberGlobalRoleMemberItem) ? get_class($arrayOfGlobalRoleMemberGlobalRoleMemberItem) : sprintf('%s(%s)', gettype($arrayOfGlobalRoleMemberGlobalRoleMemberItem), var_export($arrayOfGlobalRoleMemberGlobalRoleMemberItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalRoleMember property can only contain items of type \ID3Global\Models\GlobalRoleMember, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalRoleMember value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalRoleMember[] $globalRoleMember
     * @return \ID3Global\Arrays\ArrayOfGlobalRoleMember
     */
    public function setGlobalRoleMember(?array $globalRoleMember = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalRoleMemberArrayErrorMessage = self::validateGlobalRoleMemberForArrayConstraintsFromSetGlobalRoleMember($globalRoleMember))) {
            throw new InvalidArgumentException($globalRoleMemberArrayErrorMessage, __LINE__);
        }
        if (is_null($globalRoleMember) || (is_array($globalRoleMember) && empty($globalRoleMember))) {
            unset($this->GlobalRoleMember);
        } else {
            $this->GlobalRoleMember = $globalRoleMember;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalRoleMember|null
     */
    public function current(): ?\ID3Global\Models\GlobalRoleMember
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalRoleMember|null
     */
    public function item($index): ?\ID3Global\Models\GlobalRoleMember
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalRoleMember|null
     */
    public function first(): ?\ID3Global\Models\GlobalRoleMember
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalRoleMember|null
     */
    public function last(): ?\ID3Global\Models\GlobalRoleMember
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalRoleMember|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalRoleMember
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalRoleMember $item
     * @return \ID3Global\Arrays\ArrayOfGlobalRoleMember
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalRoleMember) {
            throw new InvalidArgumentException(sprintf('The GlobalRoleMember property can only contain items of type \ID3Global\Models\GlobalRoleMember, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalRoleMember
     */
    public function getAttributeName(): string
    {
        return 'GlobalRoleMember';
    }
}
