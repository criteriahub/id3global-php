<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalRole Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q592:ArrayOfGlobalRole
 * @subpackage Arrays
 */
class ArrayOfGlobalRole extends AbstractStructArrayBase
{
    /**
     * The GlobalRole
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalRole[]
     */
    protected ?array $GlobalRole = null;
    /**
     * Constructor method for ArrayOfGlobalRole
     * @uses ArrayOfGlobalRole::setGlobalRole()
     * @param \ID3Global\Models\GlobalRole[] $globalRole
     */
    public function __construct(?array $globalRole = null)
    {
        $this
            ->setGlobalRole($globalRole);
    }
    /**
     * Get GlobalRole value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalRole[]
     */
    public function getGlobalRole(): ?array
    {
        return isset($this->GlobalRole) ? $this->GlobalRole : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalRole method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalRole method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalRoleForArrayConstraintsFromSetGlobalRole(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalRoleGlobalRoleItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalRoleGlobalRoleItem instanceof \ID3Global\Models\GlobalRole) {
                $invalidValues[] = is_object($arrayOfGlobalRoleGlobalRoleItem) ? get_class($arrayOfGlobalRoleGlobalRoleItem) : sprintf('%s(%s)', gettype($arrayOfGlobalRoleGlobalRoleItem), var_export($arrayOfGlobalRoleGlobalRoleItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalRole property can only contain items of type \ID3Global\Models\GlobalRole, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalRole value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalRole[] $globalRole
     * @return \ID3Global\Arrays\ArrayOfGlobalRole
     */
    public function setGlobalRole(?array $globalRole = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalRoleArrayErrorMessage = self::validateGlobalRoleForArrayConstraintsFromSetGlobalRole($globalRole))) {
            throw new InvalidArgumentException($globalRoleArrayErrorMessage, __LINE__);
        }
        if (is_null($globalRole) || (is_array($globalRole) && empty($globalRole))) {
            unset($this->GlobalRole);
        } else {
            $this->GlobalRole = $globalRole;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalRole|null
     */
    public function current(): ?\ID3Global\Models\GlobalRole
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalRole|null
     */
    public function item($index): ?\ID3Global\Models\GlobalRole
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalRole|null
     */
    public function first(): ?\ID3Global\Models\GlobalRole
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalRole|null
     */
    public function last(): ?\ID3Global\Models\GlobalRole
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalRole|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalRole
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalRole $item
     * @return \ID3Global\Arrays\ArrayOfGlobalRole
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalRole) {
            throw new InvalidArgumentException(sprintf('The GlobalRole property can only contain items of type \ID3Global\Models\GlobalRole, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalRole
     */
    public function getAttributeName(): string
    {
        return 'GlobalRole';
    }
}
