<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalAccount Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q583:ArrayOfGlobalAccount
 * @subpackage Arrays
 */
class ArrayOfGlobalAccount extends AbstractStructArrayBase
{
    /**
     * The GlobalAccount
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAccount[]
     */
    protected ?array $GlobalAccount = null;
    /**
     * Constructor method for ArrayOfGlobalAccount
     * @uses ArrayOfGlobalAccount::setGlobalAccount()
     * @param \ID3Global\Models\GlobalAccount[] $globalAccount
     */
    public function __construct(?array $globalAccount = null)
    {
        $this
            ->setGlobalAccount($globalAccount);
    }
    /**
     * Get GlobalAccount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAccount[]
     */
    public function getGlobalAccount(): ?array
    {
        return isset($this->GlobalAccount) ? $this->GlobalAccount : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalAccount method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalAccount method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalAccountForArrayConstraintsFromSetGlobalAccount(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalAccountGlobalAccountItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalAccountGlobalAccountItem instanceof \ID3Global\Models\GlobalAccount) {
                $invalidValues[] = is_object($arrayOfGlobalAccountGlobalAccountItem) ? get_class($arrayOfGlobalAccountGlobalAccountItem) : sprintf('%s(%s)', gettype($arrayOfGlobalAccountGlobalAccountItem), var_export($arrayOfGlobalAccountGlobalAccountItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalAccount property can only contain items of type \ID3Global\Models\GlobalAccount, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalAccount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalAccount[] $globalAccount
     * @return \ID3Global\Arrays\ArrayOfGlobalAccount
     */
    public function setGlobalAccount(?array $globalAccount = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalAccountArrayErrorMessage = self::validateGlobalAccountForArrayConstraintsFromSetGlobalAccount($globalAccount))) {
            throw new InvalidArgumentException($globalAccountArrayErrorMessage, __LINE__);
        }
        if (is_null($globalAccount) || (is_array($globalAccount) && empty($globalAccount))) {
            unset($this->GlobalAccount);
        } else {
            $this->GlobalAccount = $globalAccount;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalAccount|null
     */
    public function current(): ?\ID3Global\Models\GlobalAccount
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalAccount|null
     */
    public function item($index): ?\ID3Global\Models\GlobalAccount
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalAccount|null
     */
    public function first(): ?\ID3Global\Models\GlobalAccount
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalAccount|null
     */
    public function last(): ?\ID3Global\Models\GlobalAccount
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalAccount|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalAccount
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalAccount $item
     * @return \ID3Global\Arrays\ArrayOfGlobalAccount
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalAccount) {
            throw new InvalidArgumentException(sprintf('The GlobalAccount property can only contain items of type \ID3Global\Models\GlobalAccount, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalAccount
     */
    public function getAttributeName(): string
    {
        return 'GlobalAccount';
    }
}
