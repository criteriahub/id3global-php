<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalConditionResultCodes Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q217:ArrayOfGlobalConditionResultCodes
 * @subpackage Arrays
 */
class ArrayOfGlobalConditionResultCodes extends AbstractStructArrayBase
{
    /**
     * The GlobalConditionResultCodes
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalConditionResultCodes[]
     */
    protected ?array $GlobalConditionResultCodes = null;
    /**
     * Constructor method for ArrayOfGlobalConditionResultCodes
     * @uses ArrayOfGlobalConditionResultCodes::setGlobalConditionResultCodes()
     * @param \ID3Global\Models\GlobalConditionResultCodes[] $globalConditionResultCodes
     */
    public function __construct(?array $globalConditionResultCodes = null)
    {
        $this
            ->setGlobalConditionResultCodes($globalConditionResultCodes);
    }
    /**
     * Get GlobalConditionResultCodes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalConditionResultCodes[]
     */
    public function getGlobalConditionResultCodes(): ?array
    {
        return isset($this->GlobalConditionResultCodes) ? $this->GlobalConditionResultCodes : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalConditionResultCodes method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalConditionResultCodes method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalConditionResultCodesForArrayConstraintsFromSetGlobalConditionResultCodes(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalConditionResultCodesGlobalConditionResultCodesItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalConditionResultCodesGlobalConditionResultCodesItem instanceof \ID3Global\Models\GlobalConditionResultCodes) {
                $invalidValues[] = is_object($arrayOfGlobalConditionResultCodesGlobalConditionResultCodesItem) ? get_class($arrayOfGlobalConditionResultCodesGlobalConditionResultCodesItem) : sprintf('%s(%s)', gettype($arrayOfGlobalConditionResultCodesGlobalConditionResultCodesItem), var_export($arrayOfGlobalConditionResultCodesGlobalConditionResultCodesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalConditionResultCodes property can only contain items of type \ID3Global\Models\GlobalConditionResultCodes, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalConditionResultCodes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalConditionResultCodes[] $globalConditionResultCodes
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionResultCodes
     */
    public function setGlobalConditionResultCodes(?array $globalConditionResultCodes = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalConditionResultCodesArrayErrorMessage = self::validateGlobalConditionResultCodesForArrayConstraintsFromSetGlobalConditionResultCodes($globalConditionResultCodes))) {
            throw new InvalidArgumentException($globalConditionResultCodesArrayErrorMessage, __LINE__);
        }
        if (is_null($globalConditionResultCodes) || (is_array($globalConditionResultCodes) && empty($globalConditionResultCodes))) {
            unset($this->GlobalConditionResultCodes);
        } else {
            $this->GlobalConditionResultCodes = $globalConditionResultCodes;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalConditionResultCodes|null
     */
    public function current(): ?\ID3Global\Models\GlobalConditionResultCodes
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalConditionResultCodes|null
     */
    public function item($index): ?\ID3Global\Models\GlobalConditionResultCodes
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalConditionResultCodes|null
     */
    public function first(): ?\ID3Global\Models\GlobalConditionResultCodes
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalConditionResultCodes|null
     */
    public function last(): ?\ID3Global\Models\GlobalConditionResultCodes
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalConditionResultCodes|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalConditionResultCodes
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalConditionResultCodes $item
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionResultCodes
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalConditionResultCodes) {
            throw new InvalidArgumentException(sprintf('The GlobalConditionResultCodes property can only contain items of type \ID3Global\Models\GlobalConditionResultCodes, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalConditionResultCodes
     */
    public function getAttributeName(): string
    {
        return 'GlobalConditionResultCodes';
    }
}
