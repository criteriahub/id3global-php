<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalDVLADrivingLicenceReportEndorsement Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q788:ArrayOfGlobalDVLADrivingLicenceReportEndorsement
 * @subpackage Arrays
 */
class ArrayOfGlobalDVLADrivingLicenceReportEndorsement extends AbstractStructArrayBase
{
    /**
     * The GlobalDVLADrivingLicenceReportEndorsement
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement[]
     */
    protected ?array $GlobalDVLADrivingLicenceReportEndorsement = null;
    /**
     * Constructor method for ArrayOfGlobalDVLADrivingLicenceReportEndorsement
     * @uses ArrayOfGlobalDVLADrivingLicenceReportEndorsement::setGlobalDVLADrivingLicenceReportEndorsement()
     * @param \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement[] $globalDVLADrivingLicenceReportEndorsement
     */
    public function __construct(?array $globalDVLADrivingLicenceReportEndorsement = null)
    {
        $this
            ->setGlobalDVLADrivingLicenceReportEndorsement($globalDVLADrivingLicenceReportEndorsement);
    }
    /**
     * Get GlobalDVLADrivingLicenceReportEndorsement value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement[]
     */
    public function getGlobalDVLADrivingLicenceReportEndorsement(): ?array
    {
        return isset($this->GlobalDVLADrivingLicenceReportEndorsement) ? $this->GlobalDVLADrivingLicenceReportEndorsement : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalDVLADrivingLicenceReportEndorsement method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalDVLADrivingLicenceReportEndorsement method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalDVLADrivingLicenceReportEndorsementForArrayConstraintsFromSetGlobalDVLADrivingLicenceReportEndorsement(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalDVLADrivingLicenceReportEndorsementGlobalDVLADrivingLicenceReportEndorsementItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalDVLADrivingLicenceReportEndorsementGlobalDVLADrivingLicenceReportEndorsementItem instanceof \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement) {
                $invalidValues[] = is_object($arrayOfGlobalDVLADrivingLicenceReportEndorsementGlobalDVLADrivingLicenceReportEndorsementItem) ? get_class($arrayOfGlobalDVLADrivingLicenceReportEndorsementGlobalDVLADrivingLicenceReportEndorsementItem) : sprintf('%s(%s)', gettype($arrayOfGlobalDVLADrivingLicenceReportEndorsementGlobalDVLADrivingLicenceReportEndorsementItem), var_export($arrayOfGlobalDVLADrivingLicenceReportEndorsementGlobalDVLADrivingLicenceReportEndorsementItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalDVLADrivingLicenceReportEndorsement property can only contain items of type \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalDVLADrivingLicenceReportEndorsement value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement[] $globalDVLADrivingLicenceReportEndorsement
     * @return \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportEndorsement
     */
    public function setGlobalDVLADrivingLicenceReportEndorsement(?array $globalDVLADrivingLicenceReportEndorsement = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalDVLADrivingLicenceReportEndorsementArrayErrorMessage = self::validateGlobalDVLADrivingLicenceReportEndorsementForArrayConstraintsFromSetGlobalDVLADrivingLicenceReportEndorsement($globalDVLADrivingLicenceReportEndorsement))) {
            throw new InvalidArgumentException($globalDVLADrivingLicenceReportEndorsementArrayErrorMessage, __LINE__);
        }
        if (is_null($globalDVLADrivingLicenceReportEndorsement) || (is_array($globalDVLADrivingLicenceReportEndorsement) && empty($globalDVLADrivingLicenceReportEndorsement))) {
            unset($this->GlobalDVLADrivingLicenceReportEndorsement);
        } else {
            $this->GlobalDVLADrivingLicenceReportEndorsement = $globalDVLADrivingLicenceReportEndorsement;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement|null
     */
    public function current(): ?\ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement|null
     */
    public function item($index): ?\ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement|null
     */
    public function first(): ?\ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement|null
     */
    public function last(): ?\ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement $item
     * @return \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportEndorsement
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement) {
            throw new InvalidArgumentException(sprintf('The GlobalDVLADrivingLicenceReportEndorsement property can only contain items of type \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalDVLADrivingLicenceReportEndorsement
     */
    public function getAttributeName(): string
    {
        return 'GlobalDVLADrivingLicenceReportEndorsement';
    }
}
