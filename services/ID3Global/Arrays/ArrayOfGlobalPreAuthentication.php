<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalPreAuthentication Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q249:ArrayOfGlobalPreAuthentication
 * @subpackage Arrays
 */
class ArrayOfGlobalPreAuthentication extends AbstractStructArrayBase
{
    /**
     * The GlobalPreAuthentication
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalPreAuthentication[]
     */
    protected ?array $GlobalPreAuthentication = null;
    /**
     * Constructor method for ArrayOfGlobalPreAuthentication
     * @uses ArrayOfGlobalPreAuthentication::setGlobalPreAuthentication()
     * @param \ID3Global\Models\GlobalPreAuthentication[] $globalPreAuthentication
     */
    public function __construct(?array $globalPreAuthentication = null)
    {
        $this
            ->setGlobalPreAuthentication($globalPreAuthentication);
    }
    /**
     * Get GlobalPreAuthentication value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalPreAuthentication[]
     */
    public function getGlobalPreAuthentication(): ?array
    {
        return isset($this->GlobalPreAuthentication) ? $this->GlobalPreAuthentication : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalPreAuthentication method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalPreAuthentication method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalPreAuthenticationForArrayConstraintsFromSetGlobalPreAuthentication(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalPreAuthenticationGlobalPreAuthenticationItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalPreAuthenticationGlobalPreAuthenticationItem instanceof \ID3Global\Models\GlobalPreAuthentication) {
                $invalidValues[] = is_object($arrayOfGlobalPreAuthenticationGlobalPreAuthenticationItem) ? get_class($arrayOfGlobalPreAuthenticationGlobalPreAuthenticationItem) : sprintf('%s(%s)', gettype($arrayOfGlobalPreAuthenticationGlobalPreAuthenticationItem), var_export($arrayOfGlobalPreAuthenticationGlobalPreAuthenticationItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalPreAuthentication property can only contain items of type \ID3Global\Models\GlobalPreAuthentication, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalPreAuthentication value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalPreAuthentication[] $globalPreAuthentication
     * @return \ID3Global\Arrays\ArrayOfGlobalPreAuthentication
     */
    public function setGlobalPreAuthentication(?array $globalPreAuthentication = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalPreAuthenticationArrayErrorMessage = self::validateGlobalPreAuthenticationForArrayConstraintsFromSetGlobalPreAuthentication($globalPreAuthentication))) {
            throw new InvalidArgumentException($globalPreAuthenticationArrayErrorMessage, __LINE__);
        }
        if (is_null($globalPreAuthentication) || (is_array($globalPreAuthentication) && empty($globalPreAuthentication))) {
            unset($this->GlobalPreAuthentication);
        } else {
            $this->GlobalPreAuthentication = $globalPreAuthentication;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalPreAuthentication|null
     */
    public function current(): ?\ID3Global\Models\GlobalPreAuthentication
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalPreAuthentication|null
     */
    public function item($index): ?\ID3Global\Models\GlobalPreAuthentication
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalPreAuthentication|null
     */
    public function first(): ?\ID3Global\Models\GlobalPreAuthentication
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalPreAuthentication|null
     */
    public function last(): ?\ID3Global\Models\GlobalPreAuthentication
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalPreAuthentication|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalPreAuthentication
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalPreAuthentication $item
     * @return \ID3Global\Arrays\ArrayOfGlobalPreAuthentication
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalPreAuthentication) {
            throw new InvalidArgumentException(sprintf('The GlobalPreAuthentication property can only contain items of type \ID3Global\Models\GlobalPreAuthentication, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalPreAuthentication
     */
    public function getAttributeName(): string
    {
        return 'GlobalPreAuthentication';
    }
}
