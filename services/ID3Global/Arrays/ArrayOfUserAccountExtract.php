<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfUserAccountExtract Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q540:ArrayOfUserAccountExtract
 * @subpackage Arrays
 */
class ArrayOfUserAccountExtract extends AbstractStructArrayBase
{
    /**
     * The UserAccountExtract
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\UserAccountExtract[]
     */
    protected ?array $UserAccountExtract = null;
    /**
     * Constructor method for ArrayOfUserAccountExtract
     * @uses ArrayOfUserAccountExtract::setUserAccountExtract()
     * @param \ID3Global\Models\UserAccountExtract[] $userAccountExtract
     */
    public function __construct(?array $userAccountExtract = null)
    {
        $this
            ->setUserAccountExtract($userAccountExtract);
    }
    /**
     * Get UserAccountExtract value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\UserAccountExtract[]
     */
    public function getUserAccountExtract(): ?array
    {
        return isset($this->UserAccountExtract) ? $this->UserAccountExtract : null;
    }
    /**
     * This method is responsible for validating the values passed to the setUserAccountExtract method
     * This method is willingly generated in order to preserve the one-line inline validation within the setUserAccountExtract method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateUserAccountExtractForArrayConstraintsFromSetUserAccountExtract(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfUserAccountExtractUserAccountExtractItem) {
            // validation for constraint: itemType
            if (!$arrayOfUserAccountExtractUserAccountExtractItem instanceof \ID3Global\Models\UserAccountExtract) {
                $invalidValues[] = is_object($arrayOfUserAccountExtractUserAccountExtractItem) ? get_class($arrayOfUserAccountExtractUserAccountExtractItem) : sprintf('%s(%s)', gettype($arrayOfUserAccountExtractUserAccountExtractItem), var_export($arrayOfUserAccountExtractUserAccountExtractItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The UserAccountExtract property can only contain items of type \ID3Global\Models\UserAccountExtract, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set UserAccountExtract value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\UserAccountExtract[] $userAccountExtract
     * @return \ID3Global\Arrays\ArrayOfUserAccountExtract
     */
    public function setUserAccountExtract(?array $userAccountExtract = null): self
    {
        // validation for constraint: array
        if ('' !== ($userAccountExtractArrayErrorMessage = self::validateUserAccountExtractForArrayConstraintsFromSetUserAccountExtract($userAccountExtract))) {
            throw new InvalidArgumentException($userAccountExtractArrayErrorMessage, __LINE__);
        }
        if (is_null($userAccountExtract) || (is_array($userAccountExtract) && empty($userAccountExtract))) {
            unset($this->UserAccountExtract);
        } else {
            $this->UserAccountExtract = $userAccountExtract;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\UserAccountExtract|null
     */
    public function current(): ?\ID3Global\Models\UserAccountExtract
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\UserAccountExtract|null
     */
    public function item($index): ?\ID3Global\Models\UserAccountExtract
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\UserAccountExtract|null
     */
    public function first(): ?\ID3Global\Models\UserAccountExtract
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\UserAccountExtract|null
     */
    public function last(): ?\ID3Global\Models\UserAccountExtract
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\UserAccountExtract|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\UserAccountExtract
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\UserAccountExtract $item
     * @return \ID3Global\Arrays\ArrayOfUserAccountExtract
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\UserAccountExtract) {
            throw new InvalidArgumentException(sprintf('The UserAccountExtract property can only contain items of type \ID3Global\Models\UserAccountExtract, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string UserAccountExtract
     */
    public function getAttributeName(): string
    {
        return 'UserAccountExtract';
    }
}
