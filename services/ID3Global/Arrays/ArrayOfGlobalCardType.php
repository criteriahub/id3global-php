<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCardType Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q182:ArrayOfGlobalCardType
 * @subpackage Arrays
 */
class ArrayOfGlobalCardType extends AbstractStructArrayBase
{
    /**
     * The GlobalCardType
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    protected ?array $GlobalCardType = null;
    /**
     * Constructor method for ArrayOfGlobalCardType
     * @uses ArrayOfGlobalCardType::setGlobalCardType()
     * @param string[] $globalCardType
     */
    public function __construct(?array $globalCardType = null)
    {
        $this
            ->setGlobalCardType($globalCardType);
    }
    /**
     * Get GlobalCardType value
     * @return string[]
     */
    public function getGlobalCardType(): ?array
    {
        return $this->GlobalCardType;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCardType method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCardType method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCardTypeForArrayConstraintsFromSetGlobalCardType(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCardTypeGlobalCardTypeItem) {
            // validation for constraint: enumeration
            if (!\ID3Global\Enums\GlobalCardType::valueIsValid($arrayOfGlobalCardTypeGlobalCardTypeItem)) {
                $invalidValues[] = is_object($arrayOfGlobalCardTypeGlobalCardTypeItem) ? get_class($arrayOfGlobalCardTypeGlobalCardTypeItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCardTypeGlobalCardTypeItem), var_export($arrayOfGlobalCardTypeGlobalCardTypeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalCardType', is_array($invalidValues) ? implode(', ', $invalidValues) : var_export($invalidValues, true), implode(', ', \ID3Global\Enums\GlobalCardType::getValidValues()));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCardType value
     * @uses \ID3Global\Enums\GlobalCardType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalCardType::getValidValues()
     * @throws InvalidArgumentException
     * @param string[] $globalCardType
     * @return \ID3Global\Arrays\ArrayOfGlobalCardType
     */
    public function setGlobalCardType(?array $globalCardType = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCardTypeArrayErrorMessage = self::validateGlobalCardTypeForArrayConstraintsFromSetGlobalCardType($globalCardType))) {
            throw new InvalidArgumentException($globalCardTypeArrayErrorMessage, __LINE__);
        }
        $this->GlobalCardType = $globalCardType;
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return string|null
     */
    public function current(): ?string
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return string|null
     */
    public function item($index): ?string
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return string|null
     */
    public function first(): ?string
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return string|null
     */
    public function last(): ?string
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return string|null
     */
    public function offsetGet($offset): ?string
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param string $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCardType
     */
    public function add($item): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalCardType::valueIsValid($item)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalCardType', is_array($item) ? implode(', ', $item) : var_export($item, true), implode(', ', \ID3Global\Enums\GlobalCardType::getValidValues())), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCardType
     */
    public function getAttributeName(): string
    {
        return 'GlobalCardType';
    }
}
