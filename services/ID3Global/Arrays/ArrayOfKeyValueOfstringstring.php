<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfKeyValueOfstringstring Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfKeyValueOfstringstring
 * @subpackage Arrays
 */
class ArrayOfKeyValueOfstringstring extends AbstractStructArrayBase
{
    /**
     * The KeyValueOfstringstring
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \ID3Global\Models\KeyValueOfstringstring[]
     */
    protected ?array $KeyValueOfstringstring = null;
    /**
     * Constructor method for ArrayOfKeyValueOfstringstring
     * @uses ArrayOfKeyValueOfstringstring::setKeyValueOfstringstring()
     * @param \ID3Global\Models\KeyValueOfstringstring[] $keyValueOfstringstring
     */
    public function __construct(?array $keyValueOfstringstring = null)
    {
        $this
            ->setKeyValueOfstringstring($keyValueOfstringstring);
    }
    /**
     * Get KeyValueOfstringstring value
     * @return \ID3Global\Models\KeyValueOfstringstring[]
     */
    public function getKeyValueOfstringstring(): ?array
    {
        return $this->KeyValueOfstringstring;
    }
    /**
     * This method is responsible for validating the values passed to the setKeyValueOfstringstring method
     * This method is willingly generated in order to preserve the one-line inline validation within the setKeyValueOfstringstring method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateKeyValueOfstringstringForArrayConstraintsFromSetKeyValueOfstringstring(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfKeyValueOfstringstringKeyValueOfstringstringItem) {
            // validation for constraint: itemType
            if (!$arrayOfKeyValueOfstringstringKeyValueOfstringstringItem instanceof \ID3Global\Models\KeyValueOfstringstring) {
                $invalidValues[] = is_object($arrayOfKeyValueOfstringstringKeyValueOfstringstringItem) ? get_class($arrayOfKeyValueOfstringstringKeyValueOfstringstringItem) : sprintf('%s(%s)', gettype($arrayOfKeyValueOfstringstringKeyValueOfstringstringItem), var_export($arrayOfKeyValueOfstringstringKeyValueOfstringstringItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The KeyValueOfstringstring property can only contain items of type \ID3Global\Models\KeyValueOfstringstring, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set KeyValueOfstringstring value
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\KeyValueOfstringstring[] $keyValueOfstringstring
     * @return \ID3Global\Arrays\ArrayOfKeyValueOfstringstring
     */
    public function setKeyValueOfstringstring(?array $keyValueOfstringstring = null): self
    {
        // validation for constraint: array
        if ('' !== ($keyValueOfstringstringArrayErrorMessage = self::validateKeyValueOfstringstringForArrayConstraintsFromSetKeyValueOfstringstring($keyValueOfstringstring))) {
            throw new InvalidArgumentException($keyValueOfstringstringArrayErrorMessage, __LINE__);
        }
        $this->KeyValueOfstringstring = $keyValueOfstringstring;
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\KeyValueOfstringstring|null
     */
    public function current(): ?\ID3Global\Models\KeyValueOfstringstring
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\KeyValueOfstringstring|null
     */
    public function item($index): ?\ID3Global\Models\KeyValueOfstringstring
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\KeyValueOfstringstring|null
     */
    public function first(): ?\ID3Global\Models\KeyValueOfstringstring
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\KeyValueOfstringstring|null
     */
    public function last(): ?\ID3Global\Models\KeyValueOfstringstring
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\KeyValueOfstringstring|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\KeyValueOfstringstring
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\KeyValueOfstringstring $item
     * @return \ID3Global\Arrays\ArrayOfKeyValueOfstringstring
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\KeyValueOfstringstring) {
            throw new InvalidArgumentException(sprintf('The KeyValueOfstringstring property can only contain items of type \ID3Global\Models\KeyValueOfstringstring, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string KeyValueOfstringstring
     */
    public function getAttributeName(): string
    {
        return 'KeyValueOfstringstring';
    }
}
