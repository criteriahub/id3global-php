<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSupportedField Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q193:ArrayOfGlobalSupportedField
 * @subpackage Arrays
 */
class ArrayOfGlobalSupportedField extends AbstractStructArrayBase
{
    /**
     * The GlobalSupportedField
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupportedField[]
     */
    protected ?array $GlobalSupportedField = null;
    /**
     * Constructor method for ArrayOfGlobalSupportedField
     * @uses ArrayOfGlobalSupportedField::setGlobalSupportedField()
     * @param \ID3Global\Models\GlobalSupportedField[] $globalSupportedField
     */
    public function __construct(?array $globalSupportedField = null)
    {
        $this
            ->setGlobalSupportedField($globalSupportedField);
    }
    /**
     * Get GlobalSupportedField value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupportedField[]
     */
    public function getGlobalSupportedField(): ?array
    {
        return isset($this->GlobalSupportedField) ? $this->GlobalSupportedField : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSupportedField method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSupportedField method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSupportedFieldForArrayConstraintsFromSetGlobalSupportedField(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSupportedFieldGlobalSupportedFieldItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSupportedFieldGlobalSupportedFieldItem instanceof \ID3Global\Models\GlobalSupportedField) {
                $invalidValues[] = is_object($arrayOfGlobalSupportedFieldGlobalSupportedFieldItem) ? get_class($arrayOfGlobalSupportedFieldGlobalSupportedFieldItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSupportedFieldGlobalSupportedFieldItem), var_export($arrayOfGlobalSupportedFieldGlobalSupportedFieldItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSupportedField property can only contain items of type \ID3Global\Models\GlobalSupportedField, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSupportedField value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupportedField[] $globalSupportedField
     * @return \ID3Global\Arrays\ArrayOfGlobalSupportedField
     */
    public function setGlobalSupportedField(?array $globalSupportedField = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSupportedFieldArrayErrorMessage = self::validateGlobalSupportedFieldForArrayConstraintsFromSetGlobalSupportedField($globalSupportedField))) {
            throw new InvalidArgumentException($globalSupportedFieldArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSupportedField) || (is_array($globalSupportedField) && empty($globalSupportedField))) {
            unset($this->GlobalSupportedField);
        } else {
            $this->GlobalSupportedField = $globalSupportedField;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSupportedField|null
     */
    public function current(): ?\ID3Global\Models\GlobalSupportedField
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSupportedField|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSupportedField
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSupportedField|null
     */
    public function first(): ?\ID3Global\Models\GlobalSupportedField
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSupportedField|null
     */
    public function last(): ?\ID3Global\Models\GlobalSupportedField
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSupportedField|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSupportedField
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupportedField $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSupportedField
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSupportedField) {
            throw new InvalidArgumentException(sprintf('The GlobalSupportedField property can only contain items of type \ID3Global\Models\GlobalSupportedField, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSupportedField
     */
    public function getAttributeName(): string
    {
        return 'GlobalSupportedField';
    }
}
