<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalProperty Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q235:ArrayOfGlobalProperty
 * @subpackage Arrays
 */
class ArrayOfGlobalProperty extends AbstractStructArrayBase
{
    /**
     * The GlobalProperty
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProperty[]
     */
    protected ?array $GlobalProperty = null;
    /**
     * Constructor method for ArrayOfGlobalProperty
     * @uses ArrayOfGlobalProperty::setGlobalProperty()
     * @param \ID3Global\Models\GlobalProperty[] $globalProperty
     */
    public function __construct(?array $globalProperty = null)
    {
        $this
            ->setGlobalProperty($globalProperty);
    }
    /**
     * Get GlobalProperty value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProperty[]
     */
    public function getGlobalProperty(): ?array
    {
        return isset($this->GlobalProperty) ? $this->GlobalProperty : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalProperty method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalProperty method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalPropertyForArrayConstraintsFromSetGlobalProperty(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalPropertyGlobalPropertyItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalPropertyGlobalPropertyItem instanceof \ID3Global\Models\GlobalProperty) {
                $invalidValues[] = is_object($arrayOfGlobalPropertyGlobalPropertyItem) ? get_class($arrayOfGlobalPropertyGlobalPropertyItem) : sprintf('%s(%s)', gettype($arrayOfGlobalPropertyGlobalPropertyItem), var_export($arrayOfGlobalPropertyGlobalPropertyItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalProperty property can only contain items of type \ID3Global\Models\GlobalProperty, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalProperty value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProperty[] $globalProperty
     * @return \ID3Global\Arrays\ArrayOfGlobalProperty
     */
    public function setGlobalProperty(?array $globalProperty = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalPropertyArrayErrorMessage = self::validateGlobalPropertyForArrayConstraintsFromSetGlobalProperty($globalProperty))) {
            throw new InvalidArgumentException($globalPropertyArrayErrorMessage, __LINE__);
        }
        if (is_null($globalProperty) || (is_array($globalProperty) && empty($globalProperty))) {
            unset($this->GlobalProperty);
        } else {
            $this->GlobalProperty = $globalProperty;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalProperty|null
     */
    public function current(): ?\ID3Global\Models\GlobalProperty
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalProperty|null
     */
    public function item($index): ?\ID3Global\Models\GlobalProperty
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalProperty|null
     */
    public function first(): ?\ID3Global\Models\GlobalProperty
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalProperty|null
     */
    public function last(): ?\ID3Global\Models\GlobalProperty
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalProperty|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalProperty
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalProperty $item
     * @return \ID3Global\Arrays\ArrayOfGlobalProperty
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalProperty) {
            throw new InvalidArgumentException(sprintf('The GlobalProperty property can only contain items of type \ID3Global\Models\GlobalProperty, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalProperty
     */
    public function getAttributeName(): string
    {
        return 'GlobalProperty';
    }
}
