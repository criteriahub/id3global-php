<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseDocumentData Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q845:ArrayOfGlobalCaseDocumentData
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseDocumentData extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseDocumentData
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseDocumentData[]
     */
    protected ?array $GlobalCaseDocumentData = null;
    /**
     * Constructor method for ArrayOfGlobalCaseDocumentData
     * @uses ArrayOfGlobalCaseDocumentData::setGlobalCaseDocumentData()
     * @param \ID3Global\Models\GlobalCaseDocumentData[] $globalCaseDocumentData
     */
    public function __construct(?array $globalCaseDocumentData = null)
    {
        $this
            ->setGlobalCaseDocumentData($globalCaseDocumentData);
    }
    /**
     * Get GlobalCaseDocumentData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseDocumentData[]
     */
    public function getGlobalCaseDocumentData(): ?array
    {
        return isset($this->GlobalCaseDocumentData) ? $this->GlobalCaseDocumentData : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseDocumentData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseDocumentData method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseDocumentDataForArrayConstraintsFromSetGlobalCaseDocumentData(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseDocumentDataGlobalCaseDocumentDataItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseDocumentDataGlobalCaseDocumentDataItem instanceof \ID3Global\Models\GlobalCaseDocumentData) {
                $invalidValues[] = is_object($arrayOfGlobalCaseDocumentDataGlobalCaseDocumentDataItem) ? get_class($arrayOfGlobalCaseDocumentDataGlobalCaseDocumentDataItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseDocumentDataGlobalCaseDocumentDataItem), var_export($arrayOfGlobalCaseDocumentDataGlobalCaseDocumentDataItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseDocumentData property can only contain items of type \ID3Global\Models\GlobalCaseDocumentData, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseDocumentData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseDocumentData[] $globalCaseDocumentData
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDocumentData
     */
    public function setGlobalCaseDocumentData(?array $globalCaseDocumentData = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseDocumentDataArrayErrorMessage = self::validateGlobalCaseDocumentDataForArrayConstraintsFromSetGlobalCaseDocumentData($globalCaseDocumentData))) {
            throw new InvalidArgumentException($globalCaseDocumentDataArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseDocumentData) || (is_array($globalCaseDocumentData) && empty($globalCaseDocumentData))) {
            unset($this->GlobalCaseDocumentData);
        } else {
            $this->GlobalCaseDocumentData = $globalCaseDocumentData;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseDocumentData|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseDocumentData
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseDocumentData|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseDocumentData
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseDocumentData|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseDocumentData
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseDocumentData|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseDocumentData
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseDocumentData|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseDocumentData
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseDocumentData $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDocumentData
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseDocumentData) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseDocumentData property can only contain items of type \ID3Global\Models\GlobalCaseDocumentData, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseDocumentData
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseDocumentData';
    }
}
