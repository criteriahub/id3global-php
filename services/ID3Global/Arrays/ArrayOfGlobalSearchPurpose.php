<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSearchPurpose Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q89:ArrayOfGlobalSearchPurpose
 * @subpackage Arrays
 */
class ArrayOfGlobalSearchPurpose extends AbstractStructArrayBase
{
    /**
     * The GlobalSearchPurpose
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSearchPurpose[]
     */
    protected ?array $GlobalSearchPurpose = null;
    /**
     * Constructor method for ArrayOfGlobalSearchPurpose
     * @uses ArrayOfGlobalSearchPurpose::setGlobalSearchPurpose()
     * @param \ID3Global\Models\GlobalSearchPurpose[] $globalSearchPurpose
     */
    public function __construct(?array $globalSearchPurpose = null)
    {
        $this
            ->setGlobalSearchPurpose($globalSearchPurpose);
    }
    /**
     * Get GlobalSearchPurpose value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSearchPurpose[]
     */
    public function getGlobalSearchPurpose(): ?array
    {
        return isset($this->GlobalSearchPurpose) ? $this->GlobalSearchPurpose : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSearchPurpose method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSearchPurpose method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSearchPurposeForArrayConstraintsFromSetGlobalSearchPurpose(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSearchPurposeGlobalSearchPurposeItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSearchPurposeGlobalSearchPurposeItem instanceof \ID3Global\Models\GlobalSearchPurpose) {
                $invalidValues[] = is_object($arrayOfGlobalSearchPurposeGlobalSearchPurposeItem) ? get_class($arrayOfGlobalSearchPurposeGlobalSearchPurposeItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSearchPurposeGlobalSearchPurposeItem), var_export($arrayOfGlobalSearchPurposeGlobalSearchPurposeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSearchPurpose property can only contain items of type \ID3Global\Models\GlobalSearchPurpose, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSearchPurpose value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSearchPurpose[] $globalSearchPurpose
     * @return \ID3Global\Arrays\ArrayOfGlobalSearchPurpose
     */
    public function setGlobalSearchPurpose(?array $globalSearchPurpose = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSearchPurposeArrayErrorMessage = self::validateGlobalSearchPurposeForArrayConstraintsFromSetGlobalSearchPurpose($globalSearchPurpose))) {
            throw new InvalidArgumentException($globalSearchPurposeArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSearchPurpose) || (is_array($globalSearchPurpose) && empty($globalSearchPurpose))) {
            unset($this->GlobalSearchPurpose);
        } else {
            $this->GlobalSearchPurpose = $globalSearchPurpose;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSearchPurpose|null
     */
    public function current(): ?\ID3Global\Models\GlobalSearchPurpose
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSearchPurpose|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSearchPurpose
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSearchPurpose|null
     */
    public function first(): ?\ID3Global\Models\GlobalSearchPurpose
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSearchPurpose|null
     */
    public function last(): ?\ID3Global\Models\GlobalSearchPurpose
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSearchPurpose|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSearchPurpose
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSearchPurpose $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSearchPurpose
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSearchPurpose) {
            throw new InvalidArgumentException(sprintf('The GlobalSearchPurpose property can only contain items of type \ID3Global\Models\GlobalSearchPurpose, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSearchPurpose
     */
    public function getAttributeName(): string
    {
        return 'GlobalSearchPurpose';
    }
}
