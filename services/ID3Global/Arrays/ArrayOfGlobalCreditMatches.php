<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCreditMatches Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q76:ArrayOfGlobalCreditMatches
 * @subpackage Arrays
 */
class ArrayOfGlobalCreditMatches extends AbstractStructArrayBase
{
    /**
     * The GlobalCreditMatches
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCreditMatches[]
     */
    protected ?array $GlobalCreditMatches = null;
    /**
     * Constructor method for ArrayOfGlobalCreditMatches
     * @uses ArrayOfGlobalCreditMatches::setGlobalCreditMatches()
     * @param \ID3Global\Models\GlobalCreditMatches[] $globalCreditMatches
     */
    public function __construct(?array $globalCreditMatches = null)
    {
        $this
            ->setGlobalCreditMatches($globalCreditMatches);
    }
    /**
     * Get GlobalCreditMatches value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCreditMatches[]
     */
    public function getGlobalCreditMatches(): ?array
    {
        return isset($this->GlobalCreditMatches) ? $this->GlobalCreditMatches : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCreditMatches method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCreditMatches method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCreditMatchesForArrayConstraintsFromSetGlobalCreditMatches(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCreditMatchesGlobalCreditMatchesItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCreditMatchesGlobalCreditMatchesItem instanceof \ID3Global\Models\GlobalCreditMatches) {
                $invalidValues[] = is_object($arrayOfGlobalCreditMatchesGlobalCreditMatchesItem) ? get_class($arrayOfGlobalCreditMatchesGlobalCreditMatchesItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCreditMatchesGlobalCreditMatchesItem), var_export($arrayOfGlobalCreditMatchesGlobalCreditMatchesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCreditMatches property can only contain items of type \ID3Global\Models\GlobalCreditMatches, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCreditMatches value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCreditMatches[] $globalCreditMatches
     * @return \ID3Global\Arrays\ArrayOfGlobalCreditMatches
     */
    public function setGlobalCreditMatches(?array $globalCreditMatches = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCreditMatchesArrayErrorMessage = self::validateGlobalCreditMatchesForArrayConstraintsFromSetGlobalCreditMatches($globalCreditMatches))) {
            throw new InvalidArgumentException($globalCreditMatchesArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCreditMatches) || (is_array($globalCreditMatches) && empty($globalCreditMatches))) {
            unset($this->GlobalCreditMatches);
        } else {
            $this->GlobalCreditMatches = $globalCreditMatches;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCreditMatches|null
     */
    public function current(): ?\ID3Global\Models\GlobalCreditMatches
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCreditMatches|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCreditMatches
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCreditMatches|null
     */
    public function first(): ?\ID3Global\Models\GlobalCreditMatches
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCreditMatches|null
     */
    public function last(): ?\ID3Global\Models\GlobalCreditMatches
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCreditMatches|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCreditMatches
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCreditMatches $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCreditMatches
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCreditMatches) {
            throw new InvalidArgumentException(sprintf('The GlobalCreditMatches property can only contain items of type \ID3Global\Models\GlobalCreditMatches, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCreditMatches
     */
    public function getAttributeName(): string
    {
        return 'GlobalCreditMatches';
    }
}
