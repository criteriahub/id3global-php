<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalSupplierAccount Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q633:ArrayOfGlobalSupplierAccount
 * @subpackage Arrays
 */
class ArrayOfGlobalSupplierAccount extends AbstractStructArrayBase
{
    /**
     * The GlobalSupplierAccount
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierAccount[]
     */
    protected ?array $GlobalSupplierAccount = null;
    /**
     * Constructor method for ArrayOfGlobalSupplierAccount
     * @uses ArrayOfGlobalSupplierAccount::setGlobalSupplierAccount()
     * @param \ID3Global\Models\GlobalSupplierAccount[] $globalSupplierAccount
     */
    public function __construct(?array $globalSupplierAccount = null)
    {
        $this
            ->setGlobalSupplierAccount($globalSupplierAccount);
    }
    /**
     * Get GlobalSupplierAccount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierAccount[]
     */
    public function getGlobalSupplierAccount(): ?array
    {
        return isset($this->GlobalSupplierAccount) ? $this->GlobalSupplierAccount : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalSupplierAccount method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalSupplierAccount method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalSupplierAccountForArrayConstraintsFromSetGlobalSupplierAccount(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalSupplierAccountGlobalSupplierAccountItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalSupplierAccountGlobalSupplierAccountItem instanceof \ID3Global\Models\GlobalSupplierAccount) {
                $invalidValues[] = is_object($arrayOfGlobalSupplierAccountGlobalSupplierAccountItem) ? get_class($arrayOfGlobalSupplierAccountGlobalSupplierAccountItem) : sprintf('%s(%s)', gettype($arrayOfGlobalSupplierAccountGlobalSupplierAccountItem), var_export($arrayOfGlobalSupplierAccountGlobalSupplierAccountItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalSupplierAccount property can only contain items of type \ID3Global\Models\GlobalSupplierAccount, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalSupplierAccount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupplierAccount[] $globalSupplierAccount
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierAccount
     */
    public function setGlobalSupplierAccount(?array $globalSupplierAccount = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalSupplierAccountArrayErrorMessage = self::validateGlobalSupplierAccountForArrayConstraintsFromSetGlobalSupplierAccount($globalSupplierAccount))) {
            throw new InvalidArgumentException($globalSupplierAccountArrayErrorMessage, __LINE__);
        }
        if (is_null($globalSupplierAccount) || (is_array($globalSupplierAccount) && empty($globalSupplierAccount))) {
            unset($this->GlobalSupplierAccount);
        } else {
            $this->GlobalSupplierAccount = $globalSupplierAccount;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalSupplierAccount|null
     */
    public function current(): ?\ID3Global\Models\GlobalSupplierAccount
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalSupplierAccount|null
     */
    public function item($index): ?\ID3Global\Models\GlobalSupplierAccount
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalSupplierAccount|null
     */
    public function first(): ?\ID3Global\Models\GlobalSupplierAccount
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalSupplierAccount|null
     */
    public function last(): ?\ID3Global\Models\GlobalSupplierAccount
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalSupplierAccount|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalSupplierAccount
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalSupplierAccount $item
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierAccount
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalSupplierAccount) {
            throw new InvalidArgumentException(sprintf('The GlobalSupplierAccount property can only contain items of type \ID3Global\Models\GlobalSupplierAccount, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalSupplierAccount
     */
    public function getAttributeName(): string
    {
        return 'GlobalSupplierAccount';
    }
}
