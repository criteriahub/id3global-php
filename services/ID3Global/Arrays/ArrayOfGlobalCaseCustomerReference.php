<?php

declare(strict_types=1);

namespace ID3Global\Arrays;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfGlobalCaseCustomerReference Arrays
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q749:ArrayOfGlobalCaseCustomerReference
 * @subpackage Arrays
 */
class ArrayOfGlobalCaseCustomerReference extends AbstractStructArrayBase
{
    /**
     * The GlobalCaseCustomerReference
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseCustomerReference[]
     */
    protected ?array $GlobalCaseCustomerReference = null;
    /**
     * Constructor method for ArrayOfGlobalCaseCustomerReference
     * @uses ArrayOfGlobalCaseCustomerReference::setGlobalCaseCustomerReference()
     * @param \ID3Global\Models\GlobalCaseCustomerReference[] $globalCaseCustomerReference
     */
    public function __construct(?array $globalCaseCustomerReference = null)
    {
        $this
            ->setGlobalCaseCustomerReference($globalCaseCustomerReference);
    }
    /**
     * Get GlobalCaseCustomerReference value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseCustomerReference[]
     */
    public function getGlobalCaseCustomerReference(): ?array
    {
        return isset($this->GlobalCaseCustomerReference) ? $this->GlobalCaseCustomerReference : null;
    }
    /**
     * This method is responsible for validating the values passed to the setGlobalCaseCustomerReference method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGlobalCaseCustomerReference method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateGlobalCaseCustomerReferenceForArrayConstraintsFromSetGlobalCaseCustomerReference(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfGlobalCaseCustomerReferenceGlobalCaseCustomerReferenceItem) {
            // validation for constraint: itemType
            if (!$arrayOfGlobalCaseCustomerReferenceGlobalCaseCustomerReferenceItem instanceof \ID3Global\Models\GlobalCaseCustomerReference) {
                $invalidValues[] = is_object($arrayOfGlobalCaseCustomerReferenceGlobalCaseCustomerReferenceItem) ? get_class($arrayOfGlobalCaseCustomerReferenceGlobalCaseCustomerReferenceItem) : sprintf('%s(%s)', gettype($arrayOfGlobalCaseCustomerReferenceGlobalCaseCustomerReferenceItem), var_export($arrayOfGlobalCaseCustomerReferenceGlobalCaseCustomerReferenceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The GlobalCaseCustomerReference property can only contain items of type \ID3Global\Models\GlobalCaseCustomerReference, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set GlobalCaseCustomerReference value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseCustomerReference[] $globalCaseCustomerReference
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseCustomerReference
     */
    public function setGlobalCaseCustomerReference(?array $globalCaseCustomerReference = null): self
    {
        // validation for constraint: array
        if ('' !== ($globalCaseCustomerReferenceArrayErrorMessage = self::validateGlobalCaseCustomerReferenceForArrayConstraintsFromSetGlobalCaseCustomerReference($globalCaseCustomerReference))) {
            throw new InvalidArgumentException($globalCaseCustomerReferenceArrayErrorMessage, __LINE__);
        }
        if (is_null($globalCaseCustomerReference) || (is_array($globalCaseCustomerReference) && empty($globalCaseCustomerReference))) {
            unset($this->GlobalCaseCustomerReference);
        } else {
            $this->GlobalCaseCustomerReference = $globalCaseCustomerReference;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \ID3Global\Models\GlobalCaseCustomerReference|null
     */
    public function current(): ?\ID3Global\Models\GlobalCaseCustomerReference
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \ID3Global\Models\GlobalCaseCustomerReference|null
     */
    public function item($index): ?\ID3Global\Models\GlobalCaseCustomerReference
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \ID3Global\Models\GlobalCaseCustomerReference|null
     */
    public function first(): ?\ID3Global\Models\GlobalCaseCustomerReference
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \ID3Global\Models\GlobalCaseCustomerReference|null
     */
    public function last(): ?\ID3Global\Models\GlobalCaseCustomerReference
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \ID3Global\Models\GlobalCaseCustomerReference|null
     */
    public function offsetGet($offset): ?\ID3Global\Models\GlobalCaseCustomerReference
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \ID3Global\Models\GlobalCaseCustomerReference $item
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseCustomerReference
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \ID3Global\Models\GlobalCaseCustomerReference) {
            throw new InvalidArgumentException(sprintf('The GlobalCaseCustomerReference property can only contain items of type \ID3Global\Models\GlobalCaseCustomerReference, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string GlobalCaseCustomerReference
     */
    public function getAttributeName(): string
    {
        return 'GlobalCaseCustomerReference';
    }
}
