<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAuthentications Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q467:GlobalAuthentications
 * @subpackage Structs
 */
class GlobalAuthentications extends AbstractStructBase
{
    /**
     * The Authentications
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalAuthentication|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalAuthentication $Authentications = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The TotalPages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPages = null;
    /**
     * The TotalAuthentications
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalAuthentications = null;
    /**
     * Constructor method for GlobalAuthentications
     * @uses GlobalAuthentications::setAuthentications()
     * @uses GlobalAuthentications::setPageSize()
     * @uses GlobalAuthentications::setTotalPages()
     * @uses GlobalAuthentications::setTotalAuthentications()
     * @param \ID3Global\Arrays\ArrayOfGlobalAuthentication $authentications
     * @param int $pageSize
     * @param int $totalPages
     * @param int $totalAuthentications
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalAuthentication $authentications = null, ?int $pageSize = null, ?int $totalPages = null, ?int $totalAuthentications = null)
    {
        $this
            ->setAuthentications($authentications)
            ->setPageSize($pageSize)
            ->setTotalPages($totalPages)
            ->setTotalAuthentications($totalAuthentications);
    }
    /**
     * Get Authentications value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalAuthentication|null
     */
    public function getAuthentications(): ?\ID3Global\Arrays\ArrayOfGlobalAuthentication
    {
        return isset($this->Authentications) ? $this->Authentications : null;
    }
    /**
     * Set Authentications value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalAuthentication $authentications
     * @return \ID3Global\Models\GlobalAuthentications
     */
    public function setAuthentications(?\ID3Global\Arrays\ArrayOfGlobalAuthentication $authentications = null): self
    {
        if (is_null($authentications) || (is_array($authentications) && empty($authentications))) {
            unset($this->Authentications);
        } else {
            $this->Authentications = $authentications;
        }
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GlobalAuthentications
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get TotalPages value
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->TotalPages;
    }
    /**
     * Set TotalPages value
     * @param int $totalPages
     * @return \ID3Global\Models\GlobalAuthentications
     */
    public function setTotalPages(?int $totalPages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPages) && !(is_int($totalPages) || ctype_digit($totalPages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPages, true), gettype($totalPages)), __LINE__);
        }
        $this->TotalPages = $totalPages;
        
        return $this;
    }
    /**
     * Get TotalAuthentications value
     * @return int|null
     */
    public function getTotalAuthentications(): ?int
    {
        return $this->TotalAuthentications;
    }
    /**
     * Set TotalAuthentications value
     * @param int $totalAuthentications
     * @return \ID3Global\Models\GlobalAuthentications
     */
    public function setTotalAuthentications(?int $totalAuthentications = null): self
    {
        // validation for constraint: int
        if (!is_null($totalAuthentications) && !(is_int($totalAuthentications) || ctype_digit($totalAuthentications))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalAuthentications, true), gettype($totalAuthentications)), __LINE__);
        }
        $this->TotalAuthentications = $totalAuthentications;
        
        return $this;
    }
}
