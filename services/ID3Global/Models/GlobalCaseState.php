<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseState Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q921:GlobalCaseState
 * @subpackage Structs
 */
class GlobalCaseState extends AbstractStructBase
{
    /**
     * The State
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $State = null;
    /**
     * The OldState
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $OldState = null;
    /**
     * The CreatedAccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $CreatedAccountID = null;
    /**
     * The CreatedUser
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CreatedUser = null;
    /**
     * The Created
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Created = null;
    /**
     * The CreatedOrganisationID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $CreatedOrganisationID = null;
    /**
     * The CreatedDomain
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CreatedDomain = null;
    /**
     * Constructor method for GlobalCaseState
     * @uses GlobalCaseState::setState()
     * @uses GlobalCaseState::setOldState()
     * @uses GlobalCaseState::setCreatedAccountID()
     * @uses GlobalCaseState::setCreatedUser()
     * @uses GlobalCaseState::setCreated()
     * @uses GlobalCaseState::setCreatedOrganisationID()
     * @uses GlobalCaseState::setCreatedDomain()
     * @param int $state
     * @param int $oldState
     * @param string $createdAccountID
     * @param string $createdUser
     * @param string $created
     * @param string $createdOrganisationID
     * @param string $createdDomain
     */
    public function __construct(?int $state = null, ?int $oldState = null, ?string $createdAccountID = null, ?string $createdUser = null, ?string $created = null, ?string $createdOrganisationID = null, ?string $createdDomain = null)
    {
        $this
            ->setState($state)
            ->setOldState($oldState)
            ->setCreatedAccountID($createdAccountID)
            ->setCreatedUser($createdUser)
            ->setCreated($created)
            ->setCreatedOrganisationID($createdOrganisationID)
            ->setCreatedDomain($createdDomain);
    }
    /**
     * Get State value
     * @return int|null
     */
    public function getState(): ?int
    {
        return $this->State;
    }
    /**
     * Set State value
     * @param int $state
     * @return \ID3Global\Models\GlobalCaseState
     */
    public function setState(?int $state = null): self
    {
        // validation for constraint: int
        if (!is_null($state) && !(is_int($state) || ctype_digit($state))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($state, true), gettype($state)), __LINE__);
        }
        $this->State = $state;
        
        return $this;
    }
    /**
     * Get OldState value
     * @return int|null
     */
    public function getOldState(): ?int
    {
        return $this->OldState;
    }
    /**
     * Set OldState value
     * @param int $oldState
     * @return \ID3Global\Models\GlobalCaseState
     */
    public function setOldState(?int $oldState = null): self
    {
        // validation for constraint: int
        if (!is_null($oldState) && !(is_int($oldState) || ctype_digit($oldState))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($oldState, true), gettype($oldState)), __LINE__);
        }
        $this->OldState = $oldState;
        
        return $this;
    }
    /**
     * Get CreatedAccountID value
     * @return string|null
     */
    public function getCreatedAccountID(): ?string
    {
        return $this->CreatedAccountID;
    }
    /**
     * Set CreatedAccountID value
     * @param string $createdAccountID
     * @return \ID3Global\Models\GlobalCaseState
     */
    public function setCreatedAccountID(?string $createdAccountID = null): self
    {
        // validation for constraint: string
        if (!is_null($createdAccountID) && !is_string($createdAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($createdAccountID, true), gettype($createdAccountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($createdAccountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $createdAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($createdAccountID, true)), __LINE__);
        }
        $this->CreatedAccountID = $createdAccountID;
        
        return $this;
    }
    /**
     * Get CreatedUser value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCreatedUser(): ?string
    {
        return isset($this->CreatedUser) ? $this->CreatedUser : null;
    }
    /**
     * Set CreatedUser value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $createdUser
     * @return \ID3Global\Models\GlobalCaseState
     */
    public function setCreatedUser(?string $createdUser = null): self
    {
        // validation for constraint: string
        if (!is_null($createdUser) && !is_string($createdUser)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($createdUser, true), gettype($createdUser)), __LINE__);
        }
        if (is_null($createdUser) || (is_array($createdUser) && empty($createdUser))) {
            unset($this->CreatedUser);
        } else {
            $this->CreatedUser = $createdUser;
        }
        
        return $this;
    }
    /**
     * Get Created value
     * @return string|null
     */
    public function getCreated(): ?string
    {
        return $this->Created;
    }
    /**
     * Set Created value
     * @param string $created
     * @return \ID3Global\Models\GlobalCaseState
     */
    public function setCreated(?string $created = null): self
    {
        // validation for constraint: string
        if (!is_null($created) && !is_string($created)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($created, true), gettype($created)), __LINE__);
        }
        $this->Created = $created;
        
        return $this;
    }
    /**
     * Get CreatedOrganisationID value
     * @return string|null
     */
    public function getCreatedOrganisationID(): ?string
    {
        return $this->CreatedOrganisationID;
    }
    /**
     * Set CreatedOrganisationID value
     * @param string $createdOrganisationID
     * @return \ID3Global\Models\GlobalCaseState
     */
    public function setCreatedOrganisationID(?string $createdOrganisationID = null): self
    {
        // validation for constraint: string
        if (!is_null($createdOrganisationID) && !is_string($createdOrganisationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($createdOrganisationID, true), gettype($createdOrganisationID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($createdOrganisationID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $createdOrganisationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($createdOrganisationID, true)), __LINE__);
        }
        $this->CreatedOrganisationID = $createdOrganisationID;
        
        return $this;
    }
    /**
     * Get CreatedDomain value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCreatedDomain(): ?string
    {
        return isset($this->CreatedDomain) ? $this->CreatedDomain : null;
    }
    /**
     * Set CreatedDomain value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $createdDomain
     * @return \ID3Global\Models\GlobalCaseState
     */
    public function setCreatedDomain(?string $createdDomain = null): self
    {
        // validation for constraint: string
        if (!is_null($createdDomain) && !is_string($createdDomain)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($createdDomain, true), gettype($createdDomain)), __LINE__);
        }
        if (is_null($createdDomain) || (is_array($createdDomain) && empty($createdDomain))) {
            unset($this->CreatedDomain);
        } else {
            $this->CreatedDomain = $createdDomain;
        }
        
        return $this;
    }
}
