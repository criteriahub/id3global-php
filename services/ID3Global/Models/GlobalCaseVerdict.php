<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseVerdict Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q762:GlobalCaseVerdict
 * @subpackage Structs
 */
class GlobalCaseVerdict extends AbstractStructBase
{
    /**
     * The ProfileId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ProfileId = null;
    /**
     * The Verdict
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Verdict = null;
    /**
     * Constructor method for GlobalCaseVerdict
     * @uses GlobalCaseVerdict::setProfileId()
     * @uses GlobalCaseVerdict::setVerdict()
     * @param string $profileId
     * @param int $verdict
     */
    public function __construct(?string $profileId = null, ?int $verdict = null)
    {
        $this
            ->setProfileId($profileId)
            ->setVerdict($verdict);
    }
    /**
     * Get ProfileId value
     * @return string|null
     */
    public function getProfileId(): ?string
    {
        return $this->ProfileId;
    }
    /**
     * Set ProfileId value
     * @param string $profileId
     * @return \ID3Global\Models\GlobalCaseVerdict
     */
    public function setProfileId(?string $profileId = null): self
    {
        // validation for constraint: string
        if (!is_null($profileId) && !is_string($profileId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($profileId, true), gettype($profileId)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($profileId) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $profileId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($profileId, true)), __LINE__);
        }
        $this->ProfileId = $profileId;
        
        return $this;
    }
    /**
     * Get Verdict value
     * @return int|null
     */
    public function getVerdict(): ?int
    {
        return $this->Verdict;
    }
    /**
     * Set Verdict value
     * @param int $verdict
     * @return \ID3Global\Models\GlobalCaseVerdict
     */
    public function setVerdict(?int $verdict = null): self
    {
        // validation for constraint: int
        if (!is_null($verdict) && !(is_int($verdict) || ctype_digit($verdict))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($verdict, true), gettype($verdict)), __LINE__);
        }
        $this->Verdict = $verdict;
        
        return $this;
    }
}
