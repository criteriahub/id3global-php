<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseDocumentData Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q847:GlobalCaseDocumentData
 * @subpackage Structs
 */
class GlobalCaseDocumentData extends AbstractStructBase
{
    /**
     * The ID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ID = null;
    /**
     * The Values
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $Values = null;
    /**
     * Constructor method for GlobalCaseDocumentData
     * @uses GlobalCaseDocumentData::setID()
     * @uses GlobalCaseDocumentData::setValues()
     * @param int $iD
     * @param \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $values
     */
    public function __construct(?int $iD = null, ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $values = null)
    {
        $this
            ->setID($iD)
            ->setValues($values);
    }
    /**
     * Get ID value
     * @return int|null
     */
    public function getID(): ?int
    {
        return $this->ID;
    }
    /**
     * Set ID value
     * @param int $iD
     * @return \ID3Global\Models\GlobalCaseDocumentData
     */
    public function setID(?int $iD = null): self
    {
        // validation for constraint: int
        if (!is_null($iD) && !(is_int($iD) || ctype_digit($iD))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($iD, true), gettype($iD)), __LINE__);
        }
        $this->ID = $iD;
        
        return $this;
    }
    /**
     * Get Values value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring|null
     */
    public function getValues(): ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring
    {
        return isset($this->Values) ? $this->Values : null;
    }
    /**
     * Set Values value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $values
     * @return \ID3Global\Models\GlobalCaseDocumentData
     */
    public function setValues(?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $values = null): self
    {
        if (is_null($values) || (is_array($values) && empty($values))) {
            unset($this->Values);
        } else {
            $this->Values = $values;
        }
        
        return $this;
    }
}
