<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ForceOverride Models
 * @subpackage Structs
 */
class ForceOverride extends AbstractStructBase
{
    /**
     * The AuthenticationID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AuthenticationID = null;
    /**
     * The ProfileIDVersion
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileIDVersion|null
     */
    protected ?\ID3Global\Models\GlobalProfileIDVersion $ProfileIDVersion = null;
    /**
     * The CustomerReference
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CustomerReference = null;
    /**
     * The Overrides
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalOverride|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalOverride $Overrides = null;
    /**
     * Constructor method for ForceOverride
     * @uses ForceOverride::setAuthenticationID()
     * @uses ForceOverride::setProfileIDVersion()
     * @uses ForceOverride::setCustomerReference()
     * @uses ForceOverride::setOverrides()
     * @param string $authenticationID
     * @param \ID3Global\Models\GlobalProfileIDVersion $profileIDVersion
     * @param string $customerReference
     * @param \ID3Global\Arrays\ArrayOfGlobalOverride $overrides
     */
    public function __construct(?string $authenticationID = null, ?\ID3Global\Models\GlobalProfileIDVersion $profileIDVersion = null, ?string $customerReference = null, ?\ID3Global\Arrays\ArrayOfGlobalOverride $overrides = null)
    {
        $this
            ->setAuthenticationID($authenticationID)
            ->setProfileIDVersion($profileIDVersion)
            ->setCustomerReference($customerReference)
            ->setOverrides($overrides);
    }
    /**
     * Get AuthenticationID value
     * @return string|null
     */
    public function getAuthenticationID(): ?string
    {
        return $this->AuthenticationID;
    }
    /**
     * Set AuthenticationID value
     * @param string $authenticationID
     * @return \ID3Global\Models\ForceOverride
     */
    public function setAuthenticationID(?string $authenticationID = null): self
    {
        // validation for constraint: string
        if (!is_null($authenticationID) && !is_string($authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authenticationID, true), gettype($authenticationID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($authenticationID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($authenticationID, true)), __LINE__);
        }
        $this->AuthenticationID = $authenticationID;
        
        return $this;
    }
    /**
     * Get ProfileIDVersion value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileIDVersion|null
     */
    public function getProfileIDVersion(): ?\ID3Global\Models\GlobalProfileIDVersion
    {
        return isset($this->ProfileIDVersion) ? $this->ProfileIDVersion : null;
    }
    /**
     * Set ProfileIDVersion value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileIDVersion $profileIDVersion
     * @return \ID3Global\Models\ForceOverride
     */
    public function setProfileIDVersion(?\ID3Global\Models\GlobalProfileIDVersion $profileIDVersion = null): self
    {
        if (is_null($profileIDVersion) || (is_array($profileIDVersion) && empty($profileIDVersion))) {
            unset($this->ProfileIDVersion);
        } else {
            $this->ProfileIDVersion = $profileIDVersion;
        }
        
        return $this;
    }
    /**
     * Get CustomerReference value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCustomerReference(): ?string
    {
        return isset($this->CustomerReference) ? $this->CustomerReference : null;
    }
    /**
     * Set CustomerReference value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $customerReference
     * @return \ID3Global\Models\ForceOverride
     */
    public function setCustomerReference(?string $customerReference = null): self
    {
        // validation for constraint: string
        if (!is_null($customerReference) && !is_string($customerReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($customerReference, true), gettype($customerReference)), __LINE__);
        }
        if (is_null($customerReference) || (is_array($customerReference) && empty($customerReference))) {
            unset($this->CustomerReference);
        } else {
            $this->CustomerReference = $customerReference;
        }
        
        return $this;
    }
    /**
     * Get Overrides value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalOverride|null
     */
    public function getOverrides(): ?\ID3Global\Arrays\ArrayOfGlobalOverride
    {
        return isset($this->Overrides) ? $this->Overrides : null;
    }
    /**
     * Set Overrides value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalOverride $overrides
     * @return \ID3Global\Models\ForceOverride
     */
    public function setOverrides(?\ID3Global\Arrays\ArrayOfGlobalOverride $overrides = null): self
    {
        if (is_null($overrides) || (is_array($overrides) && empty($overrides))) {
            unset($this->Overrides);
        } else {
            $this->Overrides = $overrides;
        }
        
        return $this;
    }
}
