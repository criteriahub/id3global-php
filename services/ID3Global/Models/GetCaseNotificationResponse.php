<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCaseNotificationResponse Models
 * @subpackage Structs
 */
class GetCaseNotificationResponse extends AbstractStructBase
{
    /**
     * The GetCaseNotificationResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseNotification|null
     */
    protected ?\ID3Global\Models\GlobalCaseNotification $GetCaseNotificationResult = null;
    /**
     * Constructor method for GetCaseNotificationResponse
     * @uses GetCaseNotificationResponse::setGetCaseNotificationResult()
     * @param \ID3Global\Models\GlobalCaseNotification $getCaseNotificationResult
     */
    public function __construct(?\ID3Global\Models\GlobalCaseNotification $getCaseNotificationResult = null)
    {
        $this
            ->setGetCaseNotificationResult($getCaseNotificationResult);
    }
    /**
     * Get GetCaseNotificationResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseNotification|null
     */
    public function getGetCaseNotificationResult(): ?\ID3Global\Models\GlobalCaseNotification
    {
        return isset($this->GetCaseNotificationResult) ? $this->GetCaseNotificationResult : null;
    }
    /**
     * Set GetCaseNotificationResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCaseNotification $getCaseNotificationResult
     * @return \ID3Global\Models\GetCaseNotificationResponse
     */
    public function setGetCaseNotificationResult(?\ID3Global\Models\GlobalCaseNotification $getCaseNotificationResult = null): self
    {
        if (is_null($getCaseNotificationResult) || (is_array($getCaseNotificationResult) && empty($getCaseNotificationResult))) {
            unset($this->GetCaseNotificationResult);
        } else {
            $this->GetCaseNotificationResult = $getCaseNotificationResult;
        }
        
        return $this;
    }
}
