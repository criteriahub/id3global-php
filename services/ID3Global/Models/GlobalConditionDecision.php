<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalConditionDecision Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q231:GlobalConditionDecision
 * @subpackage Structs
 */
class GlobalConditionDecision extends AbstractStructBase
{
    /**
     * The BandTargetText
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $BandTargetText = null;
    /**
     * The TrueIfMatched
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $TrueIfMatched = null;
    /**
     * Constructor method for GlobalConditionDecision
     * @uses GlobalConditionDecision::setBandTargetText()
     * @uses GlobalConditionDecision::setTrueIfMatched()
     * @param string $bandTargetText
     * @param bool $trueIfMatched
     */
    public function __construct(?string $bandTargetText = null, ?bool $trueIfMatched = null)
    {
        $this
            ->setBandTargetText($bandTargetText)
            ->setTrueIfMatched($trueIfMatched);
    }
    /**
     * Get BandTargetText value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBandTargetText(): ?string
    {
        return isset($this->BandTargetText) ? $this->BandTargetText : null;
    }
    /**
     * Set BandTargetText value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $bandTargetText
     * @return \ID3Global\Models\GlobalConditionDecision
     */
    public function setBandTargetText(?string $bandTargetText = null): self
    {
        // validation for constraint: string
        if (!is_null($bandTargetText) && !is_string($bandTargetText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bandTargetText, true), gettype($bandTargetText)), __LINE__);
        }
        if (is_null($bandTargetText) || (is_array($bandTargetText) && empty($bandTargetText))) {
            unset($this->BandTargetText);
        } else {
            $this->BandTargetText = $bandTargetText;
        }
        
        return $this;
    }
    /**
     * Get TrueIfMatched value
     * @return bool|null
     */
    public function getTrueIfMatched(): ?bool
    {
        return $this->TrueIfMatched;
    }
    /**
     * Set TrueIfMatched value
     * @param bool $trueIfMatched
     * @return \ID3Global\Models\GlobalConditionDecision
     */
    public function setTrueIfMatched(?bool $trueIfMatched = null): self
    {
        // validation for constraint: boolean
        if (!is_null($trueIfMatched) && !is_bool($trueIfMatched)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($trueIfMatched, true), gettype($trueIfMatched)), __LINE__);
        }
        $this->TrueIfMatched = $trueIfMatched;
        
        return $this;
    }
}
