<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCardTypesResponse Models
 * @subpackage Structs
 */
class GetCardTypesResponse extends AbstractStructBase
{
    /**
     * The GetCardTypesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCardType|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCardType $GetCardTypesResult = null;
    /**
     * Constructor method for GetCardTypesResponse
     * @uses GetCardTypesResponse::setGetCardTypesResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalCardType $getCardTypesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCardType $getCardTypesResult = null)
    {
        $this
            ->setGetCardTypesResult($getCardTypesResult);
    }
    /**
     * Get GetCardTypesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCardType|null
     */
    public function getGetCardTypesResult(): ?\ID3Global\Arrays\ArrayOfGlobalCardType
    {
        return isset($this->GetCardTypesResult) ? $this->GetCardTypesResult : null;
    }
    /**
     * Set GetCardTypesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCardType $getCardTypesResult
     * @return \ID3Global\Models\GetCardTypesResponse
     */
    public function setGetCardTypesResult(?\ID3Global\Arrays\ArrayOfGlobalCardType $getCardTypesResult = null): self
    {
        if (is_null($getCardTypesResult) || (is_array($getCardTypesResult) && empty($getCardTypesResult))) {
            unset($this->GetCardTypesResult);
        } else {
            $this->GetCardTypesResult = $getCardTypesResult;
        }
        
        return $this;
    }
}
