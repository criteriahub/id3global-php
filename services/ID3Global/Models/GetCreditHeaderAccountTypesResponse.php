<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCreditHeaderAccountTypesResponse Models
 * @subpackage Structs
 */
class GetCreditHeaderAccountTypesResponse extends AbstractStructBase
{
    /**
     * The GetCreditHeaderAccountTypesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCreditHeaderAccountType|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCreditHeaderAccountType $GetCreditHeaderAccountTypesResult = null;
    /**
     * Constructor method for GetCreditHeaderAccountTypesResponse
     * @uses GetCreditHeaderAccountTypesResponse::setGetCreditHeaderAccountTypesResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalCreditHeaderAccountType $getCreditHeaderAccountTypesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCreditHeaderAccountType $getCreditHeaderAccountTypesResult = null)
    {
        $this
            ->setGetCreditHeaderAccountTypesResult($getCreditHeaderAccountTypesResult);
    }
    /**
     * Get GetCreditHeaderAccountTypesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCreditHeaderAccountType|null
     */
    public function getGetCreditHeaderAccountTypesResult(): ?\ID3Global\Arrays\ArrayOfGlobalCreditHeaderAccountType
    {
        return isset($this->GetCreditHeaderAccountTypesResult) ? $this->GetCreditHeaderAccountTypesResult : null;
    }
    /**
     * Set GetCreditHeaderAccountTypesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCreditHeaderAccountType $getCreditHeaderAccountTypesResult
     * @return \ID3Global\Models\GetCreditHeaderAccountTypesResponse
     */
    public function setGetCreditHeaderAccountTypesResult(?\ID3Global\Arrays\ArrayOfGlobalCreditHeaderAccountType $getCreditHeaderAccountTypesResult = null): self
    {
        if (is_null($getCreditHeaderAccountTypesResult) || (is_array($getCreditHeaderAccountTypesResult) && empty($getCreditHeaderAccountTypesResult))) {
            unset($this->GetCreditHeaderAccountTypesResult);
        } else {
            $this->GetCreditHeaderAccountTypesResult = $getCreditHeaderAccountTypesResult;
        }
        
        return $this;
    }
}
