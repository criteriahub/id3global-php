<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPEPTiersResponse Models
 * @subpackage Structs
 */
class GetPEPTiersResponse extends AbstractStructBase
{
    /**
     * The GetPEPTiersResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalPEPTier|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalPEPTier $GetPEPTiersResult = null;
    /**
     * Constructor method for GetPEPTiersResponse
     * @uses GetPEPTiersResponse::setGetPEPTiersResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalPEPTier $getPEPTiersResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalPEPTier $getPEPTiersResult = null)
    {
        $this
            ->setGetPEPTiersResult($getPEPTiersResult);
    }
    /**
     * Get GetPEPTiersResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalPEPTier|null
     */
    public function getGetPEPTiersResult(): ?\ID3Global\Arrays\ArrayOfGlobalPEPTier
    {
        return isset($this->GetPEPTiersResult) ? $this->GetPEPTiersResult : null;
    }
    /**
     * Set GetPEPTiersResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalPEPTier $getPEPTiersResult
     * @return \ID3Global\Models\GetPEPTiersResponse
     */
    public function setGetPEPTiersResult(?\ID3Global\Arrays\ArrayOfGlobalPEPTier $getPEPTiersResult = null): self
    {
        if (is_null($getPEPTiersResult) || (is_array($getPEPTiersResult) && empty($getPEPTiersResult))) {
            unset($this->GetPEPTiersResult);
        } else {
            $this->GetPEPTiersResult = $getPEPTiersResult;
        }
        
        return $this;
    }
}
