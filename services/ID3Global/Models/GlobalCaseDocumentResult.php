<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseDocumentResult Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q869:GlobalCaseDocumentResult
 * @subpackage Structs
 */
class GlobalCaseDocumentResult extends AbstractStructBase
{
    /**
     * The DocumentImageID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $DocumentImageID = null;
    /**
     * The DIVData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDIVData|null
     */
    protected ?\ID3Global\Models\GlobalDIVData $DIVData = null;
    /**
     * The AuthDetails
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAuthenticationDetails|null
     */
    protected ?\ID3Global\Models\GlobalAuthenticationDetails $AuthDetails = null;
    /**
     * The InputData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseDocumentData|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseDocumentData $InputData = null;
    /**
     * Constructor method for GlobalCaseDocumentResult
     * @uses GlobalCaseDocumentResult::setDocumentImageID()
     * @uses GlobalCaseDocumentResult::setDIVData()
     * @uses GlobalCaseDocumentResult::setAuthDetails()
     * @uses GlobalCaseDocumentResult::setInputData()
     * @param string $documentImageID
     * @param \ID3Global\Models\GlobalDIVData $dIVData
     * @param \ID3Global\Models\GlobalAuthenticationDetails $authDetails
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseDocumentData $inputData
     */
    public function __construct(?string $documentImageID = null, ?\ID3Global\Models\GlobalDIVData $dIVData = null, ?\ID3Global\Models\GlobalAuthenticationDetails $authDetails = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseDocumentData $inputData = null)
    {
        $this
            ->setDocumentImageID($documentImageID)
            ->setDIVData($dIVData)
            ->setAuthDetails($authDetails)
            ->setInputData($inputData);
    }
    /**
     * Get DocumentImageID value
     * @return string|null
     */
    public function getDocumentImageID(): ?string
    {
        return $this->DocumentImageID;
    }
    /**
     * Set DocumentImageID value
     * @param string $documentImageID
     * @return \ID3Global\Models\GlobalCaseDocumentResult
     */
    public function setDocumentImageID(?string $documentImageID = null): self
    {
        // validation for constraint: string
        if (!is_null($documentImageID) && !is_string($documentImageID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentImageID, true), gettype($documentImageID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($documentImageID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $documentImageID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($documentImageID, true)), __LINE__);
        }
        $this->DocumentImageID = $documentImageID;
        
        return $this;
    }
    /**
     * Get DIVData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDIVData|null
     */
    public function getDIVData(): ?\ID3Global\Models\GlobalDIVData
    {
        return isset($this->DIVData) ? $this->DIVData : null;
    }
    /**
     * Set DIVData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalDIVData $dIVData
     * @return \ID3Global\Models\GlobalCaseDocumentResult
     */
    public function setDIVData(?\ID3Global\Models\GlobalDIVData $dIVData = null): self
    {
        if (is_null($dIVData) || (is_array($dIVData) && empty($dIVData))) {
            unset($this->DIVData);
        } else {
            $this->DIVData = $dIVData;
        }
        
        return $this;
    }
    /**
     * Get AuthDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAuthenticationDetails|null
     */
    public function getAuthDetails(): ?\ID3Global\Models\GlobalAuthenticationDetails
    {
        return isset($this->AuthDetails) ? $this->AuthDetails : null;
    }
    /**
     * Set AuthDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAuthenticationDetails $authDetails
     * @return \ID3Global\Models\GlobalCaseDocumentResult
     */
    public function setAuthDetails(?\ID3Global\Models\GlobalAuthenticationDetails $authDetails = null): self
    {
        if (is_null($authDetails) || (is_array($authDetails) && empty($authDetails))) {
            unset($this->AuthDetails);
        } else {
            $this->AuthDetails = $authDetails;
        }
        
        return $this;
    }
    /**
     * Get InputData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDocumentData|null
     */
    public function getInputData(): ?\ID3Global\Arrays\ArrayOfGlobalCaseDocumentData
    {
        return isset($this->InputData) ? $this->InputData : null;
    }
    /**
     * Set InputData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseDocumentData $inputData
     * @return \ID3Global\Models\GlobalCaseDocumentResult
     */
    public function setInputData(?\ID3Global\Arrays\ArrayOfGlobalCaseDocumentData $inputData = null): self
    {
        if (is_null($inputData) || (is_array($inputData) && empty($inputData))) {
            unset($this->InputData);
        } else {
            $this->InputData = $inputData;
        }
        
        return $this;
    }
}
