<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TestSupplierAccountCredentials Models
 * @subpackage Structs
 */
class TestSupplierAccountCredentials extends AbstractStructBase
{
    /**
     * The SupplierAccount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierAccount|null
     */
    protected ?\ID3Global\Models\GlobalSupplierAccount $SupplierAccount = null;
    /**
     * Constructor method for TestSupplierAccountCredentials
     * @uses TestSupplierAccountCredentials::setSupplierAccount()
     * @param \ID3Global\Models\GlobalSupplierAccount $supplierAccount
     */
    public function __construct(?\ID3Global\Models\GlobalSupplierAccount $supplierAccount = null)
    {
        $this
            ->setSupplierAccount($supplierAccount);
    }
    /**
     * Get SupplierAccount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierAccount|null
     */
    public function getSupplierAccount(): ?\ID3Global\Models\GlobalSupplierAccount
    {
        return isset($this->SupplierAccount) ? $this->SupplierAccount : null;
    }
    /**
     * Set SupplierAccount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupplierAccount $supplierAccount
     * @return \ID3Global\Models\TestSupplierAccountCredentials
     */
    public function setSupplierAccount(?\ID3Global\Models\GlobalSupplierAccount $supplierAccount = null): self
    {
        if (is_null($supplierAccount) || (is_array($supplierAccount) && empty($supplierAccount))) {
            unset($this->SupplierAccount);
        } else {
            $this->SupplierAccount = $supplierAccount;
        }
        
        return $this;
    }
}
