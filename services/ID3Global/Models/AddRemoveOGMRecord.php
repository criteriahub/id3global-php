<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AddRemoveOGMRecord Models
 * @subpackage Structs
 */
class AddRemoveOGMRecord extends AbstractStructBase
{
    /**
     * The AuthenticationID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AuthenticationID = null;
    /**
     * The CurrentAccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $CurrentAccountID = null;
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The CustomerReference
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CustomerReference = null;
    /**
     * The ProfileUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ProfileUID = null;
    /**
     * The ProfileVersion
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ProfileVersion = null;
    /**
     * The InputData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalInputData|null
     */
    protected ?\ID3Global\Models\GlobalInputData $InputData = null;
    /**
     * The ActionType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ActionType = null;
    /**
     * The sError
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $sError = null;
    /**
     * The SourceName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SourceName = null;
    /**
     * Constructor method for AddRemoveOGMRecord
     * @uses AddRemoveOGMRecord::setAuthenticationID()
     * @uses AddRemoveOGMRecord::setCurrentAccountID()
     * @uses AddRemoveOGMRecord::setOrgID()
     * @uses AddRemoveOGMRecord::setCustomerReference()
     * @uses AddRemoveOGMRecord::setProfileUID()
     * @uses AddRemoveOGMRecord::setProfileVersion()
     * @uses AddRemoveOGMRecord::setInputData()
     * @uses AddRemoveOGMRecord::setActionType()
     * @uses AddRemoveOGMRecord::setSError()
     * @uses AddRemoveOGMRecord::setSourceName()
     * @param string $authenticationID
     * @param string $currentAccountID
     * @param string $orgID
     * @param string $customerReference
     * @param string $profileUID
     * @param int $profileVersion
     * @param \ID3Global\Models\GlobalInputData $inputData
     * @param string $actionType
     * @param string $sError
     * @param string $sourceName
     */
    public function __construct(?string $authenticationID = null, ?string $currentAccountID = null, ?string $orgID = null, ?string $customerReference = null, ?string $profileUID = null, ?int $profileVersion = null, ?\ID3Global\Models\GlobalInputData $inputData = null, ?string $actionType = null, ?string $sError = null, ?string $sourceName = null)
    {
        $this
            ->setAuthenticationID($authenticationID)
            ->setCurrentAccountID($currentAccountID)
            ->setOrgID($orgID)
            ->setCustomerReference($customerReference)
            ->setProfileUID($profileUID)
            ->setProfileVersion($profileVersion)
            ->setInputData($inputData)
            ->setActionType($actionType)
            ->setSError($sError)
            ->setSourceName($sourceName);
    }
    /**
     * Get AuthenticationID value
     * @return string|null
     */
    public function getAuthenticationID(): ?string
    {
        return $this->AuthenticationID;
    }
    /**
     * Set AuthenticationID value
     * @param string $authenticationID
     * @return \ID3Global\Models\AddRemoveOGMRecord
     */
    public function setAuthenticationID(?string $authenticationID = null): self
    {
        // validation for constraint: string
        if (!is_null($authenticationID) && !is_string($authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authenticationID, true), gettype($authenticationID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($authenticationID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($authenticationID, true)), __LINE__);
        }
        $this->AuthenticationID = $authenticationID;
        
        return $this;
    }
    /**
     * Get CurrentAccountID value
     * @return string|null
     */
    public function getCurrentAccountID(): ?string
    {
        return $this->CurrentAccountID;
    }
    /**
     * Set CurrentAccountID value
     * @param string $currentAccountID
     * @return \ID3Global\Models\AddRemoveOGMRecord
     */
    public function setCurrentAccountID(?string $currentAccountID = null): self
    {
        // validation for constraint: string
        if (!is_null($currentAccountID) && !is_string($currentAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currentAccountID, true), gettype($currentAccountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($currentAccountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $currentAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($currentAccountID, true)), __LINE__);
        }
        $this->CurrentAccountID = $currentAccountID;
        
        return $this;
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\AddRemoveOGMRecord
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get CustomerReference value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCustomerReference(): ?string
    {
        return isset($this->CustomerReference) ? $this->CustomerReference : null;
    }
    /**
     * Set CustomerReference value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $customerReference
     * @return \ID3Global\Models\AddRemoveOGMRecord
     */
    public function setCustomerReference(?string $customerReference = null): self
    {
        // validation for constraint: string
        if (!is_null($customerReference) && !is_string($customerReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($customerReference, true), gettype($customerReference)), __LINE__);
        }
        if (is_null($customerReference) || (is_array($customerReference) && empty($customerReference))) {
            unset($this->CustomerReference);
        } else {
            $this->CustomerReference = $customerReference;
        }
        
        return $this;
    }
    /**
     * Get ProfileUID value
     * @return string|null
     */
    public function getProfileUID(): ?string
    {
        return $this->ProfileUID;
    }
    /**
     * Set ProfileUID value
     * @param string $profileUID
     * @return \ID3Global\Models\AddRemoveOGMRecord
     */
    public function setProfileUID(?string $profileUID = null): self
    {
        // validation for constraint: string
        if (!is_null($profileUID) && !is_string($profileUID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($profileUID, true), gettype($profileUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($profileUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $profileUID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($profileUID, true)), __LINE__);
        }
        $this->ProfileUID = $profileUID;
        
        return $this;
    }
    /**
     * Get ProfileVersion value
     * @return int|null
     */
    public function getProfileVersion(): ?int
    {
        return $this->ProfileVersion;
    }
    /**
     * Set ProfileVersion value
     * @param int $profileVersion
     * @return \ID3Global\Models\AddRemoveOGMRecord
     */
    public function setProfileVersion(?int $profileVersion = null): self
    {
        // validation for constraint: int
        if (!is_null($profileVersion) && !(is_int($profileVersion) || ctype_digit($profileVersion))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($profileVersion, true), gettype($profileVersion)), __LINE__);
        }
        $this->ProfileVersion = $profileVersion;
        
        return $this;
    }
    /**
     * Get InputData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalInputData|null
     */
    public function getInputData(): ?\ID3Global\Models\GlobalInputData
    {
        return isset($this->InputData) ? $this->InputData : null;
    }
    /**
     * Set InputData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalInputData $inputData
     * @return \ID3Global\Models\AddRemoveOGMRecord
     */
    public function setInputData(?\ID3Global\Models\GlobalInputData $inputData = null): self
    {
        if (is_null($inputData) || (is_array($inputData) && empty($inputData))) {
            unset($this->InputData);
        } else {
            $this->InputData = $inputData;
        }
        
        return $this;
    }
    /**
     * Get ActionType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getActionType(): ?string
    {
        return isset($this->ActionType) ? $this->ActionType : null;
    }
    /**
     * Set ActionType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $actionType
     * @return \ID3Global\Models\AddRemoveOGMRecord
     */
    public function setActionType(?string $actionType = null): self
    {
        // validation for constraint: string
        if (!is_null($actionType) && !is_string($actionType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($actionType, true), gettype($actionType)), __LINE__);
        }
        if (is_null($actionType) || (is_array($actionType) && empty($actionType))) {
            unset($this->ActionType);
        } else {
            $this->ActionType = $actionType;
        }
        
        return $this;
    }
    /**
     * Get sError value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSError(): ?string
    {
        return isset($this->sError) ? $this->sError : null;
    }
    /**
     * Set sError value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sError
     * @return \ID3Global\Models\AddRemoveOGMRecord
     */
    public function setSError(?string $sError = null): self
    {
        // validation for constraint: string
        if (!is_null($sError) && !is_string($sError)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sError, true), gettype($sError)), __LINE__);
        }
        if (is_null($sError) || (is_array($sError) && empty($sError))) {
            unset($this->sError);
        } else {
            $this->sError = $sError;
        }
        
        return $this;
    }
    /**
     * Get SourceName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSourceName(): ?string
    {
        return isset($this->SourceName) ? $this->SourceName : null;
    }
    /**
     * Set SourceName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sourceName
     * @return \ID3Global\Models\AddRemoveOGMRecord
     */
    public function setSourceName(?string $sourceName = null): self
    {
        // validation for constraint: string
        if (!is_null($sourceName) && !is_string($sourceName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sourceName, true), gettype($sourceName)), __LINE__);
        }
        if (is_null($sourceName) || (is_array($sourceName) && empty($sourceName))) {
            unset($this->SourceName);
        } else {
            $this->SourceName = $sourceName;
        }
        
        return $this;
    }
}
