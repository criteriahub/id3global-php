<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateUserAccountExtract Models
 * @subpackage Structs
 */
class CreateUserAccountExtract extends AbstractStructBase
{
    /**
     * The orgId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $orgId = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The accountType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $accountType = null;
    /**
     * The format
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $format = null;
    /**
     * Constructor method for CreateUserAccountExtract
     * @uses CreateUserAccountExtract::setOrgId()
     * @uses CreateUserAccountExtract::setName()
     * @uses CreateUserAccountExtract::setAccountType()
     * @uses CreateUserAccountExtract::setFormat()
     * @param string $orgId
     * @param string $name
     * @param string $accountType
     * @param string $format
     */
    public function __construct(?string $orgId = null, ?string $name = null, ?string $accountType = null, ?string $format = null)
    {
        $this
            ->setOrgId($orgId)
            ->setName($name)
            ->setAccountType($accountType)
            ->setFormat($format);
    }
    /**
     * Get orgId value
     * @return string|null
     */
    public function getOrgId(): ?string
    {
        return $this->orgId;
    }
    /**
     * Set orgId value
     * @param string $orgId
     * @return \ID3Global\Models\CreateUserAccountExtract
     */
    public function setOrgId(?string $orgId = null): self
    {
        // validation for constraint: string
        if (!is_null($orgId) && !is_string($orgId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgId, true), gettype($orgId)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgId) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgId, true)), __LINE__);
        }
        $this->orgId = $orgId;
        
        return $this;
    }
    /**
     * Get name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->name) ? $this->name : null;
    }
    /**
     * Set name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\CreateUserAccountExtract
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->name);
        } else {
            $this->name = $name;
        }
        
        return $this;
    }
    /**
     * Get accountType value
     * @return string|null
     */
    public function getAccountType(): ?string
    {
        return $this->accountType;
    }
    /**
     * Set accountType value
     * @uses \ID3Global\Enums\Enums_UserAccountType::valueIsValid()
     * @uses \ID3Global\Enums\Enums_UserAccountType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $accountType
     * @return \ID3Global\Models\CreateUserAccountExtract
     */
    public function setAccountType(?string $accountType = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\Enums_UserAccountType::valueIsValid($accountType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\Enums_UserAccountType', is_array($accountType) ? implode(', ', $accountType) : var_export($accountType, true), implode(', ', \ID3Global\Enums\Enums_UserAccountType::getValidValues())), __LINE__);
        }
        $this->accountType = $accountType;
        
        return $this;
    }
    /**
     * Get format value
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }
    /**
     * Set format value
     * @uses \ID3Global\Enums\Enums_Format::valueIsValid()
     * @uses \ID3Global\Enums\Enums_Format::getValidValues()
     * @throws InvalidArgumentException
     * @param string $format
     * @return \ID3Global\Models\CreateUserAccountExtract
     */
    public function setFormat(?string $format = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\Enums_Format::valueIsValid($format)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\Enums_Format', is_array($format) ? implode(', ', $format) : var_export($format, true), implode(', ', \ID3Global\Enums\Enums_Format::getValidValues())), __LINE__);
        }
        $this->format = $format;
        
        return $this;
    }
}
