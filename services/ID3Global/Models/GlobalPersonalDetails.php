<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalPersonalDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q318:GlobalPersonalDetails
 * @subpackage Structs
 */
class GlobalPersonalDetails extends AbstractStructBase
{
    /**
     * The Title
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Title = null;
    /**
     * The Forename
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Forename = null;
    /**
     * The MiddleName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MiddleName = null;
    /**
     * The Surname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Surname = null;
    /**
     * The Gender
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Gender = null;
    /**
     * The DOBDay
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $DOBDay = null;
    /**
     * The DOBMonth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $DOBMonth = null;
    /**
     * The DOBYear
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $DOBYear = null;
    /**
     * The Birth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalUKBirth|null
     */
    protected ?\ID3Global\Models\GlobalUKBirth $Birth = null;
    /**
     * The CountryOfBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CountryOfBirth = null;
    /**
     * The SecondSurname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SecondSurname = null;
    /**
     * The AdditionalMiddleNames
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $AdditionalMiddleNames = null;
    /**
     * Constructor method for GlobalPersonalDetails
     * @uses GlobalPersonalDetails::setTitle()
     * @uses GlobalPersonalDetails::setForename()
     * @uses GlobalPersonalDetails::setMiddleName()
     * @uses GlobalPersonalDetails::setSurname()
     * @uses GlobalPersonalDetails::setGender()
     * @uses GlobalPersonalDetails::setDOBDay()
     * @uses GlobalPersonalDetails::setDOBMonth()
     * @uses GlobalPersonalDetails::setDOBYear()
     * @uses GlobalPersonalDetails::setBirth()
     * @uses GlobalPersonalDetails::setCountryOfBirth()
     * @uses GlobalPersonalDetails::setSecondSurname()
     * @uses GlobalPersonalDetails::setAdditionalMiddleNames()
     * @param string $title
     * @param string $forename
     * @param string $middleName
     * @param string $surname
     * @param string $gender
     * @param int $dOBDay
     * @param int $dOBMonth
     * @param int $dOBYear
     * @param \ID3Global\Models\GlobalUKBirth $birth
     * @param string $countryOfBirth
     * @param string $secondSurname
     * @param \ID3Global\Arrays\ArrayOfstring $additionalMiddleNames
     */
    public function __construct(?string $title = null, ?string $forename = null, ?string $middleName = null, ?string $surname = null, ?string $gender = null, ?int $dOBDay = null, ?int $dOBMonth = null, ?int $dOBYear = null, ?\ID3Global\Models\GlobalUKBirth $birth = null, ?string $countryOfBirth = null, ?string $secondSurname = null, ?\ID3Global\Arrays\ArrayOfstring $additionalMiddleNames = null)
    {
        $this
            ->setTitle($title)
            ->setForename($forename)
            ->setMiddleName($middleName)
            ->setSurname($surname)
            ->setGender($gender)
            ->setDOBDay($dOBDay)
            ->setDOBMonth($dOBMonth)
            ->setDOBYear($dOBYear)
            ->setBirth($birth)
            ->setCountryOfBirth($countryOfBirth)
            ->setSecondSurname($secondSurname)
            ->setAdditionalMiddleNames($additionalMiddleNames);
    }
    /**
     * Get Title value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return isset($this->Title) ? $this->Title : null;
    }
    /**
     * Set Title value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $title
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setTitle(?string $title = null): self
    {
        // validation for constraint: string
        if (!is_null($title) && !is_string($title)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($title, true), gettype($title)), __LINE__);
        }
        if (is_null($title) || (is_array($title) && empty($title))) {
            unset($this->Title);
        } else {
            $this->Title = $title;
        }
        
        return $this;
    }
    /**
     * Get Forename value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getForename(): ?string
    {
        return isset($this->Forename) ? $this->Forename : null;
    }
    /**
     * Set Forename value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $forename
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setForename(?string $forename = null): self
    {
        // validation for constraint: string
        if (!is_null($forename) && !is_string($forename)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($forename, true), gettype($forename)), __LINE__);
        }
        if (is_null($forename) || (is_array($forename) && empty($forename))) {
            unset($this->Forename);
        } else {
            $this->Forename = $forename;
        }
        
        return $this;
    }
    /**
     * Get MiddleName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return isset($this->MiddleName) ? $this->MiddleName : null;
    }
    /**
     * Set MiddleName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $middleName
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setMiddleName(?string $middleName = null): self
    {
        // validation for constraint: string
        if (!is_null($middleName) && !is_string($middleName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($middleName, true), gettype($middleName)), __LINE__);
        }
        if (is_null($middleName) || (is_array($middleName) && empty($middleName))) {
            unset($this->MiddleName);
        } else {
            $this->MiddleName = $middleName;
        }
        
        return $this;
    }
    /**
     * Get Surname value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return isset($this->Surname) ? $this->Surname : null;
    }
    /**
     * Set Surname value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $surname
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setSurname(?string $surname = null): self
    {
        // validation for constraint: string
        if (!is_null($surname) && !is_string($surname)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($surname, true), gettype($surname)), __LINE__);
        }
        if (is_null($surname) || (is_array($surname) && empty($surname))) {
            unset($this->Surname);
        } else {
            $this->Surname = $surname;
        }
        
        return $this;
    }
    /**
     * Get Gender value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGender(): ?string
    {
        return isset($this->Gender) ? $this->Gender : null;
    }
    /**
     * Set Gender value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \ID3Global\Enums\GlobalGender::valueIsValid()
     * @uses \ID3Global\Enums\GlobalGender::getValidValues()
     * @throws InvalidArgumentException
     * @param string $gender
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setGender(?string $gender = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalGender::valueIsValid($gender)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalGender', is_array($gender) ? implode(', ', $gender) : var_export($gender, true), implode(', ', \ID3Global\Enums\GlobalGender::getValidValues())), __LINE__);
        }
        if (is_null($gender) || (is_array($gender) && empty($gender))) {
            unset($this->Gender);
        } else {
            $this->Gender = $gender;
        }
        
        return $this;
    }
    /**
     * Get DOBDay value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getDOBDay(): ?int
    {
        return isset($this->DOBDay) ? $this->DOBDay : null;
    }
    /**
     * Set DOBDay value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $dOBDay
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setDOBDay(?int $dOBDay = null): self
    {
        // validation for constraint: int
        if (!is_null($dOBDay) && !(is_int($dOBDay) || ctype_digit($dOBDay))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($dOBDay, true), gettype($dOBDay)), __LINE__);
        }
        if (is_null($dOBDay) || (is_array($dOBDay) && empty($dOBDay))) {
            unset($this->DOBDay);
        } else {
            $this->DOBDay = $dOBDay;
        }
        
        return $this;
    }
    /**
     * Get DOBMonth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getDOBMonth(): ?int
    {
        return isset($this->DOBMonth) ? $this->DOBMonth : null;
    }
    /**
     * Set DOBMonth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $dOBMonth
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setDOBMonth(?int $dOBMonth = null): self
    {
        // validation for constraint: int
        if (!is_null($dOBMonth) && !(is_int($dOBMonth) || ctype_digit($dOBMonth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($dOBMonth, true), gettype($dOBMonth)), __LINE__);
        }
        if (is_null($dOBMonth) || (is_array($dOBMonth) && empty($dOBMonth))) {
            unset($this->DOBMonth);
        } else {
            $this->DOBMonth = $dOBMonth;
        }
        
        return $this;
    }
    /**
     * Get DOBYear value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getDOBYear(): ?int
    {
        return isset($this->DOBYear) ? $this->DOBYear : null;
    }
    /**
     * Set DOBYear value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $dOBYear
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setDOBYear(?int $dOBYear = null): self
    {
        // validation for constraint: int
        if (!is_null($dOBYear) && !(is_int($dOBYear) || ctype_digit($dOBYear))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($dOBYear, true), gettype($dOBYear)), __LINE__);
        }
        if (is_null($dOBYear) || (is_array($dOBYear) && empty($dOBYear))) {
            unset($this->DOBYear);
        } else {
            $this->DOBYear = $dOBYear;
        }
        
        return $this;
    }
    /**
     * Get Birth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalUKBirth|null
     */
    public function getBirth(): ?\ID3Global\Models\GlobalUKBirth
    {
        return isset($this->Birth) ? $this->Birth : null;
    }
    /**
     * Set Birth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalUKBirth $birth
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setBirth(?\ID3Global\Models\GlobalUKBirth $birth = null): self
    {
        if (is_null($birth) || (is_array($birth) && empty($birth))) {
            unset($this->Birth);
        } else {
            $this->Birth = $birth;
        }
        
        return $this;
    }
    /**
     * Get CountryOfBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryOfBirth(): ?string
    {
        return isset($this->CountryOfBirth) ? $this->CountryOfBirth : null;
    }
    /**
     * Set CountryOfBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryOfBirth
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setCountryOfBirth(?string $countryOfBirth = null): self
    {
        // validation for constraint: string
        if (!is_null($countryOfBirth) && !is_string($countryOfBirth)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryOfBirth, true), gettype($countryOfBirth)), __LINE__);
        }
        if (is_null($countryOfBirth) || (is_array($countryOfBirth) && empty($countryOfBirth))) {
            unset($this->CountryOfBirth);
        } else {
            $this->CountryOfBirth = $countryOfBirth;
        }
        
        return $this;
    }
    /**
     * Get SecondSurname value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSecondSurname(): ?string
    {
        return isset($this->SecondSurname) ? $this->SecondSurname : null;
    }
    /**
     * Set SecondSurname value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $secondSurname
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setSecondSurname(?string $secondSurname = null): self
    {
        // validation for constraint: string
        if (!is_null($secondSurname) && !is_string($secondSurname)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($secondSurname, true), gettype($secondSurname)), __LINE__);
        }
        if (is_null($secondSurname) || (is_array($secondSurname) && empty($secondSurname))) {
            unset($this->SecondSurname);
        } else {
            $this->SecondSurname = $secondSurname;
        }
        
        return $this;
    }
    /**
     * Get AdditionalMiddleNames value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getAdditionalMiddleNames(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->AdditionalMiddleNames) ? $this->AdditionalMiddleNames : null;
    }
    /**
     * Set AdditionalMiddleNames value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $additionalMiddleNames
     * @return \ID3Global\Models\GlobalPersonalDetails
     */
    public function setAdditionalMiddleNames(?\ID3Global\Arrays\ArrayOfstring $additionalMiddleNames = null): self
    {
        if (is_null($additionalMiddleNames) || (is_array($additionalMiddleNames) && empty($additionalMiddleNames))) {
            unset($this->AdditionalMiddleNames);
        } else {
            $this->AdditionalMiddleNames = $additionalMiddleNames;
        }
        
        return $this;
    }
}
