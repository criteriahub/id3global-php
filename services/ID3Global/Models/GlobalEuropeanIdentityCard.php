<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalEuropeanIdentityCard Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q363:GlobalEuropeanIdentityCard
 * @subpackage Structs
 */
class GlobalEuropeanIdentityCard extends AbstractStructBase
{
    /**
     * The Line1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Line1 = null;
    /**
     * The Line2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Line2 = null;
    /**
     * The Line3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Line3 = null;
    /**
     * The ExpiryDay
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryDay = null;
    /**
     * The ExpiryMonth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryMonth = null;
    /**
     * The ExpiryYear
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryYear = null;
    /**
     * The CountryOfNationality
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CountryOfNationality = null;
    /**
     * The CountryOfIssue
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CountryOfIssue = null;
    /**
     * Constructor method for GlobalEuropeanIdentityCard
     * @uses GlobalEuropeanIdentityCard::setLine1()
     * @uses GlobalEuropeanIdentityCard::setLine2()
     * @uses GlobalEuropeanIdentityCard::setLine3()
     * @uses GlobalEuropeanIdentityCard::setExpiryDay()
     * @uses GlobalEuropeanIdentityCard::setExpiryMonth()
     * @uses GlobalEuropeanIdentityCard::setExpiryYear()
     * @uses GlobalEuropeanIdentityCard::setCountryOfNationality()
     * @uses GlobalEuropeanIdentityCard::setCountryOfIssue()
     * @param string $line1
     * @param string $line2
     * @param string $line3
     * @param int $expiryDay
     * @param int $expiryMonth
     * @param int $expiryYear
     * @param string $countryOfNationality
     * @param string $countryOfIssue
     */
    public function __construct(?string $line1 = null, ?string $line2 = null, ?string $line3 = null, ?int $expiryDay = null, ?int $expiryMonth = null, ?int $expiryYear = null, ?string $countryOfNationality = null, ?string $countryOfIssue = null)
    {
        $this
            ->setLine1($line1)
            ->setLine2($line2)
            ->setLine3($line3)
            ->setExpiryDay($expiryDay)
            ->setExpiryMonth($expiryMonth)
            ->setExpiryYear($expiryYear)
            ->setCountryOfNationality($countryOfNationality)
            ->setCountryOfIssue($countryOfIssue);
    }
    /**
     * Get Line1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLine1(): ?string
    {
        return isset($this->Line1) ? $this->Line1 : null;
    }
    /**
     * Set Line1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $line1
     * @return \ID3Global\Models\GlobalEuropeanIdentityCard
     */
    public function setLine1(?string $line1 = null): self
    {
        // validation for constraint: string
        if (!is_null($line1) && !is_string($line1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($line1, true), gettype($line1)), __LINE__);
        }
        if (is_null($line1) || (is_array($line1) && empty($line1))) {
            unset($this->Line1);
        } else {
            $this->Line1 = $line1;
        }
        
        return $this;
    }
    /**
     * Get Line2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLine2(): ?string
    {
        return isset($this->Line2) ? $this->Line2 : null;
    }
    /**
     * Set Line2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $line2
     * @return \ID3Global\Models\GlobalEuropeanIdentityCard
     */
    public function setLine2(?string $line2 = null): self
    {
        // validation for constraint: string
        if (!is_null($line2) && !is_string($line2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($line2, true), gettype($line2)), __LINE__);
        }
        if (is_null($line2) || (is_array($line2) && empty($line2))) {
            unset($this->Line2);
        } else {
            $this->Line2 = $line2;
        }
        
        return $this;
    }
    /**
     * Get Line3 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLine3(): ?string
    {
        return isset($this->Line3) ? $this->Line3 : null;
    }
    /**
     * Set Line3 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $line3
     * @return \ID3Global\Models\GlobalEuropeanIdentityCard
     */
    public function setLine3(?string $line3 = null): self
    {
        // validation for constraint: string
        if (!is_null($line3) && !is_string($line3)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($line3, true), gettype($line3)), __LINE__);
        }
        if (is_null($line3) || (is_array($line3) && empty($line3))) {
            unset($this->Line3);
        } else {
            $this->Line3 = $line3;
        }
        
        return $this;
    }
    /**
     * Get ExpiryDay value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryDay(): ?int
    {
        return isset($this->ExpiryDay) ? $this->ExpiryDay : null;
    }
    /**
     * Set ExpiryDay value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryDay
     * @return \ID3Global\Models\GlobalEuropeanIdentityCard
     */
    public function setExpiryDay(?int $expiryDay = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryDay) && !(is_int($expiryDay) || ctype_digit($expiryDay))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryDay, true), gettype($expiryDay)), __LINE__);
        }
        if (is_null($expiryDay) || (is_array($expiryDay) && empty($expiryDay))) {
            unset($this->ExpiryDay);
        } else {
            $this->ExpiryDay = $expiryDay;
        }
        
        return $this;
    }
    /**
     * Get ExpiryMonth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryMonth(): ?int
    {
        return isset($this->ExpiryMonth) ? $this->ExpiryMonth : null;
    }
    /**
     * Set ExpiryMonth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryMonth
     * @return \ID3Global\Models\GlobalEuropeanIdentityCard
     */
    public function setExpiryMonth(?int $expiryMonth = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryMonth) && !(is_int($expiryMonth) || ctype_digit($expiryMonth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryMonth, true), gettype($expiryMonth)), __LINE__);
        }
        if (is_null($expiryMonth) || (is_array($expiryMonth) && empty($expiryMonth))) {
            unset($this->ExpiryMonth);
        } else {
            $this->ExpiryMonth = $expiryMonth;
        }
        
        return $this;
    }
    /**
     * Get ExpiryYear value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryYear(): ?int
    {
        return isset($this->ExpiryYear) ? $this->ExpiryYear : null;
    }
    /**
     * Set ExpiryYear value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryYear
     * @return \ID3Global\Models\GlobalEuropeanIdentityCard
     */
    public function setExpiryYear(?int $expiryYear = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryYear) && !(is_int($expiryYear) || ctype_digit($expiryYear))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryYear, true), gettype($expiryYear)), __LINE__);
        }
        if (is_null($expiryYear) || (is_array($expiryYear) && empty($expiryYear))) {
            unset($this->ExpiryYear);
        } else {
            $this->ExpiryYear = $expiryYear;
        }
        
        return $this;
    }
    /**
     * Get CountryOfNationality value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryOfNationality(): ?string
    {
        return isset($this->CountryOfNationality) ? $this->CountryOfNationality : null;
    }
    /**
     * Set CountryOfNationality value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryOfNationality
     * @return \ID3Global\Models\GlobalEuropeanIdentityCard
     */
    public function setCountryOfNationality(?string $countryOfNationality = null): self
    {
        // validation for constraint: string
        if (!is_null($countryOfNationality) && !is_string($countryOfNationality)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryOfNationality, true), gettype($countryOfNationality)), __LINE__);
        }
        if (is_null($countryOfNationality) || (is_array($countryOfNationality) && empty($countryOfNationality))) {
            unset($this->CountryOfNationality);
        } else {
            $this->CountryOfNationality = $countryOfNationality;
        }
        
        return $this;
    }
    /**
     * Get CountryOfIssue value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryOfIssue(): ?string
    {
        return isset($this->CountryOfIssue) ? $this->CountryOfIssue : null;
    }
    /**
     * Set CountryOfIssue value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryOfIssue
     * @return \ID3Global\Models\GlobalEuropeanIdentityCard
     */
    public function setCountryOfIssue(?string $countryOfIssue = null): self
    {
        // validation for constraint: string
        if (!is_null($countryOfIssue) && !is_string($countryOfIssue)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryOfIssue, true), gettype($countryOfIssue)), __LINE__);
        }
        if (is_null($countryOfIssue) || (is_array($countryOfIssue) && empty($countryOfIssue))) {
            unset($this->CountryOfIssue);
        } else {
            $this->CountryOfIssue = $countryOfIssue;
        }
        
        return $this;
    }
}
