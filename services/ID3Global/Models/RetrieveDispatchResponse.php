<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RetrieveDispatchResponse Models
 * @subpackage Structs
 */
class RetrieveDispatchResponse extends AbstractStructBase
{
}
