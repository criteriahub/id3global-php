<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDocImageMinSizeKBytes Models
 * @subpackage Structs
 */
class GetDocImageMinSizeKBytes extends AbstractStructBase
{
}
