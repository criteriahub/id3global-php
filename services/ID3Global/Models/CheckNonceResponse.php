<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CheckNonceResponse Models
 * @subpackage Structs
 */
class CheckNonceResponse extends AbstractStructBase
{
    /**
     * The CheckNonceResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAccount|null
     */
    protected ?\ID3Global\Models\GlobalAccount $CheckNonceResult = null;
    /**
     * Constructor method for CheckNonceResponse
     * @uses CheckNonceResponse::setCheckNonceResult()
     * @param \ID3Global\Models\GlobalAccount $checkNonceResult
     */
    public function __construct(?\ID3Global\Models\GlobalAccount $checkNonceResult = null)
    {
        $this
            ->setCheckNonceResult($checkNonceResult);
    }
    /**
     * Get CheckNonceResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAccount|null
     */
    public function getCheckNonceResult(): ?\ID3Global\Models\GlobalAccount
    {
        return isset($this->CheckNonceResult) ? $this->CheckNonceResult : null;
    }
    /**
     * Set CheckNonceResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAccount $checkNonceResult
     * @return \ID3Global\Models\CheckNonceResponse
     */
    public function setCheckNonceResult(?\ID3Global\Models\GlobalAccount $checkNonceResult = null): self
    {
        if (is_null($checkNonceResult) || (is_array($checkNonceResult) && empty($checkNonceResult))) {
            unset($this->CheckNonceResult);
        } else {
            $this->CheckNonceResult = $checkNonceResult;
        }
        
        return $this;
    }
}
