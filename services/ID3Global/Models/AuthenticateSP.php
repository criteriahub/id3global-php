<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AuthenticateSP Models
 * @subpackage Structs
 */
class AuthenticateSP extends AbstractStructBase
{
    /**
     * The ProfileIDVersion
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileIDVersion|null
     */
    protected ?\ID3Global\Models\GlobalProfileIDVersion $ProfileIDVersion = null;
    /**
     * The CustomerReference
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CustomerReference = null;
    /**
     * The InputData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalInputData|null
     */
    protected ?\ID3Global\Models\GlobalInputData $InputData = null;
    /**
     * Constructor method for AuthenticateSP
     * @uses AuthenticateSP::setProfileIDVersion()
     * @uses AuthenticateSP::setCustomerReference()
     * @uses AuthenticateSP::setInputData()
     * @param \ID3Global\Models\GlobalProfileIDVersion $profileIDVersion
     * @param string $customerReference
     * @param \ID3Global\Models\GlobalInputData $inputData
     */
    public function __construct(?\ID3Global\Models\GlobalProfileIDVersion $profileIDVersion = null, ?string $customerReference = null, ?\ID3Global\Models\GlobalInputData $inputData = null)
    {
        $this
            ->setProfileIDVersion($profileIDVersion)
            ->setCustomerReference($customerReference)
            ->setInputData($inputData);
    }
    /**
     * Get ProfileIDVersion value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileIDVersion|null
     */
    public function getProfileIDVersion(): ?\ID3Global\Models\GlobalProfileIDVersion
    {
        return isset($this->ProfileIDVersion) ? $this->ProfileIDVersion : null;
    }
    /**
     * Set ProfileIDVersion value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileIDVersion $profileIDVersion
     * @return \ID3Global\Models\AuthenticateSP
     */
    public function setProfileIDVersion(?\ID3Global\Models\GlobalProfileIDVersion $profileIDVersion = null): self
    {
        if (is_null($profileIDVersion) || (is_array($profileIDVersion) && empty($profileIDVersion))) {
            unset($this->ProfileIDVersion);
        } else {
            $this->ProfileIDVersion = $profileIDVersion;
        }
        
        return $this;
    }
    /**
     * Get CustomerReference value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCustomerReference(): ?string
    {
        return isset($this->CustomerReference) ? $this->CustomerReference : null;
    }
    /**
     * Set CustomerReference value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $customerReference
     * @return \ID3Global\Models\AuthenticateSP
     */
    public function setCustomerReference(?string $customerReference = null): self
    {
        // validation for constraint: string
        if (!is_null($customerReference) && !is_string($customerReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($customerReference, true), gettype($customerReference)), __LINE__);
        }
        if (is_null($customerReference) || (is_array($customerReference) && empty($customerReference))) {
            unset($this->CustomerReference);
        } else {
            $this->CustomerReference = $customerReference;
        }
        
        return $this;
    }
    /**
     * Get InputData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalInputData|null
     */
    public function getInputData(): ?\ID3Global\Models\GlobalInputData
    {
        return isset($this->InputData) ? $this->InputData : null;
    }
    /**
     * Set InputData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalInputData $inputData
     * @return \ID3Global\Models\AuthenticateSP
     */
    public function setInputData(?\ID3Global\Models\GlobalInputData $inputData = null): self
    {
        if (is_null($inputData) || (is_array($inputData) && empty($inputData))) {
            unset($this->InputData);
        } else {
            $this->InputData = $inputData;
        }
        
        return $this;
    }
}
