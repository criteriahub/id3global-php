<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseConsent Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q852:GlobalCaseConsent
 * @subpackage Structs
 */
class GlobalCaseConsent extends AbstractStructBase
{
    /**
     * The Key
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Key = null;
    /**
     * The Verdict
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Verdict = null;
    /**
     * The Action
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Action = null;
    /**
     * The ConsentedByAccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ConsentedByAccountID = null;
    /**
     * The Reasons
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $Reasons = null;
    /**
     * The Description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Description = null;
    /**
     * Constructor method for GlobalCaseConsent
     * @uses GlobalCaseConsent::setKey()
     * @uses GlobalCaseConsent::setVerdict()
     * @uses GlobalCaseConsent::setAction()
     * @uses GlobalCaseConsent::setConsentedByAccountID()
     * @uses GlobalCaseConsent::setReasons()
     * @uses GlobalCaseConsent::setDescription()
     * @param string $key
     * @param string $verdict
     * @param int $action
     * @param string $consentedByAccountID
     * @param \ID3Global\Arrays\ArrayOfstring $reasons
     * @param string $description
     */
    public function __construct(?string $key = null, ?string $verdict = null, ?int $action = null, ?string $consentedByAccountID = null, ?\ID3Global\Arrays\ArrayOfstring $reasons = null, ?string $description = null)
    {
        $this
            ->setKey($key)
            ->setVerdict($verdict)
            ->setAction($action)
            ->setConsentedByAccountID($consentedByAccountID)
            ->setReasons($reasons)
            ->setDescription($description);
    }
    /**
     * Get Key value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getKey(): ?string
    {
        return isset($this->Key) ? $this->Key : null;
    }
    /**
     * Set Key value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $key
     * @return \ID3Global\Models\GlobalCaseConsent
     */
    public function setKey(?string $key = null): self
    {
        // validation for constraint: string
        if (!is_null($key) && !is_string($key)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($key, true), gettype($key)), __LINE__);
        }
        if (is_null($key) || (is_array($key) && empty($key))) {
            unset($this->Key);
        } else {
            $this->Key = $key;
        }
        
        return $this;
    }
    /**
     * Get Verdict value
     * @return string|null
     */
    public function getVerdict(): ?string
    {
        return $this->Verdict;
    }
    /**
     * Set Verdict value
     * @uses \ID3Global\Enums\GlobalConsentVerdict::valueIsValid()
     * @uses \ID3Global\Enums\GlobalConsentVerdict::getValidValues()
     * @throws InvalidArgumentException
     * @param string $verdict
     * @return \ID3Global\Models\GlobalCaseConsent
     */
    public function setVerdict(?string $verdict = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalConsentVerdict::valueIsValid($verdict)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalConsentVerdict', is_array($verdict) ? implode(', ', $verdict) : var_export($verdict, true), implode(', ', \ID3Global\Enums\GlobalConsentVerdict::getValidValues())), __LINE__);
        }
        $this->Verdict = $verdict;
        
        return $this;
    }
    /**
     * Get Action value
     * @return int|null
     */
    public function getAction(): ?int
    {
        return $this->Action;
    }
    /**
     * Set Action value
     * @param int $action
     * @return \ID3Global\Models\GlobalCaseConsent
     */
    public function setAction(?int $action = null): self
    {
        // validation for constraint: int
        if (!is_null($action) && !(is_int($action) || ctype_digit($action))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($action, true), gettype($action)), __LINE__);
        }
        $this->Action = $action;
        
        return $this;
    }
    /**
     * Get ConsentedByAccountID value
     * @return string|null
     */
    public function getConsentedByAccountID(): ?string
    {
        return $this->ConsentedByAccountID;
    }
    /**
     * Set ConsentedByAccountID value
     * @param string $consentedByAccountID
     * @return \ID3Global\Models\GlobalCaseConsent
     */
    public function setConsentedByAccountID(?string $consentedByAccountID = null): self
    {
        // validation for constraint: string
        if (!is_null($consentedByAccountID) && !is_string($consentedByAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($consentedByAccountID, true), gettype($consentedByAccountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($consentedByAccountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $consentedByAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($consentedByAccountID, true)), __LINE__);
        }
        $this->ConsentedByAccountID = $consentedByAccountID;
        
        return $this;
    }
    /**
     * Get Reasons value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getReasons(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->Reasons) ? $this->Reasons : null;
    }
    /**
     * Set Reasons value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $reasons
     * @return \ID3Global\Models\GlobalCaseConsent
     */
    public function setReasons(?\ID3Global\Arrays\ArrayOfstring $reasons = null): self
    {
        if (is_null($reasons) || (is_array($reasons) && empty($reasons))) {
            unset($this->Reasons);
        } else {
            $this->Reasons = $reasons;
        }
        
        return $this;
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \ID3Global\Models\GlobalCaseConsent
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        
        return $this;
    }
}
