<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSanctionsEnforcementData Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q477:GlobalSanctionsEnforcementData
 * @subpackage Structs
 */
class GlobalSanctionsEnforcementData extends AbstractStructBase
{
    /**
     * The Fullname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Fullname = null;
    /**
     * The SanctionsBodies
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $SanctionsBodies = null;
    /**
     * The Aliases
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $Aliases = null;
    /**
     * The SanctionsAddresses
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSanctionsAddress|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSanctionsAddress $SanctionsAddresses = null;
    /**
     * The SanctionsDates
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSanctionsDate|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSanctionsDate $SanctionsDates = null;
    /**
     * The IdentityInformation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $IdentityInformation = null;
    /**
     * Constructor method for GlobalSanctionsEnforcementData
     * @uses GlobalSanctionsEnforcementData::setFullname()
     * @uses GlobalSanctionsEnforcementData::setSanctionsBodies()
     * @uses GlobalSanctionsEnforcementData::setAliases()
     * @uses GlobalSanctionsEnforcementData::setSanctionsAddresses()
     * @uses GlobalSanctionsEnforcementData::setSanctionsDates()
     * @uses GlobalSanctionsEnforcementData::setIdentityInformation()
     * @param string $fullname
     * @param \ID3Global\Arrays\ArrayOfstring $sanctionsBodies
     * @param \ID3Global\Arrays\ArrayOfstring $aliases
     * @param \ID3Global\Arrays\ArrayOfGlobalSanctionsAddress $sanctionsAddresses
     * @param \ID3Global\Arrays\ArrayOfGlobalSanctionsDate $sanctionsDates
     * @param \ID3Global\Arrays\ArrayOfstring $identityInformation
     */
    public function __construct(?string $fullname = null, ?\ID3Global\Arrays\ArrayOfstring $sanctionsBodies = null, ?\ID3Global\Arrays\ArrayOfstring $aliases = null, ?\ID3Global\Arrays\ArrayOfGlobalSanctionsAddress $sanctionsAddresses = null, ?\ID3Global\Arrays\ArrayOfGlobalSanctionsDate $sanctionsDates = null, ?\ID3Global\Arrays\ArrayOfstring $identityInformation = null)
    {
        $this
            ->setFullname($fullname)
            ->setSanctionsBodies($sanctionsBodies)
            ->setAliases($aliases)
            ->setSanctionsAddresses($sanctionsAddresses)
            ->setSanctionsDates($sanctionsDates)
            ->setIdentityInformation($identityInformation);
    }
    /**
     * Get Fullname value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFullname(): ?string
    {
        return isset($this->Fullname) ? $this->Fullname : null;
    }
    /**
     * Set Fullname value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fullname
     * @return \ID3Global\Models\GlobalSanctionsEnforcementData
     */
    public function setFullname(?string $fullname = null): self
    {
        // validation for constraint: string
        if (!is_null($fullname) && !is_string($fullname)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fullname, true), gettype($fullname)), __LINE__);
        }
        if (is_null($fullname) || (is_array($fullname) && empty($fullname))) {
            unset($this->Fullname);
        } else {
            $this->Fullname = $fullname;
        }
        
        return $this;
    }
    /**
     * Get SanctionsBodies value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getSanctionsBodies(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->SanctionsBodies) ? $this->SanctionsBodies : null;
    }
    /**
     * Set SanctionsBodies value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $sanctionsBodies
     * @return \ID3Global\Models\GlobalSanctionsEnforcementData
     */
    public function setSanctionsBodies(?\ID3Global\Arrays\ArrayOfstring $sanctionsBodies = null): self
    {
        if (is_null($sanctionsBodies) || (is_array($sanctionsBodies) && empty($sanctionsBodies))) {
            unset($this->SanctionsBodies);
        } else {
            $this->SanctionsBodies = $sanctionsBodies;
        }
        
        return $this;
    }
    /**
     * Get Aliases value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getAliases(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->Aliases) ? $this->Aliases : null;
    }
    /**
     * Set Aliases value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $aliases
     * @return \ID3Global\Models\GlobalSanctionsEnforcementData
     */
    public function setAliases(?\ID3Global\Arrays\ArrayOfstring $aliases = null): self
    {
        if (is_null($aliases) || (is_array($aliases) && empty($aliases))) {
            unset($this->Aliases);
        } else {
            $this->Aliases = $aliases;
        }
        
        return $this;
    }
    /**
     * Get SanctionsAddresses value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsAddress|null
     */
    public function getSanctionsAddresses(): ?\ID3Global\Arrays\ArrayOfGlobalSanctionsAddress
    {
        return isset($this->SanctionsAddresses) ? $this->SanctionsAddresses : null;
    }
    /**
     * Set SanctionsAddresses value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSanctionsAddress $sanctionsAddresses
     * @return \ID3Global\Models\GlobalSanctionsEnforcementData
     */
    public function setSanctionsAddresses(?\ID3Global\Arrays\ArrayOfGlobalSanctionsAddress $sanctionsAddresses = null): self
    {
        if (is_null($sanctionsAddresses) || (is_array($sanctionsAddresses) && empty($sanctionsAddresses))) {
            unset($this->SanctionsAddresses);
        } else {
            $this->SanctionsAddresses = $sanctionsAddresses;
        }
        
        return $this;
    }
    /**
     * Get SanctionsDates value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsDate|null
     */
    public function getSanctionsDates(): ?\ID3Global\Arrays\ArrayOfGlobalSanctionsDate
    {
        return isset($this->SanctionsDates) ? $this->SanctionsDates : null;
    }
    /**
     * Set SanctionsDates value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSanctionsDate $sanctionsDates
     * @return \ID3Global\Models\GlobalSanctionsEnforcementData
     */
    public function setSanctionsDates(?\ID3Global\Arrays\ArrayOfGlobalSanctionsDate $sanctionsDates = null): self
    {
        if (is_null($sanctionsDates) || (is_array($sanctionsDates) && empty($sanctionsDates))) {
            unset($this->SanctionsDates);
        } else {
            $this->SanctionsDates = $sanctionsDates;
        }
        
        return $this;
    }
    /**
     * Get IdentityInformation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getIdentityInformation(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->IdentityInformation) ? $this->IdentityInformation : null;
    }
    /**
     * Set IdentityInformation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $identityInformation
     * @return \ID3Global\Models\GlobalSanctionsEnforcementData
     */
    public function setIdentityInformation(?\ID3Global\Arrays\ArrayOfstring $identityInformation = null): self
    {
        if (is_null($identityInformation) || (is_array($identityInformation) && empty($identityInformation))) {
            unset($this->IdentityInformation);
        } else {
            $this->IdentityInformation = $identityInformation;
        }
        
        return $this;
    }
}
