<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCreditMatches Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q77:GlobalCreditMatches
 * @subpackage Structs
 */
class GlobalCreditMatches extends AbstractStructBase
{
    /**
     * The LenderType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $LenderType = null;
    /**
     * The AccountType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccountType = null;
    /**
     * The StartDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $StartDate = null;
    /**
     * The EndDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $EndDate = null;
    /**
     * Constructor method for GlobalCreditMatches
     * @uses GlobalCreditMatches::setLenderType()
     * @uses GlobalCreditMatches::setAccountType()
     * @uses GlobalCreditMatches::setStartDate()
     * @uses GlobalCreditMatches::setEndDate()
     * @param string $lenderType
     * @param string $accountType
     * @param string $startDate
     * @param string $endDate
     */
    public function __construct(?string $lenderType = null, ?string $accountType = null, ?string $startDate = null, ?string $endDate = null)
    {
        $this
            ->setLenderType($lenderType)
            ->setAccountType($accountType)
            ->setStartDate($startDate)
            ->setEndDate($endDate);
    }
    /**
     * Get LenderType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLenderType(): ?string
    {
        return isset($this->LenderType) ? $this->LenderType : null;
    }
    /**
     * Set LenderType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $lenderType
     * @return \ID3Global\Models\GlobalCreditMatches
     */
    public function setLenderType(?string $lenderType = null): self
    {
        // validation for constraint: string
        if (!is_null($lenderType) && !is_string($lenderType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lenderType, true), gettype($lenderType)), __LINE__);
        }
        if (is_null($lenderType) || (is_array($lenderType) && empty($lenderType))) {
            unset($this->LenderType);
        } else {
            $this->LenderType = $lenderType;
        }
        
        return $this;
    }
    /**
     * Get AccountType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccountType(): ?string
    {
        return isset($this->AccountType) ? $this->AccountType : null;
    }
    /**
     * Set AccountType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accountType
     * @return \ID3Global\Models\GlobalCreditMatches
     */
    public function setAccountType(?string $accountType = null): self
    {
        // validation for constraint: string
        if (!is_null($accountType) && !is_string($accountType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountType, true), gettype($accountType)), __LINE__);
        }
        if (is_null($accountType) || (is_array($accountType) && empty($accountType))) {
            unset($this->AccountType);
        } else {
            $this->AccountType = $accountType;
        }
        
        return $this;
    }
    /**
     * Get StartDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStartDate(): ?string
    {
        return isset($this->StartDate) ? $this->StartDate : null;
    }
    /**
     * Set StartDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $startDate
     * @return \ID3Global\Models\GlobalCreditMatches
     */
    public function setStartDate(?string $startDate = null): self
    {
        // validation for constraint: string
        if (!is_null($startDate) && !is_string($startDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startDate, true), gettype($startDate)), __LINE__);
        }
        if (is_null($startDate) || (is_array($startDate) && empty($startDate))) {
            unset($this->StartDate);
        } else {
            $this->StartDate = $startDate;
        }
        
        return $this;
    }
    /**
     * Get EndDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEndDate(): ?string
    {
        return isset($this->EndDate) ? $this->EndDate : null;
    }
    /**
     * Set EndDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $endDate
     * @return \ID3Global\Models\GlobalCreditMatches
     */
    public function setEndDate(?string $endDate = null): self
    {
        // validation for constraint: string
        if (!is_null($endDate) && !is_string($endDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endDate, true), gettype($endDate)), __LINE__);
        }
        if (is_null($endDate) || (is_array($endDate) && empty($endDate))) {
            unset($this->EndDate);
        } else {
            $this->EndDate = $endDate;
        }
        
        return $this;
    }
}
