<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCardTypesMPResponse Models
 * @subpackage Structs
 */
class GetCardTypesMPResponse extends AbstractStructBase
{
    /**
     * The GetCardTypesMPResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCardType|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCardType $GetCardTypesMPResult = null;
    /**
     * Constructor method for GetCardTypesMPResponse
     * @uses GetCardTypesMPResponse::setGetCardTypesMPResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalCardType $getCardTypesMPResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCardType $getCardTypesMPResult = null)
    {
        $this
            ->setGetCardTypesMPResult($getCardTypesMPResult);
    }
    /**
     * Get GetCardTypesMPResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCardType|null
     */
    public function getGetCardTypesMPResult(): ?\ID3Global\Arrays\ArrayOfGlobalCardType
    {
        return isset($this->GetCardTypesMPResult) ? $this->GetCardTypesMPResult : null;
    }
    /**
     * Set GetCardTypesMPResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCardType $getCardTypesMPResult
     * @return \ID3Global\Models\GetCardTypesMPResponse
     */
    public function setGetCardTypesMPResult(?\ID3Global\Arrays\ArrayOfGlobalCardType $getCardTypesMPResult = null): self
    {
        if (is_null($getCardTypesMPResult) || (is_array($getCardTypesMPResult) && empty($getCardTypesMPResult))) {
            unset($this->GetCardTypesMPResult);
        } else {
            $this->GetCardTypesMPResult = $getCardTypesMPResult;
        }
        
        return $this;
    }
}
