<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetOrgAdminAccountID Models
 * @subpackage Structs
 */
class GetOrgAdminAccountID extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The Product
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Product = null;
    /**
     * Constructor method for GetOrgAdminAccountID
     * @uses GetOrgAdminAccountID::setOrgID()
     * @uses GetOrgAdminAccountID::setProduct()
     * @param string $orgID
     * @param string $product
     */
    public function __construct(?string $orgID = null, ?string $product = null)
    {
        $this
            ->setOrgID($orgID)
            ->setProduct($product);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetOrgAdminAccountID
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get Product value
     * @return string|null
     */
    public function getProduct(): ?string
    {
        return $this->Product;
    }
    /**
     * Set Product value
     * @uses \ID3Global\Enums\GlobalProduct::valueIsValid()
     * @uses \ID3Global\Enums\GlobalProduct::getValidValues()
     * @throws InvalidArgumentException
     * @param string $product
     * @return \ID3Global\Models\GetOrgAdminAccountID
     */
    public function setProduct(?string $product = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalProduct::valueIsValid($product)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalProduct', is_array($product) ? implode(', ', $product) : var_export($product, true), implode(', ', \ID3Global\Enums\GlobalProduct::getValidValues())), __LINE__);
        }
        $this->Product = $product;
        
        return $this;
    }
}
