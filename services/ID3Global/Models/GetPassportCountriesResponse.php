<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPassportCountriesResponse Models
 * @subpackage Structs
 */
class GetPassportCountriesResponse extends AbstractStructBase
{
    /**
     * The GetPassportCountriesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCountry|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCountry $GetPassportCountriesResult = null;
    /**
     * Constructor method for GetPassportCountriesResponse
     * @uses GetPassportCountriesResponse::setGetPassportCountriesResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalCountry $getPassportCountriesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCountry $getPassportCountriesResult = null)
    {
        $this
            ->setGetPassportCountriesResult($getPassportCountriesResult);
    }
    /**
     * Get GetPassportCountriesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCountry|null
     */
    public function getGetPassportCountriesResult(): ?\ID3Global\Arrays\ArrayOfGlobalCountry
    {
        return isset($this->GetPassportCountriesResult) ? $this->GetPassportCountriesResult : null;
    }
    /**
     * Set GetPassportCountriesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCountry $getPassportCountriesResult
     * @return \ID3Global\Models\GetPassportCountriesResponse
     */
    public function setGetPassportCountriesResult(?\ID3Global\Arrays\ArrayOfGlobalCountry $getPassportCountriesResult = null): self
    {
        if (is_null($getPassportCountriesResult) || (is_array($getPassportCountriesResult) && empty($getPassportCountriesResult))) {
            unset($this->GetPassportCountriesResult);
        } else {
            $this->GetPassportCountriesResult = $getPassportCountriesResult;
        }
        
        return $this;
    }
}
