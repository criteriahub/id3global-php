<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q3:GlobalAccount
 * @subpackage Structs
 */
class GlobalAccount extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The OrgName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OrgName = null;
    /**
     * The AccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AccountID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The DomainName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DomainName = null;
    /**
     * The Email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Email = null;
    /**
     * The Created
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Created = null;
    /**
     * The LastSignInSuccess
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $LastSignInSuccess = null;
    /**
     * The LastSignInFailed
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $LastSignInFailed = null;
    /**
     * The Active
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Active = null;
    /**
     * The Locked
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Locked = null;
    /**
     * The System
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $System = null;
    /**
     * The Products
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $Products = null;
    /**
     * The SelfCare
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $SelfCare = null;
    /**
     * The AuthenticationCount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $AuthenticationCount = null;
    /**
     * The AddressLookUpCount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $AddressLookUpCount = null;
    /**
     * The PasswordNeverExpires_Internal
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $PasswordNeverExpires_Internal = null;
    /**
     * Constructor method for GlobalAccount
     * @uses GlobalAccount::setOrgID()
     * @uses GlobalAccount::setOrgName()
     * @uses GlobalAccount::setAccountID()
     * @uses GlobalAccount::setName()
     * @uses GlobalAccount::setUsername()
     * @uses GlobalAccount::setDomainName()
     * @uses GlobalAccount::setEmail()
     * @uses GlobalAccount::setCreated()
     * @uses GlobalAccount::setLastSignInSuccess()
     * @uses GlobalAccount::setLastSignInFailed()
     * @uses GlobalAccount::setActive()
     * @uses GlobalAccount::setLocked()
     * @uses GlobalAccount::setSystem()
     * @uses GlobalAccount::setProducts()
     * @uses GlobalAccount::setSelfCare()
     * @uses GlobalAccount::setAuthenticationCount()
     * @uses GlobalAccount::setAddressLookUpCount()
     * @uses GlobalAccount::setPasswordNeverExpires_Internal()
     * @param string $orgID
     * @param string $orgName
     * @param string $accountID
     * @param string $name
     * @param string $username
     * @param string $domainName
     * @param string $email
     * @param string $created
     * @param string $lastSignInSuccess
     * @param string $lastSignInFailed
     * @param bool $active
     * @param bool $locked
     * @param bool $system
     * @param \ID3Global\Arrays\ArrayOfstring $products
     * @param bool $selfCare
     * @param int $authenticationCount
     * @param int $addressLookUpCount
     * @param bool $passwordNeverExpires_Internal
     */
    public function __construct(?string $orgID = null, ?string $orgName = null, ?string $accountID = null, ?string $name = null, ?string $username = null, ?string $domainName = null, ?string $email = null, ?string $created = null, ?string $lastSignInSuccess = null, ?string $lastSignInFailed = null, ?bool $active = null, ?bool $locked = null, ?bool $system = null, ?\ID3Global\Arrays\ArrayOfstring $products = null, ?bool $selfCare = null, ?int $authenticationCount = null, ?int $addressLookUpCount = null, ?bool $passwordNeverExpires_Internal = null)
    {
        $this
            ->setOrgID($orgID)
            ->setOrgName($orgName)
            ->setAccountID($accountID)
            ->setName($name)
            ->setUsername($username)
            ->setDomainName($domainName)
            ->setEmail($email)
            ->setCreated($created)
            ->setLastSignInSuccess($lastSignInSuccess)
            ->setLastSignInFailed($lastSignInFailed)
            ->setActive($active)
            ->setLocked($locked)
            ->setSystem($system)
            ->setProducts($products)
            ->setSelfCare($selfCare)
            ->setAuthenticationCount($authenticationCount)
            ->setAddressLookUpCount($addressLookUpCount)
            ->setPasswordNeverExpires_Internal($passwordNeverExpires_Internal);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get OrgName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrgName(): ?string
    {
        return isset($this->OrgName) ? $this->OrgName : null;
    }
    /**
     * Set OrgName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $orgName
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setOrgName(?string $orgName = null): self
    {
        // validation for constraint: string
        if (!is_null($orgName) && !is_string($orgName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgName, true), gettype($orgName)), __LINE__);
        }
        if (is_null($orgName) || (is_array($orgName) && empty($orgName))) {
            unset($this->OrgName);
        } else {
            $this->OrgName = $orgName;
        }
        
        return $this;
    }
    /**
     * Get AccountID value
     * @return string|null
     */
    public function getAccountID(): ?string
    {
        return $this->AccountID;
    }
    /**
     * Set AccountID value
     * @param string $accountID
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setAccountID(?string $accountID = null): self
    {
        // validation for constraint: string
        if (!is_null($accountID) && !is_string($accountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountID, true), gettype($accountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($accountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $accountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($accountID, true)), __LINE__);
        }
        $this->AccountID = $accountID;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Username value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return isset($this->Username) ? $this->Username : null;
    }
    /**
     * Set Username value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        if (is_null($username) || (is_array($username) && empty($username))) {
            unset($this->Username);
        } else {
            $this->Username = $username;
        }
        
        return $this;
    }
    /**
     * Get DomainName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDomainName(): ?string
    {
        return isset($this->DomainName) ? $this->DomainName : null;
    }
    /**
     * Set DomainName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $domainName
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setDomainName(?string $domainName = null): self
    {
        // validation for constraint: string
        if (!is_null($domainName) && !is_string($domainName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domainName, true), gettype($domainName)), __LINE__);
        }
        if (is_null($domainName) || (is_array($domainName) && empty($domainName))) {
            unset($this->DomainName);
        } else {
            $this->DomainName = $domainName;
        }
        
        return $this;
    }
    /**
     * Get Email value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return isset($this->Email) ? $this->Email : null;
    }
    /**
     * Set Email value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setEmail(?string $email = null): self
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        if (is_null($email) || (is_array($email) && empty($email))) {
            unset($this->Email);
        } else {
            $this->Email = $email;
        }
        
        return $this;
    }
    /**
     * Get Created value
     * @return string|null
     */
    public function getCreated(): ?string
    {
        return $this->Created;
    }
    /**
     * Set Created value
     * @param string $created
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setCreated(?string $created = null): self
    {
        // validation for constraint: string
        if (!is_null($created) && !is_string($created)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($created, true), gettype($created)), __LINE__);
        }
        $this->Created = $created;
        
        return $this;
    }
    /**
     * Get LastSignInSuccess value
     * @return string|null
     */
    public function getLastSignInSuccess(): ?string
    {
        return $this->LastSignInSuccess;
    }
    /**
     * Set LastSignInSuccess value
     * @param string $lastSignInSuccess
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setLastSignInSuccess(?string $lastSignInSuccess = null): self
    {
        // validation for constraint: string
        if (!is_null($lastSignInSuccess) && !is_string($lastSignInSuccess)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastSignInSuccess, true), gettype($lastSignInSuccess)), __LINE__);
        }
        $this->LastSignInSuccess = $lastSignInSuccess;
        
        return $this;
    }
    /**
     * Get LastSignInFailed value
     * @return string|null
     */
    public function getLastSignInFailed(): ?string
    {
        return $this->LastSignInFailed;
    }
    /**
     * Set LastSignInFailed value
     * @param string $lastSignInFailed
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setLastSignInFailed(?string $lastSignInFailed = null): self
    {
        // validation for constraint: string
        if (!is_null($lastSignInFailed) && !is_string($lastSignInFailed)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastSignInFailed, true), gettype($lastSignInFailed)), __LINE__);
        }
        $this->LastSignInFailed = $lastSignInFailed;
        
        return $this;
    }
    /**
     * Get Active value
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->Active;
    }
    /**
     * Set Active value
     * @param bool $active
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setActive(?bool $active = null): self
    {
        // validation for constraint: boolean
        if (!is_null($active) && !is_bool($active)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($active, true), gettype($active)), __LINE__);
        }
        $this->Active = $active;
        
        return $this;
    }
    /**
     * Get Locked value
     * @return bool|null
     */
    public function getLocked(): ?bool
    {
        return $this->Locked;
    }
    /**
     * Set Locked value
     * @param bool $locked
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setLocked(?bool $locked = null): self
    {
        // validation for constraint: boolean
        if (!is_null($locked) && !is_bool($locked)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($locked, true), gettype($locked)), __LINE__);
        }
        $this->Locked = $locked;
        
        return $this;
    }
    /**
     * Get System value
     * @return bool|null
     */
    public function getSystem(): ?bool
    {
        return $this->System;
    }
    /**
     * Set System value
     * @param bool $system
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setSystem(?bool $system = null): self
    {
        // validation for constraint: boolean
        if (!is_null($system) && !is_bool($system)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($system, true), gettype($system)), __LINE__);
        }
        $this->System = $system;
        
        return $this;
    }
    /**
     * Get Products value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getProducts(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->Products) ? $this->Products : null;
    }
    /**
     * Set Products value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $products
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setProducts(?\ID3Global\Arrays\ArrayOfstring $products = null): self
    {
        if (is_null($products) || (is_array($products) && empty($products))) {
            unset($this->Products);
        } else {
            $this->Products = $products;
        }
        
        return $this;
    }
    /**
     * Get SelfCare value
     * @return bool|null
     */
    public function getSelfCare(): ?bool
    {
        return $this->SelfCare;
    }
    /**
     * Set SelfCare value
     * @param bool $selfCare
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setSelfCare(?bool $selfCare = null): self
    {
        // validation for constraint: boolean
        if (!is_null($selfCare) && !is_bool($selfCare)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($selfCare, true), gettype($selfCare)), __LINE__);
        }
        $this->SelfCare = $selfCare;
        
        return $this;
    }
    /**
     * Get AuthenticationCount value
     * @return int|null
     */
    public function getAuthenticationCount(): ?int
    {
        return $this->AuthenticationCount;
    }
    /**
     * Set AuthenticationCount value
     * @param int $authenticationCount
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setAuthenticationCount(?int $authenticationCount = null): self
    {
        // validation for constraint: int
        if (!is_null($authenticationCount) && !(is_int($authenticationCount) || ctype_digit($authenticationCount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($authenticationCount, true), gettype($authenticationCount)), __LINE__);
        }
        $this->AuthenticationCount = $authenticationCount;
        
        return $this;
    }
    /**
     * Get AddressLookUpCount value
     * @return int|null
     */
    public function getAddressLookUpCount(): ?int
    {
        return $this->AddressLookUpCount;
    }
    /**
     * Set AddressLookUpCount value
     * @param int $addressLookUpCount
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setAddressLookUpCount(?int $addressLookUpCount = null): self
    {
        // validation for constraint: int
        if (!is_null($addressLookUpCount) && !(is_int($addressLookUpCount) || ctype_digit($addressLookUpCount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($addressLookUpCount, true), gettype($addressLookUpCount)), __LINE__);
        }
        $this->AddressLookUpCount = $addressLookUpCount;
        
        return $this;
    }
    /**
     * Get PasswordNeverExpires_Internal value
     * @return bool|null
     */
    public function getPasswordNeverExpires_Internal(): ?bool
    {
        return $this->PasswordNeverExpires_Internal;
    }
    /**
     * Set PasswordNeverExpires_Internal value
     * @param bool $passwordNeverExpires_Internal
     * @return \ID3Global\Models\GlobalAccount
     */
    public function setPasswordNeverExpires_Internal(?bool $passwordNeverExpires_Internal = null): self
    {
        // validation for constraint: boolean
        if (!is_null($passwordNeverExpires_Internal) && !is_bool($passwordNeverExpires_Internal)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($passwordNeverExpires_Internal, true), gettype($passwordNeverExpires_Internal)), __LINE__);
        }
        $this->PasswordNeverExpires_Internal = $passwordNeverExpires_Internal;
        
        return $this;
    }
}
