<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateOrganisation Models
 * @subpackage Structs
 */
class CreateOrganisation extends AbstractStructBase
{
    /**
     * The ParentOrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ParentOrgID = null;
    /**
     * The OrganisationDetails
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalOrganisationDetails|null
     */
    protected ?\ID3Global\Models\GlobalOrganisationDetails $OrganisationDetails = null;
    /**
     * The AdminName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AdminName = null;
    /**
     * The AdminUsername
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AdminUsername = null;
    /**
     * The AdminEmail
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AdminEmail = null;
    /**
     * The AdminPassword
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AdminPassword = null;
    /**
     * The AdminPIN
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AdminPIN = null;
    /**
     * Constructor method for CreateOrganisation
     * @uses CreateOrganisation::setParentOrgID()
     * @uses CreateOrganisation::setOrganisationDetails()
     * @uses CreateOrganisation::setAdminName()
     * @uses CreateOrganisation::setAdminUsername()
     * @uses CreateOrganisation::setAdminEmail()
     * @uses CreateOrganisation::setAdminPassword()
     * @uses CreateOrganisation::setAdminPIN()
     * @param string $parentOrgID
     * @param \ID3Global\Models\GlobalOrganisationDetails $organisationDetails
     * @param string $adminName
     * @param string $adminUsername
     * @param string $adminEmail
     * @param string $adminPassword
     * @param string $adminPIN
     */
    public function __construct(?string $parentOrgID = null, ?\ID3Global\Models\GlobalOrganisationDetails $organisationDetails = null, ?string $adminName = null, ?string $adminUsername = null, ?string $adminEmail = null, ?string $adminPassword = null, ?string $adminPIN = null)
    {
        $this
            ->setParentOrgID($parentOrgID)
            ->setOrganisationDetails($organisationDetails)
            ->setAdminName($adminName)
            ->setAdminUsername($adminUsername)
            ->setAdminEmail($adminEmail)
            ->setAdminPassword($adminPassword)
            ->setAdminPIN($adminPIN);
    }
    /**
     * Get ParentOrgID value
     * @return string|null
     */
    public function getParentOrgID(): ?string
    {
        return $this->ParentOrgID;
    }
    /**
     * Set ParentOrgID value
     * @param string $parentOrgID
     * @return \ID3Global\Models\CreateOrganisation
     */
    public function setParentOrgID(?string $parentOrgID = null): self
    {
        // validation for constraint: string
        if (!is_null($parentOrgID) && !is_string($parentOrgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parentOrgID, true), gettype($parentOrgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($parentOrgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $parentOrgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($parentOrgID, true)), __LINE__);
        }
        $this->ParentOrgID = $parentOrgID;
        
        return $this;
    }
    /**
     * Get OrganisationDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalOrganisationDetails|null
     */
    public function getOrganisationDetails(): ?\ID3Global\Models\GlobalOrganisationDetails
    {
        return isset($this->OrganisationDetails) ? $this->OrganisationDetails : null;
    }
    /**
     * Set OrganisationDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalOrganisationDetails $organisationDetails
     * @return \ID3Global\Models\CreateOrganisation
     */
    public function setOrganisationDetails(?\ID3Global\Models\GlobalOrganisationDetails $organisationDetails = null): self
    {
        if (is_null($organisationDetails) || (is_array($organisationDetails) && empty($organisationDetails))) {
            unset($this->OrganisationDetails);
        } else {
            $this->OrganisationDetails = $organisationDetails;
        }
        
        return $this;
    }
    /**
     * Get AdminName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdminName(): ?string
    {
        return isset($this->AdminName) ? $this->AdminName : null;
    }
    /**
     * Set AdminName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $adminName
     * @return \ID3Global\Models\CreateOrganisation
     */
    public function setAdminName(?string $adminName = null): self
    {
        // validation for constraint: string
        if (!is_null($adminName) && !is_string($adminName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($adminName, true), gettype($adminName)), __LINE__);
        }
        if (is_null($adminName) || (is_array($adminName) && empty($adminName))) {
            unset($this->AdminName);
        } else {
            $this->AdminName = $adminName;
        }
        
        return $this;
    }
    /**
     * Get AdminUsername value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdminUsername(): ?string
    {
        return isset($this->AdminUsername) ? $this->AdminUsername : null;
    }
    /**
     * Set AdminUsername value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $adminUsername
     * @return \ID3Global\Models\CreateOrganisation
     */
    public function setAdminUsername(?string $adminUsername = null): self
    {
        // validation for constraint: string
        if (!is_null($adminUsername) && !is_string($adminUsername)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($adminUsername, true), gettype($adminUsername)), __LINE__);
        }
        if (is_null($adminUsername) || (is_array($adminUsername) && empty($adminUsername))) {
            unset($this->AdminUsername);
        } else {
            $this->AdminUsername = $adminUsername;
        }
        
        return $this;
    }
    /**
     * Get AdminEmail value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdminEmail(): ?string
    {
        return isset($this->AdminEmail) ? $this->AdminEmail : null;
    }
    /**
     * Set AdminEmail value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $adminEmail
     * @return \ID3Global\Models\CreateOrganisation
     */
    public function setAdminEmail(?string $adminEmail = null): self
    {
        // validation for constraint: string
        if (!is_null($adminEmail) && !is_string($adminEmail)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($adminEmail, true), gettype($adminEmail)), __LINE__);
        }
        if (is_null($adminEmail) || (is_array($adminEmail) && empty($adminEmail))) {
            unset($this->AdminEmail);
        } else {
            $this->AdminEmail = $adminEmail;
        }
        
        return $this;
    }
    /**
     * Get AdminPassword value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdminPassword(): ?string
    {
        return isset($this->AdminPassword) ? $this->AdminPassword : null;
    }
    /**
     * Set AdminPassword value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $adminPassword
     * @return \ID3Global\Models\CreateOrganisation
     */
    public function setAdminPassword(?string $adminPassword = null): self
    {
        // validation for constraint: string
        if (!is_null($adminPassword) && !is_string($adminPassword)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($adminPassword, true), gettype($adminPassword)), __LINE__);
        }
        if (is_null($adminPassword) || (is_array($adminPassword) && empty($adminPassword))) {
            unset($this->AdminPassword);
        } else {
            $this->AdminPassword = $adminPassword;
        }
        
        return $this;
    }
    /**
     * Get AdminPIN value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdminPIN(): ?string
    {
        return isset($this->AdminPIN) ? $this->AdminPIN : null;
    }
    /**
     * Set AdminPIN value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $adminPIN
     * @return \ID3Global\Models\CreateOrganisation
     */
    public function setAdminPIN(?string $adminPIN = null): self
    {
        // validation for constraint: string
        if (!is_null($adminPIN) && !is_string($adminPIN)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($adminPIN, true), gettype($adminPIN)), __LINE__);
        }
        if (is_null($adminPIN) || (is_array($adminPIN) && empty($adminPIN))) {
            unset($this->AdminPIN);
        } else {
            $this->AdminPIN = $adminPIN;
        }
        
        return $this;
    }
}
