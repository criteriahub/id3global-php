<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DownloadDataExtractResponse Models
 * @subpackage Structs
 */
class DownloadDataExtractResponse extends AbstractStructBase
{
    /**
     * The DownloadDataExtractResult
     * Meta information extracted from the WSDL
     * - base: xs:base64Binary
     * @var string|null
     */
    protected ?string $DownloadDataExtractResult = null;
    /**
     * Constructor method for DownloadDataExtractResponse
     * @uses DownloadDataExtractResponse::setDownloadDataExtractResult()
     * @param string $downloadDataExtractResult
     */
    public function __construct(?string $downloadDataExtractResult = null)
    {
        $this
            ->setDownloadDataExtractResult($downloadDataExtractResult);
    }
    /**
     * Get DownloadDataExtractResult value
     * @return string|null
     */
    public function getDownloadDataExtractResult(): ?string
    {
        return $this->DownloadDataExtractResult;
    }
    /**
     * Set DownloadDataExtractResult value
     * @param string $downloadDataExtractResult
     * @return \ID3Global\Models\DownloadDataExtractResponse
     */
    public function setDownloadDataExtractResult(?string $downloadDataExtractResult = null): self
    {
        // validation for constraint: string
        if (!is_null($downloadDataExtractResult) && !is_string($downloadDataExtractResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($downloadDataExtractResult, true), gettype($downloadDataExtractResult)), __LINE__);
        }
        $this->DownloadDataExtractResult = $downloadDataExtractResult;
        
        return $this;
    }
}
