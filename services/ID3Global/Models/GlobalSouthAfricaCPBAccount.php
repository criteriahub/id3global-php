<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSouthAfricaCPBAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q697:GlobalSouthAfricaCPBAccount
 * @subpackage Structs
 */
class GlobalSouthAfricaCPBAccount extends GlobalSupplierAccount
{
    /**
     * The AccountNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccountNumber = null;
    /**
     * The UserCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $UserCode = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * Constructor method for GlobalSouthAfricaCPBAccount
     * @uses GlobalSouthAfricaCPBAccount::setAccountNumber()
     * @uses GlobalSouthAfricaCPBAccount::setUserCode()
     * @uses GlobalSouthAfricaCPBAccount::setPassword()
     * @param string $accountNumber
     * @param string $userCode
     * @param string $password
     */
    public function __construct(?string $accountNumber = null, ?string $userCode = null, ?string $password = null)
    {
        $this
            ->setAccountNumber($accountNumber)
            ->setUserCode($userCode)
            ->setPassword($password);
    }
    /**
     * Get AccountNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccountNumber(): ?string
    {
        return isset($this->AccountNumber) ? $this->AccountNumber : null;
    }
    /**
     * Set AccountNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accountNumber
     * @return \ID3Global\Models\GlobalSouthAfricaCPBAccount
     */
    public function setAccountNumber(?string $accountNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($accountNumber) && !is_string($accountNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountNumber, true), gettype($accountNumber)), __LINE__);
        }
        if (is_null($accountNumber) || (is_array($accountNumber) && empty($accountNumber))) {
            unset($this->AccountNumber);
        } else {
            $this->AccountNumber = $accountNumber;
        }
        
        return $this;
    }
    /**
     * Get UserCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUserCode(): ?string
    {
        return isset($this->UserCode) ? $this->UserCode : null;
    }
    /**
     * Set UserCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $userCode
     * @return \ID3Global\Models\GlobalSouthAfricaCPBAccount
     */
    public function setUserCode(?string $userCode = null): self
    {
        // validation for constraint: string
        if (!is_null($userCode) && !is_string($userCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userCode, true), gettype($userCode)), __LINE__);
        }
        if (is_null($userCode) || (is_array($userCode) && empty($userCode))) {
            unset($this->UserCode);
        } else {
            $this->UserCode = $userCode;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\GlobalSouthAfricaCPBAccount
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
}
