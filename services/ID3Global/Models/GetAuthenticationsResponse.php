<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAuthenticationsResponse Models
 * @subpackage Structs
 */
class GetAuthenticationsResponse extends AbstractStructBase
{
    /**
     * The GetAuthenticationsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAuthentications|null
     */
    protected ?\ID3Global\Models\GlobalAuthentications $GetAuthenticationsResult = null;
    /**
     * Constructor method for GetAuthenticationsResponse
     * @uses GetAuthenticationsResponse::setGetAuthenticationsResult()
     * @param \ID3Global\Models\GlobalAuthentications $getAuthenticationsResult
     */
    public function __construct(?\ID3Global\Models\GlobalAuthentications $getAuthenticationsResult = null)
    {
        $this
            ->setGetAuthenticationsResult($getAuthenticationsResult);
    }
    /**
     * Get GetAuthenticationsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAuthentications|null
     */
    public function getGetAuthenticationsResult(): ?\ID3Global\Models\GlobalAuthentications
    {
        return isset($this->GetAuthenticationsResult) ? $this->GetAuthenticationsResult : null;
    }
    /**
     * Set GetAuthenticationsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAuthentications $getAuthenticationsResult
     * @return \ID3Global\Models\GetAuthenticationsResponse
     */
    public function setGetAuthenticationsResult(?\ID3Global\Models\GlobalAuthentications $getAuthenticationsResult = null): self
    {
        if (is_null($getAuthenticationsResult) || (is_array($getAuthenticationsResult) && empty($getAuthenticationsResult))) {
            unset($this->GetAuthenticationsResult);
        } else {
            $this->GetAuthenticationsResult = $getAuthenticationsResult;
        }
        
        return $this;
    }
}
