<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalBand Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q209:GlobalBand
 * @subpackage Structs
 */
class GlobalBand extends AbstractStructBase
{
    /**
     * The BandText
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $BandText = null;
    /**
     * The StartValue
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $StartValue = null;
    /**
     * The EndValue
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $EndValue = null;
    /**
     * The PassedRules
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    protected ?\ID3Global\Arrays\ArrayOfunsignedInt $PassedRules = null;
    /**
     * Constructor method for GlobalBand
     * @uses GlobalBand::setBandText()
     * @uses GlobalBand::setStartValue()
     * @uses GlobalBand::setEndValue()
     * @uses GlobalBand::setPassedRules()
     * @param string $bandText
     * @param int $startValue
     * @param int $endValue
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $passedRules
     */
    public function __construct(?string $bandText = null, ?int $startValue = null, ?int $endValue = null, ?\ID3Global\Arrays\ArrayOfunsignedInt $passedRules = null)
    {
        $this
            ->setBandText($bandText)
            ->setStartValue($startValue)
            ->setEndValue($endValue)
            ->setPassedRules($passedRules);
    }
    /**
     * Get BandText value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBandText(): ?string
    {
        return isset($this->BandText) ? $this->BandText : null;
    }
    /**
     * Set BandText value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $bandText
     * @return \ID3Global\Models\GlobalBand
     */
    public function setBandText(?string $bandText = null): self
    {
        // validation for constraint: string
        if (!is_null($bandText) && !is_string($bandText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bandText, true), gettype($bandText)), __LINE__);
        }
        if (is_null($bandText) || (is_array($bandText) && empty($bandText))) {
            unset($this->BandText);
        } else {
            $this->BandText = $bandText;
        }
        
        return $this;
    }
    /**
     * Get StartValue value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getStartValue(): ?int
    {
        return isset($this->StartValue) ? $this->StartValue : null;
    }
    /**
     * Set StartValue value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $startValue
     * @return \ID3Global\Models\GlobalBand
     */
    public function setStartValue(?int $startValue = null): self
    {
        // validation for constraint: int
        if (!is_null($startValue) && !(is_int($startValue) || ctype_digit($startValue))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($startValue, true), gettype($startValue)), __LINE__);
        }
        if (is_null($startValue) || (is_array($startValue) && empty($startValue))) {
            unset($this->StartValue);
        } else {
            $this->StartValue = $startValue;
        }
        
        return $this;
    }
    /**
     * Get EndValue value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getEndValue(): ?int
    {
        return isset($this->EndValue) ? $this->EndValue : null;
    }
    /**
     * Set EndValue value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $endValue
     * @return \ID3Global\Models\GlobalBand
     */
    public function setEndValue(?int $endValue = null): self
    {
        // validation for constraint: int
        if (!is_null($endValue) && !(is_int($endValue) || ctype_digit($endValue))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($endValue, true), gettype($endValue)), __LINE__);
        }
        if (is_null($endValue) || (is_array($endValue) && empty($endValue))) {
            unset($this->EndValue);
        } else {
            $this->EndValue = $endValue;
        }
        
        return $this;
    }
    /**
     * Get PassedRules value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    public function getPassedRules(): ?\ID3Global\Arrays\ArrayOfunsignedInt
    {
        return isset($this->PassedRules) ? $this->PassedRules : null;
    }
    /**
     * Set PassedRules value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $passedRules
     * @return \ID3Global\Models\GlobalBand
     */
    public function setPassedRules(?\ID3Global\Arrays\ArrayOfunsignedInt $passedRules = null): self
    {
        if (is_null($passedRules) || (is_array($passedRules) && empty($passedRules))) {
            unset($this->PassedRules);
        } else {
            $this->PassedRules = $passedRules;
        }
        
        return $this;
    }
}
