<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalKeyValuePairOfstringint Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q827:GlobalKeyValuePairOfstringint
 * @subpackage Structs
 */
class GlobalKeyValuePairOfstringint extends AbstractStructBase
{
    /**
     * The Key
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Key = null;
    /**
     * The Value
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Value = null;
    /**
     * Constructor method for GlobalKeyValuePairOfstringint
     * @uses GlobalKeyValuePairOfstringint::setKey()
     * @uses GlobalKeyValuePairOfstringint::setValue()
     * @param string $key
     * @param int $value
     */
    public function __construct(?string $key = null, ?int $value = null)
    {
        $this
            ->setKey($key)
            ->setValue($value);
    }
    /**
     * Get Key value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getKey(): ?string
    {
        return isset($this->Key) ? $this->Key : null;
    }
    /**
     * Set Key value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $key
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringint
     */
    public function setKey(?string $key = null): self
    {
        // validation for constraint: string
        if (!is_null($key) && !is_string($key)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($key, true), gettype($key)), __LINE__);
        }
        if (is_null($key) || (is_array($key) && empty($key))) {
            unset($this->Key);
        } else {
            $this->Key = $key;
        }
        
        return $this;
    }
    /**
     * Get Value value
     * @return int|null
     */
    public function getValue(): ?int
    {
        return $this->Value;
    }
    /**
     * Set Value value
     * @param int $value
     * @return \ID3Global\Models\GlobalKeyValuePairOfstringint
     */
    public function setValue(?int $value = null): self
    {
        // validation for constraint: int
        if (!is_null($value) && !(is_int($value) || ctype_digit($value))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->Value = $value;
        
        return $this;
    }
}
