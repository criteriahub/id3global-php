<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalWeighting Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q245:GlobalWeighting
 * @subpackage Structs
 */
class GlobalWeighting extends AbstractStructBase
{
    /**
     * The Code
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Code = null;
    /**
     * The Weighting
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Weighting = null;
    /**
     * Constructor method for GlobalWeighting
     * @uses GlobalWeighting::setCode()
     * @uses GlobalWeighting::setWeighting()
     * @param int $code
     * @param int $weighting
     */
    public function __construct(?int $code = null, ?int $weighting = null)
    {
        $this
            ->setCode($code)
            ->setWeighting($weighting);
    }
    /**
     * Get Code value
     * @return int|null
     */
    public function getCode(): ?int
    {
        return $this->Code;
    }
    /**
     * Set Code value
     * @param int $code
     * @return \ID3Global\Models\GlobalWeighting
     */
    public function setCode(?int $code = null): self
    {
        // validation for constraint: int
        if (!is_null($code) && !(is_int($code) || ctype_digit($code))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        $this->Code = $code;
        
        return $this;
    }
    /**
     * Get Weighting value
     * @return int|null
     */
    public function getWeighting(): ?int
    {
        return $this->Weighting;
    }
    /**
     * Set Weighting value
     * @param int $weighting
     * @return \ID3Global\Models\GlobalWeighting
     */
    public function setWeighting(?int $weighting = null): self
    {
        // validation for constraint: int
        if (!is_null($weighting) && !(is_int($weighting) || ctype_digit($weighting))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($weighting, true), gettype($weighting)), __LINE__);
        }
        $this->Weighting = $weighting;
        
        return $this;
    }
}
