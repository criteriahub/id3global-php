<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDVLADrivingLicenceReportCategory Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q785:GlobalDVLADrivingLicenceReportCategory
 * @subpackage Structs
 */
class GlobalDVLADrivingLicenceReportCategory extends AbstractStructBase
{
    /**
     * The Code
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Code = null;
    /**
     * The Status
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Status = null;
    /**
     * The StartDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $StartDate = null;
    /**
     * The ExpiryDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ExpiryDate = null;
    /**
     * The Restrictions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfdouble|null
     */
    protected ?\ID3Global\Arrays\ArrayOfdouble $Restrictions = null;
    /**
     * The Identifier
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Identifier = null;
    /**
     * Constructor method for GlobalDVLADrivingLicenceReportCategory
     * @uses GlobalDVLADrivingLicenceReportCategory::setCode()
     * @uses GlobalDVLADrivingLicenceReportCategory::setStatus()
     * @uses GlobalDVLADrivingLicenceReportCategory::setStartDate()
     * @uses GlobalDVLADrivingLicenceReportCategory::setExpiryDate()
     * @uses GlobalDVLADrivingLicenceReportCategory::setRestrictions()
     * @uses GlobalDVLADrivingLicenceReportCategory::setIdentifier()
     * @param string $code
     * @param string $status
     * @param string $startDate
     * @param string $expiryDate
     * @param \ID3Global\Arrays\ArrayOfdouble $restrictions
     * @param string $identifier
     */
    public function __construct(?string $code = null, ?string $status = null, ?string $startDate = null, ?string $expiryDate = null, ?\ID3Global\Arrays\ArrayOfdouble $restrictions = null, ?string $identifier = null)
    {
        $this
            ->setCode($code)
            ->setStatus($status)
            ->setStartDate($startDate)
            ->setExpiryDate($expiryDate)
            ->setRestrictions($restrictions)
            ->setIdentifier($identifier);
    }
    /**
     * Get Code value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCode(): ?string
    {
        return isset($this->Code) ? $this->Code : null;
    }
    /**
     * Set Code value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $code
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory
     */
    public function setCode(?string $code = null): self
    {
        // validation for constraint: string
        if (!is_null($code) && !is_string($code)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        if (is_null($code) || (is_array($code) && empty($code))) {
            unset($this->Code);
        } else {
            $this->Code = $code;
        }
        
        return $this;
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @uses \ID3Global\Enums\GlobalDVLADrivingLicenceReportCategoryType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalDVLADrivingLicenceReportCategoryType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $status
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalDVLADrivingLicenceReportCategoryType::valueIsValid($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalDVLADrivingLicenceReportCategoryType', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \ID3Global\Enums\GlobalDVLADrivingLicenceReportCategoryType::getValidValues())), __LINE__);
        }
        $this->Status = $status;
        
        return $this;
    }
    /**
     * Get StartDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStartDate(): ?string
    {
        return isset($this->StartDate) ? $this->StartDate : null;
    }
    /**
     * Set StartDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $startDate
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory
     */
    public function setStartDate(?string $startDate = null): self
    {
        // validation for constraint: string
        if (!is_null($startDate) && !is_string($startDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startDate, true), gettype($startDate)), __LINE__);
        }
        if (is_null($startDate) || (is_array($startDate) && empty($startDate))) {
            unset($this->StartDate);
        } else {
            $this->StartDate = $startDate;
        }
        
        return $this;
    }
    /**
     * Get ExpiryDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getExpiryDate(): ?string
    {
        return isset($this->ExpiryDate) ? $this->ExpiryDate : null;
    }
    /**
     * Set ExpiryDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $expiryDate
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory
     */
    public function setExpiryDate(?string $expiryDate = null): self
    {
        // validation for constraint: string
        if (!is_null($expiryDate) && !is_string($expiryDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expiryDate, true), gettype($expiryDate)), __LINE__);
        }
        if (is_null($expiryDate) || (is_array($expiryDate) && empty($expiryDate))) {
            unset($this->ExpiryDate);
        } else {
            $this->ExpiryDate = $expiryDate;
        }
        
        return $this;
    }
    /**
     * Get Restrictions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfdouble|null
     */
    public function getRestrictions(): ?\ID3Global\Arrays\ArrayOfdouble
    {
        return isset($this->Restrictions) ? $this->Restrictions : null;
    }
    /**
     * Set Restrictions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfdouble $restrictions
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory
     */
    public function setRestrictions(?\ID3Global\Arrays\ArrayOfdouble $restrictions = null): self
    {
        if (is_null($restrictions) || (is_array($restrictions) && empty($restrictions))) {
            unset($this->Restrictions);
        } else {
            $this->Restrictions = $restrictions;
        }
        
        return $this;
    }
    /**
     * Get Identifier value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return isset($this->Identifier) ? $this->Identifier : null;
    }
    /**
     * Set Identifier value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $identifier
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportCategory
     */
    public function setIdentifier(?string $identifier = null): self
    {
        // validation for constraint: string
        if (!is_null($identifier) && !is_string($identifier)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($identifier, true), gettype($identifier)), __LINE__);
        }
        if (is_null($identifier) || (is_array($identifier) && empty($identifier))) {
            unset($this->Identifier);
        } else {
            $this->Identifier = $identifier;
        }
        
        return $this;
    }
}
