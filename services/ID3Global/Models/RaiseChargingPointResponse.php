<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RaiseChargingPointResponse Models
 * @subpackage Structs
 */
class RaiseChargingPointResponse extends AbstractStructBase
{
    /**
     * The RaiseChargingPointResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseDetails|null
     */
    protected ?\ID3Global\Models\GlobalCaseDetails $RaiseChargingPointResult = null;
    /**
     * Constructor method for RaiseChargingPointResponse
     * @uses RaiseChargingPointResponse::setRaiseChargingPointResult()
     * @param \ID3Global\Models\GlobalCaseDetails $raiseChargingPointResult
     */
    public function __construct(?\ID3Global\Models\GlobalCaseDetails $raiseChargingPointResult = null)
    {
        $this
            ->setRaiseChargingPointResult($raiseChargingPointResult);
    }
    /**
     * Get RaiseChargingPointResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseDetails|null
     */
    public function getRaiseChargingPointResult(): ?\ID3Global\Models\GlobalCaseDetails
    {
        return isset($this->RaiseChargingPointResult) ? $this->RaiseChargingPointResult : null;
    }
    /**
     * Set RaiseChargingPointResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCaseDetails $raiseChargingPointResult
     * @return \ID3Global\Models\RaiseChargingPointResponse
     */
    public function setRaiseChargingPointResult(?\ID3Global\Models\GlobalCaseDetails $raiseChargingPointResult = null): self
    {
        if (is_null($raiseChargingPointResult) || (is_array($raiseChargingPointResult) && empty($raiseChargingPointResult))) {
            unset($this->RaiseChargingPointResult);
        } else {
            $this->RaiseChargingPointResult = $raiseChargingPointResult;
        }
        
        return $this;
    }
}
