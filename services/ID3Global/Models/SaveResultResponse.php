<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SaveResultResponse Models
 * @subpackage Structs
 */
class SaveResultResponse extends AbstractStructBase
{
    /**
     * The SaveResultResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseDetails|null
     */
    protected ?\ID3Global\Models\GlobalCaseDetails $SaveResultResult = null;
    /**
     * Constructor method for SaveResultResponse
     * @uses SaveResultResponse::setSaveResultResult()
     * @param \ID3Global\Models\GlobalCaseDetails $saveResultResult
     */
    public function __construct(?\ID3Global\Models\GlobalCaseDetails $saveResultResult = null)
    {
        $this
            ->setSaveResultResult($saveResultResult);
    }
    /**
     * Get SaveResultResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseDetails|null
     */
    public function getSaveResultResult(): ?\ID3Global\Models\GlobalCaseDetails
    {
        return isset($this->SaveResultResult) ? $this->SaveResultResult : null;
    }
    /**
     * Set SaveResultResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCaseDetails $saveResultResult
     * @return \ID3Global\Models\SaveResultResponse
     */
    public function setSaveResultResult(?\ID3Global\Models\GlobalCaseDetails $saveResultResult = null): self
    {
        if (is_null($saveResultResult) || (is_array($saveResultResult) && empty($saveResultResult))) {
            unset($this->SaveResultResult);
        } else {
            $this->SaveResultResult = $saveResultResult;
        }
        
        return $this;
    }
}
