<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalConsent Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q432:GlobalConsent
 * @subpackage Structs
 */
class GlobalConsent extends AbstractStructBase
{
    /**
     * The UKDriverConsent
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool|null
     */
    protected ?bool $UKDriverConsent = null;
    /**
     * Constructor method for GlobalConsent
     * @uses GlobalConsent::setUKDriverConsent()
     * @param bool $uKDriverConsent
     */
    public function __construct(?bool $uKDriverConsent = null)
    {
        $this
            ->setUKDriverConsent($uKDriverConsent);
    }
    /**
     * Get UKDriverConsent value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getUKDriverConsent(): ?bool
    {
        return isset($this->UKDriverConsent) ? $this->UKDriverConsent : null;
    }
    /**
     * Set UKDriverConsent value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $uKDriverConsent
     * @return \ID3Global\Models\GlobalConsent
     */
    public function setUKDriverConsent(?bool $uKDriverConsent = null): self
    {
        // validation for constraint: boolean
        if (!is_null($uKDriverConsent) && !is_bool($uKDriverConsent)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($uKDriverConsent, true), gettype($uKDriverConsent)), __LINE__);
        }
        if (is_null($uKDriverConsent) || (is_array($uKDriverConsent) && empty($uKDriverConsent))) {
            unset($this->UKDriverConsent);
        } else {
            $this->UKDriverConsent = $uKDriverConsent;
        }
        
        return $this;
    }
}
