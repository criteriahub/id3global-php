<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AddressLookupResponse Models
 * @subpackage Structs
 */
class AddressLookupResponse extends AbstractStructBase
{
    /**
     * The AddressLookupResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalAddress|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalAddress $AddressLookupResult = null;
    /**
     * Constructor method for AddressLookupResponse
     * @uses AddressLookupResponse::setAddressLookupResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalAddress $addressLookupResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalAddress $addressLookupResult = null)
    {
        $this
            ->setAddressLookupResult($addressLookupResult);
    }
    /**
     * Get AddressLookupResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalAddress|null
     */
    public function getAddressLookupResult(): ?\ID3Global\Arrays\ArrayOfGlobalAddress
    {
        return isset($this->AddressLookupResult) ? $this->AddressLookupResult : null;
    }
    /**
     * Set AddressLookupResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalAddress $addressLookupResult
     * @return \ID3Global\Models\AddressLookupResponse
     */
    public function setAddressLookupResult(?\ID3Global\Arrays\ArrayOfGlobalAddress $addressLookupResult = null): self
    {
        if (is_null($addressLookupResult) || (is_array($addressLookupResult) && empty($addressLookupResult))) {
            unset($this->AddressLookupResult);
        } else {
            $this->AddressLookupResult = $addressLookupResult;
        }
        
        return $this;
    }
}
