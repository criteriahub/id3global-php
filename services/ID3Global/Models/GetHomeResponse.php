<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetHomeResponse Models
 * @subpackage Structs
 */
class GetHomeResponse extends AbstractStructBase
{
    /**
     * The GetHomeResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalHome|null
     */
    protected ?\ID3Global\Models\GlobalHome $GetHomeResult = null;
    /**
     * Constructor method for GetHomeResponse
     * @uses GetHomeResponse::setGetHomeResult()
     * @param \ID3Global\Models\GlobalHome $getHomeResult
     */
    public function __construct(?\ID3Global\Models\GlobalHome $getHomeResult = null)
    {
        $this
            ->setGetHomeResult($getHomeResult);
    }
    /**
     * Get GetHomeResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalHome|null
     */
    public function getGetHomeResult(): ?\ID3Global\Models\GlobalHome
    {
        return isset($this->GetHomeResult) ? $this->GetHomeResult : null;
    }
    /**
     * Set GetHomeResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalHome $getHomeResult
     * @return \ID3Global\Models\GetHomeResponse
     */
    public function setGetHomeResult(?\ID3Global\Models\GlobalHome $getHomeResult = null): self
    {
        if (is_null($getHomeResult) || (is_array($getHomeResult) && empty($getHomeResult))) {
            unset($this->GetHomeResult);
        } else {
            $this->GetHomeResult = $getHomeResult;
        }
        
        return $this;
    }
}
