<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSupplierConfigResponse Models
 * @subpackage Structs
 */
class GetSupplierConfigResponse extends AbstractStructBase
{
    /**
     * The GetSupplierConfigResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSupplierConfig|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSupplierConfig $GetSupplierConfigResult = null;
    /**
     * Constructor method for GetSupplierConfigResponse
     * @uses GetSupplierConfigResponse::setGetSupplierConfigResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplierConfig $getSupplierConfigResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalSupplierConfig $getSupplierConfigResult = null)
    {
        $this
            ->setGetSupplierConfigResult($getSupplierConfigResult);
    }
    /**
     * Get GetSupplierConfigResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierConfig|null
     */
    public function getGetSupplierConfigResult(): ?\ID3Global\Arrays\ArrayOfGlobalSupplierConfig
    {
        return isset($this->GetSupplierConfigResult) ? $this->GetSupplierConfigResult : null;
    }
    /**
     * Set GetSupplierConfigResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplierConfig $getSupplierConfigResult
     * @return \ID3Global\Models\GetSupplierConfigResponse
     */
    public function setGetSupplierConfigResult(?\ID3Global\Arrays\ArrayOfGlobalSupplierConfig $getSupplierConfigResult = null): self
    {
        if (is_null($getSupplierConfigResult) || (is_array($getSupplierConfigResult) && empty($getSupplierConfigResult))) {
            unset($this->GetSupplierConfigResult);
        } else {
            $this->GetSupplierConfigResult = $getSupplierConfigResult;
        }
        
        return $this;
    }
}
