<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalNewZealand Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q360:GlobalNewZealand
 * @subpackage Structs
 */
class GlobalNewZealand extends AbstractStructBase
{
    /**
     * The DrivingLicence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalNewZealandDrivingLicence|null
     */
    protected ?\ID3Global\Models\GlobalNewZealandDrivingLicence $DrivingLicence = null;
    /**
     * Constructor method for GlobalNewZealand
     * @uses GlobalNewZealand::setDrivingLicence()
     * @param \ID3Global\Models\GlobalNewZealandDrivingLicence $drivingLicence
     */
    public function __construct(?\ID3Global\Models\GlobalNewZealandDrivingLicence $drivingLicence = null)
    {
        $this
            ->setDrivingLicence($drivingLicence);
    }
    /**
     * Get DrivingLicence value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalNewZealandDrivingLicence|null
     */
    public function getDrivingLicence(): ?\ID3Global\Models\GlobalNewZealandDrivingLicence
    {
        return isset($this->DrivingLicence) ? $this->DrivingLicence : null;
    }
    /**
     * Set DrivingLicence value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalNewZealandDrivingLicence $drivingLicence
     * @return \ID3Global\Models\GlobalNewZealand
     */
    public function setDrivingLicence(?\ID3Global\Models\GlobalNewZealandDrivingLicence $drivingLicence = null): self
    {
        if (is_null($drivingLicence) || (is_array($drivingLicence) && empty($drivingLicence))) {
            unset($this->DrivingLicence);
        } else {
            $this->DrivingLicence = $drivingLicence;
        }
        
        return $this;
    }
}
