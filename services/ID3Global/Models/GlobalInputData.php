<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalInputData Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q308:GlobalInputData
 * @subpackage Structs
 */
class GlobalInputData extends AbstractStructBase
{
    /**
     * The Images
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalImage|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalImage $Images = null;
    /**
     * The Personal
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalPersonal|null
     */
    protected ?\ID3Global\Models\GlobalPersonal $Personal = null;
    /**
     * The Addresses
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAddresses|null
     */
    protected ?\ID3Global\Models\GlobalAddresses $Addresses = null;
    /**
     * The IdentityDocuments
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalIdentityDocuments|null
     */
    protected ?\ID3Global\Models\GlobalIdentityDocuments $IdentityDocuments = null;
    /**
     * The AddressDocuments
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAddressDocuments|null
     */
    protected ?\ID3Global\Models\GlobalAddressDocuments $AddressDocuments = null;
    /**
     * The ContactDetails
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalContactDetails|null
     */
    protected ?\ID3Global\Models\GlobalContactDetails $ContactDetails = null;
    /**
     * The Employment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalEmployment|null
     */
    protected ?\ID3Global\Models\GlobalEmployment $Employment = null;
    /**
     * The BankingDetails
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalBankingDetails|null
     */
    protected ?\ID3Global\Models\GlobalBankingDetails $BankingDetails = null;
    /**
     * The GlobalGeneric
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalGeneric|null
     */
    protected ?\ID3Global\Models\GlobalGeneric $GlobalGeneric = null;
    /**
     * The Location
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalLocation|null
     */
    protected ?\ID3Global\Models\GlobalLocation $Location = null;
    /**
     * The Consent
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalConsent|null
     */
    protected ?\ID3Global\Models\GlobalConsent $Consent = null;
    /**
     * Constructor method for GlobalInputData
     * @uses GlobalInputData::setImages()
     * @uses GlobalInputData::setPersonal()
     * @uses GlobalInputData::setAddresses()
     * @uses GlobalInputData::setIdentityDocuments()
     * @uses GlobalInputData::setAddressDocuments()
     * @uses GlobalInputData::setContactDetails()
     * @uses GlobalInputData::setEmployment()
     * @uses GlobalInputData::setBankingDetails()
     * @uses GlobalInputData::setGlobalGeneric()
     * @uses GlobalInputData::setLocation()
     * @uses GlobalInputData::setConsent()
     * @param \ID3Global\Arrays\ArrayOfGlobalImage $images
     * @param \ID3Global\Models\GlobalPersonal $personal
     * @param \ID3Global\Models\GlobalAddresses $addresses
     * @param \ID3Global\Models\GlobalIdentityDocuments $identityDocuments
     * @param \ID3Global\Models\GlobalAddressDocuments $addressDocuments
     * @param \ID3Global\Models\GlobalContactDetails $contactDetails
     * @param \ID3Global\Models\GlobalEmployment $employment
     * @param \ID3Global\Models\GlobalBankingDetails $bankingDetails
     * @param \ID3Global\Models\GlobalGeneric $globalGeneric
     * @param \ID3Global\Models\GlobalLocation $location
     * @param \ID3Global\Models\GlobalConsent $consent
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalImage $images = null, ?\ID3Global\Models\GlobalPersonal $personal = null, ?\ID3Global\Models\GlobalAddresses $addresses = null, ?\ID3Global\Models\GlobalIdentityDocuments $identityDocuments = null, ?\ID3Global\Models\GlobalAddressDocuments $addressDocuments = null, ?\ID3Global\Models\GlobalContactDetails $contactDetails = null, ?\ID3Global\Models\GlobalEmployment $employment = null, ?\ID3Global\Models\GlobalBankingDetails $bankingDetails = null, ?\ID3Global\Models\GlobalGeneric $globalGeneric = null, ?\ID3Global\Models\GlobalLocation $location = null, ?\ID3Global\Models\GlobalConsent $consent = null)
    {
        $this
            ->setImages($images)
            ->setPersonal($personal)
            ->setAddresses($addresses)
            ->setIdentityDocuments($identityDocuments)
            ->setAddressDocuments($addressDocuments)
            ->setContactDetails($contactDetails)
            ->setEmployment($employment)
            ->setBankingDetails($bankingDetails)
            ->setGlobalGeneric($globalGeneric)
            ->setLocation($location)
            ->setConsent($consent);
    }
    /**
     * Get Images value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalImage|null
     */
    public function getImages(): ?\ID3Global\Arrays\ArrayOfGlobalImage
    {
        return isset($this->Images) ? $this->Images : null;
    }
    /**
     * Set Images value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalImage $images
     * @return \ID3Global\Models\GlobalInputData
     */
    public function setImages(?\ID3Global\Arrays\ArrayOfGlobalImage $images = null): self
    {
        if (is_null($images) || (is_array($images) && empty($images))) {
            unset($this->Images);
        } else {
            $this->Images = $images;
        }
        
        return $this;
    }
    /**
     * Get Personal value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalPersonal|null
     */
    public function getPersonal(): ?\ID3Global\Models\GlobalPersonal
    {
        return isset($this->Personal) ? $this->Personal : null;
    }
    /**
     * Set Personal value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalPersonal $personal
     * @return \ID3Global\Models\GlobalInputData
     */
    public function setPersonal(?\ID3Global\Models\GlobalPersonal $personal = null): self
    {
        if (is_null($personal) || (is_array($personal) && empty($personal))) {
            unset($this->Personal);
        } else {
            $this->Personal = $personal;
        }
        
        return $this;
    }
    /**
     * Get Addresses value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAddresses|null
     */
    public function getAddresses(): ?\ID3Global\Models\GlobalAddresses
    {
        return isset($this->Addresses) ? $this->Addresses : null;
    }
    /**
     * Set Addresses value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAddresses $addresses
     * @return \ID3Global\Models\GlobalInputData
     */
    public function setAddresses(?\ID3Global\Models\GlobalAddresses $addresses = null): self
    {
        if (is_null($addresses) || (is_array($addresses) && empty($addresses))) {
            unset($this->Addresses);
        } else {
            $this->Addresses = $addresses;
        }
        
        return $this;
    }
    /**
     * Get IdentityDocuments value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalIdentityDocuments|null
     */
    public function getIdentityDocuments(): ?\ID3Global\Models\GlobalIdentityDocuments
    {
        return isset($this->IdentityDocuments) ? $this->IdentityDocuments : null;
    }
    /**
     * Set IdentityDocuments value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalIdentityDocuments $identityDocuments
     * @return \ID3Global\Models\GlobalInputData
     */
    public function setIdentityDocuments(?\ID3Global\Models\GlobalIdentityDocuments $identityDocuments = null): self
    {
        if (is_null($identityDocuments) || (is_array($identityDocuments) && empty($identityDocuments))) {
            unset($this->IdentityDocuments);
        } else {
            $this->IdentityDocuments = $identityDocuments;
        }
        
        return $this;
    }
    /**
     * Get AddressDocuments value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAddressDocuments|null
     */
    public function getAddressDocuments(): ?\ID3Global\Models\GlobalAddressDocuments
    {
        return isset($this->AddressDocuments) ? $this->AddressDocuments : null;
    }
    /**
     * Set AddressDocuments value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAddressDocuments $addressDocuments
     * @return \ID3Global\Models\GlobalInputData
     */
    public function setAddressDocuments(?\ID3Global\Models\GlobalAddressDocuments $addressDocuments = null): self
    {
        if (is_null($addressDocuments) || (is_array($addressDocuments) && empty($addressDocuments))) {
            unset($this->AddressDocuments);
        } else {
            $this->AddressDocuments = $addressDocuments;
        }
        
        return $this;
    }
    /**
     * Get ContactDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalContactDetails|null
     */
    public function getContactDetails(): ?\ID3Global\Models\GlobalContactDetails
    {
        return isset($this->ContactDetails) ? $this->ContactDetails : null;
    }
    /**
     * Set ContactDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalContactDetails $contactDetails
     * @return \ID3Global\Models\GlobalInputData
     */
    public function setContactDetails(?\ID3Global\Models\GlobalContactDetails $contactDetails = null): self
    {
        if (is_null($contactDetails) || (is_array($contactDetails) && empty($contactDetails))) {
            unset($this->ContactDetails);
        } else {
            $this->ContactDetails = $contactDetails;
        }
        
        return $this;
    }
    /**
     * Get Employment value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalEmployment|null
     */
    public function getEmployment(): ?\ID3Global\Models\GlobalEmployment
    {
        return isset($this->Employment) ? $this->Employment : null;
    }
    /**
     * Set Employment value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalEmployment $employment
     * @return \ID3Global\Models\GlobalInputData
     */
    public function setEmployment(?\ID3Global\Models\GlobalEmployment $employment = null): self
    {
        if (is_null($employment) || (is_array($employment) && empty($employment))) {
            unset($this->Employment);
        } else {
            $this->Employment = $employment;
        }
        
        return $this;
    }
    /**
     * Get BankingDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalBankingDetails|null
     */
    public function getBankingDetails(): ?\ID3Global\Models\GlobalBankingDetails
    {
        return isset($this->BankingDetails) ? $this->BankingDetails : null;
    }
    /**
     * Set BankingDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalBankingDetails $bankingDetails
     * @return \ID3Global\Models\GlobalInputData
     */
    public function setBankingDetails(?\ID3Global\Models\GlobalBankingDetails $bankingDetails = null): self
    {
        if (is_null($bankingDetails) || (is_array($bankingDetails) && empty($bankingDetails))) {
            unset($this->BankingDetails);
        } else {
            $this->BankingDetails = $bankingDetails;
        }
        
        return $this;
    }
    /**
     * Get GlobalGeneric value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalGeneric|null
     */
    public function getGlobalGeneric(): ?\ID3Global\Models\GlobalGeneric
    {
        return isset($this->GlobalGeneric) ? $this->GlobalGeneric : null;
    }
    /**
     * Set GlobalGeneric value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalGeneric $globalGeneric
     * @return \ID3Global\Models\GlobalInputData
     */
    public function setGlobalGeneric(?\ID3Global\Models\GlobalGeneric $globalGeneric = null): self
    {
        if (is_null($globalGeneric) || (is_array($globalGeneric) && empty($globalGeneric))) {
            unset($this->GlobalGeneric);
        } else {
            $this->GlobalGeneric = $globalGeneric;
        }
        
        return $this;
    }
    /**
     * Get Location value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalLocation|null
     */
    public function getLocation(): ?\ID3Global\Models\GlobalLocation
    {
        return isset($this->Location) ? $this->Location : null;
    }
    /**
     * Set Location value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalLocation $location
     * @return \ID3Global\Models\GlobalInputData
     */
    public function setLocation(?\ID3Global\Models\GlobalLocation $location = null): self
    {
        if (is_null($location) || (is_array($location) && empty($location))) {
            unset($this->Location);
        } else {
            $this->Location = $location;
        }
        
        return $this;
    }
    /**
     * Get Consent value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalConsent|null
     */
    public function getConsent(): ?\ID3Global\Models\GlobalConsent
    {
        return isset($this->Consent) ? $this->Consent : null;
    }
    /**
     * Set Consent value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalConsent $consent
     * @return \ID3Global\Models\GlobalInputData
     */
    public function setConsent(?\ID3Global\Models\GlobalConsent $consent = null): self
    {
        if (is_null($consent) || (is_array($consent) && empty($consent))) {
            unset($this->Consent);
        } else {
            $this->Consent = $consent;
        }
        
        return $this;
    }
}
