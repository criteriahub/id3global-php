<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSearchPurposesResponse Models
 * @subpackage Structs
 */
class GetSearchPurposesResponse extends AbstractStructBase
{
    /**
     * The GetSearchPurposesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSearchPurpose|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSearchPurpose $GetSearchPurposesResult = null;
    /**
     * Constructor method for GetSearchPurposesResponse
     * @uses GetSearchPurposesResponse::setGetSearchPurposesResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalSearchPurpose $getSearchPurposesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalSearchPurpose $getSearchPurposesResult = null)
    {
        $this
            ->setGetSearchPurposesResult($getSearchPurposesResult);
    }
    /**
     * Get GetSearchPurposesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSearchPurpose|null
     */
    public function getGetSearchPurposesResult(): ?\ID3Global\Arrays\ArrayOfGlobalSearchPurpose
    {
        return isset($this->GetSearchPurposesResult) ? $this->GetSearchPurposesResult : null;
    }
    /**
     * Set GetSearchPurposesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSearchPurpose $getSearchPurposesResult
     * @return \ID3Global\Models\GetSearchPurposesResponse
     */
    public function setGetSearchPurposesResult(?\ID3Global\Arrays\ArrayOfGlobalSearchPurpose $getSearchPurposesResult = null): self
    {
        if (is_null($getSearchPurposesResult) || (is_array($getSearchPurposesResult) && empty($getSearchPurposesResult))) {
            unset($this->GetSearchPurposesResult);
        } else {
            $this->GetSearchPurposesResult = $getSearchPurposesResult;
        }
        
        return $this;
    }
}
