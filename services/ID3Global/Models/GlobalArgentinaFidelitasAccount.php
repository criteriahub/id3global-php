<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalArgentinaFidelitasAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q703:GlobalArgentinaFidelitasAccount
 * @subpackage Structs
 */
class GlobalArgentinaFidelitasAccount extends GlobalSupplierAccount
{
    /**
     * The ApiKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ApiKey = null;
    /**
     * Constructor method for GlobalArgentinaFidelitasAccount
     * @uses GlobalArgentinaFidelitasAccount::setApiKey()
     * @param string $apiKey
     */
    public function __construct(?string $apiKey = null)
    {
        $this
            ->setApiKey($apiKey);
    }
    /**
     * Get ApiKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getApiKey(): ?string
    {
        return isset($this->ApiKey) ? $this->ApiKey : null;
    }
    /**
     * Set ApiKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $apiKey
     * @return \ID3Global\Models\GlobalArgentinaFidelitasAccount
     */
    public function setApiKey(?string $apiKey = null): self
    {
        // validation for constraint: string
        if (!is_null($apiKey) && !is_string($apiKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($apiKey, true), gettype($apiKey)), __LINE__);
        }
        if (is_null($apiKey) || (is_array($apiKey) && empty($apiKey))) {
            unset($this->ApiKey);
        } else {
            $this->ApiKey = $apiKey;
        }
        
        return $this;
    }
}
