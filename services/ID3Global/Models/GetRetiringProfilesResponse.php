<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetRetiringProfilesResponse Models
 * @subpackage Structs
 */
class GetRetiringProfilesResponse extends AbstractStructBase
{
    /**
     * The GetRetiringProfilesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalProfileDetails|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalProfileDetails $GetRetiringProfilesResult = null;
    /**
     * Constructor method for GetRetiringProfilesResponse
     * @uses GetRetiringProfilesResponse::setGetRetiringProfilesResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileDetails $getRetiringProfilesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalProfileDetails $getRetiringProfilesResult = null)
    {
        $this
            ->setGetRetiringProfilesResult($getRetiringProfilesResult);
    }
    /**
     * Get GetRetiringProfilesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileDetails|null
     */
    public function getGetRetiringProfilesResult(): ?\ID3Global\Arrays\ArrayOfGlobalProfileDetails
    {
        return isset($this->GetRetiringProfilesResult) ? $this->GetRetiringProfilesResult : null;
    }
    /**
     * Set GetRetiringProfilesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileDetails $getRetiringProfilesResult
     * @return \ID3Global\Models\GetRetiringProfilesResponse
     */
    public function setGetRetiringProfilesResult(?\ID3Global\Arrays\ArrayOfGlobalProfileDetails $getRetiringProfilesResult = null): self
    {
        if (is_null($getRetiringProfilesResult) || (is_array($getRetiringProfilesResult) && empty($getRetiringProfilesResult))) {
            unset($this->GetRetiringProfilesResult);
        } else {
            $this->GetRetiringProfilesResult = $getRetiringProfilesResult;
        }
        
        return $this;
    }
}
