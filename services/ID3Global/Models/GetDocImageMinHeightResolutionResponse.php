<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDocImageMinHeightResolutionResponse Models
 * @subpackage Structs
 */
class GetDocImageMinHeightResolutionResponse extends AbstractStructBase
{
    /**
     * The GetDocImageMinHeightResolutionResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $GetDocImageMinHeightResolutionResult = null;
    /**
     * Constructor method for GetDocImageMinHeightResolutionResponse
     * @uses GetDocImageMinHeightResolutionResponse::setGetDocImageMinHeightResolutionResult()
     * @param int $getDocImageMinHeightResolutionResult
     */
    public function __construct(?int $getDocImageMinHeightResolutionResult = null)
    {
        $this
            ->setGetDocImageMinHeightResolutionResult($getDocImageMinHeightResolutionResult);
    }
    /**
     * Get GetDocImageMinHeightResolutionResult value
     * @return int|null
     */
    public function getGetDocImageMinHeightResolutionResult(): ?int
    {
        return $this->GetDocImageMinHeightResolutionResult;
    }
    /**
     * Set GetDocImageMinHeightResolutionResult value
     * @param int $getDocImageMinHeightResolutionResult
     * @return \ID3Global\Models\GetDocImageMinHeightResolutionResponse
     */
    public function setGetDocImageMinHeightResolutionResult(?int $getDocImageMinHeightResolutionResult = null): self
    {
        // validation for constraint: int
        if (!is_null($getDocImageMinHeightResolutionResult) && !(is_int($getDocImageMinHeightResolutionResult) || ctype_digit($getDocImageMinHeightResolutionResult))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($getDocImageMinHeightResolutionResult, true), gettype($getDocImageMinHeightResolutionResult)), __LINE__);
        }
        $this->GetDocImageMinHeightResolutionResult = $getDocImageMinHeightResolutionResult;
        
        return $this;
    }
}
