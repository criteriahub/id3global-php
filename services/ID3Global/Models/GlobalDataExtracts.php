<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDataExtracts Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q503:GlobalDataExtracts
 * @subpackage Structs
 */
class GlobalDataExtracts extends AbstractStructBase
{
    /**
     * The Extracts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalDataExtract|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalDataExtract $Extracts = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The TotalPages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPages = null;
    /**
     * The TotalExtracts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalExtracts = null;
    /**
     * Constructor method for GlobalDataExtracts
     * @uses GlobalDataExtracts::setExtracts()
     * @uses GlobalDataExtracts::setPageSize()
     * @uses GlobalDataExtracts::setTotalPages()
     * @uses GlobalDataExtracts::setTotalExtracts()
     * @param \ID3Global\Arrays\ArrayOfGlobalDataExtract $extracts
     * @param int $pageSize
     * @param int $totalPages
     * @param int $totalExtracts
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalDataExtract $extracts = null, ?int $pageSize = null, ?int $totalPages = null, ?int $totalExtracts = null)
    {
        $this
            ->setExtracts($extracts)
            ->setPageSize($pageSize)
            ->setTotalPages($totalPages)
            ->setTotalExtracts($totalExtracts);
    }
    /**
     * Get Extracts value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalDataExtract|null
     */
    public function getExtracts(): ?\ID3Global\Arrays\ArrayOfGlobalDataExtract
    {
        return isset($this->Extracts) ? $this->Extracts : null;
    }
    /**
     * Set Extracts value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalDataExtract $extracts
     * @return \ID3Global\Models\GlobalDataExtracts
     */
    public function setExtracts(?\ID3Global\Arrays\ArrayOfGlobalDataExtract $extracts = null): self
    {
        if (is_null($extracts) || (is_array($extracts) && empty($extracts))) {
            unset($this->Extracts);
        } else {
            $this->Extracts = $extracts;
        }
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GlobalDataExtracts
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get TotalPages value
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->TotalPages;
    }
    /**
     * Set TotalPages value
     * @param int $totalPages
     * @return \ID3Global\Models\GlobalDataExtracts
     */
    public function setTotalPages(?int $totalPages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPages) && !(is_int($totalPages) || ctype_digit($totalPages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPages, true), gettype($totalPages)), __LINE__);
        }
        $this->TotalPages = $totalPages;
        
        return $this;
    }
    /**
     * Get TotalExtracts value
     * @return int|null
     */
    public function getTotalExtracts(): ?int
    {
        return $this->TotalExtracts;
    }
    /**
     * Set TotalExtracts value
     * @param int $totalExtracts
     * @return \ID3Global\Models\GlobalDataExtracts
     */
    public function setTotalExtracts(?int $totalExtracts = null): self
    {
        // validation for constraint: int
        if (!is_null($totalExtracts) && !(is_int($totalExtracts) || ctype_digit($totalExtracts))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalExtracts, true), gettype($totalExtracts)), __LINE__);
        }
        $this->TotalExtracts = $totalExtracts;
        
        return $this;
    }
}
