<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AddRemoveOGMRecordResponse Models
 * @subpackage Structs
 */
class AddRemoveOGMRecordResponse extends AbstractStructBase
{
    /**
     * The AddRemoveOGMRecordResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $AddRemoveOGMRecordResult = null;
    /**
     * The sError
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $sError = null;
    /**
     * The SourceName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SourceName = null;
    /**
     * Constructor method for AddRemoveOGMRecordResponse
     * @uses AddRemoveOGMRecordResponse::setAddRemoveOGMRecordResult()
     * @uses AddRemoveOGMRecordResponse::setSError()
     * @uses AddRemoveOGMRecordResponse::setSourceName()
     * @param bool $addRemoveOGMRecordResult
     * @param string $sError
     * @param string $sourceName
     */
    public function __construct(?bool $addRemoveOGMRecordResult = null, ?string $sError = null, ?string $sourceName = null)
    {
        $this
            ->setAddRemoveOGMRecordResult($addRemoveOGMRecordResult)
            ->setSError($sError)
            ->setSourceName($sourceName);
    }
    /**
     * Get AddRemoveOGMRecordResult value
     * @return bool|null
     */
    public function getAddRemoveOGMRecordResult(): ?bool
    {
        return $this->AddRemoveOGMRecordResult;
    }
    /**
     * Set AddRemoveOGMRecordResult value
     * @param bool $addRemoveOGMRecordResult
     * @return \ID3Global\Models\AddRemoveOGMRecordResponse
     */
    public function setAddRemoveOGMRecordResult(?bool $addRemoveOGMRecordResult = null): self
    {
        // validation for constraint: boolean
        if (!is_null($addRemoveOGMRecordResult) && !is_bool($addRemoveOGMRecordResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($addRemoveOGMRecordResult, true), gettype($addRemoveOGMRecordResult)), __LINE__);
        }
        $this->AddRemoveOGMRecordResult = $addRemoveOGMRecordResult;
        
        return $this;
    }
    /**
     * Get sError value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSError(): ?string
    {
        return isset($this->sError) ? $this->sError : null;
    }
    /**
     * Set sError value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sError
     * @return \ID3Global\Models\AddRemoveOGMRecordResponse
     */
    public function setSError(?string $sError = null): self
    {
        // validation for constraint: string
        if (!is_null($sError) && !is_string($sError)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sError, true), gettype($sError)), __LINE__);
        }
        if (is_null($sError) || (is_array($sError) && empty($sError))) {
            unset($this->sError);
        } else {
            $this->sError = $sError;
        }
        
        return $this;
    }
    /**
     * Get SourceName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSourceName(): ?string
    {
        return isset($this->SourceName) ? $this->SourceName : null;
    }
    /**
     * Set SourceName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sourceName
     * @return \ID3Global\Models\AddRemoveOGMRecordResponse
     */
    public function setSourceName(?string $sourceName = null): self
    {
        // validation for constraint: string
        if (!is_null($sourceName) && !is_string($sourceName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sourceName, true), gettype($sourceName)), __LINE__);
        }
        if (is_null($sourceName) || (is_array($sourceName) && empty($sourceName))) {
            unset($this->SourceName);
        } else {
            $this->SourceName = $sourceName;
        }
        
        return $this;
    }
}
