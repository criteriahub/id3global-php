<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ImageData Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q142:ImageData
 * @subpackage Structs
 */
class ImageData extends AbstractStructBase
{
    /**
     * The CroppedHeight
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $CroppedHeight = null;
    /**
     * The CroppedWidth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $CroppedWidth = null;
    /**
     * The SubmittedHeight
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $SubmittedHeight = null;
    /**
     * The SubmittedWidth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $SubmittedWidth = null;
    /**
     * The SubmittedSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $SubmittedSize = null;
    /**
     * The DeviceUsed
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DeviceUsed = null;
    /**
     * The Latitude
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Latitude = null;
    /**
     * The Longitude
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Longitude = null;
    /**
     * The SoftwareUsed
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SoftwareUsed = null;
    /**
     * Constructor method for ImageData
     * @uses ImageData::setCroppedHeight()
     * @uses ImageData::setCroppedWidth()
     * @uses ImageData::setSubmittedHeight()
     * @uses ImageData::setSubmittedWidth()
     * @uses ImageData::setSubmittedSize()
     * @uses ImageData::setDeviceUsed()
     * @uses ImageData::setLatitude()
     * @uses ImageData::setLongitude()
     * @uses ImageData::setSoftwareUsed()
     * @param int $croppedHeight
     * @param int $croppedWidth
     * @param int $submittedHeight
     * @param int $submittedWidth
     * @param int $submittedSize
     * @param string $deviceUsed
     * @param string $latitude
     * @param string $longitude
     * @param string $softwareUsed
     */
    public function __construct(?int $croppedHeight = null, ?int $croppedWidth = null, ?int $submittedHeight = null, ?int $submittedWidth = null, ?int $submittedSize = null, ?string $deviceUsed = null, ?string $latitude = null, ?string $longitude = null, ?string $softwareUsed = null)
    {
        $this
            ->setCroppedHeight($croppedHeight)
            ->setCroppedWidth($croppedWidth)
            ->setSubmittedHeight($submittedHeight)
            ->setSubmittedWidth($submittedWidth)
            ->setSubmittedSize($submittedSize)
            ->setDeviceUsed($deviceUsed)
            ->setLatitude($latitude)
            ->setLongitude($longitude)
            ->setSoftwareUsed($softwareUsed);
    }
    /**
     * Get CroppedHeight value
     * @return int|null
     */
    public function getCroppedHeight(): ?int
    {
        return $this->CroppedHeight;
    }
    /**
     * Set CroppedHeight value
     * @param int $croppedHeight
     * @return \ID3Global\Models\ImageData
     */
    public function setCroppedHeight(?int $croppedHeight = null): self
    {
        // validation for constraint: int
        if (!is_null($croppedHeight) && !(is_int($croppedHeight) || ctype_digit($croppedHeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($croppedHeight, true), gettype($croppedHeight)), __LINE__);
        }
        $this->CroppedHeight = $croppedHeight;
        
        return $this;
    }
    /**
     * Get CroppedWidth value
     * @return int|null
     */
    public function getCroppedWidth(): ?int
    {
        return $this->CroppedWidth;
    }
    /**
     * Set CroppedWidth value
     * @param int $croppedWidth
     * @return \ID3Global\Models\ImageData
     */
    public function setCroppedWidth(?int $croppedWidth = null): self
    {
        // validation for constraint: int
        if (!is_null($croppedWidth) && !(is_int($croppedWidth) || ctype_digit($croppedWidth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($croppedWidth, true), gettype($croppedWidth)), __LINE__);
        }
        $this->CroppedWidth = $croppedWidth;
        
        return $this;
    }
    /**
     * Get SubmittedHeight value
     * @return int|null
     */
    public function getSubmittedHeight(): ?int
    {
        return $this->SubmittedHeight;
    }
    /**
     * Set SubmittedHeight value
     * @param int $submittedHeight
     * @return \ID3Global\Models\ImageData
     */
    public function setSubmittedHeight(?int $submittedHeight = null): self
    {
        // validation for constraint: int
        if (!is_null($submittedHeight) && !(is_int($submittedHeight) || ctype_digit($submittedHeight))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($submittedHeight, true), gettype($submittedHeight)), __LINE__);
        }
        $this->SubmittedHeight = $submittedHeight;
        
        return $this;
    }
    /**
     * Get SubmittedWidth value
     * @return int|null
     */
    public function getSubmittedWidth(): ?int
    {
        return $this->SubmittedWidth;
    }
    /**
     * Set SubmittedWidth value
     * @param int $submittedWidth
     * @return \ID3Global\Models\ImageData
     */
    public function setSubmittedWidth(?int $submittedWidth = null): self
    {
        // validation for constraint: int
        if (!is_null($submittedWidth) && !(is_int($submittedWidth) || ctype_digit($submittedWidth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($submittedWidth, true), gettype($submittedWidth)), __LINE__);
        }
        $this->SubmittedWidth = $submittedWidth;
        
        return $this;
    }
    /**
     * Get SubmittedSize value
     * @return int|null
     */
    public function getSubmittedSize(): ?int
    {
        return $this->SubmittedSize;
    }
    /**
     * Set SubmittedSize value
     * @param int $submittedSize
     * @return \ID3Global\Models\ImageData
     */
    public function setSubmittedSize(?int $submittedSize = null): self
    {
        // validation for constraint: int
        if (!is_null($submittedSize) && !(is_int($submittedSize) || ctype_digit($submittedSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($submittedSize, true), gettype($submittedSize)), __LINE__);
        }
        $this->SubmittedSize = $submittedSize;
        
        return $this;
    }
    /**
     * Get DeviceUsed value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDeviceUsed(): ?string
    {
        return isset($this->DeviceUsed) ? $this->DeviceUsed : null;
    }
    /**
     * Set DeviceUsed value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $deviceUsed
     * @return \ID3Global\Models\ImageData
     */
    public function setDeviceUsed(?string $deviceUsed = null): self
    {
        // validation for constraint: string
        if (!is_null($deviceUsed) && !is_string($deviceUsed)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deviceUsed, true), gettype($deviceUsed)), __LINE__);
        }
        if (is_null($deviceUsed) || (is_array($deviceUsed) && empty($deviceUsed))) {
            unset($this->DeviceUsed);
        } else {
            $this->DeviceUsed = $deviceUsed;
        }
        
        return $this;
    }
    /**
     * Get Latitude value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLatitude(): ?string
    {
        return isset($this->Latitude) ? $this->Latitude : null;
    }
    /**
     * Set Latitude value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $latitude
     * @return \ID3Global\Models\ImageData
     */
    public function setLatitude(?string $latitude = null): self
    {
        // validation for constraint: string
        if (!is_null($latitude) && !is_string($latitude)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($latitude, true), gettype($latitude)), __LINE__);
        }
        if (is_null($latitude) || (is_array($latitude) && empty($latitude))) {
            unset($this->Latitude);
        } else {
            $this->Latitude = $latitude;
        }
        
        return $this;
    }
    /**
     * Get Longitude value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLongitude(): ?string
    {
        return isset($this->Longitude) ? $this->Longitude : null;
    }
    /**
     * Set Longitude value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $longitude
     * @return \ID3Global\Models\ImageData
     */
    public function setLongitude(?string $longitude = null): self
    {
        // validation for constraint: string
        if (!is_null($longitude) && !is_string($longitude)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($longitude, true), gettype($longitude)), __LINE__);
        }
        if (is_null($longitude) || (is_array($longitude) && empty($longitude))) {
            unset($this->Longitude);
        } else {
            $this->Longitude = $longitude;
        }
        
        return $this;
    }
    /**
     * Get SoftwareUsed value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSoftwareUsed(): ?string
    {
        return isset($this->SoftwareUsed) ? $this->SoftwareUsed : null;
    }
    /**
     * Set SoftwareUsed value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $softwareUsed
     * @return \ID3Global\Models\ImageData
     */
    public function setSoftwareUsed(?string $softwareUsed = null): self
    {
        // validation for constraint: string
        if (!is_null($softwareUsed) && !is_string($softwareUsed)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($softwareUsed, true), gettype($softwareUsed)), __LINE__);
        }
        if (is_null($softwareUsed) || (is_array($softwareUsed) && empty($softwareUsed))) {
            unset($this->SoftwareUsed);
        } else {
            $this->SoftwareUsed = $softwareUsed;
        }
        
        return $this;
    }
}
