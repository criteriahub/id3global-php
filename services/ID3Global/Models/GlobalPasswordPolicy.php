<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalPasswordPolicy Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q10:GlobalPasswordPolicy
 * @subpackage Structs
 */
class GlobalPasswordPolicy extends AbstractStructBase
{
    /**
     * The Length
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Length = null;
    /**
     * The Uppercase
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Uppercase = null;
    /**
     * The Lowercase
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Lowercase = null;
    /**
     * The Numeric
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Numeric = null;
    /**
     * The Symbols
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Symbols = null;
    /**
     * The DisableInactiveDays
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $DisableInactiveDays = null;
    /**
     * The ChangePasswordDays
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ChangePasswordDays = null;
    /**
     * The RequirePINForSignin
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $RequirePINForSignin = null;
    /**
     * Constructor method for GlobalPasswordPolicy
     * @uses GlobalPasswordPolicy::setLength()
     * @uses GlobalPasswordPolicy::setUppercase()
     * @uses GlobalPasswordPolicy::setLowercase()
     * @uses GlobalPasswordPolicy::setNumeric()
     * @uses GlobalPasswordPolicy::setSymbols()
     * @uses GlobalPasswordPolicy::setDisableInactiveDays()
     * @uses GlobalPasswordPolicy::setChangePasswordDays()
     * @uses GlobalPasswordPolicy::setRequirePINForSignin()
     * @param int $length
     * @param int $uppercase
     * @param int $lowercase
     * @param int $numeric
     * @param int $symbols
     * @param int $disableInactiveDays
     * @param int $changePasswordDays
     * @param bool $requirePINForSignin
     */
    public function __construct(?int $length = null, ?int $uppercase = null, ?int $lowercase = null, ?int $numeric = null, ?int $symbols = null, ?int $disableInactiveDays = null, ?int $changePasswordDays = null, ?bool $requirePINForSignin = null)
    {
        $this
            ->setLength($length)
            ->setUppercase($uppercase)
            ->setLowercase($lowercase)
            ->setNumeric($numeric)
            ->setSymbols($symbols)
            ->setDisableInactiveDays($disableInactiveDays)
            ->setChangePasswordDays($changePasswordDays)
            ->setRequirePINForSignin($requirePINForSignin);
    }
    /**
     * Get Length value
     * @return int|null
     */
    public function getLength(): ?int
    {
        return $this->Length;
    }
    /**
     * Set Length value
     * @param int $length
     * @return \ID3Global\Models\GlobalPasswordPolicy
     */
    public function setLength(?int $length = null): self
    {
        // validation for constraint: int
        if (!is_null($length) && !(is_int($length) || ctype_digit($length))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($length, true), gettype($length)), __LINE__);
        }
        $this->Length = $length;
        
        return $this;
    }
    /**
     * Get Uppercase value
     * @return int|null
     */
    public function getUppercase(): ?int
    {
        return $this->Uppercase;
    }
    /**
     * Set Uppercase value
     * @param int $uppercase
     * @return \ID3Global\Models\GlobalPasswordPolicy
     */
    public function setUppercase(?int $uppercase = null): self
    {
        // validation for constraint: int
        if (!is_null($uppercase) && !(is_int($uppercase) || ctype_digit($uppercase))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($uppercase, true), gettype($uppercase)), __LINE__);
        }
        $this->Uppercase = $uppercase;
        
        return $this;
    }
    /**
     * Get Lowercase value
     * @return int|null
     */
    public function getLowercase(): ?int
    {
        return $this->Lowercase;
    }
    /**
     * Set Lowercase value
     * @param int $lowercase
     * @return \ID3Global\Models\GlobalPasswordPolicy
     */
    public function setLowercase(?int $lowercase = null): self
    {
        // validation for constraint: int
        if (!is_null($lowercase) && !(is_int($lowercase) || ctype_digit($lowercase))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($lowercase, true), gettype($lowercase)), __LINE__);
        }
        $this->Lowercase = $lowercase;
        
        return $this;
    }
    /**
     * Get Numeric value
     * @return int|null
     */
    public function getNumeric(): ?int
    {
        return $this->Numeric;
    }
    /**
     * Set Numeric value
     * @param int $numeric
     * @return \ID3Global\Models\GlobalPasswordPolicy
     */
    public function setNumeric(?int $numeric = null): self
    {
        // validation for constraint: int
        if (!is_null($numeric) && !(is_int($numeric) || ctype_digit($numeric))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($numeric, true), gettype($numeric)), __LINE__);
        }
        $this->Numeric = $numeric;
        
        return $this;
    }
    /**
     * Get Symbols value
     * @return int|null
     */
    public function getSymbols(): ?int
    {
        return $this->Symbols;
    }
    /**
     * Set Symbols value
     * @param int $symbols
     * @return \ID3Global\Models\GlobalPasswordPolicy
     */
    public function setSymbols(?int $symbols = null): self
    {
        // validation for constraint: int
        if (!is_null($symbols) && !(is_int($symbols) || ctype_digit($symbols))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($symbols, true), gettype($symbols)), __LINE__);
        }
        $this->Symbols = $symbols;
        
        return $this;
    }
    /**
     * Get DisableInactiveDays value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getDisableInactiveDays(): ?int
    {
        return isset($this->DisableInactiveDays) ? $this->DisableInactiveDays : null;
    }
    /**
     * Set DisableInactiveDays value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $disableInactiveDays
     * @return \ID3Global\Models\GlobalPasswordPolicy
     */
    public function setDisableInactiveDays(?int $disableInactiveDays = null): self
    {
        // validation for constraint: int
        if (!is_null($disableInactiveDays) && !(is_int($disableInactiveDays) || ctype_digit($disableInactiveDays))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($disableInactiveDays, true), gettype($disableInactiveDays)), __LINE__);
        }
        if (is_null($disableInactiveDays) || (is_array($disableInactiveDays) && empty($disableInactiveDays))) {
            unset($this->DisableInactiveDays);
        } else {
            $this->DisableInactiveDays = $disableInactiveDays;
        }
        
        return $this;
    }
    /**
     * Get ChangePasswordDays value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getChangePasswordDays(): ?int
    {
        return isset($this->ChangePasswordDays) ? $this->ChangePasswordDays : null;
    }
    /**
     * Set ChangePasswordDays value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $changePasswordDays
     * @return \ID3Global\Models\GlobalPasswordPolicy
     */
    public function setChangePasswordDays(?int $changePasswordDays = null): self
    {
        // validation for constraint: int
        if (!is_null($changePasswordDays) && !(is_int($changePasswordDays) || ctype_digit($changePasswordDays))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($changePasswordDays, true), gettype($changePasswordDays)), __LINE__);
        }
        if (is_null($changePasswordDays) || (is_array($changePasswordDays) && empty($changePasswordDays))) {
            unset($this->ChangePasswordDays);
        } else {
            $this->ChangePasswordDays = $changePasswordDays;
        }
        
        return $this;
    }
    /**
     * Get RequirePINForSignin value
     * @return bool|null
     */
    public function getRequirePINForSignin(): ?bool
    {
        return $this->RequirePINForSignin;
    }
    /**
     * Set RequirePINForSignin value
     * @param bool $requirePINForSignin
     * @return \ID3Global\Models\GlobalPasswordPolicy
     */
    public function setRequirePINForSignin(?bool $requirePINForSignin = null): self
    {
        // validation for constraint: boolean
        if (!is_null($requirePINForSignin) && !is_bool($requirePINForSignin)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($requirePINForSignin, true), gettype($requirePINForSignin)), __LINE__);
        }
        $this->RequirePINForSignin = $requirePINForSignin;
        
        return $this;
    }
}
