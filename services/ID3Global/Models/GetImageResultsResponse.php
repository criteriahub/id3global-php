<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetImageResultsResponse Models
 * @subpackage Structs
 */
class GetImageResultsResponse extends AbstractStructBase
{
    /**
     * The GetImageResultsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDIVData|null
     */
    protected ?\ID3Global\Models\GlobalDIVData $GetImageResultsResult = null;
    /**
     * Constructor method for GetImageResultsResponse
     * @uses GetImageResultsResponse::setGetImageResultsResult()
     * @param \ID3Global\Models\GlobalDIVData $getImageResultsResult
     */
    public function __construct(?\ID3Global\Models\GlobalDIVData $getImageResultsResult = null)
    {
        $this
            ->setGetImageResultsResult($getImageResultsResult);
    }
    /**
     * Get GetImageResultsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDIVData|null
     */
    public function getGetImageResultsResult(): ?\ID3Global\Models\GlobalDIVData
    {
        return isset($this->GetImageResultsResult) ? $this->GetImageResultsResult : null;
    }
    /**
     * Set GetImageResultsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalDIVData $getImageResultsResult
     * @return \ID3Global\Models\GetImageResultsResponse
     */
    public function setGetImageResultsResult(?\ID3Global\Models\GlobalDIVData $getImageResultsResult = null): self
    {
        if (is_null($getImageResultsResult) || (is_array($getImageResultsResult) && empty($getImageResultsResult))) {
            unset($this->GetImageResultsResult);
        } else {
            $this->GetImageResultsResult = $getImageResultsResult;
        }
        
        return $this;
    }
}
