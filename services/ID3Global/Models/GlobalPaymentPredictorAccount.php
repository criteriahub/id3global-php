<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalPaymentPredictorAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q666:GlobalPaymentPredictorAccount
 * @subpackage Structs
 */
class GlobalPaymentPredictorAccount extends GlobalSupplierAccount
{
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The ClientCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ClientCode = null;
    /**
     * The Contributor
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Contributor = null;
    /**
     * The Plus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Plus = null;
    /**
     * Constructor method for GlobalPaymentPredictorAccount
     * @uses GlobalPaymentPredictorAccount::setUsername()
     * @uses GlobalPaymentPredictorAccount::setClientCode()
     * @uses GlobalPaymentPredictorAccount::setContributor()
     * @uses GlobalPaymentPredictorAccount::setPlus()
     * @param string $username
     * @param string $clientCode
     * @param bool $contributor
     * @param bool $plus
     */
    public function __construct(?string $username = null, ?string $clientCode = null, ?bool $contributor = null, ?bool $plus = null)
    {
        $this
            ->setUsername($username)
            ->setClientCode($clientCode)
            ->setContributor($contributor)
            ->setPlus($plus);
    }
    /**
     * Get Username value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return isset($this->Username) ? $this->Username : null;
    }
    /**
     * Set Username value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username
     * @return \ID3Global\Models\GlobalPaymentPredictorAccount
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        if (is_null($username) || (is_array($username) && empty($username))) {
            unset($this->Username);
        } else {
            $this->Username = $username;
        }
        
        return $this;
    }
    /**
     * Get ClientCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getClientCode(): ?string
    {
        return isset($this->ClientCode) ? $this->ClientCode : null;
    }
    /**
     * Set ClientCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $clientCode
     * @return \ID3Global\Models\GlobalPaymentPredictorAccount
     */
    public function setClientCode(?string $clientCode = null): self
    {
        // validation for constraint: string
        if (!is_null($clientCode) && !is_string($clientCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($clientCode, true), gettype($clientCode)), __LINE__);
        }
        if (is_null($clientCode) || (is_array($clientCode) && empty($clientCode))) {
            unset($this->ClientCode);
        } else {
            $this->ClientCode = $clientCode;
        }
        
        return $this;
    }
    /**
     * Get Contributor value
     * @return bool|null
     */
    public function getContributor(): ?bool
    {
        return $this->Contributor;
    }
    /**
     * Set Contributor value
     * @param bool $contributor
     * @return \ID3Global\Models\GlobalPaymentPredictorAccount
     */
    public function setContributor(?bool $contributor = null): self
    {
        // validation for constraint: boolean
        if (!is_null($contributor) && !is_bool($contributor)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($contributor, true), gettype($contributor)), __LINE__);
        }
        $this->Contributor = $contributor;
        
        return $this;
    }
    /**
     * Get Plus value
     * @return bool|null
     */
    public function getPlus(): ?bool
    {
        return $this->Plus;
    }
    /**
     * Set Plus value
     * @param bool $plus
     * @return \ID3Global\Models\GlobalPaymentPredictorAccount
     */
    public function setPlus(?bool $plus = null): self
    {
        // validation for constraint: boolean
        if (!is_null($plus) && !is_bool($plus)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($plus, true), gettype($plus)), __LINE__);
        }
        $this->Plus = $plus;
        
        return $this;
    }
}
