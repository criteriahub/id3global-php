<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetImagesResponse Models
 * @subpackage Structs
 */
class GetImagesResponse extends AbstractStructBase
{
    /**
     * The GetImagesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalImages|null
     */
    protected ?\ID3Global\Models\GlobalImages $GetImagesResult = null;
    /**
     * Constructor method for GetImagesResponse
     * @uses GetImagesResponse::setGetImagesResult()
     * @param \ID3Global\Models\GlobalImages $getImagesResult
     */
    public function __construct(?\ID3Global\Models\GlobalImages $getImagesResult = null)
    {
        $this
            ->setGetImagesResult($getImagesResult);
    }
    /**
     * Get GetImagesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalImages|null
     */
    public function getGetImagesResult(): ?\ID3Global\Models\GlobalImages
    {
        return isset($this->GetImagesResult) ? $this->GetImagesResult : null;
    }
    /**
     * Set GetImagesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalImages $getImagesResult
     * @return \ID3Global\Models\GetImagesResponse
     */
    public function setGetImagesResult(?\ID3Global\Models\GlobalImages $getImagesResult = null): self
    {
        if (is_null($getImagesResult) || (is_array($getImagesResult) && empty($getImagesResult))) {
            unset($this->GetImagesResult);
        } else {
            $this->GetImagesResult = $getImagesResult;
        }
        
        return $this;
    }
}
