<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCIFASMatch Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q67:GlobalCIFASMatch
 * @subpackage Structs
 */
class GlobalCIFASMatch extends AbstractStructBase
{
    /**
     * The CaseID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CaseID = null;
    /**
     * The Url
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Url = null;
    /**
     * The ResultCodes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCIFASResultCode|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCIFASResultCode $ResultCodes = null;
    /**
     * Constructor method for GlobalCIFASMatch
     * @uses GlobalCIFASMatch::setCaseID()
     * @uses GlobalCIFASMatch::setUrl()
     * @uses GlobalCIFASMatch::setResultCodes()
     * @param string $caseID
     * @param string $url
     * @param \ID3Global\Arrays\ArrayOfGlobalCIFASResultCode $resultCodes
     */
    public function __construct(?string $caseID = null, ?string $url = null, ?\ID3Global\Arrays\ArrayOfGlobalCIFASResultCode $resultCodes = null)
    {
        $this
            ->setCaseID($caseID)
            ->setUrl($url)
            ->setResultCodes($resultCodes);
    }
    /**
     * Get CaseID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCaseID(): ?string
    {
        return isset($this->CaseID) ? $this->CaseID : null;
    }
    /**
     * Set CaseID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $caseID
     * @return \ID3Global\Models\GlobalCIFASMatch
     */
    public function setCaseID(?string $caseID = null): self
    {
        // validation for constraint: string
        if (!is_null($caseID) && !is_string($caseID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($caseID, true), gettype($caseID)), __LINE__);
        }
        if (is_null($caseID) || (is_array($caseID) && empty($caseID))) {
            unset($this->CaseID);
        } else {
            $this->CaseID = $caseID;
        }
        
        return $this;
    }
    /**
     * Get Url value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return isset($this->Url) ? $this->Url : null;
    }
    /**
     * Set Url value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $url
     * @return \ID3Global\Models\GlobalCIFASMatch
     */
    public function setUrl(?string $url = null): self
    {
        // validation for constraint: string
        if (!is_null($url) && !is_string($url)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($url, true), gettype($url)), __LINE__);
        }
        if (is_null($url) || (is_array($url) && empty($url))) {
            unset($this->Url);
        } else {
            $this->Url = $url;
        }
        
        return $this;
    }
    /**
     * Get ResultCodes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCIFASResultCode|null
     */
    public function getResultCodes(): ?\ID3Global\Arrays\ArrayOfGlobalCIFASResultCode
    {
        return isset($this->ResultCodes) ? $this->ResultCodes : null;
    }
    /**
     * Set ResultCodes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCIFASResultCode $resultCodes
     * @return \ID3Global\Models\GlobalCIFASMatch
     */
    public function setResultCodes(?\ID3Global\Arrays\ArrayOfGlobalCIFASResultCode $resultCodes = null): self
    {
        if (is_null($resultCodes) || (is_array($resultCodes) && empty($resultCodes))) {
            unset($this->ResultCodes);
        } else {
            $this->ResultCodes = $resultCodes;
        }
        
        return $this;
    }
}
