<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetReportParametersResponse Models
 * @subpackage Structs
 */
class GetReportParametersResponse extends AbstractStructBase
{
    /**
     * The GetReportParametersResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalReportParameter|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalReportParameter $GetReportParametersResult = null;
    /**
     * Constructor method for GetReportParametersResponse
     * @uses GetReportParametersResponse::setGetReportParametersResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalReportParameter $getReportParametersResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalReportParameter $getReportParametersResult = null)
    {
        $this
            ->setGetReportParametersResult($getReportParametersResult);
    }
    /**
     * Get GetReportParametersResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalReportParameter|null
     */
    public function getGetReportParametersResult(): ?\ID3Global\Arrays\ArrayOfGlobalReportParameter
    {
        return isset($this->GetReportParametersResult) ? $this->GetReportParametersResult : null;
    }
    /**
     * Set GetReportParametersResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalReportParameter $getReportParametersResult
     * @return \ID3Global\Models\GetReportParametersResponse
     */
    public function setGetReportParametersResult(?\ID3Global\Arrays\ArrayOfGlobalReportParameter $getReportParametersResult = null): self
    {
        if (is_null($getReportParametersResult) || (is_array($getReportParametersResult) && empty($getReportParametersResult))) {
            unset($this->GetReportParametersResult);
        } else {
            $this->GetReportParametersResult = $getReportParametersResult;
        }
        
        return $this;
    }
}
