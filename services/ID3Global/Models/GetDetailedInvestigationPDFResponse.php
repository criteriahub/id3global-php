<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDetailedInvestigationPDFResponse Models
 * @subpackage Structs
 */
class GetDetailedInvestigationPDFResponse extends AbstractStructBase
{
    /**
     * The GetDetailedInvestigationPDFResult
     * Meta information extracted from the WSDL
     * - base: xs:base64Binary
     * @var string|null
     */
    protected ?string $GetDetailedInvestigationPDFResult = null;
    /**
     * Constructor method for GetDetailedInvestigationPDFResponse
     * @uses GetDetailedInvestigationPDFResponse::setGetDetailedInvestigationPDFResult()
     * @param string $getDetailedInvestigationPDFResult
     */
    public function __construct(?string $getDetailedInvestigationPDFResult = null)
    {
        $this
            ->setGetDetailedInvestigationPDFResult($getDetailedInvestigationPDFResult);
    }
    /**
     * Get GetDetailedInvestigationPDFResult value
     * @return string|null
     */
    public function getGetDetailedInvestigationPDFResult(): ?string
    {
        return $this->GetDetailedInvestigationPDFResult;
    }
    /**
     * Set GetDetailedInvestigationPDFResult value
     * @param string $getDetailedInvestigationPDFResult
     * @return \ID3Global\Models\GetDetailedInvestigationPDFResponse
     */
    public function setGetDetailedInvestigationPDFResult(?string $getDetailedInvestigationPDFResult = null): self
    {
        // validation for constraint: string
        if (!is_null($getDetailedInvestigationPDFResult) && !is_string($getDetailedInvestigationPDFResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($getDetailedInvestigationPDFResult, true), gettype($getDetailedInvestigationPDFResult)), __LINE__);
        }
        $this->GetDetailedInvestigationPDFResult = $getDetailedInvestigationPDFResult;
        
        return $this;
    }
}
