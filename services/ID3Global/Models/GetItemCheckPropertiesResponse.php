<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetItemCheckPropertiesResponse Models
 * @subpackage Structs
 */
class GetItemCheckPropertiesResponse extends AbstractStructBase
{
    /**
     * The GetItemCheckPropertiesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalProperty|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalProperty $GetItemCheckPropertiesResult = null;
    /**
     * Constructor method for GetItemCheckPropertiesResponse
     * @uses GetItemCheckPropertiesResponse::setGetItemCheckPropertiesResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalProperty $getItemCheckPropertiesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalProperty $getItemCheckPropertiesResult = null)
    {
        $this
            ->setGetItemCheckPropertiesResult($getItemCheckPropertiesResult);
    }
    /**
     * Get GetItemCheckPropertiesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalProperty|null
     */
    public function getGetItemCheckPropertiesResult(): ?\ID3Global\Arrays\ArrayOfGlobalProperty
    {
        return isset($this->GetItemCheckPropertiesResult) ? $this->GetItemCheckPropertiesResult : null;
    }
    /**
     * Set GetItemCheckPropertiesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalProperty $getItemCheckPropertiesResult
     * @return \ID3Global\Models\GetItemCheckPropertiesResponse
     */
    public function setGetItemCheckPropertiesResult(?\ID3Global\Arrays\ArrayOfGlobalProperty $getItemCheckPropertiesResult = null): self
    {
        if (is_null($getItemCheckPropertiesResult) || (is_array($getItemCheckPropertiesResult) && empty($getItemCheckPropertiesResult))) {
            unset($this->GetItemCheckPropertiesResult);
        } else {
            $this->GetItemCheckPropertiesResult = $getItemCheckPropertiesResult;
        }
        
        return $this;
    }
}
