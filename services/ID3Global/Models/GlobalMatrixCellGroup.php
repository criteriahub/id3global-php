<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalMatrixCellGroup Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q823:GlobalMatrixCellGroup
 * @subpackage Structs
 */
class GlobalMatrixCellGroup extends AbstractStructBase
{
    /**
     * The Cells
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $Cells = null;
    /**
     * Constructor method for GlobalMatrixCellGroup
     * @uses GlobalMatrixCellGroup::setCells()
     * @param \ID3Global\Arrays\ArrayOfstring $cells
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfstring $cells = null)
    {
        $this
            ->setCells($cells);
    }
    /**
     * Get Cells value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getCells(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->Cells) ? $this->Cells : null;
    }
    /**
     * Set Cells value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $cells
     * @return \ID3Global\Models\GlobalMatrixCellGroup
     */
    public function setCells(?\ID3Global\Arrays\ArrayOfstring $cells = null): self
    {
        if (is_null($cells) || (is_array($cells) && empty($cells))) {
            unset($this->Cells);
        } else {
            $this->Cells = $cells;
        }
        
        return $this;
    }
}
