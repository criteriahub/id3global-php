<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CopyProfileResponse Models
 * @subpackage Structs
 */
class CopyProfileResponse extends AbstractStructBase
{
    /**
     * The CopyProfileResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileDetails|null
     */
    protected ?\ID3Global\Models\GlobalProfileDetails $CopyProfileResult = null;
    /**
     * Constructor method for CopyProfileResponse
     * @uses CopyProfileResponse::setCopyProfileResult()
     * @param \ID3Global\Models\GlobalProfileDetails $copyProfileResult
     */
    public function __construct(?\ID3Global\Models\GlobalProfileDetails $copyProfileResult = null)
    {
        $this
            ->setCopyProfileResult($copyProfileResult);
    }
    /**
     * Get CopyProfileResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileDetails|null
     */
    public function getCopyProfileResult(): ?\ID3Global\Models\GlobalProfileDetails
    {
        return isset($this->CopyProfileResult) ? $this->CopyProfileResult : null;
    }
    /**
     * Set CopyProfileResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileDetails $copyProfileResult
     * @return \ID3Global\Models\CopyProfileResponse
     */
    public function setCopyProfileResult(?\ID3Global\Models\GlobalProfileDetails $copyProfileResult = null): self
    {
        if (is_null($copyProfileResult) || (is_array($copyProfileResult) && empty($copyProfileResult))) {
            unset($this->CopyProfileResult);
        } else {
            $this->CopyProfileResult = $copyProfileResult;
        }
        
        return $this;
    }
}
