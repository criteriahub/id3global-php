<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetReportImageResponse Models
 * @subpackage Structs
 */
class GetReportImageResponse extends AbstractStructBase
{
    /**
     * The GetReportImageResult
     * Meta information extracted from the WSDL
     * - base: xs:base64Binary
     * @var string|null
     */
    protected ?string $GetReportImageResult = null;
    /**
     * Constructor method for GetReportImageResponse
     * @uses GetReportImageResponse::setGetReportImageResult()
     * @param string $getReportImageResult
     */
    public function __construct(?string $getReportImageResult = null)
    {
        $this
            ->setGetReportImageResult($getReportImageResult);
    }
    /**
     * Get GetReportImageResult value
     * @return string|null
     */
    public function getGetReportImageResult(): ?string
    {
        return $this->GetReportImageResult;
    }
    /**
     * Set GetReportImageResult value
     * @param string $getReportImageResult
     * @return \ID3Global\Models\GetReportImageResponse
     */
    public function setGetReportImageResult(?string $getReportImageResult = null): self
    {
        // validation for constraint: string
        if (!is_null($getReportImageResult) && !is_string($getReportImageResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($getReportImageResult, true), gettype($getReportImageResult)), __LINE__);
        }
        $this->GetReportImageResult = $getReportImageResult;
        
        return $this;
    }
}
