<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCIFASProductCodeResponse Models
 * @subpackage Structs
 */
class GetCIFASProductCodeResponse extends AbstractStructBase
{
    /**
     * The GetCIFASProductCodeResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCIFASProductCode|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCIFASProductCode $GetCIFASProductCodeResult = null;
    /**
     * Constructor method for GetCIFASProductCodeResponse
     * @uses GetCIFASProductCodeResponse::setGetCIFASProductCodeResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalCIFASProductCode $getCIFASProductCodeResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCIFASProductCode $getCIFASProductCodeResult = null)
    {
        $this
            ->setGetCIFASProductCodeResult($getCIFASProductCodeResult);
    }
    /**
     * Get GetCIFASProductCodeResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCIFASProductCode|null
     */
    public function getGetCIFASProductCodeResult(): ?\ID3Global\Arrays\ArrayOfGlobalCIFASProductCode
    {
        return isset($this->GetCIFASProductCodeResult) ? $this->GetCIFASProductCodeResult : null;
    }
    /**
     * Set GetCIFASProductCodeResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCIFASProductCode $getCIFASProductCodeResult
     * @return \ID3Global\Models\GetCIFASProductCodeResponse
     */
    public function setGetCIFASProductCodeResult(?\ID3Global\Arrays\ArrayOfGlobalCIFASProductCode $getCIFASProductCodeResult = null): self
    {
        if (is_null($getCIFASProductCodeResult) || (is_array($getCIFASProductCodeResult) && empty($getCIFASProductCodeResult))) {
            unset($this->GetCIFASProductCodeResult);
        } else {
            $this->GetCIFASProductCodeResult = $getCIFASProductCodeResult;
        }
        
        return $this;
    }
}
