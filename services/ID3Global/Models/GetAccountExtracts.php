<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAccountExtracts Models
 * @subpackage Structs
 */
class GetAccountExtracts extends AbstractStructBase
{
    /**
     * The orgId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $orgId = null;
    /**
     * The page
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $page = null;
    /**
     * The pageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $pageSize = null;
    /**
     * The filter
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $filter = null;
    /**
     * Constructor method for GetAccountExtracts
     * @uses GetAccountExtracts::setOrgId()
     * @uses GetAccountExtracts::setPage()
     * @uses GetAccountExtracts::setPageSize()
     * @uses GetAccountExtracts::setFilter()
     * @param string $orgId
     * @param int $page
     * @param int $pageSize
     * @param int $filter
     */
    public function __construct(?string $orgId = null, ?int $page = null, ?int $pageSize = null, ?int $filter = null)
    {
        $this
            ->setOrgId($orgId)
            ->setPage($page)
            ->setPageSize($pageSize)
            ->setFilter($filter);
    }
    /**
     * Get orgId value
     * @return string|null
     */
    public function getOrgId(): ?string
    {
        return $this->orgId;
    }
    /**
     * Set orgId value
     * @param string $orgId
     * @return \ID3Global\Models\GetAccountExtracts
     */
    public function setOrgId(?string $orgId = null): self
    {
        // validation for constraint: string
        if (!is_null($orgId) && !is_string($orgId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgId, true), gettype($orgId)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgId) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgId, true)), __LINE__);
        }
        $this->orgId = $orgId;
        
        return $this;
    }
    /**
     * Get page value
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->page;
    }
    /**
     * Set page value
     * @param int $page
     * @return \ID3Global\Models\GetAccountExtracts
     */
    public function setPage(?int $page = null): self
    {
        // validation for constraint: int
        if (!is_null($page) && !(is_int($page) || ctype_digit($page))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($page, true), gettype($page)), __LINE__);
        }
        $this->page = $page;
        
        return $this;
    }
    /**
     * Get pageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->pageSize;
    }
    /**
     * Set pageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GetAccountExtracts
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->pageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get filter value
     * @return int|null
     */
    public function getFilter(): ?int
    {
        return $this->filter;
    }
    /**
     * Set filter value
     * @param int $filter
     * @return \ID3Global\Models\GetAccountExtracts
     */
    public function setFilter(?int $filter = null): self
    {
        // validation for constraint: int
        if (!is_null($filter) && !(is_int($filter) || ctype_digit($filter))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($filter, true), gettype($filter)), __LINE__);
        }
        $this->filter = $filter;
        
        return $this;
    }
}
