<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPEPIntelligenceData Models
 * @subpackage Structs
 */
class GetPEPIntelligenceData extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The SanctionID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SanctionID = null;
    /**
     * Constructor method for GetPEPIntelligenceData
     * @uses GetPEPIntelligenceData::setOrgID()
     * @uses GetPEPIntelligenceData::setSanctionID()
     * @param string $orgID
     * @param string $sanctionID
     */
    public function __construct(?string $orgID = null, ?string $sanctionID = null)
    {
        $this
            ->setOrgID($orgID)
            ->setSanctionID($sanctionID);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetPEPIntelligenceData
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get SanctionID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSanctionID(): ?string
    {
        return isset($this->SanctionID) ? $this->SanctionID : null;
    }
    /**
     * Set SanctionID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sanctionID
     * @return \ID3Global\Models\GetPEPIntelligenceData
     */
    public function setSanctionID(?string $sanctionID = null): self
    {
        // validation for constraint: string
        if (!is_null($sanctionID) && !is_string($sanctionID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sanctionID, true), gettype($sanctionID)), __LINE__);
        }
        if (is_null($sanctionID) || (is_array($sanctionID) && empty($sanctionID))) {
            unset($this->SanctionID);
        } else {
            $this->SanctionID = $sanctionID;
        }
        
        return $this;
    }
}
