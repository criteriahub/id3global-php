<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDocumentExtract Models
 * @subpackage Structs
 */
class GetDocumentExtract extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The StartDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $StartDate = null;
    /**
     * The EndDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $EndDate = null;
    /**
     * The SearchType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $SearchType = null;
    /**
     * The SearchValue
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SearchValue = null;
    /**
     * The Format
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Format = null;
    /**
     * Constructor method for GetDocumentExtract
     * @uses GetDocumentExtract::setOrgID()
     * @uses GetDocumentExtract::setStartDate()
     * @uses GetDocumentExtract::setEndDate()
     * @uses GetDocumentExtract::setSearchType()
     * @uses GetDocumentExtract::setSearchValue()
     * @uses GetDocumentExtract::setFormat()
     * @param string $orgID
     * @param string $startDate
     * @param string $endDate
     * @param int $searchType
     * @param string $searchValue
     * @param int $format
     */
    public function __construct(?string $orgID = null, ?string $startDate = null, ?string $endDate = null, ?int $searchType = null, ?string $searchValue = null, ?int $format = null)
    {
        $this
            ->setOrgID($orgID)
            ->setStartDate($startDate)
            ->setEndDate($endDate)
            ->setSearchType($searchType)
            ->setSearchValue($searchValue)
            ->setFormat($format);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetDocumentExtract
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get StartDate value
     * @return string|null
     */
    public function getStartDate(): ?string
    {
        return $this->StartDate;
    }
    /**
     * Set StartDate value
     * @param string $startDate
     * @return \ID3Global\Models\GetDocumentExtract
     */
    public function setStartDate(?string $startDate = null): self
    {
        // validation for constraint: string
        if (!is_null($startDate) && !is_string($startDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startDate, true), gettype($startDate)), __LINE__);
        }
        $this->StartDate = $startDate;
        
        return $this;
    }
    /**
     * Get EndDate value
     * @return string|null
     */
    public function getEndDate(): ?string
    {
        return $this->EndDate;
    }
    /**
     * Set EndDate value
     * @param string $endDate
     * @return \ID3Global\Models\GetDocumentExtract
     */
    public function setEndDate(?string $endDate = null): self
    {
        // validation for constraint: string
        if (!is_null($endDate) && !is_string($endDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endDate, true), gettype($endDate)), __LINE__);
        }
        $this->EndDate = $endDate;
        
        return $this;
    }
    /**
     * Get SearchType value
     * @return int|null
     */
    public function getSearchType(): ?int
    {
        return $this->SearchType;
    }
    /**
     * Set SearchType value
     * @param int $searchType
     * @return \ID3Global\Models\GetDocumentExtract
     */
    public function setSearchType(?int $searchType = null): self
    {
        // validation for constraint: int
        if (!is_null($searchType) && !(is_int($searchType) || ctype_digit($searchType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($searchType, true), gettype($searchType)), __LINE__);
        }
        $this->SearchType = $searchType;
        
        return $this;
    }
    /**
     * Get SearchValue value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSearchValue(): ?string
    {
        return isset($this->SearchValue) ? $this->SearchValue : null;
    }
    /**
     * Set SearchValue value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $searchValue
     * @return \ID3Global\Models\GetDocumentExtract
     */
    public function setSearchValue(?string $searchValue = null): self
    {
        // validation for constraint: string
        if (!is_null($searchValue) && !is_string($searchValue)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchValue, true), gettype($searchValue)), __LINE__);
        }
        if (is_null($searchValue) || (is_array($searchValue) && empty($searchValue))) {
            unset($this->SearchValue);
        } else {
            $this->SearchValue = $searchValue;
        }
        
        return $this;
    }
    /**
     * Get Format value
     * @return int|null
     */
    public function getFormat(): ?int
    {
        return $this->Format;
    }
    /**
     * Set Format value
     * @param int $format
     * @return \ID3Global\Models\GetDocumentExtract
     */
    public function setFormat(?int $format = null): self
    {
        // validation for constraint: int
        if (!is_null($format) && !(is_int($format) || ctype_digit($format))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($format, true), gettype($format)), __LINE__);
        }
        $this->Format = $format;
        
        return $this;
    }
}
