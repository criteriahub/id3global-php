<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ImageChecks Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q141:ImageChecks
 * @subpackage Structs
 */
class ImageChecks extends AbstractStructBase
{
    /**
     * The Blur
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Blur = null;
    /**
     * The Dark
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Dark = null;
    /**
     * The Dpi
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Dpi = null;
    /**
     * The Entropy
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Entropy = null;
    /**
     * The Size
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Size = null;
    /**
     * The Overall
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Overall = null;
    /**
     * The DocumentHasMrz
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $DocumentHasMrz = null;
    /**
     * The MrzVisible
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $MrzVisible = null;
    /**
     * The Coloured
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Coloured = null;
    /**
     * The Expired
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Expired = null;
    /**
     * The MrzBlur
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $MrzBlur = null;
    /**
     * The Reduced
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Reduced = null;
    /**
     * The Obliquity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Obliquity = null;
    /**
     * The MrzReflection
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $MrzReflection = null;
    /**
     * The Reflection
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Reflection = null;
    /**
     * Constructor method for ImageChecks
     * @uses ImageChecks::setBlur()
     * @uses ImageChecks::setDark()
     * @uses ImageChecks::setDpi()
     * @uses ImageChecks::setEntropy()
     * @uses ImageChecks::setSize()
     * @uses ImageChecks::setOverall()
     * @uses ImageChecks::setDocumentHasMrz()
     * @uses ImageChecks::setMrzVisible()
     * @uses ImageChecks::setColoured()
     * @uses ImageChecks::setExpired()
     * @uses ImageChecks::setMrzBlur()
     * @uses ImageChecks::setReduced()
     * @uses ImageChecks::setObliquity()
     * @uses ImageChecks::setMrzReflection()
     * @uses ImageChecks::setReflection()
     * @param bool $blur
     * @param bool $dark
     * @param bool $dpi
     * @param bool $entropy
     * @param bool $size
     * @param bool $overall
     * @param bool $documentHasMrz
     * @param bool $mrzVisible
     * @param bool $coloured
     * @param bool $expired
     * @param bool $mrzBlur
     * @param bool $reduced
     * @param bool $obliquity
     * @param bool $mrzReflection
     * @param bool $reflection
     */
    public function __construct(?bool $blur = null, ?bool $dark = null, ?bool $dpi = null, ?bool $entropy = null, ?bool $size = null, ?bool $overall = null, ?bool $documentHasMrz = null, ?bool $mrzVisible = null, ?bool $coloured = null, ?bool $expired = null, ?bool $mrzBlur = null, ?bool $reduced = null, ?bool $obliquity = null, ?bool $mrzReflection = null, ?bool $reflection = null)
    {
        $this
            ->setBlur($blur)
            ->setDark($dark)
            ->setDpi($dpi)
            ->setEntropy($entropy)
            ->setSize($size)
            ->setOverall($overall)
            ->setDocumentHasMrz($documentHasMrz)
            ->setMrzVisible($mrzVisible)
            ->setColoured($coloured)
            ->setExpired($expired)
            ->setMrzBlur($mrzBlur)
            ->setReduced($reduced)
            ->setObliquity($obliquity)
            ->setMrzReflection($mrzReflection)
            ->setReflection($reflection);
    }
    /**
     * Get Blur value
     * @return bool|null
     */
    public function getBlur(): ?bool
    {
        return $this->Blur;
    }
    /**
     * Set Blur value
     * @param bool $blur
     * @return \ID3Global\Models\ImageChecks
     */
    public function setBlur(?bool $blur = null): self
    {
        // validation for constraint: boolean
        if (!is_null($blur) && !is_bool($blur)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($blur, true), gettype($blur)), __LINE__);
        }
        $this->Blur = $blur;
        
        return $this;
    }
    /**
     * Get Dark value
     * @return bool|null
     */
    public function getDark(): ?bool
    {
        return $this->Dark;
    }
    /**
     * Set Dark value
     * @param bool $dark
     * @return \ID3Global\Models\ImageChecks
     */
    public function setDark(?bool $dark = null): self
    {
        // validation for constraint: boolean
        if (!is_null($dark) && !is_bool($dark)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($dark, true), gettype($dark)), __LINE__);
        }
        $this->Dark = $dark;
        
        return $this;
    }
    /**
     * Get Dpi value
     * @return bool|null
     */
    public function getDpi(): ?bool
    {
        return $this->Dpi;
    }
    /**
     * Set Dpi value
     * @param bool $dpi
     * @return \ID3Global\Models\ImageChecks
     */
    public function setDpi(?bool $dpi = null): self
    {
        // validation for constraint: boolean
        if (!is_null($dpi) && !is_bool($dpi)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($dpi, true), gettype($dpi)), __LINE__);
        }
        $this->Dpi = $dpi;
        
        return $this;
    }
    /**
     * Get Entropy value
     * @return bool|null
     */
    public function getEntropy(): ?bool
    {
        return $this->Entropy;
    }
    /**
     * Set Entropy value
     * @param bool $entropy
     * @return \ID3Global\Models\ImageChecks
     */
    public function setEntropy(?bool $entropy = null): self
    {
        // validation for constraint: boolean
        if (!is_null($entropy) && !is_bool($entropy)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($entropy, true), gettype($entropy)), __LINE__);
        }
        $this->Entropy = $entropy;
        
        return $this;
    }
    /**
     * Get Size value
     * @return bool|null
     */
    public function getSize(): ?bool
    {
        return $this->Size;
    }
    /**
     * Set Size value
     * @param bool $size
     * @return \ID3Global\Models\ImageChecks
     */
    public function setSize(?bool $size = null): self
    {
        // validation for constraint: boolean
        if (!is_null($size) && !is_bool($size)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($size, true), gettype($size)), __LINE__);
        }
        $this->Size = $size;
        
        return $this;
    }
    /**
     * Get Overall value
     * @return bool|null
     */
    public function getOverall(): ?bool
    {
        return $this->Overall;
    }
    /**
     * Set Overall value
     * @param bool $overall
     * @return \ID3Global\Models\ImageChecks
     */
    public function setOverall(?bool $overall = null): self
    {
        // validation for constraint: boolean
        if (!is_null($overall) && !is_bool($overall)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($overall, true), gettype($overall)), __LINE__);
        }
        $this->Overall = $overall;
        
        return $this;
    }
    /**
     * Get DocumentHasMrz value
     * @return bool|null
     */
    public function getDocumentHasMrz(): ?bool
    {
        return $this->DocumentHasMrz;
    }
    /**
     * Set DocumentHasMrz value
     * @param bool $documentHasMrz
     * @return \ID3Global\Models\ImageChecks
     */
    public function setDocumentHasMrz(?bool $documentHasMrz = null): self
    {
        // validation for constraint: boolean
        if (!is_null($documentHasMrz) && !is_bool($documentHasMrz)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($documentHasMrz, true), gettype($documentHasMrz)), __LINE__);
        }
        $this->DocumentHasMrz = $documentHasMrz;
        
        return $this;
    }
    /**
     * Get MrzVisible value
     * @return bool|null
     */
    public function getMrzVisible(): ?bool
    {
        return $this->MrzVisible;
    }
    /**
     * Set MrzVisible value
     * @param bool $mrzVisible
     * @return \ID3Global\Models\ImageChecks
     */
    public function setMrzVisible(?bool $mrzVisible = null): self
    {
        // validation for constraint: boolean
        if (!is_null($mrzVisible) && !is_bool($mrzVisible)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($mrzVisible, true), gettype($mrzVisible)), __LINE__);
        }
        $this->MrzVisible = $mrzVisible;
        
        return $this;
    }
    /**
     * Get Coloured value
     * @return bool|null
     */
    public function getColoured(): ?bool
    {
        return $this->Coloured;
    }
    /**
     * Set Coloured value
     * @param bool $coloured
     * @return \ID3Global\Models\ImageChecks
     */
    public function setColoured(?bool $coloured = null): self
    {
        // validation for constraint: boolean
        if (!is_null($coloured) && !is_bool($coloured)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($coloured, true), gettype($coloured)), __LINE__);
        }
        $this->Coloured = $coloured;
        
        return $this;
    }
    /**
     * Get Expired value
     * @return bool|null
     */
    public function getExpired(): ?bool
    {
        return $this->Expired;
    }
    /**
     * Set Expired value
     * @param bool $expired
     * @return \ID3Global\Models\ImageChecks
     */
    public function setExpired(?bool $expired = null): self
    {
        // validation for constraint: boolean
        if (!is_null($expired) && !is_bool($expired)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($expired, true), gettype($expired)), __LINE__);
        }
        $this->Expired = $expired;
        
        return $this;
    }
    /**
     * Get MrzBlur value
     * @return bool|null
     */
    public function getMrzBlur(): ?bool
    {
        return $this->MrzBlur;
    }
    /**
     * Set MrzBlur value
     * @param bool $mrzBlur
     * @return \ID3Global\Models\ImageChecks
     */
    public function setMrzBlur(?bool $mrzBlur = null): self
    {
        // validation for constraint: boolean
        if (!is_null($mrzBlur) && !is_bool($mrzBlur)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($mrzBlur, true), gettype($mrzBlur)), __LINE__);
        }
        $this->MrzBlur = $mrzBlur;
        
        return $this;
    }
    /**
     * Get Reduced value
     * @return bool|null
     */
    public function getReduced(): ?bool
    {
        return $this->Reduced;
    }
    /**
     * Set Reduced value
     * @param bool $reduced
     * @return \ID3Global\Models\ImageChecks
     */
    public function setReduced(?bool $reduced = null): self
    {
        // validation for constraint: boolean
        if (!is_null($reduced) && !is_bool($reduced)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($reduced, true), gettype($reduced)), __LINE__);
        }
        $this->Reduced = $reduced;
        
        return $this;
    }
    /**
     * Get Obliquity value
     * @return bool|null
     */
    public function getObliquity(): ?bool
    {
        return $this->Obliquity;
    }
    /**
     * Set Obliquity value
     * @param bool $obliquity
     * @return \ID3Global\Models\ImageChecks
     */
    public function setObliquity(?bool $obliquity = null): self
    {
        // validation for constraint: boolean
        if (!is_null($obliquity) && !is_bool($obliquity)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($obliquity, true), gettype($obliquity)), __LINE__);
        }
        $this->Obliquity = $obliquity;
        
        return $this;
    }
    /**
     * Get MrzReflection value
     * @return bool|null
     */
    public function getMrzReflection(): ?bool
    {
        return $this->MrzReflection;
    }
    /**
     * Set MrzReflection value
     * @param bool $mrzReflection
     * @return \ID3Global\Models\ImageChecks
     */
    public function setMrzReflection(?bool $mrzReflection = null): self
    {
        // validation for constraint: boolean
        if (!is_null($mrzReflection) && !is_bool($mrzReflection)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($mrzReflection, true), gettype($mrzReflection)), __LINE__);
        }
        $this->MrzReflection = $mrzReflection;
        
        return $this;
    }
    /**
     * Get Reflection value
     * @return bool|null
     */
    public function getReflection(): ?bool
    {
        return $this->Reflection;
    }
    /**
     * Set Reflection value
     * @param bool $reflection
     * @return \ID3Global\Models\ImageChecks
     */
    public function setReflection(?bool $reflection = null): self
    {
        // validation for constraint: boolean
        if (!is_null($reflection) && !is_bool($reflection)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($reflection, true), gettype($reflection)), __LINE__);
        }
        $this->Reflection = $reflection;
        
        return $this;
    }
}
