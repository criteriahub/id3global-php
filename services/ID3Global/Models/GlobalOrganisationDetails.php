<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalOrganisationDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q554:GlobalOrganisationDetails
 * @subpackage Structs
 */
class GlobalOrganisationDetails extends GlobalOrganisation
{
    /**
     * The ManagementContact
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalContact|null
     */
    protected ?\ID3Global\Models\GlobalContact $ManagementContact = null;
    /**
     * The TechnicalContact
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalContact|null
     */
    protected ?\ID3Global\Models\GlobalContact $TechnicalContact = null;
    /**
     * The UserPolicy
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalPasswordPolicy|null
     */
    protected ?\ID3Global\Models\GlobalPasswordPolicy $UserPolicy = null;
    /**
     * The AdministratorPolicy
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalPasswordPolicy|null
     */
    protected ?\ID3Global\Models\GlobalPasswordPolicy $AdministratorPolicy = null;
    /**
     * The SupportContact
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupportContact|null
     */
    protected ?\ID3Global\Models\GlobalSupportContact $SupportContact = null;
    /**
     * The ManagedBy
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ManagedBy = null;
    /**
     * The UseEmailAsAccount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $UseEmailAsAccount = null;
    /**
     * The ContractStart
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ContractStart = null;
    /**
     * The ContractEnd
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ContractEnd = null;
    /**
     * The SelfcarePasswordResets
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $SelfcarePasswordResets = null;
    /**
     * The ManagedCustomer
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $ManagedCustomer = null;
    /**
     * The BINList
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $BINList = null;
    /**
     * The BINResponseText
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $BINResponseText = null;
    /**
     * The WelcomeEmailSent
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $WelcomeEmailSent = null;
    /**
     * The DataRetentionDetails
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDataRetentionDetails|null
     */
    protected ?\ID3Global\Models\GlobalDataRetentionDetails $DataRetentionDetails = null;
    /**
     * Constructor method for GlobalOrganisationDetails
     * @uses GlobalOrganisationDetails::setManagementContact()
     * @uses GlobalOrganisationDetails::setTechnicalContact()
     * @uses GlobalOrganisationDetails::setUserPolicy()
     * @uses GlobalOrganisationDetails::setAdministratorPolicy()
     * @uses GlobalOrganisationDetails::setSupportContact()
     * @uses GlobalOrganisationDetails::setManagedBy()
     * @uses GlobalOrganisationDetails::setUseEmailAsAccount()
     * @uses GlobalOrganisationDetails::setContractStart()
     * @uses GlobalOrganisationDetails::setContractEnd()
     * @uses GlobalOrganisationDetails::setSelfcarePasswordResets()
     * @uses GlobalOrganisationDetails::setManagedCustomer()
     * @uses GlobalOrganisationDetails::setBINList()
     * @uses GlobalOrganisationDetails::setBINResponseText()
     * @uses GlobalOrganisationDetails::setWelcomeEmailSent()
     * @uses GlobalOrganisationDetails::setDataRetentionDetails()
     * @param \ID3Global\Models\GlobalContact $managementContact
     * @param \ID3Global\Models\GlobalContact $technicalContact
     * @param \ID3Global\Models\GlobalPasswordPolicy $userPolicy
     * @param \ID3Global\Models\GlobalPasswordPolicy $administratorPolicy
     * @param \ID3Global\Models\GlobalSupportContact $supportContact
     * @param string $managedBy
     * @param bool $useEmailAsAccount
     * @param string $contractStart
     * @param string $contractEnd
     * @param bool $selfcarePasswordResets
     * @param bool $managedCustomer
     * @param \ID3Global\Arrays\ArrayOfstring $bINList
     * @param string $bINResponseText
     * @param string $welcomeEmailSent
     * @param \ID3Global\Models\GlobalDataRetentionDetails $dataRetentionDetails
     */
    public function __construct(?\ID3Global\Models\GlobalContact $managementContact = null, ?\ID3Global\Models\GlobalContact $technicalContact = null, ?\ID3Global\Models\GlobalPasswordPolicy $userPolicy = null, ?\ID3Global\Models\GlobalPasswordPolicy $administratorPolicy = null, ?\ID3Global\Models\GlobalSupportContact $supportContact = null, ?string $managedBy = null, ?bool $useEmailAsAccount = null, ?string $contractStart = null, ?string $contractEnd = null, ?bool $selfcarePasswordResets = null, ?bool $managedCustomer = null, ?\ID3Global\Arrays\ArrayOfstring $bINList = null, ?string $bINResponseText = null, ?string $welcomeEmailSent = null, ?\ID3Global\Models\GlobalDataRetentionDetails $dataRetentionDetails = null)
    {
        $this
            ->setManagementContact($managementContact)
            ->setTechnicalContact($technicalContact)
            ->setUserPolicy($userPolicy)
            ->setAdministratorPolicy($administratorPolicy)
            ->setSupportContact($supportContact)
            ->setManagedBy($managedBy)
            ->setUseEmailAsAccount($useEmailAsAccount)
            ->setContractStart($contractStart)
            ->setContractEnd($contractEnd)
            ->setSelfcarePasswordResets($selfcarePasswordResets)
            ->setManagedCustomer($managedCustomer)
            ->setBINList($bINList)
            ->setBINResponseText($bINResponseText)
            ->setWelcomeEmailSent($welcomeEmailSent)
            ->setDataRetentionDetails($dataRetentionDetails);
    }
    /**
     * Get ManagementContact value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalContact|null
     */
    public function getManagementContact(): ?\ID3Global\Models\GlobalContact
    {
        return isset($this->ManagementContact) ? $this->ManagementContact : null;
    }
    /**
     * Set ManagementContact value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalContact $managementContact
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setManagementContact(?\ID3Global\Models\GlobalContact $managementContact = null): self
    {
        if (is_null($managementContact) || (is_array($managementContact) && empty($managementContact))) {
            unset($this->ManagementContact);
        } else {
            $this->ManagementContact = $managementContact;
        }
        
        return $this;
    }
    /**
     * Get TechnicalContact value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalContact|null
     */
    public function getTechnicalContact(): ?\ID3Global\Models\GlobalContact
    {
        return isset($this->TechnicalContact) ? $this->TechnicalContact : null;
    }
    /**
     * Set TechnicalContact value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalContact $technicalContact
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setTechnicalContact(?\ID3Global\Models\GlobalContact $technicalContact = null): self
    {
        if (is_null($technicalContact) || (is_array($technicalContact) && empty($technicalContact))) {
            unset($this->TechnicalContact);
        } else {
            $this->TechnicalContact = $technicalContact;
        }
        
        return $this;
    }
    /**
     * Get UserPolicy value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalPasswordPolicy|null
     */
    public function getUserPolicy(): ?\ID3Global\Models\GlobalPasswordPolicy
    {
        return isset($this->UserPolicy) ? $this->UserPolicy : null;
    }
    /**
     * Set UserPolicy value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalPasswordPolicy $userPolicy
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setUserPolicy(?\ID3Global\Models\GlobalPasswordPolicy $userPolicy = null): self
    {
        if (is_null($userPolicy) || (is_array($userPolicy) && empty($userPolicy))) {
            unset($this->UserPolicy);
        } else {
            $this->UserPolicy = $userPolicy;
        }
        
        return $this;
    }
    /**
     * Get AdministratorPolicy value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalPasswordPolicy|null
     */
    public function getAdministratorPolicy(): ?\ID3Global\Models\GlobalPasswordPolicy
    {
        return isset($this->AdministratorPolicy) ? $this->AdministratorPolicy : null;
    }
    /**
     * Set AdministratorPolicy value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalPasswordPolicy $administratorPolicy
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setAdministratorPolicy(?\ID3Global\Models\GlobalPasswordPolicy $administratorPolicy = null): self
    {
        if (is_null($administratorPolicy) || (is_array($administratorPolicy) && empty($administratorPolicy))) {
            unset($this->AdministratorPolicy);
        } else {
            $this->AdministratorPolicy = $administratorPolicy;
        }
        
        return $this;
    }
    /**
     * Get SupportContact value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupportContact|null
     */
    public function getSupportContact(): ?\ID3Global\Models\GlobalSupportContact
    {
        return isset($this->SupportContact) ? $this->SupportContact : null;
    }
    /**
     * Set SupportContact value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupportContact $supportContact
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setSupportContact(?\ID3Global\Models\GlobalSupportContact $supportContact = null): self
    {
        if (is_null($supportContact) || (is_array($supportContact) && empty($supportContact))) {
            unset($this->SupportContact);
        } else {
            $this->SupportContact = $supportContact;
        }
        
        return $this;
    }
    /**
     * Get ManagedBy value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getManagedBy(): ?string
    {
        return isset($this->ManagedBy) ? $this->ManagedBy : null;
    }
    /**
     * Set ManagedBy value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $managedBy
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setManagedBy(?string $managedBy = null): self
    {
        // validation for constraint: string
        if (!is_null($managedBy) && !is_string($managedBy)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($managedBy, true), gettype($managedBy)), __LINE__);
        }
        if (is_null($managedBy) || (is_array($managedBy) && empty($managedBy))) {
            unset($this->ManagedBy);
        } else {
            $this->ManagedBy = $managedBy;
        }
        
        return $this;
    }
    /**
     * Get UseEmailAsAccount value
     * @return bool|null
     */
    public function getUseEmailAsAccount(): ?bool
    {
        return $this->UseEmailAsAccount;
    }
    /**
     * Set UseEmailAsAccount value
     * @param bool $useEmailAsAccount
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setUseEmailAsAccount(?bool $useEmailAsAccount = null): self
    {
        // validation for constraint: boolean
        if (!is_null($useEmailAsAccount) && !is_bool($useEmailAsAccount)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($useEmailAsAccount, true), gettype($useEmailAsAccount)), __LINE__);
        }
        $this->UseEmailAsAccount = $useEmailAsAccount;
        
        return $this;
    }
    /**
     * Get ContractStart value
     * @return string|null
     */
    public function getContractStart(): ?string
    {
        return $this->ContractStart;
    }
    /**
     * Set ContractStart value
     * @param string $contractStart
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setContractStart(?string $contractStart = null): self
    {
        // validation for constraint: string
        if (!is_null($contractStart) && !is_string($contractStart)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($contractStart, true), gettype($contractStart)), __LINE__);
        }
        $this->ContractStart = $contractStart;
        
        return $this;
    }
    /**
     * Get ContractEnd value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getContractEnd(): ?string
    {
        return isset($this->ContractEnd) ? $this->ContractEnd : null;
    }
    /**
     * Set ContractEnd value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $contractEnd
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setContractEnd(?string $contractEnd = null): self
    {
        // validation for constraint: string
        if (!is_null($contractEnd) && !is_string($contractEnd)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($contractEnd, true), gettype($contractEnd)), __LINE__);
        }
        if (is_null($contractEnd) || (is_array($contractEnd) && empty($contractEnd))) {
            unset($this->ContractEnd);
        } else {
            $this->ContractEnd = $contractEnd;
        }
        
        return $this;
    }
    /**
     * Get SelfcarePasswordResets value
     * @return bool|null
     */
    public function getSelfcarePasswordResets(): ?bool
    {
        return $this->SelfcarePasswordResets;
    }
    /**
     * Set SelfcarePasswordResets value
     * @param bool $selfcarePasswordResets
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setSelfcarePasswordResets(?bool $selfcarePasswordResets = null): self
    {
        // validation for constraint: boolean
        if (!is_null($selfcarePasswordResets) && !is_bool($selfcarePasswordResets)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($selfcarePasswordResets, true), gettype($selfcarePasswordResets)), __LINE__);
        }
        $this->SelfcarePasswordResets = $selfcarePasswordResets;
        
        return $this;
    }
    /**
     * Get ManagedCustomer value
     * @return bool|null
     */
    public function getManagedCustomer(): ?bool
    {
        return $this->ManagedCustomer;
    }
    /**
     * Set ManagedCustomer value
     * @param bool $managedCustomer
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setManagedCustomer(?bool $managedCustomer = null): self
    {
        // validation for constraint: boolean
        if (!is_null($managedCustomer) && !is_bool($managedCustomer)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($managedCustomer, true), gettype($managedCustomer)), __LINE__);
        }
        $this->ManagedCustomer = $managedCustomer;
        
        return $this;
    }
    /**
     * Get BINList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getBINList(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->BINList) ? $this->BINList : null;
    }
    /**
     * Set BINList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $bINList
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setBINList(?\ID3Global\Arrays\ArrayOfstring $bINList = null): self
    {
        if (is_null($bINList) || (is_array($bINList) && empty($bINList))) {
            unset($this->BINList);
        } else {
            $this->BINList = $bINList;
        }
        
        return $this;
    }
    /**
     * Get BINResponseText value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBINResponseText(): ?string
    {
        return isset($this->BINResponseText) ? $this->BINResponseText : null;
    }
    /**
     * Set BINResponseText value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $bINResponseText
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setBINResponseText(?string $bINResponseText = null): self
    {
        // validation for constraint: string
        if (!is_null($bINResponseText) && !is_string($bINResponseText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bINResponseText, true), gettype($bINResponseText)), __LINE__);
        }
        if (is_null($bINResponseText) || (is_array($bINResponseText) && empty($bINResponseText))) {
            unset($this->BINResponseText);
        } else {
            $this->BINResponseText = $bINResponseText;
        }
        
        return $this;
    }
    /**
     * Get WelcomeEmailSent value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getWelcomeEmailSent(): ?string
    {
        return isset($this->WelcomeEmailSent) ? $this->WelcomeEmailSent : null;
    }
    /**
     * Set WelcomeEmailSent value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $welcomeEmailSent
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setWelcomeEmailSent(?string $welcomeEmailSent = null): self
    {
        // validation for constraint: string
        if (!is_null($welcomeEmailSent) && !is_string($welcomeEmailSent)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($welcomeEmailSent, true), gettype($welcomeEmailSent)), __LINE__);
        }
        if (is_null($welcomeEmailSent) || (is_array($welcomeEmailSent) && empty($welcomeEmailSent))) {
            unset($this->WelcomeEmailSent);
        } else {
            $this->WelcomeEmailSent = $welcomeEmailSent;
        }
        
        return $this;
    }
    /**
     * Get DataRetentionDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDataRetentionDetails|null
     */
    public function getDataRetentionDetails(): ?\ID3Global\Models\GlobalDataRetentionDetails
    {
        return isset($this->DataRetentionDetails) ? $this->DataRetentionDetails : null;
    }
    /**
     * Set DataRetentionDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalDataRetentionDetails $dataRetentionDetails
     * @return \ID3Global\Models\GlobalOrganisationDetails
     */
    public function setDataRetentionDetails(?\ID3Global\Models\GlobalDataRetentionDetails $dataRetentionDetails = null): self
    {
        if (is_null($dataRetentionDetails) || (is_array($dataRetentionDetails) && empty($dataRetentionDetails))) {
            unset($this->DataRetentionDetails);
        } else {
            $this->DataRetentionDetails = $dataRetentionDetails;
        }
        
        return $this;
    }
}
