<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalItem Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q215:GlobalItem
 * @subpackage Structs
 */
class GlobalItem extends AbstractStructBase
{
    /**
     * The ID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Description = null;
    /**
     * The AllConditionsMustBeMet
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $AllConditionsMustBeMet = null;
    /**
     * The ResultCodesConditions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalConditionResultCodes|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCodes $ResultCodesConditions = null;
    /**
     * The ScoreConditions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalConditionScore|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalConditionScore $ScoreConditions = null;
    /**
     * The DecisionBandConditions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalConditionDecision|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalConditionDecision $DecisionBandConditions = null;
    /**
     * Constructor method for GlobalItem
     * @uses GlobalItem::setID()
     * @uses GlobalItem::setName()
     * @uses GlobalItem::setDescription()
     * @uses GlobalItem::setAllConditionsMustBeMet()
     * @uses GlobalItem::setResultCodesConditions()
     * @uses GlobalItem::setScoreConditions()
     * @uses GlobalItem::setDecisionBandConditions()
     * @param int $iD
     * @param string $name
     * @param string $description
     * @param bool $allConditionsMustBeMet
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionResultCodes $resultCodesConditions
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionScore $scoreConditions
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionDecision $decisionBandConditions
     */
    public function __construct(?int $iD = null, ?string $name = null, ?string $description = null, ?bool $allConditionsMustBeMet = null, ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCodes $resultCodesConditions = null, ?\ID3Global\Arrays\ArrayOfGlobalConditionScore $scoreConditions = null, ?\ID3Global\Arrays\ArrayOfGlobalConditionDecision $decisionBandConditions = null)
    {
        $this
            ->setID($iD)
            ->setName($name)
            ->setDescription($description)
            ->setAllConditionsMustBeMet($allConditionsMustBeMet)
            ->setResultCodesConditions($resultCodesConditions)
            ->setScoreConditions($scoreConditions)
            ->setDecisionBandConditions($decisionBandConditions);
    }
    /**
     * Get ID value
     * @return int|null
     */
    public function getID(): ?int
    {
        return $this->ID;
    }
    /**
     * Set ID value
     * @param int $iD
     * @return \ID3Global\Models\GlobalItem
     */
    public function setID(?int $iD = null): self
    {
        // validation for constraint: int
        if (!is_null($iD) && !(is_int($iD) || ctype_digit($iD))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($iD, true), gettype($iD)), __LINE__);
        }
        $this->ID = $iD;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalItem
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \ID3Global\Models\GlobalItem
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        
        return $this;
    }
    /**
     * Get AllConditionsMustBeMet value
     * @return bool|null
     */
    public function getAllConditionsMustBeMet(): ?bool
    {
        return $this->AllConditionsMustBeMet;
    }
    /**
     * Set AllConditionsMustBeMet value
     * @param bool $allConditionsMustBeMet
     * @return \ID3Global\Models\GlobalItem
     */
    public function setAllConditionsMustBeMet(?bool $allConditionsMustBeMet = null): self
    {
        // validation for constraint: boolean
        if (!is_null($allConditionsMustBeMet) && !is_bool($allConditionsMustBeMet)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allConditionsMustBeMet, true), gettype($allConditionsMustBeMet)), __LINE__);
        }
        $this->AllConditionsMustBeMet = $allConditionsMustBeMet;
        
        return $this;
    }
    /**
     * Get ResultCodesConditions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionResultCodes|null
     */
    public function getResultCodesConditions(): ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCodes
    {
        return isset($this->ResultCodesConditions) ? $this->ResultCodesConditions : null;
    }
    /**
     * Set ResultCodesConditions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionResultCodes $resultCodesConditions
     * @return \ID3Global\Models\GlobalItem
     */
    public function setResultCodesConditions(?\ID3Global\Arrays\ArrayOfGlobalConditionResultCodes $resultCodesConditions = null): self
    {
        if (is_null($resultCodesConditions) || (is_array($resultCodesConditions) && empty($resultCodesConditions))) {
            unset($this->ResultCodesConditions);
        } else {
            $this->ResultCodesConditions = $resultCodesConditions;
        }
        
        return $this;
    }
    /**
     * Get ScoreConditions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionScore|null
     */
    public function getScoreConditions(): ?\ID3Global\Arrays\ArrayOfGlobalConditionScore
    {
        return isset($this->ScoreConditions) ? $this->ScoreConditions : null;
    }
    /**
     * Set ScoreConditions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionScore $scoreConditions
     * @return \ID3Global\Models\GlobalItem
     */
    public function setScoreConditions(?\ID3Global\Arrays\ArrayOfGlobalConditionScore $scoreConditions = null): self
    {
        if (is_null($scoreConditions) || (is_array($scoreConditions) && empty($scoreConditions))) {
            unset($this->ScoreConditions);
        } else {
            $this->ScoreConditions = $scoreConditions;
        }
        
        return $this;
    }
    /**
     * Get DecisionBandConditions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionDecision|null
     */
    public function getDecisionBandConditions(): ?\ID3Global\Arrays\ArrayOfGlobalConditionDecision
    {
        return isset($this->DecisionBandConditions) ? $this->DecisionBandConditions : null;
    }
    /**
     * Set DecisionBandConditions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionDecision $decisionBandConditions
     * @return \ID3Global\Models\GlobalItem
     */
    public function setDecisionBandConditions(?\ID3Global\Arrays\ArrayOfGlobalConditionDecision $decisionBandConditions = null): self
    {
        if (is_null($decisionBandConditions) || (is_array($decisionBandConditions) && empty($decisionBandConditions))) {
            unset($this->DecisionBandConditions);
        } else {
            $this->DecisionBandConditions = $decisionBandConditions;
        }
        
        return $this;
    }
}
