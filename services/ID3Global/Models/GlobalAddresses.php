<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAddresses Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q331:GlobalAddresses
 * @subpackage Structs
 */
class GlobalAddresses extends AbstractStructBase
{
    /**
     * The CurrentAddress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAddress|null
     */
    protected ?\ID3Global\Models\GlobalAddress $CurrentAddress = null;
    /**
     * The PreviousAddress1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAddress|null
     */
    protected ?\ID3Global\Models\GlobalAddress $PreviousAddress1 = null;
    /**
     * The PreviousAddress2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAddress|null
     */
    protected ?\ID3Global\Models\GlobalAddress $PreviousAddress2 = null;
    /**
     * The PreviousAddress3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAddress|null
     */
    protected ?\ID3Global\Models\GlobalAddress $PreviousAddress3 = null;
    /**
     * The HistoricAddresses
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalAddress|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalAddress $HistoricAddresses = null;
    /**
     * Constructor method for GlobalAddresses
     * @uses GlobalAddresses::setCurrentAddress()
     * @uses GlobalAddresses::setPreviousAddress1()
     * @uses GlobalAddresses::setPreviousAddress2()
     * @uses GlobalAddresses::setPreviousAddress3()
     * @uses GlobalAddresses::setHistoricAddresses()
     * @param \ID3Global\Models\GlobalAddress $currentAddress
     * @param \ID3Global\Models\GlobalAddress $previousAddress1
     * @param \ID3Global\Models\GlobalAddress $previousAddress2
     * @param \ID3Global\Models\GlobalAddress $previousAddress3
     * @param \ID3Global\Arrays\ArrayOfGlobalAddress $historicAddresses
     */
    public function __construct(?\ID3Global\Models\GlobalAddress $currentAddress = null, ?\ID3Global\Models\GlobalAddress $previousAddress1 = null, ?\ID3Global\Models\GlobalAddress $previousAddress2 = null, ?\ID3Global\Models\GlobalAddress $previousAddress3 = null, ?\ID3Global\Arrays\ArrayOfGlobalAddress $historicAddresses = null)
    {
        $this
            ->setCurrentAddress($currentAddress)
            ->setPreviousAddress1($previousAddress1)
            ->setPreviousAddress2($previousAddress2)
            ->setPreviousAddress3($previousAddress3)
            ->setHistoricAddresses($historicAddresses);
    }
    /**
     * Get CurrentAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAddress|null
     */
    public function getCurrentAddress(): ?\ID3Global\Models\GlobalAddress
    {
        return isset($this->CurrentAddress) ? $this->CurrentAddress : null;
    }
    /**
     * Set CurrentAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAddress $currentAddress
     * @return \ID3Global\Models\GlobalAddresses
     */
    public function setCurrentAddress(?\ID3Global\Models\GlobalAddress $currentAddress = null): self
    {
        if (is_null($currentAddress) || (is_array($currentAddress) && empty($currentAddress))) {
            unset($this->CurrentAddress);
        } else {
            $this->CurrentAddress = $currentAddress;
        }
        
        return $this;
    }
    /**
     * Get PreviousAddress1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAddress|null
     */
    public function getPreviousAddress1(): ?\ID3Global\Models\GlobalAddress
    {
        return isset($this->PreviousAddress1) ? $this->PreviousAddress1 : null;
    }
    /**
     * Set PreviousAddress1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAddress $previousAddress1
     * @return \ID3Global\Models\GlobalAddresses
     */
    public function setPreviousAddress1(?\ID3Global\Models\GlobalAddress $previousAddress1 = null): self
    {
        if (is_null($previousAddress1) || (is_array($previousAddress1) && empty($previousAddress1))) {
            unset($this->PreviousAddress1);
        } else {
            $this->PreviousAddress1 = $previousAddress1;
        }
        
        return $this;
    }
    /**
     * Get PreviousAddress2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAddress|null
     */
    public function getPreviousAddress2(): ?\ID3Global\Models\GlobalAddress
    {
        return isset($this->PreviousAddress2) ? $this->PreviousAddress2 : null;
    }
    /**
     * Set PreviousAddress2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAddress $previousAddress2
     * @return \ID3Global\Models\GlobalAddresses
     */
    public function setPreviousAddress2(?\ID3Global\Models\GlobalAddress $previousAddress2 = null): self
    {
        if (is_null($previousAddress2) || (is_array($previousAddress2) && empty($previousAddress2))) {
            unset($this->PreviousAddress2);
        } else {
            $this->PreviousAddress2 = $previousAddress2;
        }
        
        return $this;
    }
    /**
     * Get PreviousAddress3 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAddress|null
     */
    public function getPreviousAddress3(): ?\ID3Global\Models\GlobalAddress
    {
        return isset($this->PreviousAddress3) ? $this->PreviousAddress3 : null;
    }
    /**
     * Set PreviousAddress3 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAddress $previousAddress3
     * @return \ID3Global\Models\GlobalAddresses
     */
    public function setPreviousAddress3(?\ID3Global\Models\GlobalAddress $previousAddress3 = null): self
    {
        if (is_null($previousAddress3) || (is_array($previousAddress3) && empty($previousAddress3))) {
            unset($this->PreviousAddress3);
        } else {
            $this->PreviousAddress3 = $previousAddress3;
        }
        
        return $this;
    }
    /**
     * Get HistoricAddresses value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalAddress|null
     */
    public function getHistoricAddresses(): ?\ID3Global\Arrays\ArrayOfGlobalAddress
    {
        return isset($this->HistoricAddresses) ? $this->HistoricAddresses : null;
    }
    /**
     * Set HistoricAddresses value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalAddress $historicAddresses
     * @return \ID3Global\Models\GlobalAddresses
     */
    public function setHistoricAddresses(?\ID3Global\Arrays\ArrayOfGlobalAddress $historicAddresses = null): self
    {
        if (is_null($historicAddresses) || (is_array($historicAddresses) && empty($historicAddresses))) {
            unset($this->HistoricAddresses);
        } else {
            $this->HistoricAddresses = $historicAddresses;
        }
        
        return $this;
    }
}
