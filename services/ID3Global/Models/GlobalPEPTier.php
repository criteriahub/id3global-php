<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalPEPTier Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q109:GlobalPEPTier
 * @subpackage Structs
 */
class GlobalPEPTier extends AbstractStructBase
{
    /**
     * The Tier
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Tier = null;
    /**
     * The Guidelines
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $Guidelines = null;
    /**
     * Constructor method for GlobalPEPTier
     * @uses GlobalPEPTier::setTier()
     * @uses GlobalPEPTier::setGuidelines()
     * @param int $tier
     * @param \ID3Global\Arrays\ArrayOfstring $guidelines
     */
    public function __construct(?int $tier = null, ?\ID3Global\Arrays\ArrayOfstring $guidelines = null)
    {
        $this
            ->setTier($tier)
            ->setGuidelines($guidelines);
    }
    /**
     * Get Tier value
     * @return int|null
     */
    public function getTier(): ?int
    {
        return $this->Tier;
    }
    /**
     * Set Tier value
     * @param int $tier
     * @return \ID3Global\Models\GlobalPEPTier
     */
    public function setTier(?int $tier = null): self
    {
        // validation for constraint: int
        if (!is_null($tier) && !(is_int($tier) || ctype_digit($tier))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($tier, true), gettype($tier)), __LINE__);
        }
        $this->Tier = $tier;
        
        return $this;
    }
    /**
     * Get Guidelines value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getGuidelines(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->Guidelines) ? $this->Guidelines : null;
    }
    /**
     * Set Guidelines value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $guidelines
     * @return \ID3Global\Models\GlobalPEPTier
     */
    public function setGuidelines(?\ID3Global\Arrays\ArrayOfstring $guidelines = null): self
    {
        if (is_null($guidelines) || (is_array($guidelines) && empty($guidelines))) {
            unset($this->Guidelines);
        } else {
            $this->Guidelines = $guidelines;
        }
        
        return $this;
    }
}
