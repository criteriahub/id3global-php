<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseCustomerReference Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q750:GlobalCaseCustomerReference
 * @subpackage Structs
 */
class GlobalCaseCustomerReference extends AbstractStructBase
{
    /**
     * The RefrenceName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $RefrenceName = null;
    /**
     * The ShowOnInvoice
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $ShowOnInvoice = null;
    /**
     * The ShowOnReport
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $ShowOnReport = null;
    /**
     * The Mandatory
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Mandatory = null;
    /**
     * The IsSystem
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsSystem = null;
    /**
     * The IsLookup
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsLookup = null;
    /**
     * Constructor method for GlobalCaseCustomerReference
     * @uses GlobalCaseCustomerReference::setRefrenceName()
     * @uses GlobalCaseCustomerReference::setShowOnInvoice()
     * @uses GlobalCaseCustomerReference::setShowOnReport()
     * @uses GlobalCaseCustomerReference::setMandatory()
     * @uses GlobalCaseCustomerReference::setIsSystem()
     * @uses GlobalCaseCustomerReference::setIsLookup()
     * @param string $refrenceName
     * @param bool $showOnInvoice
     * @param bool $showOnReport
     * @param bool $mandatory
     * @param bool $isSystem
     * @param bool $isLookup
     */
    public function __construct(?string $refrenceName = null, ?bool $showOnInvoice = null, ?bool $showOnReport = null, ?bool $mandatory = null, ?bool $isSystem = null, ?bool $isLookup = null)
    {
        $this
            ->setRefrenceName($refrenceName)
            ->setShowOnInvoice($showOnInvoice)
            ->setShowOnReport($showOnReport)
            ->setMandatory($mandatory)
            ->setIsSystem($isSystem)
            ->setIsLookup($isLookup);
    }
    /**
     * Get RefrenceName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRefrenceName(): ?string
    {
        return isset($this->RefrenceName) ? $this->RefrenceName : null;
    }
    /**
     * Set RefrenceName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $refrenceName
     * @return \ID3Global\Models\GlobalCaseCustomerReference
     */
    public function setRefrenceName(?string $refrenceName = null): self
    {
        // validation for constraint: string
        if (!is_null($refrenceName) && !is_string($refrenceName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($refrenceName, true), gettype($refrenceName)), __LINE__);
        }
        if (is_null($refrenceName) || (is_array($refrenceName) && empty($refrenceName))) {
            unset($this->RefrenceName);
        } else {
            $this->RefrenceName = $refrenceName;
        }
        
        return $this;
    }
    /**
     * Get ShowOnInvoice value
     * @return bool|null
     */
    public function getShowOnInvoice(): ?bool
    {
        return $this->ShowOnInvoice;
    }
    /**
     * Set ShowOnInvoice value
     * @param bool $showOnInvoice
     * @return \ID3Global\Models\GlobalCaseCustomerReference
     */
    public function setShowOnInvoice(?bool $showOnInvoice = null): self
    {
        // validation for constraint: boolean
        if (!is_null($showOnInvoice) && !is_bool($showOnInvoice)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($showOnInvoice, true), gettype($showOnInvoice)), __LINE__);
        }
        $this->ShowOnInvoice = $showOnInvoice;
        
        return $this;
    }
    /**
     * Get ShowOnReport value
     * @return bool|null
     */
    public function getShowOnReport(): ?bool
    {
        return $this->ShowOnReport;
    }
    /**
     * Set ShowOnReport value
     * @param bool $showOnReport
     * @return \ID3Global\Models\GlobalCaseCustomerReference
     */
    public function setShowOnReport(?bool $showOnReport = null): self
    {
        // validation for constraint: boolean
        if (!is_null($showOnReport) && !is_bool($showOnReport)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($showOnReport, true), gettype($showOnReport)), __LINE__);
        }
        $this->ShowOnReport = $showOnReport;
        
        return $this;
    }
    /**
     * Get Mandatory value
     * @return bool|null
     */
    public function getMandatory(): ?bool
    {
        return $this->Mandatory;
    }
    /**
     * Set Mandatory value
     * @param bool $mandatory
     * @return \ID3Global\Models\GlobalCaseCustomerReference
     */
    public function setMandatory(?bool $mandatory = null): self
    {
        // validation for constraint: boolean
        if (!is_null($mandatory) && !is_bool($mandatory)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($mandatory, true), gettype($mandatory)), __LINE__);
        }
        $this->Mandatory = $mandatory;
        
        return $this;
    }
    /**
     * Get IsSystem value
     * @return bool|null
     */
    public function getIsSystem(): ?bool
    {
        return $this->IsSystem;
    }
    /**
     * Set IsSystem value
     * @param bool $isSystem
     * @return \ID3Global\Models\GlobalCaseCustomerReference
     */
    public function setIsSystem(?bool $isSystem = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isSystem) && !is_bool($isSystem)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isSystem, true), gettype($isSystem)), __LINE__);
        }
        $this->IsSystem = $isSystem;
        
        return $this;
    }
    /**
     * Get IsLookup value
     * @return bool|null
     */
    public function getIsLookup(): ?bool
    {
        return $this->IsLookup;
    }
    /**
     * Set IsLookup value
     * @param bool $isLookup
     * @return \ID3Global\Models\GlobalCaseCustomerReference
     */
    public function setIsLookup(?bool $isLookup = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isLookup) && !is_bool($isLookup)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isLookup, true), gettype($isLookup)), __LINE__);
        }
        $this->IsLookup = $isLookup;
        
        return $this;
    }
}
