<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProfileDetailsWithLatestRevisionResponse Models
 * @subpackage Structs
 */
class GetProfileDetailsWithLatestRevisionResponse extends AbstractStructBase
{
    /**
     * The GetProfileDetailsWithLatestRevisionResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileDetails|null
     */
    protected ?\ID3Global\Models\GlobalProfileDetails $GetProfileDetailsWithLatestRevisionResult = null;
    /**
     * Constructor method for GetProfileDetailsWithLatestRevisionResponse
     * @uses GetProfileDetailsWithLatestRevisionResponse::setGetProfileDetailsWithLatestRevisionResult()
     * @param \ID3Global\Models\GlobalProfileDetails $getProfileDetailsWithLatestRevisionResult
     */
    public function __construct(?\ID3Global\Models\GlobalProfileDetails $getProfileDetailsWithLatestRevisionResult = null)
    {
        $this
            ->setGetProfileDetailsWithLatestRevisionResult($getProfileDetailsWithLatestRevisionResult);
    }
    /**
     * Get GetProfileDetailsWithLatestRevisionResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileDetails|null
     */
    public function getGetProfileDetailsWithLatestRevisionResult(): ?\ID3Global\Models\GlobalProfileDetails
    {
        return isset($this->GetProfileDetailsWithLatestRevisionResult) ? $this->GetProfileDetailsWithLatestRevisionResult : null;
    }
    /**
     * Set GetProfileDetailsWithLatestRevisionResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileDetails $getProfileDetailsWithLatestRevisionResult
     * @return \ID3Global\Models\GetProfileDetailsWithLatestRevisionResponse
     */
    public function setGetProfileDetailsWithLatestRevisionResult(?\ID3Global\Models\GlobalProfileDetails $getProfileDetailsWithLatestRevisionResult = null): self
    {
        if (is_null($getProfileDetailsWithLatestRevisionResult) || (is_array($getProfileDetailsWithLatestRevisionResult) && empty($getProfileDetailsWithLatestRevisionResult))) {
            unset($this->GetProfileDetailsWithLatestRevisionResult);
        } else {
            $this->GetProfileDetailsWithLatestRevisionResult = $getProfileDetailsWithLatestRevisionResult;
        }
        
        return $this;
    }
}
