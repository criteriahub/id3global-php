<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCriminalRecordCheckDisclosureOffence Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q795:GlobalCriminalRecordCheckDisclosureOffence
 * @subpackage Structs
 */
class GlobalCriminalRecordCheckDisclosureOffence extends AbstractStructBase
{
    /**
     * The Location
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Location = null;
    /**
     * The Description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Description = null;
    /**
     * The OffenceDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $OffenceDate = null;
    /**
     * The OffenceType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $OffenceType = null;
    /**
     * Constructor method for GlobalCriminalRecordCheckDisclosureOffence
     * @uses GlobalCriminalRecordCheckDisclosureOffence::setLocation()
     * @uses GlobalCriminalRecordCheckDisclosureOffence::setDescription()
     * @uses GlobalCriminalRecordCheckDisclosureOffence::setOffenceDate()
     * @uses GlobalCriminalRecordCheckDisclosureOffence::setOffenceType()
     * @param string $location
     * @param string $description
     * @param string $offenceDate
     * @param string $offenceType
     */
    public function __construct(?string $location = null, ?string $description = null, ?string $offenceDate = null, ?string $offenceType = null)
    {
        $this
            ->setLocation($location)
            ->setDescription($description)
            ->setOffenceDate($offenceDate)
            ->setOffenceType($offenceType);
    }
    /**
     * Get Location value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return isset($this->Location) ? $this->Location : null;
    }
    /**
     * Set Location value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $location
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence
     */
    public function setLocation(?string $location = null): self
    {
        // validation for constraint: string
        if (!is_null($location) && !is_string($location)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($location, true), gettype($location)), __LINE__);
        }
        if (is_null($location) || (is_array($location) && empty($location))) {
            unset($this->Location);
        } else {
            $this->Location = $location;
        }
        
        return $this;
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        
        return $this;
    }
    /**
     * Get OffenceDate value
     * @return string|null
     */
    public function getOffenceDate(): ?string
    {
        return $this->OffenceDate;
    }
    /**
     * Set OffenceDate value
     * @param string $offenceDate
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence
     */
    public function setOffenceDate(?string $offenceDate = null): self
    {
        // validation for constraint: string
        if (!is_null($offenceDate) && !is_string($offenceDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($offenceDate, true), gettype($offenceDate)), __LINE__);
        }
        $this->OffenceDate = $offenceDate;
        
        return $this;
    }
    /**
     * Get OffenceType value
     * @return string|null
     */
    public function getOffenceType(): ?string
    {
        return $this->OffenceType;
    }
    /**
     * Set OffenceType value
     * @uses \ID3Global\Enums\GlobalCriminalRecordCheckDisclosureOffenceType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalCriminalRecordCheckDisclosureOffenceType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $offenceType
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosureOffence
     */
    public function setOffenceType(?string $offenceType = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalCriminalRecordCheckDisclosureOffenceType::valueIsValid($offenceType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalCriminalRecordCheckDisclosureOffenceType', is_array($offenceType) ? implode(', ', $offenceType) : var_export($offenceType, true), implode(', ', \ID3Global\Enums\GlobalCriminalRecordCheckDisclosureOffenceType::getValidValues())), __LINE__);
        }
        $this->OffenceType = $offenceType;
        
        return $this;
    }
}
