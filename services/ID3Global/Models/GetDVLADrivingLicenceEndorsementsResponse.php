<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDVLADrivingLicenceEndorsementsResponse Models
 * @subpackage Structs
 */
class GetDVLADrivingLicenceEndorsementsResponse extends AbstractStructBase
{
    /**
     * The GetDVLADrivingLicenceEndorsementsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceEndorsement|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceEndorsement $GetDVLADrivingLicenceEndorsementsResult = null;
    /**
     * Constructor method for GetDVLADrivingLicenceEndorsementsResponse
     * @uses GetDVLADrivingLicenceEndorsementsResponse::setGetDVLADrivingLicenceEndorsementsResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceEndorsement $getDVLADrivingLicenceEndorsementsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceEndorsement $getDVLADrivingLicenceEndorsementsResult = null)
    {
        $this
            ->setGetDVLADrivingLicenceEndorsementsResult($getDVLADrivingLicenceEndorsementsResult);
    }
    /**
     * Get GetDVLADrivingLicenceEndorsementsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceEndorsement|null
     */
    public function getGetDVLADrivingLicenceEndorsementsResult(): ?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceEndorsement
    {
        return isset($this->GetDVLADrivingLicenceEndorsementsResult) ? $this->GetDVLADrivingLicenceEndorsementsResult : null;
    }
    /**
     * Set GetDVLADrivingLicenceEndorsementsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceEndorsement $getDVLADrivingLicenceEndorsementsResult
     * @return \ID3Global\Models\GetDVLADrivingLicenceEndorsementsResponse
     */
    public function setGetDVLADrivingLicenceEndorsementsResult(?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceEndorsement $getDVLADrivingLicenceEndorsementsResult = null): self
    {
        if (is_null($getDVLADrivingLicenceEndorsementsResult) || (is_array($getDVLADrivingLicenceEndorsementsResult) && empty($getDVLADrivingLicenceEndorsementsResult))) {
            unset($this->GetDVLADrivingLicenceEndorsementsResult);
        } else {
            $this->GetDVLADrivingLicenceEndorsementsResult = $getDVLADrivingLicenceEndorsementsResult;
        }
        
        return $this;
    }
}
