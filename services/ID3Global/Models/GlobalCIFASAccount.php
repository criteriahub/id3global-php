<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCIFASAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q687:GlobalCIFASAccount
 * @subpackage Structs
 */
class GlobalCIFASAccount extends GlobalSupplierAccount
{
    /**
     * The MemberNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MemberNumber = null;
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * Constructor method for GlobalCIFASAccount
     * @uses GlobalCIFASAccount::setMemberNumber()
     * @uses GlobalCIFASAccount::setUsername()
     * @param string $memberNumber
     * @param string $username
     */
    public function __construct(?string $memberNumber = null, ?string $username = null)
    {
        $this
            ->setMemberNumber($memberNumber)
            ->setUsername($username);
    }
    /**
     * Get MemberNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMemberNumber(): ?string
    {
        return isset($this->MemberNumber) ? $this->MemberNumber : null;
    }
    /**
     * Set MemberNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $memberNumber
     * @return \ID3Global\Models\GlobalCIFASAccount
     */
    public function setMemberNumber(?string $memberNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($memberNumber) && !is_string($memberNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($memberNumber, true), gettype($memberNumber)), __LINE__);
        }
        if (is_null($memberNumber) || (is_array($memberNumber) && empty($memberNumber))) {
            unset($this->MemberNumber);
        } else {
            $this->MemberNumber = $memberNumber;
        }
        
        return $this;
    }
    /**
     * Get Username value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return isset($this->Username) ? $this->Username : null;
    }
    /**
     * Set Username value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username
     * @return \ID3Global\Models\GlobalCIFASAccount
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        if (is_null($username) || (is_array($username) && empty($username))) {
            unset($this->Username);
        } else {
            $this->Username = $username;
        }
        
        return $this;
    }
}
