<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ID3gException Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q6:ID3gException
 * @subpackage Structs
 */
class ID3gException extends AbstractStructBase
{
    /**
     * The ErrorNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ErrorNumber = null;
    /**
     * The Timestamp
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Timestamp = null;
    /**
     * The UniqueID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $UniqueID = null;
    /**
     * Constructor method for ID3gException
     * @uses ID3gException::setErrorNumber()
     * @uses ID3gException::setTimestamp()
     * @uses ID3gException::setUniqueID()
     * @param int $errorNumber
     * @param string $timestamp
     * @param string $uniqueID
     */
    public function __construct(?int $errorNumber = null, ?string $timestamp = null, ?string $uniqueID = null)
    {
        $this
            ->setErrorNumber($errorNumber)
            ->setTimestamp($timestamp)
            ->setUniqueID($uniqueID);
    }
    /**
     * Get ErrorNumber value
     * @return int|null
     */
    public function getErrorNumber(): ?int
    {
        return $this->ErrorNumber;
    }
    /**
     * Set ErrorNumber value
     * @param int $errorNumber
     * @return \ID3Global\Models\ID3gException
     */
    public function setErrorNumber(?int $errorNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($errorNumber) && !(is_int($errorNumber) || ctype_digit($errorNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($errorNumber, true), gettype($errorNumber)), __LINE__);
        }
        $this->ErrorNumber = $errorNumber;
        
        return $this;
    }
    /**
     * Get Timestamp value
     * @return string|null
     */
    public function getTimestamp(): ?string
    {
        return $this->Timestamp;
    }
    /**
     * Set Timestamp value
     * @param string $timestamp
     * @return \ID3Global\Models\ID3gException
     */
    public function setTimestamp(?string $timestamp = null): self
    {
        // validation for constraint: string
        if (!is_null($timestamp) && !is_string($timestamp)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($timestamp, true), gettype($timestamp)), __LINE__);
        }
        $this->Timestamp = $timestamp;
        
        return $this;
    }
    /**
     * Get UniqueID value
     * @return string|null
     */
    public function getUniqueID(): ?string
    {
        return $this->UniqueID;
    }
    /**
     * Set UniqueID value
     * @param string $uniqueID
     * @return \ID3Global\Models\ID3gException
     */
    public function setUniqueID(?string $uniqueID = null): self
    {
        // validation for constraint: string
        if (!is_null($uniqueID) && !is_string($uniqueID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($uniqueID, true), gettype($uniqueID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($uniqueID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $uniqueID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($uniqueID, true)), __LINE__);
        }
        $this->UniqueID = $uniqueID;
        
        return $this;
    }
}
