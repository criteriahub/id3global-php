<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalMexico Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q388:GlobalMexico
 * @subpackage Structs
 */
class GlobalMexico extends AbstractStructBase
{
    /**
     * The TaxIdentificationNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalMexicoTaxIdentificationNumber|null
     */
    protected ?\ID3Global\Models\GlobalMexicoTaxIdentificationNumber $TaxIdentificationNumber = null;
    /**
     * Constructor method for GlobalMexico
     * @uses GlobalMexico::setTaxIdentificationNumber()
     * @param \ID3Global\Models\GlobalMexicoTaxIdentificationNumber $taxIdentificationNumber
     */
    public function __construct(?\ID3Global\Models\GlobalMexicoTaxIdentificationNumber $taxIdentificationNumber = null)
    {
        $this
            ->setTaxIdentificationNumber($taxIdentificationNumber);
    }
    /**
     * Get TaxIdentificationNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalMexicoTaxIdentificationNumber|null
     */
    public function getTaxIdentificationNumber(): ?\ID3Global\Models\GlobalMexicoTaxIdentificationNumber
    {
        return isset($this->TaxIdentificationNumber) ? $this->TaxIdentificationNumber : null;
    }
    /**
     * Set TaxIdentificationNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalMexicoTaxIdentificationNumber $taxIdentificationNumber
     * @return \ID3Global\Models\GlobalMexico
     */
    public function setTaxIdentificationNumber(?\ID3Global\Models\GlobalMexicoTaxIdentificationNumber $taxIdentificationNumber = null): self
    {
        if (is_null($taxIdentificationNumber) || (is_array($taxIdentificationNumber) && empty($taxIdentificationNumber))) {
            unset($this->TaxIdentificationNumber);
        } else {
            $this->TaxIdentificationNumber = $taxIdentificationNumber;
        }
        
        return $this;
    }
}
