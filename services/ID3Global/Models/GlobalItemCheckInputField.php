<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalItemCheckInputField Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q288:GlobalItemCheckInputField
 * @subpackage Structs
 */
class GlobalItemCheckInputField extends AbstractStructBase
{
    /**
     * The Category
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Category = null;
    /**
     * The SubCategory
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SubCategory = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Regex
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Regex = null;
    /**
     * The Value
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Value = null;
    /**
     * Constructor method for GlobalItemCheckInputField
     * @uses GlobalItemCheckInputField::setCategory()
     * @uses GlobalItemCheckInputField::setSubCategory()
     * @uses GlobalItemCheckInputField::setName()
     * @uses GlobalItemCheckInputField::setRegex()
     * @uses GlobalItemCheckInputField::setValue()
     * @param string $category
     * @param string $subCategory
     * @param string $name
     * @param string $regex
     * @param string $value
     */
    public function __construct(?string $category = null, ?string $subCategory = null, ?string $name = null, ?string $regex = null, ?string $value = null)
    {
        $this
            ->setCategory($category)
            ->setSubCategory($subCategory)
            ->setName($name)
            ->setRegex($regex)
            ->setValue($value);
    }
    /**
     * Get Category value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCategory(): ?string
    {
        return isset($this->Category) ? $this->Category : null;
    }
    /**
     * Set Category value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $category
     * @return \ID3Global\Models\GlobalItemCheckInputField
     */
    public function setCategory(?string $category = null): self
    {
        // validation for constraint: string
        if (!is_null($category) && !is_string($category)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($category, true), gettype($category)), __LINE__);
        }
        if (is_null($category) || (is_array($category) && empty($category))) {
            unset($this->Category);
        } else {
            $this->Category = $category;
        }
        
        return $this;
    }
    /**
     * Get SubCategory value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSubCategory(): ?string
    {
        return isset($this->SubCategory) ? $this->SubCategory : null;
    }
    /**
     * Set SubCategory value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $subCategory
     * @return \ID3Global\Models\GlobalItemCheckInputField
     */
    public function setSubCategory(?string $subCategory = null): self
    {
        // validation for constraint: string
        if (!is_null($subCategory) && !is_string($subCategory)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subCategory, true), gettype($subCategory)), __LINE__);
        }
        if (is_null($subCategory) || (is_array($subCategory) && empty($subCategory))) {
            unset($this->SubCategory);
        } else {
            $this->SubCategory = $subCategory;
        }
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalItemCheckInputField
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Regex value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRegex(): ?string
    {
        return isset($this->Regex) ? $this->Regex : null;
    }
    /**
     * Set Regex value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $regex
     * @return \ID3Global\Models\GlobalItemCheckInputField
     */
    public function setRegex(?string $regex = null): self
    {
        // validation for constraint: string
        if (!is_null($regex) && !is_string($regex)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($regex, true), gettype($regex)), __LINE__);
        }
        if (is_null($regex) || (is_array($regex) && empty($regex))) {
            unset($this->Regex);
        } else {
            $this->Regex = $regex;
        }
        
        return $this;
    }
    /**
     * Get Value value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getValue(): ?string
    {
        return isset($this->Value) ? $this->Value : null;
    }
    /**
     * Set Value value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $value
     * @return \ID3Global\Models\GlobalItemCheckInputField
     */
    public function setValue(?string $value = null): self
    {
        // validation for constraint: string
        if (!is_null($value) && !is_string($value)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        if (is_null($value) || (is_array($value) && empty($value))) {
            unset($this->Value);
        } else {
            $this->Value = $value;
        }
        
        return $this;
    }
}
