<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UploadAndProcessByProfileProperties Models
 * @subpackage Structs
 */
class UploadAndProcessByProfileProperties extends AbstractStructBase
{
    /**
     * The ProfileID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ProfileID = null;
    /**
     * The Version
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Version = null;
    /**
     * The Revision
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Revision = null;
    /**
     * The DocImage
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DocImage = null;
    /**
     * Constructor method for UploadAndProcessByProfileProperties
     * @uses UploadAndProcessByProfileProperties::setProfileID()
     * @uses UploadAndProcessByProfileProperties::setVersion()
     * @uses UploadAndProcessByProfileProperties::setRevision()
     * @uses UploadAndProcessByProfileProperties::setDocImage()
     * @param string $profileID
     * @param int $version
     * @param int $revision
     * @param string $docImage
     */
    public function __construct(?string $profileID = null, ?int $version = null, ?int $revision = null, ?string $docImage = null)
    {
        $this
            ->setProfileID($profileID)
            ->setVersion($version)
            ->setRevision($revision)
            ->setDocImage($docImage);
    }
    /**
     * Get ProfileID value
     * @return string|null
     */
    public function getProfileID(): ?string
    {
        return $this->ProfileID;
    }
    /**
     * Set ProfileID value
     * @param string $profileID
     * @return \ID3Global\Models\UploadAndProcessByProfileProperties
     */
    public function setProfileID(?string $profileID = null): self
    {
        // validation for constraint: string
        if (!is_null($profileID) && !is_string($profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($profileID, true), gettype($profileID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($profileID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($profileID, true)), __LINE__);
        }
        $this->ProfileID = $profileID;
        
        return $this;
    }
    /**
     * Get Version value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getVersion(): ?int
    {
        return isset($this->Version) ? $this->Version : null;
    }
    /**
     * Set Version value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $version
     * @return \ID3Global\Models\UploadAndProcessByProfileProperties
     */
    public function setVersion(?int $version = null): self
    {
        // validation for constraint: int
        if (!is_null($version) && !(is_int($version) || ctype_digit($version))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($version, true), gettype($version)), __LINE__);
        }
        if (is_null($version) || (is_array($version) && empty($version))) {
            unset($this->Version);
        } else {
            $this->Version = $version;
        }
        
        return $this;
    }
    /**
     * Get Revision value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getRevision(): ?int
    {
        return isset($this->Revision) ? $this->Revision : null;
    }
    /**
     * Set Revision value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $revision
     * @return \ID3Global\Models\UploadAndProcessByProfileProperties
     */
    public function setRevision(?int $revision = null): self
    {
        // validation for constraint: int
        if (!is_null($revision) && !(is_int($revision) || ctype_digit($revision))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($revision, true), gettype($revision)), __LINE__);
        }
        if (is_null($revision) || (is_array($revision) && empty($revision))) {
            unset($this->Revision);
        } else {
            $this->Revision = $revision;
        }
        
        return $this;
    }
    /**
     * Get DocImage value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDocImage(): ?string
    {
        return isset($this->DocImage) ? $this->DocImage : null;
    }
    /**
     * Set DocImage value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $docImage
     * @return \ID3Global\Models\UploadAndProcessByProfileProperties
     */
    public function setDocImage(?string $docImage = null): self
    {
        // validation for constraint: string
        if (!is_null($docImage) && !is_string($docImage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($docImage, true), gettype($docImage)), __LINE__);
        }
        if (is_null($docImage) || (is_array($docImage) && empty($docImage))) {
            unset($this->DocImage);
        } else {
            $this->DocImage = $docImage;
        }
        
        return $this;
    }
}
