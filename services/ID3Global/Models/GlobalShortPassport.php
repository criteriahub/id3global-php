<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalShortPassport Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q375:GlobalShortPassport
 * @subpackage Structs
 */
class GlobalShortPassport extends AbstractStructBase
{
    /**
     * The Number
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Number = null;
    /**
     * The CountryOfOrigin
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CountryOfOrigin = null;
    /**
     * Constructor method for GlobalShortPassport
     * @uses GlobalShortPassport::setNumber()
     * @uses GlobalShortPassport::setCountryOfOrigin()
     * @param string $number
     * @param string $countryOfOrigin
     */
    public function __construct(?string $number = null, ?string $countryOfOrigin = null)
    {
        $this
            ->setNumber($number)
            ->setCountryOfOrigin($countryOfOrigin);
    }
    /**
     * Get Number value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return isset($this->Number) ? $this->Number : null;
    }
    /**
     * Set Number value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $number
     * @return \ID3Global\Models\GlobalShortPassport
     */
    public function setNumber(?string $number = null): self
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($number, true), gettype($number)), __LINE__);
        }
        if (is_null($number) || (is_array($number) && empty($number))) {
            unset($this->Number);
        } else {
            $this->Number = $number;
        }
        
        return $this;
    }
    /**
     * Get CountryOfOrigin value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryOfOrigin(): ?string
    {
        return isset($this->CountryOfOrigin) ? $this->CountryOfOrigin : null;
    }
    /**
     * Set CountryOfOrigin value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryOfOrigin
     * @return \ID3Global\Models\GlobalShortPassport
     */
    public function setCountryOfOrigin(?string $countryOfOrigin = null): self
    {
        // validation for constraint: string
        if (!is_null($countryOfOrigin) && !is_string($countryOfOrigin)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryOfOrigin, true), gettype($countryOfOrigin)), __LINE__);
        }
        if (is_null($countryOfOrigin) || (is_array($countryOfOrigin) && empty($countryOfOrigin))) {
            unset($this->CountryOfOrigin);
        } else {
            $this->CountryOfOrigin = $countryOfOrigin;
        }
        
        return $this;
    }
}
