<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAlternateName Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q323:GlobalAlternateName
 * @subpackage Structs
 */
class GlobalAlternateName extends AbstractStructBase
{
    /**
     * The Title
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Title = null;
    /**
     * The Forename
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Forename = null;
    /**
     * The MiddleName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MiddleName = null;
    /**
     * The Surname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Surname = null;
    /**
     * The Gender
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Gender = null;
    /**
     * The PeriodInUseStartDay
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $PeriodInUseStartDay = null;
    /**
     * The PeriodInUseStartMonth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $PeriodInUseStartMonth = null;
    /**
     * The PeriodInUseStartYear
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $PeriodInUseStartYear = null;
    /**
     * The PeriodInUseEndDay
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $PeriodInUseEndDay = null;
    /**
     * The PeriodInUseEndMonth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $PeriodInUseEndMonth = null;
    /**
     * The PeriodInUseEndYear
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $PeriodInUseEndYear = null;
    /**
     * The DocumentType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DocumentType = null;
    /**
     * Constructor method for GlobalAlternateName
     * @uses GlobalAlternateName::setTitle()
     * @uses GlobalAlternateName::setForename()
     * @uses GlobalAlternateName::setMiddleName()
     * @uses GlobalAlternateName::setSurname()
     * @uses GlobalAlternateName::setGender()
     * @uses GlobalAlternateName::setPeriodInUseStartDay()
     * @uses GlobalAlternateName::setPeriodInUseStartMonth()
     * @uses GlobalAlternateName::setPeriodInUseStartYear()
     * @uses GlobalAlternateName::setPeriodInUseEndDay()
     * @uses GlobalAlternateName::setPeriodInUseEndMonth()
     * @uses GlobalAlternateName::setPeriodInUseEndYear()
     * @uses GlobalAlternateName::setDocumentType()
     * @param string $title
     * @param string $forename
     * @param string $middleName
     * @param string $surname
     * @param string $gender
     * @param int $periodInUseStartDay
     * @param int $periodInUseStartMonth
     * @param int $periodInUseStartYear
     * @param int $periodInUseEndDay
     * @param int $periodInUseEndMonth
     * @param int $periodInUseEndYear
     * @param string $documentType
     */
    public function __construct(?string $title = null, ?string $forename = null, ?string $middleName = null, ?string $surname = null, ?string $gender = null, ?int $periodInUseStartDay = null, ?int $periodInUseStartMonth = null, ?int $periodInUseStartYear = null, ?int $periodInUseEndDay = null, ?int $periodInUseEndMonth = null, ?int $periodInUseEndYear = null, ?string $documentType = null)
    {
        $this
            ->setTitle($title)
            ->setForename($forename)
            ->setMiddleName($middleName)
            ->setSurname($surname)
            ->setGender($gender)
            ->setPeriodInUseStartDay($periodInUseStartDay)
            ->setPeriodInUseStartMonth($periodInUseStartMonth)
            ->setPeriodInUseStartYear($periodInUseStartYear)
            ->setPeriodInUseEndDay($periodInUseEndDay)
            ->setPeriodInUseEndMonth($periodInUseEndMonth)
            ->setPeriodInUseEndYear($periodInUseEndYear)
            ->setDocumentType($documentType);
    }
    /**
     * Get Title value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return isset($this->Title) ? $this->Title : null;
    }
    /**
     * Set Title value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $title
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setTitle(?string $title = null): self
    {
        // validation for constraint: string
        if (!is_null($title) && !is_string($title)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($title, true), gettype($title)), __LINE__);
        }
        if (is_null($title) || (is_array($title) && empty($title))) {
            unset($this->Title);
        } else {
            $this->Title = $title;
        }
        
        return $this;
    }
    /**
     * Get Forename value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getForename(): ?string
    {
        return isset($this->Forename) ? $this->Forename : null;
    }
    /**
     * Set Forename value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $forename
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setForename(?string $forename = null): self
    {
        // validation for constraint: string
        if (!is_null($forename) && !is_string($forename)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($forename, true), gettype($forename)), __LINE__);
        }
        if (is_null($forename) || (is_array($forename) && empty($forename))) {
            unset($this->Forename);
        } else {
            $this->Forename = $forename;
        }
        
        return $this;
    }
    /**
     * Get MiddleName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return isset($this->MiddleName) ? $this->MiddleName : null;
    }
    /**
     * Set MiddleName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $middleName
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setMiddleName(?string $middleName = null): self
    {
        // validation for constraint: string
        if (!is_null($middleName) && !is_string($middleName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($middleName, true), gettype($middleName)), __LINE__);
        }
        if (is_null($middleName) || (is_array($middleName) && empty($middleName))) {
            unset($this->MiddleName);
        } else {
            $this->MiddleName = $middleName;
        }
        
        return $this;
    }
    /**
     * Get Surname value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return isset($this->Surname) ? $this->Surname : null;
    }
    /**
     * Set Surname value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $surname
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setSurname(?string $surname = null): self
    {
        // validation for constraint: string
        if (!is_null($surname) && !is_string($surname)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($surname, true), gettype($surname)), __LINE__);
        }
        if (is_null($surname) || (is_array($surname) && empty($surname))) {
            unset($this->Surname);
        } else {
            $this->Surname = $surname;
        }
        
        return $this;
    }
    /**
     * Get Gender value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGender(): ?string
    {
        return isset($this->Gender) ? $this->Gender : null;
    }
    /**
     * Set Gender value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \ID3Global\Enums\GlobalGender::valueIsValid()
     * @uses \ID3Global\Enums\GlobalGender::getValidValues()
     * @throws InvalidArgumentException
     * @param string $gender
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setGender(?string $gender = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalGender::valueIsValid($gender)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalGender', is_array($gender) ? implode(', ', $gender) : var_export($gender, true), implode(', ', \ID3Global\Enums\GlobalGender::getValidValues())), __LINE__);
        }
        if (is_null($gender) || (is_array($gender) && empty($gender))) {
            unset($this->Gender);
        } else {
            $this->Gender = $gender;
        }
        
        return $this;
    }
    /**
     * Get PeriodInUseStartDay value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPeriodInUseStartDay(): ?int
    {
        return isset($this->PeriodInUseStartDay) ? $this->PeriodInUseStartDay : null;
    }
    /**
     * Set PeriodInUseStartDay value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $periodInUseStartDay
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setPeriodInUseStartDay(?int $periodInUseStartDay = null): self
    {
        // validation for constraint: int
        if (!is_null($periodInUseStartDay) && !(is_int($periodInUseStartDay) || ctype_digit($periodInUseStartDay))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($periodInUseStartDay, true), gettype($periodInUseStartDay)), __LINE__);
        }
        if (is_null($periodInUseStartDay) || (is_array($periodInUseStartDay) && empty($periodInUseStartDay))) {
            unset($this->PeriodInUseStartDay);
        } else {
            $this->PeriodInUseStartDay = $periodInUseStartDay;
        }
        
        return $this;
    }
    /**
     * Get PeriodInUseStartMonth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPeriodInUseStartMonth(): ?int
    {
        return isset($this->PeriodInUseStartMonth) ? $this->PeriodInUseStartMonth : null;
    }
    /**
     * Set PeriodInUseStartMonth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $periodInUseStartMonth
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setPeriodInUseStartMonth(?int $periodInUseStartMonth = null): self
    {
        // validation for constraint: int
        if (!is_null($periodInUseStartMonth) && !(is_int($periodInUseStartMonth) || ctype_digit($periodInUseStartMonth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($periodInUseStartMonth, true), gettype($periodInUseStartMonth)), __LINE__);
        }
        if (is_null($periodInUseStartMonth) || (is_array($periodInUseStartMonth) && empty($periodInUseStartMonth))) {
            unset($this->PeriodInUseStartMonth);
        } else {
            $this->PeriodInUseStartMonth = $periodInUseStartMonth;
        }
        
        return $this;
    }
    /**
     * Get PeriodInUseStartYear value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPeriodInUseStartYear(): ?int
    {
        return isset($this->PeriodInUseStartYear) ? $this->PeriodInUseStartYear : null;
    }
    /**
     * Set PeriodInUseStartYear value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $periodInUseStartYear
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setPeriodInUseStartYear(?int $periodInUseStartYear = null): self
    {
        // validation for constraint: int
        if (!is_null($periodInUseStartYear) && !(is_int($periodInUseStartYear) || ctype_digit($periodInUseStartYear))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($periodInUseStartYear, true), gettype($periodInUseStartYear)), __LINE__);
        }
        if (is_null($periodInUseStartYear) || (is_array($periodInUseStartYear) && empty($periodInUseStartYear))) {
            unset($this->PeriodInUseStartYear);
        } else {
            $this->PeriodInUseStartYear = $periodInUseStartYear;
        }
        
        return $this;
    }
    /**
     * Get PeriodInUseEndDay value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPeriodInUseEndDay(): ?int
    {
        return isset($this->PeriodInUseEndDay) ? $this->PeriodInUseEndDay : null;
    }
    /**
     * Set PeriodInUseEndDay value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $periodInUseEndDay
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setPeriodInUseEndDay(?int $periodInUseEndDay = null): self
    {
        // validation for constraint: int
        if (!is_null($periodInUseEndDay) && !(is_int($periodInUseEndDay) || ctype_digit($periodInUseEndDay))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($periodInUseEndDay, true), gettype($periodInUseEndDay)), __LINE__);
        }
        if (is_null($periodInUseEndDay) || (is_array($periodInUseEndDay) && empty($periodInUseEndDay))) {
            unset($this->PeriodInUseEndDay);
        } else {
            $this->PeriodInUseEndDay = $periodInUseEndDay;
        }
        
        return $this;
    }
    /**
     * Get PeriodInUseEndMonth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPeriodInUseEndMonth(): ?int
    {
        return isset($this->PeriodInUseEndMonth) ? $this->PeriodInUseEndMonth : null;
    }
    /**
     * Set PeriodInUseEndMonth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $periodInUseEndMonth
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setPeriodInUseEndMonth(?int $periodInUseEndMonth = null): self
    {
        // validation for constraint: int
        if (!is_null($periodInUseEndMonth) && !(is_int($periodInUseEndMonth) || ctype_digit($periodInUseEndMonth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($periodInUseEndMonth, true), gettype($periodInUseEndMonth)), __LINE__);
        }
        if (is_null($periodInUseEndMonth) || (is_array($periodInUseEndMonth) && empty($periodInUseEndMonth))) {
            unset($this->PeriodInUseEndMonth);
        } else {
            $this->PeriodInUseEndMonth = $periodInUseEndMonth;
        }
        
        return $this;
    }
    /**
     * Get PeriodInUseEndYear value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPeriodInUseEndYear(): ?int
    {
        return isset($this->PeriodInUseEndYear) ? $this->PeriodInUseEndYear : null;
    }
    /**
     * Set PeriodInUseEndYear value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $periodInUseEndYear
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setPeriodInUseEndYear(?int $periodInUseEndYear = null): self
    {
        // validation for constraint: int
        if (!is_null($periodInUseEndYear) && !(is_int($periodInUseEndYear) || ctype_digit($periodInUseEndYear))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($periodInUseEndYear, true), gettype($periodInUseEndYear)), __LINE__);
        }
        if (is_null($periodInUseEndYear) || (is_array($periodInUseEndYear) && empty($periodInUseEndYear))) {
            unset($this->PeriodInUseEndYear);
        } else {
            $this->PeriodInUseEndYear = $periodInUseEndYear;
        }
        
        return $this;
    }
    /**
     * Get DocumentType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDocumentType(): ?string
    {
        return isset($this->DocumentType) ? $this->DocumentType : null;
    }
    /**
     * Set DocumentType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $documentType
     * @return \ID3Global\Models\GlobalAlternateName
     */
    public function setDocumentType(?string $documentType = null): self
    {
        // validation for constraint: string
        if (!is_null($documentType) && !is_string($documentType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentType, true), gettype($documentType)), __LINE__);
        }
        if (is_null($documentType) || (is_array($documentType) && empty($documentType))) {
            unset($this->DocumentType);
        } else {
            $this->DocumentType = $documentType;
        }
        
        return $this;
    }
}
