<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for StoreDocumentImage Models
 * @subpackage Structs
 */
class StoreDocumentImage extends AbstractStructBase
{
    /**
     * The DocImage
     * Meta information extracted from the WSDL
     * - base: xs:base64Binary
     * @var string|null
     */
    protected ?string $DocImage = null;
    /**
     * Constructor method for StoreDocumentImage
     * @uses StoreDocumentImage::setDocImage()
     * @param string $docImage
     */
    public function __construct(?string $docImage = null)
    {
        $this
            ->setDocImage($docImage);
    }
    /**
     * Get DocImage value
     * @return string|null
     */
    public function getDocImage(): ?string
    {
        return $this->DocImage;
    }
    /**
     * Set DocImage value
     * @param string $docImage
     * @return \ID3Global\Models\StoreDocumentImage
     */
    public function setDocImage(?string $docImage = null): self
    {
        // validation for constraint: string
        if (!is_null($docImage) && !is_string($docImage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($docImage, true), gettype($docImage)), __LINE__);
        }
        $this->DocImage = $docImage;
        
        return $this;
    }
}
