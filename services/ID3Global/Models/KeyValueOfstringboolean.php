<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for KeyValueOfstringboolean Models
 * @subpackage Structs
 */
class KeyValueOfstringboolean extends AbstractStructBase
{
    /**
     * The Key
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string|null
     */
    protected ?string $Key = null;
    /**
     * The Value
     * @var bool|null
     */
    protected ?bool $Value = null;
    /**
     * Constructor method for KeyValueOfstringboolean
     * @uses KeyValueOfstringboolean::setKey()
     * @uses KeyValueOfstringboolean::setValue()
     * @param string $key
     * @param bool $value
     */
    public function __construct(?string $key = null, ?bool $value = null)
    {
        $this
            ->setKey($key)
            ->setValue($value);
    }
    /**
     * Get Key value
     * @return string|null
     */
    public function getKey(): ?string
    {
        return $this->Key;
    }
    /**
     * Set Key value
     * @param string $key
     * @return \ID3Global\Models\KeyValueOfstringboolean
     */
    public function setKey(?string $key = null): self
    {
        // validation for constraint: string
        if (!is_null($key) && !is_string($key)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($key, true), gettype($key)), __LINE__);
        }
        $this->Key = $key;
        
        return $this;
    }
    /**
     * Get Value value
     * @return bool|null
     */
    public function getValue(): ?bool
    {
        return $this->Value;
    }
    /**
     * Set Value value
     * @param bool $value
     * @return \ID3Global\Models\KeyValueOfstringboolean
     */
    public function setValue(?bool $value = null): self
    {
        // validation for constraint: boolean
        if (!is_null($value) && !is_bool($value)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->Value = $value;
        
        return $this;
    }
}
