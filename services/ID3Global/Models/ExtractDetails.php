<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ExtractDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q538:ExtractDetails
 * @subpackage Structs
 */
class ExtractDetails extends AbstractStructBase
{
    /**
     * The LstUserAccountExtracts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfUserAccountExtract|null
     */
    protected ?\ID3Global\Arrays\ArrayOfUserAccountExtract $LstUserAccountExtracts = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The TotalPages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPages = null;
    /**
     * The TotalExtracts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalExtracts = null;
    /**
     * Constructor method for ExtractDetails
     * @uses ExtractDetails::setLstUserAccountExtracts()
     * @uses ExtractDetails::setPageSize()
     * @uses ExtractDetails::setTotalPages()
     * @uses ExtractDetails::setTotalExtracts()
     * @param \ID3Global\Arrays\ArrayOfUserAccountExtract $lstUserAccountExtracts
     * @param int $pageSize
     * @param int $totalPages
     * @param int $totalExtracts
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfUserAccountExtract $lstUserAccountExtracts = null, ?int $pageSize = null, ?int $totalPages = null, ?int $totalExtracts = null)
    {
        $this
            ->setLstUserAccountExtracts($lstUserAccountExtracts)
            ->setPageSize($pageSize)
            ->setTotalPages($totalPages)
            ->setTotalExtracts($totalExtracts);
    }
    /**
     * Get LstUserAccountExtracts value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfUserAccountExtract|null
     */
    public function getLstUserAccountExtracts(): ?\ID3Global\Arrays\ArrayOfUserAccountExtract
    {
        return isset($this->LstUserAccountExtracts) ? $this->LstUserAccountExtracts : null;
    }
    /**
     * Set LstUserAccountExtracts value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfUserAccountExtract $lstUserAccountExtracts
     * @return \ID3Global\Models\ExtractDetails
     */
    public function setLstUserAccountExtracts(?\ID3Global\Arrays\ArrayOfUserAccountExtract $lstUserAccountExtracts = null): self
    {
        if (is_null($lstUserAccountExtracts) || (is_array($lstUserAccountExtracts) && empty($lstUserAccountExtracts))) {
            unset($this->LstUserAccountExtracts);
        } else {
            $this->LstUserAccountExtracts = $lstUserAccountExtracts;
        }
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\ExtractDetails
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get TotalPages value
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->TotalPages;
    }
    /**
     * Set TotalPages value
     * @param int $totalPages
     * @return \ID3Global\Models\ExtractDetails
     */
    public function setTotalPages(?int $totalPages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPages) && !(is_int($totalPages) || ctype_digit($totalPages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPages, true), gettype($totalPages)), __LINE__);
        }
        $this->TotalPages = $totalPages;
        
        return $this;
    }
    /**
     * Get TotalExtracts value
     * @return int|null
     */
    public function getTotalExtracts(): ?int
    {
        return $this->TotalExtracts;
    }
    /**
     * Set TotalExtracts value
     * @param int $totalExtracts
     * @return \ID3Global\Models\ExtractDetails
     */
    public function setTotalExtracts(?int $totalExtracts = null): self
    {
        // validation for constraint: int
        if (!is_null($totalExtracts) && !(is_int($totalExtracts) || ctype_digit($totalExtracts))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalExtracts, true), gettype($totalExtracts)), __LINE__);
        }
        $this->TotalExtracts = $totalExtracts;
        
        return $this;
    }
}
