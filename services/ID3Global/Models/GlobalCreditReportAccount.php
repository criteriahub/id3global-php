<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCreditReportAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q675:GlobalCreditReportAccount
 * @subpackage Structs
 */
class GlobalCreditReportAccount extends GlobalSupplierAccount
{
    /**
     * The Company
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Company = null;
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * The SearchPurposes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $SearchPurposes = null;
    /**
     * The AutoSearch
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $AutoSearch = null;
    /**
     * Constructor method for GlobalCreditReportAccount
     * @uses GlobalCreditReportAccount::setCompany()
     * @uses GlobalCreditReportAccount::setUsername()
     * @uses GlobalCreditReportAccount::setPassword()
     * @uses GlobalCreditReportAccount::setSearchPurposes()
     * @uses GlobalCreditReportAccount::setAutoSearch()
     * @param string $company
     * @param string $username
     * @param string $password
     * @param \ID3Global\Arrays\ArrayOfstring $searchPurposes
     * @param bool $autoSearch
     */
    public function __construct(?string $company = null, ?string $username = null, ?string $password = null, ?\ID3Global\Arrays\ArrayOfstring $searchPurposes = null, ?bool $autoSearch = null)
    {
        $this
            ->setCompany($company)
            ->setUsername($username)
            ->setPassword($password)
            ->setSearchPurposes($searchPurposes)
            ->setAutoSearch($autoSearch);
    }
    /**
     * Get Company value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCompany(): ?string
    {
        return isset($this->Company) ? $this->Company : null;
    }
    /**
     * Set Company value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $company
     * @return \ID3Global\Models\GlobalCreditReportAccount
     */
    public function setCompany(?string $company = null): self
    {
        // validation for constraint: string
        if (!is_null($company) && !is_string($company)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($company, true), gettype($company)), __LINE__);
        }
        if (is_null($company) || (is_array($company) && empty($company))) {
            unset($this->Company);
        } else {
            $this->Company = $company;
        }
        
        return $this;
    }
    /**
     * Get Username value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return isset($this->Username) ? $this->Username : null;
    }
    /**
     * Set Username value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username
     * @return \ID3Global\Models\GlobalCreditReportAccount
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        if (is_null($username) || (is_array($username) && empty($username))) {
            unset($this->Username);
        } else {
            $this->Username = $username;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\GlobalCreditReportAccount
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
    /**
     * Get SearchPurposes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getSearchPurposes(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->SearchPurposes) ? $this->SearchPurposes : null;
    }
    /**
     * Set SearchPurposes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $searchPurposes
     * @return \ID3Global\Models\GlobalCreditReportAccount
     */
    public function setSearchPurposes(?\ID3Global\Arrays\ArrayOfstring $searchPurposes = null): self
    {
        if (is_null($searchPurposes) || (is_array($searchPurposes) && empty($searchPurposes))) {
            unset($this->SearchPurposes);
        } else {
            $this->SearchPurposes = $searchPurposes;
        }
        
        return $this;
    }
    /**
     * Get AutoSearch value
     * @return bool|null
     */
    public function getAutoSearch(): ?bool
    {
        return $this->AutoSearch;
    }
    /**
     * Set AutoSearch value
     * @param bool $autoSearch
     * @return \ID3Global\Models\GlobalCreditReportAccount
     */
    public function setAutoSearch(?bool $autoSearch = null): self
    {
        // validation for constraint: boolean
        if (!is_null($autoSearch) && !is_bool($autoSearch)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($autoSearch, true), gettype($autoSearch)), __LINE__);
        }
        $this->AutoSearch = $autoSearch;
        
        return $this;
    }
}
