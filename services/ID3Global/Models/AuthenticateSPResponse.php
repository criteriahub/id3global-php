<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AuthenticateSPResponse Models
 * @subpackage Structs
 */
class AuthenticateSPResponse extends AbstractStructBase
{
    /**
     * The AuthenticateSPResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalResultData|null
     */
    protected ?\ID3Global\Models\GlobalResultData $AuthenticateSPResult = null;
    /**
     * Constructor method for AuthenticateSPResponse
     * @uses AuthenticateSPResponse::setAuthenticateSPResult()
     * @param \ID3Global\Models\GlobalResultData $authenticateSPResult
     */
    public function __construct(?\ID3Global\Models\GlobalResultData $authenticateSPResult = null)
    {
        $this
            ->setAuthenticateSPResult($authenticateSPResult);
    }
    /**
     * Get AuthenticateSPResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalResultData|null
     */
    public function getAuthenticateSPResult(): ?\ID3Global\Models\GlobalResultData
    {
        return isset($this->AuthenticateSPResult) ? $this->AuthenticateSPResult : null;
    }
    /**
     * Set AuthenticateSPResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalResultData $authenticateSPResult
     * @return \ID3Global\Models\AuthenticateSPResponse
     */
    public function setAuthenticateSPResult(?\ID3Global\Models\GlobalResultData $authenticateSPResult = null): self
    {
        if (is_null($authenticateSPResult) || (is_array($authenticateSPResult) && empty($authenticateSPResult))) {
            unset($this->AuthenticateSPResult);
        } else {
            $this->AuthenticateSPResult = $authenticateSPResult;
        }
        
        return $this;
    }
}
