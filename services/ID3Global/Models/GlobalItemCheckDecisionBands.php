<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalItemCheckDecisionBands Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q442:GlobalItemCheckDecisionBands
 * @subpackage Structs
 */
class GlobalItemCheckDecisionBands extends AbstractStructBase
{
    /**
     * The ID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ID = null;
    /**
     * The BandScore
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $BandScore = null;
    /**
     * The BandText
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $BandText = null;
    /**
     * Constructor method for GlobalItemCheckDecisionBands
     * @uses GlobalItemCheckDecisionBands::setID()
     * @uses GlobalItemCheckDecisionBands::setBandScore()
     * @uses GlobalItemCheckDecisionBands::setBandText()
     * @param int $iD
     * @param int $bandScore
     * @param string $bandText
     */
    public function __construct(?int $iD = null, ?int $bandScore = null, ?string $bandText = null)
    {
        $this
            ->setID($iD)
            ->setBandScore($bandScore)
            ->setBandText($bandText);
    }
    /**
     * Get ID value
     * @return int|null
     */
    public function getID(): ?int
    {
        return $this->ID;
    }
    /**
     * Set ID value
     * @param int $iD
     * @return \ID3Global\Models\GlobalItemCheckDecisionBands
     */
    public function setID(?int $iD = null): self
    {
        // validation for constraint: int
        if (!is_null($iD) && !(is_int($iD) || ctype_digit($iD))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($iD, true), gettype($iD)), __LINE__);
        }
        $this->ID = $iD;
        
        return $this;
    }
    /**
     * Get BandScore value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getBandScore(): ?int
    {
        return isset($this->BandScore) ? $this->BandScore : null;
    }
    /**
     * Set BandScore value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $bandScore
     * @return \ID3Global\Models\GlobalItemCheckDecisionBands
     */
    public function setBandScore(?int $bandScore = null): self
    {
        // validation for constraint: int
        if (!is_null($bandScore) && !(is_int($bandScore) || ctype_digit($bandScore))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($bandScore, true), gettype($bandScore)), __LINE__);
        }
        if (is_null($bandScore) || (is_array($bandScore) && empty($bandScore))) {
            unset($this->BandScore);
        } else {
            $this->BandScore = $bandScore;
        }
        
        return $this;
    }
    /**
     * Get BandText value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBandText(): ?string
    {
        return isset($this->BandText) ? $this->BandText : null;
    }
    /**
     * Set BandText value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $bandText
     * @return \ID3Global\Models\GlobalItemCheckDecisionBands
     */
    public function setBandText(?string $bandText = null): self
    {
        // validation for constraint: string
        if (!is_null($bandText) && !is_string($bandText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bandText, true), gettype($bandText)), __LINE__);
        }
        if (is_null($bandText) || (is_array($bandText) && empty($bandText))) {
            unset($this->BandText);
        } else {
            $this->BandText = $bandText;
        }
        
        return $this;
    }
}
