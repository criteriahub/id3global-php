<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProcessStoredImage Models
 * @subpackage Structs
 */
class ProcessStoredImage extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The ImageID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ImageID = null;
    /**
     * The Synchronous
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Synchronous = null;
    /**
     * Constructor method for ProcessStoredImage
     * @uses ProcessStoredImage::setOrgID()
     * @uses ProcessStoredImage::setImageID()
     * @uses ProcessStoredImage::setSynchronous()
     * @param string $orgID
     * @param string $imageID
     * @param bool $synchronous
     */
    public function __construct(?string $orgID = null, ?string $imageID = null, ?bool $synchronous = null)
    {
        $this
            ->setOrgID($orgID)
            ->setImageID($imageID)
            ->setSynchronous($synchronous);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\ProcessStoredImage
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get ImageID value
     * @return string|null
     */
    public function getImageID(): ?string
    {
        return $this->ImageID;
    }
    /**
     * Set ImageID value
     * @param string $imageID
     * @return \ID3Global\Models\ProcessStoredImage
     */
    public function setImageID(?string $imageID = null): self
    {
        // validation for constraint: string
        if (!is_null($imageID) && !is_string($imageID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($imageID, true), gettype($imageID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($imageID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $imageID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($imageID, true)), __LINE__);
        }
        $this->ImageID = $imageID;
        
        return $this;
    }
    /**
     * Get Synchronous value
     * @return bool|null
     */
    public function getSynchronous(): ?bool
    {
        return $this->Synchronous;
    }
    /**
     * Set Synchronous value
     * @param bool $synchronous
     * @return \ID3Global\Models\ProcessStoredImage
     */
    public function setSynchronous(?bool $synchronous = null): self
    {
        // validation for constraint: boolean
        if (!is_null($synchronous) && !is_bool($synchronous)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($synchronous, true), gettype($synchronous)), __LINE__);
        }
        $this->Synchronous = $synchronous;
        
        return $this;
    }
}
