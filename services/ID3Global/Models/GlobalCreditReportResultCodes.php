<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCreditReportResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q53:GlobalCreditReportResultCodes
 * @subpackage Structs
 */
class GlobalCreditReportResultCodes extends GlobalItemCheckResultCodes
{
    /**
     * The CreditReportID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $CreditReportID = null;
    /**
     * The SearchPurposeCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SearchPurposeCode = null;
    /**
     * The SearchPurposeName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SearchPurposeName = null;
    /**
     * The Score
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Score = null;
    /**
     * The CreditReportAccess
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $CreditReportAccess = null;
    /**
     * Constructor method for GlobalCreditReportResultCodes
     * @uses GlobalCreditReportResultCodes::setCreditReportID()
     * @uses GlobalCreditReportResultCodes::setSearchPurposeCode()
     * @uses GlobalCreditReportResultCodes::setSearchPurposeName()
     * @uses GlobalCreditReportResultCodes::setScore()
     * @uses GlobalCreditReportResultCodes::setCreditReportAccess()
     * @param string $creditReportID
     * @param string $searchPurposeCode
     * @param string $searchPurposeName
     * @param int $score
     * @param bool $creditReportAccess
     */
    public function __construct(?string $creditReportID = null, ?string $searchPurposeCode = null, ?string $searchPurposeName = null, ?int $score = null, ?bool $creditReportAccess = null)
    {
        $this
            ->setCreditReportID($creditReportID)
            ->setSearchPurposeCode($searchPurposeCode)
            ->setSearchPurposeName($searchPurposeName)
            ->setScore($score)
            ->setCreditReportAccess($creditReportAccess);
    }
    /**
     * Get CreditReportID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCreditReportID(): ?string
    {
        return isset($this->CreditReportID) ? $this->CreditReportID : null;
    }
    /**
     * Set CreditReportID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $creditReportID
     * @return \ID3Global\Models\GlobalCreditReportResultCodes
     */
    public function setCreditReportID(?string $creditReportID = null): self
    {
        // validation for constraint: string
        if (!is_null($creditReportID) && !is_string($creditReportID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($creditReportID, true), gettype($creditReportID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($creditReportID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $creditReportID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($creditReportID, true)), __LINE__);
        }
        if (is_null($creditReportID) || (is_array($creditReportID) && empty($creditReportID))) {
            unset($this->CreditReportID);
        } else {
            $this->CreditReportID = $creditReportID;
        }
        
        return $this;
    }
    /**
     * Get SearchPurposeCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSearchPurposeCode(): ?string
    {
        return isset($this->SearchPurposeCode) ? $this->SearchPurposeCode : null;
    }
    /**
     * Set SearchPurposeCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $searchPurposeCode
     * @return \ID3Global\Models\GlobalCreditReportResultCodes
     */
    public function setSearchPurposeCode(?string $searchPurposeCode = null): self
    {
        // validation for constraint: string
        if (!is_null($searchPurposeCode) && !is_string($searchPurposeCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchPurposeCode, true), gettype($searchPurposeCode)), __LINE__);
        }
        if (is_null($searchPurposeCode) || (is_array($searchPurposeCode) && empty($searchPurposeCode))) {
            unset($this->SearchPurposeCode);
        } else {
            $this->SearchPurposeCode = $searchPurposeCode;
        }
        
        return $this;
    }
    /**
     * Get SearchPurposeName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSearchPurposeName(): ?string
    {
        return isset($this->SearchPurposeName) ? $this->SearchPurposeName : null;
    }
    /**
     * Set SearchPurposeName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $searchPurposeName
     * @return \ID3Global\Models\GlobalCreditReportResultCodes
     */
    public function setSearchPurposeName(?string $searchPurposeName = null): self
    {
        // validation for constraint: string
        if (!is_null($searchPurposeName) && !is_string($searchPurposeName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchPurposeName, true), gettype($searchPurposeName)), __LINE__);
        }
        if (is_null($searchPurposeName) || (is_array($searchPurposeName) && empty($searchPurposeName))) {
            unset($this->SearchPurposeName);
        } else {
            $this->SearchPurposeName = $searchPurposeName;
        }
        
        return $this;
    }
    /**
     * Get Score value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getScore(): ?int
    {
        return isset($this->Score) ? $this->Score : null;
    }
    /**
     * Set Score value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $score
     * @return \ID3Global\Models\GlobalCreditReportResultCodes
     */
    public function setScore(?int $score = null): self
    {
        // validation for constraint: int
        if (!is_null($score) && !(is_int($score) || ctype_digit($score))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($score, true), gettype($score)), __LINE__);
        }
        if (is_null($score) || (is_array($score) && empty($score))) {
            unset($this->Score);
        } else {
            $this->Score = $score;
        }
        
        return $this;
    }
    /**
     * Get CreditReportAccess value
     * @return bool|null
     */
    public function getCreditReportAccess(): ?bool
    {
        return $this->CreditReportAccess;
    }
    /**
     * Set CreditReportAccess value
     * @param bool $creditReportAccess
     * @return \ID3Global\Models\GlobalCreditReportResultCodes
     */
    public function setCreditReportAccess(?bool $creditReportAccess = null): self
    {
        // validation for constraint: boolean
        if (!is_null($creditReportAccess) && !is_bool($creditReportAccess)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($creditReportAccess, true), gettype($creditReportAccess)), __LINE__);
        }
        $this->CreditReportAccess = $creditReportAccess;
        
        return $this;
    }
}
