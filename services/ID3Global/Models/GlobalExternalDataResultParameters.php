<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalExternalDataResultParameters Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q878:GlobalExternalDataResultParameters
 * @subpackage Structs
 */
class GlobalExternalDataResultParameters extends GlobalCaseResultParameters
{
    /**
     * The OverallMatchItemKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OverallMatchItemKey = null;
    /**
     * The Parameters
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalExternalDataResultParameter|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalExternalDataResultParameter $Parameters = null;
    /**
     * The ExternalDataIds
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint $ExternalDataIds = null;
    /**
     * Constructor method for GlobalExternalDataResultParameters
     * @uses GlobalExternalDataResultParameters::setOverallMatchItemKey()
     * @uses GlobalExternalDataResultParameters::setParameters()
     * @uses GlobalExternalDataResultParameters::setExternalDataIds()
     * @param string $overallMatchItemKey
     * @param \ID3Global\Arrays\ArrayOfGlobalExternalDataResultParameter $parameters
     * @param \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint $externalDataIds
     */
    public function __construct(?string $overallMatchItemKey = null, ?\ID3Global\Arrays\ArrayOfGlobalExternalDataResultParameter $parameters = null, ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint $externalDataIds = null)
    {
        $this
            ->setOverallMatchItemKey($overallMatchItemKey)
            ->setParameters($parameters)
            ->setExternalDataIds($externalDataIds);
    }
    /**
     * Get OverallMatchItemKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOverallMatchItemKey(): ?string
    {
        return isset($this->OverallMatchItemKey) ? $this->OverallMatchItemKey : null;
    }
    /**
     * Set OverallMatchItemKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $overallMatchItemKey
     * @return \ID3Global\Models\GlobalExternalDataResultParameters
     */
    public function setOverallMatchItemKey(?string $overallMatchItemKey = null): self
    {
        // validation for constraint: string
        if (!is_null($overallMatchItemKey) && !is_string($overallMatchItemKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($overallMatchItemKey, true), gettype($overallMatchItemKey)), __LINE__);
        }
        if (is_null($overallMatchItemKey) || (is_array($overallMatchItemKey) && empty($overallMatchItemKey))) {
            unset($this->OverallMatchItemKey);
        } else {
            $this->OverallMatchItemKey = $overallMatchItemKey;
        }
        
        return $this;
    }
    /**
     * Get Parameters value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalExternalDataResultParameter|null
     */
    public function getParameters(): ?\ID3Global\Arrays\ArrayOfGlobalExternalDataResultParameter
    {
        return isset($this->Parameters) ? $this->Parameters : null;
    }
    /**
     * Set Parameters value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalExternalDataResultParameter $parameters
     * @return \ID3Global\Models\GlobalExternalDataResultParameters
     */
    public function setParameters(?\ID3Global\Arrays\ArrayOfGlobalExternalDataResultParameter $parameters = null): self
    {
        if (is_null($parameters) || (is_array($parameters) && empty($parameters))) {
            unset($this->Parameters);
        } else {
            $this->Parameters = $parameters;
        }
        
        return $this;
    }
    /**
     * Get ExternalDataIds value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint|null
     */
    public function getExternalDataIds(): ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint
    {
        return isset($this->ExternalDataIds) ? $this->ExternalDataIds : null;
    }
    /**
     * Set ExternalDataIds value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint $externalDataIds
     * @return \ID3Global\Models\GlobalExternalDataResultParameters
     */
    public function setExternalDataIds(?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint $externalDataIds = null): self
    {
        if (is_null($externalDataIds) || (is_array($externalDataIds) && empty($externalDataIds))) {
            unset($this->ExternalDataIds);
        } else {
            $this->ExternalDataIds = $externalDataIds;
        }
        
        return $this;
    }
}
