<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalUKAddressDocuments Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q404:GlobalUKAddressDocuments
 * @subpackage Structs
 */
class GlobalUKAddressDocuments extends AbstractStructBase
{
    /**
     * The ElectricitySupplier
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalElectricitySupplier|null
     */
    protected ?\ID3Global\Models\GlobalElectricitySupplier $ElectricitySupplier = null;
    /**
     * Constructor method for GlobalUKAddressDocuments
     * @uses GlobalUKAddressDocuments::setElectricitySupplier()
     * @param \ID3Global\Models\GlobalElectricitySupplier $electricitySupplier
     */
    public function __construct(?\ID3Global\Models\GlobalElectricitySupplier $electricitySupplier = null)
    {
        $this
            ->setElectricitySupplier($electricitySupplier);
    }
    /**
     * Get ElectricitySupplier value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalElectricitySupplier|null
     */
    public function getElectricitySupplier(): ?\ID3Global\Models\GlobalElectricitySupplier
    {
        return isset($this->ElectricitySupplier) ? $this->ElectricitySupplier : null;
    }
    /**
     * Set ElectricitySupplier value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalElectricitySupplier $electricitySupplier
     * @return \ID3Global\Models\GlobalUKAddressDocuments
     */
    public function setElectricitySupplier(?\ID3Global\Models\GlobalElectricitySupplier $electricitySupplier = null): self
    {
        if (is_null($electricitySupplier) || (is_array($electricitySupplier) && empty($electricitySupplier))) {
            unset($this->ElectricitySupplier);
        } else {
            $this->ElectricitySupplier = $electricitySupplier;
        }
        
        return $this;
    }
}
