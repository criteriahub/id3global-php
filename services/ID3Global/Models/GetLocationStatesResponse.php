<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetLocationStatesResponse Models
 * @subpackage Structs
 */
class GetLocationStatesResponse extends AbstractStructBase
{
    /**
     * The GetLocationStatesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalProvince|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalProvince $GetLocationStatesResult = null;
    /**
     * Constructor method for GetLocationStatesResponse
     * @uses GetLocationStatesResponse::setGetLocationStatesResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalProvince $getLocationStatesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalProvince $getLocationStatesResult = null)
    {
        $this
            ->setGetLocationStatesResult($getLocationStatesResult);
    }
    /**
     * Get GetLocationStatesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalProvince|null
     */
    public function getGetLocationStatesResult(): ?\ID3Global\Arrays\ArrayOfGlobalProvince
    {
        return isset($this->GetLocationStatesResult) ? $this->GetLocationStatesResult : null;
    }
    /**
     * Set GetLocationStatesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalProvince $getLocationStatesResult
     * @return \ID3Global\Models\GetLocationStatesResponse
     */
    public function setGetLocationStatesResult(?\ID3Global\Arrays\ArrayOfGlobalProvince $getLocationStatesResult = null): self
    {
        if (is_null($getLocationStatesResult) || (is_array($getLocationStatesResult) && empty($getLocationStatesResult))) {
            unset($this->GetLocationStatesResult);
        } else {
            $this->GetLocationStatesResult = $getLocationStatesResult;
        }
        
        return $this;
    }
}
