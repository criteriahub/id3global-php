<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalTelephoneResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q41:GlobalTelephoneResultCodes
 * @subpackage Structs
 */
class GlobalTelephoneResultCodes extends GlobalItemCheckResultCodes
{
    /**
     * The PublishedTelephoneNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $PublishedTelephoneNumber = null;
    /**
     * Constructor method for GlobalTelephoneResultCodes
     * @uses GlobalTelephoneResultCodes::setPublishedTelephoneNumber()
     * @param string $publishedTelephoneNumber
     */
    public function __construct(?string $publishedTelephoneNumber = null)
    {
        $this
            ->setPublishedTelephoneNumber($publishedTelephoneNumber);
    }
    /**
     * Get PublishedTelephoneNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPublishedTelephoneNumber(): ?string
    {
        return isset($this->PublishedTelephoneNumber) ? $this->PublishedTelephoneNumber : null;
    }
    /**
     * Set PublishedTelephoneNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $publishedTelephoneNumber
     * @return \ID3Global\Models\GlobalTelephoneResultCodes
     */
    public function setPublishedTelephoneNumber(?string $publishedTelephoneNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($publishedTelephoneNumber) && !is_string($publishedTelephoneNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($publishedTelephoneNumber, true), gettype($publishedTelephoneNumber)), __LINE__);
        }
        if (is_null($publishedTelephoneNumber) || (is_array($publishedTelephoneNumber) && empty($publishedTelephoneNumber))) {
            unset($this->PublishedTelephoneNumber);
        } else {
            $this->PublishedTelephoneNumber = $publishedTelephoneNumber;
        }
        
        return $this;
    }
}
