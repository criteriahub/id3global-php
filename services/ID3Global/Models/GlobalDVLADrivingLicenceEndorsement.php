<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDVLADrivingLicenceEndorsement Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q104:GlobalDVLADrivingLicenceEndorsement
 * @subpackage Structs
 */
class GlobalDVLADrivingLicenceEndorsement extends AbstractStructBase
{
    /**
     * The EndorsementType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $EndorsementType = null;
    /**
     * The Code
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Code = null;
    /**
     * The Description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Description = null;
    /**
     * The Duration
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Duration = null;
    /**
     * The IsDurationFromConviction
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsDurationFromConviction = null;
    /**
     * The Points
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Points = null;
    /**
     * Constructor method for GlobalDVLADrivingLicenceEndorsement
     * @uses GlobalDVLADrivingLicenceEndorsement::setEndorsementType()
     * @uses GlobalDVLADrivingLicenceEndorsement::setCode()
     * @uses GlobalDVLADrivingLicenceEndorsement::setDescription()
     * @uses GlobalDVLADrivingLicenceEndorsement::setDuration()
     * @uses GlobalDVLADrivingLicenceEndorsement::setIsDurationFromConviction()
     * @uses GlobalDVLADrivingLicenceEndorsement::setPoints()
     * @param string $endorsementType
     * @param string $code
     * @param string $description
     * @param int $duration
     * @param bool $isDurationFromConviction
     * @param string $points
     */
    public function __construct(?string $endorsementType = null, ?string $code = null, ?string $description = null, ?int $duration = null, ?bool $isDurationFromConviction = null, ?string $points = null)
    {
        $this
            ->setEndorsementType($endorsementType)
            ->setCode($code)
            ->setDescription($description)
            ->setDuration($duration)
            ->setIsDurationFromConviction($isDurationFromConviction)
            ->setPoints($points);
    }
    /**
     * Get EndorsementType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEndorsementType(): ?string
    {
        return isset($this->EndorsementType) ? $this->EndorsementType : null;
    }
    /**
     * Set EndorsementType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $endorsementType
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement
     */
    public function setEndorsementType(?string $endorsementType = null): self
    {
        // validation for constraint: string
        if (!is_null($endorsementType) && !is_string($endorsementType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endorsementType, true), gettype($endorsementType)), __LINE__);
        }
        if (is_null($endorsementType) || (is_array($endorsementType) && empty($endorsementType))) {
            unset($this->EndorsementType);
        } else {
            $this->EndorsementType = $endorsementType;
        }
        
        return $this;
    }
    /**
     * Get Code value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCode(): ?string
    {
        return isset($this->Code) ? $this->Code : null;
    }
    /**
     * Set Code value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $code
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement
     */
    public function setCode(?string $code = null): self
    {
        // validation for constraint: string
        if (!is_null($code) && !is_string($code)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        if (is_null($code) || (is_array($code) && empty($code))) {
            unset($this->Code);
        } else {
            $this->Code = $code;
        }
        
        return $this;
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        
        return $this;
    }
    /**
     * Get Duration value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return isset($this->Duration) ? $this->Duration : null;
    }
    /**
     * Set Duration value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $duration
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement
     */
    public function setDuration(?int $duration = null): self
    {
        // validation for constraint: int
        if (!is_null($duration) && !(is_int($duration) || ctype_digit($duration))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($duration, true), gettype($duration)), __LINE__);
        }
        if (is_null($duration) || (is_array($duration) && empty($duration))) {
            unset($this->Duration);
        } else {
            $this->Duration = $duration;
        }
        
        return $this;
    }
    /**
     * Get IsDurationFromConviction value
     * @return bool|null
     */
    public function getIsDurationFromConviction(): ?bool
    {
        return $this->IsDurationFromConviction;
    }
    /**
     * Set IsDurationFromConviction value
     * @param bool $isDurationFromConviction
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement
     */
    public function setIsDurationFromConviction(?bool $isDurationFromConviction = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isDurationFromConviction) && !is_bool($isDurationFromConviction)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isDurationFromConviction, true), gettype($isDurationFromConviction)), __LINE__);
        }
        $this->IsDurationFromConviction = $isDurationFromConviction;
        
        return $this;
    }
    /**
     * Get Points value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPoints(): ?string
    {
        return isset($this->Points) ? $this->Points : null;
    }
    /**
     * Set Points value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $points
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceEndorsement
     */
    public function setPoints(?string $points = null): self
    {
        // validation for constraint: string
        if (!is_null($points) && !is_string($points)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($points, true), gettype($points)), __LINE__);
        }
        if (is_null($points) || (is_array($points) && empty($points))) {
            unset($this->Points);
        } else {
            $this->Points = $points;
        }
        
        return $this;
    }
}
