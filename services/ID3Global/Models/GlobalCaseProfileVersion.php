<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseProfileVersion Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q731:GlobalCaseProfileVersion
 * @subpackage Structs
 */
class GlobalCaseProfileVersion extends AbstractStructBase
{
    /**
     * The ProfileVersion
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileVersion|null
     */
    protected ?\ID3Global\Models\GlobalProfileVersion $ProfileVersion = null;
    /**
     * The ItemIDs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    protected ?\ID3Global\Arrays\ArrayOfunsignedInt $ItemIDs = null;
    /**
     * Constructor method for GlobalCaseProfileVersion
     * @uses GlobalCaseProfileVersion::setProfileVersion()
     * @uses GlobalCaseProfileVersion::setItemIDs()
     * @param \ID3Global\Models\GlobalProfileVersion $profileVersion
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $itemIDs
     */
    public function __construct(?\ID3Global\Models\GlobalProfileVersion $profileVersion = null, ?\ID3Global\Arrays\ArrayOfunsignedInt $itemIDs = null)
    {
        $this
            ->setProfileVersion($profileVersion)
            ->setItemIDs($itemIDs);
    }
    /**
     * Get ProfileVersion value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileVersion|null
     */
    public function getProfileVersion(): ?\ID3Global\Models\GlobalProfileVersion
    {
        return isset($this->ProfileVersion) ? $this->ProfileVersion : null;
    }
    /**
     * Set ProfileVersion value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileVersion $profileVersion
     * @return \ID3Global\Models\GlobalCaseProfileVersion
     */
    public function setProfileVersion(?\ID3Global\Models\GlobalProfileVersion $profileVersion = null): self
    {
        if (is_null($profileVersion) || (is_array($profileVersion) && empty($profileVersion))) {
            unset($this->ProfileVersion);
        } else {
            $this->ProfileVersion = $profileVersion;
        }
        
        return $this;
    }
    /**
     * Get ItemIDs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    public function getItemIDs(): ?\ID3Global\Arrays\ArrayOfunsignedInt
    {
        return isset($this->ItemIDs) ? $this->ItemIDs : null;
    }
    /**
     * Set ItemIDs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $itemIDs
     * @return \ID3Global\Models\GlobalCaseProfileVersion
     */
    public function setItemIDs(?\ID3Global\Arrays\ArrayOfunsignedInt $itemIDs = null): self
    {
        if (is_null($itemIDs) || (is_array($itemIDs) && empty($itemIDs))) {
            unset($this->ItemIDs);
        } else {
            $this->ItemIDs = $itemIDs;
        }
        
        return $this;
    }
}
