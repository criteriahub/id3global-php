<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseChargingPoint Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q765:GlobalCaseChargingPoint
 * @subpackage Structs
 */
class GlobalCaseChargingPoint extends AbstractStructBase
{
    /**
     * The AuthenticationID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AuthenticationID = null;
    /**
     * The ProfileID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ProfileID = null;
    /**
     * The ChargingDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ChargingDate = null;
    /**
     * Constructor method for GlobalCaseChargingPoint
     * @uses GlobalCaseChargingPoint::setAuthenticationID()
     * @uses GlobalCaseChargingPoint::setProfileID()
     * @uses GlobalCaseChargingPoint::setChargingDate()
     * @param string $authenticationID
     * @param string $profileID
     * @param string $chargingDate
     */
    public function __construct(?string $authenticationID = null, ?string $profileID = null, ?string $chargingDate = null)
    {
        $this
            ->setAuthenticationID($authenticationID)
            ->setProfileID($profileID)
            ->setChargingDate($chargingDate);
    }
    /**
     * Get AuthenticationID value
     * @return string|null
     */
    public function getAuthenticationID(): ?string
    {
        return $this->AuthenticationID;
    }
    /**
     * Set AuthenticationID value
     * @param string $authenticationID
     * @return \ID3Global\Models\GlobalCaseChargingPoint
     */
    public function setAuthenticationID(?string $authenticationID = null): self
    {
        // validation for constraint: string
        if (!is_null($authenticationID) && !is_string($authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authenticationID, true), gettype($authenticationID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($authenticationID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($authenticationID, true)), __LINE__);
        }
        $this->AuthenticationID = $authenticationID;
        
        return $this;
    }
    /**
     * Get ProfileID value
     * @return string|null
     */
    public function getProfileID(): ?string
    {
        return $this->ProfileID;
    }
    /**
     * Set ProfileID value
     * @param string $profileID
     * @return \ID3Global\Models\GlobalCaseChargingPoint
     */
    public function setProfileID(?string $profileID = null): self
    {
        // validation for constraint: string
        if (!is_null($profileID) && !is_string($profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($profileID, true), gettype($profileID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($profileID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($profileID, true)), __LINE__);
        }
        $this->ProfileID = $profileID;
        
        return $this;
    }
    /**
     * Get ChargingDate value
     * @return string|null
     */
    public function getChargingDate(): ?string
    {
        return $this->ChargingDate;
    }
    /**
     * Set ChargingDate value
     * @param string $chargingDate
     * @return \ID3Global\Models\GlobalCaseChargingPoint
     */
    public function setChargingDate(?string $chargingDate = null): self
    {
        // validation for constraint: string
        if (!is_null($chargingDate) && !is_string($chargingDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($chargingDate, true), gettype($chargingDate)), __LINE__);
        }
        $this->ChargingDate = $chargingDate;
        
        return $this;
    }
}
