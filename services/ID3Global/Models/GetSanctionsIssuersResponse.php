<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSanctionsIssuersResponse Models
 * @subpackage Structs
 */
class GetSanctionsIssuersResponse extends AbstractStructBase
{
    /**
     * The GetSanctionsIssuersResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer $GetSanctionsIssuersResult = null;
    /**
     * Constructor method for GetSanctionsIssuersResponse
     * @uses GetSanctionsIssuersResponse::setGetSanctionsIssuersResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer $getSanctionsIssuersResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer $getSanctionsIssuersResult = null)
    {
        $this
            ->setGetSanctionsIssuersResult($getSanctionsIssuersResult);
    }
    /**
     * Get GetSanctionsIssuersResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer|null
     */
    public function getGetSanctionsIssuersResult(): ?\ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer
    {
        return isset($this->GetSanctionsIssuersResult) ? $this->GetSanctionsIssuersResult : null;
    }
    /**
     * Set GetSanctionsIssuersResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer $getSanctionsIssuersResult
     * @return \ID3Global\Models\GetSanctionsIssuersResponse
     */
    public function setGetSanctionsIssuersResult(?\ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer $getSanctionsIssuersResult = null): self
    {
        if (is_null($getSanctionsIssuersResult) || (is_array($getSanctionsIssuersResult) && empty($getSanctionsIssuersResult))) {
            unset($this->GetSanctionsIssuersResult);
        } else {
            $this->GetSanctionsIssuersResult = $getSanctionsIssuersResult;
        }
        
        return $this;
    }
}
