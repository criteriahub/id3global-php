<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAccountPermissionResponse Models
 * @subpackage Structs
 */
class GetAccountPermissionResponse extends AbstractStructBase
{
    /**
     * The GetAccountPermissionResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $GetAccountPermissionResult = null;
    /**
     * Constructor method for GetAccountPermissionResponse
     * @uses GetAccountPermissionResponse::setGetAccountPermissionResult()
     * @param string $getAccountPermissionResult
     */
    public function __construct(?string $getAccountPermissionResult = null)
    {
        $this
            ->setGetAccountPermissionResult($getAccountPermissionResult);
    }
    /**
     * Get GetAccountPermissionResult value
     * @return string|null
     */
    public function getGetAccountPermissionResult(): ?string
    {
        return $this->GetAccountPermissionResult;
    }
    /**
     * Set GetAccountPermissionResult value
     * @uses \ID3Global\Enums\GlobalPermission::valueIsValid()
     * @uses \ID3Global\Enums\GlobalPermission::getValidValues()
     * @throws InvalidArgumentException
     * @param string $getAccountPermissionResult
     * @return \ID3Global\Models\GetAccountPermissionResponse
     */
    public function setGetAccountPermissionResult(?string $getAccountPermissionResult = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalPermission::valueIsValid($getAccountPermissionResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalPermission', is_array($getAccountPermissionResult) ? implode(', ', $getAccountPermissionResult) : var_export($getAccountPermissionResult, true), implode(', ', \ID3Global\Enums\GlobalPermission::getValidValues())), __LINE__);
        }
        $this->GetAccountPermissionResult = $getAccountPermissionResult;
        
        return $this;
    }
}
