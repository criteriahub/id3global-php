<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalGlobalDataConsortiumAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q643:GlobalGlobalDataConsortiumAccount
 * @subpackage Structs
 */
class GlobalGlobalDataConsortiumAccount extends GlobalSupplierAccount
{
    /**
     * The Tenant
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Tenant = null;
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * Constructor method for GlobalGlobalDataConsortiumAccount
     * @uses GlobalGlobalDataConsortiumAccount::setTenant()
     * @uses GlobalGlobalDataConsortiumAccount::setUsername()
     * @uses GlobalGlobalDataConsortiumAccount::setPassword()
     * @param string $tenant
     * @param string $username
     * @param string $password
     */
    public function __construct(?string $tenant = null, ?string $username = null, ?string $password = null)
    {
        $this
            ->setTenant($tenant)
            ->setUsername($username)
            ->setPassword($password);
    }
    /**
     * Get Tenant value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTenant(): ?string
    {
        return isset($this->Tenant) ? $this->Tenant : null;
    }
    /**
     * Set Tenant value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $tenant
     * @return \ID3Global\Models\GlobalGlobalDataConsortiumAccount
     */
    public function setTenant(?string $tenant = null): self
    {
        // validation for constraint: string
        if (!is_null($tenant) && !is_string($tenant)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tenant, true), gettype($tenant)), __LINE__);
        }
        if (is_null($tenant) || (is_array($tenant) && empty($tenant))) {
            unset($this->Tenant);
        } else {
            $this->Tenant = $tenant;
        }
        
        return $this;
    }
    /**
     * Get Username value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return isset($this->Username) ? $this->Username : null;
    }
    /**
     * Set Username value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username
     * @return \ID3Global\Models\GlobalGlobalDataConsortiumAccount
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        if (is_null($username) || (is_array($username) && empty($username))) {
            unset($this->Username);
        } else {
            $this->Username = $username;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\GlobalGlobalDataConsortiumAccount
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
}
