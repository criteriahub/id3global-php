<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSupportedFields Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q191:GlobalSupportedFields
 * @subpackage Structs
 */
class GlobalSupportedFields extends AbstractStructBase
{
    /**
     * The AddressFormat
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $AddressFormat = null;
    /**
     * The AddressLookupCountry
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLookupCountry = null;
    /**
     * The DocumentImageStorage
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $DocumentImageStorage = null;
    /**
     * The DocumentImageValidation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $DocumentImageValidation = null;
    /**
     * The PowerSearchEnabled
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $PowerSearchEnabled = null;
    /**
     * The SupportedFields
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSupportedField|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSupportedField $SupportedFields = null;
    /**
     * Constructor method for GlobalSupportedFields
     * @uses GlobalSupportedFields::setAddressFormat()
     * @uses GlobalSupportedFields::setAddressLookupCountry()
     * @uses GlobalSupportedFields::setDocumentImageStorage()
     * @uses GlobalSupportedFields::setDocumentImageValidation()
     * @uses GlobalSupportedFields::setPowerSearchEnabled()
     * @uses GlobalSupportedFields::setSupportedFields()
     * @param string $addressFormat
     * @param string $addressLookupCountry
     * @param bool $documentImageStorage
     * @param bool $documentImageValidation
     * @param bool $powerSearchEnabled
     * @param \ID3Global\Arrays\ArrayOfGlobalSupportedField $supportedFields
     */
    public function __construct(?string $addressFormat = null, ?string $addressLookupCountry = null, ?bool $documentImageStorage = null, ?bool $documentImageValidation = null, ?bool $powerSearchEnabled = null, ?\ID3Global\Arrays\ArrayOfGlobalSupportedField $supportedFields = null)
    {
        $this
            ->setAddressFormat($addressFormat)
            ->setAddressLookupCountry($addressLookupCountry)
            ->setDocumentImageStorage($documentImageStorage)
            ->setDocumentImageValidation($documentImageValidation)
            ->setPowerSearchEnabled($powerSearchEnabled)
            ->setSupportedFields($supportedFields);
    }
    /**
     * Get AddressFormat value
     * @return string|null
     */
    public function getAddressFormat(): ?string
    {
        return $this->AddressFormat;
    }
    /**
     * Set AddressFormat value
     * @uses \ID3Global\Enums\GlobalAddressFormat::valueIsValid()
     * @uses \ID3Global\Enums\GlobalAddressFormat::getValidValues()
     * @throws InvalidArgumentException
     * @param string $addressFormat
     * @return \ID3Global\Models\GlobalSupportedFields
     */
    public function setAddressFormat(?string $addressFormat = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalAddressFormat::valueIsValid($addressFormat)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalAddressFormat', is_array($addressFormat) ? implode(', ', $addressFormat) : var_export($addressFormat, true), implode(', ', \ID3Global\Enums\GlobalAddressFormat::getValidValues())), __LINE__);
        }
        $this->AddressFormat = $addressFormat;
        
        return $this;
    }
    /**
     * Get AddressLookupCountry value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLookupCountry(): ?string
    {
        return isset($this->AddressLookupCountry) ? $this->AddressLookupCountry : null;
    }
    /**
     * Set AddressLookupCountry value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLookupCountry
     * @return \ID3Global\Models\GlobalSupportedFields
     */
    public function setAddressLookupCountry(?string $addressLookupCountry = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLookupCountry) && !is_string($addressLookupCountry)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLookupCountry, true), gettype($addressLookupCountry)), __LINE__);
        }
        if (is_null($addressLookupCountry) || (is_array($addressLookupCountry) && empty($addressLookupCountry))) {
            unset($this->AddressLookupCountry);
        } else {
            $this->AddressLookupCountry = $addressLookupCountry;
        }
        
        return $this;
    }
    /**
     * Get DocumentImageStorage value
     * @return bool|null
     */
    public function getDocumentImageStorage(): ?bool
    {
        return $this->DocumentImageStorage;
    }
    /**
     * Set DocumentImageStorage value
     * @param bool $documentImageStorage
     * @return \ID3Global\Models\GlobalSupportedFields
     */
    public function setDocumentImageStorage(?bool $documentImageStorage = null): self
    {
        // validation for constraint: boolean
        if (!is_null($documentImageStorage) && !is_bool($documentImageStorage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($documentImageStorage, true), gettype($documentImageStorage)), __LINE__);
        }
        $this->DocumentImageStorage = $documentImageStorage;
        
        return $this;
    }
    /**
     * Get DocumentImageValidation value
     * @return bool|null
     */
    public function getDocumentImageValidation(): ?bool
    {
        return $this->DocumentImageValidation;
    }
    /**
     * Set DocumentImageValidation value
     * @param bool $documentImageValidation
     * @return \ID3Global\Models\GlobalSupportedFields
     */
    public function setDocumentImageValidation(?bool $documentImageValidation = null): self
    {
        // validation for constraint: boolean
        if (!is_null($documentImageValidation) && !is_bool($documentImageValidation)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($documentImageValidation, true), gettype($documentImageValidation)), __LINE__);
        }
        $this->DocumentImageValidation = $documentImageValidation;
        
        return $this;
    }
    /**
     * Get PowerSearchEnabled value
     * @return bool|null
     */
    public function getPowerSearchEnabled(): ?bool
    {
        return $this->PowerSearchEnabled;
    }
    /**
     * Set PowerSearchEnabled value
     * @param bool $powerSearchEnabled
     * @return \ID3Global\Models\GlobalSupportedFields
     */
    public function setPowerSearchEnabled(?bool $powerSearchEnabled = null): self
    {
        // validation for constraint: boolean
        if (!is_null($powerSearchEnabled) && !is_bool($powerSearchEnabled)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($powerSearchEnabled, true), gettype($powerSearchEnabled)), __LINE__);
        }
        $this->PowerSearchEnabled = $powerSearchEnabled;
        
        return $this;
    }
    /**
     * Get SupportedFields value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSupportedField|null
     */
    public function getSupportedFields(): ?\ID3Global\Arrays\ArrayOfGlobalSupportedField
    {
        return isset($this->SupportedFields) ? $this->SupportedFields : null;
    }
    /**
     * Set SupportedFields value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSupportedField $supportedFields
     * @return \ID3Global\Models\GlobalSupportedFields
     */
    public function setSupportedFields(?\ID3Global\Arrays\ArrayOfGlobalSupportedField $supportedFields = null): self
    {
        if (is_null($supportedFields) || (is_array($supportedFields) && empty($supportedFields))) {
            unset($this->SupportedFields);
        } else {
            $this->SupportedFields = $supportedFields;
        }
        
        return $this;
    }
}
