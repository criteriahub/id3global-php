<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSupplierFieldsBySupplierId Models
 * @subpackage Structs
 */
class GetSupplierFieldsBySupplierId extends AbstractStructBase
{
    /**
     * The supplierid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $supplierid = null;
    /**
     * Constructor method for GetSupplierFieldsBySupplierId
     * @uses GetSupplierFieldsBySupplierId::setSupplierid()
     * @param string $supplierid
     */
    public function __construct(?string $supplierid = null)
    {
        $this
            ->setSupplierid($supplierid);
    }
    /**
     * Get supplierid value
     * @return string|null
     */
    public function getSupplierid(): ?string
    {
        return $this->supplierid;
    }
    /**
     * Set supplierid value
     * @param string $supplierid
     * @return \ID3Global\Models\GetSupplierFieldsBySupplierId
     */
    public function setSupplierid(?string $supplierid = null): self
    {
        // validation for constraint: string
        if (!is_null($supplierid) && !is_string($supplierid)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($supplierid, true), gettype($supplierid)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($supplierid) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $supplierid)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($supplierid, true)), __LINE__);
        }
        $this->supplierid = $supplierid;
        
        return $this;
    }
}
