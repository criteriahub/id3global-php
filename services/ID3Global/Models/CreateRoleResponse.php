<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateRoleResponse Models
 * @subpackage Structs
 */
class CreateRoleResponse extends AbstractStructBase
{
    /**
     * The CreateRoleResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalRole|null
     */
    protected ?\ID3Global\Models\GlobalRole $CreateRoleResult = null;
    /**
     * Constructor method for CreateRoleResponse
     * @uses CreateRoleResponse::setCreateRoleResult()
     * @param \ID3Global\Models\GlobalRole $createRoleResult
     */
    public function __construct(?\ID3Global\Models\GlobalRole $createRoleResult = null)
    {
        $this
            ->setCreateRoleResult($createRoleResult);
    }
    /**
     * Get CreateRoleResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalRole|null
     */
    public function getCreateRoleResult(): ?\ID3Global\Models\GlobalRole
    {
        return isset($this->CreateRoleResult) ? $this->CreateRoleResult : null;
    }
    /**
     * Set CreateRoleResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalRole $createRoleResult
     * @return \ID3Global\Models\CreateRoleResponse
     */
    public function setCreateRoleResult(?\ID3Global\Models\GlobalRole $createRoleResult = null): self
    {
        if (is_null($createRoleResult) || (is_array($createRoleResult) && empty($createRoleResult))) {
            unset($this->CreateRoleResult);
        } else {
            $this->CreateRoleResult = $createRoleResult;
        }
        
        return $this;
    }
}
