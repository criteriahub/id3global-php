<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetLatestAuthenticationDetailsResponse Models
 * @subpackage Structs
 */
class GetLatestAuthenticationDetailsResponse extends AbstractStructBase
{
    /**
     * The GetLatestAuthenticationDetailsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAuthenticationDetails|null
     */
    protected ?\ID3Global\Models\GlobalAuthenticationDetails $GetLatestAuthenticationDetailsResult = null;
    /**
     * Constructor method for GetLatestAuthenticationDetailsResponse
     * @uses GetLatestAuthenticationDetailsResponse::setGetLatestAuthenticationDetailsResult()
     * @param \ID3Global\Models\GlobalAuthenticationDetails $getLatestAuthenticationDetailsResult
     */
    public function __construct(?\ID3Global\Models\GlobalAuthenticationDetails $getLatestAuthenticationDetailsResult = null)
    {
        $this
            ->setGetLatestAuthenticationDetailsResult($getLatestAuthenticationDetailsResult);
    }
    /**
     * Get GetLatestAuthenticationDetailsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAuthenticationDetails|null
     */
    public function getGetLatestAuthenticationDetailsResult(): ?\ID3Global\Models\GlobalAuthenticationDetails
    {
        return isset($this->GetLatestAuthenticationDetailsResult) ? $this->GetLatestAuthenticationDetailsResult : null;
    }
    /**
     * Set GetLatestAuthenticationDetailsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAuthenticationDetails $getLatestAuthenticationDetailsResult
     * @return \ID3Global\Models\GetLatestAuthenticationDetailsResponse
     */
    public function setGetLatestAuthenticationDetailsResult(?\ID3Global\Models\GlobalAuthenticationDetails $getLatestAuthenticationDetailsResult = null): self
    {
        if (is_null($getLatestAuthenticationDetailsResult) || (is_array($getLatestAuthenticationDetailsResult) && empty($getLatestAuthenticationDetailsResult))) {
            unset($this->GetLatestAuthenticationDetailsResult);
        } else {
            $this->GetLatestAuthenticationDetailsResult = $getLatestAuthenticationDetailsResult;
        }
        
        return $this;
    }
}
