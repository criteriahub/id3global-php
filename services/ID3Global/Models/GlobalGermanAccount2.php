<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalGermanAccount2 Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q662:GlobalGermanAccount2
 * @subpackage Structs
 */
class GlobalGermanAccount2 extends GlobalSupplierAccount
{
    /**
     * The ClientID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ClientID = null;
    /**
     * The Authorisation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Authorisation = null;
    /**
     * Constructor method for GlobalGermanAccount2
     * @uses GlobalGermanAccount2::setClientID()
     * @uses GlobalGermanAccount2::setAuthorisation()
     * @param string $clientID
     * @param string $authorisation
     */
    public function __construct(?string $clientID = null, ?string $authorisation = null)
    {
        $this
            ->setClientID($clientID)
            ->setAuthorisation($authorisation);
    }
    /**
     * Get ClientID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getClientID(): ?string
    {
        return isset($this->ClientID) ? $this->ClientID : null;
    }
    /**
     * Set ClientID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $clientID
     * @return \ID3Global\Models\GlobalGermanAccount2
     */
    public function setClientID(?string $clientID = null): self
    {
        // validation for constraint: string
        if (!is_null($clientID) && !is_string($clientID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($clientID, true), gettype($clientID)), __LINE__);
        }
        if (is_null($clientID) || (is_array($clientID) && empty($clientID))) {
            unset($this->ClientID);
        } else {
            $this->ClientID = $clientID;
        }
        
        return $this;
    }
    /**
     * Get Authorisation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAuthorisation(): ?string
    {
        return isset($this->Authorisation) ? $this->Authorisation : null;
    }
    /**
     * Set Authorisation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $authorisation
     * @return \ID3Global\Models\GlobalGermanAccount2
     */
    public function setAuthorisation(?string $authorisation = null): self
    {
        // validation for constraint: string
        if (!is_null($authorisation) && !is_string($authorisation)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authorisation, true), gettype($authorisation)), __LINE__);
        }
        if (is_null($authorisation) || (is_array($authorisation) && empty($authorisation))) {
            unset($this->Authorisation);
        } else {
            $this->Authorisation = $authorisation;
        }
        
        return $this;
    }
}
