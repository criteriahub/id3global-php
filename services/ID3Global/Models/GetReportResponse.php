<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetReportResponse Models
 * @subpackage Structs
 */
class GetReportResponse extends AbstractStructBase
{
    /**
     * The GetReportResult
     * Meta information extracted from the WSDL
     * - base: xs:base64Binary
     * @var string|null
     */
    protected ?string $GetReportResult = null;
    /**
     * Constructor method for GetReportResponse
     * @uses GetReportResponse::setGetReportResult()
     * @param string $getReportResult
     */
    public function __construct(?string $getReportResult = null)
    {
        $this
            ->setGetReportResult($getReportResult);
    }
    /**
     * Get GetReportResult value
     * @return string|null
     */
    public function getGetReportResult(): ?string
    {
        return $this->GetReportResult;
    }
    /**
     * Set GetReportResult value
     * @param string $getReportResult
     * @return \ID3Global\Models\GetReportResponse
     */
    public function setGetReportResult(?string $getReportResult = null): self
    {
        // validation for constraint: string
        if (!is_null($getReportResult) && !is_string($getReportResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($getReportResult, true), gettype($getReportResult)), __LINE__);
        }
        $this->GetReportResult = $getReportResult;
        
        return $this;
    }
}
