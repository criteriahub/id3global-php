<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalImageDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q158:GlobalImageDetails
 * @subpackage Structs
 */
class GlobalImageDetails extends GlobalImage
{
    /**
     * The ImageID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ImageID = null;
    /**
     * The ProcessState
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ProcessState = null;
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The Timestamp
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Timestamp = null;
    /**
     * The HelpdeskAccess
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $HelpdeskAccess = null;
    /**
     * The DIVAuthID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $DIVAuthID = null;
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The AccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AccountID = null;
    /**
     * The OrgName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OrgName = null;
    /**
     * The AccountName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccountName = null;
    /**
     * Constructor method for GlobalImageDetails
     * @uses GlobalImageDetails::setImageID()
     * @uses GlobalImageDetails::setProcessState()
     * @uses GlobalImageDetails::setUsername()
     * @uses GlobalImageDetails::setTimestamp()
     * @uses GlobalImageDetails::setHelpdeskAccess()
     * @uses GlobalImageDetails::setDIVAuthID()
     * @uses GlobalImageDetails::setOrgID()
     * @uses GlobalImageDetails::setAccountID()
     * @uses GlobalImageDetails::setOrgName()
     * @uses GlobalImageDetails::setAccountName()
     * @param string $imageID
     * @param string $processState
     * @param string $username
     * @param string $timestamp
     * @param bool $helpdeskAccess
     * @param string $dIVAuthID
     * @param string $orgID
     * @param string $accountID
     * @param string $orgName
     * @param string $accountName
     */
    public function __construct(?string $imageID = null, ?string $processState = null, ?string $username = null, ?string $timestamp = null, ?bool $helpdeskAccess = null, ?string $dIVAuthID = null, ?string $orgID = null, ?string $accountID = null, ?string $orgName = null, ?string $accountName = null)
    {
        $this
            ->setImageID($imageID)
            ->setProcessState($processState)
            ->setUsername($username)
            ->setTimestamp($timestamp)
            ->setHelpdeskAccess($helpdeskAccess)
            ->setDIVAuthID($dIVAuthID)
            ->setOrgID($orgID)
            ->setAccountID($accountID)
            ->setOrgName($orgName)
            ->setAccountName($accountName);
    }
    /**
     * Get ImageID value
     * @return string|null
     */
    public function getImageID(): ?string
    {
        return $this->ImageID;
    }
    /**
     * Set ImageID value
     * @param string $imageID
     * @return \ID3Global\Models\GlobalImageDetails
     */
    public function setImageID(?string $imageID = null): self
    {
        // validation for constraint: string
        if (!is_null($imageID) && !is_string($imageID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($imageID, true), gettype($imageID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($imageID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $imageID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($imageID, true)), __LINE__);
        }
        $this->ImageID = $imageID;
        
        return $this;
    }
    /**
     * Get ProcessState value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProcessState(): ?string
    {
        return isset($this->ProcessState) ? $this->ProcessState : null;
    }
    /**
     * Set ProcessState value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $processState
     * @return \ID3Global\Models\GlobalImageDetails
     */
    public function setProcessState(?string $processState = null): self
    {
        // validation for constraint: string
        if (!is_null($processState) && !is_string($processState)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($processState, true), gettype($processState)), __LINE__);
        }
        if (is_null($processState) || (is_array($processState) && empty($processState))) {
            unset($this->ProcessState);
        } else {
            $this->ProcessState = $processState;
        }
        
        return $this;
    }
    /**
     * Get Username value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return isset($this->Username) ? $this->Username : null;
    }
    /**
     * Set Username value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username
     * @return \ID3Global\Models\GlobalImageDetails
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        if (is_null($username) || (is_array($username) && empty($username))) {
            unset($this->Username);
        } else {
            $this->Username = $username;
        }
        
        return $this;
    }
    /**
     * Get Timestamp value
     * @return string|null
     */
    public function getTimestamp(): ?string
    {
        return $this->Timestamp;
    }
    /**
     * Set Timestamp value
     * @param string $timestamp
     * @return \ID3Global\Models\GlobalImageDetails
     */
    public function setTimestamp(?string $timestamp = null): self
    {
        // validation for constraint: string
        if (!is_null($timestamp) && !is_string($timestamp)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($timestamp, true), gettype($timestamp)), __LINE__);
        }
        $this->Timestamp = $timestamp;
        
        return $this;
    }
    /**
     * Get HelpdeskAccess value
     * @return bool|null
     */
    public function getHelpdeskAccess(): ?bool
    {
        return $this->HelpdeskAccess;
    }
    /**
     * Set HelpdeskAccess value
     * @param bool $helpdeskAccess
     * @return \ID3Global\Models\GlobalImageDetails
     */
    public function setHelpdeskAccess(?bool $helpdeskAccess = null): self
    {
        // validation for constraint: boolean
        if (!is_null($helpdeskAccess) && !is_bool($helpdeskAccess)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($helpdeskAccess, true), gettype($helpdeskAccess)), __LINE__);
        }
        $this->HelpdeskAccess = $helpdeskAccess;
        
        return $this;
    }
    /**
     * Get DIVAuthID value
     * @return string|null
     */
    public function getDIVAuthID(): ?string
    {
        return $this->DIVAuthID;
    }
    /**
     * Set DIVAuthID value
     * @param string $dIVAuthID
     * @return \ID3Global\Models\GlobalImageDetails
     */
    public function setDIVAuthID(?string $dIVAuthID = null): self
    {
        // validation for constraint: string
        if (!is_null($dIVAuthID) && !is_string($dIVAuthID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dIVAuthID, true), gettype($dIVAuthID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($dIVAuthID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $dIVAuthID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($dIVAuthID, true)), __LINE__);
        }
        $this->DIVAuthID = $dIVAuthID;
        
        return $this;
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GlobalImageDetails
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get AccountID value
     * @return string|null
     */
    public function getAccountID(): ?string
    {
        return $this->AccountID;
    }
    /**
     * Set AccountID value
     * @param string $accountID
     * @return \ID3Global\Models\GlobalImageDetails
     */
    public function setAccountID(?string $accountID = null): self
    {
        // validation for constraint: string
        if (!is_null($accountID) && !is_string($accountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountID, true), gettype($accountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($accountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $accountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($accountID, true)), __LINE__);
        }
        $this->AccountID = $accountID;
        
        return $this;
    }
    /**
     * Get OrgName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrgName(): ?string
    {
        return isset($this->OrgName) ? $this->OrgName : null;
    }
    /**
     * Set OrgName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $orgName
     * @return \ID3Global\Models\GlobalImageDetails
     */
    public function setOrgName(?string $orgName = null): self
    {
        // validation for constraint: string
        if (!is_null($orgName) && !is_string($orgName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgName, true), gettype($orgName)), __LINE__);
        }
        if (is_null($orgName) || (is_array($orgName) && empty($orgName))) {
            unset($this->OrgName);
        } else {
            $this->OrgName = $orgName;
        }
        
        return $this;
    }
    /**
     * Get AccountName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccountName(): ?string
    {
        return isset($this->AccountName) ? $this->AccountName : null;
    }
    /**
     * Set AccountName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accountName
     * @return \ID3Global\Models\GlobalImageDetails
     */
    public function setAccountName(?string $accountName = null): self
    {
        // validation for constraint: string
        if (!is_null($accountName) && !is_string($accountName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountName, true), gettype($accountName)), __LINE__);
        }
        if (is_null($accountName) || (is_array($accountName) && empty($accountName))) {
            unset($this->AccountName);
        } else {
            $this->AccountName = $accountName;
        }
        
        return $this;
    }
}
