<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetFCRASanctionsIssuersResponse Models
 * @subpackage Structs
 */
class GetFCRASanctionsIssuersResponse extends AbstractStructBase
{
    /**
     * The GetFCRASanctionsIssuersResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer $GetFCRASanctionsIssuersResult = null;
    /**
     * Constructor method for GetFCRASanctionsIssuersResponse
     * @uses GetFCRASanctionsIssuersResponse::setGetFCRASanctionsIssuersResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer $getFCRASanctionsIssuersResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer $getFCRASanctionsIssuersResult = null)
    {
        $this
            ->setGetFCRASanctionsIssuersResult($getFCRASanctionsIssuersResult);
    }
    /**
     * Get GetFCRASanctionsIssuersResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer|null
     */
    public function getGetFCRASanctionsIssuersResult(): ?\ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer
    {
        return isset($this->GetFCRASanctionsIssuersResult) ? $this->GetFCRASanctionsIssuersResult : null;
    }
    /**
     * Set GetFCRASanctionsIssuersResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer $getFCRASanctionsIssuersResult
     * @return \ID3Global\Models\GetFCRASanctionsIssuersResponse
     */
    public function setGetFCRASanctionsIssuersResult(?\ID3Global\Arrays\ArrayOfGlobalSanctionsIssuer $getFCRASanctionsIssuersResult = null): self
    {
        if (is_null($getFCRASanctionsIssuersResult) || (is_array($getFCRASanctionsIssuersResult) && empty($getFCRASanctionsIssuersResult))) {
            unset($this->GetFCRASanctionsIssuersResult);
        } else {
            $this->GetFCRASanctionsIssuersResult = $getFCRASanctionsIssuersResult;
        }
        
        return $this;
    }
}
