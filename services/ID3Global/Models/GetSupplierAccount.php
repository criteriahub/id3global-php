<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSupplierAccount Models
 * @subpackage Structs
 */
class GetSupplierAccount extends AbstractStructBase
{
    /**
     * The SupplierAccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $SupplierAccountID = null;
    /**
     * Constructor method for GetSupplierAccount
     * @uses GetSupplierAccount::setSupplierAccountID()
     * @param string $supplierAccountID
     */
    public function __construct(?string $supplierAccountID = null)
    {
        $this
            ->setSupplierAccountID($supplierAccountID);
    }
    /**
     * Get SupplierAccountID value
     * @return string|null
     */
    public function getSupplierAccountID(): ?string
    {
        return $this->SupplierAccountID;
    }
    /**
     * Set SupplierAccountID value
     * @param string $supplierAccountID
     * @return \ID3Global\Models\GetSupplierAccount
     */
    public function setSupplierAccountID(?string $supplierAccountID = null): self
    {
        // validation for constraint: string
        if (!is_null($supplierAccountID) && !is_string($supplierAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($supplierAccountID, true), gettype($supplierAccountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($supplierAccountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $supplierAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($supplierAccountID, true)), __LINE__);
        }
        $this->SupplierAccountID = $supplierAccountID;
        
        return $this;
    }
}
