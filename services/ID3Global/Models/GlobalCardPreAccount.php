<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCardPreAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q651:GlobalCardPreAccount
 * @subpackage Structs
 */
class GlobalCardPreAccount extends GlobalSupplierAccount
{
    /**
     * The EcommerceID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $EcommerceID = null;
    /**
     * The MOTOID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MOTOID = null;
    /**
     * The Amount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Amount = null;
    /**
     * Constructor method for GlobalCardPreAccount
     * @uses GlobalCardPreAccount::setEcommerceID()
     * @uses GlobalCardPreAccount::setMOTOID()
     * @uses GlobalCardPreAccount::setAmount()
     * @param string $ecommerceID
     * @param string $mOTOID
     * @param int $amount
     */
    public function __construct(?string $ecommerceID = null, ?string $mOTOID = null, ?int $amount = null)
    {
        $this
            ->setEcommerceID($ecommerceID)
            ->setMOTOID($mOTOID)
            ->setAmount($amount);
    }
    /**
     * Get EcommerceID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEcommerceID(): ?string
    {
        return isset($this->EcommerceID) ? $this->EcommerceID : null;
    }
    /**
     * Set EcommerceID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $ecommerceID
     * @return \ID3Global\Models\GlobalCardPreAccount
     */
    public function setEcommerceID(?string $ecommerceID = null): self
    {
        // validation for constraint: string
        if (!is_null($ecommerceID) && !is_string($ecommerceID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ecommerceID, true), gettype($ecommerceID)), __LINE__);
        }
        if (is_null($ecommerceID) || (is_array($ecommerceID) && empty($ecommerceID))) {
            unset($this->EcommerceID);
        } else {
            $this->EcommerceID = $ecommerceID;
        }
        
        return $this;
    }
    /**
     * Get MOTOID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMOTOID(): ?string
    {
        return isset($this->MOTOID) ? $this->MOTOID : null;
    }
    /**
     * Set MOTOID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mOTOID
     * @return \ID3Global\Models\GlobalCardPreAccount
     */
    public function setMOTOID(?string $mOTOID = null): self
    {
        // validation for constraint: string
        if (!is_null($mOTOID) && !is_string($mOTOID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mOTOID, true), gettype($mOTOID)), __LINE__);
        }
        if (is_null($mOTOID) || (is_array($mOTOID) && empty($mOTOID))) {
            unset($this->MOTOID);
        } else {
            $this->MOTOID = $mOTOID;
        }
        
        return $this;
    }
    /**
     * Get Amount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getAmount(): ?int
    {
        return isset($this->Amount) ? $this->Amount : null;
    }
    /**
     * Set Amount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $amount
     * @return \ID3Global\Models\GlobalCardPreAccount
     */
    public function setAmount(?int $amount = null): self
    {
        // validation for constraint: int
        if (!is_null($amount) && !(is_int($amount) || ctype_digit($amount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        if (is_null($amount) || (is_array($amount) && empty($amount))) {
            unset($this->Amount);
        } else {
            $this->Amount = $amount;
        }
        
        return $this;
    }
}
