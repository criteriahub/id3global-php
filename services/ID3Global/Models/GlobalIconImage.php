<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalIconImage Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q285:GlobalIconImage
 * @subpackage Structs
 */
class GlobalIconImage extends AbstractStructBase
{
    /**
     * The Label
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Label = null;
    /**
     * The Base64Image
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Base64Image = null;
    /**
     * Constructor method for GlobalIconImage
     * @uses GlobalIconImage::setLabel()
     * @uses GlobalIconImage::setBase64Image()
     * @param string $label
     * @param string $base64Image
     */
    public function __construct(?string $label = null, ?string $base64Image = null)
    {
        $this
            ->setLabel($label)
            ->setBase64Image($base64Image);
    }
    /**
     * Get Label value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return isset($this->Label) ? $this->Label : null;
    }
    /**
     * Set Label value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $label
     * @return \ID3Global\Models\GlobalIconImage
     */
    public function setLabel(?string $label = null): self
    {
        // validation for constraint: string
        if (!is_null($label) && !is_string($label)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($label, true), gettype($label)), __LINE__);
        }
        if (is_null($label) || (is_array($label) && empty($label))) {
            unset($this->Label);
        } else {
            $this->Label = $label;
        }
        
        return $this;
    }
    /**
     * Get Base64Image value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBase64Image(): ?string
    {
        return isset($this->Base64Image) ? $this->Base64Image : null;
    }
    /**
     * Set Base64Image value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $base64Image
     * @return \ID3Global\Models\GlobalIconImage
     */
    public function setBase64Image(?string $base64Image = null): self
    {
        // validation for constraint: string
        if (!is_null($base64Image) && !is_string($base64Image)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($base64Image, true), gettype($base64Image)), __LINE__);
        }
        if (is_null($base64Image) || (is_array($base64Image) && empty($base64Image))) {
            unset($this->Base64Image);
        } else {
            $this->Base64Image = $base64Image;
        }
        
        return $this;
    }
}
