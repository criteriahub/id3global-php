<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalExperianAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q710:GlobalExperianAccount
 * @subpackage Structs
 */
class GlobalExperianAccount extends GlobalSupplierAccount
{
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * The ClientID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ClientID = null;
    /**
     * The ClientSecret
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ClientSecret = null;
    /**
     * The ClientAccountNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ClientAccountNumber = null;
    /**
     * The ElectoralRegisterAccess
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ElectoralRegisterAccess = null;
    /**
     * Constructor method for GlobalExperianAccount
     * @uses GlobalExperianAccount::setUsername()
     * @uses GlobalExperianAccount::setPassword()
     * @uses GlobalExperianAccount::setClientID()
     * @uses GlobalExperianAccount::setClientSecret()
     * @uses GlobalExperianAccount::setClientAccountNumber()
     * @uses GlobalExperianAccount::setElectoralRegisterAccess()
     * @param string $username
     * @param string $password
     * @param string $clientID
     * @param string $clientSecret
     * @param string $clientAccountNumber
     * @param string $electoralRegisterAccess
     */
    public function __construct(?string $username = null, ?string $password = null, ?string $clientID = null, ?string $clientSecret = null, ?string $clientAccountNumber = null, ?string $electoralRegisterAccess = null)
    {
        $this
            ->setUsername($username)
            ->setPassword($password)
            ->setClientID($clientID)
            ->setClientSecret($clientSecret)
            ->setClientAccountNumber($clientAccountNumber)
            ->setElectoralRegisterAccess($electoralRegisterAccess);
    }
    /**
     * Get Username value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return isset($this->Username) ? $this->Username : null;
    }
    /**
     * Set Username value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username
     * @return \ID3Global\Models\GlobalExperianAccount
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        if (is_null($username) || (is_array($username) && empty($username))) {
            unset($this->Username);
        } else {
            $this->Username = $username;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\GlobalExperianAccount
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
    /**
     * Get ClientID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getClientID(): ?string
    {
        return isset($this->ClientID) ? $this->ClientID : null;
    }
    /**
     * Set ClientID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $clientID
     * @return \ID3Global\Models\GlobalExperianAccount
     */
    public function setClientID(?string $clientID = null): self
    {
        // validation for constraint: string
        if (!is_null($clientID) && !is_string($clientID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($clientID, true), gettype($clientID)), __LINE__);
        }
        if (is_null($clientID) || (is_array($clientID) && empty($clientID))) {
            unset($this->ClientID);
        } else {
            $this->ClientID = $clientID;
        }
        
        return $this;
    }
    /**
     * Get ClientSecret value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getClientSecret(): ?string
    {
        return isset($this->ClientSecret) ? $this->ClientSecret : null;
    }
    /**
     * Set ClientSecret value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $clientSecret
     * @return \ID3Global\Models\GlobalExperianAccount
     */
    public function setClientSecret(?string $clientSecret = null): self
    {
        // validation for constraint: string
        if (!is_null($clientSecret) && !is_string($clientSecret)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($clientSecret, true), gettype($clientSecret)), __LINE__);
        }
        if (is_null($clientSecret) || (is_array($clientSecret) && empty($clientSecret))) {
            unset($this->ClientSecret);
        } else {
            $this->ClientSecret = $clientSecret;
        }
        
        return $this;
    }
    /**
     * Get ClientAccountNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getClientAccountNumber(): ?string
    {
        return isset($this->ClientAccountNumber) ? $this->ClientAccountNumber : null;
    }
    /**
     * Set ClientAccountNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $clientAccountNumber
     * @return \ID3Global\Models\GlobalExperianAccount
     */
    public function setClientAccountNumber(?string $clientAccountNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($clientAccountNumber) && !is_string($clientAccountNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($clientAccountNumber, true), gettype($clientAccountNumber)), __LINE__);
        }
        if (is_null($clientAccountNumber) || (is_array($clientAccountNumber) && empty($clientAccountNumber))) {
            unset($this->ClientAccountNumber);
        } else {
            $this->ClientAccountNumber = $clientAccountNumber;
        }
        
        return $this;
    }
    /**
     * Get ElectoralRegisterAccess value
     * @return string|null
     */
    public function getElectoralRegisterAccess(): ?string
    {
        return $this->ElectoralRegisterAccess;
    }
    /**
     * Set ElectoralRegisterAccess value
     * @uses \ID3Global\Enums\GlobalElectoralRegisterAccess::valueIsValid()
     * @uses \ID3Global\Enums\GlobalElectoralRegisterAccess::getValidValues()
     * @throws InvalidArgumentException
     * @param string $electoralRegisterAccess
     * @return \ID3Global\Models\GlobalExperianAccount
     */
    public function setElectoralRegisterAccess(?string $electoralRegisterAccess = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalElectoralRegisterAccess::valueIsValid($electoralRegisterAccess)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalElectoralRegisterAccess', is_array($electoralRegisterAccess) ? implode(', ', $electoralRegisterAccess) : var_export($electoralRegisterAccess, true), implode(', ', \ID3Global\Enums\GlobalElectoralRegisterAccess::getValidValues())), __LINE__);
        }
        $this->ElectoralRegisterAccess = $electoralRegisterAccess;
        
        return $this;
    }
}
