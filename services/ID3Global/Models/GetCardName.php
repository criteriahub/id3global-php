<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCardName Models
 * @subpackage Structs
 */
class GetCardName extends AbstractStructBase
{
    /**
     * The CardType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $CardType = null;
    /**
     * Constructor method for GetCardName
     * @uses GetCardName::setCardType()
     * @param string $cardType
     */
    public function __construct(?string $cardType = null)
    {
        $this
            ->setCardType($cardType);
    }
    /**
     * Get CardType value
     * @return string|null
     */
    public function getCardType(): ?string
    {
        return $this->CardType;
    }
    /**
     * Set CardType value
     * @uses \ID3Global\Enums\GlobalCardType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalCardType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $cardType
     * @return \ID3Global\Models\GetCardName
     */
    public function setCardType(?string $cardType = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalCardType::valueIsValid($cardType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalCardType', is_array($cardType) ? implode(', ', $cardType) : var_export($cardType, true), implode(', ', \ID3Global\Enums\GlobalCardType::getValidValues())), __LINE__);
        }
        $this->CardType = $cardType;
        
        return $this;
    }
}
