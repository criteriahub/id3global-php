<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CheckNonce Models
 * @subpackage Structs
 */
class CheckNonce extends AbstractStructBase
{
    /**
     * The Nonce
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Nonce = null;
    /**
     * Constructor method for CheckNonce
     * @uses CheckNonce::setNonce()
     * @param string $nonce
     */
    public function __construct(?string $nonce = null)
    {
        $this
            ->setNonce($nonce);
    }
    /**
     * Get Nonce value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNonce(): ?string
    {
        return isset($this->Nonce) ? $this->Nonce : null;
    }
    /**
     * Set Nonce value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $nonce
     * @return \ID3Global\Models\CheckNonce
     */
    public function setNonce(?string $nonce = null): self
    {
        // validation for constraint: string
        if (!is_null($nonce) && !is_string($nonce)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nonce, true), gettype($nonce)), __LINE__);
        }
        if (is_null($nonce) || (is_array($nonce) && empty($nonce))) {
            unset($this->Nonce);
        } else {
            $this->Nonce = $nonce;
        }
        
        return $this;
    }
}
