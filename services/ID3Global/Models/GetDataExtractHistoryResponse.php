<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDataExtractHistoryResponse Models
 * @subpackage Structs
 */
class GetDataExtractHistoryResponse extends AbstractStructBase
{
    /**
     * The GetDataExtractHistoryResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalDataExtractDownloadHistory|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalDataExtractDownloadHistory $GetDataExtractHistoryResult = null;
    /**
     * Constructor method for GetDataExtractHistoryResponse
     * @uses GetDataExtractHistoryResponse::setGetDataExtractHistoryResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalDataExtractDownloadHistory $getDataExtractHistoryResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalDataExtractDownloadHistory $getDataExtractHistoryResult = null)
    {
        $this
            ->setGetDataExtractHistoryResult($getDataExtractHistoryResult);
    }
    /**
     * Get GetDataExtractHistoryResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalDataExtractDownloadHistory|null
     */
    public function getGetDataExtractHistoryResult(): ?\ID3Global\Arrays\ArrayOfGlobalDataExtractDownloadHistory
    {
        return isset($this->GetDataExtractHistoryResult) ? $this->GetDataExtractHistoryResult : null;
    }
    /**
     * Set GetDataExtractHistoryResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalDataExtractDownloadHistory $getDataExtractHistoryResult
     * @return \ID3Global\Models\GetDataExtractHistoryResponse
     */
    public function setGetDataExtractHistoryResult(?\ID3Global\Arrays\ArrayOfGlobalDataExtractDownloadHistory $getDataExtractHistoryResult = null): self
    {
        if (is_null($getDataExtractHistoryResult) || (is_array($getDataExtractHistoryResult) && empty($getDataExtractHistoryResult))) {
            unset($this->GetDataExtractHistoryResult);
        } else {
            $this->GetDataExtractHistoryResult = $getDataExtractHistoryResult;
        }
        
        return $this;
    }
}
