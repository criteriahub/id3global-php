<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetFCRAMatchDetailsResponse Models
 * @subpackage Structs
 */
class GetFCRAMatchDetailsResponse extends AbstractStructBase
{
    /**
     * The GetFCRAMatchDetailsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalFCRAMatchDetails|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalFCRAMatchDetails $GetFCRAMatchDetailsResult = null;
    /**
     * Constructor method for GetFCRAMatchDetailsResponse
     * @uses GetFCRAMatchDetailsResponse::setGetFCRAMatchDetailsResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalFCRAMatchDetails $getFCRAMatchDetailsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalFCRAMatchDetails $getFCRAMatchDetailsResult = null)
    {
        $this
            ->setGetFCRAMatchDetailsResult($getFCRAMatchDetailsResult);
    }
    /**
     * Get GetFCRAMatchDetailsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalFCRAMatchDetails|null
     */
    public function getGetFCRAMatchDetailsResult(): ?\ID3Global\Arrays\ArrayOfGlobalFCRAMatchDetails
    {
        return isset($this->GetFCRAMatchDetailsResult) ? $this->GetFCRAMatchDetailsResult : null;
    }
    /**
     * Set GetFCRAMatchDetailsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalFCRAMatchDetails $getFCRAMatchDetailsResult
     * @return \ID3Global\Models\GetFCRAMatchDetailsResponse
     */
    public function setGetFCRAMatchDetailsResult(?\ID3Global\Arrays\ArrayOfGlobalFCRAMatchDetails $getFCRAMatchDetailsResult = null): self
    {
        if (is_null($getFCRAMatchDetailsResult) || (is_array($getFCRAMatchDetailsResult) && empty($getFCRAMatchDetailsResult))) {
            unset($this->GetFCRAMatchDetailsResult);
        } else {
            $this->GetFCRAMatchDetailsResult = $getFCRAMatchDetailsResult;
        }
        
        return $this;
    }
}
