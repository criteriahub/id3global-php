<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalBreakpoint Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q247:GlobalBreakpoint
 * @subpackage Structs
 */
class GlobalBreakpoint extends GlobalItem
{
    /**
     * The IsNoRetry
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsNoRetry = null;
    /**
     * Constructor method for GlobalBreakpoint
     * @uses GlobalBreakpoint::setIsNoRetry()
     * @param bool $isNoRetry
     */
    public function __construct(?bool $isNoRetry = null)
    {
        $this
            ->setIsNoRetry($isNoRetry);
    }
    /**
     * Get IsNoRetry value
     * @return bool|null
     */
    public function getIsNoRetry(): ?bool
    {
        return $this->IsNoRetry;
    }
    /**
     * Set IsNoRetry value
     * @param bool $isNoRetry
     * @return \ID3Global\Models\GlobalBreakpoint
     */
    public function setIsNoRetry(?bool $isNoRetry = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isNoRetry) && !is_bool($isNoRetry)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isNoRetry, true), gettype($isNoRetry)), __LINE__);
        }
        $this->IsNoRetry = $isNoRetry;
        
        return $this;
    }
}
