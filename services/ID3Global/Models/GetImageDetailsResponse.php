<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetImageDetailsResponse Models
 * @subpackage Structs
 */
class GetImageDetailsResponse extends AbstractStructBase
{
    /**
     * The GetImageDetailsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalImageDetails|null
     */
    protected ?\ID3Global\Models\GlobalImageDetails $GetImageDetailsResult = null;
    /**
     * Constructor method for GetImageDetailsResponse
     * @uses GetImageDetailsResponse::setGetImageDetailsResult()
     * @param \ID3Global\Models\GlobalImageDetails $getImageDetailsResult
     */
    public function __construct(?\ID3Global\Models\GlobalImageDetails $getImageDetailsResult = null)
    {
        $this
            ->setGetImageDetailsResult($getImageDetailsResult);
    }
    /**
     * Get GetImageDetailsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalImageDetails|null
     */
    public function getGetImageDetailsResult(): ?\ID3Global\Models\GlobalImageDetails
    {
        return isset($this->GetImageDetailsResult) ? $this->GetImageDetailsResult : null;
    }
    /**
     * Set GetImageDetailsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalImageDetails $getImageDetailsResult
     * @return \ID3Global\Models\GetImageDetailsResponse
     */
    public function setGetImageDetailsResult(?\ID3Global\Models\GlobalImageDetails $getImageDetailsResult = null): self
    {
        if (is_null($getImageDetailsResult) || (is_array($getImageDetailsResult) && empty($getImageDetailsResult))) {
            unset($this->GetImageDetailsResult);
        } else {
            $this->GetImageDetailsResult = $getImageDetailsResult;
        }
        
        return $this;
    }
}
