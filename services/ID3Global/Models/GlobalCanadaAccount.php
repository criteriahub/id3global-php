<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCanadaAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q649:GlobalCanadaAccount
 * @subpackage Structs
 */
class GlobalCanadaAccount extends GlobalSupplierAccount
{
    /**
     * The MemberCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MemberCode = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * Constructor method for GlobalCanadaAccount
     * @uses GlobalCanadaAccount::setMemberCode()
     * @uses GlobalCanadaAccount::setPassword()
     * @param string $memberCode
     * @param string $password
     */
    public function __construct(?string $memberCode = null, ?string $password = null)
    {
        $this
            ->setMemberCode($memberCode)
            ->setPassword($password);
    }
    /**
     * Get MemberCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMemberCode(): ?string
    {
        return isset($this->MemberCode) ? $this->MemberCode : null;
    }
    /**
     * Set MemberCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $memberCode
     * @return \ID3Global\Models\GlobalCanadaAccount
     */
    public function setMemberCode(?string $memberCode = null): self
    {
        // validation for constraint: string
        if (!is_null($memberCode) && !is_string($memberCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($memberCode, true), gettype($memberCode)), __LINE__);
        }
        if (is_null($memberCode) || (is_array($memberCode) && empty($memberCode))) {
            unset($this->MemberCode);
        } else {
            $this->MemberCode = $memberCode;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\GlobalCanadaAccount
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
}
