<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalIndia Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q355:GlobalIndia
 * @subpackage Structs
 */
class GlobalIndia extends AbstractStructBase
{
    /**
     * The PAN
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalIndiaPAN|null
     */
    protected ?\ID3Global\Models\GlobalIndiaPAN $PAN = null;
    /**
     * The DrivingLicence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalIndiaDrivingLicence|null
     */
    protected ?\ID3Global\Models\GlobalIndiaDrivingLicence $DrivingLicence = null;
    /**
     * The Epic
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalIndiaEpic|null
     */
    protected ?\ID3Global\Models\GlobalIndiaEpic $Epic = null;
    /**
     * Constructor method for GlobalIndia
     * @uses GlobalIndia::setPAN()
     * @uses GlobalIndia::setDrivingLicence()
     * @uses GlobalIndia::setEpic()
     * @param \ID3Global\Models\GlobalIndiaPAN $pAN
     * @param \ID3Global\Models\GlobalIndiaDrivingLicence $drivingLicence
     * @param \ID3Global\Models\GlobalIndiaEpic $epic
     */
    public function __construct(?\ID3Global\Models\GlobalIndiaPAN $pAN = null, ?\ID3Global\Models\GlobalIndiaDrivingLicence $drivingLicence = null, ?\ID3Global\Models\GlobalIndiaEpic $epic = null)
    {
        $this
            ->setPAN($pAN)
            ->setDrivingLicence($drivingLicence)
            ->setEpic($epic);
    }
    /**
     * Get PAN value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalIndiaPAN|null
     */
    public function getPAN(): ?\ID3Global\Models\GlobalIndiaPAN
    {
        return isset($this->PAN) ? $this->PAN : null;
    }
    /**
     * Set PAN value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalIndiaPAN $pAN
     * @return \ID3Global\Models\GlobalIndia
     */
    public function setPAN(?\ID3Global\Models\GlobalIndiaPAN $pAN = null): self
    {
        if (is_null($pAN) || (is_array($pAN) && empty($pAN))) {
            unset($this->PAN);
        } else {
            $this->PAN = $pAN;
        }
        
        return $this;
    }
    /**
     * Get DrivingLicence value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalIndiaDrivingLicence|null
     */
    public function getDrivingLicence(): ?\ID3Global\Models\GlobalIndiaDrivingLicence
    {
        return isset($this->DrivingLicence) ? $this->DrivingLicence : null;
    }
    /**
     * Set DrivingLicence value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalIndiaDrivingLicence $drivingLicence
     * @return \ID3Global\Models\GlobalIndia
     */
    public function setDrivingLicence(?\ID3Global\Models\GlobalIndiaDrivingLicence $drivingLicence = null): self
    {
        if (is_null($drivingLicence) || (is_array($drivingLicence) && empty($drivingLicence))) {
            unset($this->DrivingLicence);
        } else {
            $this->DrivingLicence = $drivingLicence;
        }
        
        return $this;
    }
    /**
     * Get Epic value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalIndiaEpic|null
     */
    public function getEpic(): ?\ID3Global\Models\GlobalIndiaEpic
    {
        return isset($this->Epic) ? $this->Epic : null;
    }
    /**
     * Set Epic value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalIndiaEpic $epic
     * @return \ID3Global\Models\GlobalIndia
     */
    public function setEpic(?\ID3Global\Models\GlobalIndiaEpic $epic = null): self
    {
        if (is_null($epic) || (is_array($epic) && empty($epic))) {
            unset($this->Epic);
        } else {
            $this->Epic = $epic;
        }
        
        return $this;
    }
}
