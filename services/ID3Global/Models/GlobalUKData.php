<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalUKData Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q367:GlobalUKData
 * @subpackage Structs
 */
class GlobalUKData extends AbstractStructBase
{
    /**
     * The Passport
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalUKPassport|null
     */
    protected ?\ID3Global\Models\GlobalUKPassport $Passport = null;
    /**
     * The DrivingLicence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalUKDrivingLicence|null
     */
    protected ?\ID3Global\Models\GlobalUKDrivingLicence $DrivingLicence = null;
    /**
     * The NationalInsuranceNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalUKNationalInsuranceNumber|null
     */
    protected ?\ID3Global\Models\GlobalUKNationalInsuranceNumber $NationalInsuranceNumber = null;
    /**
     * Constructor method for GlobalUKData
     * @uses GlobalUKData::setPassport()
     * @uses GlobalUKData::setDrivingLicence()
     * @uses GlobalUKData::setNationalInsuranceNumber()
     * @param \ID3Global\Models\GlobalUKPassport $passport
     * @param \ID3Global\Models\GlobalUKDrivingLicence $drivingLicence
     * @param \ID3Global\Models\GlobalUKNationalInsuranceNumber $nationalInsuranceNumber
     */
    public function __construct(?\ID3Global\Models\GlobalUKPassport $passport = null, ?\ID3Global\Models\GlobalUKDrivingLicence $drivingLicence = null, ?\ID3Global\Models\GlobalUKNationalInsuranceNumber $nationalInsuranceNumber = null)
    {
        $this
            ->setPassport($passport)
            ->setDrivingLicence($drivingLicence)
            ->setNationalInsuranceNumber($nationalInsuranceNumber);
    }
    /**
     * Get Passport value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalUKPassport|null
     */
    public function getPassport(): ?\ID3Global\Models\GlobalUKPassport
    {
        return isset($this->Passport) ? $this->Passport : null;
    }
    /**
     * Set Passport value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalUKPassport $passport
     * @return \ID3Global\Models\GlobalUKData
     */
    public function setPassport(?\ID3Global\Models\GlobalUKPassport $passport = null): self
    {
        if (is_null($passport) || (is_array($passport) && empty($passport))) {
            unset($this->Passport);
        } else {
            $this->Passport = $passport;
        }
        
        return $this;
    }
    /**
     * Get DrivingLicence value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalUKDrivingLicence|null
     */
    public function getDrivingLicence(): ?\ID3Global\Models\GlobalUKDrivingLicence
    {
        return isset($this->DrivingLicence) ? $this->DrivingLicence : null;
    }
    /**
     * Set DrivingLicence value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalUKDrivingLicence $drivingLicence
     * @return \ID3Global\Models\GlobalUKData
     */
    public function setDrivingLicence(?\ID3Global\Models\GlobalUKDrivingLicence $drivingLicence = null): self
    {
        if (is_null($drivingLicence) || (is_array($drivingLicence) && empty($drivingLicence))) {
            unset($this->DrivingLicence);
        } else {
            $this->DrivingLicence = $drivingLicence;
        }
        
        return $this;
    }
    /**
     * Get NationalInsuranceNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalUKNationalInsuranceNumber|null
     */
    public function getNationalInsuranceNumber(): ?\ID3Global\Models\GlobalUKNationalInsuranceNumber
    {
        return isset($this->NationalInsuranceNumber) ? $this->NationalInsuranceNumber : null;
    }
    /**
     * Set NationalInsuranceNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalUKNationalInsuranceNumber $nationalInsuranceNumber
     * @return \ID3Global\Models\GlobalUKData
     */
    public function setNationalInsuranceNumber(?\ID3Global\Models\GlobalUKNationalInsuranceNumber $nationalInsuranceNumber = null): self
    {
        if (is_null($nationalInsuranceNumber) || (is_array($nationalInsuranceNumber) && empty($nationalInsuranceNumber))) {
            unset($this->NationalInsuranceNumber);
        } else {
            $this->NationalInsuranceNumber = $nationalInsuranceNumber;
        }
        
        return $this;
    }
}
