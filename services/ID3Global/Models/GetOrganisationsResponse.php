<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetOrganisationsResponse Models
 * @subpackage Structs
 */
class GetOrganisationsResponse extends AbstractStructBase
{
    /**
     * The GetOrganisationsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalOrganisations|null
     */
    protected ?\ID3Global\Models\GlobalOrganisations $GetOrganisationsResult = null;
    /**
     * Constructor method for GetOrganisationsResponse
     * @uses GetOrganisationsResponse::setGetOrganisationsResult()
     * @param \ID3Global\Models\GlobalOrganisations $getOrganisationsResult
     */
    public function __construct(?\ID3Global\Models\GlobalOrganisations $getOrganisationsResult = null)
    {
        $this
            ->setGetOrganisationsResult($getOrganisationsResult);
    }
    /**
     * Get GetOrganisationsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalOrganisations|null
     */
    public function getGetOrganisationsResult(): ?\ID3Global\Models\GlobalOrganisations
    {
        return isset($this->GetOrganisationsResult) ? $this->GetOrganisationsResult : null;
    }
    /**
     * Set GetOrganisationsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalOrganisations $getOrganisationsResult
     * @return \ID3Global\Models\GetOrganisationsResponse
     */
    public function setGetOrganisationsResult(?\ID3Global\Models\GlobalOrganisations $getOrganisationsResult = null): self
    {
        if (is_null($getOrganisationsResult) || (is_array($getOrganisationsResult) && empty($getOrganisationsResult))) {
            unset($this->GetOrganisationsResult);
        } else {
            $this->GetOrganisationsResult = $getOrganisationsResult;
        }
        
        return $this;
    }
}
