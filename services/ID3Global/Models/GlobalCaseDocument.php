<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseDocument Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q838:GlobalCaseDocument
 * @subpackage Structs
 */
class GlobalCaseDocument extends AbstractStructBase
{
    /**
     * The DocumentID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $DocumentID = null;
    /**
     * The DocumentImageID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $DocumentImageID = null;
    /**
     * The Report
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseDocumentReport|null
     */
    protected ?\ID3Global\Models\GlobalCaseDocumentReport $Report = null;
    /**
     * The Selected
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Selected = null;
    /**
     * The Type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Type = null;
    /**
     * The Category
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Category = null;
    /**
     * The Created
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Created = null;
    /**
     * The Deleted
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Deleted = null;
    /**
     * The InputData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseDocumentData|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseDocumentData $InputData = null;
    /**
     * The DocumentImageVersions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfguid|null
     */
    protected ?\ID3Global\Arrays\ArrayOfguid $DocumentImageVersions = null;
    /**
     * Constructor method for GlobalCaseDocument
     * @uses GlobalCaseDocument::setDocumentID()
     * @uses GlobalCaseDocument::setDocumentImageID()
     * @uses GlobalCaseDocument::setReport()
     * @uses GlobalCaseDocument::setSelected()
     * @uses GlobalCaseDocument::setType()
     * @uses GlobalCaseDocument::setCategory()
     * @uses GlobalCaseDocument::setCreated()
     * @uses GlobalCaseDocument::setDeleted()
     * @uses GlobalCaseDocument::setInputData()
     * @uses GlobalCaseDocument::setDocumentImageVersions()
     * @param string $documentID
     * @param string $documentImageID
     * @param \ID3Global\Models\GlobalCaseDocumentReport $report
     * @param bool $selected
     * @param int $type
     * @param int $category
     * @param string $created
     * @param string $deleted
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseDocumentData $inputData
     * @param \ID3Global\Arrays\ArrayOfguid $documentImageVersions
     */
    public function __construct(?string $documentID = null, ?string $documentImageID = null, ?\ID3Global\Models\GlobalCaseDocumentReport $report = null, ?bool $selected = null, ?int $type = null, ?int $category = null, ?string $created = null, ?string $deleted = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseDocumentData $inputData = null, ?\ID3Global\Arrays\ArrayOfguid $documentImageVersions = null)
    {
        $this
            ->setDocumentID($documentID)
            ->setDocumentImageID($documentImageID)
            ->setReport($report)
            ->setSelected($selected)
            ->setType($type)
            ->setCategory($category)
            ->setCreated($created)
            ->setDeleted($deleted)
            ->setInputData($inputData)
            ->setDocumentImageVersions($documentImageVersions);
    }
    /**
     * Get DocumentID value
     * @return string|null
     */
    public function getDocumentID(): ?string
    {
        return $this->DocumentID;
    }
    /**
     * Set DocumentID value
     * @param string $documentID
     * @return \ID3Global\Models\GlobalCaseDocument
     */
    public function setDocumentID(?string $documentID = null): self
    {
        // validation for constraint: string
        if (!is_null($documentID) && !is_string($documentID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentID, true), gettype($documentID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($documentID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $documentID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($documentID, true)), __LINE__);
        }
        $this->DocumentID = $documentID;
        
        return $this;
    }
    /**
     * Get DocumentImageID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDocumentImageID(): ?string
    {
        return isset($this->DocumentImageID) ? $this->DocumentImageID : null;
    }
    /**
     * Set DocumentImageID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $documentImageID
     * @return \ID3Global\Models\GlobalCaseDocument
     */
    public function setDocumentImageID(?string $documentImageID = null): self
    {
        // validation for constraint: string
        if (!is_null($documentImageID) && !is_string($documentImageID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentImageID, true), gettype($documentImageID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($documentImageID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $documentImageID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($documentImageID, true)), __LINE__);
        }
        if (is_null($documentImageID) || (is_array($documentImageID) && empty($documentImageID))) {
            unset($this->DocumentImageID);
        } else {
            $this->DocumentImageID = $documentImageID;
        }
        
        return $this;
    }
    /**
     * Get Report value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseDocumentReport|null
     */
    public function getReport(): ?\ID3Global\Models\GlobalCaseDocumentReport
    {
        return isset($this->Report) ? $this->Report : null;
    }
    /**
     * Set Report value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCaseDocumentReport $report
     * @return \ID3Global\Models\GlobalCaseDocument
     */
    public function setReport(?\ID3Global\Models\GlobalCaseDocumentReport $report = null): self
    {
        if (is_null($report) || (is_array($report) && empty($report))) {
            unset($this->Report);
        } else {
            $this->Report = $report;
        }
        
        return $this;
    }
    /**
     * Get Selected value
     * @return bool|null
     */
    public function getSelected(): ?bool
    {
        return $this->Selected;
    }
    /**
     * Set Selected value
     * @param bool $selected
     * @return \ID3Global\Models\GlobalCaseDocument
     */
    public function setSelected(?bool $selected = null): self
    {
        // validation for constraint: boolean
        if (!is_null($selected) && !is_bool($selected)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($selected, true), gettype($selected)), __LINE__);
        }
        $this->Selected = $selected;
        
        return $this;
    }
    /**
     * Get Type value
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->Type;
    }
    /**
     * Set Type value
     * @param int $type
     * @return \ID3Global\Models\GlobalCaseDocument
     */
    public function setType(?int $type = null): self
    {
        // validation for constraint: int
        if (!is_null($type) && !(is_int($type) || ctype_digit($type))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->Type = $type;
        
        return $this;
    }
    /**
     * Get Category value
     * @return int|null
     */
    public function getCategory(): ?int
    {
        return $this->Category;
    }
    /**
     * Set Category value
     * @param int $category
     * @return \ID3Global\Models\GlobalCaseDocument
     */
    public function setCategory(?int $category = null): self
    {
        // validation for constraint: int
        if (!is_null($category) && !(is_int($category) || ctype_digit($category))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($category, true), gettype($category)), __LINE__);
        }
        $this->Category = $category;
        
        return $this;
    }
    /**
     * Get Created value
     * @return string|null
     */
    public function getCreated(): ?string
    {
        return $this->Created;
    }
    /**
     * Set Created value
     * @param string $created
     * @return \ID3Global\Models\GlobalCaseDocument
     */
    public function setCreated(?string $created = null): self
    {
        // validation for constraint: string
        if (!is_null($created) && !is_string($created)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($created, true), gettype($created)), __LINE__);
        }
        $this->Created = $created;
        
        return $this;
    }
    /**
     * Get Deleted value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDeleted(): ?string
    {
        return isset($this->Deleted) ? $this->Deleted : null;
    }
    /**
     * Set Deleted value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $deleted
     * @return \ID3Global\Models\GlobalCaseDocument
     */
    public function setDeleted(?string $deleted = null): self
    {
        // validation for constraint: string
        if (!is_null($deleted) && !is_string($deleted)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deleted, true), gettype($deleted)), __LINE__);
        }
        if (is_null($deleted) || (is_array($deleted) && empty($deleted))) {
            unset($this->Deleted);
        } else {
            $this->Deleted = $deleted;
        }
        
        return $this;
    }
    /**
     * Get InputData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDocumentData|null
     */
    public function getInputData(): ?\ID3Global\Arrays\ArrayOfGlobalCaseDocumentData
    {
        return isset($this->InputData) ? $this->InputData : null;
    }
    /**
     * Set InputData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseDocumentData $inputData
     * @return \ID3Global\Models\GlobalCaseDocument
     */
    public function setInputData(?\ID3Global\Arrays\ArrayOfGlobalCaseDocumentData $inputData = null): self
    {
        if (is_null($inputData) || (is_array($inputData) && empty($inputData))) {
            unset($this->InputData);
        } else {
            $this->InputData = $inputData;
        }
        
        return $this;
    }
    /**
     * Get DocumentImageVersions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfguid|null
     */
    public function getDocumentImageVersions(): ?\ID3Global\Arrays\ArrayOfguid
    {
        return isset($this->DocumentImageVersions) ? $this->DocumentImageVersions : null;
    }
    /**
     * Set DocumentImageVersions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfguid $documentImageVersions
     * @return \ID3Global\Models\GlobalCaseDocument
     */
    public function setDocumentImageVersions(?\ID3Global\Arrays\ArrayOfguid $documentImageVersions = null): self
    {
        if (is_null($documentImageVersions) || (is_array($documentImageVersions) && empty($documentImageVersions))) {
            unset($this->DocumentImageVersions);
        } else {
            $this->DocumentImageVersions = $documentImageVersions;
        }
        
        return $this;
    }
}
