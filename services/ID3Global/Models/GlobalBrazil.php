<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalBrazil Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q391:GlobalBrazil
 * @subpackage Structs
 */
class GlobalBrazil extends AbstractStructBase
{
    /**
     * The CPFNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalBrazilCPFNumber|null
     */
    protected ?\ID3Global\Models\GlobalBrazilCPFNumber $CPFNumber = null;
    /**
     * Constructor method for GlobalBrazil
     * @uses GlobalBrazil::setCPFNumber()
     * @param \ID3Global\Models\GlobalBrazilCPFNumber $cPFNumber
     */
    public function __construct(?\ID3Global\Models\GlobalBrazilCPFNumber $cPFNumber = null)
    {
        $this
            ->setCPFNumber($cPFNumber);
    }
    /**
     * Get CPFNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalBrazilCPFNumber|null
     */
    public function getCPFNumber(): ?\ID3Global\Models\GlobalBrazilCPFNumber
    {
        return isset($this->CPFNumber) ? $this->CPFNumber : null;
    }
    /**
     * Set CPFNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalBrazilCPFNumber $cPFNumber
     * @return \ID3Global\Models\GlobalBrazil
     */
    public function setCPFNumber(?\ID3Global\Models\GlobalBrazilCPFNumber $cPFNumber = null): self
    {
        if (is_null($cPFNumber) || (is_array($cPFNumber) && empty($cPFNumber))) {
            unset($this->CPFNumber);
        } else {
            $this->CPFNumber = $cPFNumber;
        }
        
        return $this;
    }
}
