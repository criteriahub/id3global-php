<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetIdentityCardNationalityCountriesResponse Models
 * @subpackage Structs
 */
class GetIdentityCardNationalityCountriesResponse extends AbstractStructBase
{
    /**
     * The GetIdentityCardNationalityCountriesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCountry|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCountry $GetIdentityCardNationalityCountriesResult = null;
    /**
     * Constructor method for GetIdentityCardNationalityCountriesResponse
     * @uses GetIdentityCardNationalityCountriesResponse::setGetIdentityCardNationalityCountriesResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalCountry $getIdentityCardNationalityCountriesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCountry $getIdentityCardNationalityCountriesResult = null)
    {
        $this
            ->setGetIdentityCardNationalityCountriesResult($getIdentityCardNationalityCountriesResult);
    }
    /**
     * Get GetIdentityCardNationalityCountriesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCountry|null
     */
    public function getGetIdentityCardNationalityCountriesResult(): ?\ID3Global\Arrays\ArrayOfGlobalCountry
    {
        return isset($this->GetIdentityCardNationalityCountriesResult) ? $this->GetIdentityCardNationalityCountriesResult : null;
    }
    /**
     * Set GetIdentityCardNationalityCountriesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCountry $getIdentityCardNationalityCountriesResult
     * @return \ID3Global\Models\GetIdentityCardNationalityCountriesResponse
     */
    public function setGetIdentityCardNationalityCountriesResult(?\ID3Global\Arrays\ArrayOfGlobalCountry $getIdentityCardNationalityCountriesResult = null): self
    {
        if (is_null($getIdentityCardNationalityCountriesResult) || (is_array($getIdentityCardNationalityCountriesResult) && empty($getIdentityCardNationalityCountriesResult))) {
            unset($this->GetIdentityCardNationalityCountriesResult);
        } else {
            $this->GetIdentityCardNationalityCountriesResult = $getIdentityCardNationalityCountriesResult;
        }
        
        return $this;
    }
}
