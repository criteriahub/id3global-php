<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseNotification Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q738:GlobalCaseNotification
 * @subpackage Structs
 */
class GlobalCaseNotification extends AbstractStructBase
{
    /**
     * The Id
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $Id = null;
    /**
     * The CaseID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $CaseID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The LastRecipientOrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $LastRecipientOrgID = null;
    /**
     * The Updated
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Updated = null;
    /**
     * The UpdatedByOrgName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $UpdatedByOrgName = null;
    /**
     * The UpdatedByUserName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $UpdatedByUserName = null;
    /**
     * The Jobs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseNotificationJob|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseNotificationJob $Jobs = null;
    /**
     * Constructor method for GlobalCaseNotification
     * @uses GlobalCaseNotification::setId()
     * @uses GlobalCaseNotification::setCaseID()
     * @uses GlobalCaseNotification::setName()
     * @uses GlobalCaseNotification::setLastRecipientOrgID()
     * @uses GlobalCaseNotification::setUpdated()
     * @uses GlobalCaseNotification::setUpdatedByOrgName()
     * @uses GlobalCaseNotification::setUpdatedByUserName()
     * @uses GlobalCaseNotification::setJobs()
     * @param string $id
     * @param string $caseID
     * @param string $name
     * @param string $lastRecipientOrgID
     * @param string $updated
     * @param string $updatedByOrgName
     * @param string $updatedByUserName
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseNotificationJob $jobs
     */
    public function __construct(?string $id = null, ?string $caseID = null, ?string $name = null, ?string $lastRecipientOrgID = null, ?string $updated = null, ?string $updatedByOrgName = null, ?string $updatedByUserName = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseNotificationJob $jobs = null)
    {
        $this
            ->setId($id)
            ->setCaseID($caseID)
            ->setName($name)
            ->setLastRecipientOrgID($lastRecipientOrgID)
            ->setUpdated($updated)
            ->setUpdatedByOrgName($updatedByOrgName)
            ->setUpdatedByUserName($updatedByUserName)
            ->setJobs($jobs);
    }
    /**
     * Get Id value
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->Id;
    }
    /**
     * Set Id value
     * @param string $id
     * @return \ID3Global\Models\GlobalCaseNotification
     */
    public function setId(?string $id = null): self
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($id) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $id)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($id, true)), __LINE__);
        }
        $this->Id = $id;
        
        return $this;
    }
    /**
     * Get CaseID value
     * @return string|null
     */
    public function getCaseID(): ?string
    {
        return $this->CaseID;
    }
    /**
     * Set CaseID value
     * @param string $caseID
     * @return \ID3Global\Models\GlobalCaseNotification
     */
    public function setCaseID(?string $caseID = null): self
    {
        // validation for constraint: string
        if (!is_null($caseID) && !is_string($caseID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($caseID, true), gettype($caseID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($caseID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $caseID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($caseID, true)), __LINE__);
        }
        $this->CaseID = $caseID;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalCaseNotification
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get LastRecipientOrgID value
     * @return string|null
     */
    public function getLastRecipientOrgID(): ?string
    {
        return $this->LastRecipientOrgID;
    }
    /**
     * Set LastRecipientOrgID value
     * @param string $lastRecipientOrgID
     * @return \ID3Global\Models\GlobalCaseNotification
     */
    public function setLastRecipientOrgID(?string $lastRecipientOrgID = null): self
    {
        // validation for constraint: string
        if (!is_null($lastRecipientOrgID) && !is_string($lastRecipientOrgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastRecipientOrgID, true), gettype($lastRecipientOrgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($lastRecipientOrgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $lastRecipientOrgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($lastRecipientOrgID, true)), __LINE__);
        }
        $this->LastRecipientOrgID = $lastRecipientOrgID;
        
        return $this;
    }
    /**
     * Get Updated value
     * @return string|null
     */
    public function getUpdated(): ?string
    {
        return $this->Updated;
    }
    /**
     * Set Updated value
     * @param string $updated
     * @return \ID3Global\Models\GlobalCaseNotification
     */
    public function setUpdated(?string $updated = null): self
    {
        // validation for constraint: string
        if (!is_null($updated) && !is_string($updated)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updated, true), gettype($updated)), __LINE__);
        }
        $this->Updated = $updated;
        
        return $this;
    }
    /**
     * Get UpdatedByOrgName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUpdatedByOrgName(): ?string
    {
        return isset($this->UpdatedByOrgName) ? $this->UpdatedByOrgName : null;
    }
    /**
     * Set UpdatedByOrgName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $updatedByOrgName
     * @return \ID3Global\Models\GlobalCaseNotification
     */
    public function setUpdatedByOrgName(?string $updatedByOrgName = null): self
    {
        // validation for constraint: string
        if (!is_null($updatedByOrgName) && !is_string($updatedByOrgName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updatedByOrgName, true), gettype($updatedByOrgName)), __LINE__);
        }
        if (is_null($updatedByOrgName) || (is_array($updatedByOrgName) && empty($updatedByOrgName))) {
            unset($this->UpdatedByOrgName);
        } else {
            $this->UpdatedByOrgName = $updatedByOrgName;
        }
        
        return $this;
    }
    /**
     * Get UpdatedByUserName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUpdatedByUserName(): ?string
    {
        return isset($this->UpdatedByUserName) ? $this->UpdatedByUserName : null;
    }
    /**
     * Set UpdatedByUserName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $updatedByUserName
     * @return \ID3Global\Models\GlobalCaseNotification
     */
    public function setUpdatedByUserName(?string $updatedByUserName = null): self
    {
        // validation for constraint: string
        if (!is_null($updatedByUserName) && !is_string($updatedByUserName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updatedByUserName, true), gettype($updatedByUserName)), __LINE__);
        }
        if (is_null($updatedByUserName) || (is_array($updatedByUserName) && empty($updatedByUserName))) {
            unset($this->UpdatedByUserName);
        } else {
            $this->UpdatedByUserName = $updatedByUserName;
        }
        
        return $this;
    }
    /**
     * Get Jobs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseNotificationJob|null
     */
    public function getJobs(): ?\ID3Global\Arrays\ArrayOfGlobalCaseNotificationJob
    {
        return isset($this->Jobs) ? $this->Jobs : null;
    }
    /**
     * Set Jobs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseNotificationJob $jobs
     * @return \ID3Global\Models\GlobalCaseNotification
     */
    public function setJobs(?\ID3Global\Arrays\ArrayOfGlobalCaseNotificationJob $jobs = null): self
    {
        if (is_null($jobs) || (is_array($jobs) && empty($jobs))) {
            unset($this->Jobs);
        } else {
            $this->Jobs = $jobs;
        }
        
        return $this;
    }
}
