<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalLocation Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q431:GlobalLocation
 * @subpackage Structs
 */
class GlobalLocation extends AbstractStructBase
{
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Country = null;
    /**
     * The State
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $State = null;
    /**
     * The BlackBoxID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $BlackBoxID = null;
    /**
     * The IndividualID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $IndividualID = null;
    /**
     * The AcceptHeaders
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AcceptHeaders = null;
    /**
     * The UserAgent
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $UserAgent = null;
    /**
     * The IPAddress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $IPAddress = null;
    /**
     * Constructor method for GlobalLocation
     * @uses GlobalLocation::setCountry()
     * @uses GlobalLocation::setState()
     * @uses GlobalLocation::setBlackBoxID()
     * @uses GlobalLocation::setIndividualID()
     * @uses GlobalLocation::setAcceptHeaders()
     * @uses GlobalLocation::setUserAgent()
     * @uses GlobalLocation::setIPAddress()
     * @param string $country
     * @param string $state
     * @param string $blackBoxID
     * @param string $individualID
     * @param string $acceptHeaders
     * @param string $userAgent
     * @param string $iPAddress
     */
    public function __construct(?string $country = null, ?string $state = null, ?string $blackBoxID = null, ?string $individualID = null, ?string $acceptHeaders = null, ?string $userAgent = null, ?string $iPAddress = null)
    {
        $this
            ->setCountry($country)
            ->setState($state)
            ->setBlackBoxID($blackBoxID)
            ->setIndividualID($individualID)
            ->setAcceptHeaders($acceptHeaders)
            ->setUserAgent($userAgent)
            ->setIPAddress($iPAddress);
    }
    /**
     * Get Country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return isset($this->Country) ? $this->Country : null;
    }
    /**
     * Set Country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $country
     * @return \ID3Global\Models\GlobalLocation
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->Country);
        } else {
            $this->Country = $country;
        }
        
        return $this;
    }
    /**
     * Get State value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getState(): ?string
    {
        return isset($this->State) ? $this->State : null;
    }
    /**
     * Set State value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $state
     * @return \ID3Global\Models\GlobalLocation
     */
    public function setState(?string $state = null): self
    {
        // validation for constraint: string
        if (!is_null($state) && !is_string($state)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($state, true), gettype($state)), __LINE__);
        }
        if (is_null($state) || (is_array($state) && empty($state))) {
            unset($this->State);
        } else {
            $this->State = $state;
        }
        
        return $this;
    }
    /**
     * Get BlackBoxID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBlackBoxID(): ?string
    {
        return isset($this->BlackBoxID) ? $this->BlackBoxID : null;
    }
    /**
     * Set BlackBoxID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $blackBoxID
     * @return \ID3Global\Models\GlobalLocation
     */
    public function setBlackBoxID(?string $blackBoxID = null): self
    {
        // validation for constraint: string
        if (!is_null($blackBoxID) && !is_string($blackBoxID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($blackBoxID, true), gettype($blackBoxID)), __LINE__);
        }
        if (is_null($blackBoxID) || (is_array($blackBoxID) && empty($blackBoxID))) {
            unset($this->BlackBoxID);
        } else {
            $this->BlackBoxID = $blackBoxID;
        }
        
        return $this;
    }
    /**
     * Get IndividualID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIndividualID(): ?string
    {
        return isset($this->IndividualID) ? $this->IndividualID : null;
    }
    /**
     * Set IndividualID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $individualID
     * @return \ID3Global\Models\GlobalLocation
     */
    public function setIndividualID(?string $individualID = null): self
    {
        // validation for constraint: string
        if (!is_null($individualID) && !is_string($individualID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($individualID, true), gettype($individualID)), __LINE__);
        }
        if (is_null($individualID) || (is_array($individualID) && empty($individualID))) {
            unset($this->IndividualID);
        } else {
            $this->IndividualID = $individualID;
        }
        
        return $this;
    }
    /**
     * Get AcceptHeaders value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAcceptHeaders(): ?string
    {
        return isset($this->AcceptHeaders) ? $this->AcceptHeaders : null;
    }
    /**
     * Set AcceptHeaders value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $acceptHeaders
     * @return \ID3Global\Models\GlobalLocation
     */
    public function setAcceptHeaders(?string $acceptHeaders = null): self
    {
        // validation for constraint: string
        if (!is_null($acceptHeaders) && !is_string($acceptHeaders)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($acceptHeaders, true), gettype($acceptHeaders)), __LINE__);
        }
        if (is_null($acceptHeaders) || (is_array($acceptHeaders) && empty($acceptHeaders))) {
            unset($this->AcceptHeaders);
        } else {
            $this->AcceptHeaders = $acceptHeaders;
        }
        
        return $this;
    }
    /**
     * Get UserAgent value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUserAgent(): ?string
    {
        return isset($this->UserAgent) ? $this->UserAgent : null;
    }
    /**
     * Set UserAgent value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $userAgent
     * @return \ID3Global\Models\GlobalLocation
     */
    public function setUserAgent(?string $userAgent = null): self
    {
        // validation for constraint: string
        if (!is_null($userAgent) && !is_string($userAgent)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userAgent, true), gettype($userAgent)), __LINE__);
        }
        if (is_null($userAgent) || (is_array($userAgent) && empty($userAgent))) {
            unset($this->UserAgent);
        } else {
            $this->UserAgent = $userAgent;
        }
        
        return $this;
    }
    /**
     * Get IPAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIPAddress(): ?string
    {
        return isset($this->IPAddress) ? $this->IPAddress : null;
    }
    /**
     * Set IPAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $iPAddress
     * @return \ID3Global\Models\GlobalLocation
     */
    public function setIPAddress(?string $iPAddress = null): self
    {
        // validation for constraint: string
        if (!is_null($iPAddress) && !is_string($iPAddress)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iPAddress, true), gettype($iPAddress)), __LINE__);
        }
        if (is_null($iPAddress) || (is_array($iPAddress) && empty($iPAddress))) {
            unset($this->IPAddress);
        } else {
            $this->IPAddress = $iPAddress;
        }
        
        return $this;
    }
}
