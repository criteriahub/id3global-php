<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AuthenticateMP Models
 * @subpackage Structs
 */
class AuthenticateMP extends AbstractStructBase
{
    /**
     * The ProfileIDVersions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalProfileIDVersion|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalProfileIDVersion $ProfileIDVersions = null;
    /**
     * The CustomerReference
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CustomerReference = null;
    /**
     * The InputData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalInputData|null
     */
    protected ?\ID3Global\Models\GlobalInputData $InputData = null;
    /**
     * Constructor method for AuthenticateMP
     * @uses AuthenticateMP::setProfileIDVersions()
     * @uses AuthenticateMP::setCustomerReference()
     * @uses AuthenticateMP::setInputData()
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileIDVersion $profileIDVersions
     * @param string $customerReference
     * @param \ID3Global\Models\GlobalInputData $inputData
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalProfileIDVersion $profileIDVersions = null, ?string $customerReference = null, ?\ID3Global\Models\GlobalInputData $inputData = null)
    {
        $this
            ->setProfileIDVersions($profileIDVersions)
            ->setCustomerReference($customerReference)
            ->setInputData($inputData);
    }
    /**
     * Get ProfileIDVersions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileIDVersion|null
     */
    public function getProfileIDVersions(): ?\ID3Global\Arrays\ArrayOfGlobalProfileIDVersion
    {
        return isset($this->ProfileIDVersions) ? $this->ProfileIDVersions : null;
    }
    /**
     * Set ProfileIDVersions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileIDVersion $profileIDVersions
     * @return \ID3Global\Models\AuthenticateMP
     */
    public function setProfileIDVersions(?\ID3Global\Arrays\ArrayOfGlobalProfileIDVersion $profileIDVersions = null): self
    {
        if (is_null($profileIDVersions) || (is_array($profileIDVersions) && empty($profileIDVersions))) {
            unset($this->ProfileIDVersions);
        } else {
            $this->ProfileIDVersions = $profileIDVersions;
        }
        
        return $this;
    }
    /**
     * Get CustomerReference value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCustomerReference(): ?string
    {
        return isset($this->CustomerReference) ? $this->CustomerReference : null;
    }
    /**
     * Set CustomerReference value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $customerReference
     * @return \ID3Global\Models\AuthenticateMP
     */
    public function setCustomerReference(?string $customerReference = null): self
    {
        // validation for constraint: string
        if (!is_null($customerReference) && !is_string($customerReference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($customerReference, true), gettype($customerReference)), __LINE__);
        }
        if (is_null($customerReference) || (is_array($customerReference) && empty($customerReference))) {
            unset($this->CustomerReference);
        } else {
            $this->CustomerReference = $customerReference;
        }
        
        return $this;
    }
    /**
     * Get InputData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalInputData|null
     */
    public function getInputData(): ?\ID3Global\Models\GlobalInputData
    {
        return isset($this->InputData) ? $this->InputData : null;
    }
    /**
     * Set InputData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalInputData $inputData
     * @return \ID3Global\Models\AuthenticateMP
     */
    public function setInputData(?\ID3Global\Models\GlobalInputData $inputData = null): self
    {
        if (is_null($inputData) || (is_array($inputData) && empty($inputData))) {
            unset($this->InputData);
        } else {
            $this->InputData = $inputData;
        }
        
        return $this;
    }
}
