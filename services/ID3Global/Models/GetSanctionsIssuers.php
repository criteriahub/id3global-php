<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSanctionsIssuers Models
 * @subpackage Structs
 */
class GetSanctionsIssuers extends AbstractStructBase
{
}
