<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSouthAfricaCreditSuppliers Models
 * @subpackage Structs
 */
class GetSouthAfricaCreditSuppliers extends AbstractStructBase
{
}
