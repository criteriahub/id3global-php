<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetExtractFormatsResponse Models
 * @subpackage Structs
 */
class GetExtractFormatsResponse extends AbstractStructBase
{
    /**
     * The GetExtractFormatsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $GetExtractFormatsResult = null;
    /**
     * Constructor method for GetExtractFormatsResponse
     * @uses GetExtractFormatsResponse::setGetExtractFormatsResult()
     * @param \ID3Global\Arrays\ArrayOfstring $getExtractFormatsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfstring $getExtractFormatsResult = null)
    {
        $this
            ->setGetExtractFormatsResult($getExtractFormatsResult);
    }
    /**
     * Get GetExtractFormatsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getGetExtractFormatsResult(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->GetExtractFormatsResult) ? $this->GetExtractFormatsResult : null;
    }
    /**
     * Set GetExtractFormatsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $getExtractFormatsResult
     * @return \ID3Global\Models\GetExtractFormatsResponse
     */
    public function setGetExtractFormatsResult(?\ID3Global\Arrays\ArrayOfstring $getExtractFormatsResult = null): self
    {
        if (is_null($getExtractFormatsResult) || (is_array($getExtractFormatsResult) && empty($getExtractFormatsResult))) {
            unset($this->GetExtractFormatsResult);
        } else {
            $this->GetExtractFormatsResult = $getExtractFormatsResult;
        }
        
        return $this;
    }
}
