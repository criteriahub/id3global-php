<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetRoleMembersResponse Models
 * @subpackage Structs
 */
class GetRoleMembersResponse extends AbstractStructBase
{
    /**
     * The GetRoleMembersResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalRoleMembers|null
     */
    protected ?\ID3Global\Models\GlobalRoleMembers $GetRoleMembersResult = null;
    /**
     * Constructor method for GetRoleMembersResponse
     * @uses GetRoleMembersResponse::setGetRoleMembersResult()
     * @param \ID3Global\Models\GlobalRoleMembers $getRoleMembersResult
     */
    public function __construct(?\ID3Global\Models\GlobalRoleMembers $getRoleMembersResult = null)
    {
        $this
            ->setGetRoleMembersResult($getRoleMembersResult);
    }
    /**
     * Get GetRoleMembersResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalRoleMembers|null
     */
    public function getGetRoleMembersResult(): ?\ID3Global\Models\GlobalRoleMembers
    {
        return isset($this->GetRoleMembersResult) ? $this->GetRoleMembersResult : null;
    }
    /**
     * Set GetRoleMembersResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalRoleMembers $getRoleMembersResult
     * @return \ID3Global\Models\GetRoleMembersResponse
     */
    public function setGetRoleMembersResult(?\ID3Global\Models\GlobalRoleMembers $getRoleMembersResult = null): self
    {
        if (is_null($getRoleMembersResult) || (is_array($getRoleMembersResult) && empty($getRoleMembersResult))) {
            unset($this->GetRoleMembersResult);
        } else {
            $this->GetRoleMembersResult = $getRoleMembersResult;
        }
        
        return $this;
    }
}
