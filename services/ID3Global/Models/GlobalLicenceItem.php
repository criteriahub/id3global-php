<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalLicenceItem Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q275:GlobalLicenceItem
 * @subpackage Structs
 */
class GlobalLicenceItem extends AbstractStructBase
{
    /**
     * The ID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Description = null;
    /**
     * The Enabled
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Enabled = null;
    /**
     * The Pending
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Pending = null;
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Country = null;
    /**
     * The Suspended
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Suspended = null;
    /**
     * The ItemCheckType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ItemCheckType = null;
    /**
     * Constructor method for GlobalLicenceItem
     * @uses GlobalLicenceItem::setID()
     * @uses GlobalLicenceItem::setName()
     * @uses GlobalLicenceItem::setDescription()
     * @uses GlobalLicenceItem::setEnabled()
     * @uses GlobalLicenceItem::setPending()
     * @uses GlobalLicenceItem::setCountry()
     * @uses GlobalLicenceItem::setSuspended()
     * @uses GlobalLicenceItem::setItemCheckType()
     * @param int $iD
     * @param string $name
     * @param string $description
     * @param bool $enabled
     * @param bool $pending
     * @param string $country
     * @param bool $suspended
     * @param string $itemCheckType
     */
    public function __construct(?int $iD = null, ?string $name = null, ?string $description = null, ?bool $enabled = null, ?bool $pending = null, ?string $country = null, ?bool $suspended = null, ?string $itemCheckType = null)
    {
        $this
            ->setID($iD)
            ->setName($name)
            ->setDescription($description)
            ->setEnabled($enabled)
            ->setPending($pending)
            ->setCountry($country)
            ->setSuspended($suspended)
            ->setItemCheckType($itemCheckType);
    }
    /**
     * Get ID value
     * @return int|null
     */
    public function getID(): ?int
    {
        return $this->ID;
    }
    /**
     * Set ID value
     * @param int $iD
     * @return \ID3Global\Models\GlobalLicenceItem
     */
    public function setID(?int $iD = null): self
    {
        // validation for constraint: int
        if (!is_null($iD) && !(is_int($iD) || ctype_digit($iD))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($iD, true), gettype($iD)), __LINE__);
        }
        $this->ID = $iD;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalLicenceItem
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \ID3Global\Models\GlobalLicenceItem
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        
        return $this;
    }
    /**
     * Get Enabled value
     * @return bool|null
     */
    public function getEnabled(): ?bool
    {
        return $this->Enabled;
    }
    /**
     * Set Enabled value
     * @param bool $enabled
     * @return \ID3Global\Models\GlobalLicenceItem
     */
    public function setEnabled(?bool $enabled = null): self
    {
        // validation for constraint: boolean
        if (!is_null($enabled) && !is_bool($enabled)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($enabled, true), gettype($enabled)), __LINE__);
        }
        $this->Enabled = $enabled;
        
        return $this;
    }
    /**
     * Get Pending value
     * @return bool|null
     */
    public function getPending(): ?bool
    {
        return $this->Pending;
    }
    /**
     * Set Pending value
     * @param bool $pending
     * @return \ID3Global\Models\GlobalLicenceItem
     */
    public function setPending(?bool $pending = null): self
    {
        // validation for constraint: boolean
        if (!is_null($pending) && !is_bool($pending)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($pending, true), gettype($pending)), __LINE__);
        }
        $this->Pending = $pending;
        
        return $this;
    }
    /**
     * Get Country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return isset($this->Country) ? $this->Country : null;
    }
    /**
     * Set Country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $country
     * @return \ID3Global\Models\GlobalLicenceItem
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->Country);
        } else {
            $this->Country = $country;
        }
        
        return $this;
    }
    /**
     * Get Suspended value
     * @return bool|null
     */
    public function getSuspended(): ?bool
    {
        return $this->Suspended;
    }
    /**
     * Set Suspended value
     * @param bool $suspended
     * @return \ID3Global\Models\GlobalLicenceItem
     */
    public function setSuspended(?bool $suspended = null): self
    {
        // validation for constraint: boolean
        if (!is_null($suspended) && !is_bool($suspended)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($suspended, true), gettype($suspended)), __LINE__);
        }
        $this->Suspended = $suspended;
        
        return $this;
    }
    /**
     * Get ItemCheckType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getItemCheckType(): ?string
    {
        return isset($this->ItemCheckType) ? $this->ItemCheckType : null;
    }
    /**
     * Set ItemCheckType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $itemCheckType
     * @return \ID3Global\Models\GlobalLicenceItem
     */
    public function setItemCheckType(?string $itemCheckType = null): self
    {
        // validation for constraint: string
        if (!is_null($itemCheckType) && !is_string($itemCheckType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($itemCheckType, true), gettype($itemCheckType)), __LINE__);
        }
        if (is_null($itemCheckType) || (is_array($itemCheckType) && empty($itemCheckType))) {
            unset($this->ItemCheckType);
        } else {
            $this->ItemCheckType = $itemCheckType;
        }
        
        return $this;
    }
}
