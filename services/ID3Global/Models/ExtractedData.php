<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ExtractedData Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q130:ExtractedData
 * @subpackage Structs
 */
class ExtractedData extends AbstractStructBase
{
    /**
     * The Forename
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Forename = null;
    /**
     * The Surname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Surname = null;
    /**
     * The Gender
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Gender = null;
    /**
     * The DOB
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DOB = null;
    /**
     * The Nationality
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Nationality = null;
    /**
     * The Address
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\AddressDetails|null
     */
    protected ?\ID3Global\Models\AddressDetails $Address = null;
    /**
     * The IssueDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $IssueDate = null;
    /**
     * The Expiry
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Expiry = null;
    /**
     * The MRZ
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MRZ = null;
    /**
     * The AdditionalData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AdditionalData = null;
    /**
     * The FormattedAddress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAddress|null
     */
    protected ?\ID3Global\Models\GlobalAddress $FormattedAddress = null;
    /**
     * The PlaceOfBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $PlaceOfBirth = null;
    /**
     * Constructor method for ExtractedData
     * @uses ExtractedData::setForename()
     * @uses ExtractedData::setSurname()
     * @uses ExtractedData::setGender()
     * @uses ExtractedData::setDOB()
     * @uses ExtractedData::setNationality()
     * @uses ExtractedData::setAddress()
     * @uses ExtractedData::setIssueDate()
     * @uses ExtractedData::setExpiry()
     * @uses ExtractedData::setMRZ()
     * @uses ExtractedData::setAdditionalData()
     * @uses ExtractedData::setFormattedAddress()
     * @uses ExtractedData::setPlaceOfBirth()
     * @param string $forename
     * @param string $surname
     * @param string $gender
     * @param string $dOB
     * @param string $nationality
     * @param \ID3Global\Models\AddressDetails $address
     * @param string $issueDate
     * @param string $expiry
     * @param string $mRZ
     * @param string $additionalData
     * @param \ID3Global\Models\GlobalAddress $formattedAddress
     * @param string $placeOfBirth
     */
    public function __construct(?string $forename = null, ?string $surname = null, ?string $gender = null, ?string $dOB = null, ?string $nationality = null, ?\ID3Global\Models\AddressDetails $address = null, ?string $issueDate = null, ?string $expiry = null, ?string $mRZ = null, ?string $additionalData = null, ?\ID3Global\Models\GlobalAddress $formattedAddress = null, ?string $placeOfBirth = null)
    {
        $this
            ->setForename($forename)
            ->setSurname($surname)
            ->setGender($gender)
            ->setDOB($dOB)
            ->setNationality($nationality)
            ->setAddress($address)
            ->setIssueDate($issueDate)
            ->setExpiry($expiry)
            ->setMRZ($mRZ)
            ->setAdditionalData($additionalData)
            ->setFormattedAddress($formattedAddress)
            ->setPlaceOfBirth($placeOfBirth);
    }
    /**
     * Get Forename value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getForename(): ?string
    {
        return isset($this->Forename) ? $this->Forename : null;
    }
    /**
     * Set Forename value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $forename
     * @return \ID3Global\Models\ExtractedData
     */
    public function setForename(?string $forename = null): self
    {
        // validation for constraint: string
        if (!is_null($forename) && !is_string($forename)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($forename, true), gettype($forename)), __LINE__);
        }
        if (is_null($forename) || (is_array($forename) && empty($forename))) {
            unset($this->Forename);
        } else {
            $this->Forename = $forename;
        }
        
        return $this;
    }
    /**
     * Get Surname value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return isset($this->Surname) ? $this->Surname : null;
    }
    /**
     * Set Surname value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $surname
     * @return \ID3Global\Models\ExtractedData
     */
    public function setSurname(?string $surname = null): self
    {
        // validation for constraint: string
        if (!is_null($surname) && !is_string($surname)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($surname, true), gettype($surname)), __LINE__);
        }
        if (is_null($surname) || (is_array($surname) && empty($surname))) {
            unset($this->Surname);
        } else {
            $this->Surname = $surname;
        }
        
        return $this;
    }
    /**
     * Get Gender value
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->Gender;
    }
    /**
     * Set Gender value
     * @uses \ID3Global\Enums\GlobalGender::valueIsValid()
     * @uses \ID3Global\Enums\GlobalGender::getValidValues()
     * @throws InvalidArgumentException
     * @param string $gender
     * @return \ID3Global\Models\ExtractedData
     */
    public function setGender(?string $gender = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalGender::valueIsValid($gender)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalGender', is_array($gender) ? implode(', ', $gender) : var_export($gender, true), implode(', ', \ID3Global\Enums\GlobalGender::getValidValues())), __LINE__);
        }
        $this->Gender = $gender;
        
        return $this;
    }
    /**
     * Get DOB value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDOB(): ?string
    {
        return isset($this->DOB) ? $this->DOB : null;
    }
    /**
     * Set DOB value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dOB
     * @return \ID3Global\Models\ExtractedData
     */
    public function setDOB(?string $dOB = null): self
    {
        // validation for constraint: string
        if (!is_null($dOB) && !is_string($dOB)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dOB, true), gettype($dOB)), __LINE__);
        }
        if (is_null($dOB) || (is_array($dOB) && empty($dOB))) {
            unset($this->DOB);
        } else {
            $this->DOB = $dOB;
        }
        
        return $this;
    }
    /**
     * Get Nationality value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNationality(): ?string
    {
        return isset($this->Nationality) ? $this->Nationality : null;
    }
    /**
     * Set Nationality value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $nationality
     * @return \ID3Global\Models\ExtractedData
     */
    public function setNationality(?string $nationality = null): self
    {
        // validation for constraint: string
        if (!is_null($nationality) && !is_string($nationality)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nationality, true), gettype($nationality)), __LINE__);
        }
        if (is_null($nationality) || (is_array($nationality) && empty($nationality))) {
            unset($this->Nationality);
        } else {
            $this->Nationality = $nationality;
        }
        
        return $this;
    }
    /**
     * Get Address value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\AddressDetails|null
     */
    public function getAddress(): ?\ID3Global\Models\AddressDetails
    {
        return isset($this->Address) ? $this->Address : null;
    }
    /**
     * Set Address value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\AddressDetails $address
     * @return \ID3Global\Models\ExtractedData
     */
    public function setAddress(?\ID3Global\Models\AddressDetails $address = null): self
    {
        if (is_null($address) || (is_array($address) && empty($address))) {
            unset($this->Address);
        } else {
            $this->Address = $address;
        }
        
        return $this;
    }
    /**
     * Get IssueDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIssueDate(): ?string
    {
        return isset($this->IssueDate) ? $this->IssueDate : null;
    }
    /**
     * Set IssueDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $issueDate
     * @return \ID3Global\Models\ExtractedData
     */
    public function setIssueDate(?string $issueDate = null): self
    {
        // validation for constraint: string
        if (!is_null($issueDate) && !is_string($issueDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($issueDate, true), gettype($issueDate)), __LINE__);
        }
        if (is_null($issueDate) || (is_array($issueDate) && empty($issueDate))) {
            unset($this->IssueDate);
        } else {
            $this->IssueDate = $issueDate;
        }
        
        return $this;
    }
    /**
     * Get Expiry value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getExpiry(): ?string
    {
        return isset($this->Expiry) ? $this->Expiry : null;
    }
    /**
     * Set Expiry value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $expiry
     * @return \ID3Global\Models\ExtractedData
     */
    public function setExpiry(?string $expiry = null): self
    {
        // validation for constraint: string
        if (!is_null($expiry) && !is_string($expiry)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expiry, true), gettype($expiry)), __LINE__);
        }
        if (is_null($expiry) || (is_array($expiry) && empty($expiry))) {
            unset($this->Expiry);
        } else {
            $this->Expiry = $expiry;
        }
        
        return $this;
    }
    /**
     * Get MRZ value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMRZ(): ?string
    {
        return isset($this->MRZ) ? $this->MRZ : null;
    }
    /**
     * Set MRZ value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mRZ
     * @return \ID3Global\Models\ExtractedData
     */
    public function setMRZ(?string $mRZ = null): self
    {
        // validation for constraint: string
        if (!is_null($mRZ) && !is_string($mRZ)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mRZ, true), gettype($mRZ)), __LINE__);
        }
        if (is_null($mRZ) || (is_array($mRZ) && empty($mRZ))) {
            unset($this->MRZ);
        } else {
            $this->MRZ = $mRZ;
        }
        
        return $this;
    }
    /**
     * Get AdditionalData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalData(): ?string
    {
        return isset($this->AdditionalData) ? $this->AdditionalData : null;
    }
    /**
     * Set AdditionalData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalData
     * @return \ID3Global\Models\ExtractedData
     */
    public function setAdditionalData(?string $additionalData = null): self
    {
        // validation for constraint: string
        if (!is_null($additionalData) && !is_string($additionalData)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalData, true), gettype($additionalData)), __LINE__);
        }
        if (is_null($additionalData) || (is_array($additionalData) && empty($additionalData))) {
            unset($this->AdditionalData);
        } else {
            $this->AdditionalData = $additionalData;
        }
        
        return $this;
    }
    /**
     * Get FormattedAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAddress|null
     */
    public function getFormattedAddress(): ?\ID3Global\Models\GlobalAddress
    {
        return isset($this->FormattedAddress) ? $this->FormattedAddress : null;
    }
    /**
     * Set FormattedAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAddress $formattedAddress
     * @return \ID3Global\Models\ExtractedData
     */
    public function setFormattedAddress(?\ID3Global\Models\GlobalAddress $formattedAddress = null): self
    {
        if (is_null($formattedAddress) || (is_array($formattedAddress) && empty($formattedAddress))) {
            unset($this->FormattedAddress);
        } else {
            $this->FormattedAddress = $formattedAddress;
        }
        
        return $this;
    }
    /**
     * Get PlaceOfBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPlaceOfBirth(): ?string
    {
        return isset($this->PlaceOfBirth) ? $this->PlaceOfBirth : null;
    }
    /**
     * Set PlaceOfBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $placeOfBirth
     * @return \ID3Global\Models\ExtractedData
     */
    public function setPlaceOfBirth(?string $placeOfBirth = null): self
    {
        // validation for constraint: string
        if (!is_null($placeOfBirth) && !is_string($placeOfBirth)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($placeOfBirth, true), gettype($placeOfBirth)), __LINE__);
        }
        if (is_null($placeOfBirth) || (is_array($placeOfBirth) && empty($placeOfBirth))) {
            unset($this->PlaceOfBirth);
        } else {
            $this->PlaceOfBirth = $placeOfBirth;
        }
        
        return $this;
    }
}
