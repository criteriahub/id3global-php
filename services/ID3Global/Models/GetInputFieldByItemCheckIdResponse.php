<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetInputFieldByItemCheckIdResponse Models
 * @subpackage Structs
 */
class GetInputFieldByItemCheckIdResponse extends AbstractStructBase
{
    /**
     * The GetInputFieldByItemCheckIdResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckConfig|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckConfig $GetInputFieldByItemCheckIdResult = null;
    /**
     * Constructor method for GetInputFieldByItemCheckIdResponse
     * @uses GetInputFieldByItemCheckIdResponse::setGetInputFieldByItemCheckIdResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckConfig $getInputFieldByItemCheckIdResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalItemCheckConfig $getInputFieldByItemCheckIdResult = null)
    {
        $this
            ->setGetInputFieldByItemCheckIdResult($getInputFieldByItemCheckIdResult);
    }
    /**
     * Get GetInputFieldByItemCheckIdResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckConfig|null
     */
    public function getGetInputFieldByItemCheckIdResult(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckConfig
    {
        return isset($this->GetInputFieldByItemCheckIdResult) ? $this->GetInputFieldByItemCheckIdResult : null;
    }
    /**
     * Set GetInputFieldByItemCheckIdResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckConfig $getInputFieldByItemCheckIdResult
     * @return \ID3Global\Models\GetInputFieldByItemCheckIdResponse
     */
    public function setGetInputFieldByItemCheckIdResult(?\ID3Global\Arrays\ArrayOfGlobalItemCheckConfig $getInputFieldByItemCheckIdResult = null): self
    {
        if (is_null($getInputFieldByItemCheckIdResult) || (is_array($getInputFieldByItemCheckIdResult) && empty($getInputFieldByItemCheckIdResult))) {
            unset($this->GetInputFieldByItemCheckIdResult);
        } else {
            $this->GetInputFieldByItemCheckIdResult = $getInputFieldByItemCheckIdResult;
        }
        
        return $this;
    }
}
