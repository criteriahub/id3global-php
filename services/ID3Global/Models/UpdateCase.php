<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateCase Models
 * @subpackage Structs
 */
class UpdateCase extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The CaseID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $CaseID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The State
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $State = null;
    /**
     * The AuthorType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $AuthorType = null;
    /**
     * The Verdict
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Verdict = null;
    /**
     * The DisclaimerAccepted
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool|null
     */
    protected ?bool $DisclaimerAccepted = null;
    /**
     * The ActionContext
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ActionContext = null;
    /**
     * The CustomerReferences
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseReference|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseReference $CustomerReferences = null;
    /**
     * The Properties
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $Properties = null;
    /**
     * The Profiles
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalProfileIDVersion|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalProfileIDVersion $Profiles = null;
    /**
     * The DocumentCategorySubmissionTypes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType $DocumentCategorySubmissionTypes = null;
    /**
     * The Documents
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseDocument|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseDocument $Documents = null;
    /**
     * The Consents
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseConsent|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseConsent $Consents = null;
    /**
     * The InputData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalInputData|null
     */
    protected ?\ID3Global\Models\GlobalInputData $InputData = null;
    /**
     * Constructor method for UpdateCase
     * @uses UpdateCase::setOrgID()
     * @uses UpdateCase::setCaseID()
     * @uses UpdateCase::setName()
     * @uses UpdateCase::setState()
     * @uses UpdateCase::setAuthorType()
     * @uses UpdateCase::setVerdict()
     * @uses UpdateCase::setDisclaimerAccepted()
     * @uses UpdateCase::setActionContext()
     * @uses UpdateCase::setCustomerReferences()
     * @uses UpdateCase::setProperties()
     * @uses UpdateCase::setProfiles()
     * @uses UpdateCase::setDocumentCategorySubmissionTypes()
     * @uses UpdateCase::setDocuments()
     * @uses UpdateCase::setConsents()
     * @uses UpdateCase::setInputData()
     * @param string $orgID
     * @param string $caseID
     * @param string $name
     * @param int $state
     * @param int $authorType
     * @param int $verdict
     * @param bool $disclaimerAccepted
     * @param int $actionContext
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseReference $customerReferences
     * @param \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $properties
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileIDVersion $profiles
     * @param \ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType $documentCategorySubmissionTypes
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseDocument $documents
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseConsent $consents
     * @param \ID3Global\Models\GlobalInputData $inputData
     */
    public function __construct(?string $orgID = null, ?string $caseID = null, ?string $name = null, ?int $state = null, ?int $authorType = null, ?int $verdict = null, ?bool $disclaimerAccepted = null, ?int $actionContext = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseReference $customerReferences = null, ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $properties = null, ?\ID3Global\Arrays\ArrayOfGlobalProfileIDVersion $profiles = null, ?\ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType $documentCategorySubmissionTypes = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseDocument $documents = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseConsent $consents = null, ?\ID3Global\Models\GlobalInputData $inputData = null)
    {
        $this
            ->setOrgID($orgID)
            ->setCaseID($caseID)
            ->setName($name)
            ->setState($state)
            ->setAuthorType($authorType)
            ->setVerdict($verdict)
            ->setDisclaimerAccepted($disclaimerAccepted)
            ->setActionContext($actionContext)
            ->setCustomerReferences($customerReferences)
            ->setProperties($properties)
            ->setProfiles($profiles)
            ->setDocumentCategorySubmissionTypes($documentCategorySubmissionTypes)
            ->setDocuments($documents)
            ->setConsents($consents)
            ->setInputData($inputData);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\UpdateCase
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get CaseID value
     * @return string|null
     */
    public function getCaseID(): ?string
    {
        return $this->CaseID;
    }
    /**
     * Set CaseID value
     * @param string $caseID
     * @return \ID3Global\Models\UpdateCase
     */
    public function setCaseID(?string $caseID = null): self
    {
        // validation for constraint: string
        if (!is_null($caseID) && !is_string($caseID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($caseID, true), gettype($caseID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($caseID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $caseID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($caseID, true)), __LINE__);
        }
        $this->CaseID = $caseID;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\UpdateCase
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get State value
     * @return int|null
     */
    public function getState(): ?int
    {
        return $this->State;
    }
    /**
     * Set State value
     * @param int $state
     * @return \ID3Global\Models\UpdateCase
     */
    public function setState(?int $state = null): self
    {
        // validation for constraint: int
        if (!is_null($state) && !(is_int($state) || ctype_digit($state))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($state, true), gettype($state)), __LINE__);
        }
        $this->State = $state;
        
        return $this;
    }
    /**
     * Get AuthorType value
     * @return int|null
     */
    public function getAuthorType(): ?int
    {
        return $this->AuthorType;
    }
    /**
     * Set AuthorType value
     * @param int $authorType
     * @return \ID3Global\Models\UpdateCase
     */
    public function setAuthorType(?int $authorType = null): self
    {
        // validation for constraint: int
        if (!is_null($authorType) && !(is_int($authorType) || ctype_digit($authorType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($authorType, true), gettype($authorType)), __LINE__);
        }
        $this->AuthorType = $authorType;
        
        return $this;
    }
    /**
     * Get Verdict value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getVerdict(): ?int
    {
        return isset($this->Verdict) ? $this->Verdict : null;
    }
    /**
     * Set Verdict value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $verdict
     * @return \ID3Global\Models\UpdateCase
     */
    public function setVerdict(?int $verdict = null): self
    {
        // validation for constraint: int
        if (!is_null($verdict) && !(is_int($verdict) || ctype_digit($verdict))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($verdict, true), gettype($verdict)), __LINE__);
        }
        if (is_null($verdict) || (is_array($verdict) && empty($verdict))) {
            unset($this->Verdict);
        } else {
            $this->Verdict = $verdict;
        }
        
        return $this;
    }
    /**
     * Get DisclaimerAccepted value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getDisclaimerAccepted(): ?bool
    {
        return isset($this->DisclaimerAccepted) ? $this->DisclaimerAccepted : null;
    }
    /**
     * Set DisclaimerAccepted value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $disclaimerAccepted
     * @return \ID3Global\Models\UpdateCase
     */
    public function setDisclaimerAccepted(?bool $disclaimerAccepted = null): self
    {
        // validation for constraint: boolean
        if (!is_null($disclaimerAccepted) && !is_bool($disclaimerAccepted)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($disclaimerAccepted, true), gettype($disclaimerAccepted)), __LINE__);
        }
        if (is_null($disclaimerAccepted) || (is_array($disclaimerAccepted) && empty($disclaimerAccepted))) {
            unset($this->DisclaimerAccepted);
        } else {
            $this->DisclaimerAccepted = $disclaimerAccepted;
        }
        
        return $this;
    }
    /**
     * Get ActionContext value
     * @return int|null
     */
    public function getActionContext(): ?int
    {
        return $this->ActionContext;
    }
    /**
     * Set ActionContext value
     * @param int $actionContext
     * @return \ID3Global\Models\UpdateCase
     */
    public function setActionContext(?int $actionContext = null): self
    {
        // validation for constraint: int
        if (!is_null($actionContext) && !(is_int($actionContext) || ctype_digit($actionContext))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($actionContext, true), gettype($actionContext)), __LINE__);
        }
        $this->ActionContext = $actionContext;
        
        return $this;
    }
    /**
     * Get CustomerReferences value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseReference|null
     */
    public function getCustomerReferences(): ?\ID3Global\Arrays\ArrayOfGlobalCaseReference
    {
        return isset($this->CustomerReferences) ? $this->CustomerReferences : null;
    }
    /**
     * Set CustomerReferences value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseReference $customerReferences
     * @return \ID3Global\Models\UpdateCase
     */
    public function setCustomerReferences(?\ID3Global\Arrays\ArrayOfGlobalCaseReference $customerReferences = null): self
    {
        if (is_null($customerReferences) || (is_array($customerReferences) && empty($customerReferences))) {
            unset($this->CustomerReferences);
        } else {
            $this->CustomerReferences = $customerReferences;
        }
        
        return $this;
    }
    /**
     * Get Properties value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring|null
     */
    public function getProperties(): ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring
    {
        return isset($this->Properties) ? $this->Properties : null;
    }
    /**
     * Set Properties value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $properties
     * @return \ID3Global\Models\UpdateCase
     */
    public function setProperties(?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $properties = null): self
    {
        if (is_null($properties) || (is_array($properties) && empty($properties))) {
            unset($this->Properties);
        } else {
            $this->Properties = $properties;
        }
        
        return $this;
    }
    /**
     * Get Profiles value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileIDVersion|null
     */
    public function getProfiles(): ?\ID3Global\Arrays\ArrayOfGlobalProfileIDVersion
    {
        return isset($this->Profiles) ? $this->Profiles : null;
    }
    /**
     * Set Profiles value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileIDVersion $profiles
     * @return \ID3Global\Models\UpdateCase
     */
    public function setProfiles(?\ID3Global\Arrays\ArrayOfGlobalProfileIDVersion $profiles = null): self
    {
        if (is_null($profiles) || (is_array($profiles) && empty($profiles))) {
            unset($this->Profiles);
        } else {
            $this->Profiles = $profiles;
        }
        
        return $this;
    }
    /**
     * Get DocumentCategorySubmissionTypes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType|null
     */
    public function getDocumentCategorySubmissionTypes(): ?\ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType
    {
        return isset($this->DocumentCategorySubmissionTypes) ? $this->DocumentCategorySubmissionTypes : null;
    }
    /**
     * Set DocumentCategorySubmissionTypes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType $documentCategorySubmissionTypes
     * @return \ID3Global\Models\UpdateCase
     */
    public function setDocumentCategorySubmissionTypes(?\ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType $documentCategorySubmissionTypes = null): self
    {
        if (is_null($documentCategorySubmissionTypes) || (is_array($documentCategorySubmissionTypes) && empty($documentCategorySubmissionTypes))) {
            unset($this->DocumentCategorySubmissionTypes);
        } else {
            $this->DocumentCategorySubmissionTypes = $documentCategorySubmissionTypes;
        }
        
        return $this;
    }
    /**
     * Get Documents value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDocument|null
     */
    public function getDocuments(): ?\ID3Global\Arrays\ArrayOfGlobalCaseDocument
    {
        return isset($this->Documents) ? $this->Documents : null;
    }
    /**
     * Set Documents value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseDocument $documents
     * @return \ID3Global\Models\UpdateCase
     */
    public function setDocuments(?\ID3Global\Arrays\ArrayOfGlobalCaseDocument $documents = null): self
    {
        if (is_null($documents) || (is_array($documents) && empty($documents))) {
            unset($this->Documents);
        } else {
            $this->Documents = $documents;
        }
        
        return $this;
    }
    /**
     * Get Consents value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseConsent|null
     */
    public function getConsents(): ?\ID3Global\Arrays\ArrayOfGlobalCaseConsent
    {
        return isset($this->Consents) ? $this->Consents : null;
    }
    /**
     * Set Consents value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseConsent $consents
     * @return \ID3Global\Models\UpdateCase
     */
    public function setConsents(?\ID3Global\Arrays\ArrayOfGlobalCaseConsent $consents = null): self
    {
        if (is_null($consents) || (is_array($consents) && empty($consents))) {
            unset($this->Consents);
        } else {
            $this->Consents = $consents;
        }
        
        return $this;
    }
    /**
     * Get InputData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalInputData|null
     */
    public function getInputData(): ?\ID3Global\Models\GlobalInputData
    {
        return isset($this->InputData) ? $this->InputData : null;
    }
    /**
     * Set InputData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalInputData $inputData
     * @return \ID3Global\Models\UpdateCase
     */
    public function setInputData(?\ID3Global\Models\GlobalInputData $inputData = null): self
    {
        if (is_null($inputData) || (is_array($inputData) && empty($inputData))) {
            unset($this->InputData);
        } else {
            $this->InputData = $inputData;
        }
        
        return $this;
    }
}
