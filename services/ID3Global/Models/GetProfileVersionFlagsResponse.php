<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProfileVersionFlagsResponse Models
 * @subpackage Structs
 */
class GetProfileVersionFlagsResponse extends AbstractStructBase
{
    /**
     * The GetProfileVersionFlagsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalProfileVersionsFlag|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalProfileVersionsFlag $GetProfileVersionFlagsResult = null;
    /**
     * Constructor method for GetProfileVersionFlagsResponse
     * @uses GetProfileVersionFlagsResponse::setGetProfileVersionFlagsResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileVersionsFlag $getProfileVersionFlagsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalProfileVersionsFlag $getProfileVersionFlagsResult = null)
    {
        $this
            ->setGetProfileVersionFlagsResult($getProfileVersionFlagsResult);
    }
    /**
     * Get GetProfileVersionFlagsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileVersionsFlag|null
     */
    public function getGetProfileVersionFlagsResult(): ?\ID3Global\Arrays\ArrayOfGlobalProfileVersionsFlag
    {
        return isset($this->GetProfileVersionFlagsResult) ? $this->GetProfileVersionFlagsResult : null;
    }
    /**
     * Set GetProfileVersionFlagsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileVersionsFlag $getProfileVersionFlagsResult
     * @return \ID3Global\Models\GetProfileVersionFlagsResponse
     */
    public function setGetProfileVersionFlagsResult(?\ID3Global\Arrays\ArrayOfGlobalProfileVersionsFlag $getProfileVersionFlagsResult = null): self
    {
        if (is_null($getProfileVersionFlagsResult) || (is_array($getProfileVersionFlagsResult) && empty($getProfileVersionFlagsResult))) {
            unset($this->GetProfileVersionFlagsResult);
        } else {
            $this->GetProfileVersionFlagsResult = $getProfileVersionFlagsResult;
        }
        
        return $this;
    }
}
