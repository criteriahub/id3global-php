<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for StoreDocumentImageResponse Models
 * @subpackage Structs
 */
class StoreDocumentImageResponse extends AbstractStructBase
{
    /**
     * The StoreDocumentImageResult
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $StoreDocumentImageResult = null;
    /**
     * Constructor method for StoreDocumentImageResponse
     * @uses StoreDocumentImageResponse::setStoreDocumentImageResult()
     * @param string $storeDocumentImageResult
     */
    public function __construct(?string $storeDocumentImageResult = null)
    {
        $this
            ->setStoreDocumentImageResult($storeDocumentImageResult);
    }
    /**
     * Get StoreDocumentImageResult value
     * @return string|null
     */
    public function getStoreDocumentImageResult(): ?string
    {
        return $this->StoreDocumentImageResult;
    }
    /**
     * Set StoreDocumentImageResult value
     * @param string $storeDocumentImageResult
     * @return \ID3Global\Models\StoreDocumentImageResponse
     */
    public function setStoreDocumentImageResult(?string $storeDocumentImageResult = null): self
    {
        // validation for constraint: string
        if (!is_null($storeDocumentImageResult) && !is_string($storeDocumentImageResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($storeDocumentImageResult, true), gettype($storeDocumentImageResult)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($storeDocumentImageResult) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $storeDocumentImageResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($storeDocumentImageResult, true)), __LINE__);
        }
        $this->StoreDocumentImageResult = $storeDocumentImageResult;
        
        return $this;
    }
}
