<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalLifestyleResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q61:GlobalLifestyleResultCodes
 * @subpackage Structs
 */
class GlobalLifestyleResultCodes extends GlobalItemCheckResultCodes
{
    /**
     * The UKLifestyleCharacteristics
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $UKLifestyleCharacteristics = null;
    /**
     * The PublicRiskScore
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $PublicRiskScore = null;
    /**
     * Constructor method for GlobalLifestyleResultCodes
     * @uses GlobalLifestyleResultCodes::setUKLifestyleCharacteristics()
     * @uses GlobalLifestyleResultCodes::setPublicRiskScore()
     * @param string $uKLifestyleCharacteristics
     * @param string $publicRiskScore
     */
    public function __construct(?string $uKLifestyleCharacteristics = null, ?string $publicRiskScore = null)
    {
        $this
            ->setUKLifestyleCharacteristics($uKLifestyleCharacteristics)
            ->setPublicRiskScore($publicRiskScore);
    }
    /**
     * Get UKLifestyleCharacteristics value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUKLifestyleCharacteristics(): ?string
    {
        return isset($this->UKLifestyleCharacteristics) ? $this->UKLifestyleCharacteristics : null;
    }
    /**
     * Set UKLifestyleCharacteristics value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $uKLifestyleCharacteristics
     * @return \ID3Global\Models\GlobalLifestyleResultCodes
     */
    public function setUKLifestyleCharacteristics(?string $uKLifestyleCharacteristics = null): self
    {
        // validation for constraint: string
        if (!is_null($uKLifestyleCharacteristics) && !is_string($uKLifestyleCharacteristics)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($uKLifestyleCharacteristics, true), gettype($uKLifestyleCharacteristics)), __LINE__);
        }
        if (is_null($uKLifestyleCharacteristics) || (is_array($uKLifestyleCharacteristics) && empty($uKLifestyleCharacteristics))) {
            unset($this->UKLifestyleCharacteristics);
        } else {
            $this->UKLifestyleCharacteristics = $uKLifestyleCharacteristics;
        }
        
        return $this;
    }
    /**
     * Get PublicRiskScore value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPublicRiskScore(): ?string
    {
        return isset($this->PublicRiskScore) ? $this->PublicRiskScore : null;
    }
    /**
     * Set PublicRiskScore value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $publicRiskScore
     * @return \ID3Global\Models\GlobalLifestyleResultCodes
     */
    public function setPublicRiskScore(?string $publicRiskScore = null): self
    {
        // validation for constraint: string
        if (!is_null($publicRiskScore) && !is_string($publicRiskScore)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($publicRiskScore, true), gettype($publicRiskScore)), __LINE__);
        }
        if (is_null($publicRiskScore) || (is_array($publicRiskScore) && empty($publicRiskScore))) {
            unset($this->PublicRiskScore);
        } else {
            $this->PublicRiskScore = $publicRiskScore;
        }
        
        return $this;
    }
}
