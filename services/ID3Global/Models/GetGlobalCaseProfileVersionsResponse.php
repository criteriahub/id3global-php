<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetGlobalCaseProfileVersionsResponse Models
 * @subpackage Structs
 */
class GetGlobalCaseProfileVersionsResponse extends AbstractStructBase
{
    /**
     * The GetGlobalCaseProfileVersionsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseProfileVersion|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseProfileVersion $GetGlobalCaseProfileVersionsResult = null;
    /**
     * Constructor method for GetGlobalCaseProfileVersionsResponse
     * @uses GetGlobalCaseProfileVersionsResponse::setGetGlobalCaseProfileVersionsResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseProfileVersion $getGlobalCaseProfileVersionsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCaseProfileVersion $getGlobalCaseProfileVersionsResult = null)
    {
        $this
            ->setGetGlobalCaseProfileVersionsResult($getGlobalCaseProfileVersionsResult);
    }
    /**
     * Get GetGlobalCaseProfileVersionsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseProfileVersion|null
     */
    public function getGetGlobalCaseProfileVersionsResult(): ?\ID3Global\Arrays\ArrayOfGlobalCaseProfileVersion
    {
        return isset($this->GetGlobalCaseProfileVersionsResult) ? $this->GetGlobalCaseProfileVersionsResult : null;
    }
    /**
     * Set GetGlobalCaseProfileVersionsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseProfileVersion $getGlobalCaseProfileVersionsResult
     * @return \ID3Global\Models\GetGlobalCaseProfileVersionsResponse
     */
    public function setGetGlobalCaseProfileVersionsResult(?\ID3Global\Arrays\ArrayOfGlobalCaseProfileVersion $getGlobalCaseProfileVersionsResult = null): self
    {
        if (is_null($getGlobalCaseProfileVersionsResult) || (is_array($getGlobalCaseProfileVersionsResult) && empty($getGlobalCaseProfileVersionsResult))) {
            unset($this->GetGlobalCaseProfileVersionsResult);
        } else {
            $this->GetGlobalCaseProfileVersionsResult = $getGlobalCaseProfileVersionsResult;
        }
        
        return $this;
    }
}
