<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAustraliaAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q647:GlobalAustraliaAccount
 * @subpackage Structs
 */
class GlobalAustraliaAccount extends GlobalSupplierAccount
{
    /**
     * The SubscriberNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SubscriberNumber = null;
    /**
     * The UserKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $UserKey = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * Constructor method for GlobalAustraliaAccount
     * @uses GlobalAustraliaAccount::setSubscriberNumber()
     * @uses GlobalAustraliaAccount::setUserKey()
     * @uses GlobalAustraliaAccount::setPassword()
     * @param string $subscriberNumber
     * @param string $userKey
     * @param string $password
     */
    public function __construct(?string $subscriberNumber = null, ?string $userKey = null, ?string $password = null)
    {
        $this
            ->setSubscriberNumber($subscriberNumber)
            ->setUserKey($userKey)
            ->setPassword($password);
    }
    /**
     * Get SubscriberNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSubscriberNumber(): ?string
    {
        return isset($this->SubscriberNumber) ? $this->SubscriberNumber : null;
    }
    /**
     * Set SubscriberNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $subscriberNumber
     * @return \ID3Global\Models\GlobalAustraliaAccount
     */
    public function setSubscriberNumber(?string $subscriberNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($subscriberNumber) && !is_string($subscriberNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subscriberNumber, true), gettype($subscriberNumber)), __LINE__);
        }
        if (is_null($subscriberNumber) || (is_array($subscriberNumber) && empty($subscriberNumber))) {
            unset($this->SubscriberNumber);
        } else {
            $this->SubscriberNumber = $subscriberNumber;
        }
        
        return $this;
    }
    /**
     * Get UserKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUserKey(): ?string
    {
        return isset($this->UserKey) ? $this->UserKey : null;
    }
    /**
     * Set UserKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $userKey
     * @return \ID3Global\Models\GlobalAustraliaAccount
     */
    public function setUserKey(?string $userKey = null): self
    {
        // validation for constraint: string
        if (!is_null($userKey) && !is_string($userKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userKey, true), gettype($userKey)), __LINE__);
        }
        if (is_null($userKey) || (is_array($userKey) && empty($userKey))) {
            unset($this->UserKey);
        } else {
            $this->UserKey = $userKey;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\GlobalAustraliaAccount
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
}
