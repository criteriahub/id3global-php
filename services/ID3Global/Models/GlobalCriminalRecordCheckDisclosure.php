<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCriminalRecordCheckDisclosure Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q791:GlobalCriminalRecordCheckDisclosure
 * @subpackage Structs
 */
class GlobalCriminalRecordCheckDisclosure extends AbstractStructBase
{
    /**
     * The DisclosureDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $DisclosureDate = null;
    /**
     * The Reference
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Reference = null;
    /**
     * The AdditionalInformation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AdditionalInformation = null;
    /**
     * The Status
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Status = null;
    /**
     * The CriminalRecordCheckDisclosureOffences
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCriminalRecordCheckDisclosureOffence|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCriminalRecordCheckDisclosureOffence $CriminalRecordCheckDisclosureOffences = null;
    /**
     * Constructor method for GlobalCriminalRecordCheckDisclosure
     * @uses GlobalCriminalRecordCheckDisclosure::setDisclosureDate()
     * @uses GlobalCriminalRecordCheckDisclosure::setReference()
     * @uses GlobalCriminalRecordCheckDisclosure::setAdditionalInformation()
     * @uses GlobalCriminalRecordCheckDisclosure::setStatus()
     * @uses GlobalCriminalRecordCheckDisclosure::setCriminalRecordCheckDisclosureOffences()
     * @param string $disclosureDate
     * @param string $reference
     * @param string $additionalInformation
     * @param int $status
     * @param \ID3Global\Arrays\ArrayOfGlobalCriminalRecordCheckDisclosureOffence $criminalRecordCheckDisclosureOffences
     */
    public function __construct(?string $disclosureDate = null, ?string $reference = null, ?string $additionalInformation = null, ?int $status = null, ?\ID3Global\Arrays\ArrayOfGlobalCriminalRecordCheckDisclosureOffence $criminalRecordCheckDisclosureOffences = null)
    {
        $this
            ->setDisclosureDate($disclosureDate)
            ->setReference($reference)
            ->setAdditionalInformation($additionalInformation)
            ->setStatus($status)
            ->setCriminalRecordCheckDisclosureOffences($criminalRecordCheckDisclosureOffences);
    }
    /**
     * Get DisclosureDate value
     * @return string|null
     */
    public function getDisclosureDate(): ?string
    {
        return $this->DisclosureDate;
    }
    /**
     * Set DisclosureDate value
     * @param string $disclosureDate
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosure
     */
    public function setDisclosureDate(?string $disclosureDate = null): self
    {
        // validation for constraint: string
        if (!is_null($disclosureDate) && !is_string($disclosureDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($disclosureDate, true), gettype($disclosureDate)), __LINE__);
        }
        $this->DisclosureDate = $disclosureDate;
        
        return $this;
    }
    /**
     * Get Reference value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReference(): ?string
    {
        return isset($this->Reference) ? $this->Reference : null;
    }
    /**
     * Set Reference value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $reference
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosure
     */
    public function setReference(?string $reference = null): self
    {
        // validation for constraint: string
        if (!is_null($reference) && !is_string($reference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($reference, true), gettype($reference)), __LINE__);
        }
        if (is_null($reference) || (is_array($reference) && empty($reference))) {
            unset($this->Reference);
        } else {
            $this->Reference = $reference;
        }
        
        return $this;
    }
    /**
     * Get AdditionalInformation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalInformation(): ?string
    {
        return isset($this->AdditionalInformation) ? $this->AdditionalInformation : null;
    }
    /**
     * Set AdditionalInformation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalInformation
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosure
     */
    public function setAdditionalInformation(?string $additionalInformation = null): self
    {
        // validation for constraint: string
        if (!is_null($additionalInformation) && !is_string($additionalInformation)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalInformation, true), gettype($additionalInformation)), __LINE__);
        }
        if (is_null($additionalInformation) || (is_array($additionalInformation) && empty($additionalInformation))) {
            unset($this->AdditionalInformation);
        } else {
            $this->AdditionalInformation = $additionalInformation;
        }
        
        return $this;
    }
    /**
     * Get Status value
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @param int $status
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosure
     */
    public function setStatus(?int $status = null): self
    {
        // validation for constraint: int
        if (!is_null($status) && !(is_int($status) || ctype_digit($status))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($status, true), gettype($status)), __LINE__);
        }
        $this->Status = $status;
        
        return $this;
    }
    /**
     * Get CriminalRecordCheckDisclosureOffences value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCriminalRecordCheckDisclosureOffence|null
     */
    public function getCriminalRecordCheckDisclosureOffences(): ?\ID3Global\Arrays\ArrayOfGlobalCriminalRecordCheckDisclosureOffence
    {
        return isset($this->CriminalRecordCheckDisclosureOffences) ? $this->CriminalRecordCheckDisclosureOffences : null;
    }
    /**
     * Set CriminalRecordCheckDisclosureOffences value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCriminalRecordCheckDisclosureOffence $criminalRecordCheckDisclosureOffences
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosure
     */
    public function setCriminalRecordCheckDisclosureOffences(?\ID3Global\Arrays\ArrayOfGlobalCriminalRecordCheckDisclosureOffence $criminalRecordCheckDisclosureOffences = null): self
    {
        if (is_null($criminalRecordCheckDisclosureOffences) || (is_array($criminalRecordCheckDisclosureOffences) && empty($criminalRecordCheckDisclosureOffences))) {
            unset($this->CriminalRecordCheckDisclosureOffences);
        } else {
            $this->CriminalRecordCheckDisclosureOffences = $criminalRecordCheckDisclosureOffences;
        }
        
        return $this;
    }
}
