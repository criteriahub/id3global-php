<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SaveManualDispatchResultResponse Models
 * @subpackage Structs
 */
class SaveManualDispatchResultResponse extends AbstractStructBase
{
    /**
     * The SaveManualDispatchResultResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseDetails|null
     */
    protected ?\ID3Global\Models\GlobalCaseDetails $SaveManualDispatchResultResult = null;
    /**
     * Constructor method for SaveManualDispatchResultResponse
     * @uses SaveManualDispatchResultResponse::setSaveManualDispatchResultResult()
     * @param \ID3Global\Models\GlobalCaseDetails $saveManualDispatchResultResult
     */
    public function __construct(?\ID3Global\Models\GlobalCaseDetails $saveManualDispatchResultResult = null)
    {
        $this
            ->setSaveManualDispatchResultResult($saveManualDispatchResultResult);
    }
    /**
     * Get SaveManualDispatchResultResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseDetails|null
     */
    public function getSaveManualDispatchResultResult(): ?\ID3Global\Models\GlobalCaseDetails
    {
        return isset($this->SaveManualDispatchResultResult) ? $this->SaveManualDispatchResultResult : null;
    }
    /**
     * Set SaveManualDispatchResultResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCaseDetails $saveManualDispatchResultResult
     * @return \ID3Global\Models\SaveManualDispatchResultResponse
     */
    public function setSaveManualDispatchResultResult(?\ID3Global\Models\GlobalCaseDetails $saveManualDispatchResultResult = null): self
    {
        if (is_null($saveManualDispatchResultResult) || (is_array($saveManualDispatchResultResult) && empty($saveManualDispatchResultResult))) {
            unset($this->SaveManualDispatchResultResult);
        } else {
            $this->SaveManualDispatchResultResult = $saveManualDispatchResultResult;
        }
        
        return $this;
    }
}
