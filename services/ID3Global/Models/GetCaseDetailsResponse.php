<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCaseDetailsResponse Models
 * @subpackage Structs
 */
class GetCaseDetailsResponse extends AbstractStructBase
{
    /**
     * The GetCaseDetailsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseDetails|null
     */
    protected ?\ID3Global\Models\GlobalCaseDetails $GetCaseDetailsResult = null;
    /**
     * Constructor method for GetCaseDetailsResponse
     * @uses GetCaseDetailsResponse::setGetCaseDetailsResult()
     * @param \ID3Global\Models\GlobalCaseDetails $getCaseDetailsResult
     */
    public function __construct(?\ID3Global\Models\GlobalCaseDetails $getCaseDetailsResult = null)
    {
        $this
            ->setGetCaseDetailsResult($getCaseDetailsResult);
    }
    /**
     * Get GetCaseDetailsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseDetails|null
     */
    public function getGetCaseDetailsResult(): ?\ID3Global\Models\GlobalCaseDetails
    {
        return isset($this->GetCaseDetailsResult) ? $this->GetCaseDetailsResult : null;
    }
    /**
     * Set GetCaseDetailsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCaseDetails $getCaseDetailsResult
     * @return \ID3Global\Models\GetCaseDetailsResponse
     */
    public function setGetCaseDetailsResult(?\ID3Global\Models\GlobalCaseDetails $getCaseDetailsResult = null): self
    {
        if (is_null($getCaseDetailsResult) || (is_array($getCaseDetailsResult) && empty($getCaseDetailsResult))) {
            unset($this->GetCaseDetailsResult);
        } else {
            $this->GetCaseDetailsResult = $getCaseDetailsResult;
        }
        
        return $this;
    }
}
