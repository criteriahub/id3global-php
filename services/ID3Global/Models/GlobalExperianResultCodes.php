<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalExperianResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q79:GlobalExperianResultCodes
 * @subpackage Structs
 */
class GlobalExperianResultCodes extends GlobalItemCheckResultCodes
{
    /**
     * The CreditMatches
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCreditMatch|null
     */
    protected ?\ID3Global\Models\GlobalCreditMatch $CreditMatches = null;
    /**
     * Constructor method for GlobalExperianResultCodes
     * @uses GlobalExperianResultCodes::setCreditMatches()
     * @param \ID3Global\Models\GlobalCreditMatch $creditMatches
     */
    public function __construct(?\ID3Global\Models\GlobalCreditMatch $creditMatches = null)
    {
        $this
            ->setCreditMatches($creditMatches);
    }
    /**
     * Get CreditMatches value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCreditMatch|null
     */
    public function getCreditMatches(): ?\ID3Global\Models\GlobalCreditMatch
    {
        return isset($this->CreditMatches) ? $this->CreditMatches : null;
    }
    /**
     * Set CreditMatches value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCreditMatch $creditMatches
     * @return \ID3Global\Models\GlobalExperianResultCodes
     */
    public function setCreditMatches(?\ID3Global\Models\GlobalCreditMatch $creditMatches = null): self
    {
        if (is_null($creditMatches) || (is_array($creditMatches) && empty($creditMatches))) {
            unset($this->CreditMatches);
        } else {
            $this->CreditMatches = $creditMatches;
        }
        
        return $this;
    }
}
