<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetReportImage Models
 * @subpackage Structs
 */
class GetReportImage extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The RequestID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $RequestID = null;
    /**
     * The ImageID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ImageID = null;
    /**
     * Constructor method for GetReportImage
     * @uses GetReportImage::setOrgID()
     * @uses GetReportImage::setRequestID()
     * @uses GetReportImage::setImageID()
     * @param string $orgID
     * @param string $requestID
     * @param string $imageID
     */
    public function __construct(?string $orgID = null, ?string $requestID = null, ?string $imageID = null)
    {
        $this
            ->setOrgID($orgID)
            ->setRequestID($requestID)
            ->setImageID($imageID);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetReportImage
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get RequestID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRequestID(): ?string
    {
        return isset($this->RequestID) ? $this->RequestID : null;
    }
    /**
     * Set RequestID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $requestID
     * @return \ID3Global\Models\GetReportImage
     */
    public function setRequestID(?string $requestID = null): self
    {
        // validation for constraint: string
        if (!is_null($requestID) && !is_string($requestID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($requestID, true), gettype($requestID)), __LINE__);
        }
        if (is_null($requestID) || (is_array($requestID) && empty($requestID))) {
            unset($this->RequestID);
        } else {
            $this->RequestID = $requestID;
        }
        
        return $this;
    }
    /**
     * Get ImageID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getImageID(): ?string
    {
        return isset($this->ImageID) ? $this->ImageID : null;
    }
    /**
     * Set ImageID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $imageID
     * @return \ID3Global\Models\GetReportImage
     */
    public function setImageID(?string $imageID = null): self
    {
        // validation for constraint: string
        if (!is_null($imageID) && !is_string($imageID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($imageID, true), gettype($imageID)), __LINE__);
        }
        if (is_null($imageID) || (is_array($imageID) && empty($imageID))) {
            unset($this->ImageID);
        } else {
            $this->ImageID = $imageID;
        }
        
        return $this;
    }
}
