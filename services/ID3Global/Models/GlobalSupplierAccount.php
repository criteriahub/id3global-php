<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSupplierAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q638:GlobalSupplierAccount
 * @subpackage Structs
 */
class GlobalSupplierAccount extends AbstractStructBase
{
    /**
     * The SupplierAccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $SupplierAccountID = null;
    /**
     * The Created
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Created = null;
    /**
     * The Tested
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Tested = null;
    /**
     * The Success
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Success = null;
    /**
     * The Status
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Status = null;
    /**
     * The CanInherit
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $CanInherit = null;
    /**
     * The OwningOrg
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierAccountOrg|null
     */
    protected ?\ID3Global\Models\GlobalSupplierAccountOrg $OwningOrg = null;
    /**
     * The ParentOrg
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierAccountOrg|null
     */
    protected ?\ID3Global\Models\GlobalSupplierAccountOrg $ParentOrg = null;
    /**
     * The SharedByOrgs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSupplierAccountOrg|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSupplierAccountOrg $SharedByOrgs = null;
    /**
     * The SupplierID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $SupplierID = null;
    /**
     * The PendingOrg
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierAccountOrg|null
     */
    protected ?\ID3Global\Models\GlobalSupplierAccountOrg $PendingOrg = null;
    /**
     * The Active
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Active = null;
    /**
     * The PasswordChange
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $PasswordChange = null;
    /**
     * Constructor method for GlobalSupplierAccount
     * @uses GlobalSupplierAccount::setSupplierAccountID()
     * @uses GlobalSupplierAccount::setCreated()
     * @uses GlobalSupplierAccount::setTested()
     * @uses GlobalSupplierAccount::setSuccess()
     * @uses GlobalSupplierAccount::setStatus()
     * @uses GlobalSupplierAccount::setCanInherit()
     * @uses GlobalSupplierAccount::setOwningOrg()
     * @uses GlobalSupplierAccount::setParentOrg()
     * @uses GlobalSupplierAccount::setSharedByOrgs()
     * @uses GlobalSupplierAccount::setSupplierID()
     * @uses GlobalSupplierAccount::setPendingOrg()
     * @uses GlobalSupplierAccount::setActive()
     * @uses GlobalSupplierAccount::setPasswordChange()
     * @param string $supplierAccountID
     * @param string $created
     * @param string $tested
     * @param bool $success
     * @param string $status
     * @param bool $canInherit
     * @param \ID3Global\Models\GlobalSupplierAccountOrg $owningOrg
     * @param \ID3Global\Models\GlobalSupplierAccountOrg $parentOrg
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplierAccountOrg $sharedByOrgs
     * @param string $supplierID
     * @param \ID3Global\Models\GlobalSupplierAccountOrg $pendingOrg
     * @param bool $active
     * @param string $passwordChange
     */
    public function __construct(?string $supplierAccountID = null, ?string $created = null, ?string $tested = null, ?bool $success = null, ?string $status = null, ?bool $canInherit = null, ?\ID3Global\Models\GlobalSupplierAccountOrg $owningOrg = null, ?\ID3Global\Models\GlobalSupplierAccountOrg $parentOrg = null, ?\ID3Global\Arrays\ArrayOfGlobalSupplierAccountOrg $sharedByOrgs = null, ?string $supplierID = null, ?\ID3Global\Models\GlobalSupplierAccountOrg $pendingOrg = null, ?bool $active = null, ?string $passwordChange = null)
    {
        $this
            ->setSupplierAccountID($supplierAccountID)
            ->setCreated($created)
            ->setTested($tested)
            ->setSuccess($success)
            ->setStatus($status)
            ->setCanInherit($canInherit)
            ->setOwningOrg($owningOrg)
            ->setParentOrg($parentOrg)
            ->setSharedByOrgs($sharedByOrgs)
            ->setSupplierID($supplierID)
            ->setPendingOrg($pendingOrg)
            ->setActive($active)
            ->setPasswordChange($passwordChange);
    }
    /**
     * Get SupplierAccountID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSupplierAccountID(): ?string
    {
        return isset($this->SupplierAccountID) ? $this->SupplierAccountID : null;
    }
    /**
     * Set SupplierAccountID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $supplierAccountID
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setSupplierAccountID(?string $supplierAccountID = null): self
    {
        // validation for constraint: string
        if (!is_null($supplierAccountID) && !is_string($supplierAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($supplierAccountID, true), gettype($supplierAccountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($supplierAccountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $supplierAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($supplierAccountID, true)), __LINE__);
        }
        if (is_null($supplierAccountID) || (is_array($supplierAccountID) && empty($supplierAccountID))) {
            unset($this->SupplierAccountID);
        } else {
            $this->SupplierAccountID = $supplierAccountID;
        }
        
        return $this;
    }
    /**
     * Get Created value
     * @return string|null
     */
    public function getCreated(): ?string
    {
        return $this->Created;
    }
    /**
     * Set Created value
     * @param string $created
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setCreated(?string $created = null): self
    {
        // validation for constraint: string
        if (!is_null($created) && !is_string($created)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($created, true), gettype($created)), __LINE__);
        }
        $this->Created = $created;
        
        return $this;
    }
    /**
     * Get Tested value
     * @return string|null
     */
    public function getTested(): ?string
    {
        return $this->Tested;
    }
    /**
     * Set Tested value
     * @param string $tested
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setTested(?string $tested = null): self
    {
        // validation for constraint: string
        if (!is_null($tested) && !is_string($tested)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tested, true), gettype($tested)), __LINE__);
        }
        $this->Tested = $tested;
        
        return $this;
    }
    /**
     * Get Success value
     * @return bool|null
     */
    public function getSuccess(): ?bool
    {
        return $this->Success;
    }
    /**
     * Set Success value
     * @param bool $success
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setSuccess(?bool $success = null): self
    {
        // validation for constraint: boolean
        if (!is_null($success) && !is_bool($success)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($success, true), gettype($success)), __LINE__);
        }
        $this->Success = $success;
        
        return $this;
    }
    /**
     * Get Status value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return isset($this->Status) ? $this->Status : null;
    }
    /**
     * Set Status value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $status
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: string
        if (!is_null($status) && !is_string($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($status, true), gettype($status)), __LINE__);
        }
        if (is_null($status) || (is_array($status) && empty($status))) {
            unset($this->Status);
        } else {
            $this->Status = $status;
        }
        
        return $this;
    }
    /**
     * Get CanInherit value
     * @return bool|null
     */
    public function getCanInherit(): ?bool
    {
        return $this->CanInherit;
    }
    /**
     * Set CanInherit value
     * @param bool $canInherit
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setCanInherit(?bool $canInherit = null): self
    {
        // validation for constraint: boolean
        if (!is_null($canInherit) && !is_bool($canInherit)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($canInherit, true), gettype($canInherit)), __LINE__);
        }
        $this->CanInherit = $canInherit;
        
        return $this;
    }
    /**
     * Get OwningOrg value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierAccountOrg|null
     */
    public function getOwningOrg(): ?\ID3Global\Models\GlobalSupplierAccountOrg
    {
        return isset($this->OwningOrg) ? $this->OwningOrg : null;
    }
    /**
     * Set OwningOrg value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupplierAccountOrg $owningOrg
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setOwningOrg(?\ID3Global\Models\GlobalSupplierAccountOrg $owningOrg = null): self
    {
        if (is_null($owningOrg) || (is_array($owningOrg) && empty($owningOrg))) {
            unset($this->OwningOrg);
        } else {
            $this->OwningOrg = $owningOrg;
        }
        
        return $this;
    }
    /**
     * Get ParentOrg value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierAccountOrg|null
     */
    public function getParentOrg(): ?\ID3Global\Models\GlobalSupplierAccountOrg
    {
        return isset($this->ParentOrg) ? $this->ParentOrg : null;
    }
    /**
     * Set ParentOrg value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupplierAccountOrg $parentOrg
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setParentOrg(?\ID3Global\Models\GlobalSupplierAccountOrg $parentOrg = null): self
    {
        if (is_null($parentOrg) || (is_array($parentOrg) && empty($parentOrg))) {
            unset($this->ParentOrg);
        } else {
            $this->ParentOrg = $parentOrg;
        }
        
        return $this;
    }
    /**
     * Get SharedByOrgs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierAccountOrg|null
     */
    public function getSharedByOrgs(): ?\ID3Global\Arrays\ArrayOfGlobalSupplierAccountOrg
    {
        return isset($this->SharedByOrgs) ? $this->SharedByOrgs : null;
    }
    /**
     * Set SharedByOrgs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplierAccountOrg $sharedByOrgs
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setSharedByOrgs(?\ID3Global\Arrays\ArrayOfGlobalSupplierAccountOrg $sharedByOrgs = null): self
    {
        if (is_null($sharedByOrgs) || (is_array($sharedByOrgs) && empty($sharedByOrgs))) {
            unset($this->SharedByOrgs);
        } else {
            $this->SharedByOrgs = $sharedByOrgs;
        }
        
        return $this;
    }
    /**
     * Get SupplierID value
     * @return string|null
     */
    public function getSupplierID(): ?string
    {
        return $this->SupplierID;
    }
    /**
     * Set SupplierID value
     * @param string $supplierID
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setSupplierID(?string $supplierID = null): self
    {
        // validation for constraint: string
        if (!is_null($supplierID) && !is_string($supplierID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($supplierID, true), gettype($supplierID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($supplierID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $supplierID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($supplierID, true)), __LINE__);
        }
        $this->SupplierID = $supplierID;
        
        return $this;
    }
    /**
     * Get PendingOrg value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierAccountOrg|null
     */
    public function getPendingOrg(): ?\ID3Global\Models\GlobalSupplierAccountOrg
    {
        return isset($this->PendingOrg) ? $this->PendingOrg : null;
    }
    /**
     * Set PendingOrg value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupplierAccountOrg $pendingOrg
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setPendingOrg(?\ID3Global\Models\GlobalSupplierAccountOrg $pendingOrg = null): self
    {
        if (is_null($pendingOrg) || (is_array($pendingOrg) && empty($pendingOrg))) {
            unset($this->PendingOrg);
        } else {
            $this->PendingOrg = $pendingOrg;
        }
        
        return $this;
    }
    /**
     * Get Active value
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->Active;
    }
    /**
     * Set Active value
     * @param bool $active
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setActive(?bool $active = null): self
    {
        // validation for constraint: boolean
        if (!is_null($active) && !is_bool($active)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($active, true), gettype($active)), __LINE__);
        }
        $this->Active = $active;
        
        return $this;
    }
    /**
     * Get PasswordChange value
     * @return string|null
     */
    public function getPasswordChange(): ?string
    {
        return $this->PasswordChange;
    }
    /**
     * Set PasswordChange value
     * @param string $passwordChange
     * @return \ID3Global\Models\GlobalSupplierAccount
     */
    public function setPasswordChange(?string $passwordChange = null): self
    {
        // validation for constraint: string
        if (!is_null($passwordChange) && !is_string($passwordChange)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($passwordChange, true), gettype($passwordChange)), __LINE__);
        }
        $this->PasswordChange = $passwordChange;
        
        return $this;
    }
}
