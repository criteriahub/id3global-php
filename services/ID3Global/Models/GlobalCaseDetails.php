<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q754:GlobalCaseDetails
 * @subpackage Structs
 */
class GlobalCaseDetails extends GlobalCase
{
    /**
     * The DispatchResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDispatchResult|null
     */
    protected ?\ID3Global\Models\GlobalDispatchResult $DispatchResult = null;
    /**
     * The Reports
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseReport|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseReport $Reports = null;
    /**
     * The DocumentCategorySubmissionTypes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType $DocumentCategorySubmissionTypes = null;
    /**
     * The Documents
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseDocument|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseDocument $Documents = null;
    /**
     * The Consents
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseConsent|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseConsent $Consents = null;
    /**
     * The DispatchRecord
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseDispatchRecord|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseDispatchRecord $DispatchRecord = null;
    /**
     * The DisclaimerAccepted
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $DisclaimerAccepted = null;
    /**
     * The UpdatedBy
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $UpdatedBy = null;
    /**
     * The UpdatedByAccount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $UpdatedByAccount = null;
    /**
     * Constructor method for GlobalCaseDetails
     * @uses GlobalCaseDetails::setDispatchResult()
     * @uses GlobalCaseDetails::setReports()
     * @uses GlobalCaseDetails::setDocumentCategorySubmissionTypes()
     * @uses GlobalCaseDetails::setDocuments()
     * @uses GlobalCaseDetails::setConsents()
     * @uses GlobalCaseDetails::setDispatchRecord()
     * @uses GlobalCaseDetails::setDisclaimerAccepted()
     * @uses GlobalCaseDetails::setUpdatedBy()
     * @uses GlobalCaseDetails::setUpdatedByAccount()
     * @param \ID3Global\Models\GlobalDispatchResult $dispatchResult
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseReport $reports
     * @param \ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType $documentCategorySubmissionTypes
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseDocument $documents
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseConsent $consents
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseDispatchRecord $dispatchRecord
     * @param bool $disclaimerAccepted
     * @param string $updatedBy
     * @param string $updatedByAccount
     */
    public function __construct(?\ID3Global\Models\GlobalDispatchResult $dispatchResult = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseReport $reports = null, ?\ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType $documentCategorySubmissionTypes = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseDocument $documents = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseConsent $consents = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseDispatchRecord $dispatchRecord = null, ?bool $disclaimerAccepted = null, ?string $updatedBy = null, ?string $updatedByAccount = null)
    {
        $this
            ->setDispatchResult($dispatchResult)
            ->setReports($reports)
            ->setDocumentCategorySubmissionTypes($documentCategorySubmissionTypes)
            ->setDocuments($documents)
            ->setConsents($consents)
            ->setDispatchRecord($dispatchRecord)
            ->setDisclaimerAccepted($disclaimerAccepted)
            ->setUpdatedBy($updatedBy)
            ->setUpdatedByAccount($updatedByAccount);
    }
    /**
     * Get DispatchResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDispatchResult|null
     */
    public function getDispatchResult(): ?\ID3Global\Models\GlobalDispatchResult
    {
        return isset($this->DispatchResult) ? $this->DispatchResult : null;
    }
    /**
     * Set DispatchResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalDispatchResult $dispatchResult
     * @return \ID3Global\Models\GlobalCaseDetails
     */
    public function setDispatchResult(?\ID3Global\Models\GlobalDispatchResult $dispatchResult = null): self
    {
        if (is_null($dispatchResult) || (is_array($dispatchResult) && empty($dispatchResult))) {
            unset($this->DispatchResult);
        } else {
            $this->DispatchResult = $dispatchResult;
        }
        
        return $this;
    }
    /**
     * Get Reports value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseReport|null
     */
    public function getReports(): ?\ID3Global\Arrays\ArrayOfGlobalCaseReport
    {
        return isset($this->Reports) ? $this->Reports : null;
    }
    /**
     * Set Reports value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseReport $reports
     * @return \ID3Global\Models\GlobalCaseDetails
     */
    public function setReports(?\ID3Global\Arrays\ArrayOfGlobalCaseReport $reports = null): self
    {
        if (is_null($reports) || (is_array($reports) && empty($reports))) {
            unset($this->Reports);
        } else {
            $this->Reports = $reports;
        }
        
        return $this;
    }
    /**
     * Get DocumentCategorySubmissionTypes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType|null
     */
    public function getDocumentCategorySubmissionTypes(): ?\ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType
    {
        return isset($this->DocumentCategorySubmissionTypes) ? $this->DocumentCategorySubmissionTypes : null;
    }
    /**
     * Set DocumentCategorySubmissionTypes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType $documentCategorySubmissionTypes
     * @return \ID3Global\Models\GlobalCaseDetails
     */
    public function setDocumentCategorySubmissionTypes(?\ID3Global\Arrays\ArrayOfGlobalDocumentCategorySubmissionType $documentCategorySubmissionTypes = null): self
    {
        if (is_null($documentCategorySubmissionTypes) || (is_array($documentCategorySubmissionTypes) && empty($documentCategorySubmissionTypes))) {
            unset($this->DocumentCategorySubmissionTypes);
        } else {
            $this->DocumentCategorySubmissionTypes = $documentCategorySubmissionTypes;
        }
        
        return $this;
    }
    /**
     * Get Documents value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDocument|null
     */
    public function getDocuments(): ?\ID3Global\Arrays\ArrayOfGlobalCaseDocument
    {
        return isset($this->Documents) ? $this->Documents : null;
    }
    /**
     * Set Documents value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseDocument $documents
     * @return \ID3Global\Models\GlobalCaseDetails
     */
    public function setDocuments(?\ID3Global\Arrays\ArrayOfGlobalCaseDocument $documents = null): self
    {
        if (is_null($documents) || (is_array($documents) && empty($documents))) {
            unset($this->Documents);
        } else {
            $this->Documents = $documents;
        }
        
        return $this;
    }
    /**
     * Get Consents value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseConsent|null
     */
    public function getConsents(): ?\ID3Global\Arrays\ArrayOfGlobalCaseConsent
    {
        return isset($this->Consents) ? $this->Consents : null;
    }
    /**
     * Set Consents value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseConsent $consents
     * @return \ID3Global\Models\GlobalCaseDetails
     */
    public function setConsents(?\ID3Global\Arrays\ArrayOfGlobalCaseConsent $consents = null): self
    {
        if (is_null($consents) || (is_array($consents) && empty($consents))) {
            unset($this->Consents);
        } else {
            $this->Consents = $consents;
        }
        
        return $this;
    }
    /**
     * Get DispatchRecord value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseDispatchRecord|null
     */
    public function getDispatchRecord(): ?\ID3Global\Arrays\ArrayOfGlobalCaseDispatchRecord
    {
        return isset($this->DispatchRecord) ? $this->DispatchRecord : null;
    }
    /**
     * Set DispatchRecord value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseDispatchRecord $dispatchRecord
     * @return \ID3Global\Models\GlobalCaseDetails
     */
    public function setDispatchRecord(?\ID3Global\Arrays\ArrayOfGlobalCaseDispatchRecord $dispatchRecord = null): self
    {
        if (is_null($dispatchRecord) || (is_array($dispatchRecord) && empty($dispatchRecord))) {
            unset($this->DispatchRecord);
        } else {
            $this->DispatchRecord = $dispatchRecord;
        }
        
        return $this;
    }
    /**
     * Get DisclaimerAccepted value
     * @return bool|null
     */
    public function getDisclaimerAccepted(): ?bool
    {
        return $this->DisclaimerAccepted;
    }
    /**
     * Set DisclaimerAccepted value
     * @param bool $disclaimerAccepted
     * @return \ID3Global\Models\GlobalCaseDetails
     */
    public function setDisclaimerAccepted(?bool $disclaimerAccepted = null): self
    {
        // validation for constraint: boolean
        if (!is_null($disclaimerAccepted) && !is_bool($disclaimerAccepted)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($disclaimerAccepted, true), gettype($disclaimerAccepted)), __LINE__);
        }
        $this->DisclaimerAccepted = $disclaimerAccepted;
        
        return $this;
    }
    /**
     * Get UpdatedBy value
     * @return string|null
     */
    public function getUpdatedBy(): ?string
    {
        return $this->UpdatedBy;
    }
    /**
     * Set UpdatedBy value
     * @param string $updatedBy
     * @return \ID3Global\Models\GlobalCaseDetails
     */
    public function setUpdatedBy(?string $updatedBy = null): self
    {
        // validation for constraint: string
        if (!is_null($updatedBy) && !is_string($updatedBy)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updatedBy, true), gettype($updatedBy)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($updatedBy) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $updatedBy)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($updatedBy, true)), __LINE__);
        }
        $this->UpdatedBy = $updatedBy;
        
        return $this;
    }
    /**
     * Get UpdatedByAccount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUpdatedByAccount(): ?string
    {
        return isset($this->UpdatedByAccount) ? $this->UpdatedByAccount : null;
    }
    /**
     * Set UpdatedByAccount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $updatedByAccount
     * @return \ID3Global\Models\GlobalCaseDetails
     */
    public function setUpdatedByAccount(?string $updatedByAccount = null): self
    {
        // validation for constraint: string
        if (!is_null($updatedByAccount) && !is_string($updatedByAccount)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updatedByAccount, true), gettype($updatedByAccount)), __LINE__);
        }
        if (is_null($updatedByAccount) || (is_array($updatedByAccount) && empty($updatedByAccount))) {
            unset($this->UpdatedByAccount);
        } else {
            $this->UpdatedByAccount = $updatedByAccount;
        }
        
        return $this;
    }
}
