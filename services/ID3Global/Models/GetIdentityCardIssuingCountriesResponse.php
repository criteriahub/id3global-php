<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetIdentityCardIssuingCountriesResponse Models
 * @subpackage Structs
 */
class GetIdentityCardIssuingCountriesResponse extends AbstractStructBase
{
    /**
     * The GetIdentityCardIssuingCountriesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCountry|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCountry $GetIdentityCardIssuingCountriesResult = null;
    /**
     * Constructor method for GetIdentityCardIssuingCountriesResponse
     * @uses GetIdentityCardIssuingCountriesResponse::setGetIdentityCardIssuingCountriesResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalCountry $getIdentityCardIssuingCountriesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCountry $getIdentityCardIssuingCountriesResult = null)
    {
        $this
            ->setGetIdentityCardIssuingCountriesResult($getIdentityCardIssuingCountriesResult);
    }
    /**
     * Get GetIdentityCardIssuingCountriesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCountry|null
     */
    public function getGetIdentityCardIssuingCountriesResult(): ?\ID3Global\Arrays\ArrayOfGlobalCountry
    {
        return isset($this->GetIdentityCardIssuingCountriesResult) ? $this->GetIdentityCardIssuingCountriesResult : null;
    }
    /**
     * Set GetIdentityCardIssuingCountriesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCountry $getIdentityCardIssuingCountriesResult
     * @return \ID3Global\Models\GetIdentityCardIssuingCountriesResponse
     */
    public function setGetIdentityCardIssuingCountriesResult(?\ID3Global\Arrays\ArrayOfGlobalCountry $getIdentityCardIssuingCountriesResult = null): self
    {
        if (is_null($getIdentityCardIssuingCountriesResult) || (is_array($getIdentityCardIssuingCountriesResult) && empty($getIdentityCardIssuingCountriesResult))) {
            unset($this->GetIdentityCardIssuingCountriesResult);
        } else {
            $this->GetIdentityCardIssuingCountriesResult = $getIdentityCardIssuingCountriesResult;
        }
        
        return $this;
    }
}
