<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCanada Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q385:GlobalCanada
 * @subpackage Structs
 */
class GlobalCanada extends AbstractStructBase
{
    /**
     * The SocialInsuranceNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCanadaSocialInsuranceNumber|null
     */
    protected ?\ID3Global\Models\GlobalCanadaSocialInsuranceNumber $SocialInsuranceNumber = null;
    /**
     * Constructor method for GlobalCanada
     * @uses GlobalCanada::setSocialInsuranceNumber()
     * @param \ID3Global\Models\GlobalCanadaSocialInsuranceNumber $socialInsuranceNumber
     */
    public function __construct(?\ID3Global\Models\GlobalCanadaSocialInsuranceNumber $socialInsuranceNumber = null)
    {
        $this
            ->setSocialInsuranceNumber($socialInsuranceNumber);
    }
    /**
     * Get SocialInsuranceNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCanadaSocialInsuranceNumber|null
     */
    public function getSocialInsuranceNumber(): ?\ID3Global\Models\GlobalCanadaSocialInsuranceNumber
    {
        return isset($this->SocialInsuranceNumber) ? $this->SocialInsuranceNumber : null;
    }
    /**
     * Set SocialInsuranceNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCanadaSocialInsuranceNumber $socialInsuranceNumber
     * @return \ID3Global\Models\GlobalCanada
     */
    public function setSocialInsuranceNumber(?\ID3Global\Models\GlobalCanadaSocialInsuranceNumber $socialInsuranceNumber = null): self
    {
        if (is_null($socialInsuranceNumber) || (is_array($socialInsuranceNumber) && empty($socialInsuranceNumber))) {
            unset($this->SocialInsuranceNumber);
        } else {
            $this->SocialInsuranceNumber = $socialInsuranceNumber;
        }
        
        return $this;
    }
}
