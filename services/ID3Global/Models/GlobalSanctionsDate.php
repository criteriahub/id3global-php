<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSanctionsDate Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q484:GlobalSanctionsDate
 * @subpackage Structs
 */
class GlobalSanctionsDate extends AbstractStructBase
{
    /**
     * The DateType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $DateType = null;
    /**
     * The Day
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Day = null;
    /**
     * The Month
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Month = null;
    /**
     * The Year
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Year = null;
    /**
     * Constructor method for GlobalSanctionsDate
     * @uses GlobalSanctionsDate::setDateType()
     * @uses GlobalSanctionsDate::setDay()
     * @uses GlobalSanctionsDate::setMonth()
     * @uses GlobalSanctionsDate::setYear()
     * @param string $dateType
     * @param int $day
     * @param int $month
     * @param int $year
     */
    public function __construct(?string $dateType = null, ?int $day = null, ?int $month = null, ?int $year = null)
    {
        $this
            ->setDateType($dateType)
            ->setDay($day)
            ->setMonth($month)
            ->setYear($year);
    }
    /**
     * Get DateType value
     * @return string|null
     */
    public function getDateType(): ?string
    {
        return $this->DateType;
    }
    /**
     * Set DateType value
     * @uses \ID3Global\Enums\GlobalSanctionsDateType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalSanctionsDateType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $dateType
     * @return \ID3Global\Models\GlobalSanctionsDate
     */
    public function setDateType(?string $dateType = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalSanctionsDateType::valueIsValid($dateType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalSanctionsDateType', is_array($dateType) ? implode(', ', $dateType) : var_export($dateType, true), implode(', ', \ID3Global\Enums\GlobalSanctionsDateType::getValidValues())), __LINE__);
        }
        $this->DateType = $dateType;
        
        return $this;
    }
    /**
     * Get Day value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getDay(): ?int
    {
        return isset($this->Day) ? $this->Day : null;
    }
    /**
     * Set Day value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $day
     * @return \ID3Global\Models\GlobalSanctionsDate
     */
    public function setDay(?int $day = null): self
    {
        // validation for constraint: int
        if (!is_null($day) && !(is_int($day) || ctype_digit($day))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($day, true), gettype($day)), __LINE__);
        }
        if (is_null($day) || (is_array($day) && empty($day))) {
            unset($this->Day);
        } else {
            $this->Day = $day;
        }
        
        return $this;
    }
    /**
     * Get Month value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getMonth(): ?int
    {
        return isset($this->Month) ? $this->Month : null;
    }
    /**
     * Set Month value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $month
     * @return \ID3Global\Models\GlobalSanctionsDate
     */
    public function setMonth(?int $month = null): self
    {
        // validation for constraint: int
        if (!is_null($month) && !(is_int($month) || ctype_digit($month))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($month, true), gettype($month)), __LINE__);
        }
        if (is_null($month) || (is_array($month) && empty($month))) {
            unset($this->Month);
        } else {
            $this->Month = $month;
        }
        
        return $this;
    }
    /**
     * Get Year value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getYear(): ?int
    {
        return isset($this->Year) ? $this->Year : null;
    }
    /**
     * Set Year value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $year
     * @return \ID3Global\Models\GlobalSanctionsDate
     */
    public function setYear(?int $year = null): self
    {
        // validation for constraint: int
        if (!is_null($year) && !(is_int($year) || ctype_digit($year))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($year, true), gettype($year)), __LINE__);
        }
        if (is_null($year) || (is_array($year) && empty($year))) {
            unset($this->Year);
        } else {
            $this->Year = $year;
        }
        
        return $this;
    }
}
