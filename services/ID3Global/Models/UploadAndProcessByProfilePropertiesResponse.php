<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UploadAndProcessByProfilePropertiesResponse Models
 * @subpackage Structs
 */
class UploadAndProcessByProfilePropertiesResponse extends AbstractStructBase
{
    /**
     * The UploadAndProcessByProfilePropertiesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDIVData|null
     */
    protected ?\ID3Global\Models\GlobalDIVData $UploadAndProcessByProfilePropertiesResult = null;
    /**
     * Constructor method for UploadAndProcessByProfilePropertiesResponse
     * @uses UploadAndProcessByProfilePropertiesResponse::setUploadAndProcessByProfilePropertiesResult()
     * @param \ID3Global\Models\GlobalDIVData $uploadAndProcessByProfilePropertiesResult
     */
    public function __construct(?\ID3Global\Models\GlobalDIVData $uploadAndProcessByProfilePropertiesResult = null)
    {
        $this
            ->setUploadAndProcessByProfilePropertiesResult($uploadAndProcessByProfilePropertiesResult);
    }
    /**
     * Get UploadAndProcessByProfilePropertiesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDIVData|null
     */
    public function getUploadAndProcessByProfilePropertiesResult(): ?\ID3Global\Models\GlobalDIVData
    {
        return isset($this->UploadAndProcessByProfilePropertiesResult) ? $this->UploadAndProcessByProfilePropertiesResult : null;
    }
    /**
     * Set UploadAndProcessByProfilePropertiesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalDIVData $uploadAndProcessByProfilePropertiesResult
     * @return \ID3Global\Models\UploadAndProcessByProfilePropertiesResponse
     */
    public function setUploadAndProcessByProfilePropertiesResult(?\ID3Global\Models\GlobalDIVData $uploadAndProcessByProfilePropertiesResult = null): self
    {
        if (is_null($uploadAndProcessByProfilePropertiesResult) || (is_array($uploadAndProcessByProfilePropertiesResult) && empty($uploadAndProcessByProfilePropertiesResult))) {
            unset($this->UploadAndProcessByProfilePropertiesResult);
        } else {
            $this->UploadAndProcessByProfilePropertiesResult = $uploadAndProcessByProfilePropertiesResult;
        }
        
        return $this;
    }
}
