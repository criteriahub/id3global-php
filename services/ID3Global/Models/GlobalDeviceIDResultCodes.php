<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDeviceIDResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q51:GlobalDeviceIDResultCodes
 * @subpackage Structs
 */
class GlobalDeviceIDResultCodes extends GlobalItemCheckResultCodes
{
    /**
     * The DeviceID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DeviceID = null;
    /**
     * The TrackingNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $TrackingNumber = null;
    /**
     * Constructor method for GlobalDeviceIDResultCodes
     * @uses GlobalDeviceIDResultCodes::setDeviceID()
     * @uses GlobalDeviceIDResultCodes::setTrackingNumber()
     * @param string $deviceID
     * @param string $trackingNumber
     */
    public function __construct(?string $deviceID = null, ?string $trackingNumber = null)
    {
        $this
            ->setDeviceID($deviceID)
            ->setTrackingNumber($trackingNumber);
    }
    /**
     * Get DeviceID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDeviceID(): ?string
    {
        return isset($this->DeviceID) ? $this->DeviceID : null;
    }
    /**
     * Set DeviceID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $deviceID
     * @return \ID3Global\Models\GlobalDeviceIDResultCodes
     */
    public function setDeviceID(?string $deviceID = null): self
    {
        // validation for constraint: string
        if (!is_null($deviceID) && !is_string($deviceID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deviceID, true), gettype($deviceID)), __LINE__);
        }
        if (is_null($deviceID) || (is_array($deviceID) && empty($deviceID))) {
            unset($this->DeviceID);
        } else {
            $this->DeviceID = $deviceID;
        }
        
        return $this;
    }
    /**
     * Get TrackingNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTrackingNumber(): ?string
    {
        return isset($this->TrackingNumber) ? $this->TrackingNumber : null;
    }
    /**
     * Set TrackingNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $trackingNumber
     * @return \ID3Global\Models\GlobalDeviceIDResultCodes
     */
    public function setTrackingNumber(?string $trackingNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($trackingNumber) && !is_string($trackingNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($trackingNumber, true), gettype($trackingNumber)), __LINE__);
        }
        if (is_null($trackingNumber) || (is_array($trackingNumber) && empty($trackingNumber))) {
            unset($this->TrackingNumber);
        } else {
            $this->TrackingNumber = $trackingNumber;
        }
        
        return $this;
    }
}
