<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseNotificationJob Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q742:GlobalCaseNotificationJob
 * @subpackage Structs
 */
class GlobalCaseNotificationJob extends AbstractStructBase
{
    /**
     * The NotificationType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $NotificationType = null;
    /**
     * The State
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $State = null;
    /**
     * The TokenRequired
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $TokenRequired = null;
    /**
     * The Recipient
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Recipient = null;
    /**
     * The SentDates
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfdateTime|null
     */
    protected ?\ID3Global\Arrays\ArrayOfdateTime $SentDates = null;
    /**
     * The RecipientOrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $RecipientOrgID = null;
    /**
     * The RecipientDomain
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $RecipientDomain = null;
    /**
     * The SenderName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SenderName = null;
    /**
     * The SenderOrgName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SenderOrgName = null;
    /**
     * The Dismissed
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Dismissed = null;
    /**
     * Constructor method for GlobalCaseNotificationJob
     * @uses GlobalCaseNotificationJob::setNotificationType()
     * @uses GlobalCaseNotificationJob::setState()
     * @uses GlobalCaseNotificationJob::setTokenRequired()
     * @uses GlobalCaseNotificationJob::setRecipient()
     * @uses GlobalCaseNotificationJob::setSentDates()
     * @uses GlobalCaseNotificationJob::setRecipientOrgID()
     * @uses GlobalCaseNotificationJob::setRecipientDomain()
     * @uses GlobalCaseNotificationJob::setSenderName()
     * @uses GlobalCaseNotificationJob::setSenderOrgName()
     * @uses GlobalCaseNotificationJob::setDismissed()
     * @param int $notificationType
     * @param int $state
     * @param bool $tokenRequired
     * @param int $recipient
     * @param \ID3Global\Arrays\ArrayOfdateTime $sentDates
     * @param string $recipientOrgID
     * @param string $recipientDomain
     * @param string $senderName
     * @param string $senderOrgName
     * @param bool $dismissed
     */
    public function __construct(?int $notificationType = null, ?int $state = null, ?bool $tokenRequired = null, ?int $recipient = null, ?\ID3Global\Arrays\ArrayOfdateTime $sentDates = null, ?string $recipientOrgID = null, ?string $recipientDomain = null, ?string $senderName = null, ?string $senderOrgName = null, ?bool $dismissed = null)
    {
        $this
            ->setNotificationType($notificationType)
            ->setState($state)
            ->setTokenRequired($tokenRequired)
            ->setRecipient($recipient)
            ->setSentDates($sentDates)
            ->setRecipientOrgID($recipientOrgID)
            ->setRecipientDomain($recipientDomain)
            ->setSenderName($senderName)
            ->setSenderOrgName($senderOrgName)
            ->setDismissed($dismissed);
    }
    /**
     * Get NotificationType value
     * @return int|null
     */
    public function getNotificationType(): ?int
    {
        return $this->NotificationType;
    }
    /**
     * Set NotificationType value
     * @param int $notificationType
     * @return \ID3Global\Models\GlobalCaseNotificationJob
     */
    public function setNotificationType(?int $notificationType = null): self
    {
        // validation for constraint: int
        if (!is_null($notificationType) && !(is_int($notificationType) || ctype_digit($notificationType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($notificationType, true), gettype($notificationType)), __LINE__);
        }
        $this->NotificationType = $notificationType;
        
        return $this;
    }
    /**
     * Get State value
     * @return int|null
     */
    public function getState(): ?int
    {
        return $this->State;
    }
    /**
     * Set State value
     * @param int $state
     * @return \ID3Global\Models\GlobalCaseNotificationJob
     */
    public function setState(?int $state = null): self
    {
        // validation for constraint: int
        if (!is_null($state) && !(is_int($state) || ctype_digit($state))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($state, true), gettype($state)), __LINE__);
        }
        $this->State = $state;
        
        return $this;
    }
    /**
     * Get TokenRequired value
     * @return bool|null
     */
    public function getTokenRequired(): ?bool
    {
        return $this->TokenRequired;
    }
    /**
     * Set TokenRequired value
     * @param bool $tokenRequired
     * @return \ID3Global\Models\GlobalCaseNotificationJob
     */
    public function setTokenRequired(?bool $tokenRequired = null): self
    {
        // validation for constraint: boolean
        if (!is_null($tokenRequired) && !is_bool($tokenRequired)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($tokenRequired, true), gettype($tokenRequired)), __LINE__);
        }
        $this->TokenRequired = $tokenRequired;
        
        return $this;
    }
    /**
     * Get Recipient value
     * @return int|null
     */
    public function getRecipient(): ?int
    {
        return $this->Recipient;
    }
    /**
     * Set Recipient value
     * @param int $recipient
     * @return \ID3Global\Models\GlobalCaseNotificationJob
     */
    public function setRecipient(?int $recipient = null): self
    {
        // validation for constraint: int
        if (!is_null($recipient) && !(is_int($recipient) || ctype_digit($recipient))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($recipient, true), gettype($recipient)), __LINE__);
        }
        $this->Recipient = $recipient;
        
        return $this;
    }
    /**
     * Get SentDates value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfdateTime|null
     */
    public function getSentDates(): ?\ID3Global\Arrays\ArrayOfdateTime
    {
        return isset($this->SentDates) ? $this->SentDates : null;
    }
    /**
     * Set SentDates value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfdateTime $sentDates
     * @return \ID3Global\Models\GlobalCaseNotificationJob
     */
    public function setSentDates(?\ID3Global\Arrays\ArrayOfdateTime $sentDates = null): self
    {
        if (is_null($sentDates) || (is_array($sentDates) && empty($sentDates))) {
            unset($this->SentDates);
        } else {
            $this->SentDates = $sentDates;
        }
        
        return $this;
    }
    /**
     * Get RecipientOrgID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRecipientOrgID(): ?string
    {
        return isset($this->RecipientOrgID) ? $this->RecipientOrgID : null;
    }
    /**
     * Set RecipientOrgID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $recipientOrgID
     * @return \ID3Global\Models\GlobalCaseNotificationJob
     */
    public function setRecipientOrgID(?string $recipientOrgID = null): self
    {
        // validation for constraint: string
        if (!is_null($recipientOrgID) && !is_string($recipientOrgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($recipientOrgID, true), gettype($recipientOrgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($recipientOrgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $recipientOrgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($recipientOrgID, true)), __LINE__);
        }
        if (is_null($recipientOrgID) || (is_array($recipientOrgID) && empty($recipientOrgID))) {
            unset($this->RecipientOrgID);
        } else {
            $this->RecipientOrgID = $recipientOrgID;
        }
        
        return $this;
    }
    /**
     * Get RecipientDomain value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRecipientDomain(): ?string
    {
        return isset($this->RecipientDomain) ? $this->RecipientDomain : null;
    }
    /**
     * Set RecipientDomain value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $recipientDomain
     * @return \ID3Global\Models\GlobalCaseNotificationJob
     */
    public function setRecipientDomain(?string $recipientDomain = null): self
    {
        // validation for constraint: string
        if (!is_null($recipientDomain) && !is_string($recipientDomain)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($recipientDomain, true), gettype($recipientDomain)), __LINE__);
        }
        if (is_null($recipientDomain) || (is_array($recipientDomain) && empty($recipientDomain))) {
            unset($this->RecipientDomain);
        } else {
            $this->RecipientDomain = $recipientDomain;
        }
        
        return $this;
    }
    /**
     * Get SenderName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSenderName(): ?string
    {
        return isset($this->SenderName) ? $this->SenderName : null;
    }
    /**
     * Set SenderName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $senderName
     * @return \ID3Global\Models\GlobalCaseNotificationJob
     */
    public function setSenderName(?string $senderName = null): self
    {
        // validation for constraint: string
        if (!is_null($senderName) && !is_string($senderName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($senderName, true), gettype($senderName)), __LINE__);
        }
        if (is_null($senderName) || (is_array($senderName) && empty($senderName))) {
            unset($this->SenderName);
        } else {
            $this->SenderName = $senderName;
        }
        
        return $this;
    }
    /**
     * Get SenderOrgName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSenderOrgName(): ?string
    {
        return isset($this->SenderOrgName) ? $this->SenderOrgName : null;
    }
    /**
     * Set SenderOrgName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $senderOrgName
     * @return \ID3Global\Models\GlobalCaseNotificationJob
     */
    public function setSenderOrgName(?string $senderOrgName = null): self
    {
        // validation for constraint: string
        if (!is_null($senderOrgName) && !is_string($senderOrgName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($senderOrgName, true), gettype($senderOrgName)), __LINE__);
        }
        if (is_null($senderOrgName) || (is_array($senderOrgName) && empty($senderOrgName))) {
            unset($this->SenderOrgName);
        } else {
            $this->SenderOrgName = $senderOrgName;
        }
        
        return $this;
    }
    /**
     * Get Dismissed value
     * @return bool|null
     */
    public function getDismissed(): ?bool
    {
        return $this->Dismissed;
    }
    /**
     * Set Dismissed value
     * @param bool $dismissed
     * @return \ID3Global\Models\GlobalCaseNotificationJob
     */
    public function setDismissed(?bool $dismissed = null): self
    {
        // validation for constraint: boolean
        if (!is_null($dismissed) && !is_bool($dismissed)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($dismissed, true), gettype($dismissed)), __LINE__);
        }
        $this->Dismissed = $dismissed;
        
        return $this;
    }
}
