<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalMatrixResultItem Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q811:GlobalMatrixResultItem
 * @subpackage Structs
 */
class GlobalMatrixResultItem extends AbstractStructBase
{
    /**
     * The MatchType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $MatchType = null;
    /**
     * The Reason
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Reason = null;
    /**
     * The Key
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Key = null;
    /**
     * The CellKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CellKey = null;
    /**
     * The CaptionResourceKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CaptionResourceKey = null;
    /**
     * The DescriptionResourceKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DescriptionResourceKey = null;
    /**
     * The RecommendResourceKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $RecommendResourceKey = null;
    /**
     * The NotesResourceKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $NotesResourceKey = null;
    /**
     * Constructor method for GlobalMatrixResultItem
     * @uses GlobalMatrixResultItem::setMatchType()
     * @uses GlobalMatrixResultItem::setReason()
     * @uses GlobalMatrixResultItem::setKey()
     * @uses GlobalMatrixResultItem::setCellKey()
     * @uses GlobalMatrixResultItem::setCaptionResourceKey()
     * @uses GlobalMatrixResultItem::setDescriptionResourceKey()
     * @uses GlobalMatrixResultItem::setRecommendResourceKey()
     * @uses GlobalMatrixResultItem::setNotesResourceKey()
     * @param string $matchType
     * @param string $reason
     * @param string $key
     * @param string $cellKey
     * @param string $captionResourceKey
     * @param string $descriptionResourceKey
     * @param string $recommendResourceKey
     * @param string $notesResourceKey
     */
    public function __construct(?string $matchType = null, ?string $reason = null, ?string $key = null, ?string $cellKey = null, ?string $captionResourceKey = null, ?string $descriptionResourceKey = null, ?string $recommendResourceKey = null, ?string $notesResourceKey = null)
    {
        $this
            ->setMatchType($matchType)
            ->setReason($reason)
            ->setKey($key)
            ->setCellKey($cellKey)
            ->setCaptionResourceKey($captionResourceKey)
            ->setDescriptionResourceKey($descriptionResourceKey)
            ->setRecommendResourceKey($recommendResourceKey)
            ->setNotesResourceKey($notesResourceKey);
    }
    /**
     * Get MatchType value
     * @return string|null
     */
    public function getMatchType(): ?string
    {
        return $this->MatchType;
    }
    /**
     * Set MatchType value
     * @uses \ID3Global\Enums\GlobalMatrixMatchType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalMatrixMatchType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $matchType
     * @return \ID3Global\Models\GlobalMatrixResultItem
     */
    public function setMatchType(?string $matchType = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalMatrixMatchType::valueIsValid($matchType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalMatrixMatchType', is_array($matchType) ? implode(', ', $matchType) : var_export($matchType, true), implode(', ', \ID3Global\Enums\GlobalMatrixMatchType::getValidValues())), __LINE__);
        }
        $this->MatchType = $matchType;
        
        return $this;
    }
    /**
     * Get Reason value
     * @return string|null
     */
    public function getReason(): ?string
    {
        return $this->Reason;
    }
    /**
     * Set Reason value
     * @uses \ID3Global\Enums\GlobalMatrixMatchReason::valueIsValid()
     * @uses \ID3Global\Enums\GlobalMatrixMatchReason::getValidValues()
     * @throws InvalidArgumentException
     * @param string $reason
     * @return \ID3Global\Models\GlobalMatrixResultItem
     */
    public function setReason(?string $reason = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalMatrixMatchReason::valueIsValid($reason)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalMatrixMatchReason', is_array($reason) ? implode(', ', $reason) : var_export($reason, true), implode(', ', \ID3Global\Enums\GlobalMatrixMatchReason::getValidValues())), __LINE__);
        }
        $this->Reason = $reason;
        
        return $this;
    }
    /**
     * Get Key value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getKey(): ?string
    {
        return isset($this->Key) ? $this->Key : null;
    }
    /**
     * Set Key value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $key
     * @return \ID3Global\Models\GlobalMatrixResultItem
     */
    public function setKey(?string $key = null): self
    {
        // validation for constraint: string
        if (!is_null($key) && !is_string($key)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($key, true), gettype($key)), __LINE__);
        }
        if (is_null($key) || (is_array($key) && empty($key))) {
            unset($this->Key);
        } else {
            $this->Key = $key;
        }
        
        return $this;
    }
    /**
     * Get CellKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCellKey(): ?string
    {
        return isset($this->CellKey) ? $this->CellKey : null;
    }
    /**
     * Set CellKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $cellKey
     * @return \ID3Global\Models\GlobalMatrixResultItem
     */
    public function setCellKey(?string $cellKey = null): self
    {
        // validation for constraint: string
        if (!is_null($cellKey) && !is_string($cellKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cellKey, true), gettype($cellKey)), __LINE__);
        }
        if (is_null($cellKey) || (is_array($cellKey) && empty($cellKey))) {
            unset($this->CellKey);
        } else {
            $this->CellKey = $cellKey;
        }
        
        return $this;
    }
    /**
     * Get CaptionResourceKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCaptionResourceKey(): ?string
    {
        return isset($this->CaptionResourceKey) ? $this->CaptionResourceKey : null;
    }
    /**
     * Set CaptionResourceKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $captionResourceKey
     * @return \ID3Global\Models\GlobalMatrixResultItem
     */
    public function setCaptionResourceKey(?string $captionResourceKey = null): self
    {
        // validation for constraint: string
        if (!is_null($captionResourceKey) && !is_string($captionResourceKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($captionResourceKey, true), gettype($captionResourceKey)), __LINE__);
        }
        if (is_null($captionResourceKey) || (is_array($captionResourceKey) && empty($captionResourceKey))) {
            unset($this->CaptionResourceKey);
        } else {
            $this->CaptionResourceKey = $captionResourceKey;
        }
        
        return $this;
    }
    /**
     * Get DescriptionResourceKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescriptionResourceKey(): ?string
    {
        return isset($this->DescriptionResourceKey) ? $this->DescriptionResourceKey : null;
    }
    /**
     * Set DescriptionResourceKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $descriptionResourceKey
     * @return \ID3Global\Models\GlobalMatrixResultItem
     */
    public function setDescriptionResourceKey(?string $descriptionResourceKey = null): self
    {
        // validation for constraint: string
        if (!is_null($descriptionResourceKey) && !is_string($descriptionResourceKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($descriptionResourceKey, true), gettype($descriptionResourceKey)), __LINE__);
        }
        if (is_null($descriptionResourceKey) || (is_array($descriptionResourceKey) && empty($descriptionResourceKey))) {
            unset($this->DescriptionResourceKey);
        } else {
            $this->DescriptionResourceKey = $descriptionResourceKey;
        }
        
        return $this;
    }
    /**
     * Get RecommendResourceKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRecommendResourceKey(): ?string
    {
        return isset($this->RecommendResourceKey) ? $this->RecommendResourceKey : null;
    }
    /**
     * Set RecommendResourceKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $recommendResourceKey
     * @return \ID3Global\Models\GlobalMatrixResultItem
     */
    public function setRecommendResourceKey(?string $recommendResourceKey = null): self
    {
        // validation for constraint: string
        if (!is_null($recommendResourceKey) && !is_string($recommendResourceKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($recommendResourceKey, true), gettype($recommendResourceKey)), __LINE__);
        }
        if (is_null($recommendResourceKey) || (is_array($recommendResourceKey) && empty($recommendResourceKey))) {
            unset($this->RecommendResourceKey);
        } else {
            $this->RecommendResourceKey = $recommendResourceKey;
        }
        
        return $this;
    }
    /**
     * Get NotesResourceKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNotesResourceKey(): ?string
    {
        return isset($this->NotesResourceKey) ? $this->NotesResourceKey : null;
    }
    /**
     * Set NotesResourceKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $notesResourceKey
     * @return \ID3Global\Models\GlobalMatrixResultItem
     */
    public function setNotesResourceKey(?string $notesResourceKey = null): self
    {
        // validation for constraint: string
        if (!is_null($notesResourceKey) && !is_string($notesResourceKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($notesResourceKey, true), gettype($notesResourceKey)), __LINE__);
        }
        if (is_null($notesResourceKey) || (is_array($notesResourceKey) && empty($notesResourceKey))) {
            unset($this->NotesResourceKey);
        } else {
            $this->NotesResourceKey = $notesResourceKey;
        }
        
        return $this;
    }
}
