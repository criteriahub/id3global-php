<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProfileDetailsResponse Models
 * @subpackage Structs
 */
class GetProfileDetailsResponse extends AbstractStructBase
{
    /**
     * The GetProfileDetailsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileDetails|null
     */
    protected ?\ID3Global\Models\GlobalProfileDetails $GetProfileDetailsResult = null;
    /**
     * Constructor method for GetProfileDetailsResponse
     * @uses GetProfileDetailsResponse::setGetProfileDetailsResult()
     * @param \ID3Global\Models\GlobalProfileDetails $getProfileDetailsResult
     */
    public function __construct(?\ID3Global\Models\GlobalProfileDetails $getProfileDetailsResult = null)
    {
        $this
            ->setGetProfileDetailsResult($getProfileDetailsResult);
    }
    /**
     * Get GetProfileDetailsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileDetails|null
     */
    public function getGetProfileDetailsResult(): ?\ID3Global\Models\GlobalProfileDetails
    {
        return isset($this->GetProfileDetailsResult) ? $this->GetProfileDetailsResult : null;
    }
    /**
     * Set GetProfileDetailsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileDetails $getProfileDetailsResult
     * @return \ID3Global\Models\GetProfileDetailsResponse
     */
    public function setGetProfileDetailsResult(?\ID3Global\Models\GlobalProfileDetails $getProfileDetailsResult = null): self
    {
        if (is_null($getProfileDetailsResult) || (is_array($getProfileDetailsResult) && empty($getProfileDetailsResult))) {
            unset($this->GetProfileDetailsResult);
        } else {
            $this->GetProfileDetailsResult = $getProfileDetailsResult;
        }
        
        return $this;
    }
}
