<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDocumentTypesResponse Models
 * @subpackage Structs
 */
class GetDocumentTypesResponse extends AbstractStructBase
{
    /**
     * The GetDocumentTypesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $GetDocumentTypesResult = null;
    /**
     * Constructor method for GetDocumentTypesResponse
     * @uses GetDocumentTypesResponse::setGetDocumentTypesResult()
     * @param \ID3Global\Arrays\ArrayOfstring $getDocumentTypesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfstring $getDocumentTypesResult = null)
    {
        $this
            ->setGetDocumentTypesResult($getDocumentTypesResult);
    }
    /**
     * Get GetDocumentTypesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getGetDocumentTypesResult(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->GetDocumentTypesResult) ? $this->GetDocumentTypesResult : null;
    }
    /**
     * Set GetDocumentTypesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $getDocumentTypesResult
     * @return \ID3Global\Models\GetDocumentTypesResponse
     */
    public function setGetDocumentTypesResult(?\ID3Global\Arrays\ArrayOfstring $getDocumentTypesResult = null): self
    {
        if (is_null($getDocumentTypesResult) || (is_array($getDocumentTypesResult) && empty($getDocumentTypesResult))) {
            unset($this->GetDocumentTypesResult);
        } else {
            $this->GetDocumentTypesResult = $getDocumentTypesResult;
        }
        
        return $this;
    }
}
