<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAccountDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q5:GlobalAccountDetails
 * @subpackage Structs
 */
class GlobalAccountDetails extends GlobalAccount
{
    /**
     * The RequireChangePassword
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $RequireChangePassword = null;
    /**
     * The DisabledByInactivity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $DisabledByInactivity = null;
    /**
     * The DisabledByAdministrator
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $DisabledByAdministrator = null;
    /**
     * The PIN
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $PIN = null;
    /**
     * The PasswordNeverExpires
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $PasswordNeverExpires = null;
    /**
     * The IsAdminPolicy
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsAdminPolicy = null;
    /**
     * The FailedAttempts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $FailedAttempts = null;
    /**
     * Constructor method for GlobalAccountDetails
     * @uses GlobalAccountDetails::setRequireChangePassword()
     * @uses GlobalAccountDetails::setDisabledByInactivity()
     * @uses GlobalAccountDetails::setDisabledByAdministrator()
     * @uses GlobalAccountDetails::setPIN()
     * @uses GlobalAccountDetails::setPasswordNeverExpires()
     * @uses GlobalAccountDetails::setIsAdminPolicy()
     * @uses GlobalAccountDetails::setFailedAttempts()
     * @param bool $requireChangePassword
     * @param bool $disabledByInactivity
     * @param bool $disabledByAdministrator
     * @param string $pIN
     * @param bool $passwordNeverExpires
     * @param bool $isAdminPolicy
     * @param int $failedAttempts
     */
    public function __construct(?bool $requireChangePassword = null, ?bool $disabledByInactivity = null, ?bool $disabledByAdministrator = null, ?string $pIN = null, ?bool $passwordNeverExpires = null, ?bool $isAdminPolicy = null, ?int $failedAttempts = null)
    {
        $this
            ->setRequireChangePassword($requireChangePassword)
            ->setDisabledByInactivity($disabledByInactivity)
            ->setDisabledByAdministrator($disabledByAdministrator)
            ->setPIN($pIN)
            ->setPasswordNeverExpires($passwordNeverExpires)
            ->setIsAdminPolicy($isAdminPolicy)
            ->setFailedAttempts($failedAttempts);
    }
    /**
     * Get RequireChangePassword value
     * @return bool|null
     */
    public function getRequireChangePassword(): ?bool
    {
        return $this->RequireChangePassword;
    }
    /**
     * Set RequireChangePassword value
     * @param bool $requireChangePassword
     * @return \ID3Global\Models\GlobalAccountDetails
     */
    public function setRequireChangePassword(?bool $requireChangePassword = null): self
    {
        // validation for constraint: boolean
        if (!is_null($requireChangePassword) && !is_bool($requireChangePassword)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($requireChangePassword, true), gettype($requireChangePassword)), __LINE__);
        }
        $this->RequireChangePassword = $requireChangePassword;
        
        return $this;
    }
    /**
     * Get DisabledByInactivity value
     * @return bool|null
     */
    public function getDisabledByInactivity(): ?bool
    {
        return $this->DisabledByInactivity;
    }
    /**
     * Set DisabledByInactivity value
     * @param bool $disabledByInactivity
     * @return \ID3Global\Models\GlobalAccountDetails
     */
    public function setDisabledByInactivity(?bool $disabledByInactivity = null): self
    {
        // validation for constraint: boolean
        if (!is_null($disabledByInactivity) && !is_bool($disabledByInactivity)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($disabledByInactivity, true), gettype($disabledByInactivity)), __LINE__);
        }
        $this->DisabledByInactivity = $disabledByInactivity;
        
        return $this;
    }
    /**
     * Get DisabledByAdministrator value
     * @return bool|null
     */
    public function getDisabledByAdministrator(): ?bool
    {
        return $this->DisabledByAdministrator;
    }
    /**
     * Set DisabledByAdministrator value
     * @param bool $disabledByAdministrator
     * @return \ID3Global\Models\GlobalAccountDetails
     */
    public function setDisabledByAdministrator(?bool $disabledByAdministrator = null): self
    {
        // validation for constraint: boolean
        if (!is_null($disabledByAdministrator) && !is_bool($disabledByAdministrator)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($disabledByAdministrator, true), gettype($disabledByAdministrator)), __LINE__);
        }
        $this->DisabledByAdministrator = $disabledByAdministrator;
        
        return $this;
    }
    /**
     * Get PIN value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPIN(): ?string
    {
        return isset($this->PIN) ? $this->PIN : null;
    }
    /**
     * Set PIN value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $pIN
     * @return \ID3Global\Models\GlobalAccountDetails
     */
    public function setPIN(?string $pIN = null): self
    {
        // validation for constraint: string
        if (!is_null($pIN) && !is_string($pIN)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pIN, true), gettype($pIN)), __LINE__);
        }
        if (is_null($pIN) || (is_array($pIN) && empty($pIN))) {
            unset($this->PIN);
        } else {
            $this->PIN = $pIN;
        }
        
        return $this;
    }
    /**
     * Get PasswordNeverExpires value
     * @return bool|null
     */
    public function getPasswordNeverExpires(): ?bool
    {
        return $this->PasswordNeverExpires;
    }
    /**
     * Set PasswordNeverExpires value
     * @param bool $passwordNeverExpires
     * @return \ID3Global\Models\GlobalAccountDetails
     */
    public function setPasswordNeverExpires(?bool $passwordNeverExpires = null): self
    {
        // validation for constraint: boolean
        if (!is_null($passwordNeverExpires) && !is_bool($passwordNeverExpires)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($passwordNeverExpires, true), gettype($passwordNeverExpires)), __LINE__);
        }
        $this->PasswordNeverExpires = $passwordNeverExpires;
        
        return $this;
    }
    /**
     * Get IsAdminPolicy value
     * @return bool|null
     */
    public function getIsAdminPolicy(): ?bool
    {
        return $this->IsAdminPolicy;
    }
    /**
     * Set IsAdminPolicy value
     * @param bool $isAdminPolicy
     * @return \ID3Global\Models\GlobalAccountDetails
     */
    public function setIsAdminPolicy(?bool $isAdminPolicy = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isAdminPolicy) && !is_bool($isAdminPolicy)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isAdminPolicy, true), gettype($isAdminPolicy)), __LINE__);
        }
        $this->IsAdminPolicy = $isAdminPolicy;
        
        return $this;
    }
    /**
     * Get FailedAttempts value
     * @return int|null
     */
    public function getFailedAttempts(): ?int
    {
        return $this->FailedAttempts;
    }
    /**
     * Set FailedAttempts value
     * @param int $failedAttempts
     * @return \ID3Global\Models\GlobalAccountDetails
     */
    public function setFailedAttempts(?int $failedAttempts = null): self
    {
        // validation for constraint: int
        if (!is_null($failedAttempts) && !(is_int($failedAttempts) || ctype_digit($failedAttempts))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($failedAttempts, true), gettype($failedAttempts)), __LINE__);
        }
        $this->FailedAttempts = $failedAttempts;
        
        return $this;
    }
}
