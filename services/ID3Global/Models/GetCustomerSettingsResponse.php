<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCustomerSettingsResponse Models
 * @subpackage Structs
 */
class GetCustomerSettingsResponse extends AbstractStructBase
{
    /**
     * The GetCustomerSettingsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseCustomerSettings|null
     */
    protected ?\ID3Global\Models\GlobalCaseCustomerSettings $GetCustomerSettingsResult = null;
    /**
     * Constructor method for GetCustomerSettingsResponse
     * @uses GetCustomerSettingsResponse::setGetCustomerSettingsResult()
     * @param \ID3Global\Models\GlobalCaseCustomerSettings $getCustomerSettingsResult
     */
    public function __construct(?\ID3Global\Models\GlobalCaseCustomerSettings $getCustomerSettingsResult = null)
    {
        $this
            ->setGetCustomerSettingsResult($getCustomerSettingsResult);
    }
    /**
     * Get GetCustomerSettingsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseCustomerSettings|null
     */
    public function getGetCustomerSettingsResult(): ?\ID3Global\Models\GlobalCaseCustomerSettings
    {
        return isset($this->GetCustomerSettingsResult) ? $this->GetCustomerSettingsResult : null;
    }
    /**
     * Set GetCustomerSettingsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCaseCustomerSettings $getCustomerSettingsResult
     * @return \ID3Global\Models\GetCustomerSettingsResponse
     */
    public function setGetCustomerSettingsResult(?\ID3Global\Models\GlobalCaseCustomerSettings $getCustomerSettingsResult = null): self
    {
        if (is_null($getCustomerSettingsResult) || (is_array($getCustomerSettingsResult) && empty($getCustomerSettingsResult))) {
            unset($this->GetCustomerSettingsResult);
        } else {
            $this->GetCustomerSettingsResult = $getCustomerSettingsResult;
        }
        
        return $this;
    }
}
