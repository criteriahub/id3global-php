<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDataExtractsResponse Models
 * @subpackage Structs
 */
class GetDataExtractsResponse extends AbstractStructBase
{
    /**
     * The GetDataExtractsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDataExtracts|null
     */
    protected ?\ID3Global\Models\GlobalDataExtracts $GetDataExtractsResult = null;
    /**
     * Constructor method for GetDataExtractsResponse
     * @uses GetDataExtractsResponse::setGetDataExtractsResult()
     * @param \ID3Global\Models\GlobalDataExtracts $getDataExtractsResult
     */
    public function __construct(?\ID3Global\Models\GlobalDataExtracts $getDataExtractsResult = null)
    {
        $this
            ->setGetDataExtractsResult($getDataExtractsResult);
    }
    /**
     * Get GetDataExtractsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDataExtracts|null
     */
    public function getGetDataExtractsResult(): ?\ID3Global\Models\GlobalDataExtracts
    {
        return isset($this->GetDataExtractsResult) ? $this->GetDataExtractsResult : null;
    }
    /**
     * Set GetDataExtractsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalDataExtracts $getDataExtractsResult
     * @return \ID3Global\Models\GetDataExtractsResponse
     */
    public function setGetDataExtractsResult(?\ID3Global\Models\GlobalDataExtracts $getDataExtractsResult = null): self
    {
        if (is_null($getDataExtractsResult) || (is_array($getDataExtractsResult) && empty($getDataExtractsResult))) {
            unset($this->GetDataExtractsResult);
        } else {
            $this->GetDataExtractsResult = $getDataExtractsResult;
        }
        
        return $this;
    }
}
