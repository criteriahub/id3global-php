<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSanctionsResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q45:GlobalSanctionsResultCodes
 * @subpackage Structs
 */
class GlobalSanctionsResultCodes extends GlobalItemCheckResultCodes
{
    /**
     * The SanctionsMatches
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSanctionsMatch|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSanctionsMatch $SanctionsMatches = null;
    /**
     * Constructor method for GlobalSanctionsResultCodes
     * @uses GlobalSanctionsResultCodes::setSanctionsMatches()
     * @param \ID3Global\Arrays\ArrayOfGlobalSanctionsMatch $sanctionsMatches
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalSanctionsMatch $sanctionsMatches = null)
    {
        $this
            ->setSanctionsMatches($sanctionsMatches);
    }
    /**
     * Get SanctionsMatches value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSanctionsMatch|null
     */
    public function getSanctionsMatches(): ?\ID3Global\Arrays\ArrayOfGlobalSanctionsMatch
    {
        return isset($this->SanctionsMatches) ? $this->SanctionsMatches : null;
    }
    /**
     * Set SanctionsMatches value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSanctionsMatch $sanctionsMatches
     * @return \ID3Global\Models\GlobalSanctionsResultCodes
     */
    public function setSanctionsMatches(?\ID3Global\Arrays\ArrayOfGlobalSanctionsMatch $sanctionsMatches = null): self
    {
        if (is_null($sanctionsMatches) || (is_array($sanctionsMatches) && empty($sanctionsMatches))) {
            unset($this->SanctionsMatches);
        } else {
            $this->SanctionsMatches = $sanctionsMatches;
        }
        
        return $this;
    }
}
