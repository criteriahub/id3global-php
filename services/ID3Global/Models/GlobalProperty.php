<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalProperty Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q237:GlobalProperty
 * @subpackage Structs
 */
class GlobalProperty extends AbstractStructBase
{
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Values
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfKeyValueOfstringboolean|null
     */
    protected ?\ID3Global\Arrays\ArrayOfKeyValueOfstringboolean $Values = null;
    /**
     * The MultipleValues
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $MultipleValues = null;
    /**
     * Constructor method for GlobalProperty
     * @uses GlobalProperty::setName()
     * @uses GlobalProperty::setValues()
     * @uses GlobalProperty::setMultipleValues()
     * @param string $name
     * @param \ID3Global\Arrays\ArrayOfKeyValueOfstringboolean $values
     * @param bool $multipleValues
     */
    public function __construct(?string $name = null, ?\ID3Global\Arrays\ArrayOfKeyValueOfstringboolean $values = null, ?bool $multipleValues = null)
    {
        $this
            ->setName($name)
            ->setValues($values)
            ->setMultipleValues($multipleValues);
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalProperty
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Values value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfKeyValueOfstringboolean|null
     */
    public function getValues(): ?\ID3Global\Arrays\ArrayOfKeyValueOfstringboolean
    {
        return isset($this->Values) ? $this->Values : null;
    }
    /**
     * Set Values value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfKeyValueOfstringboolean $values
     * @return \ID3Global\Models\GlobalProperty
     */
    public function setValues(?\ID3Global\Arrays\ArrayOfKeyValueOfstringboolean $values = null): self
    {
        if (is_null($values) || (is_array($values) && empty($values))) {
            unset($this->Values);
        } else {
            $this->Values = $values;
        }
        
        return $this;
    }
    /**
     * Get MultipleValues value
     * @return bool|null
     */
    public function getMultipleValues(): ?bool
    {
        return $this->MultipleValues;
    }
    /**
     * Set MultipleValues value
     * @param bool $multipleValues
     * @return \ID3Global\Models\GlobalProperty
     */
    public function setMultipleValues(?bool $multipleValues = null): self
    {
        // validation for constraint: boolean
        if (!is_null($multipleValues) && !is_bool($multipleValues)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($multipleValues, true), gettype($multipleValues)), __LINE__);
        }
        $this->MultipleValues = $multipleValues;
        
        return $this;
    }
}
