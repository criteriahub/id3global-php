<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDocImageMinSizeKBytesResponse Models
 * @subpackage Structs
 */
class GetDocImageMinSizeKBytesResponse extends AbstractStructBase
{
    /**
     * The GetDocImageMinSizeKBytesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $GetDocImageMinSizeKBytesResult = null;
    /**
     * Constructor method for GetDocImageMinSizeKBytesResponse
     * @uses GetDocImageMinSizeKBytesResponse::setGetDocImageMinSizeKBytesResult()
     * @param int $getDocImageMinSizeKBytesResult
     */
    public function __construct(?int $getDocImageMinSizeKBytesResult = null)
    {
        $this
            ->setGetDocImageMinSizeKBytesResult($getDocImageMinSizeKBytesResult);
    }
    /**
     * Get GetDocImageMinSizeKBytesResult value
     * @return int|null
     */
    public function getGetDocImageMinSizeKBytesResult(): ?int
    {
        return $this->GetDocImageMinSizeKBytesResult;
    }
    /**
     * Set GetDocImageMinSizeKBytesResult value
     * @param int $getDocImageMinSizeKBytesResult
     * @return \ID3Global\Models\GetDocImageMinSizeKBytesResponse
     */
    public function setGetDocImageMinSizeKBytesResult(?int $getDocImageMinSizeKBytesResult = null): self
    {
        // validation for constraint: int
        if (!is_null($getDocImageMinSizeKBytesResult) && !(is_int($getDocImageMinSizeKBytesResult) || ctype_digit($getDocImageMinSizeKBytesResult))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($getDocImageMinSizeKBytesResult, true), gettype($getDocImageMinSizeKBytesResult)), __LINE__);
        }
        $this->GetDocImageMinSizeKBytesResult = $getDocImageMinSizeKBytesResult;
        
        return $this;
    }
}
