<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSanctionsMatch Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q49:GlobalSanctionsMatch
 * @subpackage Structs
 */
class GlobalSanctionsMatch extends AbstractStructBase
{
    /**
     * The SanctionID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SanctionID = null;
    /**
     * The Url
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Url = null;
    /**
     * The Score
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Score = null;
    /**
     * The CaseID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CaseID = null;
    /**
     * The SearchID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SearchID = null;
    /**
     * The FCRAEnabled
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $FCRAEnabled = null;
    /**
     * The Tiers
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $Tiers = null;
    /**
     * The Rank
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Rank = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * Constructor method for GlobalSanctionsMatch
     * @uses GlobalSanctionsMatch::setSanctionID()
     * @uses GlobalSanctionsMatch::setUrl()
     * @uses GlobalSanctionsMatch::setScore()
     * @uses GlobalSanctionsMatch::setCaseID()
     * @uses GlobalSanctionsMatch::setSearchID()
     * @uses GlobalSanctionsMatch::setFCRAEnabled()
     * @uses GlobalSanctionsMatch::setTiers()
     * @uses GlobalSanctionsMatch::setRank()
     * @uses GlobalSanctionsMatch::setName()
     * @param string $sanctionID
     * @param string $url
     * @param int $score
     * @param string $caseID
     * @param string $searchID
     * @param bool $fCRAEnabled
     * @param \ID3Global\Arrays\ArrayOfstring $tiers
     * @param int $rank
     * @param string $name
     */
    public function __construct(?string $sanctionID = null, ?string $url = null, ?int $score = null, ?string $caseID = null, ?string $searchID = null, ?bool $fCRAEnabled = null, ?\ID3Global\Arrays\ArrayOfstring $tiers = null, ?int $rank = null, ?string $name = null)
    {
        $this
            ->setSanctionID($sanctionID)
            ->setUrl($url)
            ->setScore($score)
            ->setCaseID($caseID)
            ->setSearchID($searchID)
            ->setFCRAEnabled($fCRAEnabled)
            ->setTiers($tiers)
            ->setRank($rank)
            ->setName($name);
    }
    /**
     * Get SanctionID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSanctionID(): ?string
    {
        return isset($this->SanctionID) ? $this->SanctionID : null;
    }
    /**
     * Set SanctionID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sanctionID
     * @return \ID3Global\Models\GlobalSanctionsMatch
     */
    public function setSanctionID(?string $sanctionID = null): self
    {
        // validation for constraint: string
        if (!is_null($sanctionID) && !is_string($sanctionID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sanctionID, true), gettype($sanctionID)), __LINE__);
        }
        if (is_null($sanctionID) || (is_array($sanctionID) && empty($sanctionID))) {
            unset($this->SanctionID);
        } else {
            $this->SanctionID = $sanctionID;
        }
        
        return $this;
    }
    /**
     * Get Url value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return isset($this->Url) ? $this->Url : null;
    }
    /**
     * Set Url value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $url
     * @return \ID3Global\Models\GlobalSanctionsMatch
     */
    public function setUrl(?string $url = null): self
    {
        // validation for constraint: string
        if (!is_null($url) && !is_string($url)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($url, true), gettype($url)), __LINE__);
        }
        if (is_null($url) || (is_array($url) && empty($url))) {
            unset($this->Url);
        } else {
            $this->Url = $url;
        }
        
        return $this;
    }
    /**
     * Get Score value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getScore(): ?int
    {
        return isset($this->Score) ? $this->Score : null;
    }
    /**
     * Set Score value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $score
     * @return \ID3Global\Models\GlobalSanctionsMatch
     */
    public function setScore(?int $score = null): self
    {
        // validation for constraint: int
        if (!is_null($score) && !(is_int($score) || ctype_digit($score))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($score, true), gettype($score)), __LINE__);
        }
        if (is_null($score) || (is_array($score) && empty($score))) {
            unset($this->Score);
        } else {
            $this->Score = $score;
        }
        
        return $this;
    }
    /**
     * Get CaseID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCaseID(): ?string
    {
        return isset($this->CaseID) ? $this->CaseID : null;
    }
    /**
     * Set CaseID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $caseID
     * @return \ID3Global\Models\GlobalSanctionsMatch
     */
    public function setCaseID(?string $caseID = null): self
    {
        // validation for constraint: string
        if (!is_null($caseID) && !is_string($caseID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($caseID, true), gettype($caseID)), __LINE__);
        }
        if (is_null($caseID) || (is_array($caseID) && empty($caseID))) {
            unset($this->CaseID);
        } else {
            $this->CaseID = $caseID;
        }
        
        return $this;
    }
    /**
     * Get SearchID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSearchID(): ?string
    {
        return isset($this->SearchID) ? $this->SearchID : null;
    }
    /**
     * Set SearchID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $searchID
     * @return \ID3Global\Models\GlobalSanctionsMatch
     */
    public function setSearchID(?string $searchID = null): self
    {
        // validation for constraint: string
        if (!is_null($searchID) && !is_string($searchID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchID, true), gettype($searchID)), __LINE__);
        }
        if (is_null($searchID) || (is_array($searchID) && empty($searchID))) {
            unset($this->SearchID);
        } else {
            $this->SearchID = $searchID;
        }
        
        return $this;
    }
    /**
     * Get FCRAEnabled value
     * @return bool|null
     */
    public function getFCRAEnabled(): ?bool
    {
        return $this->FCRAEnabled;
    }
    /**
     * Set FCRAEnabled value
     * @param bool $fCRAEnabled
     * @return \ID3Global\Models\GlobalSanctionsMatch
     */
    public function setFCRAEnabled(?bool $fCRAEnabled = null): self
    {
        // validation for constraint: boolean
        if (!is_null($fCRAEnabled) && !is_bool($fCRAEnabled)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($fCRAEnabled, true), gettype($fCRAEnabled)), __LINE__);
        }
        $this->FCRAEnabled = $fCRAEnabled;
        
        return $this;
    }
    /**
     * Get Tiers value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getTiers(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->Tiers) ? $this->Tiers : null;
    }
    /**
     * Set Tiers value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $tiers
     * @return \ID3Global\Models\GlobalSanctionsMatch
     */
    public function setTiers(?\ID3Global\Arrays\ArrayOfstring $tiers = null): self
    {
        if (is_null($tiers) || (is_array($tiers) && empty($tiers))) {
            unset($this->Tiers);
        } else {
            $this->Tiers = $tiers;
        }
        
        return $this;
    }
    /**
     * Get Rank value
     * @return int|null
     */
    public function getRank(): ?int
    {
        return $this->Rank;
    }
    /**
     * Set Rank value
     * @param int $rank
     * @return \ID3Global\Models\GlobalSanctionsMatch
     */
    public function setRank(?int $rank = null): self
    {
        // validation for constraint: int
        if (!is_null($rank) && !(is_int($rank) || ctype_digit($rank))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($rank, true), gettype($rank)), __LINE__);
        }
        $this->Rank = $rank;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalSanctionsMatch
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
}
