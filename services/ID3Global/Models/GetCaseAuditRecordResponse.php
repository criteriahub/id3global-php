<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCaseAuditRecordResponse Models
 * @subpackage Structs
 */
class GetCaseAuditRecordResponse extends AbstractStructBase
{
    /**
     * The GetCaseAuditRecordResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseState|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseState $GetCaseAuditRecordResult = null;
    /**
     * Constructor method for GetCaseAuditRecordResponse
     * @uses GetCaseAuditRecordResponse::setGetCaseAuditRecordResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseState $getCaseAuditRecordResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCaseState $getCaseAuditRecordResult = null)
    {
        $this
            ->setGetCaseAuditRecordResult($getCaseAuditRecordResult);
    }
    /**
     * Get GetCaseAuditRecordResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseState|null
     */
    public function getGetCaseAuditRecordResult(): ?\ID3Global\Arrays\ArrayOfGlobalCaseState
    {
        return isset($this->GetCaseAuditRecordResult) ? $this->GetCaseAuditRecordResult : null;
    }
    /**
     * Set GetCaseAuditRecordResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseState $getCaseAuditRecordResult
     * @return \ID3Global\Models\GetCaseAuditRecordResponse
     */
    public function setGetCaseAuditRecordResult(?\ID3Global\Arrays\ArrayOfGlobalCaseState $getCaseAuditRecordResult = null): self
    {
        if (is_null($getCaseAuditRecordResult) || (is_array($getCaseAuditRecordResult) && empty($getCaseAuditRecordResult))) {
            unset($this->GetCaseAuditRecordResult);
        } else {
            $this->GetCaseAuditRecordResult = $getCaseAuditRecordResult;
        }
        
        return $this;
    }
}
