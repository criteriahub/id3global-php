<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateProfileWithScoringMethodResponse Models
 * @subpackage Structs
 */
class CreateProfileWithScoringMethodResponse extends AbstractStructBase
{
    /**
     * The CreateProfileWithScoringMethodResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileDetails|null
     */
    protected ?\ID3Global\Models\GlobalProfileDetails $CreateProfileWithScoringMethodResult = null;
    /**
     * Constructor method for CreateProfileWithScoringMethodResponse
     * @uses CreateProfileWithScoringMethodResponse::setCreateProfileWithScoringMethodResult()
     * @param \ID3Global\Models\GlobalProfileDetails $createProfileWithScoringMethodResult
     */
    public function __construct(?\ID3Global\Models\GlobalProfileDetails $createProfileWithScoringMethodResult = null)
    {
        $this
            ->setCreateProfileWithScoringMethodResult($createProfileWithScoringMethodResult);
    }
    /**
     * Get CreateProfileWithScoringMethodResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileDetails|null
     */
    public function getCreateProfileWithScoringMethodResult(): ?\ID3Global\Models\GlobalProfileDetails
    {
        return isset($this->CreateProfileWithScoringMethodResult) ? $this->CreateProfileWithScoringMethodResult : null;
    }
    /**
     * Set CreateProfileWithScoringMethodResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileDetails $createProfileWithScoringMethodResult
     * @return \ID3Global\Models\CreateProfileWithScoringMethodResponse
     */
    public function setCreateProfileWithScoringMethodResult(?\ID3Global\Models\GlobalProfileDetails $createProfileWithScoringMethodResult = null): self
    {
        if (is_null($createProfileWithScoringMethodResult) || (is_array($createProfileWithScoringMethodResult) && empty($createProfileWithScoringMethodResult))) {
            unset($this->CreateProfileWithScoringMethodResult);
        } else {
            $this->CreateProfileWithScoringMethodResult = $createProfileWithScoringMethodResult;
        }
        
        return $this;
    }
}
