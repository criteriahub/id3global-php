<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSupplierAccountsResponse Models
 * @subpackage Structs
 */
class GetSupplierAccountsResponse extends AbstractStructBase
{
    /**
     * The GetSupplierAccountsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierAccounts|null
     */
    protected ?\ID3Global\Models\GlobalSupplierAccounts $GetSupplierAccountsResult = null;
    /**
     * Constructor method for GetSupplierAccountsResponse
     * @uses GetSupplierAccountsResponse::setGetSupplierAccountsResult()
     * @param \ID3Global\Models\GlobalSupplierAccounts $getSupplierAccountsResult
     */
    public function __construct(?\ID3Global\Models\GlobalSupplierAccounts $getSupplierAccountsResult = null)
    {
        $this
            ->setGetSupplierAccountsResult($getSupplierAccountsResult);
    }
    /**
     * Get GetSupplierAccountsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierAccounts|null
     */
    public function getGetSupplierAccountsResult(): ?\ID3Global\Models\GlobalSupplierAccounts
    {
        return isset($this->GetSupplierAccountsResult) ? $this->GetSupplierAccountsResult : null;
    }
    /**
     * Set GetSupplierAccountsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupplierAccounts $getSupplierAccountsResult
     * @return \ID3Global\Models\GetSupplierAccountsResponse
     */
    public function setGetSupplierAccountsResult(?\ID3Global\Models\GlobalSupplierAccounts $getSupplierAccountsResult = null): self
    {
        if (is_null($getSupplierAccountsResult) || (is_array($getSupplierAccountsResult) && empty($getSupplierAccountsResult))) {
            unset($this->GetSupplierAccountsResult);
        } else {
            $this->GetSupplierAccountsResult = $getSupplierAccountsResult;
        }
        
        return $this;
    }
}
