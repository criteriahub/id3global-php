<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAccountsResponse Models
 * @subpackage Structs
 */
class GetAccountsResponse extends AbstractStructBase
{
    /**
     * The GetAccountsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAccounts|null
     */
    protected ?\ID3Global\Models\GlobalAccounts $GetAccountsResult = null;
    /**
     * Constructor method for GetAccountsResponse
     * @uses GetAccountsResponse::setGetAccountsResult()
     * @param \ID3Global\Models\GlobalAccounts $getAccountsResult
     */
    public function __construct(?\ID3Global\Models\GlobalAccounts $getAccountsResult = null)
    {
        $this
            ->setGetAccountsResult($getAccountsResult);
    }
    /**
     * Get GetAccountsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAccounts|null
     */
    public function getGetAccountsResult(): ?\ID3Global\Models\GlobalAccounts
    {
        return isset($this->GetAccountsResult) ? $this->GetAccountsResult : null;
    }
    /**
     * Set GetAccountsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAccounts $getAccountsResult
     * @return \ID3Global\Models\GetAccountsResponse
     */
    public function setGetAccountsResult(?\ID3Global\Models\GlobalAccounts $getAccountsResult = null): self
    {
        if (is_null($getAccountsResult) || (is_array($getAccountsResult) && empty($getAccountsResult))) {
            unset($this->GetAccountsResult);
        } else {
            $this->GetAccountsResult = $getAccountsResult;
        }
        
        return $this;
    }
}
