<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalFCRAMatchDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q532:GlobalFCRAMatchDetails
 * @subpackage Structs
 */
class GlobalFCRAMatchDetails extends AbstractStructBase
{
    /**
     * The SourceID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SourceID = null;
    /**
     * The SourceName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SourceName = null;
    /**
     * The SourceAgency
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SourceAgency = null;
    /**
     * The SourceEntity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SourceEntity = null;
    /**
     * The KeywordsIdentified
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $KeywordsIdentified = null;
    /**
     * The SourceType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SourceType = null;
    /**
     * The DOBMatch
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DOBMatch = null;
    /**
     * The ResultsURL
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ResultsURL = null;
    /**
     * The Excerpts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Excerpts = null;
    /**
     * The FullName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $FullName = null;
    /**
     * The Aliases
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Aliases = null;
    /**
     * The AliasType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AliasType = null;
    /**
     * The DateofBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DateofBirth = null;
    /**
     * The PlaceofBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $PlaceofBirth = null;
    /**
     * The DateofDeath
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DateofDeath = null;
    /**
     * The Nationality
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Nationality = null;
    /**
     * The Address
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Address = null;
    /**
     * The IdentityDocuments
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $IdentityDocuments = null;
    /**
     * The EyeColor
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $EyeColor = null;
    /**
     * The HairColor
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $HairColor = null;
    /**
     * The Height
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Height = null;
    /**
     * The Weight
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Weight = null;
    /**
     * The Entity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Entity = null;
    /**
     * The Remarks
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Remarks = null;
    /**
     * The MailingAddressName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MailingAddressName = null;
    /**
     * The MailingAddress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MailingAddress = null;
    /**
     * The PreviousAddress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $PreviousAddress = null;
    /**
     * The RecordType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $RecordType = null;
    /**
     * The CorporateName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CorporateName = null;
    /**
     * The AvgNumberofEmployees
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AvgNumberofEmployees = null;
    /**
     * The AnnualReceipts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AnnualReceipts = null;
    /**
     * The CorporateURL
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CorporateURL = null;
    /**
     * The CompanyDivision
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CompanyDivision = null;
    /**
     * The DivisionNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DivisionNumber = null;
    /**
     * The CorporatePhone1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CorporatePhone1 = null;
    /**
     * The CorporatePhone2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CorporatePhone2 = null;
    /**
     * The CorporateFax
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CorporateFax = null;
    /**
     * The CorporateEmail
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CorporateEmail = null;
    /**
     * The RegistrationDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $RegistrationDate = null;
    /**
     * The RenewalDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $RenewalDate = null;
    /**
     * The BusinessStartDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $BusinessStartDate = null;
    /**
     * The FiscalYearEndClose
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $FiscalYearEndClose = null;
    /**
     * The OrganizationalType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OrganizationalType = null;
    /**
     * The CountryofIncorporation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CountryofIncorporation = null;
    /**
     * The StateofIncorporation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $StateofIncorporation = null;
    /**
     * The OwnerName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OwnerName = null;
    /**
     * The OwnerEmail
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OwnerEmail = null;
    /**
     * The OwnerPhone1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OwnerPhone1 = null;
    /**
     * The OwnerPhone2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OwnerPhone2 = null;
    /**
     * The OwnerFax
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OwnerFax = null;
    /**
     * The GovtAddressName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $GovtAddressName = null;
    /**
     * The GovtAddress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $GovtAddress = null;
    /**
     * The GovtPhone1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $GovtPhone1 = null;
    /**
     * The GovtPhone2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $GovtPhone2 = null;
    /**
     * The GovtEmail
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $GovtEmail = null;
    /**
     * The GovtFax
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $GovtFax = null;
    /**
     * The AltGovtAddressName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AltGovtAddressName = null;
    /**
     * The AltGovtAddress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AltGovtAddress = null;
    /**
     * The AltGovtPhone1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AltGovtPhone1 = null;
    /**
     * The AltGovtPhone2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AltGovtPhone2 = null;
    /**
     * The AltGovtFax
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AltGovtFax = null;
    /**
     * The AltGovtEmail
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AltGovtEmail = null;
    /**
     * Constructor method for GlobalFCRAMatchDetails
     * @uses GlobalFCRAMatchDetails::setSourceID()
     * @uses GlobalFCRAMatchDetails::setSourceName()
     * @uses GlobalFCRAMatchDetails::setSourceAgency()
     * @uses GlobalFCRAMatchDetails::setSourceEntity()
     * @uses GlobalFCRAMatchDetails::setKeywordsIdentified()
     * @uses GlobalFCRAMatchDetails::setSourceType()
     * @uses GlobalFCRAMatchDetails::setDOBMatch()
     * @uses GlobalFCRAMatchDetails::setResultsURL()
     * @uses GlobalFCRAMatchDetails::setExcerpts()
     * @uses GlobalFCRAMatchDetails::setFullName()
     * @uses GlobalFCRAMatchDetails::setAliases()
     * @uses GlobalFCRAMatchDetails::setAliasType()
     * @uses GlobalFCRAMatchDetails::setDateofBirth()
     * @uses GlobalFCRAMatchDetails::setPlaceofBirth()
     * @uses GlobalFCRAMatchDetails::setDateofDeath()
     * @uses GlobalFCRAMatchDetails::setNationality()
     * @uses GlobalFCRAMatchDetails::setAddress()
     * @uses GlobalFCRAMatchDetails::setIdentityDocuments()
     * @uses GlobalFCRAMatchDetails::setEyeColor()
     * @uses GlobalFCRAMatchDetails::setHairColor()
     * @uses GlobalFCRAMatchDetails::setHeight()
     * @uses GlobalFCRAMatchDetails::setWeight()
     * @uses GlobalFCRAMatchDetails::setEntity()
     * @uses GlobalFCRAMatchDetails::setRemarks()
     * @uses GlobalFCRAMatchDetails::setMailingAddressName()
     * @uses GlobalFCRAMatchDetails::setMailingAddress()
     * @uses GlobalFCRAMatchDetails::setPreviousAddress()
     * @uses GlobalFCRAMatchDetails::setRecordType()
     * @uses GlobalFCRAMatchDetails::setCorporateName()
     * @uses GlobalFCRAMatchDetails::setAvgNumberofEmployees()
     * @uses GlobalFCRAMatchDetails::setAnnualReceipts()
     * @uses GlobalFCRAMatchDetails::setCorporateURL()
     * @uses GlobalFCRAMatchDetails::setCompanyDivision()
     * @uses GlobalFCRAMatchDetails::setDivisionNumber()
     * @uses GlobalFCRAMatchDetails::setCorporatePhone1()
     * @uses GlobalFCRAMatchDetails::setCorporatePhone2()
     * @uses GlobalFCRAMatchDetails::setCorporateFax()
     * @uses GlobalFCRAMatchDetails::setCorporateEmail()
     * @uses GlobalFCRAMatchDetails::setRegistrationDate()
     * @uses GlobalFCRAMatchDetails::setRenewalDate()
     * @uses GlobalFCRAMatchDetails::setBusinessStartDate()
     * @uses GlobalFCRAMatchDetails::setFiscalYearEndClose()
     * @uses GlobalFCRAMatchDetails::setOrganizationalType()
     * @uses GlobalFCRAMatchDetails::setCountryofIncorporation()
     * @uses GlobalFCRAMatchDetails::setStateofIncorporation()
     * @uses GlobalFCRAMatchDetails::setOwnerName()
     * @uses GlobalFCRAMatchDetails::setOwnerEmail()
     * @uses GlobalFCRAMatchDetails::setOwnerPhone1()
     * @uses GlobalFCRAMatchDetails::setOwnerPhone2()
     * @uses GlobalFCRAMatchDetails::setOwnerFax()
     * @uses GlobalFCRAMatchDetails::setGovtAddressName()
     * @uses GlobalFCRAMatchDetails::setGovtAddress()
     * @uses GlobalFCRAMatchDetails::setGovtPhone1()
     * @uses GlobalFCRAMatchDetails::setGovtPhone2()
     * @uses GlobalFCRAMatchDetails::setGovtEmail()
     * @uses GlobalFCRAMatchDetails::setGovtFax()
     * @uses GlobalFCRAMatchDetails::setAltGovtAddressName()
     * @uses GlobalFCRAMatchDetails::setAltGovtAddress()
     * @uses GlobalFCRAMatchDetails::setAltGovtPhone1()
     * @uses GlobalFCRAMatchDetails::setAltGovtPhone2()
     * @uses GlobalFCRAMatchDetails::setAltGovtFax()
     * @uses GlobalFCRAMatchDetails::setAltGovtEmail()
     * @param string $sourceID
     * @param string $sourceName
     * @param string $sourceAgency
     * @param string $sourceEntity
     * @param string $keywordsIdentified
     * @param string $sourceType
     * @param string $dOBMatch
     * @param string $resultsURL
     * @param string $excerpts
     * @param string $fullName
     * @param string $aliases
     * @param string $aliasType
     * @param string $dateofBirth
     * @param string $placeofBirth
     * @param string $dateofDeath
     * @param string $nationality
     * @param string $address
     * @param string $identityDocuments
     * @param string $eyeColor
     * @param string $hairColor
     * @param string $height
     * @param string $weight
     * @param string $entity
     * @param string $remarks
     * @param string $mailingAddressName
     * @param string $mailingAddress
     * @param string $previousAddress
     * @param string $recordType
     * @param string $corporateName
     * @param string $avgNumberofEmployees
     * @param string $annualReceipts
     * @param string $corporateURL
     * @param string $companyDivision
     * @param string $divisionNumber
     * @param string $corporatePhone1
     * @param string $corporatePhone2
     * @param string $corporateFax
     * @param string $corporateEmail
     * @param string $registrationDate
     * @param string $renewalDate
     * @param string $businessStartDate
     * @param string $fiscalYearEndClose
     * @param string $organizationalType
     * @param string $countryofIncorporation
     * @param string $stateofIncorporation
     * @param string $ownerName
     * @param string $ownerEmail
     * @param string $ownerPhone1
     * @param string $ownerPhone2
     * @param string $ownerFax
     * @param string $govtAddressName
     * @param string $govtAddress
     * @param string $govtPhone1
     * @param string $govtPhone2
     * @param string $govtEmail
     * @param string $govtFax
     * @param string $altGovtAddressName
     * @param string $altGovtAddress
     * @param string $altGovtPhone1
     * @param string $altGovtPhone2
     * @param string $altGovtFax
     * @param string $altGovtEmail
     */
    public function __construct(?string $sourceID = null, ?string $sourceName = null, ?string $sourceAgency = null, ?string $sourceEntity = null, ?string $keywordsIdentified = null, ?string $sourceType = null, ?string $dOBMatch = null, ?string $resultsURL = null, ?string $excerpts = null, ?string $fullName = null, ?string $aliases = null, ?string $aliasType = null, ?string $dateofBirth = null, ?string $placeofBirth = null, ?string $dateofDeath = null, ?string $nationality = null, ?string $address = null, ?string $identityDocuments = null, ?string $eyeColor = null, ?string $hairColor = null, ?string $height = null, ?string $weight = null, ?string $entity = null, ?string $remarks = null, ?string $mailingAddressName = null, ?string $mailingAddress = null, ?string $previousAddress = null, ?string $recordType = null, ?string $corporateName = null, ?string $avgNumberofEmployees = null, ?string $annualReceipts = null, ?string $corporateURL = null, ?string $companyDivision = null, ?string $divisionNumber = null, ?string $corporatePhone1 = null, ?string $corporatePhone2 = null, ?string $corporateFax = null, ?string $corporateEmail = null, ?string $registrationDate = null, ?string $renewalDate = null, ?string $businessStartDate = null, ?string $fiscalYearEndClose = null, ?string $organizationalType = null, ?string $countryofIncorporation = null, ?string $stateofIncorporation = null, ?string $ownerName = null, ?string $ownerEmail = null, ?string $ownerPhone1 = null, ?string $ownerPhone2 = null, ?string $ownerFax = null, ?string $govtAddressName = null, ?string $govtAddress = null, ?string $govtPhone1 = null, ?string $govtPhone2 = null, ?string $govtEmail = null, ?string $govtFax = null, ?string $altGovtAddressName = null, ?string $altGovtAddress = null, ?string $altGovtPhone1 = null, ?string $altGovtPhone2 = null, ?string $altGovtFax = null, ?string $altGovtEmail = null)
    {
        $this
            ->setSourceID($sourceID)
            ->setSourceName($sourceName)
            ->setSourceAgency($sourceAgency)
            ->setSourceEntity($sourceEntity)
            ->setKeywordsIdentified($keywordsIdentified)
            ->setSourceType($sourceType)
            ->setDOBMatch($dOBMatch)
            ->setResultsURL($resultsURL)
            ->setExcerpts($excerpts)
            ->setFullName($fullName)
            ->setAliases($aliases)
            ->setAliasType($aliasType)
            ->setDateofBirth($dateofBirth)
            ->setPlaceofBirth($placeofBirth)
            ->setDateofDeath($dateofDeath)
            ->setNationality($nationality)
            ->setAddress($address)
            ->setIdentityDocuments($identityDocuments)
            ->setEyeColor($eyeColor)
            ->setHairColor($hairColor)
            ->setHeight($height)
            ->setWeight($weight)
            ->setEntity($entity)
            ->setRemarks($remarks)
            ->setMailingAddressName($mailingAddressName)
            ->setMailingAddress($mailingAddress)
            ->setPreviousAddress($previousAddress)
            ->setRecordType($recordType)
            ->setCorporateName($corporateName)
            ->setAvgNumberofEmployees($avgNumberofEmployees)
            ->setAnnualReceipts($annualReceipts)
            ->setCorporateURL($corporateURL)
            ->setCompanyDivision($companyDivision)
            ->setDivisionNumber($divisionNumber)
            ->setCorporatePhone1($corporatePhone1)
            ->setCorporatePhone2($corporatePhone2)
            ->setCorporateFax($corporateFax)
            ->setCorporateEmail($corporateEmail)
            ->setRegistrationDate($registrationDate)
            ->setRenewalDate($renewalDate)
            ->setBusinessStartDate($businessStartDate)
            ->setFiscalYearEndClose($fiscalYearEndClose)
            ->setOrganizationalType($organizationalType)
            ->setCountryofIncorporation($countryofIncorporation)
            ->setStateofIncorporation($stateofIncorporation)
            ->setOwnerName($ownerName)
            ->setOwnerEmail($ownerEmail)
            ->setOwnerPhone1($ownerPhone1)
            ->setOwnerPhone2($ownerPhone2)
            ->setOwnerFax($ownerFax)
            ->setGovtAddressName($govtAddressName)
            ->setGovtAddress($govtAddress)
            ->setGovtPhone1($govtPhone1)
            ->setGovtPhone2($govtPhone2)
            ->setGovtEmail($govtEmail)
            ->setGovtFax($govtFax)
            ->setAltGovtAddressName($altGovtAddressName)
            ->setAltGovtAddress($altGovtAddress)
            ->setAltGovtPhone1($altGovtPhone1)
            ->setAltGovtPhone2($altGovtPhone2)
            ->setAltGovtFax($altGovtFax)
            ->setAltGovtEmail($altGovtEmail);
    }
    /**
     * Get SourceID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSourceID(): ?string
    {
        return isset($this->SourceID) ? $this->SourceID : null;
    }
    /**
     * Set SourceID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sourceID
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setSourceID(?string $sourceID = null): self
    {
        // validation for constraint: string
        if (!is_null($sourceID) && !is_string($sourceID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sourceID, true), gettype($sourceID)), __LINE__);
        }
        if (is_null($sourceID) || (is_array($sourceID) && empty($sourceID))) {
            unset($this->SourceID);
        } else {
            $this->SourceID = $sourceID;
        }
        
        return $this;
    }
    /**
     * Get SourceName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSourceName(): ?string
    {
        return isset($this->SourceName) ? $this->SourceName : null;
    }
    /**
     * Set SourceName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sourceName
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setSourceName(?string $sourceName = null): self
    {
        // validation for constraint: string
        if (!is_null($sourceName) && !is_string($sourceName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sourceName, true), gettype($sourceName)), __LINE__);
        }
        if (is_null($sourceName) || (is_array($sourceName) && empty($sourceName))) {
            unset($this->SourceName);
        } else {
            $this->SourceName = $sourceName;
        }
        
        return $this;
    }
    /**
     * Get SourceAgency value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSourceAgency(): ?string
    {
        return isset($this->SourceAgency) ? $this->SourceAgency : null;
    }
    /**
     * Set SourceAgency value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sourceAgency
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setSourceAgency(?string $sourceAgency = null): self
    {
        // validation for constraint: string
        if (!is_null($sourceAgency) && !is_string($sourceAgency)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sourceAgency, true), gettype($sourceAgency)), __LINE__);
        }
        if (is_null($sourceAgency) || (is_array($sourceAgency) && empty($sourceAgency))) {
            unset($this->SourceAgency);
        } else {
            $this->SourceAgency = $sourceAgency;
        }
        
        return $this;
    }
    /**
     * Get SourceEntity value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSourceEntity(): ?string
    {
        return isset($this->SourceEntity) ? $this->SourceEntity : null;
    }
    /**
     * Set SourceEntity value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sourceEntity
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setSourceEntity(?string $sourceEntity = null): self
    {
        // validation for constraint: string
        if (!is_null($sourceEntity) && !is_string($sourceEntity)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sourceEntity, true), gettype($sourceEntity)), __LINE__);
        }
        if (is_null($sourceEntity) || (is_array($sourceEntity) && empty($sourceEntity))) {
            unset($this->SourceEntity);
        } else {
            $this->SourceEntity = $sourceEntity;
        }
        
        return $this;
    }
    /**
     * Get KeywordsIdentified value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getKeywordsIdentified(): ?string
    {
        return isset($this->KeywordsIdentified) ? $this->KeywordsIdentified : null;
    }
    /**
     * Set KeywordsIdentified value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $keywordsIdentified
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setKeywordsIdentified(?string $keywordsIdentified = null): self
    {
        // validation for constraint: string
        if (!is_null($keywordsIdentified) && !is_string($keywordsIdentified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($keywordsIdentified, true), gettype($keywordsIdentified)), __LINE__);
        }
        if (is_null($keywordsIdentified) || (is_array($keywordsIdentified) && empty($keywordsIdentified))) {
            unset($this->KeywordsIdentified);
        } else {
            $this->KeywordsIdentified = $keywordsIdentified;
        }
        
        return $this;
    }
    /**
     * Get SourceType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSourceType(): ?string
    {
        return isset($this->SourceType) ? $this->SourceType : null;
    }
    /**
     * Set SourceType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sourceType
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setSourceType(?string $sourceType = null): self
    {
        // validation for constraint: string
        if (!is_null($sourceType) && !is_string($sourceType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sourceType, true), gettype($sourceType)), __LINE__);
        }
        if (is_null($sourceType) || (is_array($sourceType) && empty($sourceType))) {
            unset($this->SourceType);
        } else {
            $this->SourceType = $sourceType;
        }
        
        return $this;
    }
    /**
     * Get DOBMatch value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDOBMatch(): ?string
    {
        return isset($this->DOBMatch) ? $this->DOBMatch : null;
    }
    /**
     * Set DOBMatch value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dOBMatch
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setDOBMatch(?string $dOBMatch = null): self
    {
        // validation for constraint: string
        if (!is_null($dOBMatch) && !is_string($dOBMatch)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dOBMatch, true), gettype($dOBMatch)), __LINE__);
        }
        if (is_null($dOBMatch) || (is_array($dOBMatch) && empty($dOBMatch))) {
            unset($this->DOBMatch);
        } else {
            $this->DOBMatch = $dOBMatch;
        }
        
        return $this;
    }
    /**
     * Get ResultsURL value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getResultsURL(): ?string
    {
        return isset($this->ResultsURL) ? $this->ResultsURL : null;
    }
    /**
     * Set ResultsURL value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $resultsURL
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setResultsURL(?string $resultsURL = null): self
    {
        // validation for constraint: string
        if (!is_null($resultsURL) && !is_string($resultsURL)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($resultsURL, true), gettype($resultsURL)), __LINE__);
        }
        if (is_null($resultsURL) || (is_array($resultsURL) && empty($resultsURL))) {
            unset($this->ResultsURL);
        } else {
            $this->ResultsURL = $resultsURL;
        }
        
        return $this;
    }
    /**
     * Get Excerpts value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getExcerpts(): ?string
    {
        return isset($this->Excerpts) ? $this->Excerpts : null;
    }
    /**
     * Set Excerpts value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $excerpts
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setExcerpts(?string $excerpts = null): self
    {
        // validation for constraint: string
        if (!is_null($excerpts) && !is_string($excerpts)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($excerpts, true), gettype($excerpts)), __LINE__);
        }
        if (is_null($excerpts) || (is_array($excerpts) && empty($excerpts))) {
            unset($this->Excerpts);
        } else {
            $this->Excerpts = $excerpts;
        }
        
        return $this;
    }
    /**
     * Get FullName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return isset($this->FullName) ? $this->FullName : null;
    }
    /**
     * Set FullName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fullName
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setFullName(?string $fullName = null): self
    {
        // validation for constraint: string
        if (!is_null($fullName) && !is_string($fullName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fullName, true), gettype($fullName)), __LINE__);
        }
        if (is_null($fullName) || (is_array($fullName) && empty($fullName))) {
            unset($this->FullName);
        } else {
            $this->FullName = $fullName;
        }
        
        return $this;
    }
    /**
     * Get Aliases value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAliases(): ?string
    {
        return isset($this->Aliases) ? $this->Aliases : null;
    }
    /**
     * Set Aliases value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $aliases
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setAliases(?string $aliases = null): self
    {
        // validation for constraint: string
        if (!is_null($aliases) && !is_string($aliases)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($aliases, true), gettype($aliases)), __LINE__);
        }
        if (is_null($aliases) || (is_array($aliases) && empty($aliases))) {
            unset($this->Aliases);
        } else {
            $this->Aliases = $aliases;
        }
        
        return $this;
    }
    /**
     * Get AliasType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAliasType(): ?string
    {
        return isset($this->AliasType) ? $this->AliasType : null;
    }
    /**
     * Set AliasType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $aliasType
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setAliasType(?string $aliasType = null): self
    {
        // validation for constraint: string
        if (!is_null($aliasType) && !is_string($aliasType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($aliasType, true), gettype($aliasType)), __LINE__);
        }
        if (is_null($aliasType) || (is_array($aliasType) && empty($aliasType))) {
            unset($this->AliasType);
        } else {
            $this->AliasType = $aliasType;
        }
        
        return $this;
    }
    /**
     * Get DateofBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDateofBirth(): ?string
    {
        return isset($this->DateofBirth) ? $this->DateofBirth : null;
    }
    /**
     * Set DateofBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dateofBirth
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setDateofBirth(?string $dateofBirth = null): self
    {
        // validation for constraint: string
        if (!is_null($dateofBirth) && !is_string($dateofBirth)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateofBirth, true), gettype($dateofBirth)), __LINE__);
        }
        if (is_null($dateofBirth) || (is_array($dateofBirth) && empty($dateofBirth))) {
            unset($this->DateofBirth);
        } else {
            $this->DateofBirth = $dateofBirth;
        }
        
        return $this;
    }
    /**
     * Get PlaceofBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPlaceofBirth(): ?string
    {
        return isset($this->PlaceofBirth) ? $this->PlaceofBirth : null;
    }
    /**
     * Set PlaceofBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $placeofBirth
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setPlaceofBirth(?string $placeofBirth = null): self
    {
        // validation for constraint: string
        if (!is_null($placeofBirth) && !is_string($placeofBirth)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($placeofBirth, true), gettype($placeofBirth)), __LINE__);
        }
        if (is_null($placeofBirth) || (is_array($placeofBirth) && empty($placeofBirth))) {
            unset($this->PlaceofBirth);
        } else {
            $this->PlaceofBirth = $placeofBirth;
        }
        
        return $this;
    }
    /**
     * Get DateofDeath value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDateofDeath(): ?string
    {
        return isset($this->DateofDeath) ? $this->DateofDeath : null;
    }
    /**
     * Set DateofDeath value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dateofDeath
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setDateofDeath(?string $dateofDeath = null): self
    {
        // validation for constraint: string
        if (!is_null($dateofDeath) && !is_string($dateofDeath)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateofDeath, true), gettype($dateofDeath)), __LINE__);
        }
        if (is_null($dateofDeath) || (is_array($dateofDeath) && empty($dateofDeath))) {
            unset($this->DateofDeath);
        } else {
            $this->DateofDeath = $dateofDeath;
        }
        
        return $this;
    }
    /**
     * Get Nationality value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNationality(): ?string
    {
        return isset($this->Nationality) ? $this->Nationality : null;
    }
    /**
     * Set Nationality value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $nationality
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setNationality(?string $nationality = null): self
    {
        // validation for constraint: string
        if (!is_null($nationality) && !is_string($nationality)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nationality, true), gettype($nationality)), __LINE__);
        }
        if (is_null($nationality) || (is_array($nationality) && empty($nationality))) {
            unset($this->Nationality);
        } else {
            $this->Nationality = $nationality;
        }
        
        return $this;
    }
    /**
     * Get Address value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return isset($this->Address) ? $this->Address : null;
    }
    /**
     * Set Address value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $address
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setAddress(?string $address = null): self
    {
        // validation for constraint: string
        if (!is_null($address) && !is_string($address)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($address, true), gettype($address)), __LINE__);
        }
        if (is_null($address) || (is_array($address) && empty($address))) {
            unset($this->Address);
        } else {
            $this->Address = $address;
        }
        
        return $this;
    }
    /**
     * Get IdentityDocuments value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIdentityDocuments(): ?string
    {
        return isset($this->IdentityDocuments) ? $this->IdentityDocuments : null;
    }
    /**
     * Set IdentityDocuments value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $identityDocuments
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setIdentityDocuments(?string $identityDocuments = null): self
    {
        // validation for constraint: string
        if (!is_null($identityDocuments) && !is_string($identityDocuments)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($identityDocuments, true), gettype($identityDocuments)), __LINE__);
        }
        if (is_null($identityDocuments) || (is_array($identityDocuments) && empty($identityDocuments))) {
            unset($this->IdentityDocuments);
        } else {
            $this->IdentityDocuments = $identityDocuments;
        }
        
        return $this;
    }
    /**
     * Get EyeColor value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEyeColor(): ?string
    {
        return isset($this->EyeColor) ? $this->EyeColor : null;
    }
    /**
     * Set EyeColor value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $eyeColor
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setEyeColor(?string $eyeColor = null): self
    {
        // validation for constraint: string
        if (!is_null($eyeColor) && !is_string($eyeColor)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($eyeColor, true), gettype($eyeColor)), __LINE__);
        }
        if (is_null($eyeColor) || (is_array($eyeColor) && empty($eyeColor))) {
            unset($this->EyeColor);
        } else {
            $this->EyeColor = $eyeColor;
        }
        
        return $this;
    }
    /**
     * Get HairColor value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getHairColor(): ?string
    {
        return isset($this->HairColor) ? $this->HairColor : null;
    }
    /**
     * Set HairColor value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $hairColor
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setHairColor(?string $hairColor = null): self
    {
        // validation for constraint: string
        if (!is_null($hairColor) && !is_string($hairColor)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hairColor, true), gettype($hairColor)), __LINE__);
        }
        if (is_null($hairColor) || (is_array($hairColor) && empty($hairColor))) {
            unset($this->HairColor);
        } else {
            $this->HairColor = $hairColor;
        }
        
        return $this;
    }
    /**
     * Get Height value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getHeight(): ?string
    {
        return isset($this->Height) ? $this->Height : null;
    }
    /**
     * Set Height value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $height
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setHeight(?string $height = null): self
    {
        // validation for constraint: string
        if (!is_null($height) && !is_string($height)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($height, true), gettype($height)), __LINE__);
        }
        if (is_null($height) || (is_array($height) && empty($height))) {
            unset($this->Height);
        } else {
            $this->Height = $height;
        }
        
        return $this;
    }
    /**
     * Get Weight value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getWeight(): ?string
    {
        return isset($this->Weight) ? $this->Weight : null;
    }
    /**
     * Set Weight value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $weight
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setWeight(?string $weight = null): self
    {
        // validation for constraint: string
        if (!is_null($weight) && !is_string($weight)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($weight, true), gettype($weight)), __LINE__);
        }
        if (is_null($weight) || (is_array($weight) && empty($weight))) {
            unset($this->Weight);
        } else {
            $this->Weight = $weight;
        }
        
        return $this;
    }
    /**
     * Get Entity value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEntity(): ?string
    {
        return isset($this->Entity) ? $this->Entity : null;
    }
    /**
     * Set Entity value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $entity
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setEntity(?string $entity = null): self
    {
        // validation for constraint: string
        if (!is_null($entity) && !is_string($entity)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($entity, true), gettype($entity)), __LINE__);
        }
        if (is_null($entity) || (is_array($entity) && empty($entity))) {
            unset($this->Entity);
        } else {
            $this->Entity = $entity;
        }
        
        return $this;
    }
    /**
     * Get Remarks value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRemarks(): ?string
    {
        return isset($this->Remarks) ? $this->Remarks : null;
    }
    /**
     * Set Remarks value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $remarks
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setRemarks(?string $remarks = null): self
    {
        // validation for constraint: string
        if (!is_null($remarks) && !is_string($remarks)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($remarks, true), gettype($remarks)), __LINE__);
        }
        if (is_null($remarks) || (is_array($remarks) && empty($remarks))) {
            unset($this->Remarks);
        } else {
            $this->Remarks = $remarks;
        }
        
        return $this;
    }
    /**
     * Get MailingAddressName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMailingAddressName(): ?string
    {
        return isset($this->MailingAddressName) ? $this->MailingAddressName : null;
    }
    /**
     * Set MailingAddressName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mailingAddressName
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setMailingAddressName(?string $mailingAddressName = null): self
    {
        // validation for constraint: string
        if (!is_null($mailingAddressName) && !is_string($mailingAddressName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mailingAddressName, true), gettype($mailingAddressName)), __LINE__);
        }
        if (is_null($mailingAddressName) || (is_array($mailingAddressName) && empty($mailingAddressName))) {
            unset($this->MailingAddressName);
        } else {
            $this->MailingAddressName = $mailingAddressName;
        }
        
        return $this;
    }
    /**
     * Get MailingAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMailingAddress(): ?string
    {
        return isset($this->MailingAddress) ? $this->MailingAddress : null;
    }
    /**
     * Set MailingAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mailingAddress
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setMailingAddress(?string $mailingAddress = null): self
    {
        // validation for constraint: string
        if (!is_null($mailingAddress) && !is_string($mailingAddress)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mailingAddress, true), gettype($mailingAddress)), __LINE__);
        }
        if (is_null($mailingAddress) || (is_array($mailingAddress) && empty($mailingAddress))) {
            unset($this->MailingAddress);
        } else {
            $this->MailingAddress = $mailingAddress;
        }
        
        return $this;
    }
    /**
     * Get PreviousAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPreviousAddress(): ?string
    {
        return isset($this->PreviousAddress) ? $this->PreviousAddress : null;
    }
    /**
     * Set PreviousAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $previousAddress
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setPreviousAddress(?string $previousAddress = null): self
    {
        // validation for constraint: string
        if (!is_null($previousAddress) && !is_string($previousAddress)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($previousAddress, true), gettype($previousAddress)), __LINE__);
        }
        if (is_null($previousAddress) || (is_array($previousAddress) && empty($previousAddress))) {
            unset($this->PreviousAddress);
        } else {
            $this->PreviousAddress = $previousAddress;
        }
        
        return $this;
    }
    /**
     * Get RecordType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRecordType(): ?string
    {
        return isset($this->RecordType) ? $this->RecordType : null;
    }
    /**
     * Set RecordType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $recordType
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setRecordType(?string $recordType = null): self
    {
        // validation for constraint: string
        if (!is_null($recordType) && !is_string($recordType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($recordType, true), gettype($recordType)), __LINE__);
        }
        if (is_null($recordType) || (is_array($recordType) && empty($recordType))) {
            unset($this->RecordType);
        } else {
            $this->RecordType = $recordType;
        }
        
        return $this;
    }
    /**
     * Get CorporateName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCorporateName(): ?string
    {
        return isset($this->CorporateName) ? $this->CorporateName : null;
    }
    /**
     * Set CorporateName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $corporateName
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setCorporateName(?string $corporateName = null): self
    {
        // validation for constraint: string
        if (!is_null($corporateName) && !is_string($corporateName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($corporateName, true), gettype($corporateName)), __LINE__);
        }
        if (is_null($corporateName) || (is_array($corporateName) && empty($corporateName))) {
            unset($this->CorporateName);
        } else {
            $this->CorporateName = $corporateName;
        }
        
        return $this;
    }
    /**
     * Get AvgNumberofEmployees value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAvgNumberofEmployees(): ?string
    {
        return isset($this->AvgNumberofEmployees) ? $this->AvgNumberofEmployees : null;
    }
    /**
     * Set AvgNumberofEmployees value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $avgNumberofEmployees
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setAvgNumberofEmployees(?string $avgNumberofEmployees = null): self
    {
        // validation for constraint: string
        if (!is_null($avgNumberofEmployees) && !is_string($avgNumberofEmployees)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($avgNumberofEmployees, true), gettype($avgNumberofEmployees)), __LINE__);
        }
        if (is_null($avgNumberofEmployees) || (is_array($avgNumberofEmployees) && empty($avgNumberofEmployees))) {
            unset($this->AvgNumberofEmployees);
        } else {
            $this->AvgNumberofEmployees = $avgNumberofEmployees;
        }
        
        return $this;
    }
    /**
     * Get AnnualReceipts value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAnnualReceipts(): ?string
    {
        return isset($this->AnnualReceipts) ? $this->AnnualReceipts : null;
    }
    /**
     * Set AnnualReceipts value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $annualReceipts
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setAnnualReceipts(?string $annualReceipts = null): self
    {
        // validation for constraint: string
        if (!is_null($annualReceipts) && !is_string($annualReceipts)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($annualReceipts, true), gettype($annualReceipts)), __LINE__);
        }
        if (is_null($annualReceipts) || (is_array($annualReceipts) && empty($annualReceipts))) {
            unset($this->AnnualReceipts);
        } else {
            $this->AnnualReceipts = $annualReceipts;
        }
        
        return $this;
    }
    /**
     * Get CorporateURL value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCorporateURL(): ?string
    {
        return isset($this->CorporateURL) ? $this->CorporateURL : null;
    }
    /**
     * Set CorporateURL value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $corporateURL
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setCorporateURL(?string $corporateURL = null): self
    {
        // validation for constraint: string
        if (!is_null($corporateURL) && !is_string($corporateURL)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($corporateURL, true), gettype($corporateURL)), __LINE__);
        }
        if (is_null($corporateURL) || (is_array($corporateURL) && empty($corporateURL))) {
            unset($this->CorporateURL);
        } else {
            $this->CorporateURL = $corporateURL;
        }
        
        return $this;
    }
    /**
     * Get CompanyDivision value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCompanyDivision(): ?string
    {
        return isset($this->CompanyDivision) ? $this->CompanyDivision : null;
    }
    /**
     * Set CompanyDivision value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $companyDivision
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setCompanyDivision(?string $companyDivision = null): self
    {
        // validation for constraint: string
        if (!is_null($companyDivision) && !is_string($companyDivision)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($companyDivision, true), gettype($companyDivision)), __LINE__);
        }
        if (is_null($companyDivision) || (is_array($companyDivision) && empty($companyDivision))) {
            unset($this->CompanyDivision);
        } else {
            $this->CompanyDivision = $companyDivision;
        }
        
        return $this;
    }
    /**
     * Get DivisionNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDivisionNumber(): ?string
    {
        return isset($this->DivisionNumber) ? $this->DivisionNumber : null;
    }
    /**
     * Set DivisionNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $divisionNumber
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setDivisionNumber(?string $divisionNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($divisionNumber) && !is_string($divisionNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($divisionNumber, true), gettype($divisionNumber)), __LINE__);
        }
        if (is_null($divisionNumber) || (is_array($divisionNumber) && empty($divisionNumber))) {
            unset($this->DivisionNumber);
        } else {
            $this->DivisionNumber = $divisionNumber;
        }
        
        return $this;
    }
    /**
     * Get CorporatePhone1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCorporatePhone1(): ?string
    {
        return isset($this->CorporatePhone1) ? $this->CorporatePhone1 : null;
    }
    /**
     * Set CorporatePhone1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $corporatePhone1
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setCorporatePhone1(?string $corporatePhone1 = null): self
    {
        // validation for constraint: string
        if (!is_null($corporatePhone1) && !is_string($corporatePhone1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($corporatePhone1, true), gettype($corporatePhone1)), __LINE__);
        }
        if (is_null($corporatePhone1) || (is_array($corporatePhone1) && empty($corporatePhone1))) {
            unset($this->CorporatePhone1);
        } else {
            $this->CorporatePhone1 = $corporatePhone1;
        }
        
        return $this;
    }
    /**
     * Get CorporatePhone2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCorporatePhone2(): ?string
    {
        return isset($this->CorporatePhone2) ? $this->CorporatePhone2 : null;
    }
    /**
     * Set CorporatePhone2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $corporatePhone2
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setCorporatePhone2(?string $corporatePhone2 = null): self
    {
        // validation for constraint: string
        if (!is_null($corporatePhone2) && !is_string($corporatePhone2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($corporatePhone2, true), gettype($corporatePhone2)), __LINE__);
        }
        if (is_null($corporatePhone2) || (is_array($corporatePhone2) && empty($corporatePhone2))) {
            unset($this->CorporatePhone2);
        } else {
            $this->CorporatePhone2 = $corporatePhone2;
        }
        
        return $this;
    }
    /**
     * Get CorporateFax value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCorporateFax(): ?string
    {
        return isset($this->CorporateFax) ? $this->CorporateFax : null;
    }
    /**
     * Set CorporateFax value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $corporateFax
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setCorporateFax(?string $corporateFax = null): self
    {
        // validation for constraint: string
        if (!is_null($corporateFax) && !is_string($corporateFax)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($corporateFax, true), gettype($corporateFax)), __LINE__);
        }
        if (is_null($corporateFax) || (is_array($corporateFax) && empty($corporateFax))) {
            unset($this->CorporateFax);
        } else {
            $this->CorporateFax = $corporateFax;
        }
        
        return $this;
    }
    /**
     * Get CorporateEmail value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCorporateEmail(): ?string
    {
        return isset($this->CorporateEmail) ? $this->CorporateEmail : null;
    }
    /**
     * Set CorporateEmail value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $corporateEmail
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setCorporateEmail(?string $corporateEmail = null): self
    {
        // validation for constraint: string
        if (!is_null($corporateEmail) && !is_string($corporateEmail)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($corporateEmail, true), gettype($corporateEmail)), __LINE__);
        }
        if (is_null($corporateEmail) || (is_array($corporateEmail) && empty($corporateEmail))) {
            unset($this->CorporateEmail);
        } else {
            $this->CorporateEmail = $corporateEmail;
        }
        
        return $this;
    }
    /**
     * Get RegistrationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRegistrationDate(): ?string
    {
        return isset($this->RegistrationDate) ? $this->RegistrationDate : null;
    }
    /**
     * Set RegistrationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $registrationDate
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setRegistrationDate(?string $registrationDate = null): self
    {
        // validation for constraint: string
        if (!is_null($registrationDate) && !is_string($registrationDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($registrationDate, true), gettype($registrationDate)), __LINE__);
        }
        if (is_null($registrationDate) || (is_array($registrationDate) && empty($registrationDate))) {
            unset($this->RegistrationDate);
        } else {
            $this->RegistrationDate = $registrationDate;
        }
        
        return $this;
    }
    /**
     * Get RenewalDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRenewalDate(): ?string
    {
        return isset($this->RenewalDate) ? $this->RenewalDate : null;
    }
    /**
     * Set RenewalDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $renewalDate
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setRenewalDate(?string $renewalDate = null): self
    {
        // validation for constraint: string
        if (!is_null($renewalDate) && !is_string($renewalDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($renewalDate, true), gettype($renewalDate)), __LINE__);
        }
        if (is_null($renewalDate) || (is_array($renewalDate) && empty($renewalDate))) {
            unset($this->RenewalDate);
        } else {
            $this->RenewalDate = $renewalDate;
        }
        
        return $this;
    }
    /**
     * Get BusinessStartDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBusinessStartDate(): ?string
    {
        return isset($this->BusinessStartDate) ? $this->BusinessStartDate : null;
    }
    /**
     * Set BusinessStartDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $businessStartDate
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setBusinessStartDate(?string $businessStartDate = null): self
    {
        // validation for constraint: string
        if (!is_null($businessStartDate) && !is_string($businessStartDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($businessStartDate, true), gettype($businessStartDate)), __LINE__);
        }
        if (is_null($businessStartDate) || (is_array($businessStartDate) && empty($businessStartDate))) {
            unset($this->BusinessStartDate);
        } else {
            $this->BusinessStartDate = $businessStartDate;
        }
        
        return $this;
    }
    /**
     * Get FiscalYearEndClose value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFiscalYearEndClose(): ?string
    {
        return isset($this->FiscalYearEndClose) ? $this->FiscalYearEndClose : null;
    }
    /**
     * Set FiscalYearEndClose value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fiscalYearEndClose
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setFiscalYearEndClose(?string $fiscalYearEndClose = null): self
    {
        // validation for constraint: string
        if (!is_null($fiscalYearEndClose) && !is_string($fiscalYearEndClose)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fiscalYearEndClose, true), gettype($fiscalYearEndClose)), __LINE__);
        }
        if (is_null($fiscalYearEndClose) || (is_array($fiscalYearEndClose) && empty($fiscalYearEndClose))) {
            unset($this->FiscalYearEndClose);
        } else {
            $this->FiscalYearEndClose = $fiscalYearEndClose;
        }
        
        return $this;
    }
    /**
     * Get OrganizationalType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrganizationalType(): ?string
    {
        return isset($this->OrganizationalType) ? $this->OrganizationalType : null;
    }
    /**
     * Set OrganizationalType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $organizationalType
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setOrganizationalType(?string $organizationalType = null): self
    {
        // validation for constraint: string
        if (!is_null($organizationalType) && !is_string($organizationalType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($organizationalType, true), gettype($organizationalType)), __LINE__);
        }
        if (is_null($organizationalType) || (is_array($organizationalType) && empty($organizationalType))) {
            unset($this->OrganizationalType);
        } else {
            $this->OrganizationalType = $organizationalType;
        }
        
        return $this;
    }
    /**
     * Get CountryofIncorporation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryofIncorporation(): ?string
    {
        return isset($this->CountryofIncorporation) ? $this->CountryofIncorporation : null;
    }
    /**
     * Set CountryofIncorporation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryofIncorporation
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setCountryofIncorporation(?string $countryofIncorporation = null): self
    {
        // validation for constraint: string
        if (!is_null($countryofIncorporation) && !is_string($countryofIncorporation)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryofIncorporation, true), gettype($countryofIncorporation)), __LINE__);
        }
        if (is_null($countryofIncorporation) || (is_array($countryofIncorporation) && empty($countryofIncorporation))) {
            unset($this->CountryofIncorporation);
        } else {
            $this->CountryofIncorporation = $countryofIncorporation;
        }
        
        return $this;
    }
    /**
     * Get StateofIncorporation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStateofIncorporation(): ?string
    {
        return isset($this->StateofIncorporation) ? $this->StateofIncorporation : null;
    }
    /**
     * Set StateofIncorporation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $stateofIncorporation
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setStateofIncorporation(?string $stateofIncorporation = null): self
    {
        // validation for constraint: string
        if (!is_null($stateofIncorporation) && !is_string($stateofIncorporation)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($stateofIncorporation, true), gettype($stateofIncorporation)), __LINE__);
        }
        if (is_null($stateofIncorporation) || (is_array($stateofIncorporation) && empty($stateofIncorporation))) {
            unset($this->StateofIncorporation);
        } else {
            $this->StateofIncorporation = $stateofIncorporation;
        }
        
        return $this;
    }
    /**
     * Get OwnerName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOwnerName(): ?string
    {
        return isset($this->OwnerName) ? $this->OwnerName : null;
    }
    /**
     * Set OwnerName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $ownerName
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setOwnerName(?string $ownerName = null): self
    {
        // validation for constraint: string
        if (!is_null($ownerName) && !is_string($ownerName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ownerName, true), gettype($ownerName)), __LINE__);
        }
        if (is_null($ownerName) || (is_array($ownerName) && empty($ownerName))) {
            unset($this->OwnerName);
        } else {
            $this->OwnerName = $ownerName;
        }
        
        return $this;
    }
    /**
     * Get OwnerEmail value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOwnerEmail(): ?string
    {
        return isset($this->OwnerEmail) ? $this->OwnerEmail : null;
    }
    /**
     * Set OwnerEmail value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $ownerEmail
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setOwnerEmail(?string $ownerEmail = null): self
    {
        // validation for constraint: string
        if (!is_null($ownerEmail) && !is_string($ownerEmail)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ownerEmail, true), gettype($ownerEmail)), __LINE__);
        }
        if (is_null($ownerEmail) || (is_array($ownerEmail) && empty($ownerEmail))) {
            unset($this->OwnerEmail);
        } else {
            $this->OwnerEmail = $ownerEmail;
        }
        
        return $this;
    }
    /**
     * Get OwnerPhone1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOwnerPhone1(): ?string
    {
        return isset($this->OwnerPhone1) ? $this->OwnerPhone1 : null;
    }
    /**
     * Set OwnerPhone1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $ownerPhone1
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setOwnerPhone1(?string $ownerPhone1 = null): self
    {
        // validation for constraint: string
        if (!is_null($ownerPhone1) && !is_string($ownerPhone1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ownerPhone1, true), gettype($ownerPhone1)), __LINE__);
        }
        if (is_null($ownerPhone1) || (is_array($ownerPhone1) && empty($ownerPhone1))) {
            unset($this->OwnerPhone1);
        } else {
            $this->OwnerPhone1 = $ownerPhone1;
        }
        
        return $this;
    }
    /**
     * Get OwnerPhone2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOwnerPhone2(): ?string
    {
        return isset($this->OwnerPhone2) ? $this->OwnerPhone2 : null;
    }
    /**
     * Set OwnerPhone2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $ownerPhone2
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setOwnerPhone2(?string $ownerPhone2 = null): self
    {
        // validation for constraint: string
        if (!is_null($ownerPhone2) && !is_string($ownerPhone2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ownerPhone2, true), gettype($ownerPhone2)), __LINE__);
        }
        if (is_null($ownerPhone2) || (is_array($ownerPhone2) && empty($ownerPhone2))) {
            unset($this->OwnerPhone2);
        } else {
            $this->OwnerPhone2 = $ownerPhone2;
        }
        
        return $this;
    }
    /**
     * Get OwnerFax value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOwnerFax(): ?string
    {
        return isset($this->OwnerFax) ? $this->OwnerFax : null;
    }
    /**
     * Set OwnerFax value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $ownerFax
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setOwnerFax(?string $ownerFax = null): self
    {
        // validation for constraint: string
        if (!is_null($ownerFax) && !is_string($ownerFax)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ownerFax, true), gettype($ownerFax)), __LINE__);
        }
        if (is_null($ownerFax) || (is_array($ownerFax) && empty($ownerFax))) {
            unset($this->OwnerFax);
        } else {
            $this->OwnerFax = $ownerFax;
        }
        
        return $this;
    }
    /**
     * Get GovtAddressName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGovtAddressName(): ?string
    {
        return isset($this->GovtAddressName) ? $this->GovtAddressName : null;
    }
    /**
     * Set GovtAddressName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $govtAddressName
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setGovtAddressName(?string $govtAddressName = null): self
    {
        // validation for constraint: string
        if (!is_null($govtAddressName) && !is_string($govtAddressName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($govtAddressName, true), gettype($govtAddressName)), __LINE__);
        }
        if (is_null($govtAddressName) || (is_array($govtAddressName) && empty($govtAddressName))) {
            unset($this->GovtAddressName);
        } else {
            $this->GovtAddressName = $govtAddressName;
        }
        
        return $this;
    }
    /**
     * Get GovtAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGovtAddress(): ?string
    {
        return isset($this->GovtAddress) ? $this->GovtAddress : null;
    }
    /**
     * Set GovtAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $govtAddress
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setGovtAddress(?string $govtAddress = null): self
    {
        // validation for constraint: string
        if (!is_null($govtAddress) && !is_string($govtAddress)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($govtAddress, true), gettype($govtAddress)), __LINE__);
        }
        if (is_null($govtAddress) || (is_array($govtAddress) && empty($govtAddress))) {
            unset($this->GovtAddress);
        } else {
            $this->GovtAddress = $govtAddress;
        }
        
        return $this;
    }
    /**
     * Get GovtPhone1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGovtPhone1(): ?string
    {
        return isset($this->GovtPhone1) ? $this->GovtPhone1 : null;
    }
    /**
     * Set GovtPhone1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $govtPhone1
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setGovtPhone1(?string $govtPhone1 = null): self
    {
        // validation for constraint: string
        if (!is_null($govtPhone1) && !is_string($govtPhone1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($govtPhone1, true), gettype($govtPhone1)), __LINE__);
        }
        if (is_null($govtPhone1) || (is_array($govtPhone1) && empty($govtPhone1))) {
            unset($this->GovtPhone1);
        } else {
            $this->GovtPhone1 = $govtPhone1;
        }
        
        return $this;
    }
    /**
     * Get GovtPhone2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGovtPhone2(): ?string
    {
        return isset($this->GovtPhone2) ? $this->GovtPhone2 : null;
    }
    /**
     * Set GovtPhone2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $govtPhone2
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setGovtPhone2(?string $govtPhone2 = null): self
    {
        // validation for constraint: string
        if (!is_null($govtPhone2) && !is_string($govtPhone2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($govtPhone2, true), gettype($govtPhone2)), __LINE__);
        }
        if (is_null($govtPhone2) || (is_array($govtPhone2) && empty($govtPhone2))) {
            unset($this->GovtPhone2);
        } else {
            $this->GovtPhone2 = $govtPhone2;
        }
        
        return $this;
    }
    /**
     * Get GovtEmail value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGovtEmail(): ?string
    {
        return isset($this->GovtEmail) ? $this->GovtEmail : null;
    }
    /**
     * Set GovtEmail value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $govtEmail
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setGovtEmail(?string $govtEmail = null): self
    {
        // validation for constraint: string
        if (!is_null($govtEmail) && !is_string($govtEmail)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($govtEmail, true), gettype($govtEmail)), __LINE__);
        }
        if (is_null($govtEmail) || (is_array($govtEmail) && empty($govtEmail))) {
            unset($this->GovtEmail);
        } else {
            $this->GovtEmail = $govtEmail;
        }
        
        return $this;
    }
    /**
     * Get GovtFax value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGovtFax(): ?string
    {
        return isset($this->GovtFax) ? $this->GovtFax : null;
    }
    /**
     * Set GovtFax value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $govtFax
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setGovtFax(?string $govtFax = null): self
    {
        // validation for constraint: string
        if (!is_null($govtFax) && !is_string($govtFax)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($govtFax, true), gettype($govtFax)), __LINE__);
        }
        if (is_null($govtFax) || (is_array($govtFax) && empty($govtFax))) {
            unset($this->GovtFax);
        } else {
            $this->GovtFax = $govtFax;
        }
        
        return $this;
    }
    /**
     * Get AltGovtAddressName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAltGovtAddressName(): ?string
    {
        return isset($this->AltGovtAddressName) ? $this->AltGovtAddressName : null;
    }
    /**
     * Set AltGovtAddressName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $altGovtAddressName
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setAltGovtAddressName(?string $altGovtAddressName = null): self
    {
        // validation for constraint: string
        if (!is_null($altGovtAddressName) && !is_string($altGovtAddressName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($altGovtAddressName, true), gettype($altGovtAddressName)), __LINE__);
        }
        if (is_null($altGovtAddressName) || (is_array($altGovtAddressName) && empty($altGovtAddressName))) {
            unset($this->AltGovtAddressName);
        } else {
            $this->AltGovtAddressName = $altGovtAddressName;
        }
        
        return $this;
    }
    /**
     * Get AltGovtAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAltGovtAddress(): ?string
    {
        return isset($this->AltGovtAddress) ? $this->AltGovtAddress : null;
    }
    /**
     * Set AltGovtAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $altGovtAddress
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setAltGovtAddress(?string $altGovtAddress = null): self
    {
        // validation for constraint: string
        if (!is_null($altGovtAddress) && !is_string($altGovtAddress)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($altGovtAddress, true), gettype($altGovtAddress)), __LINE__);
        }
        if (is_null($altGovtAddress) || (is_array($altGovtAddress) && empty($altGovtAddress))) {
            unset($this->AltGovtAddress);
        } else {
            $this->AltGovtAddress = $altGovtAddress;
        }
        
        return $this;
    }
    /**
     * Get AltGovtPhone1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAltGovtPhone1(): ?string
    {
        return isset($this->AltGovtPhone1) ? $this->AltGovtPhone1 : null;
    }
    /**
     * Set AltGovtPhone1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $altGovtPhone1
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setAltGovtPhone1(?string $altGovtPhone1 = null): self
    {
        // validation for constraint: string
        if (!is_null($altGovtPhone1) && !is_string($altGovtPhone1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($altGovtPhone1, true), gettype($altGovtPhone1)), __LINE__);
        }
        if (is_null($altGovtPhone1) || (is_array($altGovtPhone1) && empty($altGovtPhone1))) {
            unset($this->AltGovtPhone1);
        } else {
            $this->AltGovtPhone1 = $altGovtPhone1;
        }
        
        return $this;
    }
    /**
     * Get AltGovtPhone2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAltGovtPhone2(): ?string
    {
        return isset($this->AltGovtPhone2) ? $this->AltGovtPhone2 : null;
    }
    /**
     * Set AltGovtPhone2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $altGovtPhone2
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setAltGovtPhone2(?string $altGovtPhone2 = null): self
    {
        // validation for constraint: string
        if (!is_null($altGovtPhone2) && !is_string($altGovtPhone2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($altGovtPhone2, true), gettype($altGovtPhone2)), __LINE__);
        }
        if (is_null($altGovtPhone2) || (is_array($altGovtPhone2) && empty($altGovtPhone2))) {
            unset($this->AltGovtPhone2);
        } else {
            $this->AltGovtPhone2 = $altGovtPhone2;
        }
        
        return $this;
    }
    /**
     * Get AltGovtFax value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAltGovtFax(): ?string
    {
        return isset($this->AltGovtFax) ? $this->AltGovtFax : null;
    }
    /**
     * Set AltGovtFax value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $altGovtFax
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setAltGovtFax(?string $altGovtFax = null): self
    {
        // validation for constraint: string
        if (!is_null($altGovtFax) && !is_string($altGovtFax)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($altGovtFax, true), gettype($altGovtFax)), __LINE__);
        }
        if (is_null($altGovtFax) || (is_array($altGovtFax) && empty($altGovtFax))) {
            unset($this->AltGovtFax);
        } else {
            $this->AltGovtFax = $altGovtFax;
        }
        
        return $this;
    }
    /**
     * Get AltGovtEmail value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAltGovtEmail(): ?string
    {
        return isset($this->AltGovtEmail) ? $this->AltGovtEmail : null;
    }
    /**
     * Set AltGovtEmail value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $altGovtEmail
     * @return \ID3Global\Models\GlobalFCRAMatchDetails
     */
    public function setAltGovtEmail(?string $altGovtEmail = null): self
    {
        // validation for constraint: string
        if (!is_null($altGovtEmail) && !is_string($altGovtEmail)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($altGovtEmail, true), gettype($altGovtEmail)), __LINE__);
        }
        if (is_null($altGovtEmail) || (is_array($altGovtEmail) && empty($altGovtEmail))) {
            unset($this->AltGovtEmail);
        } else {
            $this->AltGovtEmail = $altGovtEmail;
        }
        
        return $this;
    }
}
