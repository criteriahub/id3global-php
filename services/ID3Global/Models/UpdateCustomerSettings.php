<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateCustomerSettings Models
 * @subpackage Structs
 */
class UpdateCustomerSettings extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The Settings
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseCustomerSettings|null
     */
    protected ?\ID3Global\Models\GlobalCaseCustomerSettings $Settings = null;
    /**
     * Constructor method for UpdateCustomerSettings
     * @uses UpdateCustomerSettings::setOrgID()
     * @uses UpdateCustomerSettings::setSettings()
     * @param string $orgID
     * @param \ID3Global\Models\GlobalCaseCustomerSettings $settings
     */
    public function __construct(?string $orgID = null, ?\ID3Global\Models\GlobalCaseCustomerSettings $settings = null)
    {
        $this
            ->setOrgID($orgID)
            ->setSettings($settings);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\UpdateCustomerSettings
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get Settings value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseCustomerSettings|null
     */
    public function getSettings(): ?\ID3Global\Models\GlobalCaseCustomerSettings
    {
        return isset($this->Settings) ? $this->Settings : null;
    }
    /**
     * Set Settings value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCaseCustomerSettings $settings
     * @return \ID3Global\Models\UpdateCustomerSettings
     */
    public function setSettings(?\ID3Global\Models\GlobalCaseCustomerSettings $settings = null): self
    {
        if (is_null($settings) || (is_array($settings) && empty($settings))) {
            unset($this->Settings);
        } else {
            $this->Settings = $settings;
        }
        
        return $this;
    }
}
