<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateCaseResponse Models
 * @subpackage Structs
 */
class CreateCaseResponse extends AbstractStructBase
{
    /**
     * The CreateCaseResult
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $CreateCaseResult = null;
    /**
     * Constructor method for CreateCaseResponse
     * @uses CreateCaseResponse::setCreateCaseResult()
     * @param string $createCaseResult
     */
    public function __construct(?string $createCaseResult = null)
    {
        $this
            ->setCreateCaseResult($createCaseResult);
    }
    /**
     * Get CreateCaseResult value
     * @return string|null
     */
    public function getCreateCaseResult(): ?string
    {
        return $this->CreateCaseResult;
    }
    /**
     * Set CreateCaseResult value
     * @param string $createCaseResult
     * @return \ID3Global\Models\CreateCaseResponse
     */
    public function setCreateCaseResult(?string $createCaseResult = null): self
    {
        // validation for constraint: string
        if (!is_null($createCaseResult) && !is_string($createCaseResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($createCaseResult, true), gettype($createCaseResult)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($createCaseResult) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $createCaseResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($createCaseResult, true)), __LINE__);
        }
        $this->CreateCaseResult = $createCaseResult;
        
        return $this;
    }
}
