<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalUnitedStatesAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q685:GlobalUnitedStatesAccount
 * @subpackage Structs
 */
class GlobalUnitedStatesAccount extends GlobalSupplierAccount
{
    /**
     * The MerchantID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MerchantID = null;
    /**
     * The SubMerchantID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SubMerchantID = null;
    /**
     * Constructor method for GlobalUnitedStatesAccount
     * @uses GlobalUnitedStatesAccount::setMerchantID()
     * @uses GlobalUnitedStatesAccount::setSubMerchantID()
     * @param string $merchantID
     * @param string $subMerchantID
     */
    public function __construct(?string $merchantID = null, ?string $subMerchantID = null)
    {
        $this
            ->setMerchantID($merchantID)
            ->setSubMerchantID($subMerchantID);
    }
    /**
     * Get MerchantID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMerchantID(): ?string
    {
        return isset($this->MerchantID) ? $this->MerchantID : null;
    }
    /**
     * Set MerchantID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $merchantID
     * @return \ID3Global\Models\GlobalUnitedStatesAccount
     */
    public function setMerchantID(?string $merchantID = null): self
    {
        // validation for constraint: string
        if (!is_null($merchantID) && !is_string($merchantID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($merchantID, true), gettype($merchantID)), __LINE__);
        }
        if (is_null($merchantID) || (is_array($merchantID) && empty($merchantID))) {
            unset($this->MerchantID);
        } else {
            $this->MerchantID = $merchantID;
        }
        
        return $this;
    }
    /**
     * Get SubMerchantID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSubMerchantID(): ?string
    {
        return isset($this->SubMerchantID) ? $this->SubMerchantID : null;
    }
    /**
     * Set SubMerchantID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $subMerchantID
     * @return \ID3Global\Models\GlobalUnitedStatesAccount
     */
    public function setSubMerchantID(?string $subMerchantID = null): self
    {
        // validation for constraint: string
        if (!is_null($subMerchantID) && !is_string($subMerchantID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subMerchantID, true), gettype($subMerchantID)), __LINE__);
        }
        if (is_null($subMerchantID) || (is_array($subMerchantID) && empty($subMerchantID))) {
            unset($this->SubMerchantID);
        } else {
            $this->SubMerchantID = $subMerchantID;
        }
        
        return $this;
    }
}
