<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCase Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q759:GlobalCase
 * @subpackage Structs
 */
class GlobalCase extends AbstractStructBase
{
    /**
     * The CaseID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $CaseID = null;
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The LinkedAccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $LinkedAccountID = null;
    /**
     * The Reference
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Reference = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The State
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $State = null;
    /**
     * The OrganisationName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OrganisationName = null;
    /**
     * The Created
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Created = null;
    /**
     * The Updated
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Updated = null;
    /**
     * The Verdicts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseVerdict|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseVerdict $Verdicts = null;
    /**
     * The AuthorType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $AuthorType = null;
    /**
     * The ChargingPoints
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseChargingPoint|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseChargingPoint $ChargingPoints = null;
    /**
     * The CreatedOrganisationID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $CreatedOrganisationID = null;
    /**
     * The Properties
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $Properties = null;
    /**
     * The CustomerReferences
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseReference|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseReference $CustomerReferences = null;
    /**
     * The IsMultipleDocument
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsMultipleDocument = null;
    /**
     * The DocumentCount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $DocumentCount = null;
    /**
     * Constructor method for GlobalCase
     * @uses GlobalCase::setCaseID()
     * @uses GlobalCase::setOrgID()
     * @uses GlobalCase::setLinkedAccountID()
     * @uses GlobalCase::setReference()
     * @uses GlobalCase::setName()
     * @uses GlobalCase::setState()
     * @uses GlobalCase::setOrganisationName()
     * @uses GlobalCase::setCreated()
     * @uses GlobalCase::setUpdated()
     * @uses GlobalCase::setVerdicts()
     * @uses GlobalCase::setAuthorType()
     * @uses GlobalCase::setChargingPoints()
     * @uses GlobalCase::setCreatedOrganisationID()
     * @uses GlobalCase::setProperties()
     * @uses GlobalCase::setCustomerReferences()
     * @uses GlobalCase::setIsMultipleDocument()
     * @uses GlobalCase::setDocumentCount()
     * @param string $caseID
     * @param string $orgID
     * @param string $linkedAccountID
     * @param string $reference
     * @param string $name
     * @param int $state
     * @param string $organisationName
     * @param string $created
     * @param string $updated
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseVerdict $verdicts
     * @param int $authorType
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseChargingPoint $chargingPoints
     * @param string $createdOrganisationID
     * @param \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $properties
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseReference $customerReferences
     * @param bool $isMultipleDocument
     * @param int $documentCount
     */
    public function __construct(?string $caseID = null, ?string $orgID = null, ?string $linkedAccountID = null, ?string $reference = null, ?string $name = null, ?int $state = null, ?string $organisationName = null, ?string $created = null, ?string $updated = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseVerdict $verdicts = null, ?int $authorType = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseChargingPoint $chargingPoints = null, ?string $createdOrganisationID = null, ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $properties = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseReference $customerReferences = null, ?bool $isMultipleDocument = null, ?int $documentCount = null)
    {
        $this
            ->setCaseID($caseID)
            ->setOrgID($orgID)
            ->setLinkedAccountID($linkedAccountID)
            ->setReference($reference)
            ->setName($name)
            ->setState($state)
            ->setOrganisationName($organisationName)
            ->setCreated($created)
            ->setUpdated($updated)
            ->setVerdicts($verdicts)
            ->setAuthorType($authorType)
            ->setChargingPoints($chargingPoints)
            ->setCreatedOrganisationID($createdOrganisationID)
            ->setProperties($properties)
            ->setCustomerReferences($customerReferences)
            ->setIsMultipleDocument($isMultipleDocument)
            ->setDocumentCount($documentCount);
    }
    /**
     * Get CaseID value
     * @return string|null
     */
    public function getCaseID(): ?string
    {
        return $this->CaseID;
    }
    /**
     * Set CaseID value
     * @param string $caseID
     * @return \ID3Global\Models\GlobalCase
     */
    public function setCaseID(?string $caseID = null): self
    {
        // validation for constraint: string
        if (!is_null($caseID) && !is_string($caseID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($caseID, true), gettype($caseID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($caseID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $caseID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($caseID, true)), __LINE__);
        }
        $this->CaseID = $caseID;
        
        return $this;
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GlobalCase
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get LinkedAccountID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLinkedAccountID(): ?string
    {
        return isset($this->LinkedAccountID) ? $this->LinkedAccountID : null;
    }
    /**
     * Set LinkedAccountID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $linkedAccountID
     * @return \ID3Global\Models\GlobalCase
     */
    public function setLinkedAccountID(?string $linkedAccountID = null): self
    {
        // validation for constraint: string
        if (!is_null($linkedAccountID) && !is_string($linkedAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($linkedAccountID, true), gettype($linkedAccountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($linkedAccountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $linkedAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($linkedAccountID, true)), __LINE__);
        }
        if (is_null($linkedAccountID) || (is_array($linkedAccountID) && empty($linkedAccountID))) {
            unset($this->LinkedAccountID);
        } else {
            $this->LinkedAccountID = $linkedAccountID;
        }
        
        return $this;
    }
    /**
     * Get Reference value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReference(): ?string
    {
        return isset($this->Reference) ? $this->Reference : null;
    }
    /**
     * Set Reference value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $reference
     * @return \ID3Global\Models\GlobalCase
     */
    public function setReference(?string $reference = null): self
    {
        // validation for constraint: string
        if (!is_null($reference) && !is_string($reference)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($reference, true), gettype($reference)), __LINE__);
        }
        if (is_null($reference) || (is_array($reference) && empty($reference))) {
            unset($this->Reference);
        } else {
            $this->Reference = $reference;
        }
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalCase
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get State value
     * @return int|null
     */
    public function getState(): ?int
    {
        return $this->State;
    }
    /**
     * Set State value
     * @param int $state
     * @return \ID3Global\Models\GlobalCase
     */
    public function setState(?int $state = null): self
    {
        // validation for constraint: int
        if (!is_null($state) && !(is_int($state) || ctype_digit($state))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($state, true), gettype($state)), __LINE__);
        }
        $this->State = $state;
        
        return $this;
    }
    /**
     * Get OrganisationName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrganisationName(): ?string
    {
        return isset($this->OrganisationName) ? $this->OrganisationName : null;
    }
    /**
     * Set OrganisationName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $organisationName
     * @return \ID3Global\Models\GlobalCase
     */
    public function setOrganisationName(?string $organisationName = null): self
    {
        // validation for constraint: string
        if (!is_null($organisationName) && !is_string($organisationName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($organisationName, true), gettype($organisationName)), __LINE__);
        }
        if (is_null($organisationName) || (is_array($organisationName) && empty($organisationName))) {
            unset($this->OrganisationName);
        } else {
            $this->OrganisationName = $organisationName;
        }
        
        return $this;
    }
    /**
     * Get Created value
     * @return string|null
     */
    public function getCreated(): ?string
    {
        return $this->Created;
    }
    /**
     * Set Created value
     * @param string $created
     * @return \ID3Global\Models\GlobalCase
     */
    public function setCreated(?string $created = null): self
    {
        // validation for constraint: string
        if (!is_null($created) && !is_string($created)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($created, true), gettype($created)), __LINE__);
        }
        $this->Created = $created;
        
        return $this;
    }
    /**
     * Get Updated value
     * @return string|null
     */
    public function getUpdated(): ?string
    {
        return $this->Updated;
    }
    /**
     * Set Updated value
     * @param string $updated
     * @return \ID3Global\Models\GlobalCase
     */
    public function setUpdated(?string $updated = null): self
    {
        // validation for constraint: string
        if (!is_null($updated) && !is_string($updated)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updated, true), gettype($updated)), __LINE__);
        }
        $this->Updated = $updated;
        
        return $this;
    }
    /**
     * Get Verdicts value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseVerdict|null
     */
    public function getVerdicts(): ?\ID3Global\Arrays\ArrayOfGlobalCaseVerdict
    {
        return isset($this->Verdicts) ? $this->Verdicts : null;
    }
    /**
     * Set Verdicts value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseVerdict $verdicts
     * @return \ID3Global\Models\GlobalCase
     */
    public function setVerdicts(?\ID3Global\Arrays\ArrayOfGlobalCaseVerdict $verdicts = null): self
    {
        if (is_null($verdicts) || (is_array($verdicts) && empty($verdicts))) {
            unset($this->Verdicts);
        } else {
            $this->Verdicts = $verdicts;
        }
        
        return $this;
    }
    /**
     * Get AuthorType value
     * @return int|null
     */
    public function getAuthorType(): ?int
    {
        return $this->AuthorType;
    }
    /**
     * Set AuthorType value
     * @param int $authorType
     * @return \ID3Global\Models\GlobalCase
     */
    public function setAuthorType(?int $authorType = null): self
    {
        // validation for constraint: int
        if (!is_null($authorType) && !(is_int($authorType) || ctype_digit($authorType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($authorType, true), gettype($authorType)), __LINE__);
        }
        $this->AuthorType = $authorType;
        
        return $this;
    }
    /**
     * Get ChargingPoints value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseChargingPoint|null
     */
    public function getChargingPoints(): ?\ID3Global\Arrays\ArrayOfGlobalCaseChargingPoint
    {
        return isset($this->ChargingPoints) ? $this->ChargingPoints : null;
    }
    /**
     * Set ChargingPoints value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseChargingPoint $chargingPoints
     * @return \ID3Global\Models\GlobalCase
     */
    public function setChargingPoints(?\ID3Global\Arrays\ArrayOfGlobalCaseChargingPoint $chargingPoints = null): self
    {
        if (is_null($chargingPoints) || (is_array($chargingPoints) && empty($chargingPoints))) {
            unset($this->ChargingPoints);
        } else {
            $this->ChargingPoints = $chargingPoints;
        }
        
        return $this;
    }
    /**
     * Get CreatedOrganisationID value
     * @return string|null
     */
    public function getCreatedOrganisationID(): ?string
    {
        return $this->CreatedOrganisationID;
    }
    /**
     * Set CreatedOrganisationID value
     * @param string $createdOrganisationID
     * @return \ID3Global\Models\GlobalCase
     */
    public function setCreatedOrganisationID(?string $createdOrganisationID = null): self
    {
        // validation for constraint: string
        if (!is_null($createdOrganisationID) && !is_string($createdOrganisationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($createdOrganisationID, true), gettype($createdOrganisationID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($createdOrganisationID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $createdOrganisationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($createdOrganisationID, true)), __LINE__);
        }
        $this->CreatedOrganisationID = $createdOrganisationID;
        
        return $this;
    }
    /**
     * Get Properties value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring|null
     */
    public function getProperties(): ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring
    {
        return isset($this->Properties) ? $this->Properties : null;
    }
    /**
     * Set Properties value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $properties
     * @return \ID3Global\Models\GlobalCase
     */
    public function setProperties(?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringstring $properties = null): self
    {
        if (is_null($properties) || (is_array($properties) && empty($properties))) {
            unset($this->Properties);
        } else {
            $this->Properties = $properties;
        }
        
        return $this;
    }
    /**
     * Get CustomerReferences value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseReference|null
     */
    public function getCustomerReferences(): ?\ID3Global\Arrays\ArrayOfGlobalCaseReference
    {
        return isset($this->CustomerReferences) ? $this->CustomerReferences : null;
    }
    /**
     * Set CustomerReferences value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseReference $customerReferences
     * @return \ID3Global\Models\GlobalCase
     */
    public function setCustomerReferences(?\ID3Global\Arrays\ArrayOfGlobalCaseReference $customerReferences = null): self
    {
        if (is_null($customerReferences) || (is_array($customerReferences) && empty($customerReferences))) {
            unset($this->CustomerReferences);
        } else {
            $this->CustomerReferences = $customerReferences;
        }
        
        return $this;
    }
    /**
     * Get IsMultipleDocument value
     * @return bool|null
     */
    public function getIsMultipleDocument(): ?bool
    {
        return $this->IsMultipleDocument;
    }
    /**
     * Set IsMultipleDocument value
     * @param bool $isMultipleDocument
     * @return \ID3Global\Models\GlobalCase
     */
    public function setIsMultipleDocument(?bool $isMultipleDocument = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isMultipleDocument) && !is_bool($isMultipleDocument)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isMultipleDocument, true), gettype($isMultipleDocument)), __LINE__);
        }
        $this->IsMultipleDocument = $isMultipleDocument;
        
        return $this;
    }
    /**
     * Get DocumentCount value
     * @return int|null
     */
    public function getDocumentCount(): ?int
    {
        return $this->DocumentCount;
    }
    /**
     * Set DocumentCount value
     * @param int $documentCount
     * @return \ID3Global\Models\GlobalCase
     */
    public function setDocumentCount(?int $documentCount = null): self
    {
        // validation for constraint: int
        if (!is_null($documentCount) && !(is_int($documentCount) || ctype_digit($documentCount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($documentCount, true), gettype($documentCount)), __LINE__);
        }
        $this->DocumentCount = $documentCount;
        
        return $this;
    }
}
