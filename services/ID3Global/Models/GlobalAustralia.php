<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAustralia Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q374:GlobalAustralia
 * @subpackage Structs
 */
class GlobalAustralia extends AbstractStructBase
{
    /**
     * The ShortPassport
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalShortPassport|null
     */
    protected ?\ID3Global\Models\GlobalShortPassport $ShortPassport = null;
    /**
     * The Medicare
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalMedicare|null
     */
    protected ?\ID3Global\Models\GlobalMedicare $Medicare = null;
    /**
     * The DrivingLicence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAustraliaDrivingLicence|null
     */
    protected ?\ID3Global\Models\GlobalAustraliaDrivingLicence $DrivingLicence = null;
    /**
     * Constructor method for GlobalAustralia
     * @uses GlobalAustralia::setShortPassport()
     * @uses GlobalAustralia::setMedicare()
     * @uses GlobalAustralia::setDrivingLicence()
     * @param \ID3Global\Models\GlobalShortPassport $shortPassport
     * @param \ID3Global\Models\GlobalMedicare $medicare
     * @param \ID3Global\Models\GlobalAustraliaDrivingLicence $drivingLicence
     */
    public function __construct(?\ID3Global\Models\GlobalShortPassport $shortPassport = null, ?\ID3Global\Models\GlobalMedicare $medicare = null, ?\ID3Global\Models\GlobalAustraliaDrivingLicence $drivingLicence = null)
    {
        $this
            ->setShortPassport($shortPassport)
            ->setMedicare($medicare)
            ->setDrivingLicence($drivingLicence);
    }
    /**
     * Get ShortPassport value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalShortPassport|null
     */
    public function getShortPassport(): ?\ID3Global\Models\GlobalShortPassport
    {
        return isset($this->ShortPassport) ? $this->ShortPassport : null;
    }
    /**
     * Set ShortPassport value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalShortPassport $shortPassport
     * @return \ID3Global\Models\GlobalAustralia
     */
    public function setShortPassport(?\ID3Global\Models\GlobalShortPassport $shortPassport = null): self
    {
        if (is_null($shortPassport) || (is_array($shortPassport) && empty($shortPassport))) {
            unset($this->ShortPassport);
        } else {
            $this->ShortPassport = $shortPassport;
        }
        
        return $this;
    }
    /**
     * Get Medicare value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalMedicare|null
     */
    public function getMedicare(): ?\ID3Global\Models\GlobalMedicare
    {
        return isset($this->Medicare) ? $this->Medicare : null;
    }
    /**
     * Set Medicare value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalMedicare $medicare
     * @return \ID3Global\Models\GlobalAustralia
     */
    public function setMedicare(?\ID3Global\Models\GlobalMedicare $medicare = null): self
    {
        if (is_null($medicare) || (is_array($medicare) && empty($medicare))) {
            unset($this->Medicare);
        } else {
            $this->Medicare = $medicare;
        }
        
        return $this;
    }
    /**
     * Get DrivingLicence value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAustraliaDrivingLicence|null
     */
    public function getDrivingLicence(): ?\ID3Global\Models\GlobalAustraliaDrivingLicence
    {
        return isset($this->DrivingLicence) ? $this->DrivingLicence : null;
    }
    /**
     * Set DrivingLicence value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAustraliaDrivingLicence $drivingLicence
     * @return \ID3Global\Models\GlobalAustralia
     */
    public function setDrivingLicence(?\ID3Global\Models\GlobalAustraliaDrivingLicence $drivingLicence = null): self
    {
        if (is_null($drivingLicence) || (is_array($drivingLicence) && empty($drivingLicence))) {
            unset($this->DrivingLicence);
        } else {
            $this->DrivingLicence = $drivingLicence;
        }
        
        return $this;
    }
}
