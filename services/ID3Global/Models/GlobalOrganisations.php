<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalOrganisations Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q564:GlobalOrganisations
 * @subpackage Structs
 */
class GlobalOrganisations extends AbstractStructBase
{
    /**
     * The Organisations
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalOrganisation|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalOrganisation $Organisations = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The TotalPages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPages = null;
    /**
     * The TotalOrganisations
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalOrganisations = null;
    /**
     * The TotalActive
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalActive = null;
    /**
     * The TotalExpired
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalExpired = null;
    /**
     * Constructor method for GlobalOrganisations
     * @uses GlobalOrganisations::setOrganisations()
     * @uses GlobalOrganisations::setPageSize()
     * @uses GlobalOrganisations::setTotalPages()
     * @uses GlobalOrganisations::setTotalOrganisations()
     * @uses GlobalOrganisations::setTotalActive()
     * @uses GlobalOrganisations::setTotalExpired()
     * @param \ID3Global\Arrays\ArrayOfGlobalOrganisation $organisations
     * @param int $pageSize
     * @param int $totalPages
     * @param int $totalOrganisations
     * @param int $totalActive
     * @param int $totalExpired
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalOrganisation $organisations = null, ?int $pageSize = null, ?int $totalPages = null, ?int $totalOrganisations = null, ?int $totalActive = null, ?int $totalExpired = null)
    {
        $this
            ->setOrganisations($organisations)
            ->setPageSize($pageSize)
            ->setTotalPages($totalPages)
            ->setTotalOrganisations($totalOrganisations)
            ->setTotalActive($totalActive)
            ->setTotalExpired($totalExpired);
    }
    /**
     * Get Organisations value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalOrganisation|null
     */
    public function getOrganisations(): ?\ID3Global\Arrays\ArrayOfGlobalOrganisation
    {
        return isset($this->Organisations) ? $this->Organisations : null;
    }
    /**
     * Set Organisations value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalOrganisation $organisations
     * @return \ID3Global\Models\GlobalOrganisations
     */
    public function setOrganisations(?\ID3Global\Arrays\ArrayOfGlobalOrganisation $organisations = null): self
    {
        if (is_null($organisations) || (is_array($organisations) && empty($organisations))) {
            unset($this->Organisations);
        } else {
            $this->Organisations = $organisations;
        }
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GlobalOrganisations
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get TotalPages value
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->TotalPages;
    }
    /**
     * Set TotalPages value
     * @param int $totalPages
     * @return \ID3Global\Models\GlobalOrganisations
     */
    public function setTotalPages(?int $totalPages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPages) && !(is_int($totalPages) || ctype_digit($totalPages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPages, true), gettype($totalPages)), __LINE__);
        }
        $this->TotalPages = $totalPages;
        
        return $this;
    }
    /**
     * Get TotalOrganisations value
     * @return int|null
     */
    public function getTotalOrganisations(): ?int
    {
        return $this->TotalOrganisations;
    }
    /**
     * Set TotalOrganisations value
     * @param int $totalOrganisations
     * @return \ID3Global\Models\GlobalOrganisations
     */
    public function setTotalOrganisations(?int $totalOrganisations = null): self
    {
        // validation for constraint: int
        if (!is_null($totalOrganisations) && !(is_int($totalOrganisations) || ctype_digit($totalOrganisations))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalOrganisations, true), gettype($totalOrganisations)), __LINE__);
        }
        $this->TotalOrganisations = $totalOrganisations;
        
        return $this;
    }
    /**
     * Get TotalActive value
     * @return int|null
     */
    public function getTotalActive(): ?int
    {
        return $this->TotalActive;
    }
    /**
     * Set TotalActive value
     * @param int $totalActive
     * @return \ID3Global\Models\GlobalOrganisations
     */
    public function setTotalActive(?int $totalActive = null): self
    {
        // validation for constraint: int
        if (!is_null($totalActive) && !(is_int($totalActive) || ctype_digit($totalActive))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalActive, true), gettype($totalActive)), __LINE__);
        }
        $this->TotalActive = $totalActive;
        
        return $this;
    }
    /**
     * Get TotalExpired value
     * @return int|null
     */
    public function getTotalExpired(): ?int
    {
        return $this->TotalExpired;
    }
    /**
     * Set TotalExpired value
     * @param int $totalExpired
     * @return \ID3Global\Models\GlobalOrganisations
     */
    public function setTotalExpired(?int $totalExpired = null): self
    {
        // validation for constraint: int
        if (!is_null($totalExpired) && !(is_int($totalExpired) || ctype_digit($totalExpired))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalExpired, true), gettype($totalExpired)), __LINE__);
        }
        $this->TotalExpired = $totalExpired;
        
        return $this;
    }
}
