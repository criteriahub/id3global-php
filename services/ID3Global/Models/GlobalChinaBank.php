<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalChinaBank Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q428:GlobalChinaBank
 * @subpackage Structs
 */
class GlobalChinaBank extends AbstractStructBase
{
    /**
     * The ChinaBankDetails
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalChinaBankDetails|null
     */
    protected ?\ID3Global\Models\GlobalChinaBankDetails $ChinaBankDetails = null;
    /**
     * Constructor method for GlobalChinaBank
     * @uses GlobalChinaBank::setChinaBankDetails()
     * @param \ID3Global\Models\GlobalChinaBankDetails $chinaBankDetails
     */
    public function __construct(?\ID3Global\Models\GlobalChinaBankDetails $chinaBankDetails = null)
    {
        $this
            ->setChinaBankDetails($chinaBankDetails);
    }
    /**
     * Get ChinaBankDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalChinaBankDetails|null
     */
    public function getChinaBankDetails(): ?\ID3Global\Models\GlobalChinaBankDetails
    {
        return isset($this->ChinaBankDetails) ? $this->ChinaBankDetails : null;
    }
    /**
     * Set ChinaBankDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalChinaBankDetails $chinaBankDetails
     * @return \ID3Global\Models\GlobalChinaBank
     */
    public function setChinaBankDetails(?\ID3Global\Models\GlobalChinaBankDetails $chinaBankDetails = null): self
    {
        if (is_null($chinaBankDetails) || (is_array($chinaBankDetails) && empty($chinaBankDetails))) {
            unset($this->ChinaBankDetails);
        } else {
            $this->ChinaBankDetails = $chinaBankDetails;
        }
        
        return $this;
    }
}
