<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAuthentications Models
 * @subpackage Structs
 */
class GetAuthentications extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The StartDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $StartDate = null;
    /**
     * The EndDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $EndDate = null;
    /**
     * The SearchType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $SearchType = null;
    /**
     * The SearchValue
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SearchValue = null;
    /**
     * The Page
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Page = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The SortType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $SortType = null;
    /**
     * The DescendingOrder
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $DescendingOrder = null;
    /**
     * Constructor method for GetAuthentications
     * @uses GetAuthentications::setOrgID()
     * @uses GetAuthentications::setStartDate()
     * @uses GetAuthentications::setEndDate()
     * @uses GetAuthentications::setSearchType()
     * @uses GetAuthentications::setSearchValue()
     * @uses GetAuthentications::setPage()
     * @uses GetAuthentications::setPageSize()
     * @uses GetAuthentications::setSortType()
     * @uses GetAuthentications::setDescendingOrder()
     * @param string $orgID
     * @param string $startDate
     * @param string $endDate
     * @param int $searchType
     * @param string $searchValue
     * @param int $page
     * @param int $pageSize
     * @param int $sortType
     * @param bool $descendingOrder
     */
    public function __construct(?string $orgID = null, ?string $startDate = null, ?string $endDate = null, ?int $searchType = null, ?string $searchValue = null, ?int $page = null, ?int $pageSize = null, ?int $sortType = null, ?bool $descendingOrder = null)
    {
        $this
            ->setOrgID($orgID)
            ->setStartDate($startDate)
            ->setEndDate($endDate)
            ->setSearchType($searchType)
            ->setSearchValue($searchValue)
            ->setPage($page)
            ->setPageSize($pageSize)
            ->setSortType($sortType)
            ->setDescendingOrder($descendingOrder);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetAuthentications
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get StartDate value
     * @return string|null
     */
    public function getStartDate(): ?string
    {
        return $this->StartDate;
    }
    /**
     * Set StartDate value
     * @param string $startDate
     * @return \ID3Global\Models\GetAuthentications
     */
    public function setStartDate(?string $startDate = null): self
    {
        // validation for constraint: string
        if (!is_null($startDate) && !is_string($startDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startDate, true), gettype($startDate)), __LINE__);
        }
        $this->StartDate = $startDate;
        
        return $this;
    }
    /**
     * Get EndDate value
     * @return string|null
     */
    public function getEndDate(): ?string
    {
        return $this->EndDate;
    }
    /**
     * Set EndDate value
     * @param string $endDate
     * @return \ID3Global\Models\GetAuthentications
     */
    public function setEndDate(?string $endDate = null): self
    {
        // validation for constraint: string
        if (!is_null($endDate) && !is_string($endDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endDate, true), gettype($endDate)), __LINE__);
        }
        $this->EndDate = $endDate;
        
        return $this;
    }
    /**
     * Get SearchType value
     * @return int|null
     */
    public function getSearchType(): ?int
    {
        return $this->SearchType;
    }
    /**
     * Set SearchType value
     * @param int $searchType
     * @return \ID3Global\Models\GetAuthentications
     */
    public function setSearchType(?int $searchType = null): self
    {
        // validation for constraint: int
        if (!is_null($searchType) && !(is_int($searchType) || ctype_digit($searchType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($searchType, true), gettype($searchType)), __LINE__);
        }
        $this->SearchType = $searchType;
        
        return $this;
    }
    /**
     * Get SearchValue value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSearchValue(): ?string
    {
        return isset($this->SearchValue) ? $this->SearchValue : null;
    }
    /**
     * Set SearchValue value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $searchValue
     * @return \ID3Global\Models\GetAuthentications
     */
    public function setSearchValue(?string $searchValue = null): self
    {
        // validation for constraint: string
        if (!is_null($searchValue) && !is_string($searchValue)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchValue, true), gettype($searchValue)), __LINE__);
        }
        if (is_null($searchValue) || (is_array($searchValue) && empty($searchValue))) {
            unset($this->SearchValue);
        } else {
            $this->SearchValue = $searchValue;
        }
        
        return $this;
    }
    /**
     * Get Page value
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->Page;
    }
    /**
     * Set Page value
     * @param int $page
     * @return \ID3Global\Models\GetAuthentications
     */
    public function setPage(?int $page = null): self
    {
        // validation for constraint: int
        if (!is_null($page) && !(is_int($page) || ctype_digit($page))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($page, true), gettype($page)), __LINE__);
        }
        $this->Page = $page;
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GetAuthentications
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get SortType value
     * @return int|null
     */
    public function getSortType(): ?int
    {
        return $this->SortType;
    }
    /**
     * Set SortType value
     * @param int $sortType
     * @return \ID3Global\Models\GetAuthentications
     */
    public function setSortType(?int $sortType = null): self
    {
        // validation for constraint: int
        if (!is_null($sortType) && !(is_int($sortType) || ctype_digit($sortType))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($sortType, true), gettype($sortType)), __LINE__);
        }
        $this->SortType = $sortType;
        
        return $this;
    }
    /**
     * Get DescendingOrder value
     * @return bool|null
     */
    public function getDescendingOrder(): ?bool
    {
        return $this->DescendingOrder;
    }
    /**
     * Set DescendingOrder value
     * @param bool $descendingOrder
     * @return \ID3Global\Models\GetAuthentications
     */
    public function setDescendingOrder(?bool $descendingOrder = null): self
    {
        // validation for constraint: boolean
        if (!is_null($descendingOrder) && !is_bool($descendingOrder)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($descendingOrder, true), gettype($descendingOrder)), __LINE__);
        }
        $this->DescendingOrder = $descendingOrder;
        
        return $this;
    }
}
