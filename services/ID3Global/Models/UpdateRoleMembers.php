<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateRoleMembers Models
 * @subpackage Structs
 */
class UpdateRoleMembers extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The RoleID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $RoleID = null;
    /**
     * The AddAccountIDs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfguid|null
     */
    protected ?\ID3Global\Arrays\ArrayOfguid $AddAccountIDs = null;
    /**
     * The RemoveAccountIDs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfguid|null
     */
    protected ?\ID3Global\Arrays\ArrayOfguid $RemoveAccountIDs = null;
    /**
     * Constructor method for UpdateRoleMembers
     * @uses UpdateRoleMembers::setOrgID()
     * @uses UpdateRoleMembers::setRoleID()
     * @uses UpdateRoleMembers::setAddAccountIDs()
     * @uses UpdateRoleMembers::setRemoveAccountIDs()
     * @param string $orgID
     * @param string $roleID
     * @param \ID3Global\Arrays\ArrayOfguid $addAccountIDs
     * @param \ID3Global\Arrays\ArrayOfguid $removeAccountIDs
     */
    public function __construct(?string $orgID = null, ?string $roleID = null, ?\ID3Global\Arrays\ArrayOfguid $addAccountIDs = null, ?\ID3Global\Arrays\ArrayOfguid $removeAccountIDs = null)
    {
        $this
            ->setOrgID($orgID)
            ->setRoleID($roleID)
            ->setAddAccountIDs($addAccountIDs)
            ->setRemoveAccountIDs($removeAccountIDs);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\UpdateRoleMembers
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get RoleID value
     * @return string|null
     */
    public function getRoleID(): ?string
    {
        return $this->RoleID;
    }
    /**
     * Set RoleID value
     * @param string $roleID
     * @return \ID3Global\Models\UpdateRoleMembers
     */
    public function setRoleID(?string $roleID = null): self
    {
        // validation for constraint: string
        if (!is_null($roleID) && !is_string($roleID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($roleID, true), gettype($roleID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($roleID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $roleID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($roleID, true)), __LINE__);
        }
        $this->RoleID = $roleID;
        
        return $this;
    }
    /**
     * Get AddAccountIDs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfguid|null
     */
    public function getAddAccountIDs(): ?\ID3Global\Arrays\ArrayOfguid
    {
        return isset($this->AddAccountIDs) ? $this->AddAccountIDs : null;
    }
    /**
     * Set AddAccountIDs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfguid $addAccountIDs
     * @return \ID3Global\Models\UpdateRoleMembers
     */
    public function setAddAccountIDs(?\ID3Global\Arrays\ArrayOfguid $addAccountIDs = null): self
    {
        if (is_null($addAccountIDs) || (is_array($addAccountIDs) && empty($addAccountIDs))) {
            unset($this->AddAccountIDs);
        } else {
            $this->AddAccountIDs = $addAccountIDs;
        }
        
        return $this;
    }
    /**
     * Get RemoveAccountIDs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfguid|null
     */
    public function getRemoveAccountIDs(): ?\ID3Global\Arrays\ArrayOfguid
    {
        return isset($this->RemoveAccountIDs) ? $this->RemoveAccountIDs : null;
    }
    /**
     * Set RemoveAccountIDs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfguid $removeAccountIDs
     * @return \ID3Global\Models\UpdateRoleMembers
     */
    public function setRemoveAccountIDs(?\ID3Global\Arrays\ArrayOfguid $removeAccountIDs = null): self
    {
        if (is_null($removeAccountIDs) || (is_array($removeAccountIDs) && empty($removeAccountIDs))) {
            unset($this->RemoveAccountIDs);
        } else {
            $this->RemoveAccountIDs = $removeAccountIDs;
        }
        
        return $this;
    }
}
