<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateRoleMembersResponse Models
 * @subpackage Structs
 */
class UpdateRoleMembersResponse extends AbstractStructBase
{
}
