<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalUS Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q380:GlobalUS
 * @subpackage Structs
 */
class GlobalUS extends AbstractStructBase
{
    /**
     * The DrivingLicense
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalUSDrivingLicense|null
     */
    protected ?\ID3Global\Models\GlobalUSDrivingLicense $DrivingLicense = null;
    /**
     * The SocialSecurity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalUSSocialSecurity|null
     */
    protected ?\ID3Global\Models\GlobalUSSocialSecurity $SocialSecurity = null;
    /**
     * Constructor method for GlobalUS
     * @uses GlobalUS::setDrivingLicense()
     * @uses GlobalUS::setSocialSecurity()
     * @param \ID3Global\Models\GlobalUSDrivingLicense $drivingLicense
     * @param \ID3Global\Models\GlobalUSSocialSecurity $socialSecurity
     */
    public function __construct(?\ID3Global\Models\GlobalUSDrivingLicense $drivingLicense = null, ?\ID3Global\Models\GlobalUSSocialSecurity $socialSecurity = null)
    {
        $this
            ->setDrivingLicense($drivingLicense)
            ->setSocialSecurity($socialSecurity);
    }
    /**
     * Get DrivingLicense value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalUSDrivingLicense|null
     */
    public function getDrivingLicense(): ?\ID3Global\Models\GlobalUSDrivingLicense
    {
        return isset($this->DrivingLicense) ? $this->DrivingLicense : null;
    }
    /**
     * Set DrivingLicense value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalUSDrivingLicense $drivingLicense
     * @return \ID3Global\Models\GlobalUS
     */
    public function setDrivingLicense(?\ID3Global\Models\GlobalUSDrivingLicense $drivingLicense = null): self
    {
        if (is_null($drivingLicense) || (is_array($drivingLicense) && empty($drivingLicense))) {
            unset($this->DrivingLicense);
        } else {
            $this->DrivingLicense = $drivingLicense;
        }
        
        return $this;
    }
    /**
     * Get SocialSecurity value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalUSSocialSecurity|null
     */
    public function getSocialSecurity(): ?\ID3Global\Models\GlobalUSSocialSecurity
    {
        return isset($this->SocialSecurity) ? $this->SocialSecurity : null;
    }
    /**
     * Set SocialSecurity value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalUSSocialSecurity $socialSecurity
     * @return \ID3Global\Models\GlobalUS
     */
    public function setSocialSecurity(?\ID3Global\Models\GlobalUSSocialSecurity $socialSecurity = null): self
    {
        if (is_null($socialSecurity) || (is_array($socialSecurity) && empty($socialSecurity))) {
            unset($this->SocialSecurity);
        } else {
            $this->SocialSecurity = $socialSecurity;
        }
        
        return $this;
    }
}
