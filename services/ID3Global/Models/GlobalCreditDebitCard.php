<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCreditDebitCard Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q426:GlobalCreditDebitCard
 * @subpackage Structs
 */
class GlobalCreditDebitCard extends AbstractStructBase
{
    /**
     * The CardHolderName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CardHolderName = null;
    /**
     * The CardNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CardNumber = null;
    /**
     * The ExpiryMonth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryMonth = null;
    /**
     * The ExpiryYear
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryYear = null;
    /**
     * The StartMonth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $StartMonth = null;
    /**
     * The StartYear
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $StartYear = null;
    /**
     * The CardIssueNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CardIssueNumber = null;
    /**
     * The CardType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CardType = null;
    /**
     * The CardVerificationCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CardVerificationCode = null;
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Country = null;
    /**
     * The State
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $State = null;
    /**
     * The MerchantData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MerchantData = null;
    /**
     * The ThreeDSecureResponse
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ThreeDSecureResponse = null;
    /**
     * Constructor method for GlobalCreditDebitCard
     * @uses GlobalCreditDebitCard::setCardHolderName()
     * @uses GlobalCreditDebitCard::setCardNumber()
     * @uses GlobalCreditDebitCard::setExpiryMonth()
     * @uses GlobalCreditDebitCard::setExpiryYear()
     * @uses GlobalCreditDebitCard::setStartMonth()
     * @uses GlobalCreditDebitCard::setStartYear()
     * @uses GlobalCreditDebitCard::setCardIssueNumber()
     * @uses GlobalCreditDebitCard::setCardType()
     * @uses GlobalCreditDebitCard::setCardVerificationCode()
     * @uses GlobalCreditDebitCard::setCountry()
     * @uses GlobalCreditDebitCard::setState()
     * @uses GlobalCreditDebitCard::setMerchantData()
     * @uses GlobalCreditDebitCard::setThreeDSecureResponse()
     * @param string $cardHolderName
     * @param string $cardNumber
     * @param int $expiryMonth
     * @param int $expiryYear
     * @param int $startMonth
     * @param int $startYear
     * @param string $cardIssueNumber
     * @param string $cardType
     * @param string $cardVerificationCode
     * @param string $country
     * @param string $state
     * @param string $merchantData
     * @param string $threeDSecureResponse
     */
    public function __construct(?string $cardHolderName = null, ?string $cardNumber = null, ?int $expiryMonth = null, ?int $expiryYear = null, ?int $startMonth = null, ?int $startYear = null, ?string $cardIssueNumber = null, ?string $cardType = null, ?string $cardVerificationCode = null, ?string $country = null, ?string $state = null, ?string $merchantData = null, ?string $threeDSecureResponse = null)
    {
        $this
            ->setCardHolderName($cardHolderName)
            ->setCardNumber($cardNumber)
            ->setExpiryMonth($expiryMonth)
            ->setExpiryYear($expiryYear)
            ->setStartMonth($startMonth)
            ->setStartYear($startYear)
            ->setCardIssueNumber($cardIssueNumber)
            ->setCardType($cardType)
            ->setCardVerificationCode($cardVerificationCode)
            ->setCountry($country)
            ->setState($state)
            ->setMerchantData($merchantData)
            ->setThreeDSecureResponse($threeDSecureResponse);
    }
    /**
     * Get CardHolderName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCardHolderName(): ?string
    {
        return isset($this->CardHolderName) ? $this->CardHolderName : null;
    }
    /**
     * Set CardHolderName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $cardHolderName
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setCardHolderName(?string $cardHolderName = null): self
    {
        // validation for constraint: string
        if (!is_null($cardHolderName) && !is_string($cardHolderName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cardHolderName, true), gettype($cardHolderName)), __LINE__);
        }
        if (is_null($cardHolderName) || (is_array($cardHolderName) && empty($cardHolderName))) {
            unset($this->CardHolderName);
        } else {
            $this->CardHolderName = $cardHolderName;
        }
        
        return $this;
    }
    /**
     * Get CardNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCardNumber(): ?string
    {
        return isset($this->CardNumber) ? $this->CardNumber : null;
    }
    /**
     * Set CardNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $cardNumber
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setCardNumber(?string $cardNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($cardNumber) && !is_string($cardNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cardNumber, true), gettype($cardNumber)), __LINE__);
        }
        if (is_null($cardNumber) || (is_array($cardNumber) && empty($cardNumber))) {
            unset($this->CardNumber);
        } else {
            $this->CardNumber = $cardNumber;
        }
        
        return $this;
    }
    /**
     * Get ExpiryMonth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryMonth(): ?int
    {
        return isset($this->ExpiryMonth) ? $this->ExpiryMonth : null;
    }
    /**
     * Set ExpiryMonth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryMonth
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setExpiryMonth(?int $expiryMonth = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryMonth) && !(is_int($expiryMonth) || ctype_digit($expiryMonth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryMonth, true), gettype($expiryMonth)), __LINE__);
        }
        if (is_null($expiryMonth) || (is_array($expiryMonth) && empty($expiryMonth))) {
            unset($this->ExpiryMonth);
        } else {
            $this->ExpiryMonth = $expiryMonth;
        }
        
        return $this;
    }
    /**
     * Get ExpiryYear value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryYear(): ?int
    {
        return isset($this->ExpiryYear) ? $this->ExpiryYear : null;
    }
    /**
     * Set ExpiryYear value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryYear
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setExpiryYear(?int $expiryYear = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryYear) && !(is_int($expiryYear) || ctype_digit($expiryYear))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryYear, true), gettype($expiryYear)), __LINE__);
        }
        if (is_null($expiryYear) || (is_array($expiryYear) && empty($expiryYear))) {
            unset($this->ExpiryYear);
        } else {
            $this->ExpiryYear = $expiryYear;
        }
        
        return $this;
    }
    /**
     * Get StartMonth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getStartMonth(): ?int
    {
        return isset($this->StartMonth) ? $this->StartMonth : null;
    }
    /**
     * Set StartMonth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $startMonth
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setStartMonth(?int $startMonth = null): self
    {
        // validation for constraint: int
        if (!is_null($startMonth) && !(is_int($startMonth) || ctype_digit($startMonth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($startMonth, true), gettype($startMonth)), __LINE__);
        }
        if (is_null($startMonth) || (is_array($startMonth) && empty($startMonth))) {
            unset($this->StartMonth);
        } else {
            $this->StartMonth = $startMonth;
        }
        
        return $this;
    }
    /**
     * Get StartYear value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getStartYear(): ?int
    {
        return isset($this->StartYear) ? $this->StartYear : null;
    }
    /**
     * Set StartYear value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $startYear
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setStartYear(?int $startYear = null): self
    {
        // validation for constraint: int
        if (!is_null($startYear) && !(is_int($startYear) || ctype_digit($startYear))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($startYear, true), gettype($startYear)), __LINE__);
        }
        if (is_null($startYear) || (is_array($startYear) && empty($startYear))) {
            unset($this->StartYear);
        } else {
            $this->StartYear = $startYear;
        }
        
        return $this;
    }
    /**
     * Get CardIssueNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCardIssueNumber(): ?string
    {
        return isset($this->CardIssueNumber) ? $this->CardIssueNumber : null;
    }
    /**
     * Set CardIssueNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $cardIssueNumber
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setCardIssueNumber(?string $cardIssueNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($cardIssueNumber) && !is_string($cardIssueNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cardIssueNumber, true), gettype($cardIssueNumber)), __LINE__);
        }
        if (is_null($cardIssueNumber) || (is_array($cardIssueNumber) && empty($cardIssueNumber))) {
            unset($this->CardIssueNumber);
        } else {
            $this->CardIssueNumber = $cardIssueNumber;
        }
        
        return $this;
    }
    /**
     * Get CardType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCardType(): ?string
    {
        return isset($this->CardType) ? $this->CardType : null;
    }
    /**
     * Set CardType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \ID3Global\Enums\GlobalCardType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalCardType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $cardType
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setCardType(?string $cardType = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalCardType::valueIsValid($cardType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalCardType', is_array($cardType) ? implode(', ', $cardType) : var_export($cardType, true), implode(', ', \ID3Global\Enums\GlobalCardType::getValidValues())), __LINE__);
        }
        if (is_null($cardType) || (is_array($cardType) && empty($cardType))) {
            unset($this->CardType);
        } else {
            $this->CardType = $cardType;
        }
        
        return $this;
    }
    /**
     * Get CardVerificationCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCardVerificationCode(): ?string
    {
        return isset($this->CardVerificationCode) ? $this->CardVerificationCode : null;
    }
    /**
     * Set CardVerificationCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $cardVerificationCode
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setCardVerificationCode(?string $cardVerificationCode = null): self
    {
        // validation for constraint: string
        if (!is_null($cardVerificationCode) && !is_string($cardVerificationCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cardVerificationCode, true), gettype($cardVerificationCode)), __LINE__);
        }
        if (is_null($cardVerificationCode) || (is_array($cardVerificationCode) && empty($cardVerificationCode))) {
            unset($this->CardVerificationCode);
        } else {
            $this->CardVerificationCode = $cardVerificationCode;
        }
        
        return $this;
    }
    /**
     * Get Country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return isset($this->Country) ? $this->Country : null;
    }
    /**
     * Set Country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $country
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->Country);
        } else {
            $this->Country = $country;
        }
        
        return $this;
    }
    /**
     * Get State value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getState(): ?string
    {
        return isset($this->State) ? $this->State : null;
    }
    /**
     * Set State value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $state
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setState(?string $state = null): self
    {
        // validation for constraint: string
        if (!is_null($state) && !is_string($state)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($state, true), gettype($state)), __LINE__);
        }
        if (is_null($state) || (is_array($state) && empty($state))) {
            unset($this->State);
        } else {
            $this->State = $state;
        }
        
        return $this;
    }
    /**
     * Get MerchantData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMerchantData(): ?string
    {
        return isset($this->MerchantData) ? $this->MerchantData : null;
    }
    /**
     * Set MerchantData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $merchantData
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setMerchantData(?string $merchantData = null): self
    {
        // validation for constraint: string
        if (!is_null($merchantData) && !is_string($merchantData)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($merchantData, true), gettype($merchantData)), __LINE__);
        }
        if (is_null($merchantData) || (is_array($merchantData) && empty($merchantData))) {
            unset($this->MerchantData);
        } else {
            $this->MerchantData = $merchantData;
        }
        
        return $this;
    }
    /**
     * Get ThreeDSecureResponse value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getThreeDSecureResponse(): ?string
    {
        return isset($this->ThreeDSecureResponse) ? $this->ThreeDSecureResponse : null;
    }
    /**
     * Set ThreeDSecureResponse value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $threeDSecureResponse
     * @return \ID3Global\Models\GlobalCreditDebitCard
     */
    public function setThreeDSecureResponse(?string $threeDSecureResponse = null): self
    {
        // validation for constraint: string
        if (!is_null($threeDSecureResponse) && !is_string($threeDSecureResponse)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($threeDSecureResponse, true), gettype($threeDSecureResponse)), __LINE__);
        }
        if (is_null($threeDSecureResponse) || (is_array($threeDSecureResponse) && empty($threeDSecureResponse))) {
            unset($this->ThreeDSecureResponse);
        } else {
            $this->ThreeDSecureResponse = $threeDSecureResponse;
        }
        
        return $this;
    }
}
