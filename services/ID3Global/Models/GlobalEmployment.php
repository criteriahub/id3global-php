<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalEmployment Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q416:GlobalEmployment
 * @subpackage Structs
 */
class GlobalEmployment extends AbstractStructBase
{
    /**
     * The ResidenceType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ResidenceType = null;
    /**
     * The EmploymentStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $EmploymentStatus = null;
    /**
     * The CurrentTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CurrentTime = null;
    /**
     * The Salary
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Salary = null;
    /**
     * Constructor method for GlobalEmployment
     * @uses GlobalEmployment::setResidenceType()
     * @uses GlobalEmployment::setEmploymentStatus()
     * @uses GlobalEmployment::setCurrentTime()
     * @uses GlobalEmployment::setSalary()
     * @param string $residenceType
     * @param string $employmentStatus
     * @param string $currentTime
     * @param string $salary
     */
    public function __construct(?string $residenceType = null, ?string $employmentStatus = null, ?string $currentTime = null, ?string $salary = null)
    {
        $this
            ->setResidenceType($residenceType)
            ->setEmploymentStatus($employmentStatus)
            ->setCurrentTime($currentTime)
            ->setSalary($salary);
    }
    /**
     * Get ResidenceType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getResidenceType(): ?string
    {
        return isset($this->ResidenceType) ? $this->ResidenceType : null;
    }
    /**
     * Set ResidenceType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \ID3Global\Enums\GlobalResidenceType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalResidenceType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $residenceType
     * @return \ID3Global\Models\GlobalEmployment
     */
    public function setResidenceType(?string $residenceType = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalResidenceType::valueIsValid($residenceType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalResidenceType', is_array($residenceType) ? implode(', ', $residenceType) : var_export($residenceType, true), implode(', ', \ID3Global\Enums\GlobalResidenceType::getValidValues())), __LINE__);
        }
        if (is_null($residenceType) || (is_array($residenceType) && empty($residenceType))) {
            unset($this->ResidenceType);
        } else {
            $this->ResidenceType = $residenceType;
        }
        
        return $this;
    }
    /**
     * Get EmploymentStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmploymentStatus(): ?string
    {
        return isset($this->EmploymentStatus) ? $this->EmploymentStatus : null;
    }
    /**
     * Set EmploymentStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \ID3Global\Enums\GlobalEmploymentStatus::valueIsValid()
     * @uses \ID3Global\Enums\GlobalEmploymentStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $employmentStatus
     * @return \ID3Global\Models\GlobalEmployment
     */
    public function setEmploymentStatus(?string $employmentStatus = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalEmploymentStatus::valueIsValid($employmentStatus)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalEmploymentStatus', is_array($employmentStatus) ? implode(', ', $employmentStatus) : var_export($employmentStatus, true), implode(', ', \ID3Global\Enums\GlobalEmploymentStatus::getValidValues())), __LINE__);
        }
        if (is_null($employmentStatus) || (is_array($employmentStatus) && empty($employmentStatus))) {
            unset($this->EmploymentStatus);
        } else {
            $this->EmploymentStatus = $employmentStatus;
        }
        
        return $this;
    }
    /**
     * Get CurrentTime value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCurrentTime(): ?string
    {
        return isset($this->CurrentTime) ? $this->CurrentTime : null;
    }
    /**
     * Set CurrentTime value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \ID3Global\Enums\GlobalCurrentTime::valueIsValid()
     * @uses \ID3Global\Enums\GlobalCurrentTime::getValidValues()
     * @throws InvalidArgumentException
     * @param string $currentTime
     * @return \ID3Global\Models\GlobalEmployment
     */
    public function setCurrentTime(?string $currentTime = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalCurrentTime::valueIsValid($currentTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalCurrentTime', is_array($currentTime) ? implode(', ', $currentTime) : var_export($currentTime, true), implode(', ', \ID3Global\Enums\GlobalCurrentTime::getValidValues())), __LINE__);
        }
        if (is_null($currentTime) || (is_array($currentTime) && empty($currentTime))) {
            unset($this->CurrentTime);
        } else {
            $this->CurrentTime = $currentTime;
        }
        
        return $this;
    }
    /**
     * Get Salary value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSalary(): ?string
    {
        return isset($this->Salary) ? $this->Salary : null;
    }
    /**
     * Set Salary value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $salary
     * @return \ID3Global\Models\GlobalEmployment
     */
    public function setSalary(?string $salary = null): self
    {
        // validation for constraint: string
        if (!is_null($salary) && !is_string($salary)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($salary, true), gettype($salary)), __LINE__);
        }
        if (is_null($salary) || (is_array($salary) && empty($salary))) {
            unset($this->Salary);
        } else {
            $this->Salary = $salary;
        }
        
        return $this;
    }
}
