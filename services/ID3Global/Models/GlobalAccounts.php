<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAccounts Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q581:GlobalAccounts
 * @subpackage Structs
 */
class GlobalAccounts extends AbstractStructBase
{
    /**
     * The Accounts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalAccount|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalAccount $Accounts = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The TotalPages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPages = null;
    /**
     * The TotalAccounts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalAccounts = null;
    /**
     * The TotalActive
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalActive = null;
    /**
     * The TotalExpired
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalExpired = null;
    /**
     * Constructor method for GlobalAccounts
     * @uses GlobalAccounts::setAccounts()
     * @uses GlobalAccounts::setPageSize()
     * @uses GlobalAccounts::setTotalPages()
     * @uses GlobalAccounts::setTotalAccounts()
     * @uses GlobalAccounts::setTotalActive()
     * @uses GlobalAccounts::setTotalExpired()
     * @param \ID3Global\Arrays\ArrayOfGlobalAccount $accounts
     * @param int $pageSize
     * @param int $totalPages
     * @param int $totalAccounts
     * @param int $totalActive
     * @param int $totalExpired
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalAccount $accounts = null, ?int $pageSize = null, ?int $totalPages = null, ?int $totalAccounts = null, ?int $totalActive = null, ?int $totalExpired = null)
    {
        $this
            ->setAccounts($accounts)
            ->setPageSize($pageSize)
            ->setTotalPages($totalPages)
            ->setTotalAccounts($totalAccounts)
            ->setTotalActive($totalActive)
            ->setTotalExpired($totalExpired);
    }
    /**
     * Get Accounts value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalAccount|null
     */
    public function getAccounts(): ?\ID3Global\Arrays\ArrayOfGlobalAccount
    {
        return isset($this->Accounts) ? $this->Accounts : null;
    }
    /**
     * Set Accounts value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalAccount $accounts
     * @return \ID3Global\Models\GlobalAccounts
     */
    public function setAccounts(?\ID3Global\Arrays\ArrayOfGlobalAccount $accounts = null): self
    {
        if (is_null($accounts) || (is_array($accounts) && empty($accounts))) {
            unset($this->Accounts);
        } else {
            $this->Accounts = $accounts;
        }
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GlobalAccounts
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get TotalPages value
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->TotalPages;
    }
    /**
     * Set TotalPages value
     * @param int $totalPages
     * @return \ID3Global\Models\GlobalAccounts
     */
    public function setTotalPages(?int $totalPages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPages) && !(is_int($totalPages) || ctype_digit($totalPages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPages, true), gettype($totalPages)), __LINE__);
        }
        $this->TotalPages = $totalPages;
        
        return $this;
    }
    /**
     * Get TotalAccounts value
     * @return int|null
     */
    public function getTotalAccounts(): ?int
    {
        return $this->TotalAccounts;
    }
    /**
     * Set TotalAccounts value
     * @param int $totalAccounts
     * @return \ID3Global\Models\GlobalAccounts
     */
    public function setTotalAccounts(?int $totalAccounts = null): self
    {
        // validation for constraint: int
        if (!is_null($totalAccounts) && !(is_int($totalAccounts) || ctype_digit($totalAccounts))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalAccounts, true), gettype($totalAccounts)), __LINE__);
        }
        $this->TotalAccounts = $totalAccounts;
        
        return $this;
    }
    /**
     * Get TotalActive value
     * @return int|null
     */
    public function getTotalActive(): ?int
    {
        return $this->TotalActive;
    }
    /**
     * Set TotalActive value
     * @param int $totalActive
     * @return \ID3Global\Models\GlobalAccounts
     */
    public function setTotalActive(?int $totalActive = null): self
    {
        // validation for constraint: int
        if (!is_null($totalActive) && !(is_int($totalActive) || ctype_digit($totalActive))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalActive, true), gettype($totalActive)), __LINE__);
        }
        $this->TotalActive = $totalActive;
        
        return $this;
    }
    /**
     * Get TotalExpired value
     * @return int|null
     */
    public function getTotalExpired(): ?int
    {
        return $this->TotalExpired;
    }
    /**
     * Set TotalExpired value
     * @param int $totalExpired
     * @return \ID3Global\Models\GlobalAccounts
     */
    public function setTotalExpired(?int $totalExpired = null): self
    {
        // validation for constraint: int
        if (!is_null($totalExpired) && !(is_int($totalExpired) || ctype_digit($totalExpired))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalExpired, true), gettype($totalExpired)), __LINE__);
        }
        $this->TotalExpired = $totalExpired;
        
        return $this;
    }
}
