<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseCustomerSettings Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q747:GlobalCaseCustomerSettings
 * @subpackage Structs
 */
class GlobalCaseCustomerSettings extends AbstractStructBase
{
    /**
     * The CorrespondanceEmail
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CorrespondanceEmail = null;
    /**
     * The ApplicantIsContact
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $ApplicantIsContact = null;
    /**
     * The ProviderIsVerifier
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $ProviderIsVerifier = null;
    /**
     * The References
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseCustomerReference|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseCustomerReference $References = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The IsNilReturnConfirmationRequired
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsNilReturnConfirmationRequired = null;
    /**
     * The SupportEmail
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SupportEmail = null;
    /**
     * The SupportTelephone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SupportTelephone = null;
    /**
     * The IsCustomerCreateNewChecksDisabled
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsCustomerCreateNewChecksDisabled = null;
    /**
     * Constructor method for GlobalCaseCustomerSettings
     * @uses GlobalCaseCustomerSettings::setCorrespondanceEmail()
     * @uses GlobalCaseCustomerSettings::setApplicantIsContact()
     * @uses GlobalCaseCustomerSettings::setProviderIsVerifier()
     * @uses GlobalCaseCustomerSettings::setReferences()
     * @uses GlobalCaseCustomerSettings::setName()
     * @uses GlobalCaseCustomerSettings::setIsNilReturnConfirmationRequired()
     * @uses GlobalCaseCustomerSettings::setSupportEmail()
     * @uses GlobalCaseCustomerSettings::setSupportTelephone()
     * @uses GlobalCaseCustomerSettings::setIsCustomerCreateNewChecksDisabled()
     * @param string $correspondanceEmail
     * @param bool $applicantIsContact
     * @param bool $providerIsVerifier
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseCustomerReference $references
     * @param string $name
     * @param bool $isNilReturnConfirmationRequired
     * @param string $supportEmail
     * @param string $supportTelephone
     * @param bool $isCustomerCreateNewChecksDisabled
     */
    public function __construct(?string $correspondanceEmail = null, ?bool $applicantIsContact = null, ?bool $providerIsVerifier = null, ?\ID3Global\Arrays\ArrayOfGlobalCaseCustomerReference $references = null, ?string $name = null, ?bool $isNilReturnConfirmationRequired = null, ?string $supportEmail = null, ?string $supportTelephone = null, ?bool $isCustomerCreateNewChecksDisabled = null)
    {
        $this
            ->setCorrespondanceEmail($correspondanceEmail)
            ->setApplicantIsContact($applicantIsContact)
            ->setProviderIsVerifier($providerIsVerifier)
            ->setReferences($references)
            ->setName($name)
            ->setIsNilReturnConfirmationRequired($isNilReturnConfirmationRequired)
            ->setSupportEmail($supportEmail)
            ->setSupportTelephone($supportTelephone)
            ->setIsCustomerCreateNewChecksDisabled($isCustomerCreateNewChecksDisabled);
    }
    /**
     * Get CorrespondanceEmail value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCorrespondanceEmail(): ?string
    {
        return isset($this->CorrespondanceEmail) ? $this->CorrespondanceEmail : null;
    }
    /**
     * Set CorrespondanceEmail value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $correspondanceEmail
     * @return \ID3Global\Models\GlobalCaseCustomerSettings
     */
    public function setCorrespondanceEmail(?string $correspondanceEmail = null): self
    {
        // validation for constraint: string
        if (!is_null($correspondanceEmail) && !is_string($correspondanceEmail)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($correspondanceEmail, true), gettype($correspondanceEmail)), __LINE__);
        }
        if (is_null($correspondanceEmail) || (is_array($correspondanceEmail) && empty($correspondanceEmail))) {
            unset($this->CorrespondanceEmail);
        } else {
            $this->CorrespondanceEmail = $correspondanceEmail;
        }
        
        return $this;
    }
    /**
     * Get ApplicantIsContact value
     * @return bool|null
     */
    public function getApplicantIsContact(): ?bool
    {
        return $this->ApplicantIsContact;
    }
    /**
     * Set ApplicantIsContact value
     * @param bool $applicantIsContact
     * @return \ID3Global\Models\GlobalCaseCustomerSettings
     */
    public function setApplicantIsContact(?bool $applicantIsContact = null): self
    {
        // validation for constraint: boolean
        if (!is_null($applicantIsContact) && !is_bool($applicantIsContact)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($applicantIsContact, true), gettype($applicantIsContact)), __LINE__);
        }
        $this->ApplicantIsContact = $applicantIsContact;
        
        return $this;
    }
    /**
     * Get ProviderIsVerifier value
     * @return bool|null
     */
    public function getProviderIsVerifier(): ?bool
    {
        return $this->ProviderIsVerifier;
    }
    /**
     * Set ProviderIsVerifier value
     * @param bool $providerIsVerifier
     * @return \ID3Global\Models\GlobalCaseCustomerSettings
     */
    public function setProviderIsVerifier(?bool $providerIsVerifier = null): self
    {
        // validation for constraint: boolean
        if (!is_null($providerIsVerifier) && !is_bool($providerIsVerifier)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($providerIsVerifier, true), gettype($providerIsVerifier)), __LINE__);
        }
        $this->ProviderIsVerifier = $providerIsVerifier;
        
        return $this;
    }
    /**
     * Get References value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseCustomerReference|null
     */
    public function getReferences(): ?\ID3Global\Arrays\ArrayOfGlobalCaseCustomerReference
    {
        return isset($this->References) ? $this->References : null;
    }
    /**
     * Set References value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseCustomerReference $references
     * @return \ID3Global\Models\GlobalCaseCustomerSettings
     */
    public function setReferences(?\ID3Global\Arrays\ArrayOfGlobalCaseCustomerReference $references = null): self
    {
        if (is_null($references) || (is_array($references) && empty($references))) {
            unset($this->References);
        } else {
            $this->References = $references;
        }
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalCaseCustomerSettings
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get IsNilReturnConfirmationRequired value
     * @return bool|null
     */
    public function getIsNilReturnConfirmationRequired(): ?bool
    {
        return $this->IsNilReturnConfirmationRequired;
    }
    /**
     * Set IsNilReturnConfirmationRequired value
     * @param bool $isNilReturnConfirmationRequired
     * @return \ID3Global\Models\GlobalCaseCustomerSettings
     */
    public function setIsNilReturnConfirmationRequired(?bool $isNilReturnConfirmationRequired = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isNilReturnConfirmationRequired) && !is_bool($isNilReturnConfirmationRequired)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isNilReturnConfirmationRequired, true), gettype($isNilReturnConfirmationRequired)), __LINE__);
        }
        $this->IsNilReturnConfirmationRequired = $isNilReturnConfirmationRequired;
        
        return $this;
    }
    /**
     * Get SupportEmail value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSupportEmail(): ?string
    {
        return isset($this->SupportEmail) ? $this->SupportEmail : null;
    }
    /**
     * Set SupportEmail value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $supportEmail
     * @return \ID3Global\Models\GlobalCaseCustomerSettings
     */
    public function setSupportEmail(?string $supportEmail = null): self
    {
        // validation for constraint: string
        if (!is_null($supportEmail) && !is_string($supportEmail)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($supportEmail, true), gettype($supportEmail)), __LINE__);
        }
        if (is_null($supportEmail) || (is_array($supportEmail) && empty($supportEmail))) {
            unset($this->SupportEmail);
        } else {
            $this->SupportEmail = $supportEmail;
        }
        
        return $this;
    }
    /**
     * Get SupportTelephone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSupportTelephone(): ?string
    {
        return isset($this->SupportTelephone) ? $this->SupportTelephone : null;
    }
    /**
     * Set SupportTelephone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $supportTelephone
     * @return \ID3Global\Models\GlobalCaseCustomerSettings
     */
    public function setSupportTelephone(?string $supportTelephone = null): self
    {
        // validation for constraint: string
        if (!is_null($supportTelephone) && !is_string($supportTelephone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($supportTelephone, true), gettype($supportTelephone)), __LINE__);
        }
        if (is_null($supportTelephone) || (is_array($supportTelephone) && empty($supportTelephone))) {
            unset($this->SupportTelephone);
        } else {
            $this->SupportTelephone = $supportTelephone;
        }
        
        return $this;
    }
    /**
     * Get IsCustomerCreateNewChecksDisabled value
     * @return bool|null
     */
    public function getIsCustomerCreateNewChecksDisabled(): ?bool
    {
        return $this->IsCustomerCreateNewChecksDisabled;
    }
    /**
     * Set IsCustomerCreateNewChecksDisabled value
     * @param bool $isCustomerCreateNewChecksDisabled
     * @return \ID3Global\Models\GlobalCaseCustomerSettings
     */
    public function setIsCustomerCreateNewChecksDisabled(?bool $isCustomerCreateNewChecksDisabled = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isCustomerCreateNewChecksDisabled) && !is_bool($isCustomerCreateNewChecksDisabled)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isCustomerCreateNewChecksDisabled, true), gettype($isCustomerCreateNewChecksDisabled)), __LINE__);
        }
        $this->IsCustomerCreateNewChecksDisabled = $isCustomerCreateNewChecksDisabled;
        
        return $this;
    }
}
