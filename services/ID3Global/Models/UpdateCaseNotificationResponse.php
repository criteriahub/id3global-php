<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateCaseNotificationResponse Models
 * @subpackage Structs
 */
class UpdateCaseNotificationResponse extends AbstractStructBase
{
}
