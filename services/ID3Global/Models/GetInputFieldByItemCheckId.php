<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetInputFieldByItemCheckId Models
 * @subpackage Structs
 */
class GetInputFieldByItemCheckId extends AbstractStructBase
{
    /**
     * The ItemCheckIDs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    protected ?\ID3Global\Arrays\ArrayOfunsignedInt $ItemCheckIDs = null;
    /**
     * Constructor method for GetInputFieldByItemCheckId
     * @uses GetInputFieldByItemCheckId::setItemCheckIDs()
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $itemCheckIDs
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfunsignedInt $itemCheckIDs = null)
    {
        $this
            ->setItemCheckIDs($itemCheckIDs);
    }
    /**
     * Get ItemCheckIDs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    public function getItemCheckIDs(): ?\ID3Global\Arrays\ArrayOfunsignedInt
    {
        return isset($this->ItemCheckIDs) ? $this->ItemCheckIDs : null;
    }
    /**
     * Set ItemCheckIDs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $itemCheckIDs
     * @return \ID3Global\Models\GetInputFieldByItemCheckId
     */
    public function setItemCheckIDs(?\ID3Global\Arrays\ArrayOfunsignedInt $itemCheckIDs = null): self
    {
        if (is_null($itemCheckIDs) || (is_array($itemCheckIDs) && empty($itemCheckIDs))) {
            unset($this->ItemCheckIDs);
        } else {
            $this->ItemCheckIDs = $itemCheckIDs;
        }
        
        return $this;
    }
}
