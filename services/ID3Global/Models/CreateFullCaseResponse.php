<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateFullCaseResponse Models
 * @subpackage Structs
 */
class CreateFullCaseResponse extends AbstractStructBase
{
    /**
     * The CreateFullCaseResult
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $CreateFullCaseResult = null;
    /**
     * Constructor method for CreateFullCaseResponse
     * @uses CreateFullCaseResponse::setCreateFullCaseResult()
     * @param string $createFullCaseResult
     */
    public function __construct(?string $createFullCaseResult = null)
    {
        $this
            ->setCreateFullCaseResult($createFullCaseResult);
    }
    /**
     * Get CreateFullCaseResult value
     * @return string|null
     */
    public function getCreateFullCaseResult(): ?string
    {
        return $this->CreateFullCaseResult;
    }
    /**
     * Set CreateFullCaseResult value
     * @param string $createFullCaseResult
     * @return \ID3Global\Models\CreateFullCaseResponse
     */
    public function setCreateFullCaseResult(?string $createFullCaseResult = null): self
    {
        // validation for constraint: string
        if (!is_null($createFullCaseResult) && !is_string($createFullCaseResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($createFullCaseResult, true), gettype($createFullCaseResult)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($createFullCaseResult) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $createFullCaseResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($createFullCaseResult, true)), __LINE__);
        }
        $this->CreateFullCaseResult = $createFullCaseResult;
        
        return $this;
    }
}
