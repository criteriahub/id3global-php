<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalNetherlandsAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q693:GlobalNetherlandsAccount
 * @subpackage Structs
 */
class GlobalNetherlandsAccount extends GlobalSupplierAccount
{
    /**
     * The APIKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $APIKey = null;
    /**
     * Constructor method for GlobalNetherlandsAccount
     * @uses GlobalNetherlandsAccount::setAPIKey()
     * @param string $aPIKey
     */
    public function __construct(?string $aPIKey = null)
    {
        $this
            ->setAPIKey($aPIKey);
    }
    /**
     * Get APIKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAPIKey(): ?string
    {
        return isset($this->APIKey) ? $this->APIKey : null;
    }
    /**
     * Set APIKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $aPIKey
     * @return \ID3Global\Models\GlobalNetherlandsAccount
     */
    public function setAPIKey(?string $aPIKey = null): self
    {
        // validation for constraint: string
        if (!is_null($aPIKey) && !is_string($aPIKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($aPIKey, true), gettype($aPIKey)), __LINE__);
        }
        if (is_null($aPIKey) || (is_array($aPIKey) && empty($aPIKey))) {
            unset($this->APIKey);
        } else {
            $this->APIKey = $aPIKey;
        }
        
        return $this;
    }
}
