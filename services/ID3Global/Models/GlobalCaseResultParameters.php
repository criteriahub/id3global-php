<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseResultParameters Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q874:GlobalCaseResultParameters
 * @subpackage Structs
 */
class GlobalCaseResultParameters extends AbstractStructBase
{
    /**
     * The ProfileID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ProfileID = null;
    /**
     * The Source
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Source = null;
    /**
     * The CompleteStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $CompleteStatus = null;
    /**
     * The PendgingStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $PendgingStatus = null;
    /**
     * The ChargingPointProfileIDs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfguid|null
     */
    protected ?\ID3Global\Arrays\ArrayOfguid $ChargingPointProfileIDs = null;
    /**
     * The IsUpdate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsUpdate = null;
    /**
     * The SuccessStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $SuccessStatus = null;
    /**
     * Constructor method for GlobalCaseResultParameters
     * @uses GlobalCaseResultParameters::setProfileID()
     * @uses GlobalCaseResultParameters::setSource()
     * @uses GlobalCaseResultParameters::setCompleteStatus()
     * @uses GlobalCaseResultParameters::setPendgingStatus()
     * @uses GlobalCaseResultParameters::setChargingPointProfileIDs()
     * @uses GlobalCaseResultParameters::setIsUpdate()
     * @uses GlobalCaseResultParameters::setSuccessStatus()
     * @param string $profileID
     * @param string $source
     * @param int $completeStatus
     * @param int $pendgingStatus
     * @param \ID3Global\Arrays\ArrayOfguid $chargingPointProfileIDs
     * @param bool $isUpdate
     * @param int $successStatus
     */
    public function __construct(?string $profileID = null, ?string $source = null, ?int $completeStatus = null, ?int $pendgingStatus = null, ?\ID3Global\Arrays\ArrayOfguid $chargingPointProfileIDs = null, ?bool $isUpdate = null, ?int $successStatus = null)
    {
        $this
            ->setProfileID($profileID)
            ->setSource($source)
            ->setCompleteStatus($completeStatus)
            ->setPendgingStatus($pendgingStatus)
            ->setChargingPointProfileIDs($chargingPointProfileIDs)
            ->setIsUpdate($isUpdate)
            ->setSuccessStatus($successStatus);
    }
    /**
     * Get ProfileID value
     * @return string|null
     */
    public function getProfileID(): ?string
    {
        return $this->ProfileID;
    }
    /**
     * Set ProfileID value
     * @param string $profileID
     * @return \ID3Global\Models\GlobalCaseResultParameters
     */
    public function setProfileID(?string $profileID = null): self
    {
        // validation for constraint: string
        if (!is_null($profileID) && !is_string($profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($profileID, true), gettype($profileID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($profileID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($profileID, true)), __LINE__);
        }
        $this->ProfileID = $profileID;
        
        return $this;
    }
    /**
     * Get Source value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSource(): ?string
    {
        return isset($this->Source) ? $this->Source : null;
    }
    /**
     * Set Source value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $source
     * @return \ID3Global\Models\GlobalCaseResultParameters
     */
    public function setSource(?string $source = null): self
    {
        // validation for constraint: string
        if (!is_null($source) && !is_string($source)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($source, true), gettype($source)), __LINE__);
        }
        if (is_null($source) || (is_array($source) && empty($source))) {
            unset($this->Source);
        } else {
            $this->Source = $source;
        }
        
        return $this;
    }
    /**
     * Get CompleteStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getCompleteStatus(): ?int
    {
        return isset($this->CompleteStatus) ? $this->CompleteStatus : null;
    }
    /**
     * Set CompleteStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $completeStatus
     * @return \ID3Global\Models\GlobalCaseResultParameters
     */
    public function setCompleteStatus(?int $completeStatus = null): self
    {
        // validation for constraint: int
        if (!is_null($completeStatus) && !(is_int($completeStatus) || ctype_digit($completeStatus))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($completeStatus, true), gettype($completeStatus)), __LINE__);
        }
        if (is_null($completeStatus) || (is_array($completeStatus) && empty($completeStatus))) {
            unset($this->CompleteStatus);
        } else {
            $this->CompleteStatus = $completeStatus;
        }
        
        return $this;
    }
    /**
     * Get PendgingStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPendgingStatus(): ?int
    {
        return isset($this->PendgingStatus) ? $this->PendgingStatus : null;
    }
    /**
     * Set PendgingStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $pendgingStatus
     * @return \ID3Global\Models\GlobalCaseResultParameters
     */
    public function setPendgingStatus(?int $pendgingStatus = null): self
    {
        // validation for constraint: int
        if (!is_null($pendgingStatus) && !(is_int($pendgingStatus) || ctype_digit($pendgingStatus))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pendgingStatus, true), gettype($pendgingStatus)), __LINE__);
        }
        if (is_null($pendgingStatus) || (is_array($pendgingStatus) && empty($pendgingStatus))) {
            unset($this->PendgingStatus);
        } else {
            $this->PendgingStatus = $pendgingStatus;
        }
        
        return $this;
    }
    /**
     * Get ChargingPointProfileIDs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfguid|null
     */
    public function getChargingPointProfileIDs(): ?\ID3Global\Arrays\ArrayOfguid
    {
        return isset($this->ChargingPointProfileIDs) ? $this->ChargingPointProfileIDs : null;
    }
    /**
     * Set ChargingPointProfileIDs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfguid $chargingPointProfileIDs
     * @return \ID3Global\Models\GlobalCaseResultParameters
     */
    public function setChargingPointProfileIDs(?\ID3Global\Arrays\ArrayOfguid $chargingPointProfileIDs = null): self
    {
        if (is_null($chargingPointProfileIDs) || (is_array($chargingPointProfileIDs) && empty($chargingPointProfileIDs))) {
            unset($this->ChargingPointProfileIDs);
        } else {
            $this->ChargingPointProfileIDs = $chargingPointProfileIDs;
        }
        
        return $this;
    }
    /**
     * Get IsUpdate value
     * @return bool|null
     */
    public function getIsUpdate(): ?bool
    {
        return $this->IsUpdate;
    }
    /**
     * Set IsUpdate value
     * @param bool $isUpdate
     * @return \ID3Global\Models\GlobalCaseResultParameters
     */
    public function setIsUpdate(?bool $isUpdate = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isUpdate) && !is_bool($isUpdate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isUpdate, true), gettype($isUpdate)), __LINE__);
        }
        $this->IsUpdate = $isUpdate;
        
        return $this;
    }
    /**
     * Get SuccessStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getSuccessStatus(): ?int
    {
        return isset($this->SuccessStatus) ? $this->SuccessStatus : null;
    }
    /**
     * Set SuccessStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $successStatus
     * @return \ID3Global\Models\GlobalCaseResultParameters
     */
    public function setSuccessStatus(?int $successStatus = null): self
    {
        // validation for constraint: int
        if (!is_null($successStatus) && !(is_int($successStatus) || ctype_digit($successStatus))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($successStatus, true), gettype($successStatus)), __LINE__);
        }
        if (is_null($successStatus) || (is_array($successStatus) && empty($successStatus))) {
            unset($this->SuccessStatus);
        } else {
            $this->SuccessStatus = $successStatus;
        }
        
        return $this;
    }
}
