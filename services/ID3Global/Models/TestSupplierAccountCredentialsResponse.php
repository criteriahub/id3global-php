<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TestSupplierAccountCredentialsResponse Models
 * @subpackage Structs
 */
class TestSupplierAccountCredentialsResponse extends AbstractStructBase
{
    /**
     * The TestSupplierAccountCredentialsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierAccount|null
     */
    protected ?\ID3Global\Models\GlobalSupplierAccount $TestSupplierAccountCredentialsResult = null;
    /**
     * Constructor method for TestSupplierAccountCredentialsResponse
     * @uses TestSupplierAccountCredentialsResponse::setTestSupplierAccountCredentialsResult()
     * @param \ID3Global\Models\GlobalSupplierAccount $testSupplierAccountCredentialsResult
     */
    public function __construct(?\ID3Global\Models\GlobalSupplierAccount $testSupplierAccountCredentialsResult = null)
    {
        $this
            ->setTestSupplierAccountCredentialsResult($testSupplierAccountCredentialsResult);
    }
    /**
     * Get TestSupplierAccountCredentialsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierAccount|null
     */
    public function getTestSupplierAccountCredentialsResult(): ?\ID3Global\Models\GlobalSupplierAccount
    {
        return isset($this->TestSupplierAccountCredentialsResult) ? $this->TestSupplierAccountCredentialsResult : null;
    }
    /**
     * Set TestSupplierAccountCredentialsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupplierAccount $testSupplierAccountCredentialsResult
     * @return \ID3Global\Models\TestSupplierAccountCredentialsResponse
     */
    public function setTestSupplierAccountCredentialsResult(?\ID3Global\Models\GlobalSupplierAccount $testSupplierAccountCredentialsResult = null): self
    {
        if (is_null($testSupplierAccountCredentialsResult) || (is_array($testSupplierAccountCredentialsResult) && empty($testSupplierAccountCredentialsResult))) {
            unset($this->TestSupplierAccountCredentialsResult);
        } else {
            $this->TestSupplierAccountCredentialsResult = $testSupplierAccountCredentialsResult;
        }
        
        return $this;
    }
}
