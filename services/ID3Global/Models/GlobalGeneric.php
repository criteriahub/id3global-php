<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalGeneric Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q430:GlobalGeneric
 * @subpackage Structs
 */
class GlobalGeneric extends AbstractStructBase
{
    /**
     * The Generic1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic1 = null;
    /**
     * The Generic2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic2 = null;
    /**
     * The Generic3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic3 = null;
    /**
     * The Generic4
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic4 = null;
    /**
     * The Generic5
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic5 = null;
    /**
     * The Generic6
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic6 = null;
    /**
     * The Generic7
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic7 = null;
    /**
     * The Generic8
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic8 = null;
    /**
     * The Generic9
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic9 = null;
    /**
     * The Generic10
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic10 = null;
    /**
     * The Generic11
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic11 = null;
    /**
     * The Generic12
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic12 = null;
    /**
     * The Generic13
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic13 = null;
    /**
     * The Generic14
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic14 = null;
    /**
     * The Generic15
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic15 = null;
    /**
     * The Generic16
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic16 = null;
    /**
     * The Generic17
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic17 = null;
    /**
     * The Generic18
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic18 = null;
    /**
     * The Generic19
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic19 = null;
    /**
     * The Generic20
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Generic20 = null;
    /**
     * Constructor method for GlobalGeneric
     * @uses GlobalGeneric::setGeneric1()
     * @uses GlobalGeneric::setGeneric2()
     * @uses GlobalGeneric::setGeneric3()
     * @uses GlobalGeneric::setGeneric4()
     * @uses GlobalGeneric::setGeneric5()
     * @uses GlobalGeneric::setGeneric6()
     * @uses GlobalGeneric::setGeneric7()
     * @uses GlobalGeneric::setGeneric8()
     * @uses GlobalGeneric::setGeneric9()
     * @uses GlobalGeneric::setGeneric10()
     * @uses GlobalGeneric::setGeneric11()
     * @uses GlobalGeneric::setGeneric12()
     * @uses GlobalGeneric::setGeneric13()
     * @uses GlobalGeneric::setGeneric14()
     * @uses GlobalGeneric::setGeneric15()
     * @uses GlobalGeneric::setGeneric16()
     * @uses GlobalGeneric::setGeneric17()
     * @uses GlobalGeneric::setGeneric18()
     * @uses GlobalGeneric::setGeneric19()
     * @uses GlobalGeneric::setGeneric20()
     * @param string $generic1
     * @param string $generic2
     * @param string $generic3
     * @param string $generic4
     * @param string $generic5
     * @param string $generic6
     * @param string $generic7
     * @param string $generic8
     * @param string $generic9
     * @param string $generic10
     * @param string $generic11
     * @param string $generic12
     * @param string $generic13
     * @param string $generic14
     * @param string $generic15
     * @param string $generic16
     * @param string $generic17
     * @param string $generic18
     * @param string $generic19
     * @param string $generic20
     */
    public function __construct(?string $generic1 = null, ?string $generic2 = null, ?string $generic3 = null, ?string $generic4 = null, ?string $generic5 = null, ?string $generic6 = null, ?string $generic7 = null, ?string $generic8 = null, ?string $generic9 = null, ?string $generic10 = null, ?string $generic11 = null, ?string $generic12 = null, ?string $generic13 = null, ?string $generic14 = null, ?string $generic15 = null, ?string $generic16 = null, ?string $generic17 = null, ?string $generic18 = null, ?string $generic19 = null, ?string $generic20 = null)
    {
        $this
            ->setGeneric1($generic1)
            ->setGeneric2($generic2)
            ->setGeneric3($generic3)
            ->setGeneric4($generic4)
            ->setGeneric5($generic5)
            ->setGeneric6($generic6)
            ->setGeneric7($generic7)
            ->setGeneric8($generic8)
            ->setGeneric9($generic9)
            ->setGeneric10($generic10)
            ->setGeneric11($generic11)
            ->setGeneric12($generic12)
            ->setGeneric13($generic13)
            ->setGeneric14($generic14)
            ->setGeneric15($generic15)
            ->setGeneric16($generic16)
            ->setGeneric17($generic17)
            ->setGeneric18($generic18)
            ->setGeneric19($generic19)
            ->setGeneric20($generic20);
    }
    /**
     * Get Generic1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric1(): ?string
    {
        return isset($this->Generic1) ? $this->Generic1 : null;
    }
    /**
     * Set Generic1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic1
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric1(?string $generic1 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic1) && !is_string($generic1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic1, true), gettype($generic1)), __LINE__);
        }
        if (is_null($generic1) || (is_array($generic1) && empty($generic1))) {
            unset($this->Generic1);
        } else {
            $this->Generic1 = $generic1;
        }
        
        return $this;
    }
    /**
     * Get Generic2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric2(): ?string
    {
        return isset($this->Generic2) ? $this->Generic2 : null;
    }
    /**
     * Set Generic2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic2
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric2(?string $generic2 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic2) && !is_string($generic2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic2, true), gettype($generic2)), __LINE__);
        }
        if (is_null($generic2) || (is_array($generic2) && empty($generic2))) {
            unset($this->Generic2);
        } else {
            $this->Generic2 = $generic2;
        }
        
        return $this;
    }
    /**
     * Get Generic3 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric3(): ?string
    {
        return isset($this->Generic3) ? $this->Generic3 : null;
    }
    /**
     * Set Generic3 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic3
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric3(?string $generic3 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic3) && !is_string($generic3)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic3, true), gettype($generic3)), __LINE__);
        }
        if (is_null($generic3) || (is_array($generic3) && empty($generic3))) {
            unset($this->Generic3);
        } else {
            $this->Generic3 = $generic3;
        }
        
        return $this;
    }
    /**
     * Get Generic4 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric4(): ?string
    {
        return isset($this->Generic4) ? $this->Generic4 : null;
    }
    /**
     * Set Generic4 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic4
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric4(?string $generic4 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic4) && !is_string($generic4)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic4, true), gettype($generic4)), __LINE__);
        }
        if (is_null($generic4) || (is_array($generic4) && empty($generic4))) {
            unset($this->Generic4);
        } else {
            $this->Generic4 = $generic4;
        }
        
        return $this;
    }
    /**
     * Get Generic5 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric5(): ?string
    {
        return isset($this->Generic5) ? $this->Generic5 : null;
    }
    /**
     * Set Generic5 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic5
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric5(?string $generic5 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic5) && !is_string($generic5)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic5, true), gettype($generic5)), __LINE__);
        }
        if (is_null($generic5) || (is_array($generic5) && empty($generic5))) {
            unset($this->Generic5);
        } else {
            $this->Generic5 = $generic5;
        }
        
        return $this;
    }
    /**
     * Get Generic6 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric6(): ?string
    {
        return isset($this->Generic6) ? $this->Generic6 : null;
    }
    /**
     * Set Generic6 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic6
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric6(?string $generic6 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic6) && !is_string($generic6)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic6, true), gettype($generic6)), __LINE__);
        }
        if (is_null($generic6) || (is_array($generic6) && empty($generic6))) {
            unset($this->Generic6);
        } else {
            $this->Generic6 = $generic6;
        }
        
        return $this;
    }
    /**
     * Get Generic7 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric7(): ?string
    {
        return isset($this->Generic7) ? $this->Generic7 : null;
    }
    /**
     * Set Generic7 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic7
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric7(?string $generic7 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic7) && !is_string($generic7)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic7, true), gettype($generic7)), __LINE__);
        }
        if (is_null($generic7) || (is_array($generic7) && empty($generic7))) {
            unset($this->Generic7);
        } else {
            $this->Generic7 = $generic7;
        }
        
        return $this;
    }
    /**
     * Get Generic8 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric8(): ?string
    {
        return isset($this->Generic8) ? $this->Generic8 : null;
    }
    /**
     * Set Generic8 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic8
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric8(?string $generic8 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic8) && !is_string($generic8)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic8, true), gettype($generic8)), __LINE__);
        }
        if (is_null($generic8) || (is_array($generic8) && empty($generic8))) {
            unset($this->Generic8);
        } else {
            $this->Generic8 = $generic8;
        }
        
        return $this;
    }
    /**
     * Get Generic9 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric9(): ?string
    {
        return isset($this->Generic9) ? $this->Generic9 : null;
    }
    /**
     * Set Generic9 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic9
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric9(?string $generic9 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic9) && !is_string($generic9)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic9, true), gettype($generic9)), __LINE__);
        }
        if (is_null($generic9) || (is_array($generic9) && empty($generic9))) {
            unset($this->Generic9);
        } else {
            $this->Generic9 = $generic9;
        }
        
        return $this;
    }
    /**
     * Get Generic10 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric10(): ?string
    {
        return isset($this->Generic10) ? $this->Generic10 : null;
    }
    /**
     * Set Generic10 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic10
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric10(?string $generic10 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic10) && !is_string($generic10)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic10, true), gettype($generic10)), __LINE__);
        }
        if (is_null($generic10) || (is_array($generic10) && empty($generic10))) {
            unset($this->Generic10);
        } else {
            $this->Generic10 = $generic10;
        }
        
        return $this;
    }
    /**
     * Get Generic11 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric11(): ?string
    {
        return isset($this->Generic11) ? $this->Generic11 : null;
    }
    /**
     * Set Generic11 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic11
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric11(?string $generic11 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic11) && !is_string($generic11)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic11, true), gettype($generic11)), __LINE__);
        }
        if (is_null($generic11) || (is_array($generic11) && empty($generic11))) {
            unset($this->Generic11);
        } else {
            $this->Generic11 = $generic11;
        }
        
        return $this;
    }
    /**
     * Get Generic12 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric12(): ?string
    {
        return isset($this->Generic12) ? $this->Generic12 : null;
    }
    /**
     * Set Generic12 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic12
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric12(?string $generic12 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic12) && !is_string($generic12)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic12, true), gettype($generic12)), __LINE__);
        }
        if (is_null($generic12) || (is_array($generic12) && empty($generic12))) {
            unset($this->Generic12);
        } else {
            $this->Generic12 = $generic12;
        }
        
        return $this;
    }
    /**
     * Get Generic13 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric13(): ?string
    {
        return isset($this->Generic13) ? $this->Generic13 : null;
    }
    /**
     * Set Generic13 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic13
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric13(?string $generic13 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic13) && !is_string($generic13)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic13, true), gettype($generic13)), __LINE__);
        }
        if (is_null($generic13) || (is_array($generic13) && empty($generic13))) {
            unset($this->Generic13);
        } else {
            $this->Generic13 = $generic13;
        }
        
        return $this;
    }
    /**
     * Get Generic14 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric14(): ?string
    {
        return isset($this->Generic14) ? $this->Generic14 : null;
    }
    /**
     * Set Generic14 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic14
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric14(?string $generic14 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic14) && !is_string($generic14)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic14, true), gettype($generic14)), __LINE__);
        }
        if (is_null($generic14) || (is_array($generic14) && empty($generic14))) {
            unset($this->Generic14);
        } else {
            $this->Generic14 = $generic14;
        }
        
        return $this;
    }
    /**
     * Get Generic15 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric15(): ?string
    {
        return isset($this->Generic15) ? $this->Generic15 : null;
    }
    /**
     * Set Generic15 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic15
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric15(?string $generic15 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic15) && !is_string($generic15)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic15, true), gettype($generic15)), __LINE__);
        }
        if (is_null($generic15) || (is_array($generic15) && empty($generic15))) {
            unset($this->Generic15);
        } else {
            $this->Generic15 = $generic15;
        }
        
        return $this;
    }
    /**
     * Get Generic16 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric16(): ?string
    {
        return isset($this->Generic16) ? $this->Generic16 : null;
    }
    /**
     * Set Generic16 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic16
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric16(?string $generic16 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic16) && !is_string($generic16)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic16, true), gettype($generic16)), __LINE__);
        }
        if (is_null($generic16) || (is_array($generic16) && empty($generic16))) {
            unset($this->Generic16);
        } else {
            $this->Generic16 = $generic16;
        }
        
        return $this;
    }
    /**
     * Get Generic17 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric17(): ?string
    {
        return isset($this->Generic17) ? $this->Generic17 : null;
    }
    /**
     * Set Generic17 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic17
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric17(?string $generic17 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic17) && !is_string($generic17)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic17, true), gettype($generic17)), __LINE__);
        }
        if (is_null($generic17) || (is_array($generic17) && empty($generic17))) {
            unset($this->Generic17);
        } else {
            $this->Generic17 = $generic17;
        }
        
        return $this;
    }
    /**
     * Get Generic18 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric18(): ?string
    {
        return isset($this->Generic18) ? $this->Generic18 : null;
    }
    /**
     * Set Generic18 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic18
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric18(?string $generic18 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic18) && !is_string($generic18)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic18, true), gettype($generic18)), __LINE__);
        }
        if (is_null($generic18) || (is_array($generic18) && empty($generic18))) {
            unset($this->Generic18);
        } else {
            $this->Generic18 = $generic18;
        }
        
        return $this;
    }
    /**
     * Get Generic19 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric19(): ?string
    {
        return isset($this->Generic19) ? $this->Generic19 : null;
    }
    /**
     * Set Generic19 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic19
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric19(?string $generic19 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic19) && !is_string($generic19)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic19, true), gettype($generic19)), __LINE__);
        }
        if (is_null($generic19) || (is_array($generic19) && empty($generic19))) {
            unset($this->Generic19);
        } else {
            $this->Generic19 = $generic19;
        }
        
        return $this;
    }
    /**
     * Get Generic20 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGeneric20(): ?string
    {
        return isset($this->Generic20) ? $this->Generic20 : null;
    }
    /**
     * Set Generic20 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $generic20
     * @return \ID3Global\Models\GlobalGeneric
     */
    public function setGeneric20(?string $generic20 = null): self
    {
        // validation for constraint: string
        if (!is_null($generic20) && !is_string($generic20)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($generic20, true), gettype($generic20)), __LINE__);
        }
        if (is_null($generic20) || (is_array($generic20) && empty($generic20))) {
            unset($this->Generic20);
        } else {
            $this->Generic20 = $generic20;
        }
        
        return $this;
    }
}
