<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalMalaysiaAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q691:GlobalMalaysiaAccount
 * @subpackage Structs
 */
class GlobalMalaysiaAccount extends GlobalSupplierAccount
{
    /**
     * The MemberID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MemberID = null;
    /**
     * The UserID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $UserID = null;
    /**
     * Constructor method for GlobalMalaysiaAccount
     * @uses GlobalMalaysiaAccount::setMemberID()
     * @uses GlobalMalaysiaAccount::setUserID()
     * @param string $memberID
     * @param string $userID
     */
    public function __construct(?string $memberID = null, ?string $userID = null)
    {
        $this
            ->setMemberID($memberID)
            ->setUserID($userID);
    }
    /**
     * Get MemberID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMemberID(): ?string
    {
        return isset($this->MemberID) ? $this->MemberID : null;
    }
    /**
     * Set MemberID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $memberID
     * @return \ID3Global\Models\GlobalMalaysiaAccount
     */
    public function setMemberID(?string $memberID = null): self
    {
        // validation for constraint: string
        if (!is_null($memberID) && !is_string($memberID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($memberID, true), gettype($memberID)), __LINE__);
        }
        if (is_null($memberID) || (is_array($memberID) && empty($memberID))) {
            unset($this->MemberID);
        } else {
            $this->MemberID = $memberID;
        }
        
        return $this;
    }
    /**
     * Get UserID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUserID(): ?string
    {
        return isset($this->UserID) ? $this->UserID : null;
    }
    /**
     * Set UserID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $userID
     * @return \ID3Global\Models\GlobalMalaysiaAccount
     */
    public function setUserID(?string $userID = null): self
    {
        // validation for constraint: string
        if (!is_null($userID) && !is_string($userID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userID, true), gettype($userID)), __LINE__);
        }
        if (is_null($userID) || (is_array($userID) && empty($userID))) {
            unset($this->UserID);
        } else {
            $this->UserID = $userID;
        }
        
        return $this;
    }
}
