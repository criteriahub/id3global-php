<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateSupplier Models
 * @subpackage Structs
 */
class UpdateSupplier extends AbstractStructBase
{
    /**
     * The Supplier
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplier|null
     */
    protected ?\ID3Global\Models\GlobalSupplier $Supplier = null;
    /**
     * Constructor method for UpdateSupplier
     * @uses UpdateSupplier::setSupplier()
     * @param \ID3Global\Models\GlobalSupplier $supplier
     */
    public function __construct(?\ID3Global\Models\GlobalSupplier $supplier = null)
    {
        $this
            ->setSupplier($supplier);
    }
    /**
     * Get Supplier value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplier|null
     */
    public function getSupplier(): ?\ID3Global\Models\GlobalSupplier
    {
        return isset($this->Supplier) ? $this->Supplier : null;
    }
    /**
     * Set Supplier value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupplier $supplier
     * @return \ID3Global\Models\UpdateSupplier
     */
    public function setSupplier(?\ID3Global\Models\GlobalSupplier $supplier = null): self
    {
        if (is_null($supplier) || (is_array($supplier) && empty($supplier))) {
            unset($this->Supplier);
        } else {
            $this->Supplier = $supplier;
        }
        
        return $this;
    }
}
