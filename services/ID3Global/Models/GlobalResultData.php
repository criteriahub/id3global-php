<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalResultData Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q437:GlobalResultData
 * @subpackage Structs
 */
class GlobalResultData extends AbstractStructBase
{
    /**
     * The AuthenticationID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AuthenticationID = null;
    /**
     * The Timestamp
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Timestamp = null;
    /**
     * The CustomerRef
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CustomerRef = null;
    /**
     * The ProfileID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ProfileID = null;
    /**
     * The ProfileName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ProfileName = null;
    /**
     * The ProfileVersion
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ProfileVersion = null;
    /**
     * The ProfileRevision
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ProfileRevision = null;
    /**
     * The ProfileState
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ProfileState = null;
    /**
     * The ResultCodes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes $ResultCodes = null;
    /**
     * The Score
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Score = null;
    /**
     * The BandText
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $BandText = null;
    /**
     * The UserBreakpoint
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $UserBreakpoint = null;
    /**
     * The NoRetry
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $NoRetry = null;
    /**
     * The ChainID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ChainID = null;
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Country = null;
    /**
     * The ItemCheckDecisionBands
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands $ItemCheckDecisionBands = null;
    /**
     * Constructor method for GlobalResultData
     * @uses GlobalResultData::setAuthenticationID()
     * @uses GlobalResultData::setTimestamp()
     * @uses GlobalResultData::setCustomerRef()
     * @uses GlobalResultData::setProfileID()
     * @uses GlobalResultData::setProfileName()
     * @uses GlobalResultData::setProfileVersion()
     * @uses GlobalResultData::setProfileRevision()
     * @uses GlobalResultData::setProfileState()
     * @uses GlobalResultData::setResultCodes()
     * @uses GlobalResultData::setScore()
     * @uses GlobalResultData::setBandText()
     * @uses GlobalResultData::setUserBreakpoint()
     * @uses GlobalResultData::setNoRetry()
     * @uses GlobalResultData::setChainID()
     * @uses GlobalResultData::setCountry()
     * @uses GlobalResultData::setItemCheckDecisionBands()
     * @param string $authenticationID
     * @param string $timestamp
     * @param string $customerRef
     * @param string $profileID
     * @param string $profileName
     * @param int $profileVersion
     * @param int $profileRevision
     * @param string $profileState
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes $resultCodes
     * @param int $score
     * @param string $bandText
     * @param int $userBreakpoint
     * @param bool $noRetry
     * @param string $chainID
     * @param string $country
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands $itemCheckDecisionBands
     */
    public function __construct(?string $authenticationID = null, ?string $timestamp = null, ?string $customerRef = null, ?string $profileID = null, ?string $profileName = null, ?int $profileVersion = null, ?int $profileRevision = null, ?string $profileState = null, ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes $resultCodes = null, ?int $score = null, ?string $bandText = null, ?int $userBreakpoint = null, ?bool $noRetry = null, ?string $chainID = null, ?string $country = null, ?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands $itemCheckDecisionBands = null)
    {
        $this
            ->setAuthenticationID($authenticationID)
            ->setTimestamp($timestamp)
            ->setCustomerRef($customerRef)
            ->setProfileID($profileID)
            ->setProfileName($profileName)
            ->setProfileVersion($profileVersion)
            ->setProfileRevision($profileRevision)
            ->setProfileState($profileState)
            ->setResultCodes($resultCodes)
            ->setScore($score)
            ->setBandText($bandText)
            ->setUserBreakpoint($userBreakpoint)
            ->setNoRetry($noRetry)
            ->setChainID($chainID)
            ->setCountry($country)
            ->setItemCheckDecisionBands($itemCheckDecisionBands);
    }
    /**
     * Get AuthenticationID value
     * @return string|null
     */
    public function getAuthenticationID(): ?string
    {
        return $this->AuthenticationID;
    }
    /**
     * Set AuthenticationID value
     * @param string $authenticationID
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setAuthenticationID(?string $authenticationID = null): self
    {
        // validation for constraint: string
        if (!is_null($authenticationID) && !is_string($authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authenticationID, true), gettype($authenticationID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($authenticationID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($authenticationID, true)), __LINE__);
        }
        $this->AuthenticationID = $authenticationID;
        
        return $this;
    }
    /**
     * Get Timestamp value
     * @return string|null
     */
    public function getTimestamp(): ?string
    {
        return $this->Timestamp;
    }
    /**
     * Set Timestamp value
     * @param string $timestamp
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setTimestamp(?string $timestamp = null): self
    {
        // validation for constraint: string
        if (!is_null($timestamp) && !is_string($timestamp)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($timestamp, true), gettype($timestamp)), __LINE__);
        }
        $this->Timestamp = $timestamp;
        
        return $this;
    }
    /**
     * Get CustomerRef value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCustomerRef(): ?string
    {
        return isset($this->CustomerRef) ? $this->CustomerRef : null;
    }
    /**
     * Set CustomerRef value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $customerRef
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setCustomerRef(?string $customerRef = null): self
    {
        // validation for constraint: string
        if (!is_null($customerRef) && !is_string($customerRef)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($customerRef, true), gettype($customerRef)), __LINE__);
        }
        if (is_null($customerRef) || (is_array($customerRef) && empty($customerRef))) {
            unset($this->CustomerRef);
        } else {
            $this->CustomerRef = $customerRef;
        }
        
        return $this;
    }
    /**
     * Get ProfileID value
     * @return string|null
     */
    public function getProfileID(): ?string
    {
        return $this->ProfileID;
    }
    /**
     * Set ProfileID value
     * @param string $profileID
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setProfileID(?string $profileID = null): self
    {
        // validation for constraint: string
        if (!is_null($profileID) && !is_string($profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($profileID, true), gettype($profileID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($profileID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($profileID, true)), __LINE__);
        }
        $this->ProfileID = $profileID;
        
        return $this;
    }
    /**
     * Get ProfileName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProfileName(): ?string
    {
        return isset($this->ProfileName) ? $this->ProfileName : null;
    }
    /**
     * Set ProfileName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $profileName
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setProfileName(?string $profileName = null): self
    {
        // validation for constraint: string
        if (!is_null($profileName) && !is_string($profileName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($profileName, true), gettype($profileName)), __LINE__);
        }
        if (is_null($profileName) || (is_array($profileName) && empty($profileName))) {
            unset($this->ProfileName);
        } else {
            $this->ProfileName = $profileName;
        }
        
        return $this;
    }
    /**
     * Get ProfileVersion value
     * @return int|null
     */
    public function getProfileVersion(): ?int
    {
        return $this->ProfileVersion;
    }
    /**
     * Set ProfileVersion value
     * @param int $profileVersion
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setProfileVersion(?int $profileVersion = null): self
    {
        // validation for constraint: int
        if (!is_null($profileVersion) && !(is_int($profileVersion) || ctype_digit($profileVersion))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($profileVersion, true), gettype($profileVersion)), __LINE__);
        }
        $this->ProfileVersion = $profileVersion;
        
        return $this;
    }
    /**
     * Get ProfileRevision value
     * @return int|null
     */
    public function getProfileRevision(): ?int
    {
        return $this->ProfileRevision;
    }
    /**
     * Set ProfileRevision value
     * @param int $profileRevision
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setProfileRevision(?int $profileRevision = null): self
    {
        // validation for constraint: int
        if (!is_null($profileRevision) && !(is_int($profileRevision) || ctype_digit($profileRevision))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($profileRevision, true), gettype($profileRevision)), __LINE__);
        }
        $this->ProfileRevision = $profileRevision;
        
        return $this;
    }
    /**
     * Get ProfileState value
     * @return string|null
     */
    public function getProfileState(): ?string
    {
        return $this->ProfileState;
    }
    /**
     * Set ProfileState value
     * @uses \ID3Global\Enums\GlobalProfileState::valueIsValid()
     * @uses \ID3Global\Enums\GlobalProfileState::getValidValues()
     * @throws InvalidArgumentException
     * @param string $profileState
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setProfileState(?string $profileState = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalProfileState::valueIsValid($profileState)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalProfileState', is_array($profileState) ? implode(', ', $profileState) : var_export($profileState, true), implode(', ', \ID3Global\Enums\GlobalProfileState::getValidValues())), __LINE__);
        }
        $this->ProfileState = $profileState;
        
        return $this;
    }
    /**
     * Get ResultCodes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes|null
     */
    public function getResultCodes(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes
    {
        return isset($this->ResultCodes) ? $this->ResultCodes : null;
    }
    /**
     * Set ResultCodes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes $resultCodes
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setResultCodes(?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes $resultCodes = null): self
    {
        if (is_null($resultCodes) || (is_array($resultCodes) && empty($resultCodes))) {
            unset($this->ResultCodes);
        } else {
            $this->ResultCodes = $resultCodes;
        }
        
        return $this;
    }
    /**
     * Get Score value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getScore(): ?int
    {
        return isset($this->Score) ? $this->Score : null;
    }
    /**
     * Set Score value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $score
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setScore(?int $score = null): self
    {
        // validation for constraint: int
        if (!is_null($score) && !(is_int($score) || ctype_digit($score))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($score, true), gettype($score)), __LINE__);
        }
        if (is_null($score) || (is_array($score) && empty($score))) {
            unset($this->Score);
        } else {
            $this->Score = $score;
        }
        
        return $this;
    }
    /**
     * Get BandText value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBandText(): ?string
    {
        return isset($this->BandText) ? $this->BandText : null;
    }
    /**
     * Set BandText value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $bandText
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setBandText(?string $bandText = null): self
    {
        // validation for constraint: string
        if (!is_null($bandText) && !is_string($bandText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bandText, true), gettype($bandText)), __LINE__);
        }
        if (is_null($bandText) || (is_array($bandText) && empty($bandText))) {
            unset($this->BandText);
        } else {
            $this->BandText = $bandText;
        }
        
        return $this;
    }
    /**
     * Get UserBreakpoint value
     * @return int|null
     */
    public function getUserBreakpoint(): ?int
    {
        return $this->UserBreakpoint;
    }
    /**
     * Set UserBreakpoint value
     * @param int $userBreakpoint
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setUserBreakpoint(?int $userBreakpoint = null): self
    {
        // validation for constraint: int
        if (!is_null($userBreakpoint) && !(is_int($userBreakpoint) || ctype_digit($userBreakpoint))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($userBreakpoint, true), gettype($userBreakpoint)), __LINE__);
        }
        $this->UserBreakpoint = $userBreakpoint;
        
        return $this;
    }
    /**
     * Get NoRetry value
     * @return bool|null
     */
    public function getNoRetry(): ?bool
    {
        return $this->NoRetry;
    }
    /**
     * Set NoRetry value
     * @param bool $noRetry
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setNoRetry(?bool $noRetry = null): self
    {
        // validation for constraint: boolean
        if (!is_null($noRetry) && !is_bool($noRetry)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($noRetry, true), gettype($noRetry)), __LINE__);
        }
        $this->NoRetry = $noRetry;
        
        return $this;
    }
    /**
     * Get ChainID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getChainID(): ?string
    {
        return isset($this->ChainID) ? $this->ChainID : null;
    }
    /**
     * Set ChainID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $chainID
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setChainID(?string $chainID = null): self
    {
        // validation for constraint: string
        if (!is_null($chainID) && !is_string($chainID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($chainID, true), gettype($chainID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($chainID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $chainID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($chainID, true)), __LINE__);
        }
        if (is_null($chainID) || (is_array($chainID) && empty($chainID))) {
            unset($this->ChainID);
        } else {
            $this->ChainID = $chainID;
        }
        
        return $this;
    }
    /**
     * Get Country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return isset($this->Country) ? $this->Country : null;
    }
    /**
     * Set Country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $country
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->Country);
        } else {
            $this->Country = $country;
        }
        
        return $this;
    }
    /**
     * Get ItemCheckDecisionBands value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands|null
     */
    public function getItemCheckDecisionBands(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands
    {
        return isset($this->ItemCheckDecisionBands) ? $this->ItemCheckDecisionBands : null;
    }
    /**
     * Set ItemCheckDecisionBands value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands $itemCheckDecisionBands
     * @return \ID3Global\Models\GlobalResultData
     */
    public function setItemCheckDecisionBands(?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands $itemCheckDecisionBands = null): self
    {
        if (is_null($itemCheckDecisionBands) || (is_array($itemCheckDecisionBands) && empty($itemCheckDecisionBands))) {
            unset($this->ItemCheckDecisionBands);
        } else {
            $this->ItemCheckDecisionBands = $itemCheckDecisionBands;
        }
        
        return $this;
    }
}
