<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSouthAfricaCreditSuppliersResponse Models
 * @subpackage Structs
 */
class GetSouthAfricaCreditSuppliersResponse extends AbstractStructBase
{
    /**
     * The GetSouthAfricaCreditSuppliersResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSouthAfricaSupplier|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSouthAfricaSupplier $GetSouthAfricaCreditSuppliersResult = null;
    /**
     * Constructor method for GetSouthAfricaCreditSuppliersResponse
     * @uses GetSouthAfricaCreditSuppliersResponse::setGetSouthAfricaCreditSuppliersResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalSouthAfricaSupplier $getSouthAfricaCreditSuppliersResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalSouthAfricaSupplier $getSouthAfricaCreditSuppliersResult = null)
    {
        $this
            ->setGetSouthAfricaCreditSuppliersResult($getSouthAfricaCreditSuppliersResult);
    }
    /**
     * Get GetSouthAfricaCreditSuppliersResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSouthAfricaSupplier|null
     */
    public function getGetSouthAfricaCreditSuppliersResult(): ?\ID3Global\Arrays\ArrayOfGlobalSouthAfricaSupplier
    {
        return isset($this->GetSouthAfricaCreditSuppliersResult) ? $this->GetSouthAfricaCreditSuppliersResult : null;
    }
    /**
     * Set GetSouthAfricaCreditSuppliersResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSouthAfricaSupplier $getSouthAfricaCreditSuppliersResult
     * @return \ID3Global\Models\GetSouthAfricaCreditSuppliersResponse
     */
    public function setGetSouthAfricaCreditSuppliersResult(?\ID3Global\Arrays\ArrayOfGlobalSouthAfricaSupplier $getSouthAfricaCreditSuppliersResult = null): self
    {
        if (is_null($getSouthAfricaCreditSuppliersResult) || (is_array($getSouthAfricaCreditSuppliersResult) && empty($getSouthAfricaCreditSuppliersResult))) {
            unset($this->GetSouthAfricaCreditSuppliersResult);
        } else {
            $this->GetSouthAfricaCreditSuppliersResult = $getSouthAfricaCreditSuppliersResult;
        }
        
        return $this;
    }
}
