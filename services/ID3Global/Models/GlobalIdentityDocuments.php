<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalIdentityDocuments Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q348:GlobalIdentityDocuments
 * @subpackage Structs
 */
class GlobalIdentityDocuments extends AbstractStructBase
{
    /**
     * The China
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalChina|null
     */
    protected ?\ID3Global\Models\GlobalChina $China = null;
    /**
     * The India
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalIndia|null
     */
    protected ?\ID3Global\Models\GlobalIndia $India = null;
    /**
     * The NewZealand
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalNewZealand|null
     */
    protected ?\ID3Global\Models\GlobalNewZealand $NewZealand = null;
    /**
     * The InternationalPassport
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalInternationalPassport|null
     */
    protected ?\ID3Global\Models\GlobalInternationalPassport $InternationalPassport = null;
    /**
     * The EuropeanIdentityCard
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalEuropeanIdentityCard|null
     */
    protected ?\ID3Global\Models\GlobalEuropeanIdentityCard $EuropeanIdentityCard = null;
    /**
     * The UK
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalUKData|null
     */
    protected ?\ID3Global\Models\GlobalUKData $UK = null;
    /**
     * The Australia
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAustralia|null
     */
    protected ?\ID3Global\Models\GlobalAustralia $Australia = null;
    /**
     * The US
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalUS|null
     */
    protected ?\ID3Global\Models\GlobalUS $US = null;
    /**
     * The IdentityCard
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalIdentityCard|null
     */
    protected ?\ID3Global\Models\GlobalIdentityCard $IdentityCard = null;
    /**
     * The Canada
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCanada|null
     */
    protected ?\ID3Global\Models\GlobalCanada $Canada = null;
    /**
     * The Mexico
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalMexico|null
     */
    protected ?\ID3Global\Models\GlobalMexico $Mexico = null;
    /**
     * The Brazil
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalBrazil|null
     */
    protected ?\ID3Global\Models\GlobalBrazil $Brazil = null;
    /**
     * The Spain
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSpain|null
     */
    protected ?\ID3Global\Models\GlobalSpain $Spain = null;
    /**
     * The Argentina
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalArgentina|null
     */
    protected ?\ID3Global\Models\GlobalArgentina $Argentina = null;
    /**
     * Constructor method for GlobalIdentityDocuments
     * @uses GlobalIdentityDocuments::setChina()
     * @uses GlobalIdentityDocuments::setIndia()
     * @uses GlobalIdentityDocuments::setNewZealand()
     * @uses GlobalIdentityDocuments::setInternationalPassport()
     * @uses GlobalIdentityDocuments::setEuropeanIdentityCard()
     * @uses GlobalIdentityDocuments::setUK()
     * @uses GlobalIdentityDocuments::setAustralia()
     * @uses GlobalIdentityDocuments::setUS()
     * @uses GlobalIdentityDocuments::setIdentityCard()
     * @uses GlobalIdentityDocuments::setCanada()
     * @uses GlobalIdentityDocuments::setMexico()
     * @uses GlobalIdentityDocuments::setBrazil()
     * @uses GlobalIdentityDocuments::setSpain()
     * @uses GlobalIdentityDocuments::setArgentina()
     * @param \ID3Global\Models\GlobalChina $china
     * @param \ID3Global\Models\GlobalIndia $india
     * @param \ID3Global\Models\GlobalNewZealand $newZealand
     * @param \ID3Global\Models\GlobalInternationalPassport $internationalPassport
     * @param \ID3Global\Models\GlobalEuropeanIdentityCard $europeanIdentityCard
     * @param \ID3Global\Models\GlobalUKData $uK
     * @param \ID3Global\Models\GlobalAustralia $australia
     * @param \ID3Global\Models\GlobalUS $uS
     * @param \ID3Global\Models\GlobalIdentityCard $identityCard
     * @param \ID3Global\Models\GlobalCanada $canada
     * @param \ID3Global\Models\GlobalMexico $mexico
     * @param \ID3Global\Models\GlobalBrazil $brazil
     * @param \ID3Global\Models\GlobalSpain $spain
     * @param \ID3Global\Models\GlobalArgentina $argentina
     */
    public function __construct(?\ID3Global\Models\GlobalChina $china = null, ?\ID3Global\Models\GlobalIndia $india = null, ?\ID3Global\Models\GlobalNewZealand $newZealand = null, ?\ID3Global\Models\GlobalInternationalPassport $internationalPassport = null, ?\ID3Global\Models\GlobalEuropeanIdentityCard $europeanIdentityCard = null, ?\ID3Global\Models\GlobalUKData $uK = null, ?\ID3Global\Models\GlobalAustralia $australia = null, ?\ID3Global\Models\GlobalUS $uS = null, ?\ID3Global\Models\GlobalIdentityCard $identityCard = null, ?\ID3Global\Models\GlobalCanada $canada = null, ?\ID3Global\Models\GlobalMexico $mexico = null, ?\ID3Global\Models\GlobalBrazil $brazil = null, ?\ID3Global\Models\GlobalSpain $spain = null, ?\ID3Global\Models\GlobalArgentina $argentina = null)
    {
        $this
            ->setChina($china)
            ->setIndia($india)
            ->setNewZealand($newZealand)
            ->setInternationalPassport($internationalPassport)
            ->setEuropeanIdentityCard($europeanIdentityCard)
            ->setUK($uK)
            ->setAustralia($australia)
            ->setUS($uS)
            ->setIdentityCard($identityCard)
            ->setCanada($canada)
            ->setMexico($mexico)
            ->setBrazil($brazil)
            ->setSpain($spain)
            ->setArgentina($argentina);
    }
    /**
     * Get China value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalChina|null
     */
    public function getChina(): ?\ID3Global\Models\GlobalChina
    {
        return isset($this->China) ? $this->China : null;
    }
    /**
     * Set China value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalChina $china
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setChina(?\ID3Global\Models\GlobalChina $china = null): self
    {
        if (is_null($china) || (is_array($china) && empty($china))) {
            unset($this->China);
        } else {
            $this->China = $china;
        }
        
        return $this;
    }
    /**
     * Get India value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalIndia|null
     */
    public function getIndia(): ?\ID3Global\Models\GlobalIndia
    {
        return isset($this->India) ? $this->India : null;
    }
    /**
     * Set India value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalIndia $india
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setIndia(?\ID3Global\Models\GlobalIndia $india = null): self
    {
        if (is_null($india) || (is_array($india) && empty($india))) {
            unset($this->India);
        } else {
            $this->India = $india;
        }
        
        return $this;
    }
    /**
     * Get NewZealand value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalNewZealand|null
     */
    public function getNewZealand(): ?\ID3Global\Models\GlobalNewZealand
    {
        return isset($this->NewZealand) ? $this->NewZealand : null;
    }
    /**
     * Set NewZealand value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalNewZealand $newZealand
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setNewZealand(?\ID3Global\Models\GlobalNewZealand $newZealand = null): self
    {
        if (is_null($newZealand) || (is_array($newZealand) && empty($newZealand))) {
            unset($this->NewZealand);
        } else {
            $this->NewZealand = $newZealand;
        }
        
        return $this;
    }
    /**
     * Get InternationalPassport value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalInternationalPassport|null
     */
    public function getInternationalPassport(): ?\ID3Global\Models\GlobalInternationalPassport
    {
        return isset($this->InternationalPassport) ? $this->InternationalPassport : null;
    }
    /**
     * Set InternationalPassport value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalInternationalPassport $internationalPassport
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setInternationalPassport(?\ID3Global\Models\GlobalInternationalPassport $internationalPassport = null): self
    {
        if (is_null($internationalPassport) || (is_array($internationalPassport) && empty($internationalPassport))) {
            unset($this->InternationalPassport);
        } else {
            $this->InternationalPassport = $internationalPassport;
        }
        
        return $this;
    }
    /**
     * Get EuropeanIdentityCard value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalEuropeanIdentityCard|null
     */
    public function getEuropeanIdentityCard(): ?\ID3Global\Models\GlobalEuropeanIdentityCard
    {
        return isset($this->EuropeanIdentityCard) ? $this->EuropeanIdentityCard : null;
    }
    /**
     * Set EuropeanIdentityCard value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalEuropeanIdentityCard $europeanIdentityCard
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setEuropeanIdentityCard(?\ID3Global\Models\GlobalEuropeanIdentityCard $europeanIdentityCard = null): self
    {
        if (is_null($europeanIdentityCard) || (is_array($europeanIdentityCard) && empty($europeanIdentityCard))) {
            unset($this->EuropeanIdentityCard);
        } else {
            $this->EuropeanIdentityCard = $europeanIdentityCard;
        }
        
        return $this;
    }
    /**
     * Get UK value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalUKData|null
     */
    public function getUK(): ?\ID3Global\Models\GlobalUKData
    {
        return isset($this->UK) ? $this->UK : null;
    }
    /**
     * Set UK value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalUKData $uK
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setUK(?\ID3Global\Models\GlobalUKData $uK = null): self
    {
        if (is_null($uK) || (is_array($uK) && empty($uK))) {
            unset($this->UK);
        } else {
            $this->UK = $uK;
        }
        
        return $this;
    }
    /**
     * Get Australia value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAustralia|null
     */
    public function getAustralia(): ?\ID3Global\Models\GlobalAustralia
    {
        return isset($this->Australia) ? $this->Australia : null;
    }
    /**
     * Set Australia value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAustralia $australia
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setAustralia(?\ID3Global\Models\GlobalAustralia $australia = null): self
    {
        if (is_null($australia) || (is_array($australia) && empty($australia))) {
            unset($this->Australia);
        } else {
            $this->Australia = $australia;
        }
        
        return $this;
    }
    /**
     * Get US value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalUS|null
     */
    public function getUS(): ?\ID3Global\Models\GlobalUS
    {
        return isset($this->US) ? $this->US : null;
    }
    /**
     * Set US value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalUS $uS
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setUS(?\ID3Global\Models\GlobalUS $uS = null): self
    {
        if (is_null($uS) || (is_array($uS) && empty($uS))) {
            unset($this->US);
        } else {
            $this->US = $uS;
        }
        
        return $this;
    }
    /**
     * Get IdentityCard value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalIdentityCard|null
     */
    public function getIdentityCard(): ?\ID3Global\Models\GlobalIdentityCard
    {
        return isset($this->IdentityCard) ? $this->IdentityCard : null;
    }
    /**
     * Set IdentityCard value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalIdentityCard $identityCard
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setIdentityCard(?\ID3Global\Models\GlobalIdentityCard $identityCard = null): self
    {
        if (is_null($identityCard) || (is_array($identityCard) && empty($identityCard))) {
            unset($this->IdentityCard);
        } else {
            $this->IdentityCard = $identityCard;
        }
        
        return $this;
    }
    /**
     * Get Canada value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCanada|null
     */
    public function getCanada(): ?\ID3Global\Models\GlobalCanada
    {
        return isset($this->Canada) ? $this->Canada : null;
    }
    /**
     * Set Canada value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCanada $canada
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setCanada(?\ID3Global\Models\GlobalCanada $canada = null): self
    {
        if (is_null($canada) || (is_array($canada) && empty($canada))) {
            unset($this->Canada);
        } else {
            $this->Canada = $canada;
        }
        
        return $this;
    }
    /**
     * Get Mexico value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalMexico|null
     */
    public function getMexico(): ?\ID3Global\Models\GlobalMexico
    {
        return isset($this->Mexico) ? $this->Mexico : null;
    }
    /**
     * Set Mexico value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalMexico $mexico
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setMexico(?\ID3Global\Models\GlobalMexico $mexico = null): self
    {
        if (is_null($mexico) || (is_array($mexico) && empty($mexico))) {
            unset($this->Mexico);
        } else {
            $this->Mexico = $mexico;
        }
        
        return $this;
    }
    /**
     * Get Brazil value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalBrazil|null
     */
    public function getBrazil(): ?\ID3Global\Models\GlobalBrazil
    {
        return isset($this->Brazil) ? $this->Brazil : null;
    }
    /**
     * Set Brazil value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalBrazil $brazil
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setBrazil(?\ID3Global\Models\GlobalBrazil $brazil = null): self
    {
        if (is_null($brazil) || (is_array($brazil) && empty($brazil))) {
            unset($this->Brazil);
        } else {
            $this->Brazil = $brazil;
        }
        
        return $this;
    }
    /**
     * Get Spain value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSpain|null
     */
    public function getSpain(): ?\ID3Global\Models\GlobalSpain
    {
        return isset($this->Spain) ? $this->Spain : null;
    }
    /**
     * Set Spain value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSpain $spain
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setSpain(?\ID3Global\Models\GlobalSpain $spain = null): self
    {
        if (is_null($spain) || (is_array($spain) && empty($spain))) {
            unset($this->Spain);
        } else {
            $this->Spain = $spain;
        }
        
        return $this;
    }
    /**
     * Get Argentina value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalArgentina|null
     */
    public function getArgentina(): ?\ID3Global\Models\GlobalArgentina
    {
        return isset($this->Argentina) ? $this->Argentina : null;
    }
    /**
     * Set Argentina value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalArgentina $argentina
     * @return \ID3Global\Models\GlobalIdentityDocuments
     */
    public function setArgentina(?\ID3Global\Models\GlobalArgentina $argentina = null): self
    {
        if (is_null($argentina) || (is_array($argentina) && empty($argentina))) {
            unset($this->Argentina);
        } else {
            $this->Argentina = $argentina;
        }
        
        return $this;
    }
}
