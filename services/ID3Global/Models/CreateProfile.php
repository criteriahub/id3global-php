<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateProfile Models
 * @subpackage Structs
 */
class CreateProfile extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Description = null;
    /**
     * The ItemIDs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    protected ?\ID3Global\Arrays\ArrayOfunsignedInt $ItemIDs = null;
    /**
     * Constructor method for CreateProfile
     * @uses CreateProfile::setOrgID()
     * @uses CreateProfile::setName()
     * @uses CreateProfile::setDescription()
     * @uses CreateProfile::setItemIDs()
     * @param string $orgID
     * @param string $name
     * @param string $description
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $itemIDs
     */
    public function __construct(?string $orgID = null, ?string $name = null, ?string $description = null, ?\ID3Global\Arrays\ArrayOfunsignedInt $itemIDs = null)
    {
        $this
            ->setOrgID($orgID)
            ->setName($name)
            ->setDescription($description)
            ->setItemIDs($itemIDs);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\CreateProfile
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\CreateProfile
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \ID3Global\Models\CreateProfile
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        
        return $this;
    }
    /**
     * Get ItemIDs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    public function getItemIDs(): ?\ID3Global\Arrays\ArrayOfunsignedInt
    {
        return isset($this->ItemIDs) ? $this->ItemIDs : null;
    }
    /**
     * Set ItemIDs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $itemIDs
     * @return \ID3Global\Models\CreateProfile
     */
    public function setItemIDs(?\ID3Global\Arrays\ArrayOfunsignedInt $itemIDs = null): self
    {
        if (is_null($itemIDs) || (is_array($itemIDs) && empty($itemIDs))) {
            unset($this->ItemIDs);
        } else {
            $this->ItemIDs = $itemIDs;
        }
        
        return $this;
    }
}
