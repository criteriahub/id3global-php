<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalPreAuthentication Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q251:GlobalPreAuthentication
 * @subpackage Structs
 */
class GlobalPreAuthentication extends AbstractStructBase
{
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The MatchDocument
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MatchDocument = null;
    /**
     * The MatchNameAddresses
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $MatchNameAddresses = null;
    /**
     * The DuplicateMatchNameAddress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $DuplicateMatchNameAddress = null;
    /**
     * The TimespanHours
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TimespanHours = null;
    /**
     * The TimespanDays
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TimespanDays = null;
    /**
     * The Duplicates
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Duplicates = null;
    /**
     * The ResponseMessage
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ResponseMessage = null;
    /**
     * The RestrictByProfile
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $RestrictByProfile = null;
    /**
     * The RestrictByUser
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $RestrictByUser = null;
    /**
     * Constructor method for GlobalPreAuthentication
     * @uses GlobalPreAuthentication::setName()
     * @uses GlobalPreAuthentication::setMatchDocument()
     * @uses GlobalPreAuthentication::setMatchNameAddresses()
     * @uses GlobalPreAuthentication::setDuplicateMatchNameAddress()
     * @uses GlobalPreAuthentication::setTimespanHours()
     * @uses GlobalPreAuthentication::setTimespanDays()
     * @uses GlobalPreAuthentication::setDuplicates()
     * @uses GlobalPreAuthentication::setResponseMessage()
     * @uses GlobalPreAuthentication::setRestrictByProfile()
     * @uses GlobalPreAuthentication::setRestrictByUser()
     * @param string $name
     * @param string $matchDocument
     * @param \ID3Global\Arrays\ArrayOfstring $matchNameAddresses
     * @param int $duplicateMatchNameAddress
     * @param int $timespanHours
     * @param int $timespanDays
     * @param int $duplicates
     * @param string $responseMessage
     * @param bool $restrictByProfile
     * @param bool $restrictByUser
     */
    public function __construct(?string $name = null, ?string $matchDocument = null, ?\ID3Global\Arrays\ArrayOfstring $matchNameAddresses = null, ?int $duplicateMatchNameAddress = null, ?int $timespanHours = null, ?int $timespanDays = null, ?int $duplicates = null, ?string $responseMessage = null, ?bool $restrictByProfile = null, ?bool $restrictByUser = null)
    {
        $this
            ->setName($name)
            ->setMatchDocument($matchDocument)
            ->setMatchNameAddresses($matchNameAddresses)
            ->setDuplicateMatchNameAddress($duplicateMatchNameAddress)
            ->setTimespanHours($timespanHours)
            ->setTimespanDays($timespanDays)
            ->setDuplicates($duplicates)
            ->setResponseMessage($responseMessage)
            ->setRestrictByProfile($restrictByProfile)
            ->setRestrictByUser($restrictByUser);
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalPreAuthentication
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get MatchDocument value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMatchDocument(): ?string
    {
        return isset($this->MatchDocument) ? $this->MatchDocument : null;
    }
    /**
     * Set MatchDocument value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $matchDocument
     * @return \ID3Global\Models\GlobalPreAuthentication
     */
    public function setMatchDocument(?string $matchDocument = null): self
    {
        // validation for constraint: string
        if (!is_null($matchDocument) && !is_string($matchDocument)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($matchDocument, true), gettype($matchDocument)), __LINE__);
        }
        if (is_null($matchDocument) || (is_array($matchDocument) && empty($matchDocument))) {
            unset($this->MatchDocument);
        } else {
            $this->MatchDocument = $matchDocument;
        }
        
        return $this;
    }
    /**
     * Get MatchNameAddresses value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getMatchNameAddresses(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->MatchNameAddresses) ? $this->MatchNameAddresses : null;
    }
    /**
     * Set MatchNameAddresses value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $matchNameAddresses
     * @return \ID3Global\Models\GlobalPreAuthentication
     */
    public function setMatchNameAddresses(?\ID3Global\Arrays\ArrayOfstring $matchNameAddresses = null): self
    {
        if (is_null($matchNameAddresses) || (is_array($matchNameAddresses) && empty($matchNameAddresses))) {
            unset($this->MatchNameAddresses);
        } else {
            $this->MatchNameAddresses = $matchNameAddresses;
        }
        
        return $this;
    }
    /**
     * Get DuplicateMatchNameAddress value
     * @return int|null
     */
    public function getDuplicateMatchNameAddress(): ?int
    {
        return $this->DuplicateMatchNameAddress;
    }
    /**
     * Set DuplicateMatchNameAddress value
     * @param int $duplicateMatchNameAddress
     * @return \ID3Global\Models\GlobalPreAuthentication
     */
    public function setDuplicateMatchNameAddress(?int $duplicateMatchNameAddress = null): self
    {
        // validation for constraint: int
        if (!is_null($duplicateMatchNameAddress) && !(is_int($duplicateMatchNameAddress) || ctype_digit($duplicateMatchNameAddress))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($duplicateMatchNameAddress, true), gettype($duplicateMatchNameAddress)), __LINE__);
        }
        $this->DuplicateMatchNameAddress = $duplicateMatchNameAddress;
        
        return $this;
    }
    /**
     * Get TimespanHours value
     * @return int|null
     */
    public function getTimespanHours(): ?int
    {
        return $this->TimespanHours;
    }
    /**
     * Set TimespanHours value
     * @param int $timespanHours
     * @return \ID3Global\Models\GlobalPreAuthentication
     */
    public function setTimespanHours(?int $timespanHours = null): self
    {
        // validation for constraint: int
        if (!is_null($timespanHours) && !(is_int($timespanHours) || ctype_digit($timespanHours))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($timespanHours, true), gettype($timespanHours)), __LINE__);
        }
        $this->TimespanHours = $timespanHours;
        
        return $this;
    }
    /**
     * Get TimespanDays value
     * @return int|null
     */
    public function getTimespanDays(): ?int
    {
        return $this->TimespanDays;
    }
    /**
     * Set TimespanDays value
     * @param int $timespanDays
     * @return \ID3Global\Models\GlobalPreAuthentication
     */
    public function setTimespanDays(?int $timespanDays = null): self
    {
        // validation for constraint: int
        if (!is_null($timespanDays) && !(is_int($timespanDays) || ctype_digit($timespanDays))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($timespanDays, true), gettype($timespanDays)), __LINE__);
        }
        $this->TimespanDays = $timespanDays;
        
        return $this;
    }
    /**
     * Get Duplicates value
     * @return int|null
     */
    public function getDuplicates(): ?int
    {
        return $this->Duplicates;
    }
    /**
     * Set Duplicates value
     * @param int $duplicates
     * @return \ID3Global\Models\GlobalPreAuthentication
     */
    public function setDuplicates(?int $duplicates = null): self
    {
        // validation for constraint: int
        if (!is_null($duplicates) && !(is_int($duplicates) || ctype_digit($duplicates))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($duplicates, true), gettype($duplicates)), __LINE__);
        }
        $this->Duplicates = $duplicates;
        
        return $this;
    }
    /**
     * Get ResponseMessage value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getResponseMessage(): ?string
    {
        return isset($this->ResponseMessage) ? $this->ResponseMessage : null;
    }
    /**
     * Set ResponseMessage value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $responseMessage
     * @return \ID3Global\Models\GlobalPreAuthentication
     */
    public function setResponseMessage(?string $responseMessage = null): self
    {
        // validation for constraint: string
        if (!is_null($responseMessage) && !is_string($responseMessage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($responseMessage, true), gettype($responseMessage)), __LINE__);
        }
        if (is_null($responseMessage) || (is_array($responseMessage) && empty($responseMessage))) {
            unset($this->ResponseMessage);
        } else {
            $this->ResponseMessage = $responseMessage;
        }
        
        return $this;
    }
    /**
     * Get RestrictByProfile value
     * @return bool|null
     */
    public function getRestrictByProfile(): ?bool
    {
        return $this->RestrictByProfile;
    }
    /**
     * Set RestrictByProfile value
     * @param bool $restrictByProfile
     * @return \ID3Global\Models\GlobalPreAuthentication
     */
    public function setRestrictByProfile(?bool $restrictByProfile = null): self
    {
        // validation for constraint: boolean
        if (!is_null($restrictByProfile) && !is_bool($restrictByProfile)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($restrictByProfile, true), gettype($restrictByProfile)), __LINE__);
        }
        $this->RestrictByProfile = $restrictByProfile;
        
        return $this;
    }
    /**
     * Get RestrictByUser value
     * @return bool|null
     */
    public function getRestrictByUser(): ?bool
    {
        return $this->RestrictByUser;
    }
    /**
     * Set RestrictByUser value
     * @param bool $restrictByUser
     * @return \ID3Global\Models\GlobalPreAuthentication
     */
    public function setRestrictByUser(?bool $restrictByUser = null): self
    {
        // validation for constraint: boolean
        if (!is_null($restrictByUser) && !is_bool($restrictByUser)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($restrictByUser, true), gettype($restrictByUser)), __LINE__);
        }
        $this->RestrictByUser = $restrictByUser;
        
        return $this;
    }
}
