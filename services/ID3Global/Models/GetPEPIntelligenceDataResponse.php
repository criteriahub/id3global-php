<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPEPIntelligenceDataResponse Models
 * @subpackage Structs
 */
class GetPEPIntelligenceDataResponse extends AbstractStructBase
{
    /**
     * The GetPEPIntelligenceDataResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalPEPIntelligenceData|null
     */
    protected ?\ID3Global\Models\GlobalPEPIntelligenceData $GetPEPIntelligenceDataResult = null;
    /**
     * Constructor method for GetPEPIntelligenceDataResponse
     * @uses GetPEPIntelligenceDataResponse::setGetPEPIntelligenceDataResult()
     * @param \ID3Global\Models\GlobalPEPIntelligenceData $getPEPIntelligenceDataResult
     */
    public function __construct(?\ID3Global\Models\GlobalPEPIntelligenceData $getPEPIntelligenceDataResult = null)
    {
        $this
            ->setGetPEPIntelligenceDataResult($getPEPIntelligenceDataResult);
    }
    /**
     * Get GetPEPIntelligenceDataResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalPEPIntelligenceData|null
     */
    public function getGetPEPIntelligenceDataResult(): ?\ID3Global\Models\GlobalPEPIntelligenceData
    {
        return isset($this->GetPEPIntelligenceDataResult) ? $this->GetPEPIntelligenceDataResult : null;
    }
    /**
     * Set GetPEPIntelligenceDataResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalPEPIntelligenceData $getPEPIntelligenceDataResult
     * @return \ID3Global\Models\GetPEPIntelligenceDataResponse
     */
    public function setGetPEPIntelligenceDataResult(?\ID3Global\Models\GlobalPEPIntelligenceData $getPEPIntelligenceDataResult = null): self
    {
        if (is_null($getPEPIntelligenceDataResult) || (is_array($getPEPIntelligenceDataResult) && empty($getPEPIntelligenceDataResult))) {
            unset($this->GetPEPIntelligenceDataResult);
        } else {
            $this->GetPEPIntelligenceDataResult = $getPEPIntelligenceDataResult;
        }
        
        return $this;
    }
}
