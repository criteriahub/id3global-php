<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSanctionsCategories Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q86:GlobalSanctionsCategories
 * @subpackage Structs
 */
class GlobalSanctionsCategories extends AbstractStructBase
{
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $Name = null;
    /**
     * Constructor method for GlobalSanctionsCategories
     * @uses GlobalSanctionsCategories::setName()
     * @param \ID3Global\Arrays\ArrayOfstring $name
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfstring $name = null)
    {
        $this
            ->setName($name);
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getName(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $name
     * @return \ID3Global\Models\GlobalSanctionsCategories
     */
    public function setName(?\ID3Global\Arrays\ArrayOfstring $name = null): self
    {
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
}
