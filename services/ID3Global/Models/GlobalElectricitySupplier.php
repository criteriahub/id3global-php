<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalElectricitySupplier Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q405:GlobalElectricitySupplier
 * @subpackage Structs
 */
class GlobalElectricitySupplier extends AbstractStructBase
{
    /**
     * The Number
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Number = null;
    /**
     * The MailSort
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MailSort = null;
    /**
     * The Postcode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Postcode = null;
    /**
     * Constructor method for GlobalElectricitySupplier
     * @uses GlobalElectricitySupplier::setNumber()
     * @uses GlobalElectricitySupplier::setMailSort()
     * @uses GlobalElectricitySupplier::setPostcode()
     * @param string $number
     * @param string $mailSort
     * @param string $postcode
     */
    public function __construct(?string $number = null, ?string $mailSort = null, ?string $postcode = null)
    {
        $this
            ->setNumber($number)
            ->setMailSort($mailSort)
            ->setPostcode($postcode);
    }
    /**
     * Get Number value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return isset($this->Number) ? $this->Number : null;
    }
    /**
     * Set Number value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $number
     * @return \ID3Global\Models\GlobalElectricitySupplier
     */
    public function setNumber(?string $number = null): self
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($number, true), gettype($number)), __LINE__);
        }
        if (is_null($number) || (is_array($number) && empty($number))) {
            unset($this->Number);
        } else {
            $this->Number = $number;
        }
        
        return $this;
    }
    /**
     * Get MailSort value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMailSort(): ?string
    {
        return isset($this->MailSort) ? $this->MailSort : null;
    }
    /**
     * Set MailSort value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mailSort
     * @return \ID3Global\Models\GlobalElectricitySupplier
     */
    public function setMailSort(?string $mailSort = null): self
    {
        // validation for constraint: string
        if (!is_null($mailSort) && !is_string($mailSort)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mailSort, true), gettype($mailSort)), __LINE__);
        }
        if (is_null($mailSort) || (is_array($mailSort) && empty($mailSort))) {
            unset($this->MailSort);
        } else {
            $this->MailSort = $mailSort;
        }
        
        return $this;
    }
    /**
     * Get Postcode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPostcode(): ?string
    {
        return isset($this->Postcode) ? $this->Postcode : null;
    }
    /**
     * Set Postcode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $postcode
     * @return \ID3Global\Models\GlobalElectricitySupplier
     */
    public function setPostcode(?string $postcode = null): self
    {
        // validation for constraint: string
        if (!is_null($postcode) && !is_string($postcode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($postcode, true), gettype($postcode)), __LINE__);
        }
        if (is_null($postcode) || (is_array($postcode) && empty($postcode))) {
            unset($this->Postcode);
        } else {
            $this->Postcode = $postcode;
        }
        
        return $this;
    }
}
