<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalImages Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q154:GlobalImages
 * @subpackage Structs
 */
class GlobalImages extends AbstractStructBase
{
    /**
     * The Images
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalImageDetails|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalImageDetails $Images = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The TotalPages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPages = null;
    /**
     * The TotalImages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalImages = null;
    /**
     * Constructor method for GlobalImages
     * @uses GlobalImages::setImages()
     * @uses GlobalImages::setPageSize()
     * @uses GlobalImages::setTotalPages()
     * @uses GlobalImages::setTotalImages()
     * @param \ID3Global\Arrays\ArrayOfGlobalImageDetails $images
     * @param int $pageSize
     * @param int $totalPages
     * @param int $totalImages
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalImageDetails $images = null, ?int $pageSize = null, ?int $totalPages = null, ?int $totalImages = null)
    {
        $this
            ->setImages($images)
            ->setPageSize($pageSize)
            ->setTotalPages($totalPages)
            ->setTotalImages($totalImages);
    }
    /**
     * Get Images value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalImageDetails|null
     */
    public function getImages(): ?\ID3Global\Arrays\ArrayOfGlobalImageDetails
    {
        return isset($this->Images) ? $this->Images : null;
    }
    /**
     * Set Images value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalImageDetails $images
     * @return \ID3Global\Models\GlobalImages
     */
    public function setImages(?\ID3Global\Arrays\ArrayOfGlobalImageDetails $images = null): self
    {
        if (is_null($images) || (is_array($images) && empty($images))) {
            unset($this->Images);
        } else {
            $this->Images = $images;
        }
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GlobalImages
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get TotalPages value
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->TotalPages;
    }
    /**
     * Set TotalPages value
     * @param int $totalPages
     * @return \ID3Global\Models\GlobalImages
     */
    public function setTotalPages(?int $totalPages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPages) && !(is_int($totalPages) || ctype_digit($totalPages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPages, true), gettype($totalPages)), __LINE__);
        }
        $this->TotalPages = $totalPages;
        
        return $this;
    }
    /**
     * Get TotalImages value
     * @return int|null
     */
    public function getTotalImages(): ?int
    {
        return $this->TotalImages;
    }
    /**
     * Set TotalImages value
     * @param int $totalImages
     * @return \ID3Global\Models\GlobalImages
     */
    public function setTotalImages(?int $totalImages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalImages) && !(is_int($totalImages) || ctype_digit($totalImages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalImages, true), gettype($totalImages)), __LINE__);
        }
        $this->TotalImages = $totalImages;
        
        return $this;
    }
}
