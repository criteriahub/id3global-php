<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetReport Models
 * @subpackage Structs
 */
class GetReport extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The Path
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Path = null;
    /**
     * The Format
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Format = null;
    /**
     * The StreamRoot
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $StreamRoot = null;
    /**
     * The PageRoot
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $PageRoot = null;
    /**
     * The Parameters
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalReportParameter|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalReportParameter $Parameters = null;
    /**
     * The PageNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageNumber = null;
    /**
     * Constructor method for GetReport
     * @uses GetReport::setOrgID()
     * @uses GetReport::setPath()
     * @uses GetReport::setFormat()
     * @uses GetReport::setStreamRoot()
     * @uses GetReport::setPageRoot()
     * @uses GetReport::setParameters()
     * @uses GetReport::setPageNumber()
     * @param string $orgID
     * @param string $path
     * @param string $format
     * @param string $streamRoot
     * @param string $pageRoot
     * @param \ID3Global\Arrays\ArrayOfGlobalReportParameter $parameters
     * @param int $pageNumber
     */
    public function __construct(?string $orgID = null, ?string $path = null, ?string $format = null, ?string $streamRoot = null, ?string $pageRoot = null, ?\ID3Global\Arrays\ArrayOfGlobalReportParameter $parameters = null, ?int $pageNumber = null)
    {
        $this
            ->setOrgID($orgID)
            ->setPath($path)
            ->setFormat($format)
            ->setStreamRoot($streamRoot)
            ->setPageRoot($pageRoot)
            ->setParameters($parameters)
            ->setPageNumber($pageNumber);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetReport
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get Path value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPath(): ?string
    {
        return isset($this->Path) ? $this->Path : null;
    }
    /**
     * Set Path value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $path
     * @return \ID3Global\Models\GetReport
     */
    public function setPath(?string $path = null): self
    {
        // validation for constraint: string
        if (!is_null($path) && !is_string($path)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($path, true), gettype($path)), __LINE__);
        }
        if (is_null($path) || (is_array($path) && empty($path))) {
            unset($this->Path);
        } else {
            $this->Path = $path;
        }
        
        return $this;
    }
    /**
     * Get Format value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return isset($this->Format) ? $this->Format : null;
    }
    /**
     * Set Format value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $format
     * @return \ID3Global\Models\GetReport
     */
    public function setFormat(?string $format = null): self
    {
        // validation for constraint: string
        if (!is_null($format) && !is_string($format)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($format, true), gettype($format)), __LINE__);
        }
        if (is_null($format) || (is_array($format) && empty($format))) {
            unset($this->Format);
        } else {
            $this->Format = $format;
        }
        
        return $this;
    }
    /**
     * Get StreamRoot value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStreamRoot(): ?string
    {
        return isset($this->StreamRoot) ? $this->StreamRoot : null;
    }
    /**
     * Set StreamRoot value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $streamRoot
     * @return \ID3Global\Models\GetReport
     */
    public function setStreamRoot(?string $streamRoot = null): self
    {
        // validation for constraint: string
        if (!is_null($streamRoot) && !is_string($streamRoot)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($streamRoot, true), gettype($streamRoot)), __LINE__);
        }
        if (is_null($streamRoot) || (is_array($streamRoot) && empty($streamRoot))) {
            unset($this->StreamRoot);
        } else {
            $this->StreamRoot = $streamRoot;
        }
        
        return $this;
    }
    /**
     * Get PageRoot value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPageRoot(): ?string
    {
        return isset($this->PageRoot) ? $this->PageRoot : null;
    }
    /**
     * Set PageRoot value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $pageRoot
     * @return \ID3Global\Models\GetReport
     */
    public function setPageRoot(?string $pageRoot = null): self
    {
        // validation for constraint: string
        if (!is_null($pageRoot) && !is_string($pageRoot)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pageRoot, true), gettype($pageRoot)), __LINE__);
        }
        if (is_null($pageRoot) || (is_array($pageRoot) && empty($pageRoot))) {
            unset($this->PageRoot);
        } else {
            $this->PageRoot = $pageRoot;
        }
        
        return $this;
    }
    /**
     * Get Parameters value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalReportParameter|null
     */
    public function getParameters(): ?\ID3Global\Arrays\ArrayOfGlobalReportParameter
    {
        return isset($this->Parameters) ? $this->Parameters : null;
    }
    /**
     * Set Parameters value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalReportParameter $parameters
     * @return \ID3Global\Models\GetReport
     */
    public function setParameters(?\ID3Global\Arrays\ArrayOfGlobalReportParameter $parameters = null): self
    {
        if (is_null($parameters) || (is_array($parameters) && empty($parameters))) {
            unset($this->Parameters);
        } else {
            $this->Parameters = $parameters;
        }
        
        return $this;
    }
    /**
     * Get PageNumber value
     * @return int|null
     */
    public function getPageNumber(): ?int
    {
        return $this->PageNumber;
    }
    /**
     * Set PageNumber value
     * @param int $pageNumber
     * @return \ID3Global\Models\GetReport
     */
    public function setPageNumber(?int $pageNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($pageNumber) && !(is_int($pageNumber) || ctype_digit($pageNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageNumber, true), gettype($pageNumber)), __LINE__);
        }
        $this->PageNumber = $pageNumber;
        
        return $this;
    }
}
