<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAccountRolesResponse Models
 * @subpackage Structs
 */
class GetAccountRolesResponse extends AbstractStructBase
{
    /**
     * The GetAccountRolesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalRoles|null
     */
    protected ?\ID3Global\Models\GlobalRoles $GetAccountRolesResult = null;
    /**
     * Constructor method for GetAccountRolesResponse
     * @uses GetAccountRolesResponse::setGetAccountRolesResult()
     * @param \ID3Global\Models\GlobalRoles $getAccountRolesResult
     */
    public function __construct(?\ID3Global\Models\GlobalRoles $getAccountRolesResult = null)
    {
        $this
            ->setGetAccountRolesResult($getAccountRolesResult);
    }
    /**
     * Get GetAccountRolesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalRoles|null
     */
    public function getGetAccountRolesResult(): ?\ID3Global\Models\GlobalRoles
    {
        return isset($this->GetAccountRolesResult) ? $this->GetAccountRolesResult : null;
    }
    /**
     * Set GetAccountRolesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalRoles $getAccountRolesResult
     * @return \ID3Global\Models\GetAccountRolesResponse
     */
    public function setGetAccountRolesResult(?\ID3Global\Models\GlobalRoles $getAccountRolesResult = null): self
    {
        if (is_null($getAccountRolesResult) || (is_array($getAccountRolesResult) && empty($getAccountRolesResult))) {
            unset($this->GetAccountRolesResult);
        } else {
            $this->GetAccountRolesResult = $getAccountRolesResult;
        }
        
        return $this;
    }
}
