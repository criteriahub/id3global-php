<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalItemCheckResultCode Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q38:GlobalItemCheckResultCode
 * @subpackage Structs
 */
class GlobalItemCheckResultCode extends AbstractStructBase
{
    /**
     * The Description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Description = null;
    /**
     * The Code
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Code = null;
    /**
     * The Override
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Override = null;
    /**
     * Constructor method for GlobalItemCheckResultCode
     * @uses GlobalItemCheckResultCode::setDescription()
     * @uses GlobalItemCheckResultCode::setCode()
     * @uses GlobalItemCheckResultCode::setOverride()
     * @param string $description
     * @param int $code
     * @param string $override
     */
    public function __construct(?string $description = null, ?int $code = null, ?string $override = null)
    {
        $this
            ->setDescription($description)
            ->setCode($code)
            ->setOverride($override);
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \ID3Global\Models\GlobalItemCheckResultCode
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        
        return $this;
    }
    /**
     * Get Code value
     * @return int|null
     */
    public function getCode(): ?int
    {
        return $this->Code;
    }
    /**
     * Set Code value
     * @param int $code
     * @return \ID3Global\Models\GlobalItemCheckResultCode
     */
    public function setCode(?int $code = null): self
    {
        // validation for constraint: int
        if (!is_null($code) && !(is_int($code) || ctype_digit($code))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        $this->Code = $code;
        
        return $this;
    }
    /**
     * Get Override value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOverride(): ?string
    {
        return isset($this->Override) ? $this->Override : null;
    }
    /**
     * Set Override value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $override
     * @return \ID3Global\Models\GlobalItemCheckResultCode
     */
    public function setOverride(?string $override = null): self
    {
        // validation for constraint: string
        if (!is_null($override) && !is_string($override)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($override, true), gettype($override)), __LINE__);
        }
        if (is_null($override) || (is_array($override) && empty($override))) {
            unset($this->Override);
        } else {
            $this->Override = $override;
        }
        
        return $this;
    }
}
