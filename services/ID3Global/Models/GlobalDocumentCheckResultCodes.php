<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDocumentCheckResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q59:GlobalDocumentCheckResultCodes
 * @subpackage Structs
 */
class GlobalDocumentCheckResultCodes extends GlobalItemCheckResultCodes
{
    /**
     * The DrivingLicenceFraudDocumentReferences
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $DrivingLicenceFraudDocumentReferences = null;
    /**
     * The PassportFraudDocumentReferences
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $PassportFraudDocumentReferences = null;
    /**
     * Constructor method for GlobalDocumentCheckResultCodes
     * @uses GlobalDocumentCheckResultCodes::setDrivingLicenceFraudDocumentReferences()
     * @uses GlobalDocumentCheckResultCodes::setPassportFraudDocumentReferences()
     * @param \ID3Global\Arrays\ArrayOfstring $drivingLicenceFraudDocumentReferences
     * @param \ID3Global\Arrays\ArrayOfstring $passportFraudDocumentReferences
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfstring $drivingLicenceFraudDocumentReferences = null, ?\ID3Global\Arrays\ArrayOfstring $passportFraudDocumentReferences = null)
    {
        $this
            ->setDrivingLicenceFraudDocumentReferences($drivingLicenceFraudDocumentReferences)
            ->setPassportFraudDocumentReferences($passportFraudDocumentReferences);
    }
    /**
     * Get DrivingLicenceFraudDocumentReferences value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getDrivingLicenceFraudDocumentReferences(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->DrivingLicenceFraudDocumentReferences) ? $this->DrivingLicenceFraudDocumentReferences : null;
    }
    /**
     * Set DrivingLicenceFraudDocumentReferences value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $drivingLicenceFraudDocumentReferences
     * @return \ID3Global\Models\GlobalDocumentCheckResultCodes
     */
    public function setDrivingLicenceFraudDocumentReferences(?\ID3Global\Arrays\ArrayOfstring $drivingLicenceFraudDocumentReferences = null): self
    {
        if (is_null($drivingLicenceFraudDocumentReferences) || (is_array($drivingLicenceFraudDocumentReferences) && empty($drivingLicenceFraudDocumentReferences))) {
            unset($this->DrivingLicenceFraudDocumentReferences);
        } else {
            $this->DrivingLicenceFraudDocumentReferences = $drivingLicenceFraudDocumentReferences;
        }
        
        return $this;
    }
    /**
     * Get PassportFraudDocumentReferences value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getPassportFraudDocumentReferences(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->PassportFraudDocumentReferences) ? $this->PassportFraudDocumentReferences : null;
    }
    /**
     * Set PassportFraudDocumentReferences value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $passportFraudDocumentReferences
     * @return \ID3Global\Models\GlobalDocumentCheckResultCodes
     */
    public function setPassportFraudDocumentReferences(?\ID3Global\Arrays\ArrayOfstring $passportFraudDocumentReferences = null): self
    {
        if (is_null($passportFraudDocumentReferences) || (is_array($passportFraudDocumentReferences) && empty($passportFraudDocumentReferences))) {
            unset($this->PassportFraudDocumentReferences);
        } else {
            $this->PassportFraudDocumentReferences = $passportFraudDocumentReferences;
        }
        
        return $this;
    }
}
