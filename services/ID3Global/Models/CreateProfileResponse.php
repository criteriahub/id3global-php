<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateProfileResponse Models
 * @subpackage Structs
 */
class CreateProfileResponse extends AbstractStructBase
{
    /**
     * The CreateProfileResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileDetails|null
     */
    protected ?\ID3Global\Models\GlobalProfileDetails $CreateProfileResult = null;
    /**
     * Constructor method for CreateProfileResponse
     * @uses CreateProfileResponse::setCreateProfileResult()
     * @param \ID3Global\Models\GlobalProfileDetails $createProfileResult
     */
    public function __construct(?\ID3Global\Models\GlobalProfileDetails $createProfileResult = null)
    {
        $this
            ->setCreateProfileResult($createProfileResult);
    }
    /**
     * Get CreateProfileResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileDetails|null
     */
    public function getCreateProfileResult(): ?\ID3Global\Models\GlobalProfileDetails
    {
        return isset($this->CreateProfileResult) ? $this->CreateProfileResult : null;
    }
    /**
     * Set CreateProfileResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileDetails $createProfileResult
     * @return \ID3Global\Models\CreateProfileResponse
     */
    public function setCreateProfileResult(?\ID3Global\Models\GlobalProfileDetails $createProfileResult = null): self
    {
        if (is_null($createProfileResult) || (is_array($createProfileResult) && empty($createProfileResult))) {
            unset($this->CreateProfileResult);
        } else {
            $this->CreateProfileResult = $createProfileResult;
        }
        
        return $this;
    }
}
