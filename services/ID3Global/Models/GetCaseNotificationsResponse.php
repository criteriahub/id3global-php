<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCaseNotificationsResponse Models
 * @subpackage Structs
 */
class GetCaseNotificationsResponse extends AbstractStructBase
{
    /**
     * The GetCaseNotificationsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseNotifications|null
     */
    protected ?\ID3Global\Models\GlobalCaseNotifications $GetCaseNotificationsResult = null;
    /**
     * Constructor method for GetCaseNotificationsResponse
     * @uses GetCaseNotificationsResponse::setGetCaseNotificationsResult()
     * @param \ID3Global\Models\GlobalCaseNotifications $getCaseNotificationsResult
     */
    public function __construct(?\ID3Global\Models\GlobalCaseNotifications $getCaseNotificationsResult = null)
    {
        $this
            ->setGetCaseNotificationsResult($getCaseNotificationsResult);
    }
    /**
     * Get GetCaseNotificationsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseNotifications|null
     */
    public function getGetCaseNotificationsResult(): ?\ID3Global\Models\GlobalCaseNotifications
    {
        return isset($this->GetCaseNotificationsResult) ? $this->GetCaseNotificationsResult : null;
    }
    /**
     * Set GetCaseNotificationsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCaseNotifications $getCaseNotificationsResult
     * @return \ID3Global\Models\GetCaseNotificationsResponse
     */
    public function setGetCaseNotificationsResult(?\ID3Global\Models\GlobalCaseNotifications $getCaseNotificationsResult = null): self
    {
        if (is_null($getCaseNotificationsResult) || (is_array($getCaseNotificationsResult) && empty($getCaseNotificationsResult))) {
            unset($this->GetCaseNotificationsResult);
        } else {
            $this->GetCaseNotificationsResult = $getCaseNotificationsResult;
        }
        
        return $this;
    }
}
