<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDispatchResult Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q774:GlobalDispatchResult
 * @subpackage Structs
 */
class GlobalDispatchResult extends AbstractStructBase
{
    /**
     * The DVLADrivingLicenceReport
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDVLADrivingLicenceReport|null
     */
    protected ?\ID3Global\Models\GlobalDVLADrivingLicenceReport $DVLADrivingLicenceReport = null;
    /**
     * The CriminalRecordCheckDisclosure
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCriminalRecordCheckDisclosure|null
     */
    protected ?\ID3Global\Models\GlobalCriminalRecordCheckDisclosure $CriminalRecordCheckDisclosure = null;
    /**
     * Constructor method for GlobalDispatchResult
     * @uses GlobalDispatchResult::setDVLADrivingLicenceReport()
     * @uses GlobalDispatchResult::setCriminalRecordCheckDisclosure()
     * @param \ID3Global\Models\GlobalDVLADrivingLicenceReport $dVLADrivingLicenceReport
     * @param \ID3Global\Models\GlobalCriminalRecordCheckDisclosure $criminalRecordCheckDisclosure
     */
    public function __construct(?\ID3Global\Models\GlobalDVLADrivingLicenceReport $dVLADrivingLicenceReport = null, ?\ID3Global\Models\GlobalCriminalRecordCheckDisclosure $criminalRecordCheckDisclosure = null)
    {
        $this
            ->setDVLADrivingLicenceReport($dVLADrivingLicenceReport)
            ->setCriminalRecordCheckDisclosure($criminalRecordCheckDisclosure);
    }
    /**
     * Get DVLADrivingLicenceReport value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport|null
     */
    public function getDVLADrivingLicenceReport(): ?\ID3Global\Models\GlobalDVLADrivingLicenceReport
    {
        return isset($this->DVLADrivingLicenceReport) ? $this->DVLADrivingLicenceReport : null;
    }
    /**
     * Set DVLADrivingLicenceReport value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalDVLADrivingLicenceReport $dVLADrivingLicenceReport
     * @return \ID3Global\Models\GlobalDispatchResult
     */
    public function setDVLADrivingLicenceReport(?\ID3Global\Models\GlobalDVLADrivingLicenceReport $dVLADrivingLicenceReport = null): self
    {
        if (is_null($dVLADrivingLicenceReport) || (is_array($dVLADrivingLicenceReport) && empty($dVLADrivingLicenceReport))) {
            unset($this->DVLADrivingLicenceReport);
        } else {
            $this->DVLADrivingLicenceReport = $dVLADrivingLicenceReport;
        }
        
        return $this;
    }
    /**
     * Get CriminalRecordCheckDisclosure value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCriminalRecordCheckDisclosure|null
     */
    public function getCriminalRecordCheckDisclosure(): ?\ID3Global\Models\GlobalCriminalRecordCheckDisclosure
    {
        return isset($this->CriminalRecordCheckDisclosure) ? $this->CriminalRecordCheckDisclosure : null;
    }
    /**
     * Set CriminalRecordCheckDisclosure value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCriminalRecordCheckDisclosure $criminalRecordCheckDisclosure
     * @return \ID3Global\Models\GlobalDispatchResult
     */
    public function setCriminalRecordCheckDisclosure(?\ID3Global\Models\GlobalCriminalRecordCheckDisclosure $criminalRecordCheckDisclosure = null): self
    {
        if (is_null($criminalRecordCheckDisclosure) || (is_array($criminalRecordCheckDisclosure) && empty($criminalRecordCheckDisclosure))) {
            unset($this->CriminalRecordCheckDisclosure);
        } else {
            $this->CriminalRecordCheckDisclosure = $criminalRecordCheckDisclosure;
        }
        
        return $this;
    }
}
