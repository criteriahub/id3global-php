<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCountryFlag Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q627:GlobalCountryFlag
 * @subpackage Structs
 */
class GlobalCountryFlag extends AbstractStructBase
{
    /**
     * The ISOCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ISOCode = null;
    /**
     * The FlagImageBase64
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $FlagImageBase64 = null;
    /**
     * Constructor method for GlobalCountryFlag
     * @uses GlobalCountryFlag::setISOCode()
     * @uses GlobalCountryFlag::setFlagImageBase64()
     * @param string $iSOCode
     * @param string $flagImageBase64
     */
    public function __construct(?string $iSOCode = null, ?string $flagImageBase64 = null)
    {
        $this
            ->setISOCode($iSOCode)
            ->setFlagImageBase64($flagImageBase64);
    }
    /**
     * Get ISOCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getISOCode(): ?string
    {
        return isset($this->ISOCode) ? $this->ISOCode : null;
    }
    /**
     * Set ISOCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $iSOCode
     * @return \ID3Global\Models\GlobalCountryFlag
     */
    public function setISOCode(?string $iSOCode = null): self
    {
        // validation for constraint: string
        if (!is_null($iSOCode) && !is_string($iSOCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iSOCode, true), gettype($iSOCode)), __LINE__);
        }
        if (is_null($iSOCode) || (is_array($iSOCode) && empty($iSOCode))) {
            unset($this->ISOCode);
        } else {
            $this->ISOCode = $iSOCode;
        }
        
        return $this;
    }
    /**
     * Get FlagImageBase64 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFlagImageBase64(): ?string
    {
        return isset($this->FlagImageBase64) ? $this->FlagImageBase64 : null;
    }
    /**
     * Set FlagImageBase64 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $flagImageBase64
     * @return \ID3Global\Models\GlobalCountryFlag
     */
    public function setFlagImageBase64(?string $flagImageBase64 = null): self
    {
        // validation for constraint: string
        if (!is_null($flagImageBase64) && !is_string($flagImageBase64)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($flagImageBase64, true), gettype($flagImageBase64)), __LINE__);
        }
        if (is_null($flagImageBase64) || (is_array($flagImageBase64) && empty($flagImageBase64))) {
            unset($this->FlagImageBase64);
        } else {
            $this->FlagImageBase64 = $flagImageBase64;
        }
        
        return $this;
    }
}
