<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAddressDocuments Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q402:GlobalAddressDocuments
 * @subpackage Structs
 */
class GlobalAddressDocuments extends AbstractStructBase
{
    /**
     * The UKAddressDocuments
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalUKAddressDocuments|null
     */
    protected ?\ID3Global\Models\GlobalUKAddressDocuments $UKAddressDocuments = null;
    /**
     * Constructor method for GlobalAddressDocuments
     * @uses GlobalAddressDocuments::setUKAddressDocuments()
     * @param \ID3Global\Models\GlobalUKAddressDocuments $uKAddressDocuments
     */
    public function __construct(?\ID3Global\Models\GlobalUKAddressDocuments $uKAddressDocuments = null)
    {
        $this
            ->setUKAddressDocuments($uKAddressDocuments);
    }
    /**
     * Get UKAddressDocuments value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalUKAddressDocuments|null
     */
    public function getUKAddressDocuments(): ?\ID3Global\Models\GlobalUKAddressDocuments
    {
        return isset($this->UKAddressDocuments) ? $this->UKAddressDocuments : null;
    }
    /**
     * Set UKAddressDocuments value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalUKAddressDocuments $uKAddressDocuments
     * @return \ID3Global\Models\GlobalAddressDocuments
     */
    public function setUKAddressDocuments(?\ID3Global\Models\GlobalUKAddressDocuments $uKAddressDocuments = null): self
    {
        if (is_null($uKAddressDocuments) || (is_array($uKAddressDocuments) && empty($uKAddressDocuments))) {
            unset($this->UKAddressDocuments);
        } else {
            $this->UKAddressDocuments = $uKAddressDocuments;
        }
        
        return $this;
    }
}
