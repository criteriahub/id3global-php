<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAustriaArvatoAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q699:GlobalAustriaArvatoAccount
 * @subpackage Structs
 */
class GlobalAustriaArvatoAccount extends GlobalSupplierAccount
{
    /**
     * The ClientID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ClientID = null;
    /**
     * The SubClientId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SubClientId = null;
    /**
     * Constructor method for GlobalAustriaArvatoAccount
     * @uses GlobalAustriaArvatoAccount::setClientID()
     * @uses GlobalAustriaArvatoAccount::setSubClientId()
     * @param string $clientID
     * @param string $subClientId
     */
    public function __construct(?string $clientID = null, ?string $subClientId = null)
    {
        $this
            ->setClientID($clientID)
            ->setSubClientId($subClientId);
    }
    /**
     * Get ClientID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getClientID(): ?string
    {
        return isset($this->ClientID) ? $this->ClientID : null;
    }
    /**
     * Set ClientID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $clientID
     * @return \ID3Global\Models\GlobalAustriaArvatoAccount
     */
    public function setClientID(?string $clientID = null): self
    {
        // validation for constraint: string
        if (!is_null($clientID) && !is_string($clientID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($clientID, true), gettype($clientID)), __LINE__);
        }
        if (is_null($clientID) || (is_array($clientID) && empty($clientID))) {
            unset($this->ClientID);
        } else {
            $this->ClientID = $clientID;
        }
        
        return $this;
    }
    /**
     * Get SubClientId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSubClientId(): ?string
    {
        return isset($this->SubClientId) ? $this->SubClientId : null;
    }
    /**
     * Set SubClientId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $subClientId
     * @return \ID3Global\Models\GlobalAustriaArvatoAccount
     */
    public function setSubClientId(?string $subClientId = null): self
    {
        // validation for constraint: string
        if (!is_null($subClientId) && !is_string($subClientId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subClientId, true), gettype($subClientId)), __LINE__);
        }
        if (is_null($subClientId) || (is_array($subClientId) && empty($subClientId))) {
            unset($this->SubClientId);
        } else {
            $this->SubClientId = $subClientId;
        }
        
        return $this;
    }
}
