<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalConditionScore Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q228:GlobalConditionScore
 * @subpackage Structs
 */
class GlobalConditionScore extends AbstractStructBase
{
    /**
     * The ScoreTargetVal
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ScoreTargetVal = null;
    /**
     * The TrueIfGTEQ
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $TrueIfGTEQ = null;
    /**
     * Constructor method for GlobalConditionScore
     * @uses GlobalConditionScore::setScoreTargetVal()
     * @uses GlobalConditionScore::setTrueIfGTEQ()
     * @param int $scoreTargetVal
     * @param bool $trueIfGTEQ
     */
    public function __construct(?int $scoreTargetVal = null, ?bool $trueIfGTEQ = null)
    {
        $this
            ->setScoreTargetVal($scoreTargetVal)
            ->setTrueIfGTEQ($trueIfGTEQ);
    }
    /**
     * Get ScoreTargetVal value
     * @return int|null
     */
    public function getScoreTargetVal(): ?int
    {
        return $this->ScoreTargetVal;
    }
    /**
     * Set ScoreTargetVal value
     * @param int $scoreTargetVal
     * @return \ID3Global\Models\GlobalConditionScore
     */
    public function setScoreTargetVal(?int $scoreTargetVal = null): self
    {
        // validation for constraint: int
        if (!is_null($scoreTargetVal) && !(is_int($scoreTargetVal) || ctype_digit($scoreTargetVal))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($scoreTargetVal, true), gettype($scoreTargetVal)), __LINE__);
        }
        $this->ScoreTargetVal = $scoreTargetVal;
        
        return $this;
    }
    /**
     * Get TrueIfGTEQ value
     * @return bool|null
     */
    public function getTrueIfGTEQ(): ?bool
    {
        return $this->TrueIfGTEQ;
    }
    /**
     * Set TrueIfGTEQ value
     * @param bool $trueIfGTEQ
     * @return \ID3Global\Models\GlobalConditionScore
     */
    public function setTrueIfGTEQ(?bool $trueIfGTEQ = null): self
    {
        // validation for constraint: boolean
        if (!is_null($trueIfGTEQ) && !is_bool($trueIfGTEQ)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($trueIfGTEQ, true), gettype($trueIfGTEQ)), __LINE__);
        }
        $this->TrueIfGTEQ = $trueIfGTEQ;
        
        return $this;
    }
}
