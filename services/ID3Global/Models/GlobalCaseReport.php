<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseReport Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q801:GlobalCaseReport
 * @subpackage Structs
 */
class GlobalCaseReport extends AbstractStructBase
{
    /**
     * The Profile
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileIDVersion|null
     */
    protected ?\ID3Global\Models\GlobalProfileIDVersion $Profile = null;
    /**
     * The AuthenticationID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AuthenticationID = null;
    /**
     * The Result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalMatrixResult|null
     */
    protected ?\ID3Global\Models\GlobalMatrixResult $Result = null;
    /**
     * Constructor method for GlobalCaseReport
     * @uses GlobalCaseReport::setProfile()
     * @uses GlobalCaseReport::setAuthenticationID()
     * @uses GlobalCaseReport::setResult()
     * @param \ID3Global\Models\GlobalProfileIDVersion $profile
     * @param string $authenticationID
     * @param \ID3Global\Models\GlobalMatrixResult $result
     */
    public function __construct(?\ID3Global\Models\GlobalProfileIDVersion $profile = null, ?string $authenticationID = null, ?\ID3Global\Models\GlobalMatrixResult $result = null)
    {
        $this
            ->setProfile($profile)
            ->setAuthenticationID($authenticationID)
            ->setResult($result);
    }
    /**
     * Get Profile value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileIDVersion|null
     */
    public function getProfile(): ?\ID3Global\Models\GlobalProfileIDVersion
    {
        return isset($this->Profile) ? $this->Profile : null;
    }
    /**
     * Set Profile value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileIDVersion $profile
     * @return \ID3Global\Models\GlobalCaseReport
     */
    public function setProfile(?\ID3Global\Models\GlobalProfileIDVersion $profile = null): self
    {
        if (is_null($profile) || (is_array($profile) && empty($profile))) {
            unset($this->Profile);
        } else {
            $this->Profile = $profile;
        }
        
        return $this;
    }
    /**
     * Get AuthenticationID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAuthenticationID(): ?string
    {
        return isset($this->AuthenticationID) ? $this->AuthenticationID : null;
    }
    /**
     * Set AuthenticationID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $authenticationID
     * @return \ID3Global\Models\GlobalCaseReport
     */
    public function setAuthenticationID(?string $authenticationID = null): self
    {
        // validation for constraint: string
        if (!is_null($authenticationID) && !is_string($authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authenticationID, true), gettype($authenticationID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($authenticationID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($authenticationID, true)), __LINE__);
        }
        if (is_null($authenticationID) || (is_array($authenticationID) && empty($authenticationID))) {
            unset($this->AuthenticationID);
        } else {
            $this->AuthenticationID = $authenticationID;
        }
        
        return $this;
    }
    /**
     * Get Result value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalMatrixResult|null
     */
    public function getResult(): ?\ID3Global\Models\GlobalMatrixResult
    {
        return isset($this->Result) ? $this->Result : null;
    }
    /**
     * Set Result value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalMatrixResult $result
     * @return \ID3Global\Models\GlobalCaseReport
     */
    public function setResult(?\ID3Global\Models\GlobalMatrixResult $result = null): self
    {
        if (is_null($result) || (is_array($result) && empty($result))) {
            unset($this->Result);
        } else {
            $this->Result = $result;
        }
        
        return $this;
    }
}
