<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCreditMatch Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q74:GlobalCreditMatch
 * @subpackage Structs
 */
class GlobalCreditMatch extends AbstractStructBase
{
    /**
     * The GlobalCreditMatches
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCreditMatches|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCreditMatches $GlobalCreditMatches = null;
    /**
     * Constructor method for GlobalCreditMatch
     * @uses GlobalCreditMatch::setGlobalCreditMatches()
     * @param \ID3Global\Arrays\ArrayOfGlobalCreditMatches $globalCreditMatches
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCreditMatches $globalCreditMatches = null)
    {
        $this
            ->setGlobalCreditMatches($globalCreditMatches);
    }
    /**
     * Get GlobalCreditMatches value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCreditMatches|null
     */
    public function getGlobalCreditMatches(): ?\ID3Global\Arrays\ArrayOfGlobalCreditMatches
    {
        return isset($this->GlobalCreditMatches) ? $this->GlobalCreditMatches : null;
    }
    /**
     * Set GlobalCreditMatches value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCreditMatches $globalCreditMatches
     * @return \ID3Global\Models\GlobalCreditMatch
     */
    public function setGlobalCreditMatches(?\ID3Global\Arrays\ArrayOfGlobalCreditMatches $globalCreditMatches = null): self
    {
        if (is_null($globalCreditMatches) || (is_array($globalCreditMatches) && empty($globalCreditMatches))) {
            unset($this->GlobalCreditMatches);
        } else {
            $this->GlobalCreditMatches = $globalCreditMatches;
        }
        
        return $this;
    }
}
