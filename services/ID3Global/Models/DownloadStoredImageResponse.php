<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DownloadStoredImageResponse Models
 * @subpackage Structs
 */
class DownloadStoredImageResponse extends AbstractStructBase
{
    /**
     * The DownloadStoredImageResult
     * Meta information extracted from the WSDL
     * - base: xs:base64Binary
     * @var string|null
     */
    protected ?string $DownloadStoredImageResult = null;
    /**
     * Constructor method for DownloadStoredImageResponse
     * @uses DownloadStoredImageResponse::setDownloadStoredImageResult()
     * @param string $downloadStoredImageResult
     */
    public function __construct(?string $downloadStoredImageResult = null)
    {
        $this
            ->setDownloadStoredImageResult($downloadStoredImageResult);
    }
    /**
     * Get DownloadStoredImageResult value
     * @return string|null
     */
    public function getDownloadStoredImageResult(): ?string
    {
        return $this->DownloadStoredImageResult;
    }
    /**
     * Set DownloadStoredImageResult value
     * @param string $downloadStoredImageResult
     * @return \ID3Global\Models\DownloadStoredImageResponse
     */
    public function setDownloadStoredImageResult(?string $downloadStoredImageResult = null): self
    {
        // validation for constraint: string
        if (!is_null($downloadStoredImageResult) && !is_string($downloadStoredImageResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($downloadStoredImageResult, true), gettype($downloadStoredImageResult)), __LINE__);
        }
        $this->DownloadStoredImageResult = $downloadStoredImageResult;
        
        return $this;
    }
}
