<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAccountExtractsResponse Models
 * @subpackage Structs
 */
class GetAccountExtractsResponse extends AbstractStructBase
{
    /**
     * The GetAccountExtractsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\ExtractDetails|null
     */
    protected ?\ID3Global\Models\ExtractDetails $GetAccountExtractsResult = null;
    /**
     * Constructor method for GetAccountExtractsResponse
     * @uses GetAccountExtractsResponse::setGetAccountExtractsResult()
     * @param \ID3Global\Models\ExtractDetails $getAccountExtractsResult
     */
    public function __construct(?\ID3Global\Models\ExtractDetails $getAccountExtractsResult = null)
    {
        $this
            ->setGetAccountExtractsResult($getAccountExtractsResult);
    }
    /**
     * Get GetAccountExtractsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\ExtractDetails|null
     */
    public function getGetAccountExtractsResult(): ?\ID3Global\Models\ExtractDetails
    {
        return isset($this->GetAccountExtractsResult) ? $this->GetAccountExtractsResult : null;
    }
    /**
     * Set GetAccountExtractsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\ExtractDetails $getAccountExtractsResult
     * @return \ID3Global\Models\GetAccountExtractsResponse
     */
    public function setGetAccountExtractsResult(?\ID3Global\Models\ExtractDetails $getAccountExtractsResult = null): self
    {
        if (is_null($getAccountExtractsResult) || (is_array($getAccountExtractsResult) && empty($getAccountExtractsResult))) {
            unset($this->GetAccountExtractsResult);
        } else {
            $this->GetAccountExtractsResult = $getAccountExtractsResult;
        }
        
        return $this;
    }
}
