<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalHome Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q571:GlobalHome
 * @subpackage Structs
 */
class GlobalHome extends AbstractStructBase
{
    /**
     * The OrgName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OrgName = null;
    /**
     * The SupportContact
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupportContact|null
     */
    protected ?\ID3Global\Models\GlobalSupportContact $SupportContact = null;
    /**
     * The Resources
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalResource|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalResource $Resources = null;
    /**
     * Constructor method for GlobalHome
     * @uses GlobalHome::setOrgName()
     * @uses GlobalHome::setSupportContact()
     * @uses GlobalHome::setResources()
     * @param string $orgName
     * @param \ID3Global\Models\GlobalSupportContact $supportContact
     * @param \ID3Global\Arrays\ArrayOfGlobalResource $resources
     */
    public function __construct(?string $orgName = null, ?\ID3Global\Models\GlobalSupportContact $supportContact = null, ?\ID3Global\Arrays\ArrayOfGlobalResource $resources = null)
    {
        $this
            ->setOrgName($orgName)
            ->setSupportContact($supportContact)
            ->setResources($resources);
    }
    /**
     * Get OrgName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrgName(): ?string
    {
        return isset($this->OrgName) ? $this->OrgName : null;
    }
    /**
     * Set OrgName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $orgName
     * @return \ID3Global\Models\GlobalHome
     */
    public function setOrgName(?string $orgName = null): self
    {
        // validation for constraint: string
        if (!is_null($orgName) && !is_string($orgName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgName, true), gettype($orgName)), __LINE__);
        }
        if (is_null($orgName) || (is_array($orgName) && empty($orgName))) {
            unset($this->OrgName);
        } else {
            $this->OrgName = $orgName;
        }
        
        return $this;
    }
    /**
     * Get SupportContact value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupportContact|null
     */
    public function getSupportContact(): ?\ID3Global\Models\GlobalSupportContact
    {
        return isset($this->SupportContact) ? $this->SupportContact : null;
    }
    /**
     * Set SupportContact value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupportContact $supportContact
     * @return \ID3Global\Models\GlobalHome
     */
    public function setSupportContact(?\ID3Global\Models\GlobalSupportContact $supportContact = null): self
    {
        if (is_null($supportContact) || (is_array($supportContact) && empty($supportContact))) {
            unset($this->SupportContact);
        } else {
            $this->SupportContact = $supportContact;
        }
        
        return $this;
    }
    /**
     * Get Resources value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalResource|null
     */
    public function getResources(): ?\ID3Global\Arrays\ArrayOfGlobalResource
    {
        return isset($this->Resources) ? $this->Resources : null;
    }
    /**
     * Set Resources value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalResource $resources
     * @return \ID3Global\Models\GlobalHome
     */
    public function setResources(?\ID3Global\Arrays\ArrayOfGlobalResource $resources = null): self
    {
        if (is_null($resources) || (is_array($resources) && empty($resources))) {
            unset($this->Resources);
        } else {
            $this->Resources = $resources;
        }
        
        return $this;
    }
}
