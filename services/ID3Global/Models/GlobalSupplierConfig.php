<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSupplierConfig Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q719:GlobalSupplierConfig
 * @subpackage Structs
 */
class GlobalSupplierConfig extends AbstractStructBase
{
    /**
     * The SupplierID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $SupplierID = null;
    /**
     * The SupplierFields
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSupplierField|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSupplierField $SupplierFields = null;
    /**
     * Constructor method for GlobalSupplierConfig
     * @uses GlobalSupplierConfig::setSupplierID()
     * @uses GlobalSupplierConfig::setSupplierFields()
     * @param string $supplierID
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplierField $supplierFields
     */
    public function __construct(?string $supplierID = null, ?\ID3Global\Arrays\ArrayOfGlobalSupplierField $supplierFields = null)
    {
        $this
            ->setSupplierID($supplierID)
            ->setSupplierFields($supplierFields);
    }
    /**
     * Get SupplierID value
     * @return string|null
     */
    public function getSupplierID(): ?string
    {
        return $this->SupplierID;
    }
    /**
     * Set SupplierID value
     * @param string $supplierID
     * @return \ID3Global\Models\GlobalSupplierConfig
     */
    public function setSupplierID(?string $supplierID = null): self
    {
        // validation for constraint: string
        if (!is_null($supplierID) && !is_string($supplierID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($supplierID, true), gettype($supplierID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($supplierID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $supplierID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($supplierID, true)), __LINE__);
        }
        $this->SupplierID = $supplierID;
        
        return $this;
    }
    /**
     * Get SupplierFields value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierField|null
     */
    public function getSupplierFields(): ?\ID3Global\Arrays\ArrayOfGlobalSupplierField
    {
        return isset($this->SupplierFields) ? $this->SupplierFields : null;
    }
    /**
     * Set SupplierFields value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplierField $supplierFields
     * @return \ID3Global\Models\GlobalSupplierConfig
     */
    public function setSupplierFields(?\ID3Global\Arrays\ArrayOfGlobalSupplierField $supplierFields = null): self
    {
        if (is_null($supplierFields) || (is_array($supplierFields) && empty($supplierFields))) {
            unset($this->SupplierFields);
        } else {
            $this->SupplierFields = $supplierFields;
        }
        
        return $this;
    }
}
