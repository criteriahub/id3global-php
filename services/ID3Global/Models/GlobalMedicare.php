<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalMedicare Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q376:GlobalMedicare
 * @subpackage Structs
 */
class GlobalMedicare extends AbstractStructBase
{
    /**
     * The CardNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CardNumber = null;
    /**
     * The ReferenceNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ReferenceNumber = null;
    /**
     * The SurnameAtCitizenship
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SurnameAtCitizenship = null;
    /**
     * The MiddleNameOnCard
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MiddleNameOnCard = null;
    /**
     * The CardColour
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CardColour = null;
    /**
     * The ExpiryDay
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryDay = null;
    /**
     * The ExpiryMonth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryMonth = null;
    /**
     * The ExpiryYear
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryYear = null;
    /**
     * Constructor method for GlobalMedicare
     * @uses GlobalMedicare::setCardNumber()
     * @uses GlobalMedicare::setReferenceNumber()
     * @uses GlobalMedicare::setSurnameAtCitizenship()
     * @uses GlobalMedicare::setMiddleNameOnCard()
     * @uses GlobalMedicare::setCardColour()
     * @uses GlobalMedicare::setExpiryDay()
     * @uses GlobalMedicare::setExpiryMonth()
     * @uses GlobalMedicare::setExpiryYear()
     * @param string $cardNumber
     * @param string $referenceNumber
     * @param string $surnameAtCitizenship
     * @param string $middleNameOnCard
     * @param string $cardColour
     * @param int $expiryDay
     * @param int $expiryMonth
     * @param int $expiryYear
     */
    public function __construct(?string $cardNumber = null, ?string $referenceNumber = null, ?string $surnameAtCitizenship = null, ?string $middleNameOnCard = null, ?string $cardColour = null, ?int $expiryDay = null, ?int $expiryMonth = null, ?int $expiryYear = null)
    {
        $this
            ->setCardNumber($cardNumber)
            ->setReferenceNumber($referenceNumber)
            ->setSurnameAtCitizenship($surnameAtCitizenship)
            ->setMiddleNameOnCard($middleNameOnCard)
            ->setCardColour($cardColour)
            ->setExpiryDay($expiryDay)
            ->setExpiryMonth($expiryMonth)
            ->setExpiryYear($expiryYear);
    }
    /**
     * Get CardNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCardNumber(): ?string
    {
        return isset($this->CardNumber) ? $this->CardNumber : null;
    }
    /**
     * Set CardNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $cardNumber
     * @return \ID3Global\Models\GlobalMedicare
     */
    public function setCardNumber(?string $cardNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($cardNumber) && !is_string($cardNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cardNumber, true), gettype($cardNumber)), __LINE__);
        }
        if (is_null($cardNumber) || (is_array($cardNumber) && empty($cardNumber))) {
            unset($this->CardNumber);
        } else {
            $this->CardNumber = $cardNumber;
        }
        
        return $this;
    }
    /**
     * Get ReferenceNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReferenceNumber(): ?string
    {
        return isset($this->ReferenceNumber) ? $this->ReferenceNumber : null;
    }
    /**
     * Set ReferenceNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $referenceNumber
     * @return \ID3Global\Models\GlobalMedicare
     */
    public function setReferenceNumber(?string $referenceNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($referenceNumber) && !is_string($referenceNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($referenceNumber, true), gettype($referenceNumber)), __LINE__);
        }
        if (is_null($referenceNumber) || (is_array($referenceNumber) && empty($referenceNumber))) {
            unset($this->ReferenceNumber);
        } else {
            $this->ReferenceNumber = $referenceNumber;
        }
        
        return $this;
    }
    /**
     * Get SurnameAtCitizenship value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSurnameAtCitizenship(): ?string
    {
        return isset($this->SurnameAtCitizenship) ? $this->SurnameAtCitizenship : null;
    }
    /**
     * Set SurnameAtCitizenship value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $surnameAtCitizenship
     * @return \ID3Global\Models\GlobalMedicare
     */
    public function setSurnameAtCitizenship(?string $surnameAtCitizenship = null): self
    {
        // validation for constraint: string
        if (!is_null($surnameAtCitizenship) && !is_string($surnameAtCitizenship)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($surnameAtCitizenship, true), gettype($surnameAtCitizenship)), __LINE__);
        }
        if (is_null($surnameAtCitizenship) || (is_array($surnameAtCitizenship) && empty($surnameAtCitizenship))) {
            unset($this->SurnameAtCitizenship);
        } else {
            $this->SurnameAtCitizenship = $surnameAtCitizenship;
        }
        
        return $this;
    }
    /**
     * Get MiddleNameOnCard value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMiddleNameOnCard(): ?string
    {
        return isset($this->MiddleNameOnCard) ? $this->MiddleNameOnCard : null;
    }
    /**
     * Set MiddleNameOnCard value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $middleNameOnCard
     * @return \ID3Global\Models\GlobalMedicare
     */
    public function setMiddleNameOnCard(?string $middleNameOnCard = null): self
    {
        // validation for constraint: string
        if (!is_null($middleNameOnCard) && !is_string($middleNameOnCard)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($middleNameOnCard, true), gettype($middleNameOnCard)), __LINE__);
        }
        if (is_null($middleNameOnCard) || (is_array($middleNameOnCard) && empty($middleNameOnCard))) {
            unset($this->MiddleNameOnCard);
        } else {
            $this->MiddleNameOnCard = $middleNameOnCard;
        }
        
        return $this;
    }
    /**
     * Get CardColour value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCardColour(): ?string
    {
        return isset($this->CardColour) ? $this->CardColour : null;
    }
    /**
     * Set CardColour value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $cardColour
     * @return \ID3Global\Models\GlobalMedicare
     */
    public function setCardColour(?string $cardColour = null): self
    {
        // validation for constraint: string
        if (!is_null($cardColour) && !is_string($cardColour)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cardColour, true), gettype($cardColour)), __LINE__);
        }
        if (is_null($cardColour) || (is_array($cardColour) && empty($cardColour))) {
            unset($this->CardColour);
        } else {
            $this->CardColour = $cardColour;
        }
        
        return $this;
    }
    /**
     * Get ExpiryDay value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryDay(): ?int
    {
        return isset($this->ExpiryDay) ? $this->ExpiryDay : null;
    }
    /**
     * Set ExpiryDay value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryDay
     * @return \ID3Global\Models\GlobalMedicare
     */
    public function setExpiryDay(?int $expiryDay = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryDay) && !(is_int($expiryDay) || ctype_digit($expiryDay))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryDay, true), gettype($expiryDay)), __LINE__);
        }
        if (is_null($expiryDay) || (is_array($expiryDay) && empty($expiryDay))) {
            unset($this->ExpiryDay);
        } else {
            $this->ExpiryDay = $expiryDay;
        }
        
        return $this;
    }
    /**
     * Get ExpiryMonth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryMonth(): ?int
    {
        return isset($this->ExpiryMonth) ? $this->ExpiryMonth : null;
    }
    /**
     * Set ExpiryMonth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryMonth
     * @return \ID3Global\Models\GlobalMedicare
     */
    public function setExpiryMonth(?int $expiryMonth = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryMonth) && !(is_int($expiryMonth) || ctype_digit($expiryMonth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryMonth, true), gettype($expiryMonth)), __LINE__);
        }
        if (is_null($expiryMonth) || (is_array($expiryMonth) && empty($expiryMonth))) {
            unset($this->ExpiryMonth);
        } else {
            $this->ExpiryMonth = $expiryMonth;
        }
        
        return $this;
    }
    /**
     * Get ExpiryYear value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryYear(): ?int
    {
        return isset($this->ExpiryYear) ? $this->ExpiryYear : null;
    }
    /**
     * Set ExpiryYear value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryYear
     * @return \ID3Global\Models\GlobalMedicare
     */
    public function setExpiryYear(?int $expiryYear = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryYear) && !(is_int($expiryYear) || ctype_digit($expiryYear))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryYear, true), gettype($expiryYear)), __LINE__);
        }
        if (is_null($expiryYear) || (is_array($expiryYear) && empty($expiryYear))) {
            unset($this->ExpiryYear);
        } else {
            $this->ExpiryYear = $expiryYear;
        }
        
        return $this;
    }
}
