<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalMatrixResult Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q808:GlobalMatrixResult
 * @subpackage Structs
 */
class GlobalMatrixResult extends AbstractStructBase
{
    /**
     * The AuthenticationId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AuthenticationId = null;
    /**
     * The ProfileId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ProfileId = null;
    /**
     * The Overall
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalMatrixResultItem|null
     */
    protected ?\ID3Global\Models\GlobalMatrixResultItem $Overall = null;
    /**
     * The Summary
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalMatrixResultItem|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalMatrixResultItem $Summary = null;
    /**
     * The OverallFields
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalMatrixResultField|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalMatrixResultField $OverallFields = null;
    /**
     * The CellGroups
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalMatrixCellGroup|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalMatrixCellGroup $CellGroups = null;
    /**
     * The SummaryTemplate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $SummaryTemplate = null;
    /**
     * The Pending
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Pending = null;
    /**
     * The ExternalDataIds
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint $ExternalDataIds = null;
    /**
     * The Date
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Date = null;
    /**
     * Constructor method for GlobalMatrixResult
     * @uses GlobalMatrixResult::setAuthenticationId()
     * @uses GlobalMatrixResult::setProfileId()
     * @uses GlobalMatrixResult::setOverall()
     * @uses GlobalMatrixResult::setSummary()
     * @uses GlobalMatrixResult::setOverallFields()
     * @uses GlobalMatrixResult::setCellGroups()
     * @uses GlobalMatrixResult::setSummaryTemplate()
     * @uses GlobalMatrixResult::setPending()
     * @uses GlobalMatrixResult::setExternalDataIds()
     * @uses GlobalMatrixResult::setDate()
     * @param string $authenticationId
     * @param string $profileId
     * @param \ID3Global\Models\GlobalMatrixResultItem $overall
     * @param \ID3Global\Arrays\ArrayOfGlobalMatrixResultItem $summary
     * @param \ID3Global\Arrays\ArrayOfGlobalMatrixResultField $overallFields
     * @param \ID3Global\Arrays\ArrayOfGlobalMatrixCellGroup $cellGroups
     * @param string $summaryTemplate
     * @param bool $pending
     * @param \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint $externalDataIds
     * @param string $date
     */
    public function __construct(?string $authenticationId = null, ?string $profileId = null, ?\ID3Global\Models\GlobalMatrixResultItem $overall = null, ?\ID3Global\Arrays\ArrayOfGlobalMatrixResultItem $summary = null, ?\ID3Global\Arrays\ArrayOfGlobalMatrixResultField $overallFields = null, ?\ID3Global\Arrays\ArrayOfGlobalMatrixCellGroup $cellGroups = null, ?string $summaryTemplate = null, ?bool $pending = null, ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint $externalDataIds = null, ?string $date = null)
    {
        $this
            ->setAuthenticationId($authenticationId)
            ->setProfileId($profileId)
            ->setOverall($overall)
            ->setSummary($summary)
            ->setOverallFields($overallFields)
            ->setCellGroups($cellGroups)
            ->setSummaryTemplate($summaryTemplate)
            ->setPending($pending)
            ->setExternalDataIds($externalDataIds)
            ->setDate($date);
    }
    /**
     * Get AuthenticationId value
     * @return string|null
     */
    public function getAuthenticationId(): ?string
    {
        return $this->AuthenticationId;
    }
    /**
     * Set AuthenticationId value
     * @param string $authenticationId
     * @return \ID3Global\Models\GlobalMatrixResult
     */
    public function setAuthenticationId(?string $authenticationId = null): self
    {
        // validation for constraint: string
        if (!is_null($authenticationId) && !is_string($authenticationId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authenticationId, true), gettype($authenticationId)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($authenticationId) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $authenticationId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($authenticationId, true)), __LINE__);
        }
        $this->AuthenticationId = $authenticationId;
        
        return $this;
    }
    /**
     * Get ProfileId value
     * @return string|null
     */
    public function getProfileId(): ?string
    {
        return $this->ProfileId;
    }
    /**
     * Set ProfileId value
     * @param string $profileId
     * @return \ID3Global\Models\GlobalMatrixResult
     */
    public function setProfileId(?string $profileId = null): self
    {
        // validation for constraint: string
        if (!is_null($profileId) && !is_string($profileId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($profileId, true), gettype($profileId)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($profileId) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $profileId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($profileId, true)), __LINE__);
        }
        $this->ProfileId = $profileId;
        
        return $this;
    }
    /**
     * Get Overall value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalMatrixResultItem|null
     */
    public function getOverall(): ?\ID3Global\Models\GlobalMatrixResultItem
    {
        return isset($this->Overall) ? $this->Overall : null;
    }
    /**
     * Set Overall value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalMatrixResultItem $overall
     * @return \ID3Global\Models\GlobalMatrixResult
     */
    public function setOverall(?\ID3Global\Models\GlobalMatrixResultItem $overall = null): self
    {
        if (is_null($overall) || (is_array($overall) && empty($overall))) {
            unset($this->Overall);
        } else {
            $this->Overall = $overall;
        }
        
        return $this;
    }
    /**
     * Get Summary value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalMatrixResultItem|null
     */
    public function getSummary(): ?\ID3Global\Arrays\ArrayOfGlobalMatrixResultItem
    {
        return isset($this->Summary) ? $this->Summary : null;
    }
    /**
     * Set Summary value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalMatrixResultItem $summary
     * @return \ID3Global\Models\GlobalMatrixResult
     */
    public function setSummary(?\ID3Global\Arrays\ArrayOfGlobalMatrixResultItem $summary = null): self
    {
        if (is_null($summary) || (is_array($summary) && empty($summary))) {
            unset($this->Summary);
        } else {
            $this->Summary = $summary;
        }
        
        return $this;
    }
    /**
     * Get OverallFields value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalMatrixResultField|null
     */
    public function getOverallFields(): ?\ID3Global\Arrays\ArrayOfGlobalMatrixResultField
    {
        return isset($this->OverallFields) ? $this->OverallFields : null;
    }
    /**
     * Set OverallFields value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalMatrixResultField $overallFields
     * @return \ID3Global\Models\GlobalMatrixResult
     */
    public function setOverallFields(?\ID3Global\Arrays\ArrayOfGlobalMatrixResultField $overallFields = null): self
    {
        if (is_null($overallFields) || (is_array($overallFields) && empty($overallFields))) {
            unset($this->OverallFields);
        } else {
            $this->OverallFields = $overallFields;
        }
        
        return $this;
    }
    /**
     * Get CellGroups value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalMatrixCellGroup|null
     */
    public function getCellGroups(): ?\ID3Global\Arrays\ArrayOfGlobalMatrixCellGroup
    {
        return isset($this->CellGroups) ? $this->CellGroups : null;
    }
    /**
     * Set CellGroups value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalMatrixCellGroup $cellGroups
     * @return \ID3Global\Models\GlobalMatrixResult
     */
    public function setCellGroups(?\ID3Global\Arrays\ArrayOfGlobalMatrixCellGroup $cellGroups = null): self
    {
        if (is_null($cellGroups) || (is_array($cellGroups) && empty($cellGroups))) {
            unset($this->CellGroups);
        } else {
            $this->CellGroups = $cellGroups;
        }
        
        return $this;
    }
    /**
     * Get SummaryTemplate value
     * @return string|null
     */
    public function getSummaryTemplate(): ?string
    {
        return $this->SummaryTemplate;
    }
    /**
     * Set SummaryTemplate value
     * @uses \ID3Global\Enums\GlobalMatrixSummaryTemplateType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalMatrixSummaryTemplateType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $summaryTemplate
     * @return \ID3Global\Models\GlobalMatrixResult
     */
    public function setSummaryTemplate(?string $summaryTemplate = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalMatrixSummaryTemplateType::valueIsValid($summaryTemplate)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalMatrixSummaryTemplateType', is_array($summaryTemplate) ? implode(', ', $summaryTemplate) : var_export($summaryTemplate, true), implode(', ', \ID3Global\Enums\GlobalMatrixSummaryTemplateType::getValidValues())), __LINE__);
        }
        $this->SummaryTemplate = $summaryTemplate;
        
        return $this;
    }
    /**
     * Get Pending value
     * @return bool|null
     */
    public function getPending(): ?bool
    {
        return $this->Pending;
    }
    /**
     * Set Pending value
     * @param bool $pending
     * @return \ID3Global\Models\GlobalMatrixResult
     */
    public function setPending(?bool $pending = null): self
    {
        // validation for constraint: boolean
        if (!is_null($pending) && !is_bool($pending)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($pending, true), gettype($pending)), __LINE__);
        }
        $this->Pending = $pending;
        
        return $this;
    }
    /**
     * Get ExternalDataIds value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint|null
     */
    public function getExternalDataIds(): ?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint
    {
        return isset($this->ExternalDataIds) ? $this->ExternalDataIds : null;
    }
    /**
     * Set ExternalDataIds value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint $externalDataIds
     * @return \ID3Global\Models\GlobalMatrixResult
     */
    public function setExternalDataIds(?\ID3Global\Arrays\ArrayOfGlobalKeyValuePairOfstringint $externalDataIds = null): self
    {
        if (is_null($externalDataIds) || (is_array($externalDataIds) && empty($externalDataIds))) {
            unset($this->ExternalDataIds);
        } else {
            $this->ExternalDataIds = $externalDataIds;
        }
        
        return $this;
    }
    /**
     * Get Date value
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->Date;
    }
    /**
     * Set Date value
     * @param string $date
     * @return \ID3Global\Models\GlobalMatrixResult
     */
    public function setDate(?string $date = null): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->Date = $date;
        
        return $this;
    }
}
