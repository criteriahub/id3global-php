<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalItemCheck Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q233:GlobalItemCheck
 * @subpackage Structs
 */
class GlobalItemCheck extends GlobalItem
{
    /**
     * The AllowedProperties
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $AllowedProperties = null;
    /**
     * The Properties
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalProperty|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalProperty $Properties = null;
    /**
     * The Scoring
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalScoring|null
     */
    protected ?\ID3Global\Models\GlobalScoring $Scoring = null;
    /**
     * Constructor method for GlobalItemCheck
     * @uses GlobalItemCheck::setAllowedProperties()
     * @uses GlobalItemCheck::setProperties()
     * @uses GlobalItemCheck::setScoring()
     * @param bool $allowedProperties
     * @param \ID3Global\Arrays\ArrayOfGlobalProperty $properties
     * @param \ID3Global\Models\GlobalScoring $scoring
     */
    public function __construct(?bool $allowedProperties = null, ?\ID3Global\Arrays\ArrayOfGlobalProperty $properties = null, ?\ID3Global\Models\GlobalScoring $scoring = null)
    {
        $this
            ->setAllowedProperties($allowedProperties)
            ->setProperties($properties)
            ->setScoring($scoring);
    }
    /**
     * Get AllowedProperties value
     * @return bool|null
     */
    public function getAllowedProperties(): ?bool
    {
        return $this->AllowedProperties;
    }
    /**
     * Set AllowedProperties value
     * @param bool $allowedProperties
     * @return \ID3Global\Models\GlobalItemCheck
     */
    public function setAllowedProperties(?bool $allowedProperties = null): self
    {
        // validation for constraint: boolean
        if (!is_null($allowedProperties) && !is_bool($allowedProperties)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allowedProperties, true), gettype($allowedProperties)), __LINE__);
        }
        $this->AllowedProperties = $allowedProperties;
        
        return $this;
    }
    /**
     * Get Properties value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalProperty|null
     */
    public function getProperties(): ?\ID3Global\Arrays\ArrayOfGlobalProperty
    {
        return isset($this->Properties) ? $this->Properties : null;
    }
    /**
     * Set Properties value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalProperty $properties
     * @return \ID3Global\Models\GlobalItemCheck
     */
    public function setProperties(?\ID3Global\Arrays\ArrayOfGlobalProperty $properties = null): self
    {
        if (is_null($properties) || (is_array($properties) && empty($properties))) {
            unset($this->Properties);
        } else {
            $this->Properties = $properties;
        }
        
        return $this;
    }
    /**
     * Get Scoring value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalScoring|null
     */
    public function getScoring(): ?\ID3Global\Models\GlobalScoring
    {
        return isset($this->Scoring) ? $this->Scoring : null;
    }
    /**
     * Set Scoring value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalScoring $scoring
     * @return \ID3Global\Models\GlobalItemCheck
     */
    public function setScoring(?\ID3Global\Models\GlobalScoring $scoring = null): self
    {
        if (is_null($scoring) || (is_array($scoring) && empty($scoring))) {
            unset($this->Scoring);
        } else {
            $this->Scoring = $scoring;
        }
        
        return $this;
    }
}
