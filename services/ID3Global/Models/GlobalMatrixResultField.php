<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalMatrixResultField Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q819:GlobalMatrixResultField
 * @subpackage Structs
 */
class GlobalMatrixResultField extends AbstractStructBase
{
    /**
     * The MatchType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $MatchType = null;
    /**
     * The ResourceKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ResourceKey = null;
    /**
     * Constructor method for GlobalMatrixResultField
     * @uses GlobalMatrixResultField::setMatchType()
     * @uses GlobalMatrixResultField::setResourceKey()
     * @param string $matchType
     * @param string $resourceKey
     */
    public function __construct(?string $matchType = null, ?string $resourceKey = null)
    {
        $this
            ->setMatchType($matchType)
            ->setResourceKey($resourceKey);
    }
    /**
     * Get MatchType value
     * @return string|null
     */
    public function getMatchType(): ?string
    {
        return $this->MatchType;
    }
    /**
     * Set MatchType value
     * @uses \ID3Global\Enums\GlobalMatrixMatchType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalMatrixMatchType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $matchType
     * @return \ID3Global\Models\GlobalMatrixResultField
     */
    public function setMatchType(?string $matchType = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalMatrixMatchType::valueIsValid($matchType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalMatrixMatchType', is_array($matchType) ? implode(', ', $matchType) : var_export($matchType, true), implode(', ', \ID3Global\Enums\GlobalMatrixMatchType::getValidValues())), __LINE__);
        }
        $this->MatchType = $matchType;
        
        return $this;
    }
    /**
     * Get ResourceKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getResourceKey(): ?string
    {
        return isset($this->ResourceKey) ? $this->ResourceKey : null;
    }
    /**
     * Set ResourceKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $resourceKey
     * @return \ID3Global\Models\GlobalMatrixResultField
     */
    public function setResourceKey(?string $resourceKey = null): self
    {
        // validation for constraint: string
        if (!is_null($resourceKey) && !is_string($resourceKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($resourceKey, true), gettype($resourceKey)), __LINE__);
        }
        if (is_null($resourceKey) || (is_array($resourceKey) && empty($resourceKey))) {
            unset($this->ResourceKey);
        } else {
            $this->ResourceKey = $resourceKey;
        }
        
        return $this;
    }
}
