<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAuthentication Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q463:GlobalAuthentication
 * @subpackage Structs
 */
class GlobalAuthentication extends AbstractStructBase
{
    /**
     * The AuthenticationID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AuthenticationID = null;
    /**
     * The ChainID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ChainID = null;
    /**
     * The Timestamp
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Timestamp = null;
    /**
     * The CustomerRef
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CustomerRef = null;
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The AccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AccountID = null;
    /**
     * The Score
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $Score = null;
    /**
     * The BandText
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $BandText = null;
    /**
     * The ProfileID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ProfileID = null;
    /**
     * The ProfileName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ProfileName = null;
    /**
     * The ProfileVersion
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ProfileVersion = null;
    /**
     * The ProfileRevision
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ProfileRevision = null;
    /**
     * The ProfileState
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ProfileState = null;
    /**
     * The HelpdeskAccess
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $HelpdeskAccess = null;
    /**
     * The IsMultipleProfile
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsMultipleProfile = null;
    /**
     * The DeletionState
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DeletionState = null;
    /**
     * The IsSentToOGM
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsSentToOGM = null;
    /**
     * The HasOGMSetup
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $HasOGMSetup = null;
    /**
     * The HasInputData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $HasInputData = null;
    /**
     * Constructor method for GlobalAuthentication
     * @uses GlobalAuthentication::setAuthenticationID()
     * @uses GlobalAuthentication::setChainID()
     * @uses GlobalAuthentication::setTimestamp()
     * @uses GlobalAuthentication::setCustomerRef()
     * @uses GlobalAuthentication::setOrgID()
     * @uses GlobalAuthentication::setAccountID()
     * @uses GlobalAuthentication::setScore()
     * @uses GlobalAuthentication::setBandText()
     * @uses GlobalAuthentication::setProfileID()
     * @uses GlobalAuthentication::setProfileName()
     * @uses GlobalAuthentication::setProfileVersion()
     * @uses GlobalAuthentication::setProfileRevision()
     * @uses GlobalAuthentication::setProfileState()
     * @uses GlobalAuthentication::setHelpdeskAccess()
     * @uses GlobalAuthentication::setIsMultipleProfile()
     * @uses GlobalAuthentication::setDeletionState()
     * @uses GlobalAuthentication::setIsSentToOGM()
     * @uses GlobalAuthentication::setHasOGMSetup()
     * @uses GlobalAuthentication::setHasInputData()
     * @param string $authenticationID
     * @param string $chainID
     * @param string $timestamp
     * @param string $customerRef
     * @param string $orgID
     * @param string $accountID
     * @param int $score
     * @param string $bandText
     * @param string $profileID
     * @param string $profileName
     * @param int $profileVersion
     * @param int $profileRevision
     * @param string $profileState
     * @param bool $helpdeskAccess
     * @param bool $isMultipleProfile
     * @param string $deletionState
     * @param bool $isSentToOGM
     * @param bool $hasOGMSetup
     * @param bool $hasInputData
     */
    public function __construct(?string $authenticationID = null, ?string $chainID = null, ?string $timestamp = null, ?string $customerRef = null, ?string $orgID = null, ?string $accountID = null, ?int $score = null, ?string $bandText = null, ?string $profileID = null, ?string $profileName = null, ?int $profileVersion = null, ?int $profileRevision = null, ?string $profileState = null, ?bool $helpdeskAccess = null, ?bool $isMultipleProfile = null, ?string $deletionState = null, ?bool $isSentToOGM = null, ?bool $hasOGMSetup = null, ?bool $hasInputData = null)
    {
        $this
            ->setAuthenticationID($authenticationID)
            ->setChainID($chainID)
            ->setTimestamp($timestamp)
            ->setCustomerRef($customerRef)
            ->setOrgID($orgID)
            ->setAccountID($accountID)
            ->setScore($score)
            ->setBandText($bandText)
            ->setProfileID($profileID)
            ->setProfileName($profileName)
            ->setProfileVersion($profileVersion)
            ->setProfileRevision($profileRevision)
            ->setProfileState($profileState)
            ->setHelpdeskAccess($helpdeskAccess)
            ->setIsMultipleProfile($isMultipleProfile)
            ->setDeletionState($deletionState)
            ->setIsSentToOGM($isSentToOGM)
            ->setHasOGMSetup($hasOGMSetup)
            ->setHasInputData($hasInputData);
    }
    /**
     * Get AuthenticationID value
     * @return string|null
     */
    public function getAuthenticationID(): ?string
    {
        return $this->AuthenticationID;
    }
    /**
     * Set AuthenticationID value
     * @param string $authenticationID
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setAuthenticationID(?string $authenticationID = null): self
    {
        // validation for constraint: string
        if (!is_null($authenticationID) && !is_string($authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authenticationID, true), gettype($authenticationID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($authenticationID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($authenticationID, true)), __LINE__);
        }
        $this->AuthenticationID = $authenticationID;
        
        return $this;
    }
    /**
     * Get ChainID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getChainID(): ?string
    {
        return isset($this->ChainID) ? $this->ChainID : null;
    }
    /**
     * Set ChainID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $chainID
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setChainID(?string $chainID = null): self
    {
        // validation for constraint: string
        if (!is_null($chainID) && !is_string($chainID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($chainID, true), gettype($chainID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($chainID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $chainID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($chainID, true)), __LINE__);
        }
        if (is_null($chainID) || (is_array($chainID) && empty($chainID))) {
            unset($this->ChainID);
        } else {
            $this->ChainID = $chainID;
        }
        
        return $this;
    }
    /**
     * Get Timestamp value
     * @return string|null
     */
    public function getTimestamp(): ?string
    {
        return $this->Timestamp;
    }
    /**
     * Set Timestamp value
     * @param string $timestamp
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setTimestamp(?string $timestamp = null): self
    {
        // validation for constraint: string
        if (!is_null($timestamp) && !is_string($timestamp)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($timestamp, true), gettype($timestamp)), __LINE__);
        }
        $this->Timestamp = $timestamp;
        
        return $this;
    }
    /**
     * Get CustomerRef value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCustomerRef(): ?string
    {
        return isset($this->CustomerRef) ? $this->CustomerRef : null;
    }
    /**
     * Set CustomerRef value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $customerRef
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setCustomerRef(?string $customerRef = null): self
    {
        // validation for constraint: string
        if (!is_null($customerRef) && !is_string($customerRef)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($customerRef, true), gettype($customerRef)), __LINE__);
        }
        if (is_null($customerRef) || (is_array($customerRef) && empty($customerRef))) {
            unset($this->CustomerRef);
        } else {
            $this->CustomerRef = $customerRef;
        }
        
        return $this;
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get AccountID value
     * @return string|null
     */
    public function getAccountID(): ?string
    {
        return $this->AccountID;
    }
    /**
     * Set AccountID value
     * @param string $accountID
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setAccountID(?string $accountID = null): self
    {
        // validation for constraint: string
        if (!is_null($accountID) && !is_string($accountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountID, true), gettype($accountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($accountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $accountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($accountID, true)), __LINE__);
        }
        $this->AccountID = $accountID;
        
        return $this;
    }
    /**
     * Get Score value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getScore(): ?int
    {
        return isset($this->Score) ? $this->Score : null;
    }
    /**
     * Set Score value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $score
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setScore(?int $score = null): self
    {
        // validation for constraint: int
        if (!is_null($score) && !(is_int($score) || ctype_digit($score))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($score, true), gettype($score)), __LINE__);
        }
        if (is_null($score) || (is_array($score) && empty($score))) {
            unset($this->Score);
        } else {
            $this->Score = $score;
        }
        
        return $this;
    }
    /**
     * Get BandText value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBandText(): ?string
    {
        return isset($this->BandText) ? $this->BandText : null;
    }
    /**
     * Set BandText value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $bandText
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setBandText(?string $bandText = null): self
    {
        // validation for constraint: string
        if (!is_null($bandText) && !is_string($bandText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bandText, true), gettype($bandText)), __LINE__);
        }
        if (is_null($bandText) || (is_array($bandText) && empty($bandText))) {
            unset($this->BandText);
        } else {
            $this->BandText = $bandText;
        }
        
        return $this;
    }
    /**
     * Get ProfileID value
     * @return string|null
     */
    public function getProfileID(): ?string
    {
        return $this->ProfileID;
    }
    /**
     * Set ProfileID value
     * @param string $profileID
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setProfileID(?string $profileID = null): self
    {
        // validation for constraint: string
        if (!is_null($profileID) && !is_string($profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($profileID, true), gettype($profileID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($profileID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($profileID, true)), __LINE__);
        }
        $this->ProfileID = $profileID;
        
        return $this;
    }
    /**
     * Get ProfileName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProfileName(): ?string
    {
        return isset($this->ProfileName) ? $this->ProfileName : null;
    }
    /**
     * Set ProfileName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $profileName
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setProfileName(?string $profileName = null): self
    {
        // validation for constraint: string
        if (!is_null($profileName) && !is_string($profileName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($profileName, true), gettype($profileName)), __LINE__);
        }
        if (is_null($profileName) || (is_array($profileName) && empty($profileName))) {
            unset($this->ProfileName);
        } else {
            $this->ProfileName = $profileName;
        }
        
        return $this;
    }
    /**
     * Get ProfileVersion value
     * @return int|null
     */
    public function getProfileVersion(): ?int
    {
        return $this->ProfileVersion;
    }
    /**
     * Set ProfileVersion value
     * @param int $profileVersion
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setProfileVersion(?int $profileVersion = null): self
    {
        // validation for constraint: int
        if (!is_null($profileVersion) && !(is_int($profileVersion) || ctype_digit($profileVersion))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($profileVersion, true), gettype($profileVersion)), __LINE__);
        }
        $this->ProfileVersion = $profileVersion;
        
        return $this;
    }
    /**
     * Get ProfileRevision value
     * @return int|null
     */
    public function getProfileRevision(): ?int
    {
        return $this->ProfileRevision;
    }
    /**
     * Set ProfileRevision value
     * @param int $profileRevision
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setProfileRevision(?int $profileRevision = null): self
    {
        // validation for constraint: int
        if (!is_null($profileRevision) && !(is_int($profileRevision) || ctype_digit($profileRevision))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($profileRevision, true), gettype($profileRevision)), __LINE__);
        }
        $this->ProfileRevision = $profileRevision;
        
        return $this;
    }
    /**
     * Get ProfileState value
     * @return string|null
     */
    public function getProfileState(): ?string
    {
        return $this->ProfileState;
    }
    /**
     * Set ProfileState value
     * @uses \ID3Global\Enums\GlobalProfileState::valueIsValid()
     * @uses \ID3Global\Enums\GlobalProfileState::getValidValues()
     * @throws InvalidArgumentException
     * @param string $profileState
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setProfileState(?string $profileState = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalProfileState::valueIsValid($profileState)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalProfileState', is_array($profileState) ? implode(', ', $profileState) : var_export($profileState, true), implode(', ', \ID3Global\Enums\GlobalProfileState::getValidValues())), __LINE__);
        }
        $this->ProfileState = $profileState;
        
        return $this;
    }
    /**
     * Get HelpdeskAccess value
     * @return bool|null
     */
    public function getHelpdeskAccess(): ?bool
    {
        return $this->HelpdeskAccess;
    }
    /**
     * Set HelpdeskAccess value
     * @param bool $helpdeskAccess
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setHelpdeskAccess(?bool $helpdeskAccess = null): self
    {
        // validation for constraint: boolean
        if (!is_null($helpdeskAccess) && !is_bool($helpdeskAccess)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($helpdeskAccess, true), gettype($helpdeskAccess)), __LINE__);
        }
        $this->HelpdeskAccess = $helpdeskAccess;
        
        return $this;
    }
    /**
     * Get IsMultipleProfile value
     * @return bool|null
     */
    public function getIsMultipleProfile(): ?bool
    {
        return $this->IsMultipleProfile;
    }
    /**
     * Set IsMultipleProfile value
     * @param bool $isMultipleProfile
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setIsMultipleProfile(?bool $isMultipleProfile = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isMultipleProfile) && !is_bool($isMultipleProfile)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isMultipleProfile, true), gettype($isMultipleProfile)), __LINE__);
        }
        $this->IsMultipleProfile = $isMultipleProfile;
        
        return $this;
    }
    /**
     * Get DeletionState value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDeletionState(): ?string
    {
        return isset($this->DeletionState) ? $this->DeletionState : null;
    }
    /**
     * Set DeletionState value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $deletionState
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setDeletionState(?string $deletionState = null): self
    {
        // validation for constraint: string
        if (!is_null($deletionState) && !is_string($deletionState)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deletionState, true), gettype($deletionState)), __LINE__);
        }
        if (is_null($deletionState) || (is_array($deletionState) && empty($deletionState))) {
            unset($this->DeletionState);
        } else {
            $this->DeletionState = $deletionState;
        }
        
        return $this;
    }
    /**
     * Get IsSentToOGM value
     * @return bool|null
     */
    public function getIsSentToOGM(): ?bool
    {
        return $this->IsSentToOGM;
    }
    /**
     * Set IsSentToOGM value
     * @param bool $isSentToOGM
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setIsSentToOGM(?bool $isSentToOGM = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isSentToOGM) && !is_bool($isSentToOGM)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isSentToOGM, true), gettype($isSentToOGM)), __LINE__);
        }
        $this->IsSentToOGM = $isSentToOGM;
        
        return $this;
    }
    /**
     * Get HasOGMSetup value
     * @return bool|null
     */
    public function getHasOGMSetup(): ?bool
    {
        return $this->HasOGMSetup;
    }
    /**
     * Set HasOGMSetup value
     * @param bool $hasOGMSetup
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setHasOGMSetup(?bool $hasOGMSetup = null): self
    {
        // validation for constraint: boolean
        if (!is_null($hasOGMSetup) && !is_bool($hasOGMSetup)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($hasOGMSetup, true), gettype($hasOGMSetup)), __LINE__);
        }
        $this->HasOGMSetup = $hasOGMSetup;
        
        return $this;
    }
    /**
     * Get HasInputData value
     * @return bool|null
     */
    public function getHasInputData(): ?bool
    {
        return $this->HasInputData;
    }
    /**
     * Set HasInputData value
     * @param bool $hasInputData
     * @return \ID3Global\Models\GlobalAuthentication
     */
    public function setHasInputData(?bool $hasInputData = null): self
    {
        // validation for constraint: boolean
        if (!is_null($hasInputData) && !is_bool($hasInputData)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($hasInputData, true), gettype($hasInputData)), __LINE__);
        }
        $this->HasInputData = $hasInputData;
        
        return $this;
    }
}
