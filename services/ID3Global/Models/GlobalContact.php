<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalContact Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q557:GlobalContact
 * @subpackage Structs
 */
class GlobalContact extends AbstractStructBase
{
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Telephone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Telephone = null;
    /**
     * The Emails
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $Emails = null;
    /**
     * Constructor method for GlobalContact
     * @uses GlobalContact::setName()
     * @uses GlobalContact::setTelephone()
     * @uses GlobalContact::setEmails()
     * @param string $name
     * @param string $telephone
     * @param \ID3Global\Arrays\ArrayOfstring $emails
     */
    public function __construct(?string $name = null, ?string $telephone = null, ?\ID3Global\Arrays\ArrayOfstring $emails = null)
    {
        $this
            ->setName($name)
            ->setTelephone($telephone)
            ->setEmails($emails);
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalContact
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Telephone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTelephone(): ?string
    {
        return isset($this->Telephone) ? $this->Telephone : null;
    }
    /**
     * Set Telephone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $telephone
     * @return \ID3Global\Models\GlobalContact
     */
    public function setTelephone(?string $telephone = null): self
    {
        // validation for constraint: string
        if (!is_null($telephone) && !is_string($telephone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($telephone, true), gettype($telephone)), __LINE__);
        }
        if (is_null($telephone) || (is_array($telephone) && empty($telephone))) {
            unset($this->Telephone);
        } else {
            $this->Telephone = $telephone;
        }
        
        return $this;
    }
    /**
     * Get Emails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getEmails(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->Emails) ? $this->Emails : null;
    }
    /**
     * Set Emails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $emails
     * @return \ID3Global\Models\GlobalContact
     */
    public function setEmails(?\ID3Global\Arrays\ArrayOfstring $emails = null): self
    {
        if (is_null($emails) || (is_array($emails) && empty($emails))) {
            unset($this->Emails);
        } else {
            $this->Emails = $emails;
        }
        
        return $this;
    }
}
