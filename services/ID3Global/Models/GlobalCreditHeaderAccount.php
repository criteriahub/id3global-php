<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCreditHeaderAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q653:GlobalCreditHeaderAccount
 * @subpackage Structs
 */
class GlobalCreditHeaderAccount extends GlobalSupplierAccount
{
    /**
     * The Company
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Company = null;
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * The MatchLevel
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $MatchLevel = null;
    /**
     * The TestMode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $TestMode = null;
    /**
     * Constructor method for GlobalCreditHeaderAccount
     * @uses GlobalCreditHeaderAccount::setCompany()
     * @uses GlobalCreditHeaderAccount::setUsername()
     * @uses GlobalCreditHeaderAccount::setPassword()
     * @uses GlobalCreditHeaderAccount::setMatchLevel()
     * @uses GlobalCreditHeaderAccount::setTestMode()
     * @param string $company
     * @param string $username
     * @param string $password
     * @param string $matchLevel
     * @param string $testMode
     */
    public function __construct(?string $company = null, ?string $username = null, ?string $password = null, ?string $matchLevel = null, ?string $testMode = null)
    {
        $this
            ->setCompany($company)
            ->setUsername($username)
            ->setPassword($password)
            ->setMatchLevel($matchLevel)
            ->setTestMode($testMode);
    }
    /**
     * Get Company value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCompany(): ?string
    {
        return isset($this->Company) ? $this->Company : null;
    }
    /**
     * Set Company value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $company
     * @return \ID3Global\Models\GlobalCreditHeaderAccount
     */
    public function setCompany(?string $company = null): self
    {
        // validation for constraint: string
        if (!is_null($company) && !is_string($company)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($company, true), gettype($company)), __LINE__);
        }
        if (is_null($company) || (is_array($company) && empty($company))) {
            unset($this->Company);
        } else {
            $this->Company = $company;
        }
        
        return $this;
    }
    /**
     * Get Username value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return isset($this->Username) ? $this->Username : null;
    }
    /**
     * Set Username value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username
     * @return \ID3Global\Models\GlobalCreditHeaderAccount
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        if (is_null($username) || (is_array($username) && empty($username))) {
            unset($this->Username);
        } else {
            $this->Username = $username;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\GlobalCreditHeaderAccount
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
    /**
     * Get MatchLevel value
     * @return string|null
     */
    public function getMatchLevel(): ?string
    {
        return $this->MatchLevel;
    }
    /**
     * Set MatchLevel value
     * @uses \ID3Global\Enums\GlobalMatchLevel::valueIsValid()
     * @uses \ID3Global\Enums\GlobalMatchLevel::getValidValues()
     * @throws InvalidArgumentException
     * @param string $matchLevel
     * @return \ID3Global\Models\GlobalCreditHeaderAccount
     */
    public function setMatchLevel(?string $matchLevel = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalMatchLevel::valueIsValid($matchLevel)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalMatchLevel', is_array($matchLevel) ? implode(', ', $matchLevel) : var_export($matchLevel, true), implode(', ', \ID3Global\Enums\GlobalMatchLevel::getValidValues())), __LINE__);
        }
        $this->MatchLevel = $matchLevel;
        
        return $this;
    }
    /**
     * Get TestMode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTestMode(): ?string
    {
        return isset($this->TestMode) ? $this->TestMode : null;
    }
    /**
     * Set TestMode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $testMode
     * @return \ID3Global\Models\GlobalCreditHeaderAccount
     */
    public function setTestMode(?string $testMode = null): self
    {
        // validation for constraint: string
        if (!is_null($testMode) && !is_string($testMode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($testMode, true), gettype($testMode)), __LINE__);
        }
        if (is_null($testMode) || (is_array($testMode) && empty($testMode))) {
            unset($this->TestMode);
        } else {
            $this->TestMode = $testMode;
        }
        
        return $this;
    }
}
