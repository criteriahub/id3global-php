<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProfileChainResponse Models
 * @subpackage Structs
 */
class GetProfileChainResponse extends AbstractStructBase
{
    /**
     * The GetProfileChainResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalProfileVersion|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalProfileVersion $GetProfileChainResult = null;
    /**
     * Constructor method for GetProfileChainResponse
     * @uses GetProfileChainResponse::setGetProfileChainResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileVersion $getProfileChainResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalProfileVersion $getProfileChainResult = null)
    {
        $this
            ->setGetProfileChainResult($getProfileChainResult);
    }
    /**
     * Get GetProfileChainResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileVersion|null
     */
    public function getGetProfileChainResult(): ?\ID3Global\Arrays\ArrayOfGlobalProfileVersion
    {
        return isset($this->GetProfileChainResult) ? $this->GetProfileChainResult : null;
    }
    /**
     * Set GetProfileChainResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileVersion $getProfileChainResult
     * @return \ID3Global\Models\GetProfileChainResponse
     */
    public function setGetProfileChainResult(?\ID3Global\Arrays\ArrayOfGlobalProfileVersion $getProfileChainResult = null): self
    {
        if (is_null($getProfileChainResult) || (is_array($getProfileChainResult) && empty($getProfileChainResult))) {
            unset($this->GetProfileChainResult);
        } else {
            $this->GetProfileChainResult = $getProfileChainResult;
        }
        
        return $this;
    }
}
