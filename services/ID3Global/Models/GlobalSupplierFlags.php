<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSupplierFlags Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q624:GlobalSupplierFlags
 * @subpackage Structs
 */
class GlobalSupplierFlags extends AbstractStructBase
{
    /**
     * The SupplierId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $SupplierId = null;
    /**
     * The Flags
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCountryFlag|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCountryFlag $Flags = null;
    /**
     * Constructor method for GlobalSupplierFlags
     * @uses GlobalSupplierFlags::setSupplierId()
     * @uses GlobalSupplierFlags::setFlags()
     * @param string $supplierId
     * @param \ID3Global\Arrays\ArrayOfGlobalCountryFlag $flags
     */
    public function __construct(?string $supplierId = null, ?\ID3Global\Arrays\ArrayOfGlobalCountryFlag $flags = null)
    {
        $this
            ->setSupplierId($supplierId)
            ->setFlags($flags);
    }
    /**
     * Get SupplierId value
     * @return string|null
     */
    public function getSupplierId(): ?string
    {
        return $this->SupplierId;
    }
    /**
     * Set SupplierId value
     * @param string $supplierId
     * @return \ID3Global\Models\GlobalSupplierFlags
     */
    public function setSupplierId(?string $supplierId = null): self
    {
        // validation for constraint: string
        if (!is_null($supplierId) && !is_string($supplierId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($supplierId, true), gettype($supplierId)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($supplierId) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $supplierId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($supplierId, true)), __LINE__);
        }
        $this->SupplierId = $supplierId;
        
        return $this;
    }
    /**
     * Get Flags value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCountryFlag|null
     */
    public function getFlags(): ?\ID3Global\Arrays\ArrayOfGlobalCountryFlag
    {
        return isset($this->Flags) ? $this->Flags : null;
    }
    /**
     * Set Flags value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCountryFlag $flags
     * @return \ID3Global\Models\GlobalSupplierFlags
     */
    public function setFlags(?\ID3Global\Arrays\ArrayOfGlobalCountryFlag $flags = null): self
    {
        if (is_null($flags) || (is_array($flags) && empty($flags))) {
            unset($this->Flags);
        } else {
            $this->Flags = $flags;
        }
        
        return $this;
    }
}
