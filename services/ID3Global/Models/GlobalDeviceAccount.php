<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDeviceAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q656:GlobalDeviceAccount
 * @subpackage Structs
 */
class GlobalDeviceAccount extends GlobalSupplierAccount
{
    /**
     * The SubscriberID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SubscriberID = null;
    /**
     * The SystemUsername
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SystemUsername = null;
    /**
     * The SystemPassword
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SystemPassword = null;
    /**
     * The LoginSubscriberName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $LoginSubscriberName = null;
    /**
     * The LoginUsername
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $LoginUsername = null;
    /**
     * The LoginPassword
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $LoginPassword = null;
    /**
     * Constructor method for GlobalDeviceAccount
     * @uses GlobalDeviceAccount::setSubscriberID()
     * @uses GlobalDeviceAccount::setSystemUsername()
     * @uses GlobalDeviceAccount::setSystemPassword()
     * @uses GlobalDeviceAccount::setLoginSubscriberName()
     * @uses GlobalDeviceAccount::setLoginUsername()
     * @uses GlobalDeviceAccount::setLoginPassword()
     * @param string $subscriberID
     * @param string $systemUsername
     * @param string $systemPassword
     * @param string $loginSubscriberName
     * @param string $loginUsername
     * @param string $loginPassword
     */
    public function __construct(?string $subscriberID = null, ?string $systemUsername = null, ?string $systemPassword = null, ?string $loginSubscriberName = null, ?string $loginUsername = null, ?string $loginPassword = null)
    {
        $this
            ->setSubscriberID($subscriberID)
            ->setSystemUsername($systemUsername)
            ->setSystemPassword($systemPassword)
            ->setLoginSubscriberName($loginSubscriberName)
            ->setLoginUsername($loginUsername)
            ->setLoginPassword($loginPassword);
    }
    /**
     * Get SubscriberID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSubscriberID(): ?string
    {
        return isset($this->SubscriberID) ? $this->SubscriberID : null;
    }
    /**
     * Set SubscriberID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $subscriberID
     * @return \ID3Global\Models\GlobalDeviceAccount
     */
    public function setSubscriberID(?string $subscriberID = null): self
    {
        // validation for constraint: string
        if (!is_null($subscriberID) && !is_string($subscriberID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subscriberID, true), gettype($subscriberID)), __LINE__);
        }
        if (is_null($subscriberID) || (is_array($subscriberID) && empty($subscriberID))) {
            unset($this->SubscriberID);
        } else {
            $this->SubscriberID = $subscriberID;
        }
        
        return $this;
    }
    /**
     * Get SystemUsername value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSystemUsername(): ?string
    {
        return isset($this->SystemUsername) ? $this->SystemUsername : null;
    }
    /**
     * Set SystemUsername value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $systemUsername
     * @return \ID3Global\Models\GlobalDeviceAccount
     */
    public function setSystemUsername(?string $systemUsername = null): self
    {
        // validation for constraint: string
        if (!is_null($systemUsername) && !is_string($systemUsername)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($systemUsername, true), gettype($systemUsername)), __LINE__);
        }
        if (is_null($systemUsername) || (is_array($systemUsername) && empty($systemUsername))) {
            unset($this->SystemUsername);
        } else {
            $this->SystemUsername = $systemUsername;
        }
        
        return $this;
    }
    /**
     * Get SystemPassword value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSystemPassword(): ?string
    {
        return isset($this->SystemPassword) ? $this->SystemPassword : null;
    }
    /**
     * Set SystemPassword value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $systemPassword
     * @return \ID3Global\Models\GlobalDeviceAccount
     */
    public function setSystemPassword(?string $systemPassword = null): self
    {
        // validation for constraint: string
        if (!is_null($systemPassword) && !is_string($systemPassword)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($systemPassword, true), gettype($systemPassword)), __LINE__);
        }
        if (is_null($systemPassword) || (is_array($systemPassword) && empty($systemPassword))) {
            unset($this->SystemPassword);
        } else {
            $this->SystemPassword = $systemPassword;
        }
        
        return $this;
    }
    /**
     * Get LoginSubscriberName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLoginSubscriberName(): ?string
    {
        return isset($this->LoginSubscriberName) ? $this->LoginSubscriberName : null;
    }
    /**
     * Set LoginSubscriberName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $loginSubscriberName
     * @return \ID3Global\Models\GlobalDeviceAccount
     */
    public function setLoginSubscriberName(?string $loginSubscriberName = null): self
    {
        // validation for constraint: string
        if (!is_null($loginSubscriberName) && !is_string($loginSubscriberName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($loginSubscriberName, true), gettype($loginSubscriberName)), __LINE__);
        }
        if (is_null($loginSubscriberName) || (is_array($loginSubscriberName) && empty($loginSubscriberName))) {
            unset($this->LoginSubscriberName);
        } else {
            $this->LoginSubscriberName = $loginSubscriberName;
        }
        
        return $this;
    }
    /**
     * Get LoginUsername value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLoginUsername(): ?string
    {
        return isset($this->LoginUsername) ? $this->LoginUsername : null;
    }
    /**
     * Set LoginUsername value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $loginUsername
     * @return \ID3Global\Models\GlobalDeviceAccount
     */
    public function setLoginUsername(?string $loginUsername = null): self
    {
        // validation for constraint: string
        if (!is_null($loginUsername) && !is_string($loginUsername)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($loginUsername, true), gettype($loginUsername)), __LINE__);
        }
        if (is_null($loginUsername) || (is_array($loginUsername) && empty($loginUsername))) {
            unset($this->LoginUsername);
        } else {
            $this->LoginUsername = $loginUsername;
        }
        
        return $this;
    }
    /**
     * Get LoginPassword value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLoginPassword(): ?string
    {
        return isset($this->LoginPassword) ? $this->LoginPassword : null;
    }
    /**
     * Set LoginPassword value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $loginPassword
     * @return \ID3Global\Models\GlobalDeviceAccount
     */
    public function setLoginPassword(?string $loginPassword = null): self
    {
        // validation for constraint: string
        if (!is_null($loginPassword) && !is_string($loginPassword)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($loginPassword, true), gettype($loginPassword)), __LINE__);
        }
        if (is_null($loginPassword) || (is_array($loginPassword) && empty($loginPassword))) {
            unset($this->LoginPassword);
        } else {
            $this->LoginPassword = $loginPassword;
        }
        
        return $this;
    }
}
