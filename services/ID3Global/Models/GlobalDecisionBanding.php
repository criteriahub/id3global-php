<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDecisionBanding Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q205:GlobalDecisionBanding
 * @subpackage Structs
 */
class GlobalDecisionBanding extends AbstractStructBase
{
    /**
     * The ExceptionText
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ExceptionText = null;
    /**
     * The Bands
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalBand|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalBand $Bands = null;
    /**
     * The ExceptionPass
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    protected ?\ID3Global\Arrays\ArrayOfunsignedInt $ExceptionPass = null;
    /**
     * Constructor method for GlobalDecisionBanding
     * @uses GlobalDecisionBanding::setExceptionText()
     * @uses GlobalDecisionBanding::setBands()
     * @uses GlobalDecisionBanding::setExceptionPass()
     * @param string $exceptionText
     * @param \ID3Global\Arrays\ArrayOfGlobalBand $bands
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $exceptionPass
     */
    public function __construct(?string $exceptionText = null, ?\ID3Global\Arrays\ArrayOfGlobalBand $bands = null, ?\ID3Global\Arrays\ArrayOfunsignedInt $exceptionPass = null)
    {
        $this
            ->setExceptionText($exceptionText)
            ->setBands($bands)
            ->setExceptionPass($exceptionPass);
    }
    /**
     * Get ExceptionText value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getExceptionText(): ?string
    {
        return isset($this->ExceptionText) ? $this->ExceptionText : null;
    }
    /**
     * Set ExceptionText value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $exceptionText
     * @return \ID3Global\Models\GlobalDecisionBanding
     */
    public function setExceptionText(?string $exceptionText = null): self
    {
        // validation for constraint: string
        if (!is_null($exceptionText) && !is_string($exceptionText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($exceptionText, true), gettype($exceptionText)), __LINE__);
        }
        if (is_null($exceptionText) || (is_array($exceptionText) && empty($exceptionText))) {
            unset($this->ExceptionText);
        } else {
            $this->ExceptionText = $exceptionText;
        }
        
        return $this;
    }
    /**
     * Get Bands value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalBand|null
     */
    public function getBands(): ?\ID3Global\Arrays\ArrayOfGlobalBand
    {
        return isset($this->Bands) ? $this->Bands : null;
    }
    /**
     * Set Bands value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalBand $bands
     * @return \ID3Global\Models\GlobalDecisionBanding
     */
    public function setBands(?\ID3Global\Arrays\ArrayOfGlobalBand $bands = null): self
    {
        if (is_null($bands) || (is_array($bands) && empty($bands))) {
            unset($this->Bands);
        } else {
            $this->Bands = $bands;
        }
        
        return $this;
    }
    /**
     * Get ExceptionPass value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    public function getExceptionPass(): ?\ID3Global\Arrays\ArrayOfunsignedInt
    {
        return isset($this->ExceptionPass) ? $this->ExceptionPass : null;
    }
    /**
     * Set ExceptionPass value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $exceptionPass
     * @return \ID3Global\Models\GlobalDecisionBanding
     */
    public function setExceptionPass(?\ID3Global\Arrays\ArrayOfunsignedInt $exceptionPass = null): self
    {
        if (is_null($exceptionPass) || (is_array($exceptionPass) && empty($exceptionPass))) {
            unset($this->ExceptionPass);
        } else {
            $this->ExceptionPass = $exceptionPass;
        }
        
        return $this;
    }
}
