<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAddress Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q134:GlobalAddress
 * @subpackage Structs
 */
class GlobalAddress extends AbstractStructBase
{
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Country = null;
    /**
     * The Street
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Street = null;
    /**
     * The SubStreet
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SubStreet = null;
    /**
     * The City
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $City = null;
    /**
     * The SubCity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SubCity = null;
    /**
     * The StateDistrict
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $StateDistrict = null;
    /**
     * The POBox
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $POBox = null;
    /**
     * The Region
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Region = null;
    /**
     * The Principality
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Principality = null;
    /**
     * The ZipPostcode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ZipPostcode = null;
    /**
     * The DpsZipPlus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DpsZipPlus = null;
    /**
     * The CedexMailsort
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CedexMailsort = null;
    /**
     * The Department
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Department = null;
    /**
     * The Company
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Company = null;
    /**
     * The Building
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Building = null;
    /**
     * The SubBuilding
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SubBuilding = null;
    /**
     * The Premise
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Premise = null;
    /**
     * The AddressLine1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLine1 = null;
    /**
     * The AddressLine2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLine2 = null;
    /**
     * The AddressLine3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLine3 = null;
    /**
     * The AddressLine4
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLine4 = null;
    /**
     * The AddressLine5
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLine5 = null;
    /**
     * The AddressLine6
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLine6 = null;
    /**
     * The AddressLine7
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLine7 = null;
    /**
     * The AddressLine8
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLine8 = null;
    /**
     * The FirstYearOfResidence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $FirstYearOfResidence = null;
    /**
     * The LastYearOfResidence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $LastYearOfResidence = null;
    /**
     * The FirstMonthOfResidence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $FirstMonthOfResidence = null;
    /**
     * The LastMonthOfResidence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $LastMonthOfResidence = null;
    /**
     * Constructor method for GlobalAddress
     * @uses GlobalAddress::setCountry()
     * @uses GlobalAddress::setStreet()
     * @uses GlobalAddress::setSubStreet()
     * @uses GlobalAddress::setCity()
     * @uses GlobalAddress::setSubCity()
     * @uses GlobalAddress::setStateDistrict()
     * @uses GlobalAddress::setPOBox()
     * @uses GlobalAddress::setRegion()
     * @uses GlobalAddress::setPrincipality()
     * @uses GlobalAddress::setZipPostcode()
     * @uses GlobalAddress::setDpsZipPlus()
     * @uses GlobalAddress::setCedexMailsort()
     * @uses GlobalAddress::setDepartment()
     * @uses GlobalAddress::setCompany()
     * @uses GlobalAddress::setBuilding()
     * @uses GlobalAddress::setSubBuilding()
     * @uses GlobalAddress::setPremise()
     * @uses GlobalAddress::setAddressLine1()
     * @uses GlobalAddress::setAddressLine2()
     * @uses GlobalAddress::setAddressLine3()
     * @uses GlobalAddress::setAddressLine4()
     * @uses GlobalAddress::setAddressLine5()
     * @uses GlobalAddress::setAddressLine6()
     * @uses GlobalAddress::setAddressLine7()
     * @uses GlobalAddress::setAddressLine8()
     * @uses GlobalAddress::setFirstYearOfResidence()
     * @uses GlobalAddress::setLastYearOfResidence()
     * @uses GlobalAddress::setFirstMonthOfResidence()
     * @uses GlobalAddress::setLastMonthOfResidence()
     * @param string $country
     * @param string $street
     * @param string $subStreet
     * @param string $city
     * @param string $subCity
     * @param string $stateDistrict
     * @param string $pOBox
     * @param string $region
     * @param string $principality
     * @param string $zipPostcode
     * @param string $dpsZipPlus
     * @param string $cedexMailsort
     * @param string $department
     * @param string $company
     * @param string $building
     * @param string $subBuilding
     * @param string $premise
     * @param string $addressLine1
     * @param string $addressLine2
     * @param string $addressLine3
     * @param string $addressLine4
     * @param string $addressLine5
     * @param string $addressLine6
     * @param string $addressLine7
     * @param string $addressLine8
     * @param int $firstYearOfResidence
     * @param int $lastYearOfResidence
     * @param int $firstMonthOfResidence
     * @param int $lastMonthOfResidence
     */
    public function __construct(?string $country = null, ?string $street = null, ?string $subStreet = null, ?string $city = null, ?string $subCity = null, ?string $stateDistrict = null, ?string $pOBox = null, ?string $region = null, ?string $principality = null, ?string $zipPostcode = null, ?string $dpsZipPlus = null, ?string $cedexMailsort = null, ?string $department = null, ?string $company = null, ?string $building = null, ?string $subBuilding = null, ?string $premise = null, ?string $addressLine1 = null, ?string $addressLine2 = null, ?string $addressLine3 = null, ?string $addressLine4 = null, ?string $addressLine5 = null, ?string $addressLine6 = null, ?string $addressLine7 = null, ?string $addressLine8 = null, ?int $firstYearOfResidence = null, ?int $lastYearOfResidence = null, ?int $firstMonthOfResidence = null, ?int $lastMonthOfResidence = null)
    {
        $this
            ->setCountry($country)
            ->setStreet($street)
            ->setSubStreet($subStreet)
            ->setCity($city)
            ->setSubCity($subCity)
            ->setStateDistrict($stateDistrict)
            ->setPOBox($pOBox)
            ->setRegion($region)
            ->setPrincipality($principality)
            ->setZipPostcode($zipPostcode)
            ->setDpsZipPlus($dpsZipPlus)
            ->setCedexMailsort($cedexMailsort)
            ->setDepartment($department)
            ->setCompany($company)
            ->setBuilding($building)
            ->setSubBuilding($subBuilding)
            ->setPremise($premise)
            ->setAddressLine1($addressLine1)
            ->setAddressLine2($addressLine2)
            ->setAddressLine3($addressLine3)
            ->setAddressLine4($addressLine4)
            ->setAddressLine5($addressLine5)
            ->setAddressLine6($addressLine6)
            ->setAddressLine7($addressLine7)
            ->setAddressLine8($addressLine8)
            ->setFirstYearOfResidence($firstYearOfResidence)
            ->setLastYearOfResidence($lastYearOfResidence)
            ->setFirstMonthOfResidence($firstMonthOfResidence)
            ->setLastMonthOfResidence($lastMonthOfResidence);
    }
    /**
     * Get Country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return isset($this->Country) ? $this->Country : null;
    }
    /**
     * Set Country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $country
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->Country);
        } else {
            $this->Country = $country;
        }
        
        return $this;
    }
    /**
     * Get Street value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return isset($this->Street) ? $this->Street : null;
    }
    /**
     * Set Street value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $street
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setStreet(?string $street = null): self
    {
        // validation for constraint: string
        if (!is_null($street) && !is_string($street)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($street, true), gettype($street)), __LINE__);
        }
        if (is_null($street) || (is_array($street) && empty($street))) {
            unset($this->Street);
        } else {
            $this->Street = $street;
        }
        
        return $this;
    }
    /**
     * Get SubStreet value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSubStreet(): ?string
    {
        return isset($this->SubStreet) ? $this->SubStreet : null;
    }
    /**
     * Set SubStreet value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $subStreet
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setSubStreet(?string $subStreet = null): self
    {
        // validation for constraint: string
        if (!is_null($subStreet) && !is_string($subStreet)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subStreet, true), gettype($subStreet)), __LINE__);
        }
        if (is_null($subStreet) || (is_array($subStreet) && empty($subStreet))) {
            unset($this->SubStreet);
        } else {
            $this->SubStreet = $subStreet;
        }
        
        return $this;
    }
    /**
     * Get City value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCity(): ?string
    {
        return isset($this->City) ? $this->City : null;
    }
    /**
     * Set City value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $city
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setCity(?string $city = null): self
    {
        // validation for constraint: string
        if (!is_null($city) && !is_string($city)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($city, true), gettype($city)), __LINE__);
        }
        if (is_null($city) || (is_array($city) && empty($city))) {
            unset($this->City);
        } else {
            $this->City = $city;
        }
        
        return $this;
    }
    /**
     * Get SubCity value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSubCity(): ?string
    {
        return isset($this->SubCity) ? $this->SubCity : null;
    }
    /**
     * Set SubCity value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $subCity
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setSubCity(?string $subCity = null): self
    {
        // validation for constraint: string
        if (!is_null($subCity) && !is_string($subCity)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subCity, true), gettype($subCity)), __LINE__);
        }
        if (is_null($subCity) || (is_array($subCity) && empty($subCity))) {
            unset($this->SubCity);
        } else {
            $this->SubCity = $subCity;
        }
        
        return $this;
    }
    /**
     * Get StateDistrict value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStateDistrict(): ?string
    {
        return isset($this->StateDistrict) ? $this->StateDistrict : null;
    }
    /**
     * Set StateDistrict value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $stateDistrict
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setStateDistrict(?string $stateDistrict = null): self
    {
        // validation for constraint: string
        if (!is_null($stateDistrict) && !is_string($stateDistrict)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($stateDistrict, true), gettype($stateDistrict)), __LINE__);
        }
        if (is_null($stateDistrict) || (is_array($stateDistrict) && empty($stateDistrict))) {
            unset($this->StateDistrict);
        } else {
            $this->StateDistrict = $stateDistrict;
        }
        
        return $this;
    }
    /**
     * Get POBox value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPOBox(): ?string
    {
        return isset($this->POBox) ? $this->POBox : null;
    }
    /**
     * Set POBox value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $pOBox
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setPOBox(?string $pOBox = null): self
    {
        // validation for constraint: string
        if (!is_null($pOBox) && !is_string($pOBox)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pOBox, true), gettype($pOBox)), __LINE__);
        }
        if (is_null($pOBox) || (is_array($pOBox) && empty($pOBox))) {
            unset($this->POBox);
        } else {
            $this->POBox = $pOBox;
        }
        
        return $this;
    }
    /**
     * Get Region value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return isset($this->Region) ? $this->Region : null;
    }
    /**
     * Set Region value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $region
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setRegion(?string $region = null): self
    {
        // validation for constraint: string
        if (!is_null($region) && !is_string($region)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($region, true), gettype($region)), __LINE__);
        }
        if (is_null($region) || (is_array($region) && empty($region))) {
            unset($this->Region);
        } else {
            $this->Region = $region;
        }
        
        return $this;
    }
    /**
     * Get Principality value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPrincipality(): ?string
    {
        return isset($this->Principality) ? $this->Principality : null;
    }
    /**
     * Set Principality value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $principality
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setPrincipality(?string $principality = null): self
    {
        // validation for constraint: string
        if (!is_null($principality) && !is_string($principality)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($principality, true), gettype($principality)), __LINE__);
        }
        if (is_null($principality) || (is_array($principality) && empty($principality))) {
            unset($this->Principality);
        } else {
            $this->Principality = $principality;
        }
        
        return $this;
    }
    /**
     * Get ZipPostcode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getZipPostcode(): ?string
    {
        return isset($this->ZipPostcode) ? $this->ZipPostcode : null;
    }
    /**
     * Set ZipPostcode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $zipPostcode
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setZipPostcode(?string $zipPostcode = null): self
    {
        // validation for constraint: string
        if (!is_null($zipPostcode) && !is_string($zipPostcode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipPostcode, true), gettype($zipPostcode)), __LINE__);
        }
        if (is_null($zipPostcode) || (is_array($zipPostcode) && empty($zipPostcode))) {
            unset($this->ZipPostcode);
        } else {
            $this->ZipPostcode = $zipPostcode;
        }
        
        return $this;
    }
    /**
     * Get DpsZipPlus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDpsZipPlus(): ?string
    {
        return isset($this->DpsZipPlus) ? $this->DpsZipPlus : null;
    }
    /**
     * Set DpsZipPlus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dpsZipPlus
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setDpsZipPlus(?string $dpsZipPlus = null): self
    {
        // validation for constraint: string
        if (!is_null($dpsZipPlus) && !is_string($dpsZipPlus)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dpsZipPlus, true), gettype($dpsZipPlus)), __LINE__);
        }
        if (is_null($dpsZipPlus) || (is_array($dpsZipPlus) && empty($dpsZipPlus))) {
            unset($this->DpsZipPlus);
        } else {
            $this->DpsZipPlus = $dpsZipPlus;
        }
        
        return $this;
    }
    /**
     * Get CedexMailsort value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCedexMailsort(): ?string
    {
        return isset($this->CedexMailsort) ? $this->CedexMailsort : null;
    }
    /**
     * Set CedexMailsort value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $cedexMailsort
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setCedexMailsort(?string $cedexMailsort = null): self
    {
        // validation for constraint: string
        if (!is_null($cedexMailsort) && !is_string($cedexMailsort)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cedexMailsort, true), gettype($cedexMailsort)), __LINE__);
        }
        if (is_null($cedexMailsort) || (is_array($cedexMailsort) && empty($cedexMailsort))) {
            unset($this->CedexMailsort);
        } else {
            $this->CedexMailsort = $cedexMailsort;
        }
        
        return $this;
    }
    /**
     * Get Department value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDepartment(): ?string
    {
        return isset($this->Department) ? $this->Department : null;
    }
    /**
     * Set Department value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $department
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setDepartment(?string $department = null): self
    {
        // validation for constraint: string
        if (!is_null($department) && !is_string($department)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($department, true), gettype($department)), __LINE__);
        }
        if (is_null($department) || (is_array($department) && empty($department))) {
            unset($this->Department);
        } else {
            $this->Department = $department;
        }
        
        return $this;
    }
    /**
     * Get Company value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCompany(): ?string
    {
        return isset($this->Company) ? $this->Company : null;
    }
    /**
     * Set Company value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $company
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setCompany(?string $company = null): self
    {
        // validation for constraint: string
        if (!is_null($company) && !is_string($company)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($company, true), gettype($company)), __LINE__);
        }
        if (is_null($company) || (is_array($company) && empty($company))) {
            unset($this->Company);
        } else {
            $this->Company = $company;
        }
        
        return $this;
    }
    /**
     * Get Building value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBuilding(): ?string
    {
        return isset($this->Building) ? $this->Building : null;
    }
    /**
     * Set Building value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $building
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setBuilding(?string $building = null): self
    {
        // validation for constraint: string
        if (!is_null($building) && !is_string($building)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($building, true), gettype($building)), __LINE__);
        }
        if (is_null($building) || (is_array($building) && empty($building))) {
            unset($this->Building);
        } else {
            $this->Building = $building;
        }
        
        return $this;
    }
    /**
     * Get SubBuilding value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSubBuilding(): ?string
    {
        return isset($this->SubBuilding) ? $this->SubBuilding : null;
    }
    /**
     * Set SubBuilding value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $subBuilding
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setSubBuilding(?string $subBuilding = null): self
    {
        // validation for constraint: string
        if (!is_null($subBuilding) && !is_string($subBuilding)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subBuilding, true), gettype($subBuilding)), __LINE__);
        }
        if (is_null($subBuilding) || (is_array($subBuilding) && empty($subBuilding))) {
            unset($this->SubBuilding);
        } else {
            $this->SubBuilding = $subBuilding;
        }
        
        return $this;
    }
    /**
     * Get Premise value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPremise(): ?string
    {
        return isset($this->Premise) ? $this->Premise : null;
    }
    /**
     * Set Premise value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $premise
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setPremise(?string $premise = null): self
    {
        // validation for constraint: string
        if (!is_null($premise) && !is_string($premise)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($premise, true), gettype($premise)), __LINE__);
        }
        if (is_null($premise) || (is_array($premise) && empty($premise))) {
            unset($this->Premise);
        } else {
            $this->Premise = $premise;
        }
        
        return $this;
    }
    /**
     * Get AddressLine1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLine1(): ?string
    {
        return isset($this->AddressLine1) ? $this->AddressLine1 : null;
    }
    /**
     * Set AddressLine1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLine1
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setAddressLine1(?string $addressLine1 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine1) && !is_string($addressLine1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine1, true), gettype($addressLine1)), __LINE__);
        }
        if (is_null($addressLine1) || (is_array($addressLine1) && empty($addressLine1))) {
            unset($this->AddressLine1);
        } else {
            $this->AddressLine1 = $addressLine1;
        }
        
        return $this;
    }
    /**
     * Get AddressLine2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLine2(): ?string
    {
        return isset($this->AddressLine2) ? $this->AddressLine2 : null;
    }
    /**
     * Set AddressLine2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLine2
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setAddressLine2(?string $addressLine2 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine2) && !is_string($addressLine2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine2, true), gettype($addressLine2)), __LINE__);
        }
        if (is_null($addressLine2) || (is_array($addressLine2) && empty($addressLine2))) {
            unset($this->AddressLine2);
        } else {
            $this->AddressLine2 = $addressLine2;
        }
        
        return $this;
    }
    /**
     * Get AddressLine3 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLine3(): ?string
    {
        return isset($this->AddressLine3) ? $this->AddressLine3 : null;
    }
    /**
     * Set AddressLine3 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLine3
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setAddressLine3(?string $addressLine3 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine3) && !is_string($addressLine3)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine3, true), gettype($addressLine3)), __LINE__);
        }
        if (is_null($addressLine3) || (is_array($addressLine3) && empty($addressLine3))) {
            unset($this->AddressLine3);
        } else {
            $this->AddressLine3 = $addressLine3;
        }
        
        return $this;
    }
    /**
     * Get AddressLine4 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLine4(): ?string
    {
        return isset($this->AddressLine4) ? $this->AddressLine4 : null;
    }
    /**
     * Set AddressLine4 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLine4
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setAddressLine4(?string $addressLine4 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine4) && !is_string($addressLine4)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine4, true), gettype($addressLine4)), __LINE__);
        }
        if (is_null($addressLine4) || (is_array($addressLine4) && empty($addressLine4))) {
            unset($this->AddressLine4);
        } else {
            $this->AddressLine4 = $addressLine4;
        }
        
        return $this;
    }
    /**
     * Get AddressLine5 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLine5(): ?string
    {
        return isset($this->AddressLine5) ? $this->AddressLine5 : null;
    }
    /**
     * Set AddressLine5 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLine5
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setAddressLine5(?string $addressLine5 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine5) && !is_string($addressLine5)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine5, true), gettype($addressLine5)), __LINE__);
        }
        if (is_null($addressLine5) || (is_array($addressLine5) && empty($addressLine5))) {
            unset($this->AddressLine5);
        } else {
            $this->AddressLine5 = $addressLine5;
        }
        
        return $this;
    }
    /**
     * Get AddressLine6 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLine6(): ?string
    {
        return isset($this->AddressLine6) ? $this->AddressLine6 : null;
    }
    /**
     * Set AddressLine6 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLine6
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setAddressLine6(?string $addressLine6 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine6) && !is_string($addressLine6)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine6, true), gettype($addressLine6)), __LINE__);
        }
        if (is_null($addressLine6) || (is_array($addressLine6) && empty($addressLine6))) {
            unset($this->AddressLine6);
        } else {
            $this->AddressLine6 = $addressLine6;
        }
        
        return $this;
    }
    /**
     * Get AddressLine7 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLine7(): ?string
    {
        return isset($this->AddressLine7) ? $this->AddressLine7 : null;
    }
    /**
     * Set AddressLine7 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLine7
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setAddressLine7(?string $addressLine7 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine7) && !is_string($addressLine7)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine7, true), gettype($addressLine7)), __LINE__);
        }
        if (is_null($addressLine7) || (is_array($addressLine7) && empty($addressLine7))) {
            unset($this->AddressLine7);
        } else {
            $this->AddressLine7 = $addressLine7;
        }
        
        return $this;
    }
    /**
     * Get AddressLine8 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLine8(): ?string
    {
        return isset($this->AddressLine8) ? $this->AddressLine8 : null;
    }
    /**
     * Set AddressLine8 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLine8
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setAddressLine8(?string $addressLine8 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine8) && !is_string($addressLine8)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine8, true), gettype($addressLine8)), __LINE__);
        }
        if (is_null($addressLine8) || (is_array($addressLine8) && empty($addressLine8))) {
            unset($this->AddressLine8);
        } else {
            $this->AddressLine8 = $addressLine8;
        }
        
        return $this;
    }
    /**
     * Get FirstYearOfResidence value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getFirstYearOfResidence(): ?int
    {
        return isset($this->FirstYearOfResidence) ? $this->FirstYearOfResidence : null;
    }
    /**
     * Set FirstYearOfResidence value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $firstYearOfResidence
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setFirstYearOfResidence(?int $firstYearOfResidence = null): self
    {
        // validation for constraint: int
        if (!is_null($firstYearOfResidence) && !(is_int($firstYearOfResidence) || ctype_digit($firstYearOfResidence))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($firstYearOfResidence, true), gettype($firstYearOfResidence)), __LINE__);
        }
        if (is_null($firstYearOfResidence) || (is_array($firstYearOfResidence) && empty($firstYearOfResidence))) {
            unset($this->FirstYearOfResidence);
        } else {
            $this->FirstYearOfResidence = $firstYearOfResidence;
        }
        
        return $this;
    }
    /**
     * Get LastYearOfResidence value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getLastYearOfResidence(): ?int
    {
        return isset($this->LastYearOfResidence) ? $this->LastYearOfResidence : null;
    }
    /**
     * Set LastYearOfResidence value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $lastYearOfResidence
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setLastYearOfResidence(?int $lastYearOfResidence = null): self
    {
        // validation for constraint: int
        if (!is_null($lastYearOfResidence) && !(is_int($lastYearOfResidence) || ctype_digit($lastYearOfResidence))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($lastYearOfResidence, true), gettype($lastYearOfResidence)), __LINE__);
        }
        if (is_null($lastYearOfResidence) || (is_array($lastYearOfResidence) && empty($lastYearOfResidence))) {
            unset($this->LastYearOfResidence);
        } else {
            $this->LastYearOfResidence = $lastYearOfResidence;
        }
        
        return $this;
    }
    /**
     * Get FirstMonthOfResidence value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getFirstMonthOfResidence(): ?int
    {
        return isset($this->FirstMonthOfResidence) ? $this->FirstMonthOfResidence : null;
    }
    /**
     * Set FirstMonthOfResidence value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $firstMonthOfResidence
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setFirstMonthOfResidence(?int $firstMonthOfResidence = null): self
    {
        // validation for constraint: int
        if (!is_null($firstMonthOfResidence) && !(is_int($firstMonthOfResidence) || ctype_digit($firstMonthOfResidence))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($firstMonthOfResidence, true), gettype($firstMonthOfResidence)), __LINE__);
        }
        if (is_null($firstMonthOfResidence) || (is_array($firstMonthOfResidence) && empty($firstMonthOfResidence))) {
            unset($this->FirstMonthOfResidence);
        } else {
            $this->FirstMonthOfResidence = $firstMonthOfResidence;
        }
        
        return $this;
    }
    /**
     * Get LastMonthOfResidence value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getLastMonthOfResidence(): ?int
    {
        return isset($this->LastMonthOfResidence) ? $this->LastMonthOfResidence : null;
    }
    /**
     * Set LastMonthOfResidence value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $lastMonthOfResidence
     * @return \ID3Global\Models\GlobalAddress
     */
    public function setLastMonthOfResidence(?int $lastMonthOfResidence = null): self
    {
        // validation for constraint: int
        if (!is_null($lastMonthOfResidence) && !(is_int($lastMonthOfResidence) || ctype_digit($lastMonthOfResidence))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($lastMonthOfResidence, true), gettype($lastMonthOfResidence)), __LINE__);
        }
        if (is_null($lastMonthOfResidence) || (is_array($lastMonthOfResidence) && empty($lastMonthOfResidence))) {
            unset($this->LastMonthOfResidence);
        } else {
            $this->LastMonthOfResidence = $lastMonthOfResidence;
        }
        
        return $this;
    }
}
