<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalResource Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q575:GlobalResource
 * @subpackage Structs
 */
class GlobalResource extends AbstractStructBase
{
    /**
     * The ResourceID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ResourceID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Permission
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Permission = null;
    /**
     * Constructor method for GlobalResource
     * @uses GlobalResource::setResourceID()
     * @uses GlobalResource::setName()
     * @uses GlobalResource::setPermission()
     * @param string $resourceID
     * @param string $name
     * @param string $permission
     */
    public function __construct(?string $resourceID = null, ?string $name = null, ?string $permission = null)
    {
        $this
            ->setResourceID($resourceID)
            ->setName($name)
            ->setPermission($permission);
    }
    /**
     * Get ResourceID value
     * @return string|null
     */
    public function getResourceID(): ?string
    {
        return $this->ResourceID;
    }
    /**
     * Set ResourceID value
     * @param string $resourceID
     * @return \ID3Global\Models\GlobalResource
     */
    public function setResourceID(?string $resourceID = null): self
    {
        // validation for constraint: string
        if (!is_null($resourceID) && !is_string($resourceID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($resourceID, true), gettype($resourceID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($resourceID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $resourceID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($resourceID, true)), __LINE__);
        }
        $this->ResourceID = $resourceID;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalResource
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Permission value
     * @return string|null
     */
    public function getPermission(): ?string
    {
        return $this->Permission;
    }
    /**
     * Set Permission value
     * @uses \ID3Global\Enums\GlobalPermission::valueIsValid()
     * @uses \ID3Global\Enums\GlobalPermission::getValidValues()
     * @throws InvalidArgumentException
     * @param string $permission
     * @return \ID3Global\Models\GlobalResource
     */
    public function setPermission(?string $permission = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalPermission::valueIsValid($permission)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalPermission', is_array($permission) ? implode(', ', $permission) : var_export($permission, true), implode(', ', \ID3Global\Enums\GlobalPermission::getValidValues())), __LINE__);
        }
        $this->Permission = $permission;
        
        return $this;
    }
}
