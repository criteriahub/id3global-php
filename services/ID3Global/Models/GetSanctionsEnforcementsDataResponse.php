<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSanctionsEnforcementsDataResponse Models
 * @subpackage Structs
 */
class GetSanctionsEnforcementsDataResponse extends AbstractStructBase
{
    /**
     * The GetSanctionsEnforcementsDataResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSanctionsEnforcementData|null
     */
    protected ?\ID3Global\Models\GlobalSanctionsEnforcementData $GetSanctionsEnforcementsDataResult = null;
    /**
     * Constructor method for GetSanctionsEnforcementsDataResponse
     * @uses GetSanctionsEnforcementsDataResponse::setGetSanctionsEnforcementsDataResult()
     * @param \ID3Global\Models\GlobalSanctionsEnforcementData $getSanctionsEnforcementsDataResult
     */
    public function __construct(?\ID3Global\Models\GlobalSanctionsEnforcementData $getSanctionsEnforcementsDataResult = null)
    {
        $this
            ->setGetSanctionsEnforcementsDataResult($getSanctionsEnforcementsDataResult);
    }
    /**
     * Get GetSanctionsEnforcementsDataResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSanctionsEnforcementData|null
     */
    public function getGetSanctionsEnforcementsDataResult(): ?\ID3Global\Models\GlobalSanctionsEnforcementData
    {
        return isset($this->GetSanctionsEnforcementsDataResult) ? $this->GetSanctionsEnforcementsDataResult : null;
    }
    /**
     * Set GetSanctionsEnforcementsDataResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSanctionsEnforcementData $getSanctionsEnforcementsDataResult
     * @return \ID3Global\Models\GetSanctionsEnforcementsDataResponse
     */
    public function setGetSanctionsEnforcementsDataResult(?\ID3Global\Models\GlobalSanctionsEnforcementData $getSanctionsEnforcementsDataResult = null): self
    {
        if (is_null($getSanctionsEnforcementsDataResult) || (is_array($getSanctionsEnforcementsDataResult) && empty($getSanctionsEnforcementsDataResult))) {
            unset($this->GetSanctionsEnforcementsDataResult);
        } else {
            $this->GetSanctionsEnforcementsDataResult = $getSanctionsEnforcementsDataResult;
        }
        
        return $this;
    }
}
