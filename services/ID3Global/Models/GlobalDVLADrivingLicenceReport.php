<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDVLADrivingLicenceReport Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q779:GlobalDVLADrivingLicenceReport
 * @subpackage Structs
 */
class GlobalDVLADrivingLicenceReport extends AbstractStructBase
{
    /**
     * The Surname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Surname = null;
    /**
     * The LicenceNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $LicenceNumber = null;
    /**
     * The Forenames
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Forenames = null;
    /**
     * The Gender
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Gender = null;
    /**
     * The DateOfBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DateOfBirth = null;
    /**
     * The LicenseType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $LicenseType = null;
    /**
     * The TypeLiteral
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $TypeLiteral = null;
    /**
     * The Disqualified
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Disqualified = null;
    /**
     * The ExpiryDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ExpiryDate = null;
    /**
     * The Status
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Status = null;
    /**
     * The IssueNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $IssueNumber = null;
    /**
     * The Postcode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Postcode = null;
    /**
     * The AddressLine1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLine1 = null;
    /**
     * The AddressLine2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLine2 = null;
    /**
     * The AddressLine3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLine3 = null;
    /**
     * The AddressRegion
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressRegion = null;
    /**
     * The AddressCity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressCity = null;
    /**
     * The Categories
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportCategory|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportCategory $Categories = null;
    /**
     * The Endorsements
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportEndorsement|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportEndorsement $Endorsements = null;
    /**
     * The DisqualifiedUntil
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DisqualifiedUntil = null;
    /**
     * The NilReturn
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $NilReturn = null;
    /**
     * Constructor method for GlobalDVLADrivingLicenceReport
     * @uses GlobalDVLADrivingLicenceReport::setSurname()
     * @uses GlobalDVLADrivingLicenceReport::setLicenceNumber()
     * @uses GlobalDVLADrivingLicenceReport::setForenames()
     * @uses GlobalDVLADrivingLicenceReport::setGender()
     * @uses GlobalDVLADrivingLicenceReport::setDateOfBirth()
     * @uses GlobalDVLADrivingLicenceReport::setLicenseType()
     * @uses GlobalDVLADrivingLicenceReport::setTypeLiteral()
     * @uses GlobalDVLADrivingLicenceReport::setDisqualified()
     * @uses GlobalDVLADrivingLicenceReport::setExpiryDate()
     * @uses GlobalDVLADrivingLicenceReport::setStatus()
     * @uses GlobalDVLADrivingLicenceReport::setIssueNumber()
     * @uses GlobalDVLADrivingLicenceReport::setPostcode()
     * @uses GlobalDVLADrivingLicenceReport::setAddressLine1()
     * @uses GlobalDVLADrivingLicenceReport::setAddressLine2()
     * @uses GlobalDVLADrivingLicenceReport::setAddressLine3()
     * @uses GlobalDVLADrivingLicenceReport::setAddressRegion()
     * @uses GlobalDVLADrivingLicenceReport::setAddressCity()
     * @uses GlobalDVLADrivingLicenceReport::setCategories()
     * @uses GlobalDVLADrivingLicenceReport::setEndorsements()
     * @uses GlobalDVLADrivingLicenceReport::setDisqualifiedUntil()
     * @uses GlobalDVLADrivingLicenceReport::setNilReturn()
     * @param string $surname
     * @param string $licenceNumber
     * @param string $forenames
     * @param string $gender
     * @param string $dateOfBirth
     * @param string $licenseType
     * @param string $typeLiteral
     * @param bool $disqualified
     * @param string $expiryDate
     * @param string $status
     * @param int $issueNumber
     * @param string $postcode
     * @param string $addressLine1
     * @param string $addressLine2
     * @param string $addressLine3
     * @param string $addressRegion
     * @param string $addressCity
     * @param \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportCategory $categories
     * @param \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportEndorsement $endorsements
     * @param string $disqualifiedUntil
     * @param int $nilReturn
     */
    public function __construct(?string $surname = null, ?string $licenceNumber = null, ?string $forenames = null, ?string $gender = null, ?string $dateOfBirth = null, ?string $licenseType = null, ?string $typeLiteral = null, ?bool $disqualified = null, ?string $expiryDate = null, ?string $status = null, ?int $issueNumber = null, ?string $postcode = null, ?string $addressLine1 = null, ?string $addressLine2 = null, ?string $addressLine3 = null, ?string $addressRegion = null, ?string $addressCity = null, ?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportCategory $categories = null, ?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportEndorsement $endorsements = null, ?string $disqualifiedUntil = null, ?int $nilReturn = null)
    {
        $this
            ->setSurname($surname)
            ->setLicenceNumber($licenceNumber)
            ->setForenames($forenames)
            ->setGender($gender)
            ->setDateOfBirth($dateOfBirth)
            ->setLicenseType($licenseType)
            ->setTypeLiteral($typeLiteral)
            ->setDisqualified($disqualified)
            ->setExpiryDate($expiryDate)
            ->setStatus($status)
            ->setIssueNumber($issueNumber)
            ->setPostcode($postcode)
            ->setAddressLine1($addressLine1)
            ->setAddressLine2($addressLine2)
            ->setAddressLine3($addressLine3)
            ->setAddressRegion($addressRegion)
            ->setAddressCity($addressCity)
            ->setCategories($categories)
            ->setEndorsements($endorsements)
            ->setDisqualifiedUntil($disqualifiedUntil)
            ->setNilReturn($nilReturn);
    }
    /**
     * Get Surname value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return isset($this->Surname) ? $this->Surname : null;
    }
    /**
     * Set Surname value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $surname
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setSurname(?string $surname = null): self
    {
        // validation for constraint: string
        if (!is_null($surname) && !is_string($surname)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($surname, true), gettype($surname)), __LINE__);
        }
        if (is_null($surname) || (is_array($surname) && empty($surname))) {
            unset($this->Surname);
        } else {
            $this->Surname = $surname;
        }
        
        return $this;
    }
    /**
     * Get LicenceNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLicenceNumber(): ?string
    {
        return isset($this->LicenceNumber) ? $this->LicenceNumber : null;
    }
    /**
     * Set LicenceNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $licenceNumber
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setLicenceNumber(?string $licenceNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($licenceNumber) && !is_string($licenceNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($licenceNumber, true), gettype($licenceNumber)), __LINE__);
        }
        if (is_null($licenceNumber) || (is_array($licenceNumber) && empty($licenceNumber))) {
            unset($this->LicenceNumber);
        } else {
            $this->LicenceNumber = $licenceNumber;
        }
        
        return $this;
    }
    /**
     * Get Forenames value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getForenames(): ?string
    {
        return isset($this->Forenames) ? $this->Forenames : null;
    }
    /**
     * Set Forenames value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $forenames
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setForenames(?string $forenames = null): self
    {
        // validation for constraint: string
        if (!is_null($forenames) && !is_string($forenames)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($forenames, true), gettype($forenames)), __LINE__);
        }
        if (is_null($forenames) || (is_array($forenames) && empty($forenames))) {
            unset($this->Forenames);
        } else {
            $this->Forenames = $forenames;
        }
        
        return $this;
    }
    /**
     * Get Gender value
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->Gender;
    }
    /**
     * Set Gender value
     * @uses \ID3Global\Enums\GlobalGender::valueIsValid()
     * @uses \ID3Global\Enums\GlobalGender::getValidValues()
     * @throws InvalidArgumentException
     * @param string $gender
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setGender(?string $gender = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalGender::valueIsValid($gender)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalGender', is_array($gender) ? implode(', ', $gender) : var_export($gender, true), implode(', ', \ID3Global\Enums\GlobalGender::getValidValues())), __LINE__);
        }
        $this->Gender = $gender;
        
        return $this;
    }
    /**
     * Get DateOfBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDateOfBirth(): ?string
    {
        return isset($this->DateOfBirth) ? $this->DateOfBirth : null;
    }
    /**
     * Set DateOfBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dateOfBirth
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setDateOfBirth(?string $dateOfBirth = null): self
    {
        // validation for constraint: string
        if (!is_null($dateOfBirth) && !is_string($dateOfBirth)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateOfBirth, true), gettype($dateOfBirth)), __LINE__);
        }
        if (is_null($dateOfBirth) || (is_array($dateOfBirth) && empty($dateOfBirth))) {
            unset($this->DateOfBirth);
        } else {
            $this->DateOfBirth = $dateOfBirth;
        }
        
        return $this;
    }
    /**
     * Get LicenseType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLicenseType(): ?string
    {
        return isset($this->LicenseType) ? $this->LicenseType : null;
    }
    /**
     * Set LicenseType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $licenseType
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setLicenseType(?string $licenseType = null): self
    {
        // validation for constraint: string
        if (!is_null($licenseType) && !is_string($licenseType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($licenseType, true), gettype($licenseType)), __LINE__);
        }
        if (is_null($licenseType) || (is_array($licenseType) && empty($licenseType))) {
            unset($this->LicenseType);
        } else {
            $this->LicenseType = $licenseType;
        }
        
        return $this;
    }
    /**
     * Get TypeLiteral value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTypeLiteral(): ?string
    {
        return isset($this->TypeLiteral) ? $this->TypeLiteral : null;
    }
    /**
     * Set TypeLiteral value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $typeLiteral
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setTypeLiteral(?string $typeLiteral = null): self
    {
        // validation for constraint: string
        if (!is_null($typeLiteral) && !is_string($typeLiteral)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typeLiteral, true), gettype($typeLiteral)), __LINE__);
        }
        if (is_null($typeLiteral) || (is_array($typeLiteral) && empty($typeLiteral))) {
            unset($this->TypeLiteral);
        } else {
            $this->TypeLiteral = $typeLiteral;
        }
        
        return $this;
    }
    /**
     * Get Disqualified value
     * @return bool|null
     */
    public function getDisqualified(): ?bool
    {
        return $this->Disqualified;
    }
    /**
     * Set Disqualified value
     * @param bool $disqualified
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setDisqualified(?bool $disqualified = null): self
    {
        // validation for constraint: boolean
        if (!is_null($disqualified) && !is_bool($disqualified)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($disqualified, true), gettype($disqualified)), __LINE__);
        }
        $this->Disqualified = $disqualified;
        
        return $this;
    }
    /**
     * Get ExpiryDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getExpiryDate(): ?string
    {
        return isset($this->ExpiryDate) ? $this->ExpiryDate : null;
    }
    /**
     * Set ExpiryDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $expiryDate
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setExpiryDate(?string $expiryDate = null): self
    {
        // validation for constraint: string
        if (!is_null($expiryDate) && !is_string($expiryDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expiryDate, true), gettype($expiryDate)), __LINE__);
        }
        if (is_null($expiryDate) || (is_array($expiryDate) && empty($expiryDate))) {
            unset($this->ExpiryDate);
        } else {
            $this->ExpiryDate = $expiryDate;
        }
        
        return $this;
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @uses \ID3Global\Enums\GlobalDispatchReportStatus::valueIsValid()
     * @uses \ID3Global\Enums\GlobalDispatchReportStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $status
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalDispatchReportStatus::valueIsValid($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalDispatchReportStatus', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \ID3Global\Enums\GlobalDispatchReportStatus::getValidValues())), __LINE__);
        }
        $this->Status = $status;
        
        return $this;
    }
    /**
     * Get IssueNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getIssueNumber(): ?int
    {
        return isset($this->IssueNumber) ? $this->IssueNumber : null;
    }
    /**
     * Set IssueNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $issueNumber
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setIssueNumber(?int $issueNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($issueNumber) && !(is_int($issueNumber) || ctype_digit($issueNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($issueNumber, true), gettype($issueNumber)), __LINE__);
        }
        if (is_null($issueNumber) || (is_array($issueNumber) && empty($issueNumber))) {
            unset($this->IssueNumber);
        } else {
            $this->IssueNumber = $issueNumber;
        }
        
        return $this;
    }
    /**
     * Get Postcode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPostcode(): ?string
    {
        return isset($this->Postcode) ? $this->Postcode : null;
    }
    /**
     * Set Postcode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $postcode
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setPostcode(?string $postcode = null): self
    {
        // validation for constraint: string
        if (!is_null($postcode) && !is_string($postcode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($postcode, true), gettype($postcode)), __LINE__);
        }
        if (is_null($postcode) || (is_array($postcode) && empty($postcode))) {
            unset($this->Postcode);
        } else {
            $this->Postcode = $postcode;
        }
        
        return $this;
    }
    /**
     * Get AddressLine1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLine1(): ?string
    {
        return isset($this->AddressLine1) ? $this->AddressLine1 : null;
    }
    /**
     * Set AddressLine1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLine1
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setAddressLine1(?string $addressLine1 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine1) && !is_string($addressLine1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine1, true), gettype($addressLine1)), __LINE__);
        }
        if (is_null($addressLine1) || (is_array($addressLine1) && empty($addressLine1))) {
            unset($this->AddressLine1);
        } else {
            $this->AddressLine1 = $addressLine1;
        }
        
        return $this;
    }
    /**
     * Get AddressLine2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLine2(): ?string
    {
        return isset($this->AddressLine2) ? $this->AddressLine2 : null;
    }
    /**
     * Set AddressLine2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLine2
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setAddressLine2(?string $addressLine2 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine2) && !is_string($addressLine2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine2, true), gettype($addressLine2)), __LINE__);
        }
        if (is_null($addressLine2) || (is_array($addressLine2) && empty($addressLine2))) {
            unset($this->AddressLine2);
        } else {
            $this->AddressLine2 = $addressLine2;
        }
        
        return $this;
    }
    /**
     * Get AddressLine3 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLine3(): ?string
    {
        return isset($this->AddressLine3) ? $this->AddressLine3 : null;
    }
    /**
     * Set AddressLine3 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLine3
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setAddressLine3(?string $addressLine3 = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLine3) && !is_string($addressLine3)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLine3, true), gettype($addressLine3)), __LINE__);
        }
        if (is_null($addressLine3) || (is_array($addressLine3) && empty($addressLine3))) {
            unset($this->AddressLine3);
        } else {
            $this->AddressLine3 = $addressLine3;
        }
        
        return $this;
    }
    /**
     * Get AddressRegion value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressRegion(): ?string
    {
        return isset($this->AddressRegion) ? $this->AddressRegion : null;
    }
    /**
     * Set AddressRegion value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressRegion
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setAddressRegion(?string $addressRegion = null): self
    {
        // validation for constraint: string
        if (!is_null($addressRegion) && !is_string($addressRegion)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressRegion, true), gettype($addressRegion)), __LINE__);
        }
        if (is_null($addressRegion) || (is_array($addressRegion) && empty($addressRegion))) {
            unset($this->AddressRegion);
        } else {
            $this->AddressRegion = $addressRegion;
        }
        
        return $this;
    }
    /**
     * Get AddressCity value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressCity(): ?string
    {
        return isset($this->AddressCity) ? $this->AddressCity : null;
    }
    /**
     * Set AddressCity value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressCity
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setAddressCity(?string $addressCity = null): self
    {
        // validation for constraint: string
        if (!is_null($addressCity) && !is_string($addressCity)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressCity, true), gettype($addressCity)), __LINE__);
        }
        if (is_null($addressCity) || (is_array($addressCity) && empty($addressCity))) {
            unset($this->AddressCity);
        } else {
            $this->AddressCity = $addressCity;
        }
        
        return $this;
    }
    /**
     * Get Categories value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportCategory|null
     */
    public function getCategories(): ?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportCategory
    {
        return isset($this->Categories) ? $this->Categories : null;
    }
    /**
     * Set Categories value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportCategory $categories
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setCategories(?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportCategory $categories = null): self
    {
        if (is_null($categories) || (is_array($categories) && empty($categories))) {
            unset($this->Categories);
        } else {
            $this->Categories = $categories;
        }
        
        return $this;
    }
    /**
     * Get Endorsements value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportEndorsement|null
     */
    public function getEndorsements(): ?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportEndorsement
    {
        return isset($this->Endorsements) ? $this->Endorsements : null;
    }
    /**
     * Set Endorsements value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportEndorsement $endorsements
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setEndorsements(?\ID3Global\Arrays\ArrayOfGlobalDVLADrivingLicenceReportEndorsement $endorsements = null): self
    {
        if (is_null($endorsements) || (is_array($endorsements) && empty($endorsements))) {
            unset($this->Endorsements);
        } else {
            $this->Endorsements = $endorsements;
        }
        
        return $this;
    }
    /**
     * Get DisqualifiedUntil value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDisqualifiedUntil(): ?string
    {
        return isset($this->DisqualifiedUntil) ? $this->DisqualifiedUntil : null;
    }
    /**
     * Set DisqualifiedUntil value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $disqualifiedUntil
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setDisqualifiedUntil(?string $disqualifiedUntil = null): self
    {
        // validation for constraint: string
        if (!is_null($disqualifiedUntil) && !is_string($disqualifiedUntil)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($disqualifiedUntil, true), gettype($disqualifiedUntil)), __LINE__);
        }
        if (is_null($disqualifiedUntil) || (is_array($disqualifiedUntil) && empty($disqualifiedUntil))) {
            unset($this->DisqualifiedUntil);
        } else {
            $this->DisqualifiedUntil = $disqualifiedUntil;
        }
        
        return $this;
    }
    /**
     * Get NilReturn value
     * @return int|null
     */
    public function getNilReturn(): ?int
    {
        return $this->NilReturn;
    }
    /**
     * Set NilReturn value
     * @param int $nilReturn
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReport
     */
    public function setNilReturn(?int $nilReturn = null): self
    {
        // validation for constraint: int
        if (!is_null($nilReturn) && !(is_int($nilReturn) || ctype_digit($nilReturn))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($nilReturn, true), gettype($nilReturn)), __LINE__);
        }
        $this->NilReturn = $nilReturn;
        
        return $this;
    }
}
