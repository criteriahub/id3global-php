<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SupportedFieldsResponse Models
 * @subpackage Structs
 */
class SupportedFieldsResponse extends AbstractStructBase
{
    /**
     * The SupportedFieldsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupportedFields|null
     */
    protected ?\ID3Global\Models\GlobalSupportedFields $SupportedFieldsResult = null;
    /**
     * Constructor method for SupportedFieldsResponse
     * @uses SupportedFieldsResponse::setSupportedFieldsResult()
     * @param \ID3Global\Models\GlobalSupportedFields $supportedFieldsResult
     */
    public function __construct(?\ID3Global\Models\GlobalSupportedFields $supportedFieldsResult = null)
    {
        $this
            ->setSupportedFieldsResult($supportedFieldsResult);
    }
    /**
     * Get SupportedFieldsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupportedFields|null
     */
    public function getSupportedFieldsResult(): ?\ID3Global\Models\GlobalSupportedFields
    {
        return isset($this->SupportedFieldsResult) ? $this->SupportedFieldsResult : null;
    }
    /**
     * Set SupportedFieldsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupportedFields $supportedFieldsResult
     * @return \ID3Global\Models\SupportedFieldsResponse
     */
    public function setSupportedFieldsResult(?\ID3Global\Models\GlobalSupportedFields $supportedFieldsResult = null): self
    {
        if (is_null($supportedFieldsResult) || (is_array($supportedFieldsResult) && empty($supportedFieldsResult))) {
            unset($this->SupportedFieldsResult);
        } else {
            $this->SupportedFieldsResult = $supportedFieldsResult;
        }
        
        return $this;
    }
}
