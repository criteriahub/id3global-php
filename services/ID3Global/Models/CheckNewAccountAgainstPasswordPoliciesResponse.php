<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CheckNewAccountAgainstPasswordPoliciesResponse Models
 * @subpackage Structs
 */
class CheckNewAccountAgainstPasswordPoliciesResponse extends AbstractStructBase
{
    /**
     * The CheckNewAccountAgainstPasswordPoliciesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $CheckNewAccountAgainstPasswordPoliciesResult = null;
    /**
     * Constructor method for CheckNewAccountAgainstPasswordPoliciesResponse
     * @uses CheckNewAccountAgainstPasswordPoliciesResponse::setCheckNewAccountAgainstPasswordPoliciesResult()
     * @param bool $checkNewAccountAgainstPasswordPoliciesResult
     */
    public function __construct(?bool $checkNewAccountAgainstPasswordPoliciesResult = null)
    {
        $this
            ->setCheckNewAccountAgainstPasswordPoliciesResult($checkNewAccountAgainstPasswordPoliciesResult);
    }
    /**
     * Get CheckNewAccountAgainstPasswordPoliciesResult value
     * @return bool|null
     */
    public function getCheckNewAccountAgainstPasswordPoliciesResult(): ?bool
    {
        return $this->CheckNewAccountAgainstPasswordPoliciesResult;
    }
    /**
     * Set CheckNewAccountAgainstPasswordPoliciesResult value
     * @param bool $checkNewAccountAgainstPasswordPoliciesResult
     * @return \ID3Global\Models\CheckNewAccountAgainstPasswordPoliciesResponse
     */
    public function setCheckNewAccountAgainstPasswordPoliciesResult(?bool $checkNewAccountAgainstPasswordPoliciesResult = null): self
    {
        // validation for constraint: boolean
        if (!is_null($checkNewAccountAgainstPasswordPoliciesResult) && !is_bool($checkNewAccountAgainstPasswordPoliciesResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($checkNewAccountAgainstPasswordPoliciesResult, true), gettype($checkNewAccountAgainstPasswordPoliciesResult)), __LINE__);
        }
        $this->CheckNewAccountAgainstPasswordPoliciesResult = $checkNewAccountAgainstPasswordPoliciesResult;
        
        return $this;
    }
}
