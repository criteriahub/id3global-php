<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCaseInputDataResponse Models
 * @subpackage Structs
 */
class GetCaseInputDataResponse extends AbstractStructBase
{
    /**
     * The GetCaseInputDataResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalInputData|null
     */
    protected ?\ID3Global\Models\GlobalInputData $GetCaseInputDataResult = null;
    /**
     * Constructor method for GetCaseInputDataResponse
     * @uses GetCaseInputDataResponse::setGetCaseInputDataResult()
     * @param \ID3Global\Models\GlobalInputData $getCaseInputDataResult
     */
    public function __construct(?\ID3Global\Models\GlobalInputData $getCaseInputDataResult = null)
    {
        $this
            ->setGetCaseInputDataResult($getCaseInputDataResult);
    }
    /**
     * Get GetCaseInputDataResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalInputData|null
     */
    public function getGetCaseInputDataResult(): ?\ID3Global\Models\GlobalInputData
    {
        return isset($this->GetCaseInputDataResult) ? $this->GetCaseInputDataResult : null;
    }
    /**
     * Set GetCaseInputDataResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalInputData $getCaseInputDataResult
     * @return \ID3Global\Models\GetCaseInputDataResponse
     */
    public function setGetCaseInputDataResult(?\ID3Global\Models\GlobalInputData $getCaseInputDataResult = null): self
    {
        if (is_null($getCaseInputDataResult) || (is_array($getCaseInputDataResult) && empty($getCaseInputDataResult))) {
            unset($this->GetCaseInputDataResult);
        } else {
            $this->GetCaseInputDataResult = $getCaseInputDataResult;
        }
        
        return $this;
    }
}
