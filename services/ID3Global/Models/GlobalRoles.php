<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalRoles Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q590:GlobalRoles
 * @subpackage Structs
 */
class GlobalRoles extends AbstractStructBase
{
    /**
     * The Roles
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalRole|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalRole $Roles = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The TotalPages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPages = null;
    /**
     * The TotalRoles
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalRoles = null;
    /**
     * The TotalActive
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalActive = null;
    /**
     * The TotalExpired
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalExpired = null;
    /**
     * Constructor method for GlobalRoles
     * @uses GlobalRoles::setRoles()
     * @uses GlobalRoles::setPageSize()
     * @uses GlobalRoles::setTotalPages()
     * @uses GlobalRoles::setTotalRoles()
     * @uses GlobalRoles::setTotalActive()
     * @uses GlobalRoles::setTotalExpired()
     * @param \ID3Global\Arrays\ArrayOfGlobalRole $roles
     * @param int $pageSize
     * @param int $totalPages
     * @param int $totalRoles
     * @param int $totalActive
     * @param int $totalExpired
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalRole $roles = null, ?int $pageSize = null, ?int $totalPages = null, ?int $totalRoles = null, ?int $totalActive = null, ?int $totalExpired = null)
    {
        $this
            ->setRoles($roles)
            ->setPageSize($pageSize)
            ->setTotalPages($totalPages)
            ->setTotalRoles($totalRoles)
            ->setTotalActive($totalActive)
            ->setTotalExpired($totalExpired);
    }
    /**
     * Get Roles value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalRole|null
     */
    public function getRoles(): ?\ID3Global\Arrays\ArrayOfGlobalRole
    {
        return isset($this->Roles) ? $this->Roles : null;
    }
    /**
     * Set Roles value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalRole $roles
     * @return \ID3Global\Models\GlobalRoles
     */
    public function setRoles(?\ID3Global\Arrays\ArrayOfGlobalRole $roles = null): self
    {
        if (is_null($roles) || (is_array($roles) && empty($roles))) {
            unset($this->Roles);
        } else {
            $this->Roles = $roles;
        }
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GlobalRoles
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get TotalPages value
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->TotalPages;
    }
    /**
     * Set TotalPages value
     * @param int $totalPages
     * @return \ID3Global\Models\GlobalRoles
     */
    public function setTotalPages(?int $totalPages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPages) && !(is_int($totalPages) || ctype_digit($totalPages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPages, true), gettype($totalPages)), __LINE__);
        }
        $this->TotalPages = $totalPages;
        
        return $this;
    }
    /**
     * Get TotalRoles value
     * @return int|null
     */
    public function getTotalRoles(): ?int
    {
        return $this->TotalRoles;
    }
    /**
     * Set TotalRoles value
     * @param int $totalRoles
     * @return \ID3Global\Models\GlobalRoles
     */
    public function setTotalRoles(?int $totalRoles = null): self
    {
        // validation for constraint: int
        if (!is_null($totalRoles) && !(is_int($totalRoles) || ctype_digit($totalRoles))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalRoles, true), gettype($totalRoles)), __LINE__);
        }
        $this->TotalRoles = $totalRoles;
        
        return $this;
    }
    /**
     * Get TotalActive value
     * @return int|null
     */
    public function getTotalActive(): ?int
    {
        return $this->TotalActive;
    }
    /**
     * Set TotalActive value
     * @param int $totalActive
     * @return \ID3Global\Models\GlobalRoles
     */
    public function setTotalActive(?int $totalActive = null): self
    {
        // validation for constraint: int
        if (!is_null($totalActive) && !(is_int($totalActive) || ctype_digit($totalActive))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalActive, true), gettype($totalActive)), __LINE__);
        }
        $this->TotalActive = $totalActive;
        
        return $this;
    }
    /**
     * Get TotalExpired value
     * @return int|null
     */
    public function getTotalExpired(): ?int
    {
        return $this->TotalExpired;
    }
    /**
     * Set TotalExpired value
     * @param int $totalExpired
     * @return \ID3Global\Models\GlobalRoles
     */
    public function setTotalExpired(?int $totalExpired = null): self
    {
        // validation for constraint: int
        if (!is_null($totalExpired) && !(is_int($totalExpired) || ctype_digit($totalExpired))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalExpired, true), gettype($totalExpired)), __LINE__);
        }
        $this->TotalExpired = $totalExpired;
        
        return $this;
    }
}
