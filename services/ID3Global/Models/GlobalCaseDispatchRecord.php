<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseDispatchRecord Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q858:GlobalCaseDispatchRecord
 * @subpackage Structs
 */
class GlobalCaseDispatchRecord extends AbstractStructBase
{
    /**
     * The DispatchService
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $DispatchService = null;
    /**
     * The Updated
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Updated = null;
    /**
     * The Status
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Status = null;
    /**
     * The ChargingPointProfileID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ChargingPointProfileID = null;
    /**
     * The Message
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Message = null;
    /**
     * The ProfileID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ProfileID = null;
    /**
     * The ResendStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ResendStatus = null;
    /**
     * The ConsentStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ConsentStatus = null;
    /**
     * The RejectStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $RejectStatus = null;
    /**
     * The AccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AccountID = null;
    /**
     * Constructor method for GlobalCaseDispatchRecord
     * @uses GlobalCaseDispatchRecord::setDispatchService()
     * @uses GlobalCaseDispatchRecord::setUpdated()
     * @uses GlobalCaseDispatchRecord::setStatus()
     * @uses GlobalCaseDispatchRecord::setChargingPointProfileID()
     * @uses GlobalCaseDispatchRecord::setMessage()
     * @uses GlobalCaseDispatchRecord::setProfileID()
     * @uses GlobalCaseDispatchRecord::setResendStatus()
     * @uses GlobalCaseDispatchRecord::setConsentStatus()
     * @uses GlobalCaseDispatchRecord::setRejectStatus()
     * @uses GlobalCaseDispatchRecord::setAccountID()
     * @param string $dispatchService
     * @param string $updated
     * @param string $status
     * @param string $chargingPointProfileID
     * @param string $message
     * @param string $profileID
     * @param int $resendStatus
     * @param int $consentStatus
     * @param int $rejectStatus
     * @param string $accountID
     */
    public function __construct(?string $dispatchService = null, ?string $updated = null, ?string $status = null, ?string $chargingPointProfileID = null, ?string $message = null, ?string $profileID = null, ?int $resendStatus = null, ?int $consentStatus = null, ?int $rejectStatus = null, ?string $accountID = null)
    {
        $this
            ->setDispatchService($dispatchService)
            ->setUpdated($updated)
            ->setStatus($status)
            ->setChargingPointProfileID($chargingPointProfileID)
            ->setMessage($message)
            ->setProfileID($profileID)
            ->setResendStatus($resendStatus)
            ->setConsentStatus($consentStatus)
            ->setRejectStatus($rejectStatus)
            ->setAccountID($accountID);
    }
    /**
     * Get DispatchService value
     * @return string|null
     */
    public function getDispatchService(): ?string
    {
        return $this->DispatchService;
    }
    /**
     * Set DispatchService value
     * @uses \ID3Global\Enums\GlobalCaseDispatchServiceType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalCaseDispatchServiceType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $dispatchService
     * @return \ID3Global\Models\GlobalCaseDispatchRecord
     */
    public function setDispatchService(?string $dispatchService = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalCaseDispatchServiceType::valueIsValid($dispatchService)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalCaseDispatchServiceType', is_array($dispatchService) ? implode(', ', $dispatchService) : var_export($dispatchService, true), implode(', ', \ID3Global\Enums\GlobalCaseDispatchServiceType::getValidValues())), __LINE__);
        }
        $this->DispatchService = $dispatchService;
        
        return $this;
    }
    /**
     * Get Updated value
     * @return string|null
     */
    public function getUpdated(): ?string
    {
        return $this->Updated;
    }
    /**
     * Set Updated value
     * @param string $updated
     * @return \ID3Global\Models\GlobalCaseDispatchRecord
     */
    public function setUpdated(?string $updated = null): self
    {
        // validation for constraint: string
        if (!is_null($updated) && !is_string($updated)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updated, true), gettype($updated)), __LINE__);
        }
        $this->Updated = $updated;
        
        return $this;
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @uses \ID3Global\Enums\GlobalCaseDispatchRecordStatus::valueIsValid()
     * @uses \ID3Global\Enums\GlobalCaseDispatchRecordStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $status
     * @return \ID3Global\Models\GlobalCaseDispatchRecord
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalCaseDispatchRecordStatus::valueIsValid($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalCaseDispatchRecordStatus', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \ID3Global\Enums\GlobalCaseDispatchRecordStatus::getValidValues())), __LINE__);
        }
        $this->Status = $status;
        
        return $this;
    }
    /**
     * Get ChargingPointProfileID value
     * @return string|null
     */
    public function getChargingPointProfileID(): ?string
    {
        return $this->ChargingPointProfileID;
    }
    /**
     * Set ChargingPointProfileID value
     * @param string $chargingPointProfileID
     * @return \ID3Global\Models\GlobalCaseDispatchRecord
     */
    public function setChargingPointProfileID(?string $chargingPointProfileID = null): self
    {
        // validation for constraint: string
        if (!is_null($chargingPointProfileID) && !is_string($chargingPointProfileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($chargingPointProfileID, true), gettype($chargingPointProfileID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($chargingPointProfileID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $chargingPointProfileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($chargingPointProfileID, true)), __LINE__);
        }
        $this->ChargingPointProfileID = $chargingPointProfileID;
        
        return $this;
    }
    /**
     * Get Message value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return isset($this->Message) ? $this->Message : null;
    }
    /**
     * Set Message value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $message
     * @return \ID3Global\Models\GlobalCaseDispatchRecord
     */
    public function setMessage(?string $message = null): self
    {
        // validation for constraint: string
        if (!is_null($message) && !is_string($message)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($message, true), gettype($message)), __LINE__);
        }
        if (is_null($message) || (is_array($message) && empty($message))) {
            unset($this->Message);
        } else {
            $this->Message = $message;
        }
        
        return $this;
    }
    /**
     * Get ProfileID value
     * @return string|null
     */
    public function getProfileID(): ?string
    {
        return $this->ProfileID;
    }
    /**
     * Set ProfileID value
     * @param string $profileID
     * @return \ID3Global\Models\GlobalCaseDispatchRecord
     */
    public function setProfileID(?string $profileID = null): self
    {
        // validation for constraint: string
        if (!is_null($profileID) && !is_string($profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($profileID, true), gettype($profileID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($profileID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $profileID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($profileID, true)), __LINE__);
        }
        $this->ProfileID = $profileID;
        
        return $this;
    }
    /**
     * Get ResendStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getResendStatus(): ?int
    {
        return isset($this->ResendStatus) ? $this->ResendStatus : null;
    }
    /**
     * Set ResendStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $resendStatus
     * @return \ID3Global\Models\GlobalCaseDispatchRecord
     */
    public function setResendStatus(?int $resendStatus = null): self
    {
        // validation for constraint: int
        if (!is_null($resendStatus) && !(is_int($resendStatus) || ctype_digit($resendStatus))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($resendStatus, true), gettype($resendStatus)), __LINE__);
        }
        if (is_null($resendStatus) || (is_array($resendStatus) && empty($resendStatus))) {
            unset($this->ResendStatus);
        } else {
            $this->ResendStatus = $resendStatus;
        }
        
        return $this;
    }
    /**
     * Get ConsentStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getConsentStatus(): ?int
    {
        return isset($this->ConsentStatus) ? $this->ConsentStatus : null;
    }
    /**
     * Set ConsentStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $consentStatus
     * @return \ID3Global\Models\GlobalCaseDispatchRecord
     */
    public function setConsentStatus(?int $consentStatus = null): self
    {
        // validation for constraint: int
        if (!is_null($consentStatus) && !(is_int($consentStatus) || ctype_digit($consentStatus))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($consentStatus, true), gettype($consentStatus)), __LINE__);
        }
        if (is_null($consentStatus) || (is_array($consentStatus) && empty($consentStatus))) {
            unset($this->ConsentStatus);
        } else {
            $this->ConsentStatus = $consentStatus;
        }
        
        return $this;
    }
    /**
     * Get RejectStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getRejectStatus(): ?int
    {
        return isset($this->RejectStatus) ? $this->RejectStatus : null;
    }
    /**
     * Set RejectStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $rejectStatus
     * @return \ID3Global\Models\GlobalCaseDispatchRecord
     */
    public function setRejectStatus(?int $rejectStatus = null): self
    {
        // validation for constraint: int
        if (!is_null($rejectStatus) && !(is_int($rejectStatus) || ctype_digit($rejectStatus))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($rejectStatus, true), gettype($rejectStatus)), __LINE__);
        }
        if (is_null($rejectStatus) || (is_array($rejectStatus) && empty($rejectStatus))) {
            unset($this->RejectStatus);
        } else {
            $this->RejectStatus = $rejectStatus;
        }
        
        return $this;
    }
    /**
     * Get AccountID value
     * @return string|null
     */
    public function getAccountID(): ?string
    {
        return $this->AccountID;
    }
    /**
     * Set AccountID value
     * @param string $accountID
     * @return \ID3Global\Models\GlobalCaseDispatchRecord
     */
    public function setAccountID(?string $accountID = null): self
    {
        // validation for constraint: string
        if (!is_null($accountID) && !is_string($accountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountID, true), gettype($accountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($accountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $accountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($accountID, true)), __LINE__);
        }
        $this->AccountID = $accountID;
        
        return $this;
    }
}
