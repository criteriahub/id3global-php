<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdatePermissions Models
 * @subpackage Structs
 */
class UpdatePermissions extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The ResourceID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ResourceID = null;
    /**
     * The AddRolePermissions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalRolePermission|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalRolePermission $AddRolePermissions = null;
    /**
     * The RemoveRolePermissions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalRolePermission|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalRolePermission $RemoveRolePermissions = null;
    /**
     * Constructor method for UpdatePermissions
     * @uses UpdatePermissions::setOrgID()
     * @uses UpdatePermissions::setResourceID()
     * @uses UpdatePermissions::setAddRolePermissions()
     * @uses UpdatePermissions::setRemoveRolePermissions()
     * @param string $orgID
     * @param string $resourceID
     * @param \ID3Global\Arrays\ArrayOfGlobalRolePermission $addRolePermissions
     * @param \ID3Global\Arrays\ArrayOfGlobalRolePermission $removeRolePermissions
     */
    public function __construct(?string $orgID = null, ?string $resourceID = null, ?\ID3Global\Arrays\ArrayOfGlobalRolePermission $addRolePermissions = null, ?\ID3Global\Arrays\ArrayOfGlobalRolePermission $removeRolePermissions = null)
    {
        $this
            ->setOrgID($orgID)
            ->setResourceID($resourceID)
            ->setAddRolePermissions($addRolePermissions)
            ->setRemoveRolePermissions($removeRolePermissions);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\UpdatePermissions
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get ResourceID value
     * @return string|null
     */
    public function getResourceID(): ?string
    {
        return $this->ResourceID;
    }
    /**
     * Set ResourceID value
     * @param string $resourceID
     * @return \ID3Global\Models\UpdatePermissions
     */
    public function setResourceID(?string $resourceID = null): self
    {
        // validation for constraint: string
        if (!is_null($resourceID) && !is_string($resourceID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($resourceID, true), gettype($resourceID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($resourceID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $resourceID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($resourceID, true)), __LINE__);
        }
        $this->ResourceID = $resourceID;
        
        return $this;
    }
    /**
     * Get AddRolePermissions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalRolePermission|null
     */
    public function getAddRolePermissions(): ?\ID3Global\Arrays\ArrayOfGlobalRolePermission
    {
        return isset($this->AddRolePermissions) ? $this->AddRolePermissions : null;
    }
    /**
     * Set AddRolePermissions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalRolePermission $addRolePermissions
     * @return \ID3Global\Models\UpdatePermissions
     */
    public function setAddRolePermissions(?\ID3Global\Arrays\ArrayOfGlobalRolePermission $addRolePermissions = null): self
    {
        if (is_null($addRolePermissions) || (is_array($addRolePermissions) && empty($addRolePermissions))) {
            unset($this->AddRolePermissions);
        } else {
            $this->AddRolePermissions = $addRolePermissions;
        }
        
        return $this;
    }
    /**
     * Get RemoveRolePermissions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalRolePermission|null
     */
    public function getRemoveRolePermissions(): ?\ID3Global\Arrays\ArrayOfGlobalRolePermission
    {
        return isset($this->RemoveRolePermissions) ? $this->RemoveRolePermissions : null;
    }
    /**
     * Set RemoveRolePermissions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalRolePermission $removeRolePermissions
     * @return \ID3Global\Models\UpdatePermissions
     */
    public function setRemoveRolePermissions(?\ID3Global\Arrays\ArrayOfGlobalRolePermission $removeRolePermissions = null): self
    {
        if (is_null($removeRolePermissions) || (is_array($removeRolePermissions) && empty($removeRolePermissions))) {
            unset($this->RemoveRolePermissions);
        } else {
            $this->RemoveRolePermissions = $removeRolePermissions;
        }
        
        return $this;
    }
}
