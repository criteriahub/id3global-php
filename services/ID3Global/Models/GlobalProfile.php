<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalProfile Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q167:GlobalProfile
 * @subpackage Structs
 */
class GlobalProfile extends AbstractStructBase
{
    /**
     * The ID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Description = null;
    /**
     * The Created
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Created = null;
    /**
     * The Updated
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Updated = null;
    /**
     * The UpdatedBy
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $UpdatedBy = null;
    /**
     * The UpdatedByAccount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $UpdatedByAccount = null;
    /**
     * Constructor method for GlobalProfile
     * @uses GlobalProfile::setID()
     * @uses GlobalProfile::setName()
     * @uses GlobalProfile::setDescription()
     * @uses GlobalProfile::setCreated()
     * @uses GlobalProfile::setUpdated()
     * @uses GlobalProfile::setUpdatedBy()
     * @uses GlobalProfile::setUpdatedByAccount()
     * @param string $iD
     * @param string $name
     * @param string $description
     * @param string $created
     * @param string $updated
     * @param string $updatedBy
     * @param string $updatedByAccount
     */
    public function __construct(?string $iD = null, ?string $name = null, ?string $description = null, ?string $created = null, ?string $updated = null, ?string $updatedBy = null, ?string $updatedByAccount = null)
    {
        $this
            ->setID($iD)
            ->setName($name)
            ->setDescription($description)
            ->setCreated($created)
            ->setUpdated($updated)
            ->setUpdatedBy($updatedBy)
            ->setUpdatedByAccount($updatedByAccount);
    }
    /**
     * Get ID value
     * @return string|null
     */
    public function getID(): ?string
    {
        return $this->ID;
    }
    /**
     * Set ID value
     * @param string $iD
     * @return \ID3Global\Models\GlobalProfile
     */
    public function setID(?string $iD = null): self
    {
        // validation for constraint: string
        if (!is_null($iD) && !is_string($iD)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($iD, true), gettype($iD)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($iD) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $iD)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($iD, true)), __LINE__);
        }
        $this->ID = $iD;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalProfile
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \ID3Global\Models\GlobalProfile
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        
        return $this;
    }
    /**
     * Get Created value
     * @return string|null
     */
    public function getCreated(): ?string
    {
        return $this->Created;
    }
    /**
     * Set Created value
     * @param string $created
     * @return \ID3Global\Models\GlobalProfile
     */
    public function setCreated(?string $created = null): self
    {
        // validation for constraint: string
        if (!is_null($created) && !is_string($created)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($created, true), gettype($created)), __LINE__);
        }
        $this->Created = $created;
        
        return $this;
    }
    /**
     * Get Updated value
     * @return string|null
     */
    public function getUpdated(): ?string
    {
        return $this->Updated;
    }
    /**
     * Set Updated value
     * @param string $updated
     * @return \ID3Global\Models\GlobalProfile
     */
    public function setUpdated(?string $updated = null): self
    {
        // validation for constraint: string
        if (!is_null($updated) && !is_string($updated)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updated, true), gettype($updated)), __LINE__);
        }
        $this->Updated = $updated;
        
        return $this;
    }
    /**
     * Get UpdatedBy value
     * @return string|null
     */
    public function getUpdatedBy(): ?string
    {
        return $this->UpdatedBy;
    }
    /**
     * Set UpdatedBy value
     * @param string $updatedBy
     * @return \ID3Global\Models\GlobalProfile
     */
    public function setUpdatedBy(?string $updatedBy = null): self
    {
        // validation for constraint: string
        if (!is_null($updatedBy) && !is_string($updatedBy)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updatedBy, true), gettype($updatedBy)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($updatedBy) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $updatedBy)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($updatedBy, true)), __LINE__);
        }
        $this->UpdatedBy = $updatedBy;
        
        return $this;
    }
    /**
     * Get UpdatedByAccount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUpdatedByAccount(): ?string
    {
        return isset($this->UpdatedByAccount) ? $this->UpdatedByAccount : null;
    }
    /**
     * Set UpdatedByAccount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $updatedByAccount
     * @return \ID3Global\Models\GlobalProfile
     */
    public function setUpdatedByAccount(?string $updatedByAccount = null): self
    {
        // validation for constraint: string
        if (!is_null($updatedByAccount) && !is_string($updatedByAccount)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updatedByAccount, true), gettype($updatedByAccount)), __LINE__);
        }
        if (is_null($updatedByAccount) || (is_array($updatedByAccount) && empty($updatedByAccount))) {
            unset($this->UpdatedByAccount);
        } else {
            $this->UpdatedByAccount = $updatedByAccount;
        }
        
        return $this;
    }
}
