<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCIFASResultCode Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q70:GlobalCIFASResultCode
 * @subpackage Structs
 */
class GlobalCIFASResultCode extends AbstractStructBase
{
    /**
     * The Code
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Code = null;
    /**
     * Constructor method for GlobalCIFASResultCode
     * @uses GlobalCIFASResultCode::setCode()
     * @param int $code
     */
    public function __construct(?int $code = null)
    {
        $this
            ->setCode($code);
    }
    /**
     * Get Code value
     * @return int|null
     */
    public function getCode(): ?int
    {
        return $this->Code;
    }
    /**
     * Set Code value
     * @param int $code
     * @return \ID3Global\Models\GlobalCIFASResultCode
     */
    public function setCode(?int $code = null): self
    {
        // validation for constraint: int
        if (!is_null($code) && !(is_int($code) || ctype_digit($code))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        $this->Code = $code;
        
        return $this;
    }
}
