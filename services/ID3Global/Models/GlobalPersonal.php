<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalPersonal Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q314:GlobalPersonal
 * @subpackage Structs
 */
class GlobalPersonal extends AbstractStructBase
{
    /**
     * The PersonalDetails
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalPersonalDetails|null
     */
    protected ?\ID3Global\Models\GlobalPersonalDetails $PersonalDetails = null;
    /**
     * The AlternateName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAlternateName|null
     */
    protected ?\ID3Global\Models\GlobalAlternateName $AlternateName = null;
    /**
     * The Aliases
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalAlternateName|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalAlternateName $Aliases = null;
    /**
     * Constructor method for GlobalPersonal
     * @uses GlobalPersonal::setPersonalDetails()
     * @uses GlobalPersonal::setAlternateName()
     * @uses GlobalPersonal::setAliases()
     * @param \ID3Global\Models\GlobalPersonalDetails $personalDetails
     * @param \ID3Global\Models\GlobalAlternateName $alternateName
     * @param \ID3Global\Arrays\ArrayOfGlobalAlternateName $aliases
     */
    public function __construct(?\ID3Global\Models\GlobalPersonalDetails $personalDetails = null, ?\ID3Global\Models\GlobalAlternateName $alternateName = null, ?\ID3Global\Arrays\ArrayOfGlobalAlternateName $aliases = null)
    {
        $this
            ->setPersonalDetails($personalDetails)
            ->setAlternateName($alternateName)
            ->setAliases($aliases);
    }
    /**
     * Get PersonalDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalPersonalDetails|null
     */
    public function getPersonalDetails(): ?\ID3Global\Models\GlobalPersonalDetails
    {
        return isset($this->PersonalDetails) ? $this->PersonalDetails : null;
    }
    /**
     * Set PersonalDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalPersonalDetails $personalDetails
     * @return \ID3Global\Models\GlobalPersonal
     */
    public function setPersonalDetails(?\ID3Global\Models\GlobalPersonalDetails $personalDetails = null): self
    {
        if (is_null($personalDetails) || (is_array($personalDetails) && empty($personalDetails))) {
            unset($this->PersonalDetails);
        } else {
            $this->PersonalDetails = $personalDetails;
        }
        
        return $this;
    }
    /**
     * Get AlternateName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAlternateName|null
     */
    public function getAlternateName(): ?\ID3Global\Models\GlobalAlternateName
    {
        return isset($this->AlternateName) ? $this->AlternateName : null;
    }
    /**
     * Set AlternateName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAlternateName $alternateName
     * @return \ID3Global\Models\GlobalPersonal
     */
    public function setAlternateName(?\ID3Global\Models\GlobalAlternateName $alternateName = null): self
    {
        if (is_null($alternateName) || (is_array($alternateName) && empty($alternateName))) {
            unset($this->AlternateName);
        } else {
            $this->AlternateName = $alternateName;
        }
        
        return $this;
    }
    /**
     * Get Aliases value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalAlternateName|null
     */
    public function getAliases(): ?\ID3Global\Arrays\ArrayOfGlobalAlternateName
    {
        return isset($this->Aliases) ? $this->Aliases : null;
    }
    /**
     * Set Aliases value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalAlternateName $aliases
     * @return \ID3Global\Models\GlobalPersonal
     */
    public function setAliases(?\ID3Global\Arrays\ArrayOfGlobalAlternateName $aliases = null): self
    {
        if (is_null($aliases) || (is_array($aliases) && empty($aliases))) {
            unset($this->Aliases);
        } else {
            $this->Aliases = $aliases;
        }
        
        return $this;
    }
}
