<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDataExtractDownloadHistory Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q514:GlobalDataExtractDownloadHistory
 * @subpackage Structs
 */
class GlobalDataExtractDownloadHistory extends AbstractStructBase
{
    /**
     * The Timestamp
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Timestamp = null;
    /**
     * The AccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AccountID = null;
    /**
     * The AccountName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccountName = null;
    /**
     * Constructor method for GlobalDataExtractDownloadHistory
     * @uses GlobalDataExtractDownloadHistory::setTimestamp()
     * @uses GlobalDataExtractDownloadHistory::setAccountID()
     * @uses GlobalDataExtractDownloadHistory::setAccountName()
     * @param string $timestamp
     * @param string $accountID
     * @param string $accountName
     */
    public function __construct(?string $timestamp = null, ?string $accountID = null, ?string $accountName = null)
    {
        $this
            ->setTimestamp($timestamp)
            ->setAccountID($accountID)
            ->setAccountName($accountName);
    }
    /**
     * Get Timestamp value
     * @return string|null
     */
    public function getTimestamp(): ?string
    {
        return $this->Timestamp;
    }
    /**
     * Set Timestamp value
     * @param string $timestamp
     * @return \ID3Global\Models\GlobalDataExtractDownloadHistory
     */
    public function setTimestamp(?string $timestamp = null): self
    {
        // validation for constraint: string
        if (!is_null($timestamp) && !is_string($timestamp)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($timestamp, true), gettype($timestamp)), __LINE__);
        }
        $this->Timestamp = $timestamp;
        
        return $this;
    }
    /**
     * Get AccountID value
     * @return string|null
     */
    public function getAccountID(): ?string
    {
        return $this->AccountID;
    }
    /**
     * Set AccountID value
     * @param string $accountID
     * @return \ID3Global\Models\GlobalDataExtractDownloadHistory
     */
    public function setAccountID(?string $accountID = null): self
    {
        // validation for constraint: string
        if (!is_null($accountID) && !is_string($accountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountID, true), gettype($accountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($accountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $accountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($accountID, true)), __LINE__);
        }
        $this->AccountID = $accountID;
        
        return $this;
    }
    /**
     * Get AccountName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccountName(): ?string
    {
        return isset($this->AccountName) ? $this->AccountName : null;
    }
    /**
     * Set AccountName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accountName
     * @return \ID3Global\Models\GlobalDataExtractDownloadHistory
     */
    public function setAccountName(?string $accountName = null): self
    {
        // validation for constraint: string
        if (!is_null($accountName) && !is_string($accountName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountName, true), gettype($accountName)), __LINE__);
        }
        if (is_null($accountName) || (is_array($accountName) && empty($accountName))) {
            unset($this->AccountName);
        } else {
            $this->AccountName = $accountName;
        }
        
        return $this;
    }
}
