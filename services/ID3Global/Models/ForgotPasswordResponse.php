<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ForgotPasswordResponse Models
 * @subpackage Structs
 */
class ForgotPasswordResponse extends AbstractStructBase
{
}
