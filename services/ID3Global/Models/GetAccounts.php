<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAccounts Models
 * @subpackage Structs
 */
class GetAccounts extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The Page
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Page = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The Search
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Search = null;
    /**
     * The Filter
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Filter = null;
    /**
     * Constructor method for GetAccounts
     * @uses GetAccounts::setOrgID()
     * @uses GetAccounts::setPage()
     * @uses GetAccounts::setPageSize()
     * @uses GetAccounts::setSearch()
     * @uses GetAccounts::setFilter()
     * @param string $orgID
     * @param int $page
     * @param int $pageSize
     * @param string $search
     * @param int $filter
     */
    public function __construct(?string $orgID = null, ?int $page = null, ?int $pageSize = null, ?string $search = null, ?int $filter = null)
    {
        $this
            ->setOrgID($orgID)
            ->setPage($page)
            ->setPageSize($pageSize)
            ->setSearch($search)
            ->setFilter($filter);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetAccounts
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get Page value
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->Page;
    }
    /**
     * Set Page value
     * @param int $page
     * @return \ID3Global\Models\GetAccounts
     */
    public function setPage(?int $page = null): self
    {
        // validation for constraint: int
        if (!is_null($page) && !(is_int($page) || ctype_digit($page))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($page, true), gettype($page)), __LINE__);
        }
        $this->Page = $page;
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GetAccounts
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get Search value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSearch(): ?string
    {
        return isset($this->Search) ? $this->Search : null;
    }
    /**
     * Set Search value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $search
     * @return \ID3Global\Models\GetAccounts
     */
    public function setSearch(?string $search = null): self
    {
        // validation for constraint: string
        if (!is_null($search) && !is_string($search)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($search, true), gettype($search)), __LINE__);
        }
        if (is_null($search) || (is_array($search) && empty($search))) {
            unset($this->Search);
        } else {
            $this->Search = $search;
        }
        
        return $this;
    }
    /**
     * Get Filter value
     * @return int|null
     */
    public function getFilter(): ?int
    {
        return $this->Filter;
    }
    /**
     * Set Filter value
     * @param int $filter
     * @return \ID3Global\Models\GetAccounts
     */
    public function setFilter(?int $filter = null): self
    {
        // validation for constraint: int
        if (!is_null($filter) && !(is_int($filter) || ctype_digit($filter))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($filter, true), gettype($filter)), __LINE__);
        }
        $this->Filter = $filter;
        
        return $this;
    }
}
