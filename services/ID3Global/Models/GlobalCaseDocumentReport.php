<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseDocumentReport Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q843:GlobalCaseDocumentReport
 * @subpackage Structs
 */
class GlobalCaseDocumentReport extends AbstractStructBase
{
    /**
     * The Profile
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileIDVersion|null
     */
    protected ?\ID3Global\Models\GlobalProfileIDVersion $Profile = null;
    /**
     * The ValidationID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ValidationID = null;
    /**
     * The AuthenticationID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AuthenticationID = null;
    /**
     * The Result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalMatrixResult|null
     */
    protected ?\ID3Global\Models\GlobalMatrixResult $Result = null;
    /**
     * The DocumentType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $DocumentType = null;
    /**
     * The LastChecked
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $LastChecked = null;
    /**
     * The Status
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Status = null;
    /**
     * Constructor method for GlobalCaseDocumentReport
     * @uses GlobalCaseDocumentReport::setProfile()
     * @uses GlobalCaseDocumentReport::setValidationID()
     * @uses GlobalCaseDocumentReport::setAuthenticationID()
     * @uses GlobalCaseDocumentReport::setResult()
     * @uses GlobalCaseDocumentReport::setDocumentType()
     * @uses GlobalCaseDocumentReport::setLastChecked()
     * @uses GlobalCaseDocumentReport::setStatus()
     * @param \ID3Global\Models\GlobalProfileIDVersion $profile
     * @param string $validationID
     * @param string $authenticationID
     * @param \ID3Global\Models\GlobalMatrixResult $result
     * @param string $documentType
     * @param string $lastChecked
     * @param string $status
     */
    public function __construct(?\ID3Global\Models\GlobalProfileIDVersion $profile = null, ?string $validationID = null, ?string $authenticationID = null, ?\ID3Global\Models\GlobalMatrixResult $result = null, ?string $documentType = null, ?string $lastChecked = null, ?string $status = null)
    {
        $this
            ->setProfile($profile)
            ->setValidationID($validationID)
            ->setAuthenticationID($authenticationID)
            ->setResult($result)
            ->setDocumentType($documentType)
            ->setLastChecked($lastChecked)
            ->setStatus($status);
    }
    /**
     * Get Profile value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileIDVersion|null
     */
    public function getProfile(): ?\ID3Global\Models\GlobalProfileIDVersion
    {
        return isset($this->Profile) ? $this->Profile : null;
    }
    /**
     * Set Profile value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileIDVersion $profile
     * @return \ID3Global\Models\GlobalCaseDocumentReport
     */
    public function setProfile(?\ID3Global\Models\GlobalProfileIDVersion $profile = null): self
    {
        if (is_null($profile) || (is_array($profile) && empty($profile))) {
            unset($this->Profile);
        } else {
            $this->Profile = $profile;
        }
        
        return $this;
    }
    /**
     * Get ValidationID value
     * @return string|null
     */
    public function getValidationID(): ?string
    {
        return $this->ValidationID;
    }
    /**
     * Set ValidationID value
     * @param string $validationID
     * @return \ID3Global\Models\GlobalCaseDocumentReport
     */
    public function setValidationID(?string $validationID = null): self
    {
        // validation for constraint: string
        if (!is_null($validationID) && !is_string($validationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($validationID, true), gettype($validationID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($validationID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $validationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($validationID, true)), __LINE__);
        }
        $this->ValidationID = $validationID;
        
        return $this;
    }
    /**
     * Get AuthenticationID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAuthenticationID(): ?string
    {
        return isset($this->AuthenticationID) ? $this->AuthenticationID : null;
    }
    /**
     * Set AuthenticationID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $authenticationID
     * @return \ID3Global\Models\GlobalCaseDocumentReport
     */
    public function setAuthenticationID(?string $authenticationID = null): self
    {
        // validation for constraint: string
        if (!is_null($authenticationID) && !is_string($authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authenticationID, true), gettype($authenticationID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($authenticationID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($authenticationID, true)), __LINE__);
        }
        if (is_null($authenticationID) || (is_array($authenticationID) && empty($authenticationID))) {
            unset($this->AuthenticationID);
        } else {
            $this->AuthenticationID = $authenticationID;
        }
        
        return $this;
    }
    /**
     * Get Result value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalMatrixResult|null
     */
    public function getResult(): ?\ID3Global\Models\GlobalMatrixResult
    {
        return isset($this->Result) ? $this->Result : null;
    }
    /**
     * Set Result value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalMatrixResult $result
     * @return \ID3Global\Models\GlobalCaseDocumentReport
     */
    public function setResult(?\ID3Global\Models\GlobalMatrixResult $result = null): self
    {
        if (is_null($result) || (is_array($result) && empty($result))) {
            unset($this->Result);
        } else {
            $this->Result = $result;
        }
        
        return $this;
    }
    /**
     * Get DocumentType value
     * @return string|null
     */
    public function getDocumentType(): ?string
    {
        return $this->DocumentType;
    }
    /**
     * Set DocumentType value
     * @uses \ID3Global\Enums\GlobalDocumentType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalDocumentType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $documentType
     * @return \ID3Global\Models\GlobalCaseDocumentReport
     */
    public function setDocumentType(?string $documentType = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalDocumentType::valueIsValid($documentType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalDocumentType', is_array($documentType) ? implode(', ', $documentType) : var_export($documentType, true), implode(', ', \ID3Global\Enums\GlobalDocumentType::getValidValues())), __LINE__);
        }
        $this->DocumentType = $documentType;
        
        return $this;
    }
    /**
     * Get LastChecked value
     * @return string|null
     */
    public function getLastChecked(): ?string
    {
        return $this->LastChecked;
    }
    /**
     * Set LastChecked value
     * @param string $lastChecked
     * @return \ID3Global\Models\GlobalCaseDocumentReport
     */
    public function setLastChecked(?string $lastChecked = null): self
    {
        // validation for constraint: string
        if (!is_null($lastChecked) && !is_string($lastChecked)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastChecked, true), gettype($lastChecked)), __LINE__);
        }
        $this->LastChecked = $lastChecked;
        
        return $this;
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @uses \ID3Global\Enums\GlobalDIVData_GlobalDocumentExtractedStatus::valueIsValid()
     * @uses \ID3Global\Enums\GlobalDIVData_GlobalDocumentExtractedStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $status
     * @return \ID3Global\Models\GlobalCaseDocumentReport
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalDIVData_GlobalDocumentExtractedStatus::valueIsValid($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalDIVData_GlobalDocumentExtractedStatus', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \ID3Global\Enums\GlobalDIVData_GlobalDocumentExtractedStatus::getValidValues())), __LINE__);
        }
        $this->Status = $status;
        
        return $this;
    }
}
