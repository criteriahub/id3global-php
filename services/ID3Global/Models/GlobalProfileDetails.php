<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalProfileDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q202:GlobalProfileDetails
 * @subpackage Structs
 */
class GlobalProfileDetails extends GlobalProfileVersion
{
    /**
     * The DecisionBanding
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDecisionBanding|null
     */
    protected ?\ID3Global\Models\GlobalDecisionBanding $DecisionBanding = null;
    /**
     * The Items
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItem|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItem $Items = null;
    /**
     * The PreAuthenticationRules
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalPreAuthentication|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalPreAuthentication $PreAuthenticationRules = null;
    /**
     * The ItemCheckDecisionBands
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBand|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBand $ItemCheckDecisionBands = null;
    /**
     * Constructor method for GlobalProfileDetails
     * @uses GlobalProfileDetails::setDecisionBanding()
     * @uses GlobalProfileDetails::setItems()
     * @uses GlobalProfileDetails::setPreAuthenticationRules()
     * @uses GlobalProfileDetails::setItemCheckDecisionBands()
     * @param \ID3Global\Models\GlobalDecisionBanding $decisionBanding
     * @param \ID3Global\Arrays\ArrayOfGlobalItem $items
     * @param \ID3Global\Arrays\ArrayOfGlobalPreAuthentication $preAuthenticationRules
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBand $itemCheckDecisionBands
     */
    public function __construct(?\ID3Global\Models\GlobalDecisionBanding $decisionBanding = null, ?\ID3Global\Arrays\ArrayOfGlobalItem $items = null, ?\ID3Global\Arrays\ArrayOfGlobalPreAuthentication $preAuthenticationRules = null, ?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBand $itemCheckDecisionBands = null)
    {
        $this
            ->setDecisionBanding($decisionBanding)
            ->setItems($items)
            ->setPreAuthenticationRules($preAuthenticationRules)
            ->setItemCheckDecisionBands($itemCheckDecisionBands);
    }
    /**
     * Get DecisionBanding value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDecisionBanding|null
     */
    public function getDecisionBanding(): ?\ID3Global\Models\GlobalDecisionBanding
    {
        return isset($this->DecisionBanding) ? $this->DecisionBanding : null;
    }
    /**
     * Set DecisionBanding value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalDecisionBanding $decisionBanding
     * @return \ID3Global\Models\GlobalProfileDetails
     */
    public function setDecisionBanding(?\ID3Global\Models\GlobalDecisionBanding $decisionBanding = null): self
    {
        if (is_null($decisionBanding) || (is_array($decisionBanding) && empty($decisionBanding))) {
            unset($this->DecisionBanding);
        } else {
            $this->DecisionBanding = $decisionBanding;
        }
        
        return $this;
    }
    /**
     * Get Items value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItem|null
     */
    public function getItems(): ?\ID3Global\Arrays\ArrayOfGlobalItem
    {
        return isset($this->Items) ? $this->Items : null;
    }
    /**
     * Set Items value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItem $items
     * @return \ID3Global\Models\GlobalProfileDetails
     */
    public function setItems(?\ID3Global\Arrays\ArrayOfGlobalItem $items = null): self
    {
        if (is_null($items) || (is_array($items) && empty($items))) {
            unset($this->Items);
        } else {
            $this->Items = $items;
        }
        
        return $this;
    }
    /**
     * Get PreAuthenticationRules value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalPreAuthentication|null
     */
    public function getPreAuthenticationRules(): ?\ID3Global\Arrays\ArrayOfGlobalPreAuthentication
    {
        return isset($this->PreAuthenticationRules) ? $this->PreAuthenticationRules : null;
    }
    /**
     * Set PreAuthenticationRules value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalPreAuthentication $preAuthenticationRules
     * @return \ID3Global\Models\GlobalProfileDetails
     */
    public function setPreAuthenticationRules(?\ID3Global\Arrays\ArrayOfGlobalPreAuthentication $preAuthenticationRules = null): self
    {
        if (is_null($preAuthenticationRules) || (is_array($preAuthenticationRules) && empty($preAuthenticationRules))) {
            unset($this->PreAuthenticationRules);
        } else {
            $this->PreAuthenticationRules = $preAuthenticationRules;
        }
        
        return $this;
    }
    /**
     * Get ItemCheckDecisionBands value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBand|null
     */
    public function getItemCheckDecisionBands(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBand
    {
        return isset($this->ItemCheckDecisionBands) ? $this->ItemCheckDecisionBands : null;
    }
    /**
     * Set ItemCheckDecisionBands value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBand $itemCheckDecisionBands
     * @return \ID3Global\Models\GlobalProfileDetails
     */
    public function setItemCheckDecisionBands(?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBand $itemCheckDecisionBands = null): self
    {
        if (is_null($itemCheckDecisionBands) || (is_array($itemCheckDecisionBands) && empty($itemCheckDecisionBands))) {
            unset($this->ItemCheckDecisionBands);
        } else {
            $this->ItemCheckDecisionBands = $itemCheckDecisionBands;
        }
        
        return $this;
    }
}
