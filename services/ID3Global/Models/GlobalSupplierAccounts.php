<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSupplierAccounts Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q631:GlobalSupplierAccounts
 * @subpackage Structs
 */
class GlobalSupplierAccounts extends AbstractStructBase
{
    /**
     * The SupplierAccounts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSupplierAccount|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSupplierAccount $SupplierAccounts = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The TotalPages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPages = null;
    /**
     * The TotalAccounts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalAccounts = null;
    /**
     * The TotalActive
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalActive = null;
    /**
     * The TotalPending
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPending = null;
    /**
     * Constructor method for GlobalSupplierAccounts
     * @uses GlobalSupplierAccounts::setSupplierAccounts()
     * @uses GlobalSupplierAccounts::setPageSize()
     * @uses GlobalSupplierAccounts::setTotalPages()
     * @uses GlobalSupplierAccounts::setTotalAccounts()
     * @uses GlobalSupplierAccounts::setTotalActive()
     * @uses GlobalSupplierAccounts::setTotalPending()
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplierAccount $supplierAccounts
     * @param int $pageSize
     * @param int $totalPages
     * @param int $totalAccounts
     * @param int $totalActive
     * @param int $totalPending
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalSupplierAccount $supplierAccounts = null, ?int $pageSize = null, ?int $totalPages = null, ?int $totalAccounts = null, ?int $totalActive = null, ?int $totalPending = null)
    {
        $this
            ->setSupplierAccounts($supplierAccounts)
            ->setPageSize($pageSize)
            ->setTotalPages($totalPages)
            ->setTotalAccounts($totalAccounts)
            ->setTotalActive($totalActive)
            ->setTotalPending($totalPending);
    }
    /**
     * Get SupplierAccounts value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierAccount|null
     */
    public function getSupplierAccounts(): ?\ID3Global\Arrays\ArrayOfGlobalSupplierAccount
    {
        return isset($this->SupplierAccounts) ? $this->SupplierAccounts : null;
    }
    /**
     * Set SupplierAccounts value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplierAccount $supplierAccounts
     * @return \ID3Global\Models\GlobalSupplierAccounts
     */
    public function setSupplierAccounts(?\ID3Global\Arrays\ArrayOfGlobalSupplierAccount $supplierAccounts = null): self
    {
        if (is_null($supplierAccounts) || (is_array($supplierAccounts) && empty($supplierAccounts))) {
            unset($this->SupplierAccounts);
        } else {
            $this->SupplierAccounts = $supplierAccounts;
        }
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GlobalSupplierAccounts
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get TotalPages value
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->TotalPages;
    }
    /**
     * Set TotalPages value
     * @param int $totalPages
     * @return \ID3Global\Models\GlobalSupplierAccounts
     */
    public function setTotalPages(?int $totalPages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPages) && !(is_int($totalPages) || ctype_digit($totalPages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPages, true), gettype($totalPages)), __LINE__);
        }
        $this->TotalPages = $totalPages;
        
        return $this;
    }
    /**
     * Get TotalAccounts value
     * @return int|null
     */
    public function getTotalAccounts(): ?int
    {
        return $this->TotalAccounts;
    }
    /**
     * Set TotalAccounts value
     * @param int $totalAccounts
     * @return \ID3Global\Models\GlobalSupplierAccounts
     */
    public function setTotalAccounts(?int $totalAccounts = null): self
    {
        // validation for constraint: int
        if (!is_null($totalAccounts) && !(is_int($totalAccounts) || ctype_digit($totalAccounts))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalAccounts, true), gettype($totalAccounts)), __LINE__);
        }
        $this->TotalAccounts = $totalAccounts;
        
        return $this;
    }
    /**
     * Get TotalActive value
     * @return int|null
     */
    public function getTotalActive(): ?int
    {
        return $this->TotalActive;
    }
    /**
     * Set TotalActive value
     * @param int $totalActive
     * @return \ID3Global\Models\GlobalSupplierAccounts
     */
    public function setTotalActive(?int $totalActive = null): self
    {
        // validation for constraint: int
        if (!is_null($totalActive) && !(is_int($totalActive) || ctype_digit($totalActive))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalActive, true), gettype($totalActive)), __LINE__);
        }
        $this->TotalActive = $totalActive;
        
        return $this;
    }
    /**
     * Get TotalPending value
     * @return int|null
     */
    public function getTotalPending(): ?int
    {
        return $this->TotalPending;
    }
    /**
     * Set TotalPending value
     * @param int $totalPending
     * @return \ID3Global\Models\GlobalSupplierAccounts
     */
    public function setTotalPending(?int $totalPending = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPending) && !(is_int($totalPending) || ctype_digit($totalPending))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPending, true), gettype($totalPending)), __LINE__);
        }
        $this->TotalPending = $totalPending;
        
        return $this;
    }
}
