<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDIVData Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q126:GlobalDIVData
 * @subpackage Structs
 */
class GlobalDIVData extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The AuthenticationID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $AuthenticationID = null;
    /**
     * The ExtractedData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\ExtractedData|null
     */
    protected ?\ID3Global\Models\ExtractedData $ExtractedData = null;
    /**
     * The Document
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\DocumentDetails|null
     */
    protected ?\ID3Global\Models\DocumentDetails $Document = null;
    /**
     * The TestResults
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfDocumentForgeryTests|null
     */
    protected ?\ID3Global\Arrays\ArrayOfDocumentForgeryTests $TestResults = null;
    /**
     * The Checks
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\ImageChecks|null
     */
    protected ?\ID3Global\Models\ImageChecks $Checks = null;
    /**
     * The Image
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\ImageData|null
     */
    protected ?\ID3Global\Models\ImageData $Image = null;
    /**
     * The Status
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Status = null;
    /**
     * Constructor method for GlobalDIVData
     * @uses GlobalDIVData::setOrgID()
     * @uses GlobalDIVData::setAuthenticationID()
     * @uses GlobalDIVData::setExtractedData()
     * @uses GlobalDIVData::setDocument()
     * @uses GlobalDIVData::setTestResults()
     * @uses GlobalDIVData::setChecks()
     * @uses GlobalDIVData::setImage()
     * @uses GlobalDIVData::setStatus()
     * @param string $orgID
     * @param string $authenticationID
     * @param \ID3Global\Models\ExtractedData $extractedData
     * @param \ID3Global\Models\DocumentDetails $document
     * @param \ID3Global\Arrays\ArrayOfDocumentForgeryTests $testResults
     * @param \ID3Global\Models\ImageChecks $checks
     * @param \ID3Global\Models\ImageData $image
     * @param string $status
     */
    public function __construct(?string $orgID = null, ?string $authenticationID = null, ?\ID3Global\Models\ExtractedData $extractedData = null, ?\ID3Global\Models\DocumentDetails $document = null, ?\ID3Global\Arrays\ArrayOfDocumentForgeryTests $testResults = null, ?\ID3Global\Models\ImageChecks $checks = null, ?\ID3Global\Models\ImageData $image = null, ?string $status = null)
    {
        $this
            ->setOrgID($orgID)
            ->setAuthenticationID($authenticationID)
            ->setExtractedData($extractedData)
            ->setDocument($document)
            ->setTestResults($testResults)
            ->setChecks($checks)
            ->setImage($image)
            ->setStatus($status);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GlobalDIVData
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get AuthenticationID value
     * @return string|null
     */
    public function getAuthenticationID(): ?string
    {
        return $this->AuthenticationID;
    }
    /**
     * Set AuthenticationID value
     * @param string $authenticationID
     * @return \ID3Global\Models\GlobalDIVData
     */
    public function setAuthenticationID(?string $authenticationID = null): self
    {
        // validation for constraint: string
        if (!is_null($authenticationID) && !is_string($authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authenticationID, true), gettype($authenticationID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($authenticationID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $authenticationID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($authenticationID, true)), __LINE__);
        }
        $this->AuthenticationID = $authenticationID;
        
        return $this;
    }
    /**
     * Get ExtractedData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\ExtractedData|null
     */
    public function getExtractedData(): ?\ID3Global\Models\ExtractedData
    {
        return isset($this->ExtractedData) ? $this->ExtractedData : null;
    }
    /**
     * Set ExtractedData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\ExtractedData $extractedData
     * @return \ID3Global\Models\GlobalDIVData
     */
    public function setExtractedData(?\ID3Global\Models\ExtractedData $extractedData = null): self
    {
        if (is_null($extractedData) || (is_array($extractedData) && empty($extractedData))) {
            unset($this->ExtractedData);
        } else {
            $this->ExtractedData = $extractedData;
        }
        
        return $this;
    }
    /**
     * Get Document value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\DocumentDetails|null
     */
    public function getDocument(): ?\ID3Global\Models\DocumentDetails
    {
        return isset($this->Document) ? $this->Document : null;
    }
    /**
     * Set Document value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\DocumentDetails $document
     * @return \ID3Global\Models\GlobalDIVData
     */
    public function setDocument(?\ID3Global\Models\DocumentDetails $document = null): self
    {
        if (is_null($document) || (is_array($document) && empty($document))) {
            unset($this->Document);
        } else {
            $this->Document = $document;
        }
        
        return $this;
    }
    /**
     * Get TestResults value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfDocumentForgeryTests|null
     */
    public function getTestResults(): ?\ID3Global\Arrays\ArrayOfDocumentForgeryTests
    {
        return isset($this->TestResults) ? $this->TestResults : null;
    }
    /**
     * Set TestResults value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfDocumentForgeryTests $testResults
     * @return \ID3Global\Models\GlobalDIVData
     */
    public function setTestResults(?\ID3Global\Arrays\ArrayOfDocumentForgeryTests $testResults = null): self
    {
        if (is_null($testResults) || (is_array($testResults) && empty($testResults))) {
            unset($this->TestResults);
        } else {
            $this->TestResults = $testResults;
        }
        
        return $this;
    }
    /**
     * Get Checks value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\ImageChecks|null
     */
    public function getChecks(): ?\ID3Global\Models\ImageChecks
    {
        return isset($this->Checks) ? $this->Checks : null;
    }
    /**
     * Set Checks value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\ImageChecks $checks
     * @return \ID3Global\Models\GlobalDIVData
     */
    public function setChecks(?\ID3Global\Models\ImageChecks $checks = null): self
    {
        if (is_null($checks) || (is_array($checks) && empty($checks))) {
            unset($this->Checks);
        } else {
            $this->Checks = $checks;
        }
        
        return $this;
    }
    /**
     * Get Image value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\ImageData|null
     */
    public function getImage(): ?\ID3Global\Models\ImageData
    {
        return isset($this->Image) ? $this->Image : null;
    }
    /**
     * Set Image value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\ImageData $image
     * @return \ID3Global\Models\GlobalDIVData
     */
    public function setImage(?\ID3Global\Models\ImageData $image = null): self
    {
        if (is_null($image) || (is_array($image) && empty($image))) {
            unset($this->Image);
        } else {
            $this->Image = $image;
        }
        
        return $this;
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @uses \ID3Global\Enums\GlobalDIVData_GlobalDocumentExtractedStatus::valueIsValid()
     * @uses \ID3Global\Enums\GlobalDIVData_GlobalDocumentExtractedStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $status
     * @return \ID3Global\Models\GlobalDIVData
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalDIVData_GlobalDocumentExtractedStatus::valueIsValid($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalDIVData_GlobalDocumentExtractedStatus', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \ID3Global\Enums\GlobalDIVData_GlobalDocumentExtractedStatus::getValidValues())), __LINE__);
        }
        $this->Status = $status;
        
        return $this;
    }
}
