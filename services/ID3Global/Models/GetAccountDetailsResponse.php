<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAccountDetailsResponse Models
 * @subpackage Structs
 */
class GetAccountDetailsResponse extends AbstractStructBase
{
    /**
     * The GetAccountDetailsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAccountDetails|null
     */
    protected ?\ID3Global\Models\GlobalAccountDetails $GetAccountDetailsResult = null;
    /**
     * Constructor method for GetAccountDetailsResponse
     * @uses GetAccountDetailsResponse::setGetAccountDetailsResult()
     * @param \ID3Global\Models\GlobalAccountDetails $getAccountDetailsResult
     */
    public function __construct(?\ID3Global\Models\GlobalAccountDetails $getAccountDetailsResult = null)
    {
        $this
            ->setGetAccountDetailsResult($getAccountDetailsResult);
    }
    /**
     * Get GetAccountDetailsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAccountDetails|null
     */
    public function getGetAccountDetailsResult(): ?\ID3Global\Models\GlobalAccountDetails
    {
        return isset($this->GetAccountDetailsResult) ? $this->GetAccountDetailsResult : null;
    }
    /**
     * Set GetAccountDetailsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAccountDetails $getAccountDetailsResult
     * @return \ID3Global\Models\GetAccountDetailsResponse
     */
    public function setGetAccountDetailsResult(?\ID3Global\Models\GlobalAccountDetails $getAccountDetailsResult = null): self
    {
        if (is_null($getAccountDetailsResult) || (is_array($getAccountDetailsResult) && empty($getAccountDetailsResult))) {
            unset($this->GetAccountDetailsResult);
        } else {
            $this->GetAccountDetailsResult = $getAccountDetailsResult;
        }
        
        return $this;
    }
}
