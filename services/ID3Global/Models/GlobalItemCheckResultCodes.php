<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalItemCheckResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q35:GlobalItemCheckResultCodes
 * @subpackage Structs
 */
class GlobalItemCheckResultCodes extends AbstractStructBase
{
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Description = null;
    /**
     * The Comment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $Comment = null;
    /**
     * The Match
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $Match = null;
    /**
     * The Warning
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $Warning = null;
    /**
     * The Mismatch
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $Mismatch = null;
    /**
     * The ID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ID = null;
    /**
     * The Pass
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Pass = null;
    /**
     * The Address
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Address = null;
    /**
     * The Forename
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Forename = null;
    /**
     * The Surname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Surname = null;
    /**
     * The DOB
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $DOB = null;
    /**
     * The Alert
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Alert = null;
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Country = null;
    /**
     * Constructor method for GlobalItemCheckResultCodes
     * @uses GlobalItemCheckResultCodes::setName()
     * @uses GlobalItemCheckResultCodes::setDescription()
     * @uses GlobalItemCheckResultCodes::setComment()
     * @uses GlobalItemCheckResultCodes::setMatch()
     * @uses GlobalItemCheckResultCodes::setWarning()
     * @uses GlobalItemCheckResultCodes::setMismatch()
     * @uses GlobalItemCheckResultCodes::setID()
     * @uses GlobalItemCheckResultCodes::setPass()
     * @uses GlobalItemCheckResultCodes::setAddress()
     * @uses GlobalItemCheckResultCodes::setForename()
     * @uses GlobalItemCheckResultCodes::setSurname()
     * @uses GlobalItemCheckResultCodes::setDOB()
     * @uses GlobalItemCheckResultCodes::setAlert()
     * @uses GlobalItemCheckResultCodes::setCountry()
     * @param string $name
     * @param string $description
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $comment
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $match
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $warning
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $mismatch
     * @param int $iD
     * @param string $pass
     * @param string $address
     * @param string $forename
     * @param string $surname
     * @param string $dOB
     * @param string $alert
     * @param string $country
     */
    public function __construct(?string $name = null, ?string $description = null, ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $comment = null, ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $match = null, ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $warning = null, ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $mismatch = null, ?int $iD = null, ?string $pass = null, ?string $address = null, ?string $forename = null, ?string $surname = null, ?string $dOB = null, ?string $alert = null, ?string $country = null)
    {
        $this
            ->setName($name)
            ->setDescription($description)
            ->setComment($comment)
            ->setMatch($match)
            ->setWarning($warning)
            ->setMismatch($mismatch)
            ->setID($iD)
            ->setPass($pass)
            ->setAddress($address)
            ->setForename($forename)
            ->setSurname($surname)
            ->setDOB($dOB)
            ->setAlert($alert)
            ->setCountry($country);
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        
        return $this;
    }
    /**
     * Get Comment value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode|null
     */
    public function getComment(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode
    {
        return isset($this->Comment) ? $this->Comment : null;
    }
    /**
     * Set Comment value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $comment
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setComment(?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $comment = null): self
    {
        if (is_null($comment) || (is_array($comment) && empty($comment))) {
            unset($this->Comment);
        } else {
            $this->Comment = $comment;
        }
        
        return $this;
    }
    /**
     * Get Match value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode|null
     */
    public function getMatch(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode
    {
        return isset($this->Match) ? $this->Match : null;
    }
    /**
     * Set Match value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $match
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setMatch(?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $match = null): self
    {
        if (is_null($match) || (is_array($match) && empty($match))) {
            unset($this->Match);
        } else {
            $this->Match = $match;
        }
        
        return $this;
    }
    /**
     * Get Warning value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode|null
     */
    public function getWarning(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode
    {
        return isset($this->Warning) ? $this->Warning : null;
    }
    /**
     * Set Warning value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $warning
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setWarning(?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $warning = null): self
    {
        if (is_null($warning) || (is_array($warning) && empty($warning))) {
            unset($this->Warning);
        } else {
            $this->Warning = $warning;
        }
        
        return $this;
    }
    /**
     * Get Mismatch value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode|null
     */
    public function getMismatch(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode
    {
        return isset($this->Mismatch) ? $this->Mismatch : null;
    }
    /**
     * Set Mismatch value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $mismatch
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setMismatch(?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCode $mismatch = null): self
    {
        if (is_null($mismatch) || (is_array($mismatch) && empty($mismatch))) {
            unset($this->Mismatch);
        } else {
            $this->Mismatch = $mismatch;
        }
        
        return $this;
    }
    /**
     * Get ID value
     * @return int|null
     */
    public function getID(): ?int
    {
        return $this->ID;
    }
    /**
     * Set ID value
     * @param int $iD
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setID(?int $iD = null): self
    {
        // validation for constraint: int
        if (!is_null($iD) && !(is_int($iD) || ctype_digit($iD))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($iD, true), gettype($iD)), __LINE__);
        }
        $this->ID = $iD;
        
        return $this;
    }
    /**
     * Get Pass value
     * @return string|null
     */
    public function getPass(): ?string
    {
        return $this->Pass;
    }
    /**
     * Set Pass value
     * @uses \ID3Global\Enums\GlobalMatch::valueIsValid()
     * @uses \ID3Global\Enums\GlobalMatch::getValidValues()
     * @throws InvalidArgumentException
     * @param string $pass
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setPass(?string $pass = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalMatch::valueIsValid($pass)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalMatch', is_array($pass) ? implode(', ', $pass) : var_export($pass, true), implode(', ', \ID3Global\Enums\GlobalMatch::getValidValues())), __LINE__);
        }
        $this->Pass = $pass;
        
        return $this;
    }
    /**
     * Get Address value
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->Address;
    }
    /**
     * Set Address value
     * @uses \ID3Global\Enums\GlobalMatch::valueIsValid()
     * @uses \ID3Global\Enums\GlobalMatch::getValidValues()
     * @throws InvalidArgumentException
     * @param string $address
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setAddress(?string $address = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalMatch::valueIsValid($address)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalMatch', is_array($address) ? implode(', ', $address) : var_export($address, true), implode(', ', \ID3Global\Enums\GlobalMatch::getValidValues())), __LINE__);
        }
        $this->Address = $address;
        
        return $this;
    }
    /**
     * Get Forename value
     * @return string|null
     */
    public function getForename(): ?string
    {
        return $this->Forename;
    }
    /**
     * Set Forename value
     * @uses \ID3Global\Enums\GlobalMatch::valueIsValid()
     * @uses \ID3Global\Enums\GlobalMatch::getValidValues()
     * @throws InvalidArgumentException
     * @param string $forename
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setForename(?string $forename = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalMatch::valueIsValid($forename)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalMatch', is_array($forename) ? implode(', ', $forename) : var_export($forename, true), implode(', ', \ID3Global\Enums\GlobalMatch::getValidValues())), __LINE__);
        }
        $this->Forename = $forename;
        
        return $this;
    }
    /**
     * Get Surname value
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->Surname;
    }
    /**
     * Set Surname value
     * @uses \ID3Global\Enums\GlobalMatch::valueIsValid()
     * @uses \ID3Global\Enums\GlobalMatch::getValidValues()
     * @throws InvalidArgumentException
     * @param string $surname
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setSurname(?string $surname = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalMatch::valueIsValid($surname)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalMatch', is_array($surname) ? implode(', ', $surname) : var_export($surname, true), implode(', ', \ID3Global\Enums\GlobalMatch::getValidValues())), __LINE__);
        }
        $this->Surname = $surname;
        
        return $this;
    }
    /**
     * Get DOB value
     * @return string|null
     */
    public function getDOB(): ?string
    {
        return $this->DOB;
    }
    /**
     * Set DOB value
     * @uses \ID3Global\Enums\GlobalMatch::valueIsValid()
     * @uses \ID3Global\Enums\GlobalMatch::getValidValues()
     * @throws InvalidArgumentException
     * @param string $dOB
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setDOB(?string $dOB = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalMatch::valueIsValid($dOB)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalMatch', is_array($dOB) ? implode(', ', $dOB) : var_export($dOB, true), implode(', ', \ID3Global\Enums\GlobalMatch::getValidValues())), __LINE__);
        }
        $this->DOB = $dOB;
        
        return $this;
    }
    /**
     * Get Alert value
     * @return string|null
     */
    public function getAlert(): ?string
    {
        return $this->Alert;
    }
    /**
     * Set Alert value
     * @uses \ID3Global\Enums\GlobalMatch::valueIsValid()
     * @uses \ID3Global\Enums\GlobalMatch::getValidValues()
     * @throws InvalidArgumentException
     * @param string $alert
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setAlert(?string $alert = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalMatch::valueIsValid($alert)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalMatch', is_array($alert) ? implode(', ', $alert) : var_export($alert, true), implode(', ', \ID3Global\Enums\GlobalMatch::getValidValues())), __LINE__);
        }
        $this->Alert = $alert;
        
        return $this;
    }
    /**
     * Get Country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return isset($this->Country) ? $this->Country : null;
    }
    /**
     * Set Country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $country
     * @return \ID3Global\Models\GlobalItemCheckResultCodes
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->Country);
        } else {
            $this->Country = $country;
        }
        
        return $this;
    }
}
