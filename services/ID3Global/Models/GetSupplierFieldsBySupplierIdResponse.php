<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSupplierFieldsBySupplierIdResponse Models
 * @subpackage Structs
 */
class GetSupplierFieldsBySupplierIdResponse extends AbstractStructBase
{
    /**
     * The GetSupplierFieldsBySupplierIdResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierConfig|null
     */
    protected ?\ID3Global\Models\GlobalSupplierConfig $GetSupplierFieldsBySupplierIdResult = null;
    /**
     * Constructor method for GetSupplierFieldsBySupplierIdResponse
     * @uses GetSupplierFieldsBySupplierIdResponse::setGetSupplierFieldsBySupplierIdResult()
     * @param \ID3Global\Models\GlobalSupplierConfig $getSupplierFieldsBySupplierIdResult
     */
    public function __construct(?\ID3Global\Models\GlobalSupplierConfig $getSupplierFieldsBySupplierIdResult = null)
    {
        $this
            ->setGetSupplierFieldsBySupplierIdResult($getSupplierFieldsBySupplierIdResult);
    }
    /**
     * Get GetSupplierFieldsBySupplierIdResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierConfig|null
     */
    public function getGetSupplierFieldsBySupplierIdResult(): ?\ID3Global\Models\GlobalSupplierConfig
    {
        return isset($this->GetSupplierFieldsBySupplierIdResult) ? $this->GetSupplierFieldsBySupplierIdResult : null;
    }
    /**
     * Set GetSupplierFieldsBySupplierIdResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupplierConfig $getSupplierFieldsBySupplierIdResult
     * @return \ID3Global\Models\GetSupplierFieldsBySupplierIdResponse
     */
    public function setGetSupplierFieldsBySupplierIdResult(?\ID3Global\Models\GlobalSupplierConfig $getSupplierFieldsBySupplierIdResult = null): self
    {
        if (is_null($getSupplierFieldsBySupplierIdResult) || (is_array($getSupplierFieldsBySupplierIdResult) && empty($getSupplierFieldsBySupplierIdResult))) {
            unset($this->GetSupplierFieldsBySupplierIdResult);
        } else {
            $this->GetSupplierFieldsBySupplierIdResult = $getSupplierFieldsBySupplierIdResult;
        }
        
        return $this;
    }
}
