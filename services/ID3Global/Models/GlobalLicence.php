<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalLicence Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q272:GlobalLicence
 * @subpackage Structs
 */
class GlobalLicence extends AbstractStructBase
{
    /**
     * The ItemChecks
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalLicenceItem|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalLicenceItem $ItemChecks = null;
    /**
     * The Options
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalLicenceItem|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalLicenceItem $Options = null;
    /**
     * Constructor method for GlobalLicence
     * @uses GlobalLicence::setItemChecks()
     * @uses GlobalLicence::setOptions()
     * @param \ID3Global\Arrays\ArrayOfGlobalLicenceItem $itemChecks
     * @param \ID3Global\Arrays\ArrayOfGlobalLicenceItem $options
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalLicenceItem $itemChecks = null, ?\ID3Global\Arrays\ArrayOfGlobalLicenceItem $options = null)
    {
        $this
            ->setItemChecks($itemChecks)
            ->setOptions($options);
    }
    /**
     * Get ItemChecks value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalLicenceItem|null
     */
    public function getItemChecks(): ?\ID3Global\Arrays\ArrayOfGlobalLicenceItem
    {
        return isset($this->ItemChecks) ? $this->ItemChecks : null;
    }
    /**
     * Set ItemChecks value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalLicenceItem $itemChecks
     * @return \ID3Global\Models\GlobalLicence
     */
    public function setItemChecks(?\ID3Global\Arrays\ArrayOfGlobalLicenceItem $itemChecks = null): self
    {
        if (is_null($itemChecks) || (is_array($itemChecks) && empty($itemChecks))) {
            unset($this->ItemChecks);
        } else {
            $this->ItemChecks = $itemChecks;
        }
        
        return $this;
    }
    /**
     * Get Options value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalLicenceItem|null
     */
    public function getOptions(): ?\ID3Global\Arrays\ArrayOfGlobalLicenceItem
    {
        return isset($this->Options) ? $this->Options : null;
    }
    /**
     * Set Options value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalLicenceItem $options
     * @return \ID3Global\Models\GlobalLicence
     */
    public function setOptions(?\ID3Global\Arrays\ArrayOfGlobalLicenceItem $options = null): self
    {
        if (is_null($options) || (is_array($options) && empty($options))) {
            unset($this->Options);
        } else {
            $this->Options = $options;
        }
        
        return $this;
    }
}
