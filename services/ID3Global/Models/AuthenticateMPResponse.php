<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AuthenticateMPResponse Models
 * @subpackage Structs
 */
class AuthenticateMPResponse extends AbstractStructBase
{
    /**
     * The AuthenticateMPResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalResultData|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalResultData $AuthenticateMPResult = null;
    /**
     * Constructor method for AuthenticateMPResponse
     * @uses AuthenticateMPResponse::setAuthenticateMPResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalResultData $authenticateMPResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalResultData $authenticateMPResult = null)
    {
        $this
            ->setAuthenticateMPResult($authenticateMPResult);
    }
    /**
     * Get AuthenticateMPResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalResultData|null
     */
    public function getAuthenticateMPResult(): ?\ID3Global\Arrays\ArrayOfGlobalResultData
    {
        return isset($this->AuthenticateMPResult) ? $this->AuthenticateMPResult : null;
    }
    /**
     * Set AuthenticateMPResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalResultData $authenticateMPResult
     * @return \ID3Global\Models\AuthenticateMPResponse
     */
    public function setAuthenticateMPResult(?\ID3Global\Arrays\ArrayOfGlobalResultData $authenticateMPResult = null): self
    {
        if (is_null($authenticateMPResult) || (is_array($authenticateMPResult) && empty($authenticateMPResult))) {
            unset($this->AuthenticateMPResult);
        } else {
            $this->AuthenticateMPResult = $authenticateMPResult;
        }
        
        return $this;
    }
}
