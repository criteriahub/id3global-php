<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProfileVersionItemIDsResponse Models
 * @subpackage Structs
 */
class GetProfileVersionItemIDsResponse extends AbstractStructBase
{
    /**
     * The GetProfileVersionItemIDsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    protected ?\ID3Global\Arrays\ArrayOfunsignedInt $GetProfileVersionItemIDsResult = null;
    /**
     * Constructor method for GetProfileVersionItemIDsResponse
     * @uses GetProfileVersionItemIDsResponse::setGetProfileVersionItemIDsResult()
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $getProfileVersionItemIDsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfunsignedInt $getProfileVersionItemIDsResult = null)
    {
        $this
            ->setGetProfileVersionItemIDsResult($getProfileVersionItemIDsResult);
    }
    /**
     * Get GetProfileVersionItemIDsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    public function getGetProfileVersionItemIDsResult(): ?\ID3Global\Arrays\ArrayOfunsignedInt
    {
        return isset($this->GetProfileVersionItemIDsResult) ? $this->GetProfileVersionItemIDsResult : null;
    }
    /**
     * Set GetProfileVersionItemIDsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $getProfileVersionItemIDsResult
     * @return \ID3Global\Models\GetProfileVersionItemIDsResponse
     */
    public function setGetProfileVersionItemIDsResult(?\ID3Global\Arrays\ArrayOfunsignedInt $getProfileVersionItemIDsResult = null): self
    {
        if (is_null($getProfileVersionItemIDsResult) || (is_array($getProfileVersionItemIDsResult) && empty($getProfileVersionItemIDsResult))) {
            unset($this->GetProfileVersionItemIDsResult);
        } else {
            $this->GetProfileVersionItemIDsResult = $getProfileVersionItemIDsResult;
        }
        
        return $this;
    }
}
