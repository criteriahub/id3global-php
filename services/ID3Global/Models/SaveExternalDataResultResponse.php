<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SaveExternalDataResultResponse Models
 * @subpackage Structs
 */
class SaveExternalDataResultResponse extends AbstractStructBase
{
    /**
     * The SaveExternalDataResultResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseDetails|null
     */
    protected ?\ID3Global\Models\GlobalCaseDetails $SaveExternalDataResultResult = null;
    /**
     * Constructor method for SaveExternalDataResultResponse
     * @uses SaveExternalDataResultResponse::setSaveExternalDataResultResult()
     * @param \ID3Global\Models\GlobalCaseDetails $saveExternalDataResultResult
     */
    public function __construct(?\ID3Global\Models\GlobalCaseDetails $saveExternalDataResultResult = null)
    {
        $this
            ->setSaveExternalDataResultResult($saveExternalDataResultResult);
    }
    /**
     * Get SaveExternalDataResultResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseDetails|null
     */
    public function getSaveExternalDataResultResult(): ?\ID3Global\Models\GlobalCaseDetails
    {
        return isset($this->SaveExternalDataResultResult) ? $this->SaveExternalDataResultResult : null;
    }
    /**
     * Set SaveExternalDataResultResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCaseDetails $saveExternalDataResultResult
     * @return \ID3Global\Models\SaveExternalDataResultResponse
     */
    public function setSaveExternalDataResultResult(?\ID3Global\Models\GlobalCaseDetails $saveExternalDataResultResult = null): self
    {
        if (is_null($saveExternalDataResultResult) || (is_array($saveExternalDataResultResult) && empty($saveExternalDataResultResult))) {
            unset($this->SaveExternalDataResultResult);
        } else {
            $this->SaveExternalDataResultResult = $saveExternalDataResultResult;
        }
        
        return $this;
    }
}
