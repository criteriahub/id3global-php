<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalBankAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q424:GlobalBankAccount
 * @subpackage Structs
 */
class GlobalBankAccount extends AbstractStructBase
{
    /**
     * The SortCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SortCode = null;
    /**
     * The AccountNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccountNumber = null;
    /**
     * Constructor method for GlobalBankAccount
     * @uses GlobalBankAccount::setSortCode()
     * @uses GlobalBankAccount::setAccountNumber()
     * @param string $sortCode
     * @param string $accountNumber
     */
    public function __construct(?string $sortCode = null, ?string $accountNumber = null)
    {
        $this
            ->setSortCode($sortCode)
            ->setAccountNumber($accountNumber);
    }
    /**
     * Get SortCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSortCode(): ?string
    {
        return isset($this->SortCode) ? $this->SortCode : null;
    }
    /**
     * Set SortCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sortCode
     * @return \ID3Global\Models\GlobalBankAccount
     */
    public function setSortCode(?string $sortCode = null): self
    {
        // validation for constraint: string
        if (!is_null($sortCode) && !is_string($sortCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sortCode, true), gettype($sortCode)), __LINE__);
        }
        if (is_null($sortCode) || (is_array($sortCode) && empty($sortCode))) {
            unset($this->SortCode);
        } else {
            $this->SortCode = $sortCode;
        }
        
        return $this;
    }
    /**
     * Get AccountNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccountNumber(): ?string
    {
        return isset($this->AccountNumber) ? $this->AccountNumber : null;
    }
    /**
     * Set AccountNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accountNumber
     * @return \ID3Global\Models\GlobalBankAccount
     */
    public function setAccountNumber(?string $accountNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($accountNumber) && !is_string($accountNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountNumber, true), gettype($accountNumber)), __LINE__);
        }
        if (is_null($accountNumber) || (is_array($accountNumber) && empty($accountNumber))) {
            unset($this->AccountNumber);
        } else {
            $this->AccountNumber = $accountNumber;
        }
        
        return $this;
    }
}
