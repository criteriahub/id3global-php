<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProfilesResponse Models
 * @subpackage Structs
 */
class GetProfilesResponse extends AbstractStructBase
{
    /**
     * The GetProfilesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalProfile|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalProfile $GetProfilesResult = null;
    /**
     * Constructor method for GetProfilesResponse
     * @uses GetProfilesResponse::setGetProfilesResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalProfile $getProfilesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalProfile $getProfilesResult = null)
    {
        $this
            ->setGetProfilesResult($getProfilesResult);
    }
    /**
     * Get GetProfilesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalProfile|null
     */
    public function getGetProfilesResult(): ?\ID3Global\Arrays\ArrayOfGlobalProfile
    {
        return isset($this->GetProfilesResult) ? $this->GetProfilesResult : null;
    }
    /**
     * Set GetProfilesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalProfile $getProfilesResult
     * @return \ID3Global\Models\GetProfilesResponse
     */
    public function setGetProfilesResult(?\ID3Global\Arrays\ArrayOfGlobalProfile $getProfilesResult = null): self
    {
        if (is_null($getProfilesResult) || (is_array($getProfilesResult) && empty($getProfilesResult))) {
            unset($this->GetProfilesResult);
        } else {
            $this->GetProfilesResult = $getProfilesResult;
        }
        
        return $this;
    }
}
