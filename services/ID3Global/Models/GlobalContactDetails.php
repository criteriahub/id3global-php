<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalContactDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q409:GlobalContactDetails
 * @subpackage Structs
 */
class GlobalContactDetails extends AbstractStructBase
{
    /**
     * The LandTelephone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalLandTelephone|null
     */
    protected ?\ID3Global\Models\GlobalLandTelephone $LandTelephone = null;
    /**
     * The MobileTelephone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalMobileTelephone|null
     */
    protected ?\ID3Global\Models\GlobalMobileTelephone $MobileTelephone = null;
    /**
     * The Email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Email = null;
    /**
     * The WorkTelephone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalWorkTelephone|null
     */
    protected ?\ID3Global\Models\GlobalWorkTelephone $WorkTelephone = null;
    /**
     * Constructor method for GlobalContactDetails
     * @uses GlobalContactDetails::setLandTelephone()
     * @uses GlobalContactDetails::setMobileTelephone()
     * @uses GlobalContactDetails::setEmail()
     * @uses GlobalContactDetails::setWorkTelephone()
     * @param \ID3Global\Models\GlobalLandTelephone $landTelephone
     * @param \ID3Global\Models\GlobalMobileTelephone $mobileTelephone
     * @param string $email
     * @param \ID3Global\Models\GlobalWorkTelephone $workTelephone
     */
    public function __construct(?\ID3Global\Models\GlobalLandTelephone $landTelephone = null, ?\ID3Global\Models\GlobalMobileTelephone $mobileTelephone = null, ?string $email = null, ?\ID3Global\Models\GlobalWorkTelephone $workTelephone = null)
    {
        $this
            ->setLandTelephone($landTelephone)
            ->setMobileTelephone($mobileTelephone)
            ->setEmail($email)
            ->setWorkTelephone($workTelephone);
    }
    /**
     * Get LandTelephone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalLandTelephone|null
     */
    public function getLandTelephone(): ?\ID3Global\Models\GlobalLandTelephone
    {
        return isset($this->LandTelephone) ? $this->LandTelephone : null;
    }
    /**
     * Set LandTelephone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalLandTelephone $landTelephone
     * @return \ID3Global\Models\GlobalContactDetails
     */
    public function setLandTelephone(?\ID3Global\Models\GlobalLandTelephone $landTelephone = null): self
    {
        if (is_null($landTelephone) || (is_array($landTelephone) && empty($landTelephone))) {
            unset($this->LandTelephone);
        } else {
            $this->LandTelephone = $landTelephone;
        }
        
        return $this;
    }
    /**
     * Get MobileTelephone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalMobileTelephone|null
     */
    public function getMobileTelephone(): ?\ID3Global\Models\GlobalMobileTelephone
    {
        return isset($this->MobileTelephone) ? $this->MobileTelephone : null;
    }
    /**
     * Set MobileTelephone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalMobileTelephone $mobileTelephone
     * @return \ID3Global\Models\GlobalContactDetails
     */
    public function setMobileTelephone(?\ID3Global\Models\GlobalMobileTelephone $mobileTelephone = null): self
    {
        if (is_null($mobileTelephone) || (is_array($mobileTelephone) && empty($mobileTelephone))) {
            unset($this->MobileTelephone);
        } else {
            $this->MobileTelephone = $mobileTelephone;
        }
        
        return $this;
    }
    /**
     * Get Email value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return isset($this->Email) ? $this->Email : null;
    }
    /**
     * Set Email value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email
     * @return \ID3Global\Models\GlobalContactDetails
     */
    public function setEmail(?string $email = null): self
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        if (is_null($email) || (is_array($email) && empty($email))) {
            unset($this->Email);
        } else {
            $this->Email = $email;
        }
        
        return $this;
    }
    /**
     * Get WorkTelephone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalWorkTelephone|null
     */
    public function getWorkTelephone(): ?\ID3Global\Models\GlobalWorkTelephone
    {
        return isset($this->WorkTelephone) ? $this->WorkTelephone : null;
    }
    /**
     * Set WorkTelephone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalWorkTelephone $workTelephone
     * @return \ID3Global\Models\GlobalContactDetails
     */
    public function setWorkTelephone(?\ID3Global\Models\GlobalWorkTelephone $workTelephone = null): self
    {
        if (is_null($workTelephone) || (is_array($workTelephone) && empty($workTelephone))) {
            unset($this->WorkTelephone);
        } else {
            $this->WorkTelephone = $workTelephone;
        }
        
        return $this;
    }
}
