<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateAccountResponse Models
 * @subpackage Structs
 */
class CreateAccountResponse extends AbstractStructBase
{
    /**
     * The CreateAccountResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAccount|null
     */
    protected ?\ID3Global\Models\GlobalAccount $CreateAccountResult = null;
    /**
     * Constructor method for CreateAccountResponse
     * @uses CreateAccountResponse::setCreateAccountResult()
     * @param \ID3Global\Models\GlobalAccount $createAccountResult
     */
    public function __construct(?\ID3Global\Models\GlobalAccount $createAccountResult = null)
    {
        $this
            ->setCreateAccountResult($createAccountResult);
    }
    /**
     * Get CreateAccountResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAccount|null
     */
    public function getCreateAccountResult(): ?\ID3Global\Models\GlobalAccount
    {
        return isset($this->CreateAccountResult) ? $this->CreateAccountResult : null;
    }
    /**
     * Set CreateAccountResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAccount $createAccountResult
     * @return \ID3Global\Models\CreateAccountResponse
     */
    public function setCreateAccountResult(?\ID3Global\Models\GlobalAccount $createAccountResult = null): self
    {
        if (is_null($createAccountResult) || (is_array($createAccountResult) && empty($createAccountResult))) {
            unset($this->CreateAccountResult);
        } else {
            $this->CreateAccountResult = $createAccountResult;
        }
        
        return $this;
    }
}
