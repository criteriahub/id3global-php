<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetItemCheckResultCodesResponse Models
 * @subpackage Structs
 */
class GetItemCheckResultCodesResponse extends AbstractStructBase
{
    /**
     * The GetItemCheckResultCodesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalItemCheckResultCodes|null
     */
    protected ?\ID3Global\Models\GlobalItemCheckResultCodes $GetItemCheckResultCodesResult = null;
    /**
     * Constructor method for GetItemCheckResultCodesResponse
     * @uses GetItemCheckResultCodesResponse::setGetItemCheckResultCodesResult()
     * @param \ID3Global\Models\GlobalItemCheckResultCodes $getItemCheckResultCodesResult
     */
    public function __construct(?\ID3Global\Models\GlobalItemCheckResultCodes $getItemCheckResultCodesResult = null)
    {
        $this
            ->setGetItemCheckResultCodesResult($getItemCheckResultCodesResult);
    }
    /**
     * Get GetItemCheckResultCodesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalItemCheckResultCodes|null
     */
    public function getGetItemCheckResultCodesResult(): ?\ID3Global\Models\GlobalItemCheckResultCodes
    {
        return isset($this->GetItemCheckResultCodesResult) ? $this->GetItemCheckResultCodesResult : null;
    }
    /**
     * Set GetItemCheckResultCodesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalItemCheckResultCodes $getItemCheckResultCodesResult
     * @return \ID3Global\Models\GetItemCheckResultCodesResponse
     */
    public function setGetItemCheckResultCodesResult(?\ID3Global\Models\GlobalItemCheckResultCodes $getItemCheckResultCodesResult = null): self
    {
        if (is_null($getItemCheckResultCodesResult) || (is_array($getItemCheckResultCodesResult) && empty($getItemCheckResultCodesResult))) {
            unset($this->GetItemCheckResultCodesResult);
        } else {
            $this->GetItemCheckResultCodesResult = $getItemCheckResultCodesResult;
        }
        
        return $this;
    }
}
