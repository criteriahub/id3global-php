<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateProfile Models
 * @subpackage Structs
 */
class UpdateProfile extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The Profile
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfile|null
     */
    protected ?\ID3Global\Models\GlobalProfile $Profile = null;
    /**
     * Constructor method for UpdateProfile
     * @uses UpdateProfile::setOrgID()
     * @uses UpdateProfile::setProfile()
     * @param string $orgID
     * @param \ID3Global\Models\GlobalProfile $profile
     */
    public function __construct(?string $orgID = null, ?\ID3Global\Models\GlobalProfile $profile = null)
    {
        $this
            ->setOrgID($orgID)
            ->setProfile($profile);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\UpdateProfile
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get Profile value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfile|null
     */
    public function getProfile(): ?\ID3Global\Models\GlobalProfile
    {
        return isset($this->Profile) ? $this->Profile : null;
    }
    /**
     * Set Profile value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfile $profile
     * @return \ID3Global\Models\UpdateProfile
     */
    public function setProfile(?\ID3Global\Models\GlobalProfile $profile = null): self
    {
        if (is_null($profile) || (is_array($profile) && empty($profile))) {
            unset($this->Profile);
        } else {
            $this->Profile = $profile;
        }
        
        return $this;
    }
}
