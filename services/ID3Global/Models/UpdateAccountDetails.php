<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateAccountDetails Models
 * @subpackage Structs
 */
class UpdateAccountDetails extends AbstractStructBase
{
    /**
     * The Account
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAccountDetails|null
     */
    protected ?\ID3Global\Models\GlobalAccountDetails $Account = null;
    /**
     * Constructor method for UpdateAccountDetails
     * @uses UpdateAccountDetails::setAccount()
     * @param \ID3Global\Models\GlobalAccountDetails $account
     */
    public function __construct(?\ID3Global\Models\GlobalAccountDetails $account = null)
    {
        $this
            ->setAccount($account);
    }
    /**
     * Get Account value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAccountDetails|null
     */
    public function getAccount(): ?\ID3Global\Models\GlobalAccountDetails
    {
        return isset($this->Account) ? $this->Account : null;
    }
    /**
     * Set Account value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAccountDetails $account
     * @return \ID3Global\Models\UpdateAccountDetails
     */
    public function setAccount(?\ID3Global\Models\GlobalAccountDetails $account = null): self
    {
        if (is_null($account) || (is_array($account) && empty($account))) {
            unset($this->Account);
        } else {
            $this->Account = $account;
        }
        
        return $this;
    }
}
