<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetLicenceResponse Models
 * @subpackage Structs
 */
class GetLicenceResponse extends AbstractStructBase
{
    /**
     * The GetLicenceResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalLicence|null
     */
    protected ?\ID3Global\Models\GlobalLicence $GetLicenceResult = null;
    /**
     * Constructor method for GetLicenceResponse
     * @uses GetLicenceResponse::setGetLicenceResult()
     * @param \ID3Global\Models\GlobalLicence $getLicenceResult
     */
    public function __construct(?\ID3Global\Models\GlobalLicence $getLicenceResult = null)
    {
        $this
            ->setGetLicenceResult($getLicenceResult);
    }
    /**
     * Get GetLicenceResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalLicence|null
     */
    public function getGetLicenceResult(): ?\ID3Global\Models\GlobalLicence
    {
        return isset($this->GetLicenceResult) ? $this->GetLicenceResult : null;
    }
    /**
     * Set GetLicenceResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalLicence $getLicenceResult
     * @return \ID3Global\Models\GetLicenceResponse
     */
    public function setGetLicenceResult(?\ID3Global\Models\GlobalLicence $getLicenceResult = null): self
    {
        if (is_null($getLicenceResult) || (is_array($getLicenceResult) && empty($getLicenceResult))) {
            unset($this->GetLicenceResult);
        } else {
            $this->GetLicenceResult = $getLicenceResult;
        }
        
        return $this;
    }
}
