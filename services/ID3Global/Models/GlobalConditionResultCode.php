<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalConditionResultCode Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q225:GlobalConditionResultCode
 * @subpackage Structs
 */
class GlobalConditionResultCode extends AbstractStructBase
{
    /**
     * The Code
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Code = null;
    /**
     * The TrueIfPresent
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $TrueIfPresent = null;
    /**
     * Constructor method for GlobalConditionResultCode
     * @uses GlobalConditionResultCode::setCode()
     * @uses GlobalConditionResultCode::setTrueIfPresent()
     * @param int $code
     * @param bool $trueIfPresent
     */
    public function __construct(?int $code = null, ?bool $trueIfPresent = null)
    {
        $this
            ->setCode($code)
            ->setTrueIfPresent($trueIfPresent);
    }
    /**
     * Get Code value
     * @return int|null
     */
    public function getCode(): ?int
    {
        return $this->Code;
    }
    /**
     * Set Code value
     * @param int $code
     * @return \ID3Global\Models\GlobalConditionResultCode
     */
    public function setCode(?int $code = null): self
    {
        // validation for constraint: int
        if (!is_null($code) && !(is_int($code) || ctype_digit($code))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        $this->Code = $code;
        
        return $this;
    }
    /**
     * Get TrueIfPresent value
     * @return bool|null
     */
    public function getTrueIfPresent(): ?bool
    {
        return $this->TrueIfPresent;
    }
    /**
     * Set TrueIfPresent value
     * @param bool $trueIfPresent
     * @return \ID3Global\Models\GlobalConditionResultCode
     */
    public function setTrueIfPresent(?bool $trueIfPresent = null): self
    {
        // validation for constraint: boolean
        if (!is_null($trueIfPresent) && !is_bool($trueIfPresent)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($trueIfPresent, true), gettype($trueIfPresent)), __LINE__);
        }
        $this->TrueIfPresent = $trueIfPresent;
        
        return $this;
    }
}
