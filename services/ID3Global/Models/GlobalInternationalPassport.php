<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalInternationalPassport Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q362:GlobalInternationalPassport
 * @subpackage Structs
 */
class GlobalInternationalPassport extends AbstractStructBase
{
    /**
     * The Number
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Number = null;
    /**
     * The ExpiryDay
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryDay = null;
    /**
     * The ExpiryMonth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryMonth = null;
    /**
     * The ExpiryYear
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryYear = null;
    /**
     * The CountryOfOrigin
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CountryOfOrigin = null;
    /**
     * The IssueDay
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $IssueDay = null;
    /**
     * The IssueMonth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $IssueMonth = null;
    /**
     * The IssueYear
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $IssueYear = null;
    /**
     * The ShortPassportNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ShortPassportNumber = null;
    /**
     * Constructor method for GlobalInternationalPassport
     * @uses GlobalInternationalPassport::setNumber()
     * @uses GlobalInternationalPassport::setExpiryDay()
     * @uses GlobalInternationalPassport::setExpiryMonth()
     * @uses GlobalInternationalPassport::setExpiryYear()
     * @uses GlobalInternationalPassport::setCountryOfOrigin()
     * @uses GlobalInternationalPassport::setIssueDay()
     * @uses GlobalInternationalPassport::setIssueMonth()
     * @uses GlobalInternationalPassport::setIssueYear()
     * @uses GlobalInternationalPassport::setShortPassportNumber()
     * @param string $number
     * @param int $expiryDay
     * @param int $expiryMonth
     * @param int $expiryYear
     * @param string $countryOfOrigin
     * @param int $issueDay
     * @param int $issueMonth
     * @param int $issueYear
     * @param string $shortPassportNumber
     */
    public function __construct(?string $number = null, ?int $expiryDay = null, ?int $expiryMonth = null, ?int $expiryYear = null, ?string $countryOfOrigin = null, ?int $issueDay = null, ?int $issueMonth = null, ?int $issueYear = null, ?string $shortPassportNumber = null)
    {
        $this
            ->setNumber($number)
            ->setExpiryDay($expiryDay)
            ->setExpiryMonth($expiryMonth)
            ->setExpiryYear($expiryYear)
            ->setCountryOfOrigin($countryOfOrigin)
            ->setIssueDay($issueDay)
            ->setIssueMonth($issueMonth)
            ->setIssueYear($issueYear)
            ->setShortPassportNumber($shortPassportNumber);
    }
    /**
     * Get Number value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return isset($this->Number) ? $this->Number : null;
    }
    /**
     * Set Number value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $number
     * @return \ID3Global\Models\GlobalInternationalPassport
     */
    public function setNumber(?string $number = null): self
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($number, true), gettype($number)), __LINE__);
        }
        if (is_null($number) || (is_array($number) && empty($number))) {
            unset($this->Number);
        } else {
            $this->Number = $number;
        }
        
        return $this;
    }
    /**
     * Get ExpiryDay value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryDay(): ?int
    {
        return isset($this->ExpiryDay) ? $this->ExpiryDay : null;
    }
    /**
     * Set ExpiryDay value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryDay
     * @return \ID3Global\Models\GlobalInternationalPassport
     */
    public function setExpiryDay(?int $expiryDay = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryDay) && !(is_int($expiryDay) || ctype_digit($expiryDay))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryDay, true), gettype($expiryDay)), __LINE__);
        }
        if (is_null($expiryDay) || (is_array($expiryDay) && empty($expiryDay))) {
            unset($this->ExpiryDay);
        } else {
            $this->ExpiryDay = $expiryDay;
        }
        
        return $this;
    }
    /**
     * Get ExpiryMonth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryMonth(): ?int
    {
        return isset($this->ExpiryMonth) ? $this->ExpiryMonth : null;
    }
    /**
     * Set ExpiryMonth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryMonth
     * @return \ID3Global\Models\GlobalInternationalPassport
     */
    public function setExpiryMonth(?int $expiryMonth = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryMonth) && !(is_int($expiryMonth) || ctype_digit($expiryMonth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryMonth, true), gettype($expiryMonth)), __LINE__);
        }
        if (is_null($expiryMonth) || (is_array($expiryMonth) && empty($expiryMonth))) {
            unset($this->ExpiryMonth);
        } else {
            $this->ExpiryMonth = $expiryMonth;
        }
        
        return $this;
    }
    /**
     * Get ExpiryYear value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryYear(): ?int
    {
        return isset($this->ExpiryYear) ? $this->ExpiryYear : null;
    }
    /**
     * Set ExpiryYear value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryYear
     * @return \ID3Global\Models\GlobalInternationalPassport
     */
    public function setExpiryYear(?int $expiryYear = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryYear) && !(is_int($expiryYear) || ctype_digit($expiryYear))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryYear, true), gettype($expiryYear)), __LINE__);
        }
        if (is_null($expiryYear) || (is_array($expiryYear) && empty($expiryYear))) {
            unset($this->ExpiryYear);
        } else {
            $this->ExpiryYear = $expiryYear;
        }
        
        return $this;
    }
    /**
     * Get CountryOfOrigin value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryOfOrigin(): ?string
    {
        return isset($this->CountryOfOrigin) ? $this->CountryOfOrigin : null;
    }
    /**
     * Set CountryOfOrigin value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryOfOrigin
     * @return \ID3Global\Models\GlobalInternationalPassport
     */
    public function setCountryOfOrigin(?string $countryOfOrigin = null): self
    {
        // validation for constraint: string
        if (!is_null($countryOfOrigin) && !is_string($countryOfOrigin)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryOfOrigin, true), gettype($countryOfOrigin)), __LINE__);
        }
        if (is_null($countryOfOrigin) || (is_array($countryOfOrigin) && empty($countryOfOrigin))) {
            unset($this->CountryOfOrigin);
        } else {
            $this->CountryOfOrigin = $countryOfOrigin;
        }
        
        return $this;
    }
    /**
     * Get IssueDay value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getIssueDay(): ?int
    {
        return isset($this->IssueDay) ? $this->IssueDay : null;
    }
    /**
     * Set IssueDay value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $issueDay
     * @return \ID3Global\Models\GlobalInternationalPassport
     */
    public function setIssueDay(?int $issueDay = null): self
    {
        // validation for constraint: int
        if (!is_null($issueDay) && !(is_int($issueDay) || ctype_digit($issueDay))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($issueDay, true), gettype($issueDay)), __LINE__);
        }
        if (is_null($issueDay) || (is_array($issueDay) && empty($issueDay))) {
            unset($this->IssueDay);
        } else {
            $this->IssueDay = $issueDay;
        }
        
        return $this;
    }
    /**
     * Get IssueMonth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getIssueMonth(): ?int
    {
        return isset($this->IssueMonth) ? $this->IssueMonth : null;
    }
    /**
     * Set IssueMonth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $issueMonth
     * @return \ID3Global\Models\GlobalInternationalPassport
     */
    public function setIssueMonth(?int $issueMonth = null): self
    {
        // validation for constraint: int
        if (!is_null($issueMonth) && !(is_int($issueMonth) || ctype_digit($issueMonth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($issueMonth, true), gettype($issueMonth)), __LINE__);
        }
        if (is_null($issueMonth) || (is_array($issueMonth) && empty($issueMonth))) {
            unset($this->IssueMonth);
        } else {
            $this->IssueMonth = $issueMonth;
        }
        
        return $this;
    }
    /**
     * Get IssueYear value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getIssueYear(): ?int
    {
        return isset($this->IssueYear) ? $this->IssueYear : null;
    }
    /**
     * Set IssueYear value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $issueYear
     * @return \ID3Global\Models\GlobalInternationalPassport
     */
    public function setIssueYear(?int $issueYear = null): self
    {
        // validation for constraint: int
        if (!is_null($issueYear) && !(is_int($issueYear) || ctype_digit($issueYear))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($issueYear, true), gettype($issueYear)), __LINE__);
        }
        if (is_null($issueYear) || (is_array($issueYear) && empty($issueYear))) {
            unset($this->IssueYear);
        } else {
            $this->IssueYear = $issueYear;
        }
        
        return $this;
    }
    /**
     * Get ShortPassportNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getShortPassportNumber(): ?string
    {
        return isset($this->ShortPassportNumber) ? $this->ShortPassportNumber : null;
    }
    /**
     * Set ShortPassportNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $shortPassportNumber
     * @return \ID3Global\Models\GlobalInternationalPassport
     */
    public function setShortPassportNumber(?string $shortPassportNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($shortPassportNumber) && !is_string($shortPassportNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shortPassportNumber, true), gettype($shortPassportNumber)), __LINE__);
        }
        if (is_null($shortPassportNumber) || (is_array($shortPassportNumber) && empty($shortPassportNumber))) {
            unset($this->ShortPassportNumber);
        } else {
            $this->ShortPassportNumber = $shortPassportNumber;
        }
        
        return $this;
    }
}
