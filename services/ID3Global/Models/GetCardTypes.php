<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCardTypes Models
 * @subpackage Structs
 */
class GetCardTypes extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The ProfileIDVersion
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileIDVersion|null
     */
    protected ?\ID3Global\Models\GlobalProfileIDVersion $ProfileIDVersion = null;
    /**
     * Constructor method for GetCardTypes
     * @uses GetCardTypes::setOrgID()
     * @uses GetCardTypes::setProfileIDVersion()
     * @param string $orgID
     * @param \ID3Global\Models\GlobalProfileIDVersion $profileIDVersion
     */
    public function __construct(?string $orgID = null, ?\ID3Global\Models\GlobalProfileIDVersion $profileIDVersion = null)
    {
        $this
            ->setOrgID($orgID)
            ->setProfileIDVersion($profileIDVersion);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetCardTypes
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get ProfileIDVersion value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileIDVersion|null
     */
    public function getProfileIDVersion(): ?\ID3Global\Models\GlobalProfileIDVersion
    {
        return isset($this->ProfileIDVersion) ? $this->ProfileIDVersion : null;
    }
    /**
     * Set ProfileIDVersion value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileIDVersion $profileIDVersion
     * @return \ID3Global\Models\GetCardTypes
     */
    public function setProfileIDVersion(?\ID3Global\Models\GlobalProfileIDVersion $profileIDVersion = null): self
    {
        if (is_null($profileIDVersion) || (is_array($profileIDVersion) && empty($profileIDVersion))) {
            unset($this->ProfileIDVersion);
        } else {
            $this->ProfileIDVersion = $profileIDVersion;
        }
        
        return $this;
    }
}
