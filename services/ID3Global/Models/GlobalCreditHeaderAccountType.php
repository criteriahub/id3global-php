<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCreditHeaderAccountType Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q117:GlobalCreditHeaderAccountType
 * @subpackage Structs
 */
class GlobalCreditHeaderAccountType extends AbstractStructBase
{
    /**
     * The AccountType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccountType = null;
    /**
     * The Description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Description = null;
    /**
     * The AccountCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccountCode = null;
    /**
     * Constructor method for GlobalCreditHeaderAccountType
     * @uses GlobalCreditHeaderAccountType::setAccountType()
     * @uses GlobalCreditHeaderAccountType::setDescription()
     * @uses GlobalCreditHeaderAccountType::setAccountCode()
     * @param string $accountType
     * @param string $description
     * @param string $accountCode
     */
    public function __construct(?string $accountType = null, ?string $description = null, ?string $accountCode = null)
    {
        $this
            ->setAccountType($accountType)
            ->setDescription($description)
            ->setAccountCode($accountCode);
    }
    /**
     * Get AccountType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccountType(): ?string
    {
        return isset($this->AccountType) ? $this->AccountType : null;
    }
    /**
     * Set AccountType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accountType
     * @return \ID3Global\Models\GlobalCreditHeaderAccountType
     */
    public function setAccountType(?string $accountType = null): self
    {
        // validation for constraint: string
        if (!is_null($accountType) && !is_string($accountType)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountType, true), gettype($accountType)), __LINE__);
        }
        if (is_null($accountType) || (is_array($accountType) && empty($accountType))) {
            unset($this->AccountType);
        } else {
            $this->AccountType = $accountType;
        }
        
        return $this;
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \ID3Global\Models\GlobalCreditHeaderAccountType
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        
        return $this;
    }
    /**
     * Get AccountCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccountCode(): ?string
    {
        return isset($this->AccountCode) ? $this->AccountCode : null;
    }
    /**
     * Set AccountCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accountCode
     * @return \ID3Global\Models\GlobalCreditHeaderAccountType
     */
    public function setAccountCode(?string $accountCode = null): self
    {
        // validation for constraint: string
        if (!is_null($accountCode) && !is_string($accountCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountCode, true), gettype($accountCode)), __LINE__);
        }
        if (is_null($accountCode) || (is_array($accountCode) && empty($accountCode))) {
            unset($this->AccountCode);
        } else {
            $this->AccountCode = $accountCode;
        }
        
        return $this;
    }
}
