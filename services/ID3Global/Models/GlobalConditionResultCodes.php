<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalConditionResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q222:GlobalConditionResultCodes
 * @subpackage Structs
 */
class GlobalConditionResultCodes extends AbstractStructBase
{
    /**
     * The ID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ID = null;
    /**
     * The Comment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalConditionResultCode|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $Comment = null;
    /**
     * The Match
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalConditionResultCode|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $Match = null;
    /**
     * The Mismatch
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalConditionResultCode|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $Mismatch = null;
    /**
     * The Warning
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalConditionResultCode|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $Warning = null;
    /**
     * Constructor method for GlobalConditionResultCodes
     * @uses GlobalConditionResultCodes::setID()
     * @uses GlobalConditionResultCodes::setComment()
     * @uses GlobalConditionResultCodes::setMatch()
     * @uses GlobalConditionResultCodes::setMismatch()
     * @uses GlobalConditionResultCodes::setWarning()
     * @param int $iD
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionResultCode $comment
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionResultCode $match
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionResultCode $mismatch
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionResultCode $warning
     */
    public function __construct(?int $iD = null, ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $comment = null, ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $match = null, ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $mismatch = null, ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $warning = null)
    {
        $this
            ->setID($iD)
            ->setComment($comment)
            ->setMatch($match)
            ->setMismatch($mismatch)
            ->setWarning($warning);
    }
    /**
     * Get ID value
     * @return int|null
     */
    public function getID(): ?int
    {
        return $this->ID;
    }
    /**
     * Set ID value
     * @param int $iD
     * @return \ID3Global\Models\GlobalConditionResultCodes
     */
    public function setID(?int $iD = null): self
    {
        // validation for constraint: int
        if (!is_null($iD) && !(is_int($iD) || ctype_digit($iD))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($iD, true), gettype($iD)), __LINE__);
        }
        $this->ID = $iD;
        
        return $this;
    }
    /**
     * Get Comment value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionResultCode|null
     */
    public function getComment(): ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode
    {
        return isset($this->Comment) ? $this->Comment : null;
    }
    /**
     * Set Comment value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionResultCode $comment
     * @return \ID3Global\Models\GlobalConditionResultCodes
     */
    public function setComment(?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $comment = null): self
    {
        if (is_null($comment) || (is_array($comment) && empty($comment))) {
            unset($this->Comment);
        } else {
            $this->Comment = $comment;
        }
        
        return $this;
    }
    /**
     * Get Match value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionResultCode|null
     */
    public function getMatch(): ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode
    {
        return isset($this->Match) ? $this->Match : null;
    }
    /**
     * Set Match value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionResultCode $match
     * @return \ID3Global\Models\GlobalConditionResultCodes
     */
    public function setMatch(?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $match = null): self
    {
        if (is_null($match) || (is_array($match) && empty($match))) {
            unset($this->Match);
        } else {
            $this->Match = $match;
        }
        
        return $this;
    }
    /**
     * Get Mismatch value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionResultCode|null
     */
    public function getMismatch(): ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode
    {
        return isset($this->Mismatch) ? $this->Mismatch : null;
    }
    /**
     * Set Mismatch value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionResultCode $mismatch
     * @return \ID3Global\Models\GlobalConditionResultCodes
     */
    public function setMismatch(?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $mismatch = null): self
    {
        if (is_null($mismatch) || (is_array($mismatch) && empty($mismatch))) {
            unset($this->Mismatch);
        } else {
            $this->Mismatch = $mismatch;
        }
        
        return $this;
    }
    /**
     * Get Warning value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalConditionResultCode|null
     */
    public function getWarning(): ?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode
    {
        return isset($this->Warning) ? $this->Warning : null;
    }
    /**
     * Set Warning value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalConditionResultCode $warning
     * @return \ID3Global\Models\GlobalConditionResultCodes
     */
    public function setWarning(?\ID3Global\Arrays\ArrayOfGlobalConditionResultCode $warning = null): self
    {
        if (is_null($warning) || (is_array($warning) && empty($warning))) {
            unset($this->Warning);
        } else {
            $this->Warning = $warning;
        }
        
        return $this;
    }
}
