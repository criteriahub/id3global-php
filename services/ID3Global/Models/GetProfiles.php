<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProfiles Models
 * @subpackage Structs
 */
class GetProfiles extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The IncludeTest
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IncludeTest = null;
    /**
     * The IncludePreEffective
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IncludePreEffective = null;
    /**
     * The IncludeEffective
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IncludeEffective = null;
    /**
     * The IncludeRetired
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IncludeRetired = null;
    /**
     * Constructor method for GetProfiles
     * @uses GetProfiles::setOrgID()
     * @uses GetProfiles::setIncludeTest()
     * @uses GetProfiles::setIncludePreEffective()
     * @uses GetProfiles::setIncludeEffective()
     * @uses GetProfiles::setIncludeRetired()
     * @param string $orgID
     * @param bool $includeTest
     * @param bool $includePreEffective
     * @param bool $includeEffective
     * @param bool $includeRetired
     */
    public function __construct(?string $orgID = null, ?bool $includeTest = null, ?bool $includePreEffective = null, ?bool $includeEffective = null, ?bool $includeRetired = null)
    {
        $this
            ->setOrgID($orgID)
            ->setIncludeTest($includeTest)
            ->setIncludePreEffective($includePreEffective)
            ->setIncludeEffective($includeEffective)
            ->setIncludeRetired($includeRetired);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetProfiles
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get IncludeTest value
     * @return bool|null
     */
    public function getIncludeTest(): ?bool
    {
        return $this->IncludeTest;
    }
    /**
     * Set IncludeTest value
     * @param bool $includeTest
     * @return \ID3Global\Models\GetProfiles
     */
    public function setIncludeTest(?bool $includeTest = null): self
    {
        // validation for constraint: boolean
        if (!is_null($includeTest) && !is_bool($includeTest)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($includeTest, true), gettype($includeTest)), __LINE__);
        }
        $this->IncludeTest = $includeTest;
        
        return $this;
    }
    /**
     * Get IncludePreEffective value
     * @return bool|null
     */
    public function getIncludePreEffective(): ?bool
    {
        return $this->IncludePreEffective;
    }
    /**
     * Set IncludePreEffective value
     * @param bool $includePreEffective
     * @return \ID3Global\Models\GetProfiles
     */
    public function setIncludePreEffective(?bool $includePreEffective = null): self
    {
        // validation for constraint: boolean
        if (!is_null($includePreEffective) && !is_bool($includePreEffective)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($includePreEffective, true), gettype($includePreEffective)), __LINE__);
        }
        $this->IncludePreEffective = $includePreEffective;
        
        return $this;
    }
    /**
     * Get IncludeEffective value
     * @return bool|null
     */
    public function getIncludeEffective(): ?bool
    {
        return $this->IncludeEffective;
    }
    /**
     * Set IncludeEffective value
     * @param bool $includeEffective
     * @return \ID3Global\Models\GetProfiles
     */
    public function setIncludeEffective(?bool $includeEffective = null): self
    {
        // validation for constraint: boolean
        if (!is_null($includeEffective) && !is_bool($includeEffective)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($includeEffective, true), gettype($includeEffective)), __LINE__);
        }
        $this->IncludeEffective = $includeEffective;
        
        return $this;
    }
    /**
     * Get IncludeRetired value
     * @return bool|null
     */
    public function getIncludeRetired(): ?bool
    {
        return $this->IncludeRetired;
    }
    /**
     * Set IncludeRetired value
     * @param bool $includeRetired
     * @return \ID3Global\Models\GetProfiles
     */
    public function setIncludeRetired(?bool $includeRetired = null): self
    {
        // validation for constraint: boolean
        if (!is_null($includeRetired) && !is_bool($includeRetired)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($includeRetired, true), gettype($includeRetired)), __LINE__);
        }
        $this->IncludeRetired = $includeRetired;
        
        return $this;
    }
}
