<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalFraudAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q658:GlobalFraudAccount
 * @subpackage Structs
 */
class GlobalFraudAccount extends GlobalSupplierAccount
{
    /**
     * The MembershipID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MembershipID = null;
    /**
     * The SystemUsername
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SystemUsername = null;
    /**
     * The SystemPassword
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SystemPassword = null;
    /**
     * The IntegrationUsername
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $IntegrationUsername = null;
    /**
     * The IntegrationPassword
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $IntegrationPassword = null;
    /**
     * Constructor method for GlobalFraudAccount
     * @uses GlobalFraudAccount::setMembershipID()
     * @uses GlobalFraudAccount::setSystemUsername()
     * @uses GlobalFraudAccount::setSystemPassword()
     * @uses GlobalFraudAccount::setIntegrationUsername()
     * @uses GlobalFraudAccount::setIntegrationPassword()
     * @param string $membershipID
     * @param string $systemUsername
     * @param string $systemPassword
     * @param string $integrationUsername
     * @param string $integrationPassword
     */
    public function __construct(?string $membershipID = null, ?string $systemUsername = null, ?string $systemPassword = null, ?string $integrationUsername = null, ?string $integrationPassword = null)
    {
        $this
            ->setMembershipID($membershipID)
            ->setSystemUsername($systemUsername)
            ->setSystemPassword($systemPassword)
            ->setIntegrationUsername($integrationUsername)
            ->setIntegrationPassword($integrationPassword);
    }
    /**
     * Get MembershipID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMembershipID(): ?string
    {
        return isset($this->MembershipID) ? $this->MembershipID : null;
    }
    /**
     * Set MembershipID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $membershipID
     * @return \ID3Global\Models\GlobalFraudAccount
     */
    public function setMembershipID(?string $membershipID = null): self
    {
        // validation for constraint: string
        if (!is_null($membershipID) && !is_string($membershipID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($membershipID, true), gettype($membershipID)), __LINE__);
        }
        if (is_null($membershipID) || (is_array($membershipID) && empty($membershipID))) {
            unset($this->MembershipID);
        } else {
            $this->MembershipID = $membershipID;
        }
        
        return $this;
    }
    /**
     * Get SystemUsername value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSystemUsername(): ?string
    {
        return isset($this->SystemUsername) ? $this->SystemUsername : null;
    }
    /**
     * Set SystemUsername value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $systemUsername
     * @return \ID3Global\Models\GlobalFraudAccount
     */
    public function setSystemUsername(?string $systemUsername = null): self
    {
        // validation for constraint: string
        if (!is_null($systemUsername) && !is_string($systemUsername)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($systemUsername, true), gettype($systemUsername)), __LINE__);
        }
        if (is_null($systemUsername) || (is_array($systemUsername) && empty($systemUsername))) {
            unset($this->SystemUsername);
        } else {
            $this->SystemUsername = $systemUsername;
        }
        
        return $this;
    }
    /**
     * Get SystemPassword value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSystemPassword(): ?string
    {
        return isset($this->SystemPassword) ? $this->SystemPassword : null;
    }
    /**
     * Set SystemPassword value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $systemPassword
     * @return \ID3Global\Models\GlobalFraudAccount
     */
    public function setSystemPassword(?string $systemPassword = null): self
    {
        // validation for constraint: string
        if (!is_null($systemPassword) && !is_string($systemPassword)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($systemPassword, true), gettype($systemPassword)), __LINE__);
        }
        if (is_null($systemPassword) || (is_array($systemPassword) && empty($systemPassword))) {
            unset($this->SystemPassword);
        } else {
            $this->SystemPassword = $systemPassword;
        }
        
        return $this;
    }
    /**
     * Get IntegrationUsername value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIntegrationUsername(): ?string
    {
        return isset($this->IntegrationUsername) ? $this->IntegrationUsername : null;
    }
    /**
     * Set IntegrationUsername value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $integrationUsername
     * @return \ID3Global\Models\GlobalFraudAccount
     */
    public function setIntegrationUsername(?string $integrationUsername = null): self
    {
        // validation for constraint: string
        if (!is_null($integrationUsername) && !is_string($integrationUsername)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($integrationUsername, true), gettype($integrationUsername)), __LINE__);
        }
        if (is_null($integrationUsername) || (is_array($integrationUsername) && empty($integrationUsername))) {
            unset($this->IntegrationUsername);
        } else {
            $this->IntegrationUsername = $integrationUsername;
        }
        
        return $this;
    }
    /**
     * Get IntegrationPassword value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIntegrationPassword(): ?string
    {
        return isset($this->IntegrationPassword) ? $this->IntegrationPassword : null;
    }
    /**
     * Set IntegrationPassword value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $integrationPassword
     * @return \ID3Global\Models\GlobalFraudAccount
     */
    public function setIntegrationPassword(?string $integrationPassword = null): self
    {
        // validation for constraint: string
        if (!is_null($integrationPassword) && !is_string($integrationPassword)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($integrationPassword, true), gettype($integrationPassword)), __LINE__);
        }
        if (is_null($integrationPassword) || (is_array($integrationPassword) && empty($integrationPassword))) {
            unset($this->IntegrationPassword);
        } else {
            $this->IntegrationPassword = $integrationPassword;
        }
        
        return $this;
    }
}
