<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProcessStoredImageResponse Models
 * @subpackage Structs
 */
class ProcessStoredImageResponse extends AbstractStructBase
{
    /**
     * The ProcessStoredImageResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDIVData|null
     */
    protected ?\ID3Global\Models\GlobalDIVData $ProcessStoredImageResult = null;
    /**
     * Constructor method for ProcessStoredImageResponse
     * @uses ProcessStoredImageResponse::setProcessStoredImageResult()
     * @param \ID3Global\Models\GlobalDIVData $processStoredImageResult
     */
    public function __construct(?\ID3Global\Models\GlobalDIVData $processStoredImageResult = null)
    {
        $this
            ->setProcessStoredImageResult($processStoredImageResult);
    }
    /**
     * Get ProcessStoredImageResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDIVData|null
     */
    public function getProcessStoredImageResult(): ?\ID3Global\Models\GlobalDIVData
    {
        return isset($this->ProcessStoredImageResult) ? $this->ProcessStoredImageResult : null;
    }
    /**
     * Set ProcessStoredImageResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalDIVData $processStoredImageResult
     * @return \ID3Global\Models\ProcessStoredImageResponse
     */
    public function setProcessStoredImageResult(?\ID3Global\Models\GlobalDIVData $processStoredImageResult = null): self
    {
        if (is_null($processStoredImageResult) || (is_array($processStoredImageResult) && empty($processStoredImageResult))) {
            unset($this->ProcessStoredImageResult);
        } else {
            $this->ProcessStoredImageResult = $processStoredImageResult;
        }
        
        return $this;
    }
}
