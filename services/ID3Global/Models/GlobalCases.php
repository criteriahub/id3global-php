<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCases Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q913:GlobalCases
 * @subpackage Structs
 */
class GlobalCases extends AbstractStructBase
{
    /**
     * The Cases
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCase|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCase $Cases = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The TotalPages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPages = null;
    /**
     * The TotalCases
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalCases = null;
    /**
     * The InProgress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $InProgress = null;
    /**
     * Constructor method for GlobalCases
     * @uses GlobalCases::setCases()
     * @uses GlobalCases::setPageSize()
     * @uses GlobalCases::setTotalPages()
     * @uses GlobalCases::setTotalCases()
     * @uses GlobalCases::setInProgress()
     * @param \ID3Global\Arrays\ArrayOfGlobalCase $cases
     * @param int $pageSize
     * @param int $totalPages
     * @param int $totalCases
     * @param int $inProgress
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCase $cases = null, ?int $pageSize = null, ?int $totalPages = null, ?int $totalCases = null, ?int $inProgress = null)
    {
        $this
            ->setCases($cases)
            ->setPageSize($pageSize)
            ->setTotalPages($totalPages)
            ->setTotalCases($totalCases)
            ->setInProgress($inProgress);
    }
    /**
     * Get Cases value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCase|null
     */
    public function getCases(): ?\ID3Global\Arrays\ArrayOfGlobalCase
    {
        return isset($this->Cases) ? $this->Cases : null;
    }
    /**
     * Set Cases value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCase $cases
     * @return \ID3Global\Models\GlobalCases
     */
    public function setCases(?\ID3Global\Arrays\ArrayOfGlobalCase $cases = null): self
    {
        if (is_null($cases) || (is_array($cases) && empty($cases))) {
            unset($this->Cases);
        } else {
            $this->Cases = $cases;
        }
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GlobalCases
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get TotalPages value
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->TotalPages;
    }
    /**
     * Set TotalPages value
     * @param int $totalPages
     * @return \ID3Global\Models\GlobalCases
     */
    public function setTotalPages(?int $totalPages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPages) && !(is_int($totalPages) || ctype_digit($totalPages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPages, true), gettype($totalPages)), __LINE__);
        }
        $this->TotalPages = $totalPages;
        
        return $this;
    }
    /**
     * Get TotalCases value
     * @return int|null
     */
    public function getTotalCases(): ?int
    {
        return $this->TotalCases;
    }
    /**
     * Set TotalCases value
     * @param int $totalCases
     * @return \ID3Global\Models\GlobalCases
     */
    public function setTotalCases(?int $totalCases = null): self
    {
        // validation for constraint: int
        if (!is_null($totalCases) && !(is_int($totalCases) || ctype_digit($totalCases))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalCases, true), gettype($totalCases)), __LINE__);
        }
        $this->TotalCases = $totalCases;
        
        return $this;
    }
    /**
     * Get InProgress value
     * @return int|null
     */
    public function getInProgress(): ?int
    {
        return $this->InProgress;
    }
    /**
     * Set InProgress value
     * @param int $inProgress
     * @return \ID3Global\Models\GlobalCases
     */
    public function setInProgress(?int $inProgress = null): self
    {
        // validation for constraint: int
        if (!is_null($inProgress) && !(is_int($inProgress) || ctype_digit($inProgress))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($inProgress, true), gettype($inProgress)), __LINE__);
        }
        $this->InProgress = $inProgress;
        
        return $this;
    }
}
