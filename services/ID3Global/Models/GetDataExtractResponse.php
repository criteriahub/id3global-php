<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDataExtractResponse Models
 * @subpackage Structs
 */
class GetDataExtractResponse extends AbstractStructBase
{
    /**
     * The GetDataExtractResult
     * Meta information extracted from the WSDL
     * - base: xs:base64Binary
     * @var string|null
     */
    protected ?string $GetDataExtractResult = null;
    /**
     * Constructor method for GetDataExtractResponse
     * @uses GetDataExtractResponse::setGetDataExtractResult()
     * @param string $getDataExtractResult
     */
    public function __construct(?string $getDataExtractResult = null)
    {
        $this
            ->setGetDataExtractResult($getDataExtractResult);
    }
    /**
     * Get GetDataExtractResult value
     * @return string|null
     */
    public function getGetDataExtractResult(): ?string
    {
        return $this->GetDataExtractResult;
    }
    /**
     * Set GetDataExtractResult value
     * @param string $getDataExtractResult
     * @return \ID3Global\Models\GetDataExtractResponse
     */
    public function setGetDataExtractResult(?string $getDataExtractResult = null): self
    {
        // validation for constraint: string
        if (!is_null($getDataExtractResult) && !is_string($getDataExtractResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($getDataExtractResult, true), gettype($getDataExtractResult)), __LINE__);
        }
        $this->GetDataExtractResult = $getDataExtractResult;
        
        return $this;
    }
}
