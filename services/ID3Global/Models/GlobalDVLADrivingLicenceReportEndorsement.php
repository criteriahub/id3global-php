<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDVLADrivingLicenceReportEndorsement Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q789:GlobalDVLADrivingLicenceReportEndorsement
 * @subpackage Structs
 */
class GlobalDVLADrivingLicenceReportEndorsement extends AbstractStructBase
{
    /**
     * The OffenceCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OffenceCode = null;
    /**
     * The OffenceDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OffenceDate = null;
    /**
     * The ConvictionDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ConvictionDate = null;
    /**
     * The Points
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Points = null;
    /**
     * The Expires
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Expires = null;
    /**
     * The OffenceDescription
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OffenceDescription = null;
    /**
     * Constructor method for GlobalDVLADrivingLicenceReportEndorsement
     * @uses GlobalDVLADrivingLicenceReportEndorsement::setOffenceCode()
     * @uses GlobalDVLADrivingLicenceReportEndorsement::setOffenceDate()
     * @uses GlobalDVLADrivingLicenceReportEndorsement::setConvictionDate()
     * @uses GlobalDVLADrivingLicenceReportEndorsement::setPoints()
     * @uses GlobalDVLADrivingLicenceReportEndorsement::setExpires()
     * @uses GlobalDVLADrivingLicenceReportEndorsement::setOffenceDescription()
     * @param string $offenceCode
     * @param string $offenceDate
     * @param string $convictionDate
     * @param int $points
     * @param string $expires
     * @param string $offenceDescription
     */
    public function __construct(?string $offenceCode = null, ?string $offenceDate = null, ?string $convictionDate = null, ?int $points = null, ?string $expires = null, ?string $offenceDescription = null)
    {
        $this
            ->setOffenceCode($offenceCode)
            ->setOffenceDate($offenceDate)
            ->setConvictionDate($convictionDate)
            ->setPoints($points)
            ->setExpires($expires)
            ->setOffenceDescription($offenceDescription);
    }
    /**
     * Get OffenceCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOffenceCode(): ?string
    {
        return isset($this->OffenceCode) ? $this->OffenceCode : null;
    }
    /**
     * Set OffenceCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $offenceCode
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement
     */
    public function setOffenceCode(?string $offenceCode = null): self
    {
        // validation for constraint: string
        if (!is_null($offenceCode) && !is_string($offenceCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($offenceCode, true), gettype($offenceCode)), __LINE__);
        }
        if (is_null($offenceCode) || (is_array($offenceCode) && empty($offenceCode))) {
            unset($this->OffenceCode);
        } else {
            $this->OffenceCode = $offenceCode;
        }
        
        return $this;
    }
    /**
     * Get OffenceDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOffenceDate(): ?string
    {
        return isset($this->OffenceDate) ? $this->OffenceDate : null;
    }
    /**
     * Set OffenceDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $offenceDate
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement
     */
    public function setOffenceDate(?string $offenceDate = null): self
    {
        // validation for constraint: string
        if (!is_null($offenceDate) && !is_string($offenceDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($offenceDate, true), gettype($offenceDate)), __LINE__);
        }
        if (is_null($offenceDate) || (is_array($offenceDate) && empty($offenceDate))) {
            unset($this->OffenceDate);
        } else {
            $this->OffenceDate = $offenceDate;
        }
        
        return $this;
    }
    /**
     * Get ConvictionDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getConvictionDate(): ?string
    {
        return isset($this->ConvictionDate) ? $this->ConvictionDate : null;
    }
    /**
     * Set ConvictionDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $convictionDate
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement
     */
    public function setConvictionDate(?string $convictionDate = null): self
    {
        // validation for constraint: string
        if (!is_null($convictionDate) && !is_string($convictionDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($convictionDate, true), gettype($convictionDate)), __LINE__);
        }
        if (is_null($convictionDate) || (is_array($convictionDate) && empty($convictionDate))) {
            unset($this->ConvictionDate);
        } else {
            $this->ConvictionDate = $convictionDate;
        }
        
        return $this;
    }
    /**
     * Get Points value
     * @return int|null
     */
    public function getPoints(): ?int
    {
        return $this->Points;
    }
    /**
     * Set Points value
     * @param int $points
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement
     */
    public function setPoints(?int $points = null): self
    {
        // validation for constraint: int
        if (!is_null($points) && !(is_int($points) || ctype_digit($points))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($points, true), gettype($points)), __LINE__);
        }
        $this->Points = $points;
        
        return $this;
    }
    /**
     * Get Expires value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getExpires(): ?string
    {
        return isset($this->Expires) ? $this->Expires : null;
    }
    /**
     * Set Expires value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $expires
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement
     */
    public function setExpires(?string $expires = null): self
    {
        // validation for constraint: string
        if (!is_null($expires) && !is_string($expires)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expires, true), gettype($expires)), __LINE__);
        }
        if (is_null($expires) || (is_array($expires) && empty($expires))) {
            unset($this->Expires);
        } else {
            $this->Expires = $expires;
        }
        
        return $this;
    }
    /**
     * Get OffenceDescription value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOffenceDescription(): ?string
    {
        return isset($this->OffenceDescription) ? $this->OffenceDescription : null;
    }
    /**
     * Set OffenceDescription value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $offenceDescription
     * @return \ID3Global\Models\GlobalDVLADrivingLicenceReportEndorsement
     */
    public function setOffenceDescription(?string $offenceDescription = null): self
    {
        // validation for constraint: string
        if (!is_null($offenceDescription) && !is_string($offenceDescription)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($offenceDescription, true), gettype($offenceDescription)), __LINE__);
        }
        if (is_null($offenceDescription) || (is_array($offenceDescription) && empty($offenceDescription))) {
            unset($this->OffenceDescription);
        } else {
            $this->OffenceDescription = $offenceDescription;
        }
        
        return $this;
    }
}
