<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAuthenticationDetailsResponse Models
 * @subpackage Structs
 */
class GetAuthenticationDetailsResponse extends AbstractStructBase
{
    /**
     * The GetAuthenticationDetailsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAuthenticationDetails|null
     */
    protected ?\ID3Global\Models\GlobalAuthenticationDetails $GetAuthenticationDetailsResult = null;
    /**
     * Constructor method for GetAuthenticationDetailsResponse
     * @uses GetAuthenticationDetailsResponse::setGetAuthenticationDetailsResult()
     * @param \ID3Global\Models\GlobalAuthenticationDetails $getAuthenticationDetailsResult
     */
    public function __construct(?\ID3Global\Models\GlobalAuthenticationDetails $getAuthenticationDetailsResult = null)
    {
        $this
            ->setGetAuthenticationDetailsResult($getAuthenticationDetailsResult);
    }
    /**
     * Get GetAuthenticationDetailsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAuthenticationDetails|null
     */
    public function getGetAuthenticationDetailsResult(): ?\ID3Global\Models\GlobalAuthenticationDetails
    {
        return isset($this->GetAuthenticationDetailsResult) ? $this->GetAuthenticationDetailsResult : null;
    }
    /**
     * Set GetAuthenticationDetailsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAuthenticationDetails $getAuthenticationDetailsResult
     * @return \ID3Global\Models\GetAuthenticationDetailsResponse
     */
    public function setGetAuthenticationDetailsResult(?\ID3Global\Models\GlobalAuthenticationDetails $getAuthenticationDetailsResult = null): self
    {
        if (is_null($getAuthenticationDetailsResult) || (is_array($getAuthenticationDetailsResult) && empty($getAuthenticationDetailsResult))) {
            unset($this->GetAuthenticationDetailsResult);
        } else {
            $this->GetAuthenticationDetailsResult = $getAuthenticationDetailsResult;
        }
        
        return $this;
    }
}
