<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetRolePermissionsResponse Models
 * @subpackage Structs
 */
class GetRolePermissionsResponse extends AbstractStructBase
{
    /**
     * The GetRolePermissionsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalResource|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalResource $GetRolePermissionsResult = null;
    /**
     * Constructor method for GetRolePermissionsResponse
     * @uses GetRolePermissionsResponse::setGetRolePermissionsResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalResource $getRolePermissionsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalResource $getRolePermissionsResult = null)
    {
        $this
            ->setGetRolePermissionsResult($getRolePermissionsResult);
    }
    /**
     * Get GetRolePermissionsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalResource|null
     */
    public function getGetRolePermissionsResult(): ?\ID3Global\Arrays\ArrayOfGlobalResource
    {
        return isset($this->GetRolePermissionsResult) ? $this->GetRolePermissionsResult : null;
    }
    /**
     * Set GetRolePermissionsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalResource $getRolePermissionsResult
     * @return \ID3Global\Models\GetRolePermissionsResponse
     */
    public function setGetRolePermissionsResult(?\ID3Global\Arrays\ArrayOfGlobalResource $getRolePermissionsResult = null): self
    {
        if (is_null($getRolePermissionsResult) || (is_array($getRolePermissionsResult) && empty($getRolePermissionsResult))) {
            unset($this->GetRolePermissionsResult);
        } else {
            $this->GetRolePermissionsResult = $getRolePermissionsResult;
        }
        
        return $this;
    }
}
