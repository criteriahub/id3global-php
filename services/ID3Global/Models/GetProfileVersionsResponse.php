<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProfileVersionsResponse Models
 * @subpackage Structs
 */
class GetProfileVersionsResponse extends AbstractStructBase
{
    /**
     * The GetProfileVersionsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalProfileVersion|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalProfileVersion $GetProfileVersionsResult = null;
    /**
     * Constructor method for GetProfileVersionsResponse
     * @uses GetProfileVersionsResponse::setGetProfileVersionsResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileVersion $getProfileVersionsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalProfileVersion $getProfileVersionsResult = null)
    {
        $this
            ->setGetProfileVersionsResult($getProfileVersionsResult);
    }
    /**
     * Get GetProfileVersionsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalProfileVersion|null
     */
    public function getGetProfileVersionsResult(): ?\ID3Global\Arrays\ArrayOfGlobalProfileVersion
    {
        return isset($this->GetProfileVersionsResult) ? $this->GetProfileVersionsResult : null;
    }
    /**
     * Set GetProfileVersionsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalProfileVersion $getProfileVersionsResult
     * @return \ID3Global\Models\GetProfileVersionsResponse
     */
    public function setGetProfileVersionsResult(?\ID3Global\Arrays\ArrayOfGlobalProfileVersion $getProfileVersionsResult = null): self
    {
        if (is_null($getProfileVersionsResult) || (is_array($getProfileVersionsResult) && empty($getProfileVersionsResult))) {
            unset($this->GetProfileVersionsResult);
        } else {
            $this->GetProfileVersionsResult = $getProfileVersionsResult;
        }
        
        return $this;
    }
}
