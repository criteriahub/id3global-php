<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSanctionsIssuer Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q84:GlobalSanctionsIssuer
 * @subpackage Structs
 */
class GlobalSanctionsIssuer extends AbstractStructBase
{
    /**
     * The ID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Type = null;
    /**
     * The Agency
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Agency = null;
    /**
     * The Entity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Entity = null;
    /**
     * The AgencyAcronym
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AgencyAcronym = null;
    /**
     * The Categories
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSanctionsCategories|null
     */
    protected ?\ID3Global\Models\GlobalSanctionsCategories $Categories = null;
    /**
     * The Continent
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Continent = null;
    /**
     * Constructor method for GlobalSanctionsIssuer
     * @uses GlobalSanctionsIssuer::setID()
     * @uses GlobalSanctionsIssuer::setName()
     * @uses GlobalSanctionsIssuer::setType()
     * @uses GlobalSanctionsIssuer::setAgency()
     * @uses GlobalSanctionsIssuer::setEntity()
     * @uses GlobalSanctionsIssuer::setAgencyAcronym()
     * @uses GlobalSanctionsIssuer::setCategories()
     * @uses GlobalSanctionsIssuer::setContinent()
     * @param int $iD
     * @param string $name
     * @param string $type
     * @param string $agency
     * @param string $entity
     * @param string $agencyAcronym
     * @param \ID3Global\Models\GlobalSanctionsCategories $categories
     * @param string $continent
     */
    public function __construct(?int $iD = null, ?string $name = null, ?string $type = null, ?string $agency = null, ?string $entity = null, ?string $agencyAcronym = null, ?\ID3Global\Models\GlobalSanctionsCategories $categories = null, ?string $continent = null)
    {
        $this
            ->setID($iD)
            ->setName($name)
            ->setType($type)
            ->setAgency($agency)
            ->setEntity($entity)
            ->setAgencyAcronym($agencyAcronym)
            ->setCategories($categories)
            ->setContinent($continent);
    }
    /**
     * Get ID value
     * @return int|null
     */
    public function getID(): ?int
    {
        return $this->ID;
    }
    /**
     * Set ID value
     * @param int $iD
     * @return \ID3Global\Models\GlobalSanctionsIssuer
     */
    public function setID(?int $iD = null): self
    {
        // validation for constraint: int
        if (!is_null($iD) && !(is_int($iD) || ctype_digit($iD))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($iD, true), gettype($iD)), __LINE__);
        }
        $this->ID = $iD;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalSanctionsIssuer
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Type value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getType(): ?string
    {
        return isset($this->Type) ? $this->Type : null;
    }
    /**
     * Set Type value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $type
     * @return \ID3Global\Models\GlobalSanctionsIssuer
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        if (is_null($type) || (is_array($type) && empty($type))) {
            unset($this->Type);
        } else {
            $this->Type = $type;
        }
        
        return $this;
    }
    /**
     * Get Agency value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAgency(): ?string
    {
        return isset($this->Agency) ? $this->Agency : null;
    }
    /**
     * Set Agency value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $agency
     * @return \ID3Global\Models\GlobalSanctionsIssuer
     */
    public function setAgency(?string $agency = null): self
    {
        // validation for constraint: string
        if (!is_null($agency) && !is_string($agency)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agency, true), gettype($agency)), __LINE__);
        }
        if (is_null($agency) || (is_array($agency) && empty($agency))) {
            unset($this->Agency);
        } else {
            $this->Agency = $agency;
        }
        
        return $this;
    }
    /**
     * Get Entity value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEntity(): ?string
    {
        return isset($this->Entity) ? $this->Entity : null;
    }
    /**
     * Set Entity value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $entity
     * @return \ID3Global\Models\GlobalSanctionsIssuer
     */
    public function setEntity(?string $entity = null): self
    {
        // validation for constraint: string
        if (!is_null($entity) && !is_string($entity)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($entity, true), gettype($entity)), __LINE__);
        }
        if (is_null($entity) || (is_array($entity) && empty($entity))) {
            unset($this->Entity);
        } else {
            $this->Entity = $entity;
        }
        
        return $this;
    }
    /**
     * Get AgencyAcronym value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAgencyAcronym(): ?string
    {
        return isset($this->AgencyAcronym) ? $this->AgencyAcronym : null;
    }
    /**
     * Set AgencyAcronym value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $agencyAcronym
     * @return \ID3Global\Models\GlobalSanctionsIssuer
     */
    public function setAgencyAcronym(?string $agencyAcronym = null): self
    {
        // validation for constraint: string
        if (!is_null($agencyAcronym) && !is_string($agencyAcronym)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agencyAcronym, true), gettype($agencyAcronym)), __LINE__);
        }
        if (is_null($agencyAcronym) || (is_array($agencyAcronym) && empty($agencyAcronym))) {
            unset($this->AgencyAcronym);
        } else {
            $this->AgencyAcronym = $agencyAcronym;
        }
        
        return $this;
    }
    /**
     * Get Categories value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSanctionsCategories|null
     */
    public function getCategories(): ?\ID3Global\Models\GlobalSanctionsCategories
    {
        return isset($this->Categories) ? $this->Categories : null;
    }
    /**
     * Set Categories value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSanctionsCategories $categories
     * @return \ID3Global\Models\GlobalSanctionsIssuer
     */
    public function setCategories(?\ID3Global\Models\GlobalSanctionsCategories $categories = null): self
    {
        if (is_null($categories) || (is_array($categories) && empty($categories))) {
            unset($this->Categories);
        } else {
            $this->Categories = $categories;
        }
        
        return $this;
    }
    /**
     * Get Continent value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getContinent(): ?string
    {
        return isset($this->Continent) ? $this->Continent : null;
    }
    /**
     * Set Continent value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $continent
     * @return \ID3Global\Models\GlobalSanctionsIssuer
     */
    public function setContinent(?string $continent = null): self
    {
        // validation for constraint: string
        if (!is_null($continent) && !is_string($continent)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($continent, true), gettype($continent)), __LINE__);
        }
        if (is_null($continent) || (is_array($continent) && empty($continent))) {
            unset($this->Continent);
        } else {
            $this->Continent = $continent;
        }
        
        return $this;
    }
}
