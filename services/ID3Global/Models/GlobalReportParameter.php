<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalReportParameter Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q525:GlobalReportParameter
 * @subpackage Structs
 */
class GlobalReportParameter extends AbstractStructBase
{
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Value
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Value = null;
    /**
     * The Type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Type = null;
    /**
     * The DefaultValues
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $DefaultValues = null;
    /**
     * The Prompt
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Prompt = null;
    /**
     * The ValidValues
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfKeyValueOfstringstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfKeyValueOfstringstring $ValidValues = null;
    /**
     * Constructor method for GlobalReportParameter
     * @uses GlobalReportParameter::setName()
     * @uses GlobalReportParameter::setValue()
     * @uses GlobalReportParameter::setType()
     * @uses GlobalReportParameter::setDefaultValues()
     * @uses GlobalReportParameter::setPrompt()
     * @uses GlobalReportParameter::setValidValues()
     * @param string $name
     * @param string $value
     * @param string $type
     * @param \ID3Global\Arrays\ArrayOfstring $defaultValues
     * @param string $prompt
     * @param \ID3Global\Arrays\ArrayOfKeyValueOfstringstring $validValues
     */
    public function __construct(?string $name = null, ?string $value = null, ?string $type = null, ?\ID3Global\Arrays\ArrayOfstring $defaultValues = null, ?string $prompt = null, ?\ID3Global\Arrays\ArrayOfKeyValueOfstringstring $validValues = null)
    {
        $this
            ->setName($name)
            ->setValue($value)
            ->setType($type)
            ->setDefaultValues($defaultValues)
            ->setPrompt($prompt)
            ->setValidValues($validValues);
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalReportParameter
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Value value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getValue(): ?string
    {
        return isset($this->Value) ? $this->Value : null;
    }
    /**
     * Set Value value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $value
     * @return \ID3Global\Models\GlobalReportParameter
     */
    public function setValue(?string $value = null): self
    {
        // validation for constraint: string
        if (!is_null($value) && !is_string($value)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        if (is_null($value) || (is_array($value) && empty($value))) {
            unset($this->Value);
        } else {
            $this->Value = $value;
        }
        
        return $this;
    }
    /**
     * Get Type value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getType(): ?string
    {
        return isset($this->Type) ? $this->Type : null;
    }
    /**
     * Set Type value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $type
     * @return \ID3Global\Models\GlobalReportParameter
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        if (is_null($type) || (is_array($type) && empty($type))) {
            unset($this->Type);
        } else {
            $this->Type = $type;
        }
        
        return $this;
    }
    /**
     * Get DefaultValues value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getDefaultValues(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->DefaultValues) ? $this->DefaultValues : null;
    }
    /**
     * Set DefaultValues value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $defaultValues
     * @return \ID3Global\Models\GlobalReportParameter
     */
    public function setDefaultValues(?\ID3Global\Arrays\ArrayOfstring $defaultValues = null): self
    {
        if (is_null($defaultValues) || (is_array($defaultValues) && empty($defaultValues))) {
            unset($this->DefaultValues);
        } else {
            $this->DefaultValues = $defaultValues;
        }
        
        return $this;
    }
    /**
     * Get Prompt value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPrompt(): ?string
    {
        return isset($this->Prompt) ? $this->Prompt : null;
    }
    /**
     * Set Prompt value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $prompt
     * @return \ID3Global\Models\GlobalReportParameter
     */
    public function setPrompt(?string $prompt = null): self
    {
        // validation for constraint: string
        if (!is_null($prompt) && !is_string($prompt)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($prompt, true), gettype($prompt)), __LINE__);
        }
        if (is_null($prompt) || (is_array($prompt) && empty($prompt))) {
            unset($this->Prompt);
        } else {
            $this->Prompt = $prompt;
        }
        
        return $this;
    }
    /**
     * Get ValidValues value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfKeyValueOfstringstring|null
     */
    public function getValidValues(): ?\ID3Global\Arrays\ArrayOfKeyValueOfstringstring
    {
        return isset($this->ValidValues) ? $this->ValidValues : null;
    }
    /**
     * Set ValidValues value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfKeyValueOfstringstring $validValues
     * @return \ID3Global\Models\GlobalReportParameter
     */
    public function setValidValues(?\ID3Global\Arrays\ArrayOfKeyValueOfstringstring $validValues = null): self
    {
        if (is_null($validValues) || (is_array($validValues) && empty($validValues))) {
            unset($this->ValidValues);
        } else {
            $this->ValidValues = $validValues;
        }
        
        return $this;
    }
}
