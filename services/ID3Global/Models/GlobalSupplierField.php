<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSupplierField Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q722:GlobalSupplierField
 * @subpackage Structs
 */
class GlobalSupplierField extends AbstractStructBase
{
    /**
     * The Property
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Property = null;
    /**
     * The Fieldname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Fieldname = null;
    /**
     * The Displayname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Displayname = null;
    /**
     * The Input
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Input = null;
    /**
     * Constructor method for GlobalSupplierField
     * @uses GlobalSupplierField::setProperty()
     * @uses GlobalSupplierField::setFieldname()
     * @uses GlobalSupplierField::setDisplayname()
     * @uses GlobalSupplierField::setInput()
     * @param string $property
     * @param string $fieldname
     * @param string $displayname
     * @param string $input
     */
    public function __construct(?string $property = null, ?string $fieldname = null, ?string $displayname = null, ?string $input = null)
    {
        $this
            ->setProperty($property)
            ->setFieldname($fieldname)
            ->setDisplayname($displayname)
            ->setInput($input);
    }
    /**
     * Get Property value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProperty(): ?string
    {
        return isset($this->Property) ? $this->Property : null;
    }
    /**
     * Set Property value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $property
     * @return \ID3Global\Models\GlobalSupplierField
     */
    public function setProperty(?string $property = null): self
    {
        // validation for constraint: string
        if (!is_null($property) && !is_string($property)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($property, true), gettype($property)), __LINE__);
        }
        if (is_null($property) || (is_array($property) && empty($property))) {
            unset($this->Property);
        } else {
            $this->Property = $property;
        }
        
        return $this;
    }
    /**
     * Get Fieldname value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFieldname(): ?string
    {
        return isset($this->Fieldname) ? $this->Fieldname : null;
    }
    /**
     * Set Fieldname value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fieldname
     * @return \ID3Global\Models\GlobalSupplierField
     */
    public function setFieldname(?string $fieldname = null): self
    {
        // validation for constraint: string
        if (!is_null($fieldname) && !is_string($fieldname)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fieldname, true), gettype($fieldname)), __LINE__);
        }
        if (is_null($fieldname) || (is_array($fieldname) && empty($fieldname))) {
            unset($this->Fieldname);
        } else {
            $this->Fieldname = $fieldname;
        }
        
        return $this;
    }
    /**
     * Get Displayname value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDisplayname(): ?string
    {
        return isset($this->Displayname) ? $this->Displayname : null;
    }
    /**
     * Set Displayname value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $displayname
     * @return \ID3Global\Models\GlobalSupplierField
     */
    public function setDisplayname(?string $displayname = null): self
    {
        // validation for constraint: string
        if (!is_null($displayname) && !is_string($displayname)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($displayname, true), gettype($displayname)), __LINE__);
        }
        if (is_null($displayname) || (is_array($displayname) && empty($displayname))) {
            unset($this->Displayname);
        } else {
            $this->Displayname = $displayname;
        }
        
        return $this;
    }
    /**
     * Get Input value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInput(): ?string
    {
        return isset($this->Input) ? $this->Input : null;
    }
    /**
     * Set Input value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $input
     * @return \ID3Global\Models\GlobalSupplierField
     */
    public function setInput(?string $input = null): self
    {
        // validation for constraint: string
        if (!is_null($input) && !is_string($input)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($input, true), gettype($input)), __LINE__);
        }
        if (is_null($input) || (is_array($input) && empty($input))) {
            unset($this->Input);
        } else {
            $this->Input = $input;
        }
        
        return $this;
    }
}
