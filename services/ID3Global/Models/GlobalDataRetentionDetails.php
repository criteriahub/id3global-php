<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDataRetentionDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q559:GlobalDataRetentionDetails
 * @subpackage Structs
 */
class GlobalDataRetentionDetails extends AbstractStructBase
{
    /**
     * The RetentionDays
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $RetentionDays = null;
    /**
     * The UpdatedAccountID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $UpdatedAccountID = null;
    /**
     * The UpdatedUsername
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $UpdatedUsername = null;
    /**
     * The UpdatedDomainName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $UpdatedDomainName = null;
    /**
     * The UpdatedDatetime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $UpdatedDatetime = null;
    /**
     * The EffectiveFromDateTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $EffectiveFromDateTime = null;
    /**
     * Constructor method for GlobalDataRetentionDetails
     * @uses GlobalDataRetentionDetails::setRetentionDays()
     * @uses GlobalDataRetentionDetails::setUpdatedAccountID()
     * @uses GlobalDataRetentionDetails::setUpdatedUsername()
     * @uses GlobalDataRetentionDetails::setUpdatedDomainName()
     * @uses GlobalDataRetentionDetails::setUpdatedDatetime()
     * @uses GlobalDataRetentionDetails::setEffectiveFromDateTime()
     * @param int $retentionDays
     * @param string $updatedAccountID
     * @param string $updatedUsername
     * @param string $updatedDomainName
     * @param string $updatedDatetime
     * @param string $effectiveFromDateTime
     */
    public function __construct(?int $retentionDays = null, ?string $updatedAccountID = null, ?string $updatedUsername = null, ?string $updatedDomainName = null, ?string $updatedDatetime = null, ?string $effectiveFromDateTime = null)
    {
        $this
            ->setRetentionDays($retentionDays)
            ->setUpdatedAccountID($updatedAccountID)
            ->setUpdatedUsername($updatedUsername)
            ->setUpdatedDomainName($updatedDomainName)
            ->setUpdatedDatetime($updatedDatetime)
            ->setEffectiveFromDateTime($effectiveFromDateTime);
    }
    /**
     * Get RetentionDays value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getRetentionDays(): ?int
    {
        return isset($this->RetentionDays) ? $this->RetentionDays : null;
    }
    /**
     * Set RetentionDays value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $retentionDays
     * @return \ID3Global\Models\GlobalDataRetentionDetails
     */
    public function setRetentionDays(?int $retentionDays = null): self
    {
        // validation for constraint: int
        if (!is_null($retentionDays) && !(is_int($retentionDays) || ctype_digit($retentionDays))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($retentionDays, true), gettype($retentionDays)), __LINE__);
        }
        if (is_null($retentionDays) || (is_array($retentionDays) && empty($retentionDays))) {
            unset($this->RetentionDays);
        } else {
            $this->RetentionDays = $retentionDays;
        }
        
        return $this;
    }
    /**
     * Get UpdatedAccountID value
     * @return string|null
     */
    public function getUpdatedAccountID(): ?string
    {
        return $this->UpdatedAccountID;
    }
    /**
     * Set UpdatedAccountID value
     * @param string $updatedAccountID
     * @return \ID3Global\Models\GlobalDataRetentionDetails
     */
    public function setUpdatedAccountID(?string $updatedAccountID = null): self
    {
        // validation for constraint: string
        if (!is_null($updatedAccountID) && !is_string($updatedAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updatedAccountID, true), gettype($updatedAccountID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($updatedAccountID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $updatedAccountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($updatedAccountID, true)), __LINE__);
        }
        $this->UpdatedAccountID = $updatedAccountID;
        
        return $this;
    }
    /**
     * Get UpdatedUsername value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUpdatedUsername(): ?string
    {
        return isset($this->UpdatedUsername) ? $this->UpdatedUsername : null;
    }
    /**
     * Set UpdatedUsername value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $updatedUsername
     * @return \ID3Global\Models\GlobalDataRetentionDetails
     */
    public function setUpdatedUsername(?string $updatedUsername = null): self
    {
        // validation for constraint: string
        if (!is_null($updatedUsername) && !is_string($updatedUsername)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updatedUsername, true), gettype($updatedUsername)), __LINE__);
        }
        if (is_null($updatedUsername) || (is_array($updatedUsername) && empty($updatedUsername))) {
            unset($this->UpdatedUsername);
        } else {
            $this->UpdatedUsername = $updatedUsername;
        }
        
        return $this;
    }
    /**
     * Get UpdatedDomainName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUpdatedDomainName(): ?string
    {
        return isset($this->UpdatedDomainName) ? $this->UpdatedDomainName : null;
    }
    /**
     * Set UpdatedDomainName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $updatedDomainName
     * @return \ID3Global\Models\GlobalDataRetentionDetails
     */
    public function setUpdatedDomainName(?string $updatedDomainName = null): self
    {
        // validation for constraint: string
        if (!is_null($updatedDomainName) && !is_string($updatedDomainName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updatedDomainName, true), gettype($updatedDomainName)), __LINE__);
        }
        if (is_null($updatedDomainName) || (is_array($updatedDomainName) && empty($updatedDomainName))) {
            unset($this->UpdatedDomainName);
        } else {
            $this->UpdatedDomainName = $updatedDomainName;
        }
        
        return $this;
    }
    /**
     * Get UpdatedDatetime value
     * @return string|null
     */
    public function getUpdatedDatetime(): ?string
    {
        return $this->UpdatedDatetime;
    }
    /**
     * Set UpdatedDatetime value
     * @param string $updatedDatetime
     * @return \ID3Global\Models\GlobalDataRetentionDetails
     */
    public function setUpdatedDatetime(?string $updatedDatetime = null): self
    {
        // validation for constraint: string
        if (!is_null($updatedDatetime) && !is_string($updatedDatetime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($updatedDatetime, true), gettype($updatedDatetime)), __LINE__);
        }
        $this->UpdatedDatetime = $updatedDatetime;
        
        return $this;
    }
    /**
     * Get EffectiveFromDateTime value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEffectiveFromDateTime(): ?string
    {
        return isset($this->EffectiveFromDateTime) ? $this->EffectiveFromDateTime : null;
    }
    /**
     * Set EffectiveFromDateTime value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $effectiveFromDateTime
     * @return \ID3Global\Models\GlobalDataRetentionDetails
     */
    public function setEffectiveFromDateTime(?string $effectiveFromDateTime = null): self
    {
        // validation for constraint: string
        if (!is_null($effectiveFromDateTime) && !is_string($effectiveFromDateTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($effectiveFromDateTime, true), gettype($effectiveFromDateTime)), __LINE__);
        }
        if (is_null($effectiveFromDateTime) || (is_array($effectiveFromDateTime) && empty($effectiveFromDateTime))) {
            unset($this->EffectiveFromDateTime);
        } else {
            $this->EffectiveFromDateTime = $effectiveFromDateTime;
        }
        
        return $this;
    }
}
