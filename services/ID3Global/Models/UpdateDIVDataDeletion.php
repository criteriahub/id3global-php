<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateDIVDataDeletion Models
 * @subpackage Structs
 */
class UpdateDIVDataDeletion extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The ImageID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $ImageID = null;
    /**
     * The Enable
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Enable = null;
    /**
     * Constructor method for UpdateDIVDataDeletion
     * @uses UpdateDIVDataDeletion::setOrgID()
     * @uses UpdateDIVDataDeletion::setImageID()
     * @uses UpdateDIVDataDeletion::setEnable()
     * @param string $orgID
     * @param string $imageID
     * @param bool $enable
     */
    public function __construct(?string $orgID = null, ?string $imageID = null, ?bool $enable = null)
    {
        $this
            ->setOrgID($orgID)
            ->setImageID($imageID)
            ->setEnable($enable);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\UpdateDIVDataDeletion
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get ImageID value
     * @return string|null
     */
    public function getImageID(): ?string
    {
        return $this->ImageID;
    }
    /**
     * Set ImageID value
     * @param string $imageID
     * @return \ID3Global\Models\UpdateDIVDataDeletion
     */
    public function setImageID(?string $imageID = null): self
    {
        // validation for constraint: string
        if (!is_null($imageID) && !is_string($imageID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($imageID, true), gettype($imageID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($imageID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $imageID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($imageID, true)), __LINE__);
        }
        $this->ImageID = $imageID;
        
        return $this;
    }
    /**
     * Get Enable value
     * @return bool|null
     */
    public function getEnable(): ?bool
    {
        return $this->Enable;
    }
    /**
     * Set Enable value
     * @param bool $enable
     * @return \ID3Global\Models\UpdateDIVDataDeletion
     */
    public function setEnable(?bool $enable = null): self
    {
        // validation for constraint: boolean
        if (!is_null($enable) && !is_bool($enable)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($enable, true), gettype($enable)), __LINE__);
        }
        $this->Enable = $enable;
        
        return $this;
    }
}
