<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPINSequenceResponse Models
 * @subpackage Structs
 */
class GetPINSequenceResponse extends AbstractStructBase
{
    /**
     * The GetPINSequenceResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $GetPINSequenceResult = null;
    /**
     * Constructor method for GetPINSequenceResponse
     * @uses GetPINSequenceResponse::setGetPINSequenceResult()
     * @param string $getPINSequenceResult
     */
    public function __construct(?string $getPINSequenceResult = null)
    {
        $this
            ->setGetPINSequenceResult($getPINSequenceResult);
    }
    /**
     * Get GetPINSequenceResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGetPINSequenceResult(): ?string
    {
        return isset($this->GetPINSequenceResult) ? $this->GetPINSequenceResult : null;
    }
    /**
     * Set GetPINSequenceResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $getPINSequenceResult
     * @return \ID3Global\Models\GetPINSequenceResponse
     */
    public function setGetPINSequenceResult(?string $getPINSequenceResult = null): self
    {
        // validation for constraint: string
        if (!is_null($getPINSequenceResult) && !is_string($getPINSequenceResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($getPINSequenceResult, true), gettype($getPINSequenceResult)), __LINE__);
        }
        if (is_null($getPINSequenceResult) || (is_array($getPINSequenceResult) && empty($getPINSequenceResult))) {
            unset($this->GetPINSequenceResult);
        } else {
            $this->GetPINSequenceResult = $getPINSequenceResult;
        }
        
        return $this;
    }
}
