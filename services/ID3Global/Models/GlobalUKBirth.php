<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalUKBirth Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q320:GlobalUKBirth
 * @subpackage Structs
 */
class GlobalUKBirth extends AbstractStructBase
{
    /**
     * The MothersMaidenName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MothersMaidenName = null;
    /**
     * The SurnameAtBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SurnameAtBirth = null;
    /**
     * The TownOfBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $TownOfBirth = null;
    /**
     * The ProvinceOfBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ProvinceOfBirth = null;
    /**
     * The MunicipalityOfBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MunicipalityOfBirth = null;
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Country = null;
    /**
     * Constructor method for GlobalUKBirth
     * @uses GlobalUKBirth::setMothersMaidenName()
     * @uses GlobalUKBirth::setSurnameAtBirth()
     * @uses GlobalUKBirth::setTownOfBirth()
     * @uses GlobalUKBirth::setProvinceOfBirth()
     * @uses GlobalUKBirth::setMunicipalityOfBirth()
     * @uses GlobalUKBirth::setCountry()
     * @param string $mothersMaidenName
     * @param string $surnameAtBirth
     * @param string $townOfBirth
     * @param string $provinceOfBirth
     * @param string $municipalityOfBirth
     * @param string $country
     */
    public function __construct(?string $mothersMaidenName = null, ?string $surnameAtBirth = null, ?string $townOfBirth = null, ?string $provinceOfBirth = null, ?string $municipalityOfBirth = null, ?string $country = null)
    {
        $this
            ->setMothersMaidenName($mothersMaidenName)
            ->setSurnameAtBirth($surnameAtBirth)
            ->setTownOfBirth($townOfBirth)
            ->setProvinceOfBirth($provinceOfBirth)
            ->setMunicipalityOfBirth($municipalityOfBirth)
            ->setCountry($country);
    }
    /**
     * Get MothersMaidenName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMothersMaidenName(): ?string
    {
        return isset($this->MothersMaidenName) ? $this->MothersMaidenName : null;
    }
    /**
     * Set MothersMaidenName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mothersMaidenName
     * @return \ID3Global\Models\GlobalUKBirth
     */
    public function setMothersMaidenName(?string $mothersMaidenName = null): self
    {
        // validation for constraint: string
        if (!is_null($mothersMaidenName) && !is_string($mothersMaidenName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mothersMaidenName, true), gettype($mothersMaidenName)), __LINE__);
        }
        if (is_null($mothersMaidenName) || (is_array($mothersMaidenName) && empty($mothersMaidenName))) {
            unset($this->MothersMaidenName);
        } else {
            $this->MothersMaidenName = $mothersMaidenName;
        }
        
        return $this;
    }
    /**
     * Get SurnameAtBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSurnameAtBirth(): ?string
    {
        return isset($this->SurnameAtBirth) ? $this->SurnameAtBirth : null;
    }
    /**
     * Set SurnameAtBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $surnameAtBirth
     * @return \ID3Global\Models\GlobalUKBirth
     */
    public function setSurnameAtBirth(?string $surnameAtBirth = null): self
    {
        // validation for constraint: string
        if (!is_null($surnameAtBirth) && !is_string($surnameAtBirth)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($surnameAtBirth, true), gettype($surnameAtBirth)), __LINE__);
        }
        if (is_null($surnameAtBirth) || (is_array($surnameAtBirth) && empty($surnameAtBirth))) {
            unset($this->SurnameAtBirth);
        } else {
            $this->SurnameAtBirth = $surnameAtBirth;
        }
        
        return $this;
    }
    /**
     * Get TownOfBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTownOfBirth(): ?string
    {
        return isset($this->TownOfBirth) ? $this->TownOfBirth : null;
    }
    /**
     * Set TownOfBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $townOfBirth
     * @return \ID3Global\Models\GlobalUKBirth
     */
    public function setTownOfBirth(?string $townOfBirth = null): self
    {
        // validation for constraint: string
        if (!is_null($townOfBirth) && !is_string($townOfBirth)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($townOfBirth, true), gettype($townOfBirth)), __LINE__);
        }
        if (is_null($townOfBirth) || (is_array($townOfBirth) && empty($townOfBirth))) {
            unset($this->TownOfBirth);
        } else {
            $this->TownOfBirth = $townOfBirth;
        }
        
        return $this;
    }
    /**
     * Get ProvinceOfBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProvinceOfBirth(): ?string
    {
        return isset($this->ProvinceOfBirth) ? $this->ProvinceOfBirth : null;
    }
    /**
     * Set ProvinceOfBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $provinceOfBirth
     * @return \ID3Global\Models\GlobalUKBirth
     */
    public function setProvinceOfBirth(?string $provinceOfBirth = null): self
    {
        // validation for constraint: string
        if (!is_null($provinceOfBirth) && !is_string($provinceOfBirth)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($provinceOfBirth, true), gettype($provinceOfBirth)), __LINE__);
        }
        if (is_null($provinceOfBirth) || (is_array($provinceOfBirth) && empty($provinceOfBirth))) {
            unset($this->ProvinceOfBirth);
        } else {
            $this->ProvinceOfBirth = $provinceOfBirth;
        }
        
        return $this;
    }
    /**
     * Get MunicipalityOfBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMunicipalityOfBirth(): ?string
    {
        return isset($this->MunicipalityOfBirth) ? $this->MunicipalityOfBirth : null;
    }
    /**
     * Set MunicipalityOfBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $municipalityOfBirth
     * @return \ID3Global\Models\GlobalUKBirth
     */
    public function setMunicipalityOfBirth(?string $municipalityOfBirth = null): self
    {
        // validation for constraint: string
        if (!is_null($municipalityOfBirth) && !is_string($municipalityOfBirth)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($municipalityOfBirth, true), gettype($municipalityOfBirth)), __LINE__);
        }
        if (is_null($municipalityOfBirth) || (is_array($municipalityOfBirth) && empty($municipalityOfBirth))) {
            unset($this->MunicipalityOfBirth);
        } else {
            $this->MunicipalityOfBirth = $municipalityOfBirth;
        }
        
        return $this;
    }
    /**
     * Get Country value
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->Country;
    }
    /**
     * Set Country value
     * @uses \ID3Global\Enums\GlobalUKBirthsIndexCountry::valueIsValid()
     * @uses \ID3Global\Enums\GlobalUKBirthsIndexCountry::getValidValues()
     * @throws InvalidArgumentException
     * @param string $country
     * @return \ID3Global\Models\GlobalUKBirth
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalUKBirthsIndexCountry::valueIsValid($country)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalUKBirthsIndexCountry', is_array($country) ? implode(', ', $country) : var_export($country, true), implode(', ', \ID3Global\Enums\GlobalUKBirthsIndexCountry::getValidValues())), __LINE__);
        }
        $this->Country = $country;
        
        return $this;
    }
}
