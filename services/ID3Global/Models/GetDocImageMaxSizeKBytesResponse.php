<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDocImageMaxSizeKBytesResponse Models
 * @subpackage Structs
 */
class GetDocImageMaxSizeKBytesResponse extends AbstractStructBase
{
    /**
     * The GetDocImageMaxSizeKBytesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $GetDocImageMaxSizeKBytesResult = null;
    /**
     * Constructor method for GetDocImageMaxSizeKBytesResponse
     * @uses GetDocImageMaxSizeKBytesResponse::setGetDocImageMaxSizeKBytesResult()
     * @param int $getDocImageMaxSizeKBytesResult
     */
    public function __construct(?int $getDocImageMaxSizeKBytesResult = null)
    {
        $this
            ->setGetDocImageMaxSizeKBytesResult($getDocImageMaxSizeKBytesResult);
    }
    /**
     * Get GetDocImageMaxSizeKBytesResult value
     * @return int|null
     */
    public function getGetDocImageMaxSizeKBytesResult(): ?int
    {
        return $this->GetDocImageMaxSizeKBytesResult;
    }
    /**
     * Set GetDocImageMaxSizeKBytesResult value
     * @param int $getDocImageMaxSizeKBytesResult
     * @return \ID3Global\Models\GetDocImageMaxSizeKBytesResponse
     */
    public function setGetDocImageMaxSizeKBytesResult(?int $getDocImageMaxSizeKBytesResult = null): self
    {
        // validation for constraint: int
        if (!is_null($getDocImageMaxSizeKBytesResult) && !(is_int($getDocImageMaxSizeKBytesResult) || ctype_digit($getDocImageMaxSizeKBytesResult))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($getDocImageMaxSizeKBytesResult, true), gettype($getDocImageMaxSizeKBytesResult)), __LINE__);
        }
        $this->GetDocImageMaxSizeKBytesResult = $getDocImageMaxSizeKBytesResult;
        
        return $this;
    }
}
