<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AddressDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q133:AddressDetails
 * @subpackage Structs
 */
class AddressDetails extends AbstractStructBase
{
    /**
     * The AddressLines
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $AddressLines = null;
    /**
     * The ZipPostcode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ZipPostcode = null;
    /**
     * Constructor method for AddressDetails
     * @uses AddressDetails::setAddressLines()
     * @uses AddressDetails::setZipPostcode()
     * @param \ID3Global\Arrays\ArrayOfstring $addressLines
     * @param string $zipPostcode
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfstring $addressLines = null, ?string $zipPostcode = null)
    {
        $this
            ->setAddressLines($addressLines)
            ->setZipPostcode($zipPostcode);
    }
    /**
     * Get AddressLines value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getAddressLines(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->AddressLines) ? $this->AddressLines : null;
    }
    /**
     * Set AddressLines value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $addressLines
     * @return \ID3Global\Models\AddressDetails
     */
    public function setAddressLines(?\ID3Global\Arrays\ArrayOfstring $addressLines = null): self
    {
        if (is_null($addressLines) || (is_array($addressLines) && empty($addressLines))) {
            unset($this->AddressLines);
        } else {
            $this->AddressLines = $addressLines;
        }
        
        return $this;
    }
    /**
     * Get ZipPostcode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getZipPostcode(): ?string
    {
        return isset($this->ZipPostcode) ? $this->ZipPostcode : null;
    }
    /**
     * Set ZipPostcode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $zipPostcode
     * @return \ID3Global\Models\AddressDetails
     */
    public function setZipPostcode(?string $zipPostcode = null): self
    {
        // validation for constraint: string
        if (!is_null($zipPostcode) && !is_string($zipPostcode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipPostcode, true), gettype($zipPostcode)), __LINE__);
        }
        if (is_null($zipPostcode) || (is_array($zipPostcode) && empty($zipPostcode))) {
            unset($this->ZipPostcode);
        } else {
            $this->ZipPostcode = $zipPostcode;
        }
        
        return $this;
    }
}
