<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalGermanAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q660:GlobalGermanAccount
 * @subpackage Structs
 */
class GlobalGermanAccount extends GlobalSupplierAccount
{
    /**
     * The AccessCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccessCode = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * Constructor method for GlobalGermanAccount
     * @uses GlobalGermanAccount::setAccessCode()
     * @uses GlobalGermanAccount::setPassword()
     * @param string $accessCode
     * @param string $password
     */
    public function __construct(?string $accessCode = null, ?string $password = null)
    {
        $this
            ->setAccessCode($accessCode)
            ->setPassword($password);
    }
    /**
     * Get AccessCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccessCode(): ?string
    {
        return isset($this->AccessCode) ? $this->AccessCode : null;
    }
    /**
     * Set AccessCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accessCode
     * @return \ID3Global\Models\GlobalGermanAccount
     */
    public function setAccessCode(?string $accessCode = null): self
    {
        // validation for constraint: string
        if (!is_null($accessCode) && !is_string($accessCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accessCode, true), gettype($accessCode)), __LINE__);
        }
        if (is_null($accessCode) || (is_array($accessCode) && empty($accessCode))) {
            unset($this->AccessCode);
        } else {
            $this->AccessCode = $accessCode;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\GlobalGermanAccount
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
}
