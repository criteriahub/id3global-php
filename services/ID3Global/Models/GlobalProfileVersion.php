<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalProfileVersion Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q166:GlobalProfileVersion
 * @subpackage Structs
 */
class GlobalProfileVersion extends GlobalProfile
{
    /**
     * The Version
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Version = null;
    /**
     * The Revision
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Revision = null;
    /**
     * The State
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $State = null;
    /**
     * The Start
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Start = null;
    /**
     * The End
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $End = null;
    /**
     * The AddressFormat
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $AddressFormat = null;
    /**
     * The ReAuthenticationLifetime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ReAuthenticationLifetime = null;
    /**
     * The AddressLookupEnabled
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $AddressLookupEnabled = null;
    /**
     * The SessionBlocking
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $SessionBlocking = null;
    /**
     * The DisableInputStorage
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $DisableInputStorage = null;
    /**
     * The RevisionNote
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $RevisionNote = null;
    /**
     * The Country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Country = null;
    /**
     * The VersionCreated
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $VersionCreated = null;
    /**
     * The VersionUpdated
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $VersionUpdated = null;
    /**
     * The VersionUpdatedBy
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $VersionUpdatedBy = null;
    /**
     * The DocumentImageValidation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $DocumentImageValidation = null;
    /**
     * The DocumentImageStorage
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $DocumentImageStorage = null;
    /**
     * The VersionUpdatedByAccount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $VersionUpdatedByAccount = null;
    /**
     * The IsURU
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $IsURU = null;
    /**
     * The HasVelocity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $HasVelocity = null;
    /**
     * The ScoringMethod
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ScoringMethod = null;
    /**
     * Constructor method for GlobalProfileVersion
     * @uses GlobalProfileVersion::setVersion()
     * @uses GlobalProfileVersion::setRevision()
     * @uses GlobalProfileVersion::setState()
     * @uses GlobalProfileVersion::setStart()
     * @uses GlobalProfileVersion::setEnd()
     * @uses GlobalProfileVersion::setAddressFormat()
     * @uses GlobalProfileVersion::setReAuthenticationLifetime()
     * @uses GlobalProfileVersion::setAddressLookupEnabled()
     * @uses GlobalProfileVersion::setSessionBlocking()
     * @uses GlobalProfileVersion::setDisableInputStorage()
     * @uses GlobalProfileVersion::setRevisionNote()
     * @uses GlobalProfileVersion::setCountry()
     * @uses GlobalProfileVersion::setVersionCreated()
     * @uses GlobalProfileVersion::setVersionUpdated()
     * @uses GlobalProfileVersion::setVersionUpdatedBy()
     * @uses GlobalProfileVersion::setDocumentImageValidation()
     * @uses GlobalProfileVersion::setDocumentImageStorage()
     * @uses GlobalProfileVersion::setVersionUpdatedByAccount()
     * @uses GlobalProfileVersion::setIsURU()
     * @uses GlobalProfileVersion::setHasVelocity()
     * @uses GlobalProfileVersion::setScoringMethod()
     * @param int $version
     * @param int $revision
     * @param string $state
     * @param string $start
     * @param string $end
     * @param string $addressFormat
     * @param int $reAuthenticationLifetime
     * @param bool $addressLookupEnabled
     * @param bool $sessionBlocking
     * @param bool $disableInputStorage
     * @param string $revisionNote
     * @param string $country
     * @param string $versionCreated
     * @param string $versionUpdated
     * @param string $versionUpdatedBy
     * @param bool $documentImageValidation
     * @param bool $documentImageStorage
     * @param string $versionUpdatedByAccount
     * @param string $isURU
     * @param bool $hasVelocity
     * @param string $scoringMethod
     */
    public function __construct(?int $version = null, ?int $revision = null, ?string $state = null, ?string $start = null, ?string $end = null, ?string $addressFormat = null, ?int $reAuthenticationLifetime = null, ?bool $addressLookupEnabled = null, ?bool $sessionBlocking = null, ?bool $disableInputStorage = null, ?string $revisionNote = null, ?string $country = null, ?string $versionCreated = null, ?string $versionUpdated = null, ?string $versionUpdatedBy = null, ?bool $documentImageValidation = null, ?bool $documentImageStorage = null, ?string $versionUpdatedByAccount = null, ?string $isURU = null, ?bool $hasVelocity = null, ?string $scoringMethod = null)
    {
        $this
            ->setVersion($version)
            ->setRevision($revision)
            ->setState($state)
            ->setStart($start)
            ->setEnd($end)
            ->setAddressFormat($addressFormat)
            ->setReAuthenticationLifetime($reAuthenticationLifetime)
            ->setAddressLookupEnabled($addressLookupEnabled)
            ->setSessionBlocking($sessionBlocking)
            ->setDisableInputStorage($disableInputStorage)
            ->setRevisionNote($revisionNote)
            ->setCountry($country)
            ->setVersionCreated($versionCreated)
            ->setVersionUpdated($versionUpdated)
            ->setVersionUpdatedBy($versionUpdatedBy)
            ->setDocumentImageValidation($documentImageValidation)
            ->setDocumentImageStorage($documentImageStorage)
            ->setVersionUpdatedByAccount($versionUpdatedByAccount)
            ->setIsURU($isURU)
            ->setHasVelocity($hasVelocity)
            ->setScoringMethod($scoringMethod);
    }
    /**
     * Get Version value
     * @return int|null
     */
    public function getVersion(): ?int
    {
        return $this->Version;
    }
    /**
     * Set Version value
     * @param int $version
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setVersion(?int $version = null): self
    {
        // validation for constraint: int
        if (!is_null($version) && !(is_int($version) || ctype_digit($version))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($version, true), gettype($version)), __LINE__);
        }
        $this->Version = $version;
        
        return $this;
    }
    /**
     * Get Revision value
     * @return int|null
     */
    public function getRevision(): ?int
    {
        return $this->Revision;
    }
    /**
     * Set Revision value
     * @param int $revision
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setRevision(?int $revision = null): self
    {
        // validation for constraint: int
        if (!is_null($revision) && !(is_int($revision) || ctype_digit($revision))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($revision, true), gettype($revision)), __LINE__);
        }
        $this->Revision = $revision;
        
        return $this;
    }
    /**
     * Get State value
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->State;
    }
    /**
     * Set State value
     * @uses \ID3Global\Enums\GlobalProfileState::valueIsValid()
     * @uses \ID3Global\Enums\GlobalProfileState::getValidValues()
     * @throws InvalidArgumentException
     * @param string $state
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setState(?string $state = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalProfileState::valueIsValid($state)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalProfileState', is_array($state) ? implode(', ', $state) : var_export($state, true), implode(', ', \ID3Global\Enums\GlobalProfileState::getValidValues())), __LINE__);
        }
        $this->State = $state;
        
        return $this;
    }
    /**
     * Get Start value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStart(): ?string
    {
        return isset($this->Start) ? $this->Start : null;
    }
    /**
     * Set Start value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $start
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setStart(?string $start = null): self
    {
        // validation for constraint: string
        if (!is_null($start) && !is_string($start)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($start, true), gettype($start)), __LINE__);
        }
        if (is_null($start) || (is_array($start) && empty($start))) {
            unset($this->Start);
        } else {
            $this->Start = $start;
        }
        
        return $this;
    }
    /**
     * Get End value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEnd(): ?string
    {
        return isset($this->End) ? $this->End : null;
    }
    /**
     * Set End value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $end
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setEnd(?string $end = null): self
    {
        // validation for constraint: string
        if (!is_null($end) && !is_string($end)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($end, true), gettype($end)), __LINE__);
        }
        if (is_null($end) || (is_array($end) && empty($end))) {
            unset($this->End);
        } else {
            $this->End = $end;
        }
        
        return $this;
    }
    /**
     * Get AddressFormat value
     * @return string|null
     */
    public function getAddressFormat(): ?string
    {
        return $this->AddressFormat;
    }
    /**
     * Set AddressFormat value
     * @uses \ID3Global\Enums\GlobalAddressFormat::valueIsValid()
     * @uses \ID3Global\Enums\GlobalAddressFormat::getValidValues()
     * @throws InvalidArgumentException
     * @param string $addressFormat
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setAddressFormat(?string $addressFormat = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalAddressFormat::valueIsValid($addressFormat)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalAddressFormat', is_array($addressFormat) ? implode(', ', $addressFormat) : var_export($addressFormat, true), implode(', ', \ID3Global\Enums\GlobalAddressFormat::getValidValues())), __LINE__);
        }
        $this->AddressFormat = $addressFormat;
        
        return $this;
    }
    /**
     * Get ReAuthenticationLifetime value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getReAuthenticationLifetime(): ?int
    {
        return isset($this->ReAuthenticationLifetime) ? $this->ReAuthenticationLifetime : null;
    }
    /**
     * Set ReAuthenticationLifetime value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $reAuthenticationLifetime
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setReAuthenticationLifetime(?int $reAuthenticationLifetime = null): self
    {
        // validation for constraint: int
        if (!is_null($reAuthenticationLifetime) && !(is_int($reAuthenticationLifetime) || ctype_digit($reAuthenticationLifetime))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($reAuthenticationLifetime, true), gettype($reAuthenticationLifetime)), __LINE__);
        }
        if (is_null($reAuthenticationLifetime) || (is_array($reAuthenticationLifetime) && empty($reAuthenticationLifetime))) {
            unset($this->ReAuthenticationLifetime);
        } else {
            $this->ReAuthenticationLifetime = $reAuthenticationLifetime;
        }
        
        return $this;
    }
    /**
     * Get AddressLookupEnabled value
     * @return bool|null
     */
    public function getAddressLookupEnabled(): ?bool
    {
        return $this->AddressLookupEnabled;
    }
    /**
     * Set AddressLookupEnabled value
     * @param bool $addressLookupEnabled
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setAddressLookupEnabled(?bool $addressLookupEnabled = null): self
    {
        // validation for constraint: boolean
        if (!is_null($addressLookupEnabled) && !is_bool($addressLookupEnabled)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($addressLookupEnabled, true), gettype($addressLookupEnabled)), __LINE__);
        }
        $this->AddressLookupEnabled = $addressLookupEnabled;
        
        return $this;
    }
    /**
     * Get SessionBlocking value
     * @return bool|null
     */
    public function getSessionBlocking(): ?bool
    {
        return $this->SessionBlocking;
    }
    /**
     * Set SessionBlocking value
     * @param bool $sessionBlocking
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setSessionBlocking(?bool $sessionBlocking = null): self
    {
        // validation for constraint: boolean
        if (!is_null($sessionBlocking) && !is_bool($sessionBlocking)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($sessionBlocking, true), gettype($sessionBlocking)), __LINE__);
        }
        $this->SessionBlocking = $sessionBlocking;
        
        return $this;
    }
    /**
     * Get DisableInputStorage value
     * @return bool|null
     */
    public function getDisableInputStorage(): ?bool
    {
        return $this->DisableInputStorage;
    }
    /**
     * Set DisableInputStorage value
     * @param bool $disableInputStorage
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setDisableInputStorage(?bool $disableInputStorage = null): self
    {
        // validation for constraint: boolean
        if (!is_null($disableInputStorage) && !is_bool($disableInputStorage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($disableInputStorage, true), gettype($disableInputStorage)), __LINE__);
        }
        $this->DisableInputStorage = $disableInputStorage;
        
        return $this;
    }
    /**
     * Get RevisionNote value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRevisionNote(): ?string
    {
        return isset($this->RevisionNote) ? $this->RevisionNote : null;
    }
    /**
     * Set RevisionNote value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $revisionNote
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setRevisionNote(?string $revisionNote = null): self
    {
        // validation for constraint: string
        if (!is_null($revisionNote) && !is_string($revisionNote)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($revisionNote, true), gettype($revisionNote)), __LINE__);
        }
        if (is_null($revisionNote) || (is_array($revisionNote) && empty($revisionNote))) {
            unset($this->RevisionNote);
        } else {
            $this->RevisionNote = $revisionNote;
        }
        
        return $this;
    }
    /**
     * Get Country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return isset($this->Country) ? $this->Country : null;
    }
    /**
     * Set Country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $country
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->Country);
        } else {
            $this->Country = $country;
        }
        
        return $this;
    }
    /**
     * Get VersionCreated value
     * @return string|null
     */
    public function getVersionCreated(): ?string
    {
        return $this->VersionCreated;
    }
    /**
     * Set VersionCreated value
     * @param string $versionCreated
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setVersionCreated(?string $versionCreated = null): self
    {
        // validation for constraint: string
        if (!is_null($versionCreated) && !is_string($versionCreated)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($versionCreated, true), gettype($versionCreated)), __LINE__);
        }
        $this->VersionCreated = $versionCreated;
        
        return $this;
    }
    /**
     * Get VersionUpdated value
     * @return string|null
     */
    public function getVersionUpdated(): ?string
    {
        return $this->VersionUpdated;
    }
    /**
     * Set VersionUpdated value
     * @param string $versionUpdated
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setVersionUpdated(?string $versionUpdated = null): self
    {
        // validation for constraint: string
        if (!is_null($versionUpdated) && !is_string($versionUpdated)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($versionUpdated, true), gettype($versionUpdated)), __LINE__);
        }
        $this->VersionUpdated = $versionUpdated;
        
        return $this;
    }
    /**
     * Get VersionUpdatedBy value
     * @return string|null
     */
    public function getVersionUpdatedBy(): ?string
    {
        return $this->VersionUpdatedBy;
    }
    /**
     * Set VersionUpdatedBy value
     * @param string $versionUpdatedBy
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setVersionUpdatedBy(?string $versionUpdatedBy = null): self
    {
        // validation for constraint: string
        if (!is_null($versionUpdatedBy) && !is_string($versionUpdatedBy)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($versionUpdatedBy, true), gettype($versionUpdatedBy)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($versionUpdatedBy) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $versionUpdatedBy)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($versionUpdatedBy, true)), __LINE__);
        }
        $this->VersionUpdatedBy = $versionUpdatedBy;
        
        return $this;
    }
    /**
     * Get DocumentImageValidation value
     * @return bool|null
     */
    public function getDocumentImageValidation(): ?bool
    {
        return $this->DocumentImageValidation;
    }
    /**
     * Set DocumentImageValidation value
     * @param bool $documentImageValidation
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setDocumentImageValidation(?bool $documentImageValidation = null): self
    {
        // validation for constraint: boolean
        if (!is_null($documentImageValidation) && !is_bool($documentImageValidation)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($documentImageValidation, true), gettype($documentImageValidation)), __LINE__);
        }
        $this->DocumentImageValidation = $documentImageValidation;
        
        return $this;
    }
    /**
     * Get DocumentImageStorage value
     * @return bool|null
     */
    public function getDocumentImageStorage(): ?bool
    {
        return $this->DocumentImageStorage;
    }
    /**
     * Set DocumentImageStorage value
     * @param bool $documentImageStorage
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setDocumentImageStorage(?bool $documentImageStorage = null): self
    {
        // validation for constraint: boolean
        if (!is_null($documentImageStorage) && !is_bool($documentImageStorage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($documentImageStorage, true), gettype($documentImageStorage)), __LINE__);
        }
        $this->DocumentImageStorage = $documentImageStorage;
        
        return $this;
    }
    /**
     * Get VersionUpdatedByAccount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getVersionUpdatedByAccount(): ?string
    {
        return isset($this->VersionUpdatedByAccount) ? $this->VersionUpdatedByAccount : null;
    }
    /**
     * Set VersionUpdatedByAccount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $versionUpdatedByAccount
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setVersionUpdatedByAccount(?string $versionUpdatedByAccount = null): self
    {
        // validation for constraint: string
        if (!is_null($versionUpdatedByAccount) && !is_string($versionUpdatedByAccount)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($versionUpdatedByAccount, true), gettype($versionUpdatedByAccount)), __LINE__);
        }
        if (is_null($versionUpdatedByAccount) || (is_array($versionUpdatedByAccount) && empty($versionUpdatedByAccount))) {
            unset($this->VersionUpdatedByAccount);
        } else {
            $this->VersionUpdatedByAccount = $versionUpdatedByAccount;
        }
        
        return $this;
    }
    /**
     * Get IsURU value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getIsURU(): ?string
    {
        return isset($this->IsURU) ? $this->IsURU : null;
    }
    /**
     * Set IsURU value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $isURU
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setIsURU(?string $isURU = null): self
    {
        // validation for constraint: string
        if (!is_null($isURU) && !is_string($isURU)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($isURU, true), gettype($isURU)), __LINE__);
        }
        if (is_null($isURU) || (is_array($isURU) && empty($isURU))) {
            unset($this->IsURU);
        } else {
            $this->IsURU = $isURU;
        }
        
        return $this;
    }
    /**
     * Get HasVelocity value
     * @return bool|null
     */
    public function getHasVelocity(): ?bool
    {
        return $this->HasVelocity;
    }
    /**
     * Set HasVelocity value
     * @param bool $hasVelocity
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setHasVelocity(?bool $hasVelocity = null): self
    {
        // validation for constraint: boolean
        if (!is_null($hasVelocity) && !is_bool($hasVelocity)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($hasVelocity, true), gettype($hasVelocity)), __LINE__);
        }
        $this->HasVelocity = $hasVelocity;
        
        return $this;
    }
    /**
     * Get ScoringMethod value
     * @return string|null
     */
    public function getScoringMethod(): ?string
    {
        return $this->ScoringMethod;
    }
    /**
     * Set ScoringMethod value
     * @uses \ID3Global\Enums\GlobalScoringMethod::valueIsValid()
     * @uses \ID3Global\Enums\GlobalScoringMethod::getValidValues()
     * @throws InvalidArgumentException
     * @param string $scoringMethod
     * @return \ID3Global\Models\GlobalProfileVersion
     */
    public function setScoringMethod(?string $scoringMethod = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalScoringMethod::valueIsValid($scoringMethod)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalScoringMethod', is_array($scoringMethod) ? implode(', ', $scoringMethod) : var_export($scoringMethod, true), implode(', ', \ID3Global\Enums\GlobalScoringMethod::getValidValues())), __LINE__);
        }
        $this->ScoringMethod = $scoringMethod;
        
        return $this;
    }
}
