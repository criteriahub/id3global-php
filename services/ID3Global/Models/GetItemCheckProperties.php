<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetItemCheckProperties Models
 * @subpackage Structs
 */
class GetItemCheckProperties extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The ItemCheckID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ItemCheckID = null;
    /**
     * Constructor method for GetItemCheckProperties
     * @uses GetItemCheckProperties::setOrgID()
     * @uses GetItemCheckProperties::setItemCheckID()
     * @param string $orgID
     * @param int $itemCheckID
     */
    public function __construct(?string $orgID = null, ?int $itemCheckID = null)
    {
        $this
            ->setOrgID($orgID)
            ->setItemCheckID($itemCheckID);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetItemCheckProperties
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get ItemCheckID value
     * @return int|null
     */
    public function getItemCheckID(): ?int
    {
        return $this->ItemCheckID;
    }
    /**
     * Set ItemCheckID value
     * @param int $itemCheckID
     * @return \ID3Global\Models\GetItemCheckProperties
     */
    public function setItemCheckID(?int $itemCheckID = null): self
    {
        // validation for constraint: int
        if (!is_null($itemCheckID) && !(is_int($itemCheckID) || ctype_digit($itemCheckID))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($itemCheckID, true), gettype($itemCheckID)), __LINE__);
        }
        $this->ItemCheckID = $itemCheckID;
        
        return $this;
    }
}
