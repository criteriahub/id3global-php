<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCardNameResponse Models
 * @subpackage Structs
 */
class GetCardNameResponse extends AbstractStructBase
{
    /**
     * The GetCardNameResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $GetCardNameResult = null;
    /**
     * Constructor method for GetCardNameResponse
     * @uses GetCardNameResponse::setGetCardNameResult()
     * @param string $getCardNameResult
     */
    public function __construct(?string $getCardNameResult = null)
    {
        $this
            ->setGetCardNameResult($getCardNameResult);
    }
    /**
     * Get GetCardNameResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGetCardNameResult(): ?string
    {
        return isset($this->GetCardNameResult) ? $this->GetCardNameResult : null;
    }
    /**
     * Set GetCardNameResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $getCardNameResult
     * @return \ID3Global\Models\GetCardNameResponse
     */
    public function setGetCardNameResult(?string $getCardNameResult = null): self
    {
        // validation for constraint: string
        if (!is_null($getCardNameResult) && !is_string($getCardNameResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($getCardNameResult, true), gettype($getCardNameResult)), __LINE__);
        }
        if (is_null($getCardNameResult) || (is_array($getCardNameResult) && empty($getCardNameResult))) {
            unset($this->GetCardNameResult);
        } else {
            $this->GetCardNameResult = $getCardNameResult;
        }
        
        return $this;
    }
}
