<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalRolePermission Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q612:GlobalRolePermission
 * @subpackage Structs
 */
class GlobalRolePermission extends AbstractStructBase
{
    /**
     * The Role
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalRole|null
     */
    protected ?\ID3Global\Models\GlobalRole $Role = null;
    /**
     * The Permission
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Permission = null;
    /**
     * The System
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $System = null;
    /**
     * Constructor method for GlobalRolePermission
     * @uses GlobalRolePermission::setRole()
     * @uses GlobalRolePermission::setPermission()
     * @uses GlobalRolePermission::setSystem()
     * @param \ID3Global\Models\GlobalRole $role
     * @param string $permission
     * @param bool $system
     */
    public function __construct(?\ID3Global\Models\GlobalRole $role = null, ?string $permission = null, ?bool $system = null)
    {
        $this
            ->setRole($role)
            ->setPermission($permission)
            ->setSystem($system);
    }
    /**
     * Get Role value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalRole|null
     */
    public function getRole(): ?\ID3Global\Models\GlobalRole
    {
        return isset($this->Role) ? $this->Role : null;
    }
    /**
     * Set Role value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalRole $role
     * @return \ID3Global\Models\GlobalRolePermission
     */
    public function setRole(?\ID3Global\Models\GlobalRole $role = null): self
    {
        if (is_null($role) || (is_array($role) && empty($role))) {
            unset($this->Role);
        } else {
            $this->Role = $role;
        }
        
        return $this;
    }
    /**
     * Get Permission value
     * @return string|null
     */
    public function getPermission(): ?string
    {
        return $this->Permission;
    }
    /**
     * Set Permission value
     * @uses \ID3Global\Enums\GlobalPermission::valueIsValid()
     * @uses \ID3Global\Enums\GlobalPermission::getValidValues()
     * @throws InvalidArgumentException
     * @param string $permission
     * @return \ID3Global\Models\GlobalRolePermission
     */
    public function setPermission(?string $permission = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalPermission::valueIsValid($permission)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalPermission', is_array($permission) ? implode(', ', $permission) : var_export($permission, true), implode(', ', \ID3Global\Enums\GlobalPermission::getValidValues())), __LINE__);
        }
        $this->Permission = $permission;
        
        return $this;
    }
    /**
     * Get System value
     * @return bool|null
     */
    public function getSystem(): ?bool
    {
        return $this->System;
    }
    /**
     * Set System value
     * @param bool $system
     * @return \ID3Global\Models\GlobalRolePermission
     */
    public function setSystem(?bool $system = null): self
    {
        // validation for constraint: boolean
        if (!is_null($system) && !is_bool($system)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($system, true), gettype($system)), __LINE__);
        }
        $this->System = $system;
        
        return $this;
    }
}
