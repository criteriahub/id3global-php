<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetOrganisationDetailsResponse Models
 * @subpackage Structs
 */
class GetOrganisationDetailsResponse extends AbstractStructBase
{
    /**
     * The GetOrganisationDetailsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalOrganisationDetails|null
     */
    protected ?\ID3Global\Models\GlobalOrganisationDetails $GetOrganisationDetailsResult = null;
    /**
     * Constructor method for GetOrganisationDetailsResponse
     * @uses GetOrganisationDetailsResponse::setGetOrganisationDetailsResult()
     * @param \ID3Global\Models\GlobalOrganisationDetails $getOrganisationDetailsResult
     */
    public function __construct(?\ID3Global\Models\GlobalOrganisationDetails $getOrganisationDetailsResult = null)
    {
        $this
            ->setGetOrganisationDetailsResult($getOrganisationDetailsResult);
    }
    /**
     * Get GetOrganisationDetailsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalOrganisationDetails|null
     */
    public function getGetOrganisationDetailsResult(): ?\ID3Global\Models\GlobalOrganisationDetails
    {
        return isset($this->GetOrganisationDetailsResult) ? $this->GetOrganisationDetailsResult : null;
    }
    /**
     * Set GetOrganisationDetailsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalOrganisationDetails $getOrganisationDetailsResult
     * @return \ID3Global\Models\GetOrganisationDetailsResponse
     */
    public function setGetOrganisationDetailsResult(?\ID3Global\Models\GlobalOrganisationDetails $getOrganisationDetailsResult = null): self
    {
        if (is_null($getOrganisationDetailsResult) || (is_array($getOrganisationDetailsResult) && empty($getOrganisationDetailsResult))) {
            unset($this->GetOrganisationDetailsResult);
        } else {
            $this->GetOrganisationDetailsResult = $getOrganisationDetailsResult;
        }
        
        return $this;
    }
}
