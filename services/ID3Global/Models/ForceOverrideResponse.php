<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ForceOverrideResponse Models
 * @subpackage Structs
 */
class ForceOverrideResponse extends AbstractStructBase
{
    /**
     * The ForceOverrideResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalResultData|null
     */
    protected ?\ID3Global\Models\GlobalResultData $ForceOverrideResult = null;
    /**
     * Constructor method for ForceOverrideResponse
     * @uses ForceOverrideResponse::setForceOverrideResult()
     * @param \ID3Global\Models\GlobalResultData $forceOverrideResult
     */
    public function __construct(?\ID3Global\Models\GlobalResultData $forceOverrideResult = null)
    {
        $this
            ->setForceOverrideResult($forceOverrideResult);
    }
    /**
     * Get ForceOverrideResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalResultData|null
     */
    public function getForceOverrideResult(): ?\ID3Global\Models\GlobalResultData
    {
        return isset($this->ForceOverrideResult) ? $this->ForceOverrideResult : null;
    }
    /**
     * Set ForceOverrideResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalResultData $forceOverrideResult
     * @return \ID3Global\Models\ForceOverrideResponse
     */
    public function setForceOverrideResult(?\ID3Global\Models\GlobalResultData $forceOverrideResult = null): self
    {
        if (is_null($forceOverrideResult) || (is_array($forceOverrideResult) && empty($forceOverrideResult))) {
            unset($this->ForceOverrideResult);
        } else {
            $this->ForceOverrideResult = $forceOverrideResult;
        }
        
        return $this;
    }
}
