<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalRoleMembers Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q600:GlobalRoleMembers
 * @subpackage Structs
 */
class GlobalRoleMembers extends AbstractStructBase
{
    /**
     * The Members
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalRoleMember|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalRoleMember $Members = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The TotalPages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPages = null;
    /**
     * The TotalAccounts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalAccounts = null;
    /**
     * The TotalMembers
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalMembers = null;
    /**
     * The TotalNotMembers
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalNotMembers = null;
    /**
     * Constructor method for GlobalRoleMembers
     * @uses GlobalRoleMembers::setMembers()
     * @uses GlobalRoleMembers::setPageSize()
     * @uses GlobalRoleMembers::setTotalPages()
     * @uses GlobalRoleMembers::setTotalAccounts()
     * @uses GlobalRoleMembers::setTotalMembers()
     * @uses GlobalRoleMembers::setTotalNotMembers()
     * @param \ID3Global\Arrays\ArrayOfGlobalRoleMember $members
     * @param int $pageSize
     * @param int $totalPages
     * @param int $totalAccounts
     * @param int $totalMembers
     * @param int $totalNotMembers
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalRoleMember $members = null, ?int $pageSize = null, ?int $totalPages = null, ?int $totalAccounts = null, ?int $totalMembers = null, ?int $totalNotMembers = null)
    {
        $this
            ->setMembers($members)
            ->setPageSize($pageSize)
            ->setTotalPages($totalPages)
            ->setTotalAccounts($totalAccounts)
            ->setTotalMembers($totalMembers)
            ->setTotalNotMembers($totalNotMembers);
    }
    /**
     * Get Members value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalRoleMember|null
     */
    public function getMembers(): ?\ID3Global\Arrays\ArrayOfGlobalRoleMember
    {
        return isset($this->Members) ? $this->Members : null;
    }
    /**
     * Set Members value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalRoleMember $members
     * @return \ID3Global\Models\GlobalRoleMembers
     */
    public function setMembers(?\ID3Global\Arrays\ArrayOfGlobalRoleMember $members = null): self
    {
        if (is_null($members) || (is_array($members) && empty($members))) {
            unset($this->Members);
        } else {
            $this->Members = $members;
        }
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GlobalRoleMembers
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get TotalPages value
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->TotalPages;
    }
    /**
     * Set TotalPages value
     * @param int $totalPages
     * @return \ID3Global\Models\GlobalRoleMembers
     */
    public function setTotalPages(?int $totalPages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPages) && !(is_int($totalPages) || ctype_digit($totalPages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPages, true), gettype($totalPages)), __LINE__);
        }
        $this->TotalPages = $totalPages;
        
        return $this;
    }
    /**
     * Get TotalAccounts value
     * @return int|null
     */
    public function getTotalAccounts(): ?int
    {
        return $this->TotalAccounts;
    }
    /**
     * Set TotalAccounts value
     * @param int $totalAccounts
     * @return \ID3Global\Models\GlobalRoleMembers
     */
    public function setTotalAccounts(?int $totalAccounts = null): self
    {
        // validation for constraint: int
        if (!is_null($totalAccounts) && !(is_int($totalAccounts) || ctype_digit($totalAccounts))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalAccounts, true), gettype($totalAccounts)), __LINE__);
        }
        $this->TotalAccounts = $totalAccounts;
        
        return $this;
    }
    /**
     * Get TotalMembers value
     * @return int|null
     */
    public function getTotalMembers(): ?int
    {
        return $this->TotalMembers;
    }
    /**
     * Set TotalMembers value
     * @param int $totalMembers
     * @return \ID3Global\Models\GlobalRoleMembers
     */
    public function setTotalMembers(?int $totalMembers = null): self
    {
        // validation for constraint: int
        if (!is_null($totalMembers) && !(is_int($totalMembers) || ctype_digit($totalMembers))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalMembers, true), gettype($totalMembers)), __LINE__);
        }
        $this->TotalMembers = $totalMembers;
        
        return $this;
    }
    /**
     * Get TotalNotMembers value
     * @return int|null
     */
    public function getTotalNotMembers(): ?int
    {
        return $this->TotalNotMembers;
    }
    /**
     * Set TotalNotMembers value
     * @param int $totalNotMembers
     * @return \ID3Global\Models\GlobalRoleMembers
     */
    public function setTotalNotMembers(?int $totalNotMembers = null): self
    {
        // validation for constraint: int
        if (!is_null($totalNotMembers) && !(is_int($totalNotMembers) || ctype_digit($totalNotMembers))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalNotMembers, true), gettype($totalNotMembers)), __LINE__);
        }
        $this->TotalNotMembers = $totalNotMembers;
        
        return $this;
    }
}
