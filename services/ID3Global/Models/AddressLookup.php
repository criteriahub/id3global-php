<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AddressLookup Models
 * @subpackage Structs
 */
class AddressLookup extends AbstractStructBase
{
    /**
     * The InputData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAddress|null
     */
    protected ?\ID3Global\Models\GlobalAddress $InputData = null;
    /**
     * Constructor method for AddressLookup
     * @uses AddressLookup::setInputData()
     * @param \ID3Global\Models\GlobalAddress $inputData
     */
    public function __construct(?\ID3Global\Models\GlobalAddress $inputData = null)
    {
        $this
            ->setInputData($inputData);
    }
    /**
     * Get InputData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAddress|null
     */
    public function getInputData(): ?\ID3Global\Models\GlobalAddress
    {
        return isset($this->InputData) ? $this->InputData : null;
    }
    /**
     * Set InputData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAddress $inputData
     * @return \ID3Global\Models\AddressLookup
     */
    public function setInputData(?\ID3Global\Models\GlobalAddress $inputData = null): self
    {
        if (is_null($inputData) || (is_array($inputData) && empty($inputData))) {
            unset($this->InputData);
        } else {
            $this->InputData = $inputData;
        }
        
        return $this;
    }
}
