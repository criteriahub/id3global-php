<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDocImageMinWidthResolutionResponse Models
 * @subpackage Structs
 */
class GetDocImageMinWidthResolutionResponse extends AbstractStructBase
{
    /**
     * The GetDocImageMinWidthResolutionResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $GetDocImageMinWidthResolutionResult = null;
    /**
     * Constructor method for GetDocImageMinWidthResolutionResponse
     * @uses GetDocImageMinWidthResolutionResponse::setGetDocImageMinWidthResolutionResult()
     * @param int $getDocImageMinWidthResolutionResult
     */
    public function __construct(?int $getDocImageMinWidthResolutionResult = null)
    {
        $this
            ->setGetDocImageMinWidthResolutionResult($getDocImageMinWidthResolutionResult);
    }
    /**
     * Get GetDocImageMinWidthResolutionResult value
     * @return int|null
     */
    public function getGetDocImageMinWidthResolutionResult(): ?int
    {
        return $this->GetDocImageMinWidthResolutionResult;
    }
    /**
     * Set GetDocImageMinWidthResolutionResult value
     * @param int $getDocImageMinWidthResolutionResult
     * @return \ID3Global\Models\GetDocImageMinWidthResolutionResponse
     */
    public function setGetDocImageMinWidthResolutionResult(?int $getDocImageMinWidthResolutionResult = null): self
    {
        // validation for constraint: int
        if (!is_null($getDocImageMinWidthResolutionResult) && !(is_int($getDocImageMinWidthResolutionResult) || ctype_digit($getDocImageMinWidthResolutionResult))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($getDocImageMinWidthResolutionResult, true), gettype($getDocImageMinWidthResolutionResult)), __LINE__);
        }
        $this->GetDocImageMinWidthResolutionResult = $getDocImageMinWidthResolutionResult;
        
        return $this;
    }
}
