<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetLocationCountriesResponse Models
 * @subpackage Structs
 */
class GetLocationCountriesResponse extends AbstractStructBase
{
    /**
     * The GetLocationCountriesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCountry|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCountry $GetLocationCountriesResult = null;
    /**
     * Constructor method for GetLocationCountriesResponse
     * @uses GetLocationCountriesResponse::setGetLocationCountriesResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalCountry $getLocationCountriesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCountry $getLocationCountriesResult = null)
    {
        $this
            ->setGetLocationCountriesResult($getLocationCountriesResult);
    }
    /**
     * Get GetLocationCountriesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCountry|null
     */
    public function getGetLocationCountriesResult(): ?\ID3Global\Arrays\ArrayOfGlobalCountry
    {
        return isset($this->GetLocationCountriesResult) ? $this->GetLocationCountriesResult : null;
    }
    /**
     * Set GetLocationCountriesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCountry $getLocationCountriesResult
     * @return \ID3Global\Models\GetLocationCountriesResponse
     */
    public function setGetLocationCountriesResult(?\ID3Global\Arrays\ArrayOfGlobalCountry $getLocationCountriesResult = null): self
    {
        if (is_null($getLocationCountriesResult) || (is_array($getLocationCountriesResult) && empty($getLocationCountriesResult))) {
            unset($this->GetLocationCountriesResult);
        } else {
            $this->GetLocationCountriesResult = $getLocationCountriesResult;
        }
        
        return $this;
    }
}
