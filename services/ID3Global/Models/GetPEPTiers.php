<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPEPTiers Models
 * @subpackage Structs
 */
class GetPEPTiers extends AbstractStructBase
{
}
