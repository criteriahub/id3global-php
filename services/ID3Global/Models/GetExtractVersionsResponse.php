<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetExtractVersionsResponse Models
 * @subpackage Structs
 */
class GetExtractVersionsResponse extends AbstractStructBase
{
    /**
     * The GetExtractVersionsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $GetExtractVersionsResult = null;
    /**
     * Constructor method for GetExtractVersionsResponse
     * @uses GetExtractVersionsResponse::setGetExtractVersionsResult()
     * @param \ID3Global\Arrays\ArrayOfstring $getExtractVersionsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfstring $getExtractVersionsResult = null)
    {
        $this
            ->setGetExtractVersionsResult($getExtractVersionsResult);
    }
    /**
     * Get GetExtractVersionsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getGetExtractVersionsResult(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->GetExtractVersionsResult) ? $this->GetExtractVersionsResult : null;
    }
    /**
     * Set GetExtractVersionsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $getExtractVersionsResult
     * @return \ID3Global\Models\GetExtractVersionsResponse
     */
    public function setGetExtractVersionsResult(?\ID3Global\Arrays\ArrayOfstring $getExtractVersionsResult = null): self
    {
        if (is_null($getExtractVersionsResult) || (is_array($getExtractVersionsResult) && empty($getExtractVersionsResult))) {
            unset($this->GetExtractVersionsResult);
        } else {
            $this->GetExtractVersionsResult = $getExtractVersionsResult;
        }
        
        return $this;
    }
}
