<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSharedFraudResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q43:GlobalSharedFraudResultCodes
 * @subpackage Structs
 */
class GlobalSharedFraudResultCodes extends GlobalItemCheckResultCodes
{
    /**
     * The RiskAssessmentID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $RiskAssessmentID = null;
    /**
     * The EventID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $EventID = null;
    /**
     * Constructor method for GlobalSharedFraudResultCodes
     * @uses GlobalSharedFraudResultCodes::setRiskAssessmentID()
     * @uses GlobalSharedFraudResultCodes::setEventID()
     * @param string $riskAssessmentID
     * @param string $eventID
     */
    public function __construct(?string $riskAssessmentID = null, ?string $eventID = null)
    {
        $this
            ->setRiskAssessmentID($riskAssessmentID)
            ->setEventID($eventID);
    }
    /**
     * Get RiskAssessmentID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRiskAssessmentID(): ?string
    {
        return isset($this->RiskAssessmentID) ? $this->RiskAssessmentID : null;
    }
    /**
     * Set RiskAssessmentID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $riskAssessmentID
     * @return \ID3Global\Models\GlobalSharedFraudResultCodes
     */
    public function setRiskAssessmentID(?string $riskAssessmentID = null): self
    {
        // validation for constraint: string
        if (!is_null($riskAssessmentID) && !is_string($riskAssessmentID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($riskAssessmentID, true), gettype($riskAssessmentID)), __LINE__);
        }
        if (is_null($riskAssessmentID) || (is_array($riskAssessmentID) && empty($riskAssessmentID))) {
            unset($this->RiskAssessmentID);
        } else {
            $this->RiskAssessmentID = $riskAssessmentID;
        }
        
        return $this;
    }
    /**
     * Get EventID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEventID(): ?string
    {
        return isset($this->EventID) ? $this->EventID : null;
    }
    /**
     * Set EventID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $eventID
     * @return \ID3Global\Models\GlobalSharedFraudResultCodes
     */
    public function setEventID(?string $eventID = null): self
    {
        // validation for constraint: string
        if (!is_null($eventID) && !is_string($eventID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($eventID, true), gettype($eventID)), __LINE__);
        }
        if (is_null($eventID) || (is_array($eventID) && empty($eventID))) {
            unset($this->EventID);
        } else {
            $this->EventID = $eventID;
        }
        
        return $this;
    }
}
