<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSuppliersResponse Models
 * @subpackage Structs
 */
class GetSuppliersResponse extends AbstractStructBase
{
    /**
     * The GetSuppliersResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSupplier|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSupplier $GetSuppliersResult = null;
    /**
     * Constructor method for GetSuppliersResponse
     * @uses GetSuppliersResponse::setGetSuppliersResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplier $getSuppliersResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalSupplier $getSuppliersResult = null)
    {
        $this
            ->setGetSuppliersResult($getSuppliersResult);
    }
    /**
     * Get GetSuppliersResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplier|null
     */
    public function getGetSuppliersResult(): ?\ID3Global\Arrays\ArrayOfGlobalSupplier
    {
        return isset($this->GetSuppliersResult) ? $this->GetSuppliersResult : null;
    }
    /**
     * Set GetSuppliersResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplier $getSuppliersResult
     * @return \ID3Global\Models\GetSuppliersResponse
     */
    public function setGetSuppliersResult(?\ID3Global\Arrays\ArrayOfGlobalSupplier $getSuppliersResult = null): self
    {
        if (is_null($getSuppliersResult) || (is_array($getSuppliersResult) && empty($getSuppliersResult))) {
            unset($this->GetSuppliersResult);
        } else {
            $this->GetSuppliersResult = $getSuppliersResult;
        }
        
        return $this;
    }
}
