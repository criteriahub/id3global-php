<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalRole Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q593:GlobalRole
 * @subpackage Structs
 */
class GlobalRole extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The OrgName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $OrgName = null;
    /**
     * The RoleID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $RoleID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Description = null;
    /**
     * The DomainName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DomainName = null;
    /**
     * The IsAdminPolicy
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsAdminPolicy = null;
    /**
     * The Created
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Created = null;
    /**
     * The Active
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Active = null;
    /**
     * The System
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $System = null;
    /**
     * The IsMember
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsMember = null;
    /**
     * The HelpdeskAccess
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $HelpdeskAccess = null;
    /**
     * Constructor method for GlobalRole
     * @uses GlobalRole::setOrgID()
     * @uses GlobalRole::setOrgName()
     * @uses GlobalRole::setRoleID()
     * @uses GlobalRole::setName()
     * @uses GlobalRole::setDescription()
     * @uses GlobalRole::setDomainName()
     * @uses GlobalRole::setIsAdminPolicy()
     * @uses GlobalRole::setCreated()
     * @uses GlobalRole::setActive()
     * @uses GlobalRole::setSystem()
     * @uses GlobalRole::setIsMember()
     * @uses GlobalRole::setHelpdeskAccess()
     * @param string $orgID
     * @param string $orgName
     * @param string $roleID
     * @param string $name
     * @param string $description
     * @param string $domainName
     * @param bool $isAdminPolicy
     * @param string $created
     * @param bool $active
     * @param bool $system
     * @param bool $isMember
     * @param bool $helpdeskAccess
     */
    public function __construct(?string $orgID = null, ?string $orgName = null, ?string $roleID = null, ?string $name = null, ?string $description = null, ?string $domainName = null, ?bool $isAdminPolicy = null, ?string $created = null, ?bool $active = null, ?bool $system = null, ?bool $isMember = null, ?bool $helpdeskAccess = null)
    {
        $this
            ->setOrgID($orgID)
            ->setOrgName($orgName)
            ->setRoleID($roleID)
            ->setName($name)
            ->setDescription($description)
            ->setDomainName($domainName)
            ->setIsAdminPolicy($isAdminPolicy)
            ->setCreated($created)
            ->setActive($active)
            ->setSystem($system)
            ->setIsMember($isMember)
            ->setHelpdeskAccess($helpdeskAccess);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GlobalRole
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get OrgName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrgName(): ?string
    {
        return isset($this->OrgName) ? $this->OrgName : null;
    }
    /**
     * Set OrgName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $orgName
     * @return \ID3Global\Models\GlobalRole
     */
    public function setOrgName(?string $orgName = null): self
    {
        // validation for constraint: string
        if (!is_null($orgName) && !is_string($orgName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgName, true), gettype($orgName)), __LINE__);
        }
        if (is_null($orgName) || (is_array($orgName) && empty($orgName))) {
            unset($this->OrgName);
        } else {
            $this->OrgName = $orgName;
        }
        
        return $this;
    }
    /**
     * Get RoleID value
     * @return string|null
     */
    public function getRoleID(): ?string
    {
        return $this->RoleID;
    }
    /**
     * Set RoleID value
     * @param string $roleID
     * @return \ID3Global\Models\GlobalRole
     */
    public function setRoleID(?string $roleID = null): self
    {
        // validation for constraint: string
        if (!is_null($roleID) && !is_string($roleID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($roleID, true), gettype($roleID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($roleID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $roleID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($roleID, true)), __LINE__);
        }
        $this->RoleID = $roleID;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalRole
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \ID3Global\Models\GlobalRole
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        
        return $this;
    }
    /**
     * Get DomainName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDomainName(): ?string
    {
        return isset($this->DomainName) ? $this->DomainName : null;
    }
    /**
     * Set DomainName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $domainName
     * @return \ID3Global\Models\GlobalRole
     */
    public function setDomainName(?string $domainName = null): self
    {
        // validation for constraint: string
        if (!is_null($domainName) && !is_string($domainName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domainName, true), gettype($domainName)), __LINE__);
        }
        if (is_null($domainName) || (is_array($domainName) && empty($domainName))) {
            unset($this->DomainName);
        } else {
            $this->DomainName = $domainName;
        }
        
        return $this;
    }
    /**
     * Get IsAdminPolicy value
     * @return bool|null
     */
    public function getIsAdminPolicy(): ?bool
    {
        return $this->IsAdminPolicy;
    }
    /**
     * Set IsAdminPolicy value
     * @param bool $isAdminPolicy
     * @return \ID3Global\Models\GlobalRole
     */
    public function setIsAdminPolicy(?bool $isAdminPolicy = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isAdminPolicy) && !is_bool($isAdminPolicy)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isAdminPolicy, true), gettype($isAdminPolicy)), __LINE__);
        }
        $this->IsAdminPolicy = $isAdminPolicy;
        
        return $this;
    }
    /**
     * Get Created value
     * @return string|null
     */
    public function getCreated(): ?string
    {
        return $this->Created;
    }
    /**
     * Set Created value
     * @param string $created
     * @return \ID3Global\Models\GlobalRole
     */
    public function setCreated(?string $created = null): self
    {
        // validation for constraint: string
        if (!is_null($created) && !is_string($created)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($created, true), gettype($created)), __LINE__);
        }
        $this->Created = $created;
        
        return $this;
    }
    /**
     * Get Active value
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->Active;
    }
    /**
     * Set Active value
     * @param bool $active
     * @return \ID3Global\Models\GlobalRole
     */
    public function setActive(?bool $active = null): self
    {
        // validation for constraint: boolean
        if (!is_null($active) && !is_bool($active)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($active, true), gettype($active)), __LINE__);
        }
        $this->Active = $active;
        
        return $this;
    }
    /**
     * Get System value
     * @return bool|null
     */
    public function getSystem(): ?bool
    {
        return $this->System;
    }
    /**
     * Set System value
     * @param bool $system
     * @return \ID3Global\Models\GlobalRole
     */
    public function setSystem(?bool $system = null): self
    {
        // validation for constraint: boolean
        if (!is_null($system) && !is_bool($system)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($system, true), gettype($system)), __LINE__);
        }
        $this->System = $system;
        
        return $this;
    }
    /**
     * Get IsMember value
     * @return bool|null
     */
    public function getIsMember(): ?bool
    {
        return $this->IsMember;
    }
    /**
     * Set IsMember value
     * @param bool $isMember
     * @return \ID3Global\Models\GlobalRole
     */
    public function setIsMember(?bool $isMember = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isMember) && !is_bool($isMember)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isMember, true), gettype($isMember)), __LINE__);
        }
        $this->IsMember = $isMember;
        
        return $this;
    }
    /**
     * Get HelpdeskAccess value
     * @return bool|null
     */
    public function getHelpdeskAccess(): ?bool
    {
        return $this->HelpdeskAccess;
    }
    /**
     * Set HelpdeskAccess value
     * @param bool $helpdeskAccess
     * @return \ID3Global\Models\GlobalRole
     */
    public function setHelpdeskAccess(?bool $helpdeskAccess = null): self
    {
        // validation for constraint: boolean
        if (!is_null($helpdeskAccess) && !is_bool($helpdeskAccess)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($helpdeskAccess, true), gettype($helpdeskAccess)), __LINE__);
        }
        $this->HelpdeskAccess = $helpdeskAccess;
        
        return $this;
    }
}
