<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalMobileAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q689:GlobalMobileAccount
 * @subpackage Structs
 */
class GlobalMobileAccount extends GlobalSupplierAccount
{
    /**
     * The AccessToken
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccessToken = null;
    /**
     * Constructor method for GlobalMobileAccount
     * @uses GlobalMobileAccount::setAccessToken()
     * @param string $accessToken
     */
    public function __construct(?string $accessToken = null)
    {
        $this
            ->setAccessToken($accessToken);
    }
    /**
     * Get AccessToken value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccessToken(): ?string
    {
        return isset($this->AccessToken) ? $this->AccessToken : null;
    }
    /**
     * Set AccessToken value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accessToken
     * @return \ID3Global\Models\GlobalMobileAccount
     */
    public function setAccessToken(?string $accessToken = null): self
    {
        // validation for constraint: string
        if (!is_null($accessToken) && !is_string($accessToken)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accessToken, true), gettype($accessToken)), __LINE__);
        }
        if (is_null($accessToken) || (is_array($accessToken) && empty($accessToken))) {
            unset($this->AccessToken);
        } else {
            $this->AccessToken = $accessToken;
        }
        
        return $this;
    }
}
