<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSupportContact Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q558:GlobalSupportContact
 * @subpackage Structs
 */
class GlobalSupportContact extends AbstractStructBase
{
    /**
     * The Telephone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Telephone = null;
    /**
     * The Email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Email = null;
    /**
     * The Hours
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Hours = null;
    /**
     * Constructor method for GlobalSupportContact
     * @uses GlobalSupportContact::setTelephone()
     * @uses GlobalSupportContact::setEmail()
     * @uses GlobalSupportContact::setHours()
     * @param string $telephone
     * @param string $email
     * @param string $hours
     */
    public function __construct(?string $telephone = null, ?string $email = null, ?string $hours = null)
    {
        $this
            ->setTelephone($telephone)
            ->setEmail($email)
            ->setHours($hours);
    }
    /**
     * Get Telephone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTelephone(): ?string
    {
        return isset($this->Telephone) ? $this->Telephone : null;
    }
    /**
     * Set Telephone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $telephone
     * @return \ID3Global\Models\GlobalSupportContact
     */
    public function setTelephone(?string $telephone = null): self
    {
        // validation for constraint: string
        if (!is_null($telephone) && !is_string($telephone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($telephone, true), gettype($telephone)), __LINE__);
        }
        if (is_null($telephone) || (is_array($telephone) && empty($telephone))) {
            unset($this->Telephone);
        } else {
            $this->Telephone = $telephone;
        }
        
        return $this;
    }
    /**
     * Get Email value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return isset($this->Email) ? $this->Email : null;
    }
    /**
     * Set Email value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email
     * @return \ID3Global\Models\GlobalSupportContact
     */
    public function setEmail(?string $email = null): self
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        if (is_null($email) || (is_array($email) && empty($email))) {
            unset($this->Email);
        } else {
            $this->Email = $email;
        }
        
        return $this;
    }
    /**
     * Get Hours value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getHours(): ?string
    {
        return isset($this->Hours) ? $this->Hours : null;
    }
    /**
     * Set Hours value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $hours
     * @return \ID3Global\Models\GlobalSupportContact
     */
    public function setHours(?string $hours = null): self
    {
        // validation for constraint: string
        if (!is_null($hours) && !is_string($hours)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hours, true), gettype($hours)), __LINE__);
        }
        if (is_null($hours) || (is_array($hours) && empty($hours))) {
            unset($this->Hours);
        } else {
            $this->Hours = $hours;
        }
        
        return $this;
    }
}
