<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDetailedInvestigationResponse Models
 * @subpackage Structs
 */
class GetDetailedInvestigationResponse extends AbstractStructBase
{
    /**
     * The GetDetailedInvestigationResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $GetDetailedInvestigationResult = null;
    /**
     * Constructor method for GetDetailedInvestigationResponse
     * @uses GetDetailedInvestigationResponse::setGetDetailedInvestigationResult()
     * @param string $getDetailedInvestigationResult
     */
    public function __construct(?string $getDetailedInvestigationResult = null)
    {
        $this
            ->setGetDetailedInvestigationResult($getDetailedInvestigationResult);
    }
    /**
     * Get GetDetailedInvestigationResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGetDetailedInvestigationResult(): ?string
    {
        return isset($this->GetDetailedInvestigationResult) ? $this->GetDetailedInvestigationResult : null;
    }
    /**
     * Set GetDetailedInvestigationResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $getDetailedInvestigationResult
     * @return \ID3Global\Models\GetDetailedInvestigationResponse
     */
    public function setGetDetailedInvestigationResult(?string $getDetailedInvestigationResult = null): self
    {
        // validation for constraint: string
        if (!is_null($getDetailedInvestigationResult) && !is_string($getDetailedInvestigationResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($getDetailedInvestigationResult, true), gettype($getDetailedInvestigationResult)), __LINE__);
        }
        if (is_null($getDetailedInvestigationResult) || (is_array($getDetailedInvestigationResult) && empty($getDetailedInvestigationResult))) {
            unset($this->GetDetailedInvestigationResult);
        } else {
            $this->GetDetailedInvestigationResult = $getDetailedInvestigationResult;
        }
        
        return $this;
    }
}
