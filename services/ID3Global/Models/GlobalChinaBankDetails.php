<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalChinaBankDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q429:GlobalChinaBankDetails
 * @subpackage Structs
 */
class GlobalChinaBankDetails extends AbstractStructBase
{
    /**
     * The ChinaBankCardNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ChinaBankCardNumber = null;
    /**
     * Constructor method for GlobalChinaBankDetails
     * @uses GlobalChinaBankDetails::setChinaBankCardNumber()
     * @param string $chinaBankCardNumber
     */
    public function __construct(?string $chinaBankCardNumber = null)
    {
        $this
            ->setChinaBankCardNumber($chinaBankCardNumber);
    }
    /**
     * Get ChinaBankCardNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getChinaBankCardNumber(): ?string
    {
        return isset($this->ChinaBankCardNumber) ? $this->ChinaBankCardNumber : null;
    }
    /**
     * Set ChinaBankCardNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $chinaBankCardNumber
     * @return \ID3Global\Models\GlobalChinaBankDetails
     */
    public function setChinaBankCardNumber(?string $chinaBankCardNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($chinaBankCardNumber) && !is_string($chinaBankCardNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($chinaBankCardNumber, true), gettype($chinaBankCardNumber)), __LINE__);
        }
        if (is_null($chinaBankCardNumber) || (is_array($chinaBankCardNumber) && empty($chinaBankCardNumber))) {
            unset($this->ChinaBankCardNumber);
        } else {
            $this->ChinaBankCardNumber = $chinaBankCardNumber;
        }
        
        return $this;
    }
}
