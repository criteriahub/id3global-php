<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDocImageSupportedFileTypesResponse Models
 * @subpackage Structs
 */
class GetDocImageSupportedFileTypesResponse extends AbstractStructBase
{
    /**
     * The GetDocImageSupportedFileTypesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $GetDocImageSupportedFileTypesResult = null;
    /**
     * Constructor method for GetDocImageSupportedFileTypesResponse
     * @uses GetDocImageSupportedFileTypesResponse::setGetDocImageSupportedFileTypesResult()
     * @param \ID3Global\Arrays\ArrayOfstring $getDocImageSupportedFileTypesResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfstring $getDocImageSupportedFileTypesResult = null)
    {
        $this
            ->setGetDocImageSupportedFileTypesResult($getDocImageSupportedFileTypesResult);
    }
    /**
     * Get GetDocImageSupportedFileTypesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getGetDocImageSupportedFileTypesResult(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->GetDocImageSupportedFileTypesResult) ? $this->GetDocImageSupportedFileTypesResult : null;
    }
    /**
     * Set GetDocImageSupportedFileTypesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $getDocImageSupportedFileTypesResult
     * @return \ID3Global\Models\GetDocImageSupportedFileTypesResponse
     */
    public function setGetDocImageSupportedFileTypesResult(?\ID3Global\Arrays\ArrayOfstring $getDocImageSupportedFileTypesResult = null): self
    {
        if (is_null($getDocImageSupportedFileTypesResult) || (is_array($getDocImageSupportedFileTypesResult) && empty($getDocImageSupportedFileTypesResult))) {
            unset($this->GetDocImageSupportedFileTypesResult);
        } else {
            $this->GetDocImageSupportedFileTypesResult = $getDocImageSupportedFileTypesResult;
        }
        
        return $this;
    }
}
