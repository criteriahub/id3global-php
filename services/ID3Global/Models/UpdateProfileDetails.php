<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateProfileDetails Models
 * @subpackage Structs
 */
class UpdateProfileDetails extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The ProfileFull
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalProfileDetails|null
     */
    protected ?\ID3Global\Models\GlobalProfileDetails $ProfileFull = null;
    /**
     * The NewVersion
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $NewVersion = null;
    /**
     * Constructor method for UpdateProfileDetails
     * @uses UpdateProfileDetails::setOrgID()
     * @uses UpdateProfileDetails::setProfileFull()
     * @uses UpdateProfileDetails::setNewVersion()
     * @param string $orgID
     * @param \ID3Global\Models\GlobalProfileDetails $profileFull
     * @param bool $newVersion
     */
    public function __construct(?string $orgID = null, ?\ID3Global\Models\GlobalProfileDetails $profileFull = null, ?bool $newVersion = null)
    {
        $this
            ->setOrgID($orgID)
            ->setProfileFull($profileFull)
            ->setNewVersion($newVersion);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\UpdateProfileDetails
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get ProfileFull value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalProfileDetails|null
     */
    public function getProfileFull(): ?\ID3Global\Models\GlobalProfileDetails
    {
        return isset($this->ProfileFull) ? $this->ProfileFull : null;
    }
    /**
     * Set ProfileFull value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalProfileDetails $profileFull
     * @return \ID3Global\Models\UpdateProfileDetails
     */
    public function setProfileFull(?\ID3Global\Models\GlobalProfileDetails $profileFull = null): self
    {
        if (is_null($profileFull) || (is_array($profileFull) && empty($profileFull))) {
            unset($this->ProfileFull);
        } else {
            $this->ProfileFull = $profileFull;
        }
        
        return $this;
    }
    /**
     * Get NewVersion value
     * @return bool|null
     */
    public function getNewVersion(): ?bool
    {
        return $this->NewVersion;
    }
    /**
     * Set NewVersion value
     * @param bool $newVersion
     * @return \ID3Global\Models\UpdateProfileDetails
     */
    public function setNewVersion(?bool $newVersion = null): self
    {
        // validation for constraint: boolean
        if (!is_null($newVersion) && !is_bool($newVersion)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($newVersion, true), gettype($newVersion)), __LINE__);
        }
        $this->NewVersion = $newVersion;
        
        return $this;
    }
}
