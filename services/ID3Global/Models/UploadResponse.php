<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UploadResponse Models
 * @subpackage Structs
 */
class UploadResponse extends AbstractStructBase
{
    /**
     * The UploadResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDIVData|null
     */
    protected ?\ID3Global\Models\GlobalDIVData $UploadResult = null;
    /**
     * Constructor method for UploadResponse
     * @uses UploadResponse::setUploadResult()
     * @param \ID3Global\Models\GlobalDIVData $uploadResult
     */
    public function __construct(?\ID3Global\Models\GlobalDIVData $uploadResult = null)
    {
        $this
            ->setUploadResult($uploadResult);
    }
    /**
     * Get UploadResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDIVData|null
     */
    public function getUploadResult(): ?\ID3Global\Models\GlobalDIVData
    {
        return isset($this->UploadResult) ? $this->UploadResult : null;
    }
    /**
     * Set UploadResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalDIVData $uploadResult
     * @return \ID3Global\Models\UploadResponse
     */
    public function setUploadResult(?\ID3Global\Models\GlobalDIVData $uploadResult = null): self
    {
        if (is_null($uploadResult) || (is_array($uploadResult) && empty($uploadResult))) {
            unset($this->UploadResult);
        } else {
            $this->UploadResult = $uploadResult;
        }
        
        return $this;
    }
}
