<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseDispatchParameters Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q889:GlobalCaseDispatchParameters
 * @subpackage Structs
 */
class GlobalCaseDispatchParameters extends GlobalCaseResultParameters
{
    /**
     * The ServiceType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $ServiceType = null;
    /**
     * The ServiceAgreementID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ServiceAgreementID = null;
    /**
     * The RejectStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $RejectStatus = null;
    /**
     * The ResendStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ResendStatus = null;
    /**
     * The ConsentStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ConsentStatus = null;
    /**
     * Constructor method for GlobalCaseDispatchParameters
     * @uses GlobalCaseDispatchParameters::setServiceType()
     * @uses GlobalCaseDispatchParameters::setServiceAgreementID()
     * @uses GlobalCaseDispatchParameters::setRejectStatus()
     * @uses GlobalCaseDispatchParameters::setResendStatus()
     * @uses GlobalCaseDispatchParameters::setConsentStatus()
     * @param string $serviceType
     * @param string $serviceAgreementID
     * @param int $rejectStatus
     * @param int $resendStatus
     * @param int $consentStatus
     */
    public function __construct(?string $serviceType = null, ?string $serviceAgreementID = null, ?int $rejectStatus = null, ?int $resendStatus = null, ?int $consentStatus = null)
    {
        $this
            ->setServiceType($serviceType)
            ->setServiceAgreementID($serviceAgreementID)
            ->setRejectStatus($rejectStatus)
            ->setResendStatus($resendStatus)
            ->setConsentStatus($consentStatus);
    }
    /**
     * Get ServiceType value
     * @return string|null
     */
    public function getServiceType(): ?string
    {
        return $this->ServiceType;
    }
    /**
     * Set ServiceType value
     * @uses \ID3Global\Enums\GlobalCaseDispatchServiceType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalCaseDispatchServiceType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $serviceType
     * @return \ID3Global\Models\GlobalCaseDispatchParameters
     */
    public function setServiceType(?string $serviceType = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalCaseDispatchServiceType::valueIsValid($serviceType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalCaseDispatchServiceType', is_array($serviceType) ? implode(', ', $serviceType) : var_export($serviceType, true), implode(', ', \ID3Global\Enums\GlobalCaseDispatchServiceType::getValidValues())), __LINE__);
        }
        $this->ServiceType = $serviceType;
        
        return $this;
    }
    /**
     * Get ServiceAgreementID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getServiceAgreementID(): ?string
    {
        return isset($this->ServiceAgreementID) ? $this->ServiceAgreementID : null;
    }
    /**
     * Set ServiceAgreementID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $serviceAgreementID
     * @return \ID3Global\Models\GlobalCaseDispatchParameters
     */
    public function setServiceAgreementID(?string $serviceAgreementID = null): self
    {
        // validation for constraint: string
        if (!is_null($serviceAgreementID) && !is_string($serviceAgreementID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceAgreementID, true), gettype($serviceAgreementID)), __LINE__);
        }
        if (is_null($serviceAgreementID) || (is_array($serviceAgreementID) && empty($serviceAgreementID))) {
            unset($this->ServiceAgreementID);
        } else {
            $this->ServiceAgreementID = $serviceAgreementID;
        }
        
        return $this;
    }
    /**
     * Get RejectStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getRejectStatus(): ?int
    {
        return isset($this->RejectStatus) ? $this->RejectStatus : null;
    }
    /**
     * Set RejectStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $rejectStatus
     * @return \ID3Global\Models\GlobalCaseDispatchParameters
     */
    public function setRejectStatus(?int $rejectStatus = null): self
    {
        // validation for constraint: int
        if (!is_null($rejectStatus) && !(is_int($rejectStatus) || ctype_digit($rejectStatus))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($rejectStatus, true), gettype($rejectStatus)), __LINE__);
        }
        if (is_null($rejectStatus) || (is_array($rejectStatus) && empty($rejectStatus))) {
            unset($this->RejectStatus);
        } else {
            $this->RejectStatus = $rejectStatus;
        }
        
        return $this;
    }
    /**
     * Get ResendStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getResendStatus(): ?int
    {
        return isset($this->ResendStatus) ? $this->ResendStatus : null;
    }
    /**
     * Set ResendStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $resendStatus
     * @return \ID3Global\Models\GlobalCaseDispatchParameters
     */
    public function setResendStatus(?int $resendStatus = null): self
    {
        // validation for constraint: int
        if (!is_null($resendStatus) && !(is_int($resendStatus) || ctype_digit($resendStatus))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($resendStatus, true), gettype($resendStatus)), __LINE__);
        }
        if (is_null($resendStatus) || (is_array($resendStatus) && empty($resendStatus))) {
            unset($this->ResendStatus);
        } else {
            $this->ResendStatus = $resendStatus;
        }
        
        return $this;
    }
    /**
     * Get ConsentStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getConsentStatus(): ?int
    {
        return isset($this->ConsentStatus) ? $this->ConsentStatus : null;
    }
    /**
     * Set ConsentStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $consentStatus
     * @return \ID3Global\Models\GlobalCaseDispatchParameters
     */
    public function setConsentStatus(?int $consentStatus = null): self
    {
        // validation for constraint: int
        if (!is_null($consentStatus) && !(is_int($consentStatus) || ctype_digit($consentStatus))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($consentStatus, true), gettype($consentStatus)), __LINE__);
        }
        if (is_null($consentStatus) || (is_array($consentStatus) && empty($consentStatus))) {
            unset($this->ConsentStatus);
        } else {
            $this->ConsentStatus = $consentStatus;
        }
        
        return $this;
    }
}
