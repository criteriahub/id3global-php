<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSupplierAccountResponse Models
 * @subpackage Structs
 */
class GetSupplierAccountResponse extends AbstractStructBase
{
    /**
     * The GetSupplierAccountResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierAccount|null
     */
    protected ?\ID3Global\Models\GlobalSupplierAccount $GetSupplierAccountResult = null;
    /**
     * Constructor method for GetSupplierAccountResponse
     * @uses GetSupplierAccountResponse::setGetSupplierAccountResult()
     * @param \ID3Global\Models\GlobalSupplierAccount $getSupplierAccountResult
     */
    public function __construct(?\ID3Global\Models\GlobalSupplierAccount $getSupplierAccountResult = null)
    {
        $this
            ->setGetSupplierAccountResult($getSupplierAccountResult);
    }
    /**
     * Get GetSupplierAccountResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierAccount|null
     */
    public function getGetSupplierAccountResult(): ?\ID3Global\Models\GlobalSupplierAccount
    {
        return isset($this->GetSupplierAccountResult) ? $this->GetSupplierAccountResult : null;
    }
    /**
     * Set GetSupplierAccountResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupplierAccount $getSupplierAccountResult
     * @return \ID3Global\Models\GetSupplierAccountResponse
     */
    public function setGetSupplierAccountResult(?\ID3Global\Models\GlobalSupplierAccount $getSupplierAccountResult = null): self
    {
        if (is_null($getSupplierAccountResult) || (is_array($getSupplierAccountResult) && empty($getSupplierAccountResult))) {
            unset($this->GetSupplierAccountResult);
        } else {
            $this->GetSupplierAccountResult = $getSupplierAccountResult;
        }
        
        return $this;
    }
}
