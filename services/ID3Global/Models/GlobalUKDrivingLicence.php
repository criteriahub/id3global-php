<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalUKDrivingLicence Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q369:GlobalUKDrivingLicence
 * @subpackage Structs
 */
class GlobalUKDrivingLicence extends AbstractStructBase
{
    /**
     * The Number
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Number = null;
    /**
     * The MailSort
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MailSort = null;
    /**
     * The Postcode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Postcode = null;
    /**
     * The Microfiche
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Microfiche = null;
    /**
     * The IssueDay
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $IssueDay = null;
    /**
     * The IssueMonth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $IssueMonth = null;
    /**
     * The IssueYear
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $IssueYear = null;
    /**
     * The IssueNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $IssueNumber = null;
    /**
     * The ExpiryDay
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryDay = null;
    /**
     * The ExpiryMonth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryMonth = null;
    /**
     * The ExpiryYear
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExpiryYear = null;
    /**
     * Constructor method for GlobalUKDrivingLicence
     * @uses GlobalUKDrivingLicence::setNumber()
     * @uses GlobalUKDrivingLicence::setMailSort()
     * @uses GlobalUKDrivingLicence::setPostcode()
     * @uses GlobalUKDrivingLicence::setMicrofiche()
     * @uses GlobalUKDrivingLicence::setIssueDay()
     * @uses GlobalUKDrivingLicence::setIssueMonth()
     * @uses GlobalUKDrivingLicence::setIssueYear()
     * @uses GlobalUKDrivingLicence::setIssueNumber()
     * @uses GlobalUKDrivingLicence::setExpiryDay()
     * @uses GlobalUKDrivingLicence::setExpiryMonth()
     * @uses GlobalUKDrivingLicence::setExpiryYear()
     * @param string $number
     * @param string $mailSort
     * @param string $postcode
     * @param string $microfiche
     * @param int $issueDay
     * @param int $issueMonth
     * @param int $issueYear
     * @param int $issueNumber
     * @param int $expiryDay
     * @param int $expiryMonth
     * @param int $expiryYear
     */
    public function __construct(?string $number = null, ?string $mailSort = null, ?string $postcode = null, ?string $microfiche = null, ?int $issueDay = null, ?int $issueMonth = null, ?int $issueYear = null, ?int $issueNumber = null, ?int $expiryDay = null, ?int $expiryMonth = null, ?int $expiryYear = null)
    {
        $this
            ->setNumber($number)
            ->setMailSort($mailSort)
            ->setPostcode($postcode)
            ->setMicrofiche($microfiche)
            ->setIssueDay($issueDay)
            ->setIssueMonth($issueMonth)
            ->setIssueYear($issueYear)
            ->setIssueNumber($issueNumber)
            ->setExpiryDay($expiryDay)
            ->setExpiryMonth($expiryMonth)
            ->setExpiryYear($expiryYear);
    }
    /**
     * Get Number value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return isset($this->Number) ? $this->Number : null;
    }
    /**
     * Set Number value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $number
     * @return \ID3Global\Models\GlobalUKDrivingLicence
     */
    public function setNumber(?string $number = null): self
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($number, true), gettype($number)), __LINE__);
        }
        if (is_null($number) || (is_array($number) && empty($number))) {
            unset($this->Number);
        } else {
            $this->Number = $number;
        }
        
        return $this;
    }
    /**
     * Get MailSort value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMailSort(): ?string
    {
        return isset($this->MailSort) ? $this->MailSort : null;
    }
    /**
     * Set MailSort value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mailSort
     * @return \ID3Global\Models\GlobalUKDrivingLicence
     */
    public function setMailSort(?string $mailSort = null): self
    {
        // validation for constraint: string
        if (!is_null($mailSort) && !is_string($mailSort)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mailSort, true), gettype($mailSort)), __LINE__);
        }
        if (is_null($mailSort) || (is_array($mailSort) && empty($mailSort))) {
            unset($this->MailSort);
        } else {
            $this->MailSort = $mailSort;
        }
        
        return $this;
    }
    /**
     * Get Postcode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPostcode(): ?string
    {
        return isset($this->Postcode) ? $this->Postcode : null;
    }
    /**
     * Set Postcode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $postcode
     * @return \ID3Global\Models\GlobalUKDrivingLicence
     */
    public function setPostcode(?string $postcode = null): self
    {
        // validation for constraint: string
        if (!is_null($postcode) && !is_string($postcode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($postcode, true), gettype($postcode)), __LINE__);
        }
        if (is_null($postcode) || (is_array($postcode) && empty($postcode))) {
            unset($this->Postcode);
        } else {
            $this->Postcode = $postcode;
        }
        
        return $this;
    }
    /**
     * Get Microfiche value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMicrofiche(): ?string
    {
        return isset($this->Microfiche) ? $this->Microfiche : null;
    }
    /**
     * Set Microfiche value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $microfiche
     * @return \ID3Global\Models\GlobalUKDrivingLicence
     */
    public function setMicrofiche(?string $microfiche = null): self
    {
        // validation for constraint: string
        if (!is_null($microfiche) && !is_string($microfiche)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($microfiche, true), gettype($microfiche)), __LINE__);
        }
        if (is_null($microfiche) || (is_array($microfiche) && empty($microfiche))) {
            unset($this->Microfiche);
        } else {
            $this->Microfiche = $microfiche;
        }
        
        return $this;
    }
    /**
     * Get IssueDay value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getIssueDay(): ?int
    {
        return isset($this->IssueDay) ? $this->IssueDay : null;
    }
    /**
     * Set IssueDay value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $issueDay
     * @return \ID3Global\Models\GlobalUKDrivingLicence
     */
    public function setIssueDay(?int $issueDay = null): self
    {
        // validation for constraint: int
        if (!is_null($issueDay) && !(is_int($issueDay) || ctype_digit($issueDay))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($issueDay, true), gettype($issueDay)), __LINE__);
        }
        if (is_null($issueDay) || (is_array($issueDay) && empty($issueDay))) {
            unset($this->IssueDay);
        } else {
            $this->IssueDay = $issueDay;
        }
        
        return $this;
    }
    /**
     * Get IssueMonth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getIssueMonth(): ?int
    {
        return isset($this->IssueMonth) ? $this->IssueMonth : null;
    }
    /**
     * Set IssueMonth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $issueMonth
     * @return \ID3Global\Models\GlobalUKDrivingLicence
     */
    public function setIssueMonth(?int $issueMonth = null): self
    {
        // validation for constraint: int
        if (!is_null($issueMonth) && !(is_int($issueMonth) || ctype_digit($issueMonth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($issueMonth, true), gettype($issueMonth)), __LINE__);
        }
        if (is_null($issueMonth) || (is_array($issueMonth) && empty($issueMonth))) {
            unset($this->IssueMonth);
        } else {
            $this->IssueMonth = $issueMonth;
        }
        
        return $this;
    }
    /**
     * Get IssueYear value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getIssueYear(): ?int
    {
        return isset($this->IssueYear) ? $this->IssueYear : null;
    }
    /**
     * Set IssueYear value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $issueYear
     * @return \ID3Global\Models\GlobalUKDrivingLicence
     */
    public function setIssueYear(?int $issueYear = null): self
    {
        // validation for constraint: int
        if (!is_null($issueYear) && !(is_int($issueYear) || ctype_digit($issueYear))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($issueYear, true), gettype($issueYear)), __LINE__);
        }
        if (is_null($issueYear) || (is_array($issueYear) && empty($issueYear))) {
            unset($this->IssueYear);
        } else {
            $this->IssueYear = $issueYear;
        }
        
        return $this;
    }
    /**
     * Get IssueNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getIssueNumber(): ?int
    {
        return isset($this->IssueNumber) ? $this->IssueNumber : null;
    }
    /**
     * Set IssueNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $issueNumber
     * @return \ID3Global\Models\GlobalUKDrivingLicence
     */
    public function setIssueNumber(?int $issueNumber = null): self
    {
        // validation for constraint: int
        if (!is_null($issueNumber) && !(is_int($issueNumber) || ctype_digit($issueNumber))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($issueNumber, true), gettype($issueNumber)), __LINE__);
        }
        if (is_null($issueNumber) || (is_array($issueNumber) && empty($issueNumber))) {
            unset($this->IssueNumber);
        } else {
            $this->IssueNumber = $issueNumber;
        }
        
        return $this;
    }
    /**
     * Get ExpiryDay value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryDay(): ?int
    {
        return isset($this->ExpiryDay) ? $this->ExpiryDay : null;
    }
    /**
     * Set ExpiryDay value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryDay
     * @return \ID3Global\Models\GlobalUKDrivingLicence
     */
    public function setExpiryDay(?int $expiryDay = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryDay) && !(is_int($expiryDay) || ctype_digit($expiryDay))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryDay, true), gettype($expiryDay)), __LINE__);
        }
        if (is_null($expiryDay) || (is_array($expiryDay) && empty($expiryDay))) {
            unset($this->ExpiryDay);
        } else {
            $this->ExpiryDay = $expiryDay;
        }
        
        return $this;
    }
    /**
     * Get ExpiryMonth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryMonth(): ?int
    {
        return isset($this->ExpiryMonth) ? $this->ExpiryMonth : null;
    }
    /**
     * Set ExpiryMonth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryMonth
     * @return \ID3Global\Models\GlobalUKDrivingLicence
     */
    public function setExpiryMonth(?int $expiryMonth = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryMonth) && !(is_int($expiryMonth) || ctype_digit($expiryMonth))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryMonth, true), gettype($expiryMonth)), __LINE__);
        }
        if (is_null($expiryMonth) || (is_array($expiryMonth) && empty($expiryMonth))) {
            unset($this->ExpiryMonth);
        } else {
            $this->ExpiryMonth = $expiryMonth;
        }
        
        return $this;
    }
    /**
     * Get ExpiryYear value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExpiryYear(): ?int
    {
        return isset($this->ExpiryYear) ? $this->ExpiryYear : null;
    }
    /**
     * Set ExpiryYear value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $expiryYear
     * @return \ID3Global\Models\GlobalUKDrivingLicence
     */
    public function setExpiryYear(?int $expiryYear = null): self
    {
        // validation for constraint: int
        if (!is_null($expiryYear) && !(is_int($expiryYear) || ctype_digit($expiryYear))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($expiryYear, true), gettype($expiryYear)), __LINE__);
        }
        if (is_null($expiryYear) || (is_array($expiryYear) && empty($expiryYear))) {
            unset($this->ExpiryYear);
        } else {
            $this->ExpiryYear = $expiryYear;
        }
        
        return $this;
    }
}
