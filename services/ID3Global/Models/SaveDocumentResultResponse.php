<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SaveDocumentResultResponse Models
 * @subpackage Structs
 */
class SaveDocumentResultResponse extends AbstractStructBase
{
    /**
     * The SaveDocumentResultResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCaseDetails|null
     */
    protected ?\ID3Global\Models\GlobalCaseDetails $SaveDocumentResultResult = null;
    /**
     * Constructor method for SaveDocumentResultResponse
     * @uses SaveDocumentResultResponse::setSaveDocumentResultResult()
     * @param \ID3Global\Models\GlobalCaseDetails $saveDocumentResultResult
     */
    public function __construct(?\ID3Global\Models\GlobalCaseDetails $saveDocumentResultResult = null)
    {
        $this
            ->setSaveDocumentResultResult($saveDocumentResultResult);
    }
    /**
     * Get SaveDocumentResultResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCaseDetails|null
     */
    public function getSaveDocumentResultResult(): ?\ID3Global\Models\GlobalCaseDetails
    {
        return isset($this->SaveDocumentResultResult) ? $this->SaveDocumentResultResult : null;
    }
    /**
     * Set SaveDocumentResultResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCaseDetails $saveDocumentResultResult
     * @return \ID3Global\Models\SaveDocumentResultResponse
     */
    public function setSaveDocumentResultResult(?\ID3Global\Models\GlobalCaseDetails $saveDocumentResultResult = null): self
    {
        if (is_null($saveDocumentResultResult) || (is_array($saveDocumentResultResult) && empty($saveDocumentResultResult))) {
            unset($this->SaveDocumentResultResult);
        } else {
            $this->SaveDocumentResultResult = $saveDocumentResultResult;
        }
        
        return $this;
    }
}
