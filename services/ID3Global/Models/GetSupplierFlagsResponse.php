<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSupplierFlagsResponse Models
 * @subpackage Structs
 */
class GetSupplierFlagsResponse extends AbstractStructBase
{
    /**
     * The GetSupplierFlagsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalSupplierFlags|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalSupplierFlags $GetSupplierFlagsResult = null;
    /**
     * Constructor method for GetSupplierFlagsResponse
     * @uses GetSupplierFlagsResponse::setGetSupplierFlagsResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplierFlags $getSupplierFlagsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalSupplierFlags $getSupplierFlagsResult = null)
    {
        $this
            ->setGetSupplierFlagsResult($getSupplierFlagsResult);
    }
    /**
     * Get GetSupplierFlagsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalSupplierFlags|null
     */
    public function getGetSupplierFlagsResult(): ?\ID3Global\Arrays\ArrayOfGlobalSupplierFlags
    {
        return isset($this->GetSupplierFlagsResult) ? $this->GetSupplierFlagsResult : null;
    }
    /**
     * Set GetSupplierFlagsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalSupplierFlags $getSupplierFlagsResult
     * @return \ID3Global\Models\GetSupplierFlagsResponse
     */
    public function setGetSupplierFlagsResult(?\ID3Global\Arrays\ArrayOfGlobalSupplierFlags $getSupplierFlagsResult = null): self
    {
        if (is_null($getSupplierFlagsResult) || (is_array($getSupplierFlagsResult) && empty($getSupplierFlagsResult))) {
            unset($this->GetSupplierFlagsResult);
        } else {
            $this->GetSupplierFlagsResult = $getSupplierFlagsResult;
        }
        
        return $this;
    }
}
