<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetFCRAMatchDetails Models
 * @subpackage Structs
 */
class GetFCRAMatchDetails extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The CaseID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $CaseID = null;
    /**
     * The SearchID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SearchID = null;
    /**
     * The FCRAEnabled
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $FCRAEnabled = null;
    /**
     * Constructor method for GetFCRAMatchDetails
     * @uses GetFCRAMatchDetails::setOrgID()
     * @uses GetFCRAMatchDetails::setCaseID()
     * @uses GetFCRAMatchDetails::setSearchID()
     * @uses GetFCRAMatchDetails::setFCRAEnabled()
     * @param string $orgID
     * @param string $caseID
     * @param string $searchID
     * @param bool $fCRAEnabled
     */
    public function __construct(?string $orgID = null, ?string $caseID = null, ?string $searchID = null, ?bool $fCRAEnabled = null)
    {
        $this
            ->setOrgID($orgID)
            ->setCaseID($caseID)
            ->setSearchID($searchID)
            ->setFCRAEnabled($fCRAEnabled);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetFCRAMatchDetails
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get CaseID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCaseID(): ?string
    {
        return isset($this->CaseID) ? $this->CaseID : null;
    }
    /**
     * Set CaseID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $caseID
     * @return \ID3Global\Models\GetFCRAMatchDetails
     */
    public function setCaseID(?string $caseID = null): self
    {
        // validation for constraint: string
        if (!is_null($caseID) && !is_string($caseID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($caseID, true), gettype($caseID)), __LINE__);
        }
        if (is_null($caseID) || (is_array($caseID) && empty($caseID))) {
            unset($this->CaseID);
        } else {
            $this->CaseID = $caseID;
        }
        
        return $this;
    }
    /**
     * Get SearchID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSearchID(): ?string
    {
        return isset($this->SearchID) ? $this->SearchID : null;
    }
    /**
     * Set SearchID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $searchID
     * @return \ID3Global\Models\GetFCRAMatchDetails
     */
    public function setSearchID(?string $searchID = null): self
    {
        // validation for constraint: string
        if (!is_null($searchID) && !is_string($searchID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchID, true), gettype($searchID)), __LINE__);
        }
        if (is_null($searchID) || (is_array($searchID) && empty($searchID))) {
            unset($this->SearchID);
        } else {
            $this->SearchID = $searchID;
        }
        
        return $this;
    }
    /**
     * Get FCRAEnabled value
     * @return bool|null
     */
    public function getFCRAEnabled(): ?bool
    {
        return $this->FCRAEnabled;
    }
    /**
     * Set FCRAEnabled value
     * @param bool $fCRAEnabled
     * @return \ID3Global\Models\GetFCRAMatchDetails
     */
    public function setFCRAEnabled(?bool $fCRAEnabled = null): self
    {
        // validation for constraint: boolean
        if (!is_null($fCRAEnabled) && !is_bool($fCRAEnabled)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($fCRAEnabled, true), gettype($fCRAEnabled)), __LINE__);
        }
        $this->FCRAEnabled = $fCRAEnabled;
        
        return $this;
    }
}
