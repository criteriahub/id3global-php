<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalItemCheckConfig Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q284:GlobalItemCheckConfig
 * @subpackage Structs
 */
class GlobalItemCheckConfig extends AbstractStructBase
{
    /**
     * The Id
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Id = null;
    /**
     * The ItemCheckIcon
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalIconImage|null
     */
    protected ?\ID3Global\Models\GlobalIconImage $ItemCheckIcon = null;
    /**
     * The CountryIcon
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalIconImage|null
     */
    protected ?\ID3Global\Models\GlobalIconImage $CountryIcon = null;
    /**
     * The InputFields
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckInputField|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckInputField $InputFields = null;
    /**
     * The AddressLookupCountry
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AddressLookupCountry = null;
    /**
     * Constructor method for GlobalItemCheckConfig
     * @uses GlobalItemCheckConfig::setId()
     * @uses GlobalItemCheckConfig::setItemCheckIcon()
     * @uses GlobalItemCheckConfig::setCountryIcon()
     * @uses GlobalItemCheckConfig::setInputFields()
     * @uses GlobalItemCheckConfig::setAddressLookupCountry()
     * @param int $id
     * @param \ID3Global\Models\GlobalIconImage $itemCheckIcon
     * @param \ID3Global\Models\GlobalIconImage $countryIcon
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckInputField $inputFields
     * @param string $addressLookupCountry
     */
    public function __construct(?int $id = null, ?\ID3Global\Models\GlobalIconImage $itemCheckIcon = null, ?\ID3Global\Models\GlobalIconImage $countryIcon = null, ?\ID3Global\Arrays\ArrayOfGlobalItemCheckInputField $inputFields = null, ?string $addressLookupCountry = null)
    {
        $this
            ->setId($id)
            ->setItemCheckIcon($itemCheckIcon)
            ->setCountryIcon($countryIcon)
            ->setInputFields($inputFields)
            ->setAddressLookupCountry($addressLookupCountry);
    }
    /**
     * Get Id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->Id;
    }
    /**
     * Set Id value
     * @param int $id
     * @return \ID3Global\Models\GlobalItemCheckConfig
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->Id = $id;
        
        return $this;
    }
    /**
     * Get ItemCheckIcon value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalIconImage|null
     */
    public function getItemCheckIcon(): ?\ID3Global\Models\GlobalIconImage
    {
        return isset($this->ItemCheckIcon) ? $this->ItemCheckIcon : null;
    }
    /**
     * Set ItemCheckIcon value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalIconImage $itemCheckIcon
     * @return \ID3Global\Models\GlobalItemCheckConfig
     */
    public function setItemCheckIcon(?\ID3Global\Models\GlobalIconImage $itemCheckIcon = null): self
    {
        if (is_null($itemCheckIcon) || (is_array($itemCheckIcon) && empty($itemCheckIcon))) {
            unset($this->ItemCheckIcon);
        } else {
            $this->ItemCheckIcon = $itemCheckIcon;
        }
        
        return $this;
    }
    /**
     * Get CountryIcon value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalIconImage|null
     */
    public function getCountryIcon(): ?\ID3Global\Models\GlobalIconImage
    {
        return isset($this->CountryIcon) ? $this->CountryIcon : null;
    }
    /**
     * Set CountryIcon value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalIconImage $countryIcon
     * @return \ID3Global\Models\GlobalItemCheckConfig
     */
    public function setCountryIcon(?\ID3Global\Models\GlobalIconImage $countryIcon = null): self
    {
        if (is_null($countryIcon) || (is_array($countryIcon) && empty($countryIcon))) {
            unset($this->CountryIcon);
        } else {
            $this->CountryIcon = $countryIcon;
        }
        
        return $this;
    }
    /**
     * Get InputFields value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckInputField|null
     */
    public function getInputFields(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckInputField
    {
        return isset($this->InputFields) ? $this->InputFields : null;
    }
    /**
     * Set InputFields value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckInputField $inputFields
     * @return \ID3Global\Models\GlobalItemCheckConfig
     */
    public function setInputFields(?\ID3Global\Arrays\ArrayOfGlobalItemCheckInputField $inputFields = null): self
    {
        if (is_null($inputFields) || (is_array($inputFields) && empty($inputFields))) {
            unset($this->InputFields);
        } else {
            $this->InputFields = $inputFields;
        }
        
        return $this;
    }
    /**
     * Get AddressLookupCountry value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressLookupCountry(): ?string
    {
        return isset($this->AddressLookupCountry) ? $this->AddressLookupCountry : null;
    }
    /**
     * Set AddressLookupCountry value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressLookupCountry
     * @return \ID3Global\Models\GlobalItemCheckConfig
     */
    public function setAddressLookupCountry(?string $addressLookupCountry = null): self
    {
        // validation for constraint: string
        if (!is_null($addressLookupCountry) && !is_string($addressLookupCountry)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressLookupCountry, true), gettype($addressLookupCountry)), __LINE__);
        }
        if (is_null($addressLookupCountry) || (is_array($addressLookupCountry) && empty($addressLookupCountry))) {
            unset($this->AddressLookupCountry);
        } else {
            $this->AddressLookupCountry = $addressLookupCountry;
        }
        
        return $this;
    }
}
