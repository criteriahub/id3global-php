<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CheckCredentialsResponse Models
 * @subpackage Structs
 */
class CheckCredentialsResponse extends AbstractStructBase
{
    /**
     * The CheckCredentialsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalAccount|null
     */
    protected ?\ID3Global\Models\GlobalAccount $CheckCredentialsResult = null;
    /**
     * Constructor method for CheckCredentialsResponse
     * @uses CheckCredentialsResponse::setCheckCredentialsResult()
     * @param \ID3Global\Models\GlobalAccount $checkCredentialsResult
     */
    public function __construct(?\ID3Global\Models\GlobalAccount $checkCredentialsResult = null)
    {
        $this
            ->setCheckCredentialsResult($checkCredentialsResult);
    }
    /**
     * Get CheckCredentialsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalAccount|null
     */
    public function getCheckCredentialsResult(): ?\ID3Global\Models\GlobalAccount
    {
        return isset($this->CheckCredentialsResult) ? $this->CheckCredentialsResult : null;
    }
    /**
     * Set CheckCredentialsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalAccount $checkCredentialsResult
     * @return \ID3Global\Models\CheckCredentialsResponse
     */
    public function setCheckCredentialsResult(?\ID3Global\Models\GlobalAccount $checkCredentialsResult = null): self
    {
        if (is_null($checkCredentialsResult) || (is_array($checkCredentialsResult) && empty($checkCredentialsResult))) {
            unset($this->CheckCredentialsResult);
        } else {
            $this->CheckCredentialsResult = $checkCredentialsResult;
        }
        
        return $this;
    }
}
