<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalItemCheckDecisionBand Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q255:GlobalItemCheckDecisionBand
 * @subpackage Structs
 */
class GlobalItemCheckDecisionBand extends AbstractStructBase
{
    /**
     * The ItemCheckId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ItemCheckId = null;
    /**
     * The ExceptionScore
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int|null
     */
    protected ?int $ExceptionScore = null;
    /**
     * The Bands
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckBand|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckBand $Bands = null;
    /**
     * Constructor method for GlobalItemCheckDecisionBand
     * @uses GlobalItemCheckDecisionBand::setItemCheckId()
     * @uses GlobalItemCheckDecisionBand::setExceptionScore()
     * @uses GlobalItemCheckDecisionBand::setBands()
     * @param int $itemCheckId
     * @param int $exceptionScore
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckBand $bands
     */
    public function __construct(?int $itemCheckId = null, ?int $exceptionScore = null, ?\ID3Global\Arrays\ArrayOfGlobalItemCheckBand $bands = null)
    {
        $this
            ->setItemCheckId($itemCheckId)
            ->setExceptionScore($exceptionScore)
            ->setBands($bands);
    }
    /**
     * Get ItemCheckId value
     * @return int|null
     */
    public function getItemCheckId(): ?int
    {
        return $this->ItemCheckId;
    }
    /**
     * Set ItemCheckId value
     * @param int $itemCheckId
     * @return \ID3Global\Models\GlobalItemCheckDecisionBand
     */
    public function setItemCheckId(?int $itemCheckId = null): self
    {
        // validation for constraint: int
        if (!is_null($itemCheckId) && !(is_int($itemCheckId) || ctype_digit($itemCheckId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($itemCheckId, true), gettype($itemCheckId)), __LINE__);
        }
        $this->ItemCheckId = $itemCheckId;
        
        return $this;
    }
    /**
     * Get ExceptionScore value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getExceptionScore(): ?int
    {
        return isset($this->ExceptionScore) ? $this->ExceptionScore : null;
    }
    /**
     * Set ExceptionScore value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $exceptionScore
     * @return \ID3Global\Models\GlobalItemCheckDecisionBand
     */
    public function setExceptionScore(?int $exceptionScore = null): self
    {
        // validation for constraint: int
        if (!is_null($exceptionScore) && !(is_int($exceptionScore) || ctype_digit($exceptionScore))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($exceptionScore, true), gettype($exceptionScore)), __LINE__);
        }
        if (is_null($exceptionScore) || (is_array($exceptionScore) && empty($exceptionScore))) {
            unset($this->ExceptionScore);
        } else {
            $this->ExceptionScore = $exceptionScore;
        }
        
        return $this;
    }
    /**
     * Get Bands value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckBand|null
     */
    public function getBands(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckBand
    {
        return isset($this->Bands) ? $this->Bands : null;
    }
    /**
     * Set Bands value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckBand $bands
     * @return \ID3Global\Models\GlobalItemCheckDecisionBand
     */
    public function setBands(?\ID3Global\Arrays\ArrayOfGlobalItemCheckBand $bands = null): self
    {
        if (is_null($bands) || (is_array($bands) && empty($bands))) {
            unset($this->Bands);
        } else {
            $this->Bands = $bands;
        }
        
        return $this;
    }
}
