<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalExternalDataResultParameter Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q881:GlobalExternalDataResultParameter
 * @subpackage Structs
 */
class GlobalExternalDataResultParameter extends AbstractStructBase
{
    /**
     * The MatchFieldByName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MatchFieldByName = null;
    /**
     * The MatchItemKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MatchItemKey = null;
    /**
     * Constructor method for GlobalExternalDataResultParameter
     * @uses GlobalExternalDataResultParameter::setMatchFieldByName()
     * @uses GlobalExternalDataResultParameter::setMatchItemKey()
     * @param string $matchFieldByName
     * @param string $matchItemKey
     */
    public function __construct(?string $matchFieldByName = null, ?string $matchItemKey = null)
    {
        $this
            ->setMatchFieldByName($matchFieldByName)
            ->setMatchItemKey($matchItemKey);
    }
    /**
     * Get MatchFieldByName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMatchFieldByName(): ?string
    {
        return isset($this->MatchFieldByName) ? $this->MatchFieldByName : null;
    }
    /**
     * Set MatchFieldByName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $matchFieldByName
     * @return \ID3Global\Models\GlobalExternalDataResultParameter
     */
    public function setMatchFieldByName(?string $matchFieldByName = null): self
    {
        // validation for constraint: string
        if (!is_null($matchFieldByName) && !is_string($matchFieldByName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($matchFieldByName, true), gettype($matchFieldByName)), __LINE__);
        }
        if (is_null($matchFieldByName) || (is_array($matchFieldByName) && empty($matchFieldByName))) {
            unset($this->MatchFieldByName);
        } else {
            $this->MatchFieldByName = $matchFieldByName;
        }
        
        return $this;
    }
    /**
     * Get MatchItemKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMatchItemKey(): ?string
    {
        return isset($this->MatchItemKey) ? $this->MatchItemKey : null;
    }
    /**
     * Set MatchItemKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $matchItemKey
     * @return \ID3Global\Models\GlobalExternalDataResultParameter
     */
    public function setMatchItemKey(?string $matchItemKey = null): self
    {
        // validation for constraint: string
        if (!is_null($matchItemKey) && !is_string($matchItemKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($matchItemKey, true), gettype($matchItemKey)), __LINE__);
        }
        if (is_null($matchItemKey) || (is_array($matchItemKey) && empty($matchItemKey))) {
            unset($this->MatchItemKey);
        } else {
            $this->MatchItemKey = $matchItemKey;
        }
        
        return $this;
    }
}
