<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCreditAffordabilityAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q707:GlobalCreditAffordabilityAccount
 * @subpackage Structs
 */
class GlobalCreditAffordabilityAccount extends GlobalSupplierAccount
{
    /**
     * The Company
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Company = null;
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * The LicenseBillingOption
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $LicenseBillingOption = null;
    /**
     * Constructor method for GlobalCreditAffordabilityAccount
     * @uses GlobalCreditAffordabilityAccount::setCompany()
     * @uses GlobalCreditAffordabilityAccount::setUsername()
     * @uses GlobalCreditAffordabilityAccount::setPassword()
     * @uses GlobalCreditAffordabilityAccount::setLicenseBillingOption()
     * @param string $company
     * @param string $username
     * @param string $password
     * @param string $licenseBillingOption
     */
    public function __construct(?string $company = null, ?string $username = null, ?string $password = null, ?string $licenseBillingOption = null)
    {
        $this
            ->setCompany($company)
            ->setUsername($username)
            ->setPassword($password)
            ->setLicenseBillingOption($licenseBillingOption);
    }
    /**
     * Get Company value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCompany(): ?string
    {
        return isset($this->Company) ? $this->Company : null;
    }
    /**
     * Set Company value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $company
     * @return \ID3Global\Models\GlobalCreditAffordabilityAccount
     */
    public function setCompany(?string $company = null): self
    {
        // validation for constraint: string
        if (!is_null($company) && !is_string($company)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($company, true), gettype($company)), __LINE__);
        }
        if (is_null($company) || (is_array($company) && empty($company))) {
            unset($this->Company);
        } else {
            $this->Company = $company;
        }
        
        return $this;
    }
    /**
     * Get Username value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return isset($this->Username) ? $this->Username : null;
    }
    /**
     * Set Username value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username
     * @return \ID3Global\Models\GlobalCreditAffordabilityAccount
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        if (is_null($username) || (is_array($username) && empty($username))) {
            unset($this->Username);
        } else {
            $this->Username = $username;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\GlobalCreditAffordabilityAccount
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
    /**
     * Get LicenseBillingOption value
     * @return string|null
     */
    public function getLicenseBillingOption(): ?string
    {
        return $this->LicenseBillingOption;
    }
    /**
     * Set LicenseBillingOption value
     * @uses \ID3Global\Enums\GlobalLicenseBillingOption::valueIsValid()
     * @uses \ID3Global\Enums\GlobalLicenseBillingOption::getValidValues()
     * @throws InvalidArgumentException
     * @param string $licenseBillingOption
     * @return \ID3Global\Models\GlobalCreditAffordabilityAccount
     */
    public function setLicenseBillingOption(?string $licenseBillingOption = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalLicenseBillingOption::valueIsValid($licenseBillingOption)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalLicenseBillingOption', is_array($licenseBillingOption) ? implode(', ', $licenseBillingOption) : var_export($licenseBillingOption, true), implode(', ', \ID3Global\Enums\GlobalLicenseBillingOption::getValidValues())), __LINE__);
        }
        $this->LicenseBillingOption = $licenseBillingOption;
        
        return $this;
    }
}
