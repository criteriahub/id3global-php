<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSearchPurposes Models
 * @subpackage Structs
 */
class GetSearchPurposes extends AbstractStructBase
{
    /**
     * The itemcheckID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $itemcheckID = null;
    /**
     * Constructor method for GetSearchPurposes
     * @uses GetSearchPurposes::setItemcheckID()
     * @param int $itemcheckID
     */
    public function __construct(?int $itemcheckID = null)
    {
        $this
            ->setItemcheckID($itemcheckID);
    }
    /**
     * Get itemcheckID value
     * @return int|null
     */
    public function getItemcheckID(): ?int
    {
        return $this->itemcheckID;
    }
    /**
     * Set itemcheckID value
     * @param int $itemcheckID
     * @return \ID3Global\Models\GetSearchPurposes
     */
    public function setItemcheckID(?int $itemcheckID = null): self
    {
        // validation for constraint: int
        if (!is_null($itemcheckID) && !(is_int($itemcheckID) || ctype_digit($itemcheckID))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($itemcheckID, true), gettype($itemcheckID)), __LINE__);
        }
        $this->itemcheckID = $itemcheckID;
        
        return $this;
    }
}
