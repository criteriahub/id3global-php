<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCasesResponse Models
 * @subpackage Structs
 */
class GetCasesResponse extends AbstractStructBase
{
    /**
     * The GetCasesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCases|null
     */
    protected ?\ID3Global\Models\GlobalCases $GetCasesResult = null;
    /**
     * Constructor method for GetCasesResponse
     * @uses GetCasesResponse::setGetCasesResult()
     * @param \ID3Global\Models\GlobalCases $getCasesResult
     */
    public function __construct(?\ID3Global\Models\GlobalCases $getCasesResult = null)
    {
        $this
            ->setGetCasesResult($getCasesResult);
    }
    /**
     * Get GetCasesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCases|null
     */
    public function getGetCasesResult(): ?\ID3Global\Models\GlobalCases
    {
        return isset($this->GetCasesResult) ? $this->GetCasesResult : null;
    }
    /**
     * Set GetCasesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCases $getCasesResult
     * @return \ID3Global\Models\GetCasesResponse
     */
    public function setGetCasesResult(?\ID3Global\Models\GlobalCases $getCasesResult = null): self
    {
        if (is_null($getCasesResult) || (is_array($getCasesResult) && empty($getCasesResult))) {
            unset($this->GetCasesResult);
        } else {
            $this->GetCasesResult = $getCasesResult;
        }
        
        return $this;
    }
}
