<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DownloadExtract Models
 * @subpackage Structs
 */
class DownloadExtract extends AbstractStructBase
{
    /**
     * The orgId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $orgId = null;
    /**
     * The extractId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $extractId = null;
    /**
     * Constructor method for DownloadExtract
     * @uses DownloadExtract::setOrgId()
     * @uses DownloadExtract::setExtractId()
     * @param string $orgId
     * @param string $extractId
     */
    public function __construct(?string $orgId = null, ?string $extractId = null)
    {
        $this
            ->setOrgId($orgId)
            ->setExtractId($extractId);
    }
    /**
     * Get orgId value
     * @return string|null
     */
    public function getOrgId(): ?string
    {
        return $this->orgId;
    }
    /**
     * Set orgId value
     * @param string $orgId
     * @return \ID3Global\Models\DownloadExtract
     */
    public function setOrgId(?string $orgId = null): self
    {
        // validation for constraint: string
        if (!is_null($orgId) && !is_string($orgId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgId, true), gettype($orgId)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgId) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgId, true)), __LINE__);
        }
        $this->orgId = $orgId;
        
        return $this;
    }
    /**
     * Get extractId value
     * @return string|null
     */
    public function getExtractId(): ?string
    {
        return $this->extractId;
    }
    /**
     * Set extractId value
     * @param string $extractId
     * @return \ID3Global\Models\DownloadExtract
     */
    public function setExtractId(?string $extractId = null): self
    {
        // validation for constraint: string
        if (!is_null($extractId) && !is_string($extractId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($extractId, true), gettype($extractId)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($extractId) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $extractId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($extractId, true)), __LINE__);
        }
        $this->extractId = $extractId;
        
        return $this;
    }
}
