<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSupplierAccountDto Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q705:GlobalSupplierAccountDto
 * @subpackage Structs
 */
class GlobalSupplierAccountDto extends GlobalSupplierAccount
{
    /**
     * The Domain1Override
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Domain1Override = null;
    /**
     * The Username1Override
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username1Override = null;
    /**
     * The Password1Override
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password1Override = null;
    /**
     * The Domain2Override
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Domain2Override = null;
    /**
     * The Username2Override
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username2Override = null;
    /**
     * The Password2Override
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password2Override = null;
    /**
     * The Misc1Override
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Misc1Override = null;
    /**
     * The Misc2Override
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Misc2Override = null;
    /**
     * The Misc3Override
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Misc3Override = null;
    /**
     * The Misc4Override
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Misc4Override = null;
    /**
     * Constructor method for GlobalSupplierAccountDto
     * @uses GlobalSupplierAccountDto::setDomain1Override()
     * @uses GlobalSupplierAccountDto::setUsername1Override()
     * @uses GlobalSupplierAccountDto::setPassword1Override()
     * @uses GlobalSupplierAccountDto::setDomain2Override()
     * @uses GlobalSupplierAccountDto::setUsername2Override()
     * @uses GlobalSupplierAccountDto::setPassword2Override()
     * @uses GlobalSupplierAccountDto::setMisc1Override()
     * @uses GlobalSupplierAccountDto::setMisc2Override()
     * @uses GlobalSupplierAccountDto::setMisc3Override()
     * @uses GlobalSupplierAccountDto::setMisc4Override()
     * @param string $domain1Override
     * @param string $username1Override
     * @param string $password1Override
     * @param string $domain2Override
     * @param string $username2Override
     * @param string $password2Override
     * @param string $misc1Override
     * @param string $misc2Override
     * @param string $misc3Override
     * @param string $misc4Override
     */
    public function __construct(?string $domain1Override = null, ?string $username1Override = null, ?string $password1Override = null, ?string $domain2Override = null, ?string $username2Override = null, ?string $password2Override = null, ?string $misc1Override = null, ?string $misc2Override = null, ?string $misc3Override = null, ?string $misc4Override = null)
    {
        $this
            ->setDomain1Override($domain1Override)
            ->setUsername1Override($username1Override)
            ->setPassword1Override($password1Override)
            ->setDomain2Override($domain2Override)
            ->setUsername2Override($username2Override)
            ->setPassword2Override($password2Override)
            ->setMisc1Override($misc1Override)
            ->setMisc2Override($misc2Override)
            ->setMisc3Override($misc3Override)
            ->setMisc4Override($misc4Override);
    }
    /**
     * Get Domain1Override value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDomain1Override(): ?string
    {
        return isset($this->Domain1Override) ? $this->Domain1Override : null;
    }
    /**
     * Set Domain1Override value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $domain1Override
     * @return \ID3Global\Models\GlobalSupplierAccountDto
     */
    public function setDomain1Override(?string $domain1Override = null): self
    {
        // validation for constraint: string
        if (!is_null($domain1Override) && !is_string($domain1Override)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain1Override, true), gettype($domain1Override)), __LINE__);
        }
        if (is_null($domain1Override) || (is_array($domain1Override) && empty($domain1Override))) {
            unset($this->Domain1Override);
        } else {
            $this->Domain1Override = $domain1Override;
        }
        
        return $this;
    }
    /**
     * Get Username1Override value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername1Override(): ?string
    {
        return isset($this->Username1Override) ? $this->Username1Override : null;
    }
    /**
     * Set Username1Override value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username1Override
     * @return \ID3Global\Models\GlobalSupplierAccountDto
     */
    public function setUsername1Override(?string $username1Override = null): self
    {
        // validation for constraint: string
        if (!is_null($username1Override) && !is_string($username1Override)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username1Override, true), gettype($username1Override)), __LINE__);
        }
        if (is_null($username1Override) || (is_array($username1Override) && empty($username1Override))) {
            unset($this->Username1Override);
        } else {
            $this->Username1Override = $username1Override;
        }
        
        return $this;
    }
    /**
     * Get Password1Override value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword1Override(): ?string
    {
        return isset($this->Password1Override) ? $this->Password1Override : null;
    }
    /**
     * Set Password1Override value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password1Override
     * @return \ID3Global\Models\GlobalSupplierAccountDto
     */
    public function setPassword1Override(?string $password1Override = null): self
    {
        // validation for constraint: string
        if (!is_null($password1Override) && !is_string($password1Override)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password1Override, true), gettype($password1Override)), __LINE__);
        }
        if (is_null($password1Override) || (is_array($password1Override) && empty($password1Override))) {
            unset($this->Password1Override);
        } else {
            $this->Password1Override = $password1Override;
        }
        
        return $this;
    }
    /**
     * Get Domain2Override value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDomain2Override(): ?string
    {
        return isset($this->Domain2Override) ? $this->Domain2Override : null;
    }
    /**
     * Set Domain2Override value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $domain2Override
     * @return \ID3Global\Models\GlobalSupplierAccountDto
     */
    public function setDomain2Override(?string $domain2Override = null): self
    {
        // validation for constraint: string
        if (!is_null($domain2Override) && !is_string($domain2Override)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain2Override, true), gettype($domain2Override)), __LINE__);
        }
        if (is_null($domain2Override) || (is_array($domain2Override) && empty($domain2Override))) {
            unset($this->Domain2Override);
        } else {
            $this->Domain2Override = $domain2Override;
        }
        
        return $this;
    }
    /**
     * Get Username2Override value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername2Override(): ?string
    {
        return isset($this->Username2Override) ? $this->Username2Override : null;
    }
    /**
     * Set Username2Override value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username2Override
     * @return \ID3Global\Models\GlobalSupplierAccountDto
     */
    public function setUsername2Override(?string $username2Override = null): self
    {
        // validation for constraint: string
        if (!is_null($username2Override) && !is_string($username2Override)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username2Override, true), gettype($username2Override)), __LINE__);
        }
        if (is_null($username2Override) || (is_array($username2Override) && empty($username2Override))) {
            unset($this->Username2Override);
        } else {
            $this->Username2Override = $username2Override;
        }
        
        return $this;
    }
    /**
     * Get Password2Override value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword2Override(): ?string
    {
        return isset($this->Password2Override) ? $this->Password2Override : null;
    }
    /**
     * Set Password2Override value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password2Override
     * @return \ID3Global\Models\GlobalSupplierAccountDto
     */
    public function setPassword2Override(?string $password2Override = null): self
    {
        // validation for constraint: string
        if (!is_null($password2Override) && !is_string($password2Override)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password2Override, true), gettype($password2Override)), __LINE__);
        }
        if (is_null($password2Override) || (is_array($password2Override) && empty($password2Override))) {
            unset($this->Password2Override);
        } else {
            $this->Password2Override = $password2Override;
        }
        
        return $this;
    }
    /**
     * Get Misc1Override value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMisc1Override(): ?string
    {
        return isset($this->Misc1Override) ? $this->Misc1Override : null;
    }
    /**
     * Set Misc1Override value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $misc1Override
     * @return \ID3Global\Models\GlobalSupplierAccountDto
     */
    public function setMisc1Override(?string $misc1Override = null): self
    {
        // validation for constraint: string
        if (!is_null($misc1Override) && !is_string($misc1Override)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($misc1Override, true), gettype($misc1Override)), __LINE__);
        }
        if (is_null($misc1Override) || (is_array($misc1Override) && empty($misc1Override))) {
            unset($this->Misc1Override);
        } else {
            $this->Misc1Override = $misc1Override;
        }
        
        return $this;
    }
    /**
     * Get Misc2Override value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMisc2Override(): ?string
    {
        return isset($this->Misc2Override) ? $this->Misc2Override : null;
    }
    /**
     * Set Misc2Override value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $misc2Override
     * @return \ID3Global\Models\GlobalSupplierAccountDto
     */
    public function setMisc2Override(?string $misc2Override = null): self
    {
        // validation for constraint: string
        if (!is_null($misc2Override) && !is_string($misc2Override)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($misc2Override, true), gettype($misc2Override)), __LINE__);
        }
        if (is_null($misc2Override) || (is_array($misc2Override) && empty($misc2Override))) {
            unset($this->Misc2Override);
        } else {
            $this->Misc2Override = $misc2Override;
        }
        
        return $this;
    }
    /**
     * Get Misc3Override value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMisc3Override(): ?string
    {
        return isset($this->Misc3Override) ? $this->Misc3Override : null;
    }
    /**
     * Set Misc3Override value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $misc3Override
     * @return \ID3Global\Models\GlobalSupplierAccountDto
     */
    public function setMisc3Override(?string $misc3Override = null): self
    {
        // validation for constraint: string
        if (!is_null($misc3Override) && !is_string($misc3Override)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($misc3Override, true), gettype($misc3Override)), __LINE__);
        }
        if (is_null($misc3Override) || (is_array($misc3Override) && empty($misc3Override))) {
            unset($this->Misc3Override);
        } else {
            $this->Misc3Override = $misc3Override;
        }
        
        return $this;
    }
    /**
     * Get Misc4Override value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMisc4Override(): ?string
    {
        return isset($this->Misc4Override) ? $this->Misc4Override : null;
    }
    /**
     * Set Misc4Override value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $misc4Override
     * @return \ID3Global\Models\GlobalSupplierAccountDto
     */
    public function setMisc4Override(?string $misc4Override = null): self
    {
        // validation for constraint: string
        if (!is_null($misc4Override) && !is_string($misc4Override)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($misc4Override, true), gettype($misc4Override)), __LINE__);
        }
        if (is_null($misc4Override) || (is_array($misc4Override) && empty($misc4Override))) {
            unset($this->Misc4Override);
        } else {
            $this->Misc4Override = $misc4Override;
        }
        
        return $this;
    }
}
