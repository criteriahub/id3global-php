<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPermissionsResponse Models
 * @subpackage Structs
 */
class GetPermissionsResponse extends AbstractStructBase
{
    /**
     * The GetPermissionsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalRolePermission|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalRolePermission $GetPermissionsResult = null;
    /**
     * Constructor method for GetPermissionsResponse
     * @uses GetPermissionsResponse::setGetPermissionsResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalRolePermission $getPermissionsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalRolePermission $getPermissionsResult = null)
    {
        $this
            ->setGetPermissionsResult($getPermissionsResult);
    }
    /**
     * Get GetPermissionsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalRolePermission|null
     */
    public function getGetPermissionsResult(): ?\ID3Global\Arrays\ArrayOfGlobalRolePermission
    {
        return isset($this->GetPermissionsResult) ? $this->GetPermissionsResult : null;
    }
    /**
     * Set GetPermissionsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalRolePermission $getPermissionsResult
     * @return \ID3Global\Models\GetPermissionsResponse
     */
    public function setGetPermissionsResult(?\ID3Global\Arrays\ArrayOfGlobalRolePermission $getPermissionsResult = null): self
    {
        if (is_null($getPermissionsResult) || (is_array($getPermissionsResult) && empty($getPermissionsResult))) {
            unset($this->GetPermissionsResult);
        } else {
            $this->GetPermissionsResult = $getPermissionsResult;
        }
        
        return $this;
    }
}
