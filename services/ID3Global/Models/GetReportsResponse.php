<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetReportsResponse Models
 * @subpackage Structs
 */
class GetReportsResponse extends AbstractStructBase
{
    /**
     * The GetReportsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalReportDetails|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalReportDetails $GetReportsResult = null;
    /**
     * Constructor method for GetReportsResponse
     * @uses GetReportsResponse::setGetReportsResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalReportDetails $getReportsResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalReportDetails $getReportsResult = null)
    {
        $this
            ->setGetReportsResult($getReportsResult);
    }
    /**
     * Get GetReportsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalReportDetails|null
     */
    public function getGetReportsResult(): ?\ID3Global\Arrays\ArrayOfGlobalReportDetails
    {
        return isset($this->GetReportsResult) ? $this->GetReportsResult : null;
    }
    /**
     * Set GetReportsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalReportDetails $getReportsResult
     * @return \ID3Global\Models\GetReportsResponse
     */
    public function setGetReportsResult(?\ID3Global\Arrays\ArrayOfGlobalReportDetails $getReportsResult = null): self
    {
        if (is_null($getReportsResult) || (is_array($getReportsResult) && empty($getReportsResult))) {
            unset($this->GetReportsResult);
        } else {
            $this->GetReportsResult = $getReportsResult;
        }
        
        return $this;
    }
}
