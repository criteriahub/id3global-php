<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateAccount Models
 * @subpackage Structs
 */
class CreateAccount extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The Email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Email = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * The PIN
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $PIN = null;
    /**
     * The PasswordNeverExpires
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $PasswordNeverExpires = null;
    /**
     * The RoleIDs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfguid|null
     */
    protected ?\ID3Global\Arrays\ArrayOfguid $RoleIDs = null;
    /**
     * Constructor method for CreateAccount
     * @uses CreateAccount::setOrgID()
     * @uses CreateAccount::setName()
     * @uses CreateAccount::setUsername()
     * @uses CreateAccount::setEmail()
     * @uses CreateAccount::setPassword()
     * @uses CreateAccount::setPIN()
     * @uses CreateAccount::setPasswordNeverExpires()
     * @uses CreateAccount::setRoleIDs()
     * @param string $orgID
     * @param string $name
     * @param string $username
     * @param string $email
     * @param string $password
     * @param string $pIN
     * @param bool $passwordNeverExpires
     * @param \ID3Global\Arrays\ArrayOfguid $roleIDs
     */
    public function __construct(?string $orgID = null, ?string $name = null, ?string $username = null, ?string $email = null, ?string $password = null, ?string $pIN = null, ?bool $passwordNeverExpires = null, ?\ID3Global\Arrays\ArrayOfguid $roleIDs = null)
    {
        $this
            ->setOrgID($orgID)
            ->setName($name)
            ->setUsername($username)
            ->setEmail($email)
            ->setPassword($password)
            ->setPIN($pIN)
            ->setPasswordNeverExpires($passwordNeverExpires)
            ->setRoleIDs($roleIDs);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\CreateAccount
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\CreateAccount
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Username value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return isset($this->Username) ? $this->Username : null;
    }
    /**
     * Set Username value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username
     * @return \ID3Global\Models\CreateAccount
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        if (is_null($username) || (is_array($username) && empty($username))) {
            unset($this->Username);
        } else {
            $this->Username = $username;
        }
        
        return $this;
    }
    /**
     * Get Email value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return isset($this->Email) ? $this->Email : null;
    }
    /**
     * Set Email value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email
     * @return \ID3Global\Models\CreateAccount
     */
    public function setEmail(?string $email = null): self
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        if (is_null($email) || (is_array($email) && empty($email))) {
            unset($this->Email);
        } else {
            $this->Email = $email;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\CreateAccount
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
    /**
     * Get PIN value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPIN(): ?string
    {
        return isset($this->PIN) ? $this->PIN : null;
    }
    /**
     * Set PIN value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $pIN
     * @return \ID3Global\Models\CreateAccount
     */
    public function setPIN(?string $pIN = null): self
    {
        // validation for constraint: string
        if (!is_null($pIN) && !is_string($pIN)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pIN, true), gettype($pIN)), __LINE__);
        }
        if (is_null($pIN) || (is_array($pIN) && empty($pIN))) {
            unset($this->PIN);
        } else {
            $this->PIN = $pIN;
        }
        
        return $this;
    }
    /**
     * Get PasswordNeverExpires value
     * @return bool|null
     */
    public function getPasswordNeverExpires(): ?bool
    {
        return $this->PasswordNeverExpires;
    }
    /**
     * Set PasswordNeverExpires value
     * @param bool $passwordNeverExpires
     * @return \ID3Global\Models\CreateAccount
     */
    public function setPasswordNeverExpires(?bool $passwordNeverExpires = null): self
    {
        // validation for constraint: boolean
        if (!is_null($passwordNeverExpires) && !is_bool($passwordNeverExpires)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($passwordNeverExpires, true), gettype($passwordNeverExpires)), __LINE__);
        }
        $this->PasswordNeverExpires = $passwordNeverExpires;
        
        return $this;
    }
    /**
     * Get RoleIDs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfguid|null
     */
    public function getRoleIDs(): ?\ID3Global\Arrays\ArrayOfguid
    {
        return isset($this->RoleIDs) ? $this->RoleIDs : null;
    }
    /**
     * Set RoleIDs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfguid $roleIDs
     * @return \ID3Global\Models\CreateAccount
     */
    public function setRoleIDs(?\ID3Global\Arrays\ArrayOfguid $roleIDs = null): self
    {
        if (is_null($roleIDs) || (is_array($roleIDs) && empty($roleIDs))) {
            unset($this->RoleIDs);
        } else {
            $this->RoleIDs = $roleIDs;
        }
        
        return $this;
    }
}
