<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalStatus Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:GlobalStatus
 * @subpackage Structs
 */
class GlobalStatus extends AbstractStructBase
{
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The RequestText
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $RequestText = null;
    /**
     * The ResponseText
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ResponseText = null;
    /**
     * Constructor method for GlobalStatus
     * @uses GlobalStatus::setName()
     * @uses GlobalStatus::setRequestText()
     * @uses GlobalStatus::setResponseText()
     * @param string $name
     * @param string $requestText
     * @param string $responseText
     */
    public function __construct(?string $name = null, ?string $requestText = null, ?string $responseText = null)
    {
        $this
            ->setName($name)
            ->setRequestText($requestText)
            ->setResponseText($responseText);
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalStatus
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get RequestText value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRequestText(): ?string
    {
        return isset($this->RequestText) ? $this->RequestText : null;
    }
    /**
     * Set RequestText value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $requestText
     * @return \ID3Global\Models\GlobalStatus
     */
    public function setRequestText(?string $requestText = null): self
    {
        // validation for constraint: string
        if (!is_null($requestText) && !is_string($requestText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($requestText, true), gettype($requestText)), __LINE__);
        }
        if (is_null($requestText) || (is_array($requestText) && empty($requestText))) {
            unset($this->RequestText);
        } else {
            $this->RequestText = $requestText;
        }
        
        return $this;
    }
    /**
     * Get ResponseText value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getResponseText(): ?string
    {
        return isset($this->ResponseText) ? $this->ResponseText : null;
    }
    /**
     * Set ResponseText value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $responseText
     * @return \ID3Global\Models\GlobalStatus
     */
    public function setResponseText(?string $responseText = null): self
    {
        // validation for constraint: string
        if (!is_null($responseText) && !is_string($responseText)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($responseText, true), gettype($responseText)), __LINE__);
        }
        if (is_null($responseText) || (is_array($responseText) && empty($responseText))) {
            unset($this->ResponseText);
        } else {
            $this->ResponseText = $responseText;
        }
        
        return $this;
    }
}
