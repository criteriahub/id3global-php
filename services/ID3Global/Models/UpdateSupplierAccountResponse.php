<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UpdateSupplierAccountResponse Models
 * @subpackage Structs
 */
class UpdateSupplierAccountResponse extends AbstractStructBase
{
    /**
     * The UpdateSupplierAccountResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSupplierAccount|null
     */
    protected ?\ID3Global\Models\GlobalSupplierAccount $UpdateSupplierAccountResult = null;
    /**
     * Constructor method for UpdateSupplierAccountResponse
     * @uses UpdateSupplierAccountResponse::setUpdateSupplierAccountResult()
     * @param \ID3Global\Models\GlobalSupplierAccount $updateSupplierAccountResult
     */
    public function __construct(?\ID3Global\Models\GlobalSupplierAccount $updateSupplierAccountResult = null)
    {
        $this
            ->setUpdateSupplierAccountResult($updateSupplierAccountResult);
    }
    /**
     * Get UpdateSupplierAccountResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSupplierAccount|null
     */
    public function getUpdateSupplierAccountResult(): ?\ID3Global\Models\GlobalSupplierAccount
    {
        return isset($this->UpdateSupplierAccountResult) ? $this->UpdateSupplierAccountResult : null;
    }
    /**
     * Set UpdateSupplierAccountResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSupplierAccount $updateSupplierAccountResult
     * @return \ID3Global\Models\UpdateSupplierAccountResponse
     */
    public function setUpdateSupplierAccountResult(?\ID3Global\Models\GlobalSupplierAccount $updateSupplierAccountResult = null): self
    {
        if (is_null($updateSupplierAccountResult) || (is_array($updateSupplierAccountResult) && empty($updateSupplierAccountResult))) {
            unset($this->UpdateSupplierAccountResult);
        } else {
            $this->UpdateSupplierAccountResult = $updateSupplierAccountResult;
        }
        
        return $this;
    }
}
