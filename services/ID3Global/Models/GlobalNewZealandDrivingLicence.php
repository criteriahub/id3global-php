<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalNewZealandDrivingLicence Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q361:GlobalNewZealandDrivingLicence
 * @subpackage Structs
 */
class GlobalNewZealandDrivingLicence extends AbstractStructBase
{
    /**
     * The Number
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Number = null;
    /**
     * The Version
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Version = null;
    /**
     * The VehicleRegistration
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $VehicleRegistration = null;
    /**
     * Constructor method for GlobalNewZealandDrivingLicence
     * @uses GlobalNewZealandDrivingLicence::setNumber()
     * @uses GlobalNewZealandDrivingLicence::setVersion()
     * @uses GlobalNewZealandDrivingLicence::setVehicleRegistration()
     * @param string $number
     * @param string $version
     * @param string $vehicleRegistration
     */
    public function __construct(?string $number = null, ?string $version = null, ?string $vehicleRegistration = null)
    {
        $this
            ->setNumber($number)
            ->setVersion($version)
            ->setVehicleRegistration($vehicleRegistration);
    }
    /**
     * Get Number value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return isset($this->Number) ? $this->Number : null;
    }
    /**
     * Set Number value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $number
     * @return \ID3Global\Models\GlobalNewZealandDrivingLicence
     */
    public function setNumber(?string $number = null): self
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($number, true), gettype($number)), __LINE__);
        }
        if (is_null($number) || (is_array($number) && empty($number))) {
            unset($this->Number);
        } else {
            $this->Number = $number;
        }
        
        return $this;
    }
    /**
     * Get Version value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getVersion(): ?string
    {
        return isset($this->Version) ? $this->Version : null;
    }
    /**
     * Set Version value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $version
     * @return \ID3Global\Models\GlobalNewZealandDrivingLicence
     */
    public function setVersion(?string $version = null): self
    {
        // validation for constraint: string
        if (!is_null($version) && !is_string($version)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($version, true), gettype($version)), __LINE__);
        }
        if (is_null($version) || (is_array($version) && empty($version))) {
            unset($this->Version);
        } else {
            $this->Version = $version;
        }
        
        return $this;
    }
    /**
     * Get VehicleRegistration value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getVehicleRegistration(): ?string
    {
        return isset($this->VehicleRegistration) ? $this->VehicleRegistration : null;
    }
    /**
     * Set VehicleRegistration value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $vehicleRegistration
     * @return \ID3Global\Models\GlobalNewZealandDrivingLicence
     */
    public function setVehicleRegistration(?string $vehicleRegistration = null): self
    {
        // validation for constraint: string
        if (!is_null($vehicleRegistration) && !is_string($vehicleRegistration)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($vehicleRegistration, true), gettype($vehicleRegistration)), __LINE__);
        }
        if (is_null($vehicleRegistration) || (is_array($vehicleRegistration) && empty($vehicleRegistration))) {
            unset($this->VehicleRegistration);
        } else {
            $this->VehicleRegistration = $vehicleRegistration;
        }
        
        return $this;
    }
}
