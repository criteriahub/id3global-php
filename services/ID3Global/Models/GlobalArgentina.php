<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalArgentina Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q398:GlobalArgentina
 * @subpackage Structs
 */
class GlobalArgentina extends AbstractStructBase
{
    /**
     * The DNINumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalArgentinaDNINumber|null
     */
    protected ?\ID3Global\Models\GlobalArgentinaDNINumber $DNINumber = null;
    /**
     * The TaxIdentificationNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalArgentinaTaxIdentificationNumber|null
     */
    protected ?\ID3Global\Models\GlobalArgentinaTaxIdentificationNumber $TaxIdentificationNumber = null;
    /**
     * Constructor method for GlobalArgentina
     * @uses GlobalArgentina::setDNINumber()
     * @uses GlobalArgentina::setTaxIdentificationNumber()
     * @param \ID3Global\Models\GlobalArgentinaDNINumber $dNINumber
     * @param \ID3Global\Models\GlobalArgentinaTaxIdentificationNumber $taxIdentificationNumber
     */
    public function __construct(?\ID3Global\Models\GlobalArgentinaDNINumber $dNINumber = null, ?\ID3Global\Models\GlobalArgentinaTaxIdentificationNumber $taxIdentificationNumber = null)
    {
        $this
            ->setDNINumber($dNINumber)
            ->setTaxIdentificationNumber($taxIdentificationNumber);
    }
    /**
     * Get DNINumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalArgentinaDNINumber|null
     */
    public function getDNINumber(): ?\ID3Global\Models\GlobalArgentinaDNINumber
    {
        return isset($this->DNINumber) ? $this->DNINumber : null;
    }
    /**
     * Set DNINumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalArgentinaDNINumber $dNINumber
     * @return \ID3Global\Models\GlobalArgentina
     */
    public function setDNINumber(?\ID3Global\Models\GlobalArgentinaDNINumber $dNINumber = null): self
    {
        if (is_null($dNINumber) || (is_array($dNINumber) && empty($dNINumber))) {
            unset($this->DNINumber);
        } else {
            $this->DNINumber = $dNINumber;
        }
        
        return $this;
    }
    /**
     * Get TaxIdentificationNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalArgentinaTaxIdentificationNumber|null
     */
    public function getTaxIdentificationNumber(): ?\ID3Global\Models\GlobalArgentinaTaxIdentificationNumber
    {
        return isset($this->TaxIdentificationNumber) ? $this->TaxIdentificationNumber : null;
    }
    /**
     * Set TaxIdentificationNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalArgentinaTaxIdentificationNumber $taxIdentificationNumber
     * @return \ID3Global\Models\GlobalArgentina
     */
    public function setTaxIdentificationNumber(?\ID3Global\Models\GlobalArgentinaTaxIdentificationNumber $taxIdentificationNumber = null): self
    {
        if (is_null($taxIdentificationNumber) || (is_array($taxIdentificationNumber) && empty($taxIdentificationNumber))) {
            unset($this->TaxIdentificationNumber);
        } else {
            $this->TaxIdentificationNumber = $taxIdentificationNumber;
        }
        
        return $this;
    }
}
