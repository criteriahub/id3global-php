<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalScoring Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q242:GlobalScoring
 * @subpackage Structs
 */
class GlobalScoring extends AbstractStructBase
{
    /**
     * The Comment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalWeighting|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalWeighting $Comment = null;
    /**
     * The Match
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalWeighting|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalWeighting $Match = null;
    /**
     * The Warning
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalWeighting|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalWeighting $Warning = null;
    /**
     * The Mismatch
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalWeighting|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalWeighting $Mismatch = null;
    /**
     * Constructor method for GlobalScoring
     * @uses GlobalScoring::setComment()
     * @uses GlobalScoring::setMatch()
     * @uses GlobalScoring::setWarning()
     * @uses GlobalScoring::setMismatch()
     * @param \ID3Global\Arrays\ArrayOfGlobalWeighting $comment
     * @param \ID3Global\Arrays\ArrayOfGlobalWeighting $match
     * @param \ID3Global\Arrays\ArrayOfGlobalWeighting $warning
     * @param \ID3Global\Arrays\ArrayOfGlobalWeighting $mismatch
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalWeighting $comment = null, ?\ID3Global\Arrays\ArrayOfGlobalWeighting $match = null, ?\ID3Global\Arrays\ArrayOfGlobalWeighting $warning = null, ?\ID3Global\Arrays\ArrayOfGlobalWeighting $mismatch = null)
    {
        $this
            ->setComment($comment)
            ->setMatch($match)
            ->setWarning($warning)
            ->setMismatch($mismatch);
    }
    /**
     * Get Comment value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalWeighting|null
     */
    public function getComment(): ?\ID3Global\Arrays\ArrayOfGlobalWeighting
    {
        return isset($this->Comment) ? $this->Comment : null;
    }
    /**
     * Set Comment value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalWeighting $comment
     * @return \ID3Global\Models\GlobalScoring
     */
    public function setComment(?\ID3Global\Arrays\ArrayOfGlobalWeighting $comment = null): self
    {
        if (is_null($comment) || (is_array($comment) && empty($comment))) {
            unset($this->Comment);
        } else {
            $this->Comment = $comment;
        }
        
        return $this;
    }
    /**
     * Get Match value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalWeighting|null
     */
    public function getMatch(): ?\ID3Global\Arrays\ArrayOfGlobalWeighting
    {
        return isset($this->Match) ? $this->Match : null;
    }
    /**
     * Set Match value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalWeighting $match
     * @return \ID3Global\Models\GlobalScoring
     */
    public function setMatch(?\ID3Global\Arrays\ArrayOfGlobalWeighting $match = null): self
    {
        if (is_null($match) || (is_array($match) && empty($match))) {
            unset($this->Match);
        } else {
            $this->Match = $match;
        }
        
        return $this;
    }
    /**
     * Get Warning value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalWeighting|null
     */
    public function getWarning(): ?\ID3Global\Arrays\ArrayOfGlobalWeighting
    {
        return isset($this->Warning) ? $this->Warning : null;
    }
    /**
     * Set Warning value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalWeighting $warning
     * @return \ID3Global\Models\GlobalScoring
     */
    public function setWarning(?\ID3Global\Arrays\ArrayOfGlobalWeighting $warning = null): self
    {
        if (is_null($warning) || (is_array($warning) && empty($warning))) {
            unset($this->Warning);
        } else {
            $this->Warning = $warning;
        }
        
        return $this;
    }
    /**
     * Get Mismatch value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalWeighting|null
     */
    public function getMismatch(): ?\ID3Global\Arrays\ArrayOfGlobalWeighting
    {
        return isset($this->Mismatch) ? $this->Mismatch : null;
    }
    /**
     * Set Mismatch value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalWeighting $mismatch
     * @return \ID3Global\Models\GlobalScoring
     */
    public function setMismatch(?\ID3Global\Arrays\ArrayOfGlobalWeighting $mismatch = null): self
    {
        if (is_null($mismatch) || (is_array($mismatch) && empty($mismatch))) {
            unset($this->Mismatch);
        } else {
            $this->Mismatch = $mismatch;
        }
        
        return $this;
    }
}
