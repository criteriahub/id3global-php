<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RetrieveDispatch Models
 * @subpackage Structs
 */
class RetrieveDispatch extends AbstractStructBase
{
    /**
     * The Source
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Source = null;
    /**
     * The Service
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Service = null;
    /**
     * Constructor method for RetrieveDispatch
     * @uses RetrieveDispatch::setSource()
     * @uses RetrieveDispatch::setService()
     * @param string $source
     * @param string $service
     */
    public function __construct(?string $source = null, ?string $service = null)
    {
        $this
            ->setSource($source)
            ->setService($service);
    }
    /**
     * Get Source value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSource(): ?string
    {
        return isset($this->Source) ? $this->Source : null;
    }
    /**
     * Set Source value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $source
     * @return \ID3Global\Models\RetrieveDispatch
     */
    public function setSource(?string $source = null): self
    {
        // validation for constraint: string
        if (!is_null($source) && !is_string($source)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($source, true), gettype($source)), __LINE__);
        }
        if (is_null($source) || (is_array($source) && empty($source))) {
            unset($this->Source);
        } else {
            $this->Source = $source;
        }
        
        return $this;
    }
    /**
     * Get Service value
     * @return string|null
     */
    public function getService(): ?string
    {
        return $this->Service;
    }
    /**
     * Set Service value
     * @uses \ID3Global\Enums\GlobalCaseDispatchServiceType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalCaseDispatchServiceType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $service
     * @return \ID3Global\Models\RetrieveDispatch
     */
    public function setService(?string $service = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalCaseDispatchServiceType::valueIsValid($service)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalCaseDispatchServiceType', is_array($service) ? implode(', ', $service) : var_export($service, true), implode(', ', \ID3Global\Enums\GlobalCaseDispatchServiceType::getValidValues())), __LINE__);
        }
        $this->Service = $service;
        
        return $this;
    }
}
