<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCIFASResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q63:GlobalCIFASResultCodes
 * @subpackage Structs
 */
class GlobalCIFASResultCodes extends GlobalItemCheckResultCodes
{
    /**
     * The CIFASMatches
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCIFASMatch|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCIFASMatch $CIFASMatches = null;
    /**
     * Constructor method for GlobalCIFASResultCodes
     * @uses GlobalCIFASResultCodes::setCIFASMatches()
     * @param \ID3Global\Arrays\ArrayOfGlobalCIFASMatch $cIFASMatches
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCIFASMatch $cIFASMatches = null)
    {
        $this
            ->setCIFASMatches($cIFASMatches);
    }
    /**
     * Get CIFASMatches value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCIFASMatch|null
     */
    public function getCIFASMatches(): ?\ID3Global\Arrays\ArrayOfGlobalCIFASMatch
    {
        return isset($this->CIFASMatches) ? $this->CIFASMatches : null;
    }
    /**
     * Set CIFASMatches value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCIFASMatch $cIFASMatches
     * @return \ID3Global\Models\GlobalCIFASResultCodes
     */
    public function setCIFASMatches(?\ID3Global\Arrays\ArrayOfGlobalCIFASMatch $cIFASMatches = null): self
    {
        if (is_null($cIFASMatches) || (is_array($cIFASMatches) && empty($cIFASMatches))) {
            unset($this->CIFASMatches);
        } else {
            $this->CIFASMatches = $cIFASMatches;
        }
        
        return $this;
    }
}
