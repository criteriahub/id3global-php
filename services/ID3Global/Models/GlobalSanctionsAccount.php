<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSanctionsAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q677:GlobalSanctionsAccount
 * @subpackage Structs
 */
class GlobalSanctionsAccount extends GlobalSupplierAccount
{
    /**
     * The AccountID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccountID = null;
    /**
     * The AccountingCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccountingCode = null;
    /**
     * Constructor method for GlobalSanctionsAccount
     * @uses GlobalSanctionsAccount::setAccountID()
     * @uses GlobalSanctionsAccount::setAccountingCode()
     * @param string $accountID
     * @param string $accountingCode
     */
    public function __construct(?string $accountID = null, ?string $accountingCode = null)
    {
        $this
            ->setAccountID($accountID)
            ->setAccountingCode($accountingCode);
    }
    /**
     * Get AccountID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccountID(): ?string
    {
        return isset($this->AccountID) ? $this->AccountID : null;
    }
    /**
     * Set AccountID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accountID
     * @return \ID3Global\Models\GlobalSanctionsAccount
     */
    public function setAccountID(?string $accountID = null): self
    {
        // validation for constraint: string
        if (!is_null($accountID) && !is_string($accountID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountID, true), gettype($accountID)), __LINE__);
        }
        if (is_null($accountID) || (is_array($accountID) && empty($accountID))) {
            unset($this->AccountID);
        } else {
            $this->AccountID = $accountID;
        }
        
        return $this;
    }
    /**
     * Get AccountingCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccountingCode(): ?string
    {
        return isset($this->AccountingCode) ? $this->AccountingCode : null;
    }
    /**
     * Set AccountingCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accountingCode
     * @return \ID3Global\Models\GlobalSanctionsAccount
     */
    public function setAccountingCode(?string $accountingCode = null): self
    {
        // validation for constraint: string
        if (!is_null($accountingCode) && !is_string($accountingCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountingCode, true), gettype($accountingCode)), __LINE__);
        }
        if (is_null($accountingCode) || (is_array($accountingCode) && empty($accountingCode))) {
            unset($this->AccountingCode);
        } else {
            $this->AccountingCode = $accountingCode;
        }
        
        return $this;
    }
}
