<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalLandTelephone Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q410:GlobalLandTelephone
 * @subpackage Structs
 */
class GlobalLandTelephone extends AbstractStructBase
{
    /**
     * The ExDirectory
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool|null
     */
    protected ?bool $ExDirectory = null;
    /**
     * The Number
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Number = null;
    /**
     * Constructor method for GlobalLandTelephone
     * @uses GlobalLandTelephone::setExDirectory()
     * @uses GlobalLandTelephone::setNumber()
     * @param bool $exDirectory
     * @param string $number
     */
    public function __construct(?bool $exDirectory = null, ?string $number = null)
    {
        $this
            ->setExDirectory($exDirectory)
            ->setNumber($number);
    }
    /**
     * Get ExDirectory value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getExDirectory(): ?bool
    {
        return isset($this->ExDirectory) ? $this->ExDirectory : null;
    }
    /**
     * Set ExDirectory value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $exDirectory
     * @return \ID3Global\Models\GlobalLandTelephone
     */
    public function setExDirectory(?bool $exDirectory = null): self
    {
        // validation for constraint: boolean
        if (!is_null($exDirectory) && !is_bool($exDirectory)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($exDirectory, true), gettype($exDirectory)), __LINE__);
        }
        if (is_null($exDirectory) || (is_array($exDirectory) && empty($exDirectory))) {
            unset($this->ExDirectory);
        } else {
            $this->ExDirectory = $exDirectory;
        }
        
        return $this;
    }
    /**
     * Get Number value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return isset($this->Number) ? $this->Number : null;
    }
    /**
     * Set Number value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $number
     * @return \ID3Global\Models\GlobalLandTelephone
     */
    public function setNumber(?string $number = null): self
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($number, true), gettype($number)), __LINE__);
        }
        if (is_null($number) || (is_array($number) && empty($number))) {
            unset($this->Number);
        } else {
            $this->Number = $number;
        }
        
        return $this;
    }
}
