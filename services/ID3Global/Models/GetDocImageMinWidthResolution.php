<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDocImageMinWidthResolution Models
 * @subpackage Structs
 */
class GetDocImageMinWidthResolution extends AbstractStructBase
{
}
