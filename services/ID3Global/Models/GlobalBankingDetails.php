<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalBankingDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q423:GlobalBankingDetails
 * @subpackage Structs
 */
class GlobalBankingDetails extends AbstractStructBase
{
    /**
     * The BankAccount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalBankAccount|null
     */
    protected ?\ID3Global\Models\GlobalBankAccount $BankAccount = null;
    /**
     * The CreditDebitCard
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalCreditDebitCard|null
     */
    protected ?\ID3Global\Models\GlobalCreditDebitCard $CreditDebitCard = null;
    /**
     * The China
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalChinaBank|null
     */
    protected ?\ID3Global\Models\GlobalChinaBank $China = null;
    /**
     * Constructor method for GlobalBankingDetails
     * @uses GlobalBankingDetails::setBankAccount()
     * @uses GlobalBankingDetails::setCreditDebitCard()
     * @uses GlobalBankingDetails::setChina()
     * @param \ID3Global\Models\GlobalBankAccount $bankAccount
     * @param \ID3Global\Models\GlobalCreditDebitCard $creditDebitCard
     * @param \ID3Global\Models\GlobalChinaBank $china
     */
    public function __construct(?\ID3Global\Models\GlobalBankAccount $bankAccount = null, ?\ID3Global\Models\GlobalCreditDebitCard $creditDebitCard = null, ?\ID3Global\Models\GlobalChinaBank $china = null)
    {
        $this
            ->setBankAccount($bankAccount)
            ->setCreditDebitCard($creditDebitCard)
            ->setChina($china);
    }
    /**
     * Get BankAccount value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalBankAccount|null
     */
    public function getBankAccount(): ?\ID3Global\Models\GlobalBankAccount
    {
        return isset($this->BankAccount) ? $this->BankAccount : null;
    }
    /**
     * Set BankAccount value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalBankAccount $bankAccount
     * @return \ID3Global\Models\GlobalBankingDetails
     */
    public function setBankAccount(?\ID3Global\Models\GlobalBankAccount $bankAccount = null): self
    {
        if (is_null($bankAccount) || (is_array($bankAccount) && empty($bankAccount))) {
            unset($this->BankAccount);
        } else {
            $this->BankAccount = $bankAccount;
        }
        
        return $this;
    }
    /**
     * Get CreditDebitCard value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalCreditDebitCard|null
     */
    public function getCreditDebitCard(): ?\ID3Global\Models\GlobalCreditDebitCard
    {
        return isset($this->CreditDebitCard) ? $this->CreditDebitCard : null;
    }
    /**
     * Set CreditDebitCard value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalCreditDebitCard $creditDebitCard
     * @return \ID3Global\Models\GlobalBankingDetails
     */
    public function setCreditDebitCard(?\ID3Global\Models\GlobalCreditDebitCard $creditDebitCard = null): self
    {
        if (is_null($creditDebitCard) || (is_array($creditDebitCard) && empty($creditDebitCard))) {
            unset($this->CreditDebitCard);
        } else {
            $this->CreditDebitCard = $creditDebitCard;
        }
        
        return $this;
    }
    /**
     * Get China value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalChinaBank|null
     */
    public function getChina(): ?\ID3Global\Models\GlobalChinaBank
    {
        return isset($this->China) ? $this->China : null;
    }
    /**
     * Set China value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalChinaBank $china
     * @return \ID3Global\Models\GlobalBankingDetails
     */
    public function setChina(?\ID3Global\Models\GlobalChinaBank $china = null): self
    {
        if (is_null($china) || (is_array($china) && empty($china))) {
            unset($this->China);
        } else {
            $this->China = $china;
        }
        
        return $this;
    }
}
