<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalIPVerificationAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q664:GlobalIPVerificationAccount
 * @subpackage Structs
 */
class GlobalIPVerificationAccount extends GlobalSupplierAccount
{
    /**
     * The LicenceKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $LicenceKey = null;
    /**
     * Constructor method for GlobalIPVerificationAccount
     * @uses GlobalIPVerificationAccount::setLicenceKey()
     * @param string $licenceKey
     */
    public function __construct(?string $licenceKey = null)
    {
        $this
            ->setLicenceKey($licenceKey);
    }
    /**
     * Get LicenceKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLicenceKey(): ?string
    {
        return isset($this->LicenceKey) ? $this->LicenceKey : null;
    }
    /**
     * Set LicenceKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $licenceKey
     * @return \ID3Global\Models\GlobalIPVerificationAccount
     */
    public function setLicenceKey(?string $licenceKey = null): self
    {
        // validation for constraint: string
        if (!is_null($licenceKey) && !is_string($licenceKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($licenceKey, true), gettype($licenceKey)), __LINE__);
        }
        if (is_null($licenceKey) || (is_array($licenceKey) && empty($licenceKey))) {
            unset($this->LicenceKey);
        } else {
            $this->LicenceKey = $licenceKey;
        }
        
        return $this;
    }
}
