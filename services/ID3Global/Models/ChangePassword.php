<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ChangePassword Models
 * @subpackage Structs
 */
class ChangePassword extends AbstractStructBase
{
    /**
     * The AccountName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccountName = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * The PINSequence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $PINSequence = null;
    /**
     * The NewPassword
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $NewPassword = null;
    /**
     * Constructor method for ChangePassword
     * @uses ChangePassword::setAccountName()
     * @uses ChangePassword::setPassword()
     * @uses ChangePassword::setPINSequence()
     * @uses ChangePassword::setNewPassword()
     * @param string $accountName
     * @param string $password
     * @param string $pINSequence
     * @param string $newPassword
     */
    public function __construct(?string $accountName = null, ?string $password = null, ?string $pINSequence = null, ?string $newPassword = null)
    {
        $this
            ->setAccountName($accountName)
            ->setPassword($password)
            ->setPINSequence($pINSequence)
            ->setNewPassword($newPassword);
    }
    /**
     * Get AccountName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccountName(): ?string
    {
        return isset($this->AccountName) ? $this->AccountName : null;
    }
    /**
     * Set AccountName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accountName
     * @return \ID3Global\Models\ChangePassword
     */
    public function setAccountName(?string $accountName = null): self
    {
        // validation for constraint: string
        if (!is_null($accountName) && !is_string($accountName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountName, true), gettype($accountName)), __LINE__);
        }
        if (is_null($accountName) || (is_array($accountName) && empty($accountName))) {
            unset($this->AccountName);
        } else {
            $this->AccountName = $accountName;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\ChangePassword
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
    /**
     * Get PINSequence value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPINSequence(): ?string
    {
        return isset($this->PINSequence) ? $this->PINSequence : null;
    }
    /**
     * Set PINSequence value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $pINSequence
     * @return \ID3Global\Models\ChangePassword
     */
    public function setPINSequence(?string $pINSequence = null): self
    {
        // validation for constraint: string
        if (!is_null($pINSequence) && !is_string($pINSequence)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pINSequence, true), gettype($pINSequence)), __LINE__);
        }
        if (is_null($pINSequence) || (is_array($pINSequence) && empty($pINSequence))) {
            unset($this->PINSequence);
        } else {
            $this->PINSequence = $pINSequence;
        }
        
        return $this;
    }
    /**
     * Get NewPassword value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNewPassword(): ?string
    {
        return isset($this->NewPassword) ? $this->NewPassword : null;
    }
    /**
     * Set NewPassword value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $newPassword
     * @return \ID3Global\Models\ChangePassword
     */
    public function setNewPassword(?string $newPassword = null): self
    {
        // validation for constraint: string
        if (!is_null($newPassword) && !is_string($newPassword)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($newPassword, true), gettype($newPassword)), __LINE__);
        }
        if (is_null($newPassword) || (is_array($newPassword) && empty($newPassword))) {
            unset($this->NewPassword);
        } else {
            $this->NewPassword = $newPassword;
        }
        
        return $this;
    }
}
