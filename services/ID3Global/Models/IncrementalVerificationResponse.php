<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for IncrementalVerificationResponse Models
 * @subpackage Structs
 */
class IncrementalVerificationResponse extends AbstractStructBase
{
    /**
     * The IncrementalVerificationResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalResultData|null
     */
    protected ?\ID3Global\Models\GlobalResultData $IncrementalVerificationResult = null;
    /**
     * Constructor method for IncrementalVerificationResponse
     * @uses IncrementalVerificationResponse::setIncrementalVerificationResult()
     * @param \ID3Global\Models\GlobalResultData $incrementalVerificationResult
     */
    public function __construct(?\ID3Global\Models\GlobalResultData $incrementalVerificationResult = null)
    {
        $this
            ->setIncrementalVerificationResult($incrementalVerificationResult);
    }
    /**
     * Get IncrementalVerificationResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalResultData|null
     */
    public function getIncrementalVerificationResult(): ?\ID3Global\Models\GlobalResultData
    {
        return isset($this->IncrementalVerificationResult) ? $this->IncrementalVerificationResult : null;
    }
    /**
     * Set IncrementalVerificationResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalResultData $incrementalVerificationResult
     * @return \ID3Global\Models\IncrementalVerificationResponse
     */
    public function setIncrementalVerificationResult(?\ID3Global\Models\GlobalResultData $incrementalVerificationResult = null): self
    {
        if (is_null($incrementalVerificationResult) || (is_array($incrementalVerificationResult) && empty($incrementalVerificationResult))) {
            unset($this->IncrementalVerificationResult);
        } else {
            $this->IncrementalVerificationResult = $incrementalVerificationResult;
        }
        
        return $this;
    }
}
