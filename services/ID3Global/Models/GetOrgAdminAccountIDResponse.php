<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetOrgAdminAccountIDResponse Models
 * @subpackage Structs
 */
class GetOrgAdminAccountIDResponse extends AbstractStructBase
{
    /**
     * The GetOrgAdminAccountIDResult
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $GetOrgAdminAccountIDResult = null;
    /**
     * Constructor method for GetOrgAdminAccountIDResponse
     * @uses GetOrgAdminAccountIDResponse::setGetOrgAdminAccountIDResult()
     * @param string $getOrgAdminAccountIDResult
     */
    public function __construct(?string $getOrgAdminAccountIDResult = null)
    {
        $this
            ->setGetOrgAdminAccountIDResult($getOrgAdminAccountIDResult);
    }
    /**
     * Get GetOrgAdminAccountIDResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getGetOrgAdminAccountIDResult(): ?string
    {
        return isset($this->GetOrgAdminAccountIDResult) ? $this->GetOrgAdminAccountIDResult : null;
    }
    /**
     * Set GetOrgAdminAccountIDResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $getOrgAdminAccountIDResult
     * @return \ID3Global\Models\GetOrgAdminAccountIDResponse
     */
    public function setGetOrgAdminAccountIDResult(?string $getOrgAdminAccountIDResult = null): self
    {
        // validation for constraint: string
        if (!is_null($getOrgAdminAccountIDResult) && !is_string($getOrgAdminAccountIDResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($getOrgAdminAccountIDResult, true), gettype($getOrgAdminAccountIDResult)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($getOrgAdminAccountIDResult) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $getOrgAdminAccountIDResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($getOrgAdminAccountIDResult, true)), __LINE__);
        }
        if (is_null($getOrgAdminAccountIDResult) || (is_array($getOrgAdminAccountIDResult) && empty($getOrgAdminAccountIDResult))) {
            unset($this->GetOrgAdminAccountIDResult);
        } else {
            $this->GetOrgAdminAccountIDResult = $getOrgAdminAccountIDResult;
        }
        
        return $this;
    }
}
