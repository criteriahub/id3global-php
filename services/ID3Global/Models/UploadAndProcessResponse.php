<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for UploadAndProcessResponse Models
 * @subpackage Structs
 */
class UploadAndProcessResponse extends AbstractStructBase
{
    /**
     * The UploadAndProcessResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalDIVData|null
     */
    protected ?\ID3Global\Models\GlobalDIVData $UploadAndProcessResult = null;
    /**
     * Constructor method for UploadAndProcessResponse
     * @uses UploadAndProcessResponse::setUploadAndProcessResult()
     * @param \ID3Global\Models\GlobalDIVData $uploadAndProcessResult
     */
    public function __construct(?\ID3Global\Models\GlobalDIVData $uploadAndProcessResult = null)
    {
        $this
            ->setUploadAndProcessResult($uploadAndProcessResult);
    }
    /**
     * Get UploadAndProcessResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalDIVData|null
     */
    public function getUploadAndProcessResult(): ?\ID3Global\Models\GlobalDIVData
    {
        return isset($this->UploadAndProcessResult) ? $this->UploadAndProcessResult : null;
    }
    /**
     * Set UploadAndProcessResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalDIVData $uploadAndProcessResult
     * @return \ID3Global\Models\UploadAndProcessResponse
     */
    public function setUploadAndProcessResult(?\ID3Global\Models\GlobalDIVData $uploadAndProcessResult = null): self
    {
        if (is_null($uploadAndProcessResult) || (is_array($uploadAndProcessResult) && empty($uploadAndProcessResult))) {
            unset($this->UploadAndProcessResult);
        } else {
            $this->UploadAndProcessResult = $uploadAndProcessResult;
        }
        
        return $this;
    }
}
