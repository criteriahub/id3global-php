<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetItalyProvinceOfBirthResponse Models
 * @subpackage Structs
 */
class GetItalyProvinceOfBirthResponse extends AbstractStructBase
{
    /**
     * The GetItalyProvinceOfBirthResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalProvince|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalProvince $GetItalyProvinceOfBirthResult = null;
    /**
     * Constructor method for GetItalyProvinceOfBirthResponse
     * @uses GetItalyProvinceOfBirthResponse::setGetItalyProvinceOfBirthResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalProvince $getItalyProvinceOfBirthResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalProvince $getItalyProvinceOfBirthResult = null)
    {
        $this
            ->setGetItalyProvinceOfBirthResult($getItalyProvinceOfBirthResult);
    }
    /**
     * Get GetItalyProvinceOfBirthResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalProvince|null
     */
    public function getGetItalyProvinceOfBirthResult(): ?\ID3Global\Arrays\ArrayOfGlobalProvince
    {
        return isset($this->GetItalyProvinceOfBirthResult) ? $this->GetItalyProvinceOfBirthResult : null;
    }
    /**
     * Set GetItalyProvinceOfBirthResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalProvince $getItalyProvinceOfBirthResult
     * @return \ID3Global\Models\GetItalyProvinceOfBirthResponse
     */
    public function setGetItalyProvinceOfBirthResult(?\ID3Global\Arrays\ArrayOfGlobalProvince $getItalyProvinceOfBirthResult = null): self
    {
        if (is_null($getItalyProvinceOfBirthResult) || (is_array($getItalyProvinceOfBirthResult) && empty($getItalyProvinceOfBirthResult))) {
            unset($this->GetItalyProvinceOfBirthResult);
        } else {
            $this->GetItalyProvinceOfBirthResult = $getItalyProvinceOfBirthResult;
        }
        
        return $this;
    }
}
