<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseDocumentResultParameters Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q872:GlobalCaseDocumentResultParameters
 * @subpackage Structs
 */
class GlobalCaseDocumentResultParameters extends GlobalCaseResultParameters
{
    /**
     * The RequireValidationTypes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    protected ?\ID3Global\Arrays\ArrayOfunsignedInt $RequireValidationTypes = null;
    /**
     * The MatchItemPassKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MatchItemPassKey = null;
    /**
     * The MatchItemWarningKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MatchItemWarningKey = null;
    /**
     * The MatchItemFailKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MatchItemFailKey = null;
    /**
     * Constructor method for GlobalCaseDocumentResultParameters
     * @uses GlobalCaseDocumentResultParameters::setRequireValidationTypes()
     * @uses GlobalCaseDocumentResultParameters::setMatchItemPassKey()
     * @uses GlobalCaseDocumentResultParameters::setMatchItemWarningKey()
     * @uses GlobalCaseDocumentResultParameters::setMatchItemFailKey()
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $requireValidationTypes
     * @param string $matchItemPassKey
     * @param string $matchItemWarningKey
     * @param string $matchItemFailKey
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfunsignedInt $requireValidationTypes = null, ?string $matchItemPassKey = null, ?string $matchItemWarningKey = null, ?string $matchItemFailKey = null)
    {
        $this
            ->setRequireValidationTypes($requireValidationTypes)
            ->setMatchItemPassKey($matchItemPassKey)
            ->setMatchItemWarningKey($matchItemWarningKey)
            ->setMatchItemFailKey($matchItemFailKey);
    }
    /**
     * Get RequireValidationTypes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfunsignedInt|null
     */
    public function getRequireValidationTypes(): ?\ID3Global\Arrays\ArrayOfunsignedInt
    {
        return isset($this->RequireValidationTypes) ? $this->RequireValidationTypes : null;
    }
    /**
     * Set RequireValidationTypes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfunsignedInt $requireValidationTypes
     * @return \ID3Global\Models\GlobalCaseDocumentResultParameters
     */
    public function setRequireValidationTypes(?\ID3Global\Arrays\ArrayOfunsignedInt $requireValidationTypes = null): self
    {
        if (is_null($requireValidationTypes) || (is_array($requireValidationTypes) && empty($requireValidationTypes))) {
            unset($this->RequireValidationTypes);
        } else {
            $this->RequireValidationTypes = $requireValidationTypes;
        }
        
        return $this;
    }
    /**
     * Get MatchItemPassKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMatchItemPassKey(): ?string
    {
        return isset($this->MatchItemPassKey) ? $this->MatchItemPassKey : null;
    }
    /**
     * Set MatchItemPassKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $matchItemPassKey
     * @return \ID3Global\Models\GlobalCaseDocumentResultParameters
     */
    public function setMatchItemPassKey(?string $matchItemPassKey = null): self
    {
        // validation for constraint: string
        if (!is_null($matchItemPassKey) && !is_string($matchItemPassKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($matchItemPassKey, true), gettype($matchItemPassKey)), __LINE__);
        }
        if (is_null($matchItemPassKey) || (is_array($matchItemPassKey) && empty($matchItemPassKey))) {
            unset($this->MatchItemPassKey);
        } else {
            $this->MatchItemPassKey = $matchItemPassKey;
        }
        
        return $this;
    }
    /**
     * Get MatchItemWarningKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMatchItemWarningKey(): ?string
    {
        return isset($this->MatchItemWarningKey) ? $this->MatchItemWarningKey : null;
    }
    /**
     * Set MatchItemWarningKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $matchItemWarningKey
     * @return \ID3Global\Models\GlobalCaseDocumentResultParameters
     */
    public function setMatchItemWarningKey(?string $matchItemWarningKey = null): self
    {
        // validation for constraint: string
        if (!is_null($matchItemWarningKey) && !is_string($matchItemWarningKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($matchItemWarningKey, true), gettype($matchItemWarningKey)), __LINE__);
        }
        if (is_null($matchItemWarningKey) || (is_array($matchItemWarningKey) && empty($matchItemWarningKey))) {
            unset($this->MatchItemWarningKey);
        } else {
            $this->MatchItemWarningKey = $matchItemWarningKey;
        }
        
        return $this;
    }
    /**
     * Get MatchItemFailKey value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMatchItemFailKey(): ?string
    {
        return isset($this->MatchItemFailKey) ? $this->MatchItemFailKey : null;
    }
    /**
     * Set MatchItemFailKey value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $matchItemFailKey
     * @return \ID3Global\Models\GlobalCaseDocumentResultParameters
     */
    public function setMatchItemFailKey(?string $matchItemFailKey = null): self
    {
        // validation for constraint: string
        if (!is_null($matchItemFailKey) && !is_string($matchItemFailKey)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($matchItemFailKey, true), gettype($matchItemFailKey)), __LINE__);
        }
        if (is_null($matchItemFailKey) || (is_array($matchItemFailKey) && empty($matchItemFailKey))) {
            unset($this->MatchItemFailKey);
        } else {
            $this->MatchItemFailKey = $matchItemFailKey;
        }
        
        return $this;
    }
}
