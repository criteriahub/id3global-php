<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSouthAfricaAccount Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q668:GlobalSouthAfricaAccount
 * @subpackage Structs
 */
class GlobalSouthAfricaAccount extends GlobalSupplierAccount
{
    /**
     * The SubscriberCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SubscriberCode = null;
    /**
     * The SecurityCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SecurityCode = null;
    /**
     * The EnquirerContactName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $EnquirerContactName = null;
    /**
     * The EnquirerContactPhoneNo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $EnquirerContactPhoneNo = null;
    /**
     * Constructor method for GlobalSouthAfricaAccount
     * @uses GlobalSouthAfricaAccount::setSubscriberCode()
     * @uses GlobalSouthAfricaAccount::setSecurityCode()
     * @uses GlobalSouthAfricaAccount::setEnquirerContactName()
     * @uses GlobalSouthAfricaAccount::setEnquirerContactPhoneNo()
     * @param string $subscriberCode
     * @param string $securityCode
     * @param string $enquirerContactName
     * @param string $enquirerContactPhoneNo
     */
    public function __construct(?string $subscriberCode = null, ?string $securityCode = null, ?string $enquirerContactName = null, ?string $enquirerContactPhoneNo = null)
    {
        $this
            ->setSubscriberCode($subscriberCode)
            ->setSecurityCode($securityCode)
            ->setEnquirerContactName($enquirerContactName)
            ->setEnquirerContactPhoneNo($enquirerContactPhoneNo);
    }
    /**
     * Get SubscriberCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSubscriberCode(): ?string
    {
        return isset($this->SubscriberCode) ? $this->SubscriberCode : null;
    }
    /**
     * Set SubscriberCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $subscriberCode
     * @return \ID3Global\Models\GlobalSouthAfricaAccount
     */
    public function setSubscriberCode(?string $subscriberCode = null): self
    {
        // validation for constraint: string
        if (!is_null($subscriberCode) && !is_string($subscriberCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subscriberCode, true), gettype($subscriberCode)), __LINE__);
        }
        if (is_null($subscriberCode) || (is_array($subscriberCode) && empty($subscriberCode))) {
            unset($this->SubscriberCode);
        } else {
            $this->SubscriberCode = $subscriberCode;
        }
        
        return $this;
    }
    /**
     * Get SecurityCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSecurityCode(): ?string
    {
        return isset($this->SecurityCode) ? $this->SecurityCode : null;
    }
    /**
     * Set SecurityCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $securityCode
     * @return \ID3Global\Models\GlobalSouthAfricaAccount
     */
    public function setSecurityCode(?string $securityCode = null): self
    {
        // validation for constraint: string
        if (!is_null($securityCode) && !is_string($securityCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($securityCode, true), gettype($securityCode)), __LINE__);
        }
        if (is_null($securityCode) || (is_array($securityCode) && empty($securityCode))) {
            unset($this->SecurityCode);
        } else {
            $this->SecurityCode = $securityCode;
        }
        
        return $this;
    }
    /**
     * Get EnquirerContactName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEnquirerContactName(): ?string
    {
        return isset($this->EnquirerContactName) ? $this->EnquirerContactName : null;
    }
    /**
     * Set EnquirerContactName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $enquirerContactName
     * @return \ID3Global\Models\GlobalSouthAfricaAccount
     */
    public function setEnquirerContactName(?string $enquirerContactName = null): self
    {
        // validation for constraint: string
        if (!is_null($enquirerContactName) && !is_string($enquirerContactName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($enquirerContactName, true), gettype($enquirerContactName)), __LINE__);
        }
        if (is_null($enquirerContactName) || (is_array($enquirerContactName) && empty($enquirerContactName))) {
            unset($this->EnquirerContactName);
        } else {
            $this->EnquirerContactName = $enquirerContactName;
        }
        
        return $this;
    }
    /**
     * Get EnquirerContactPhoneNo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEnquirerContactPhoneNo(): ?string
    {
        return isset($this->EnquirerContactPhoneNo) ? $this->EnquirerContactPhoneNo : null;
    }
    /**
     * Set EnquirerContactPhoneNo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $enquirerContactPhoneNo
     * @return \ID3Global\Models\GlobalSouthAfricaAccount
     */
    public function setEnquirerContactPhoneNo(?string $enquirerContactPhoneNo = null): self
    {
        // validation for constraint: string
        if (!is_null($enquirerContactPhoneNo) && !is_string($enquirerContactPhoneNo)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($enquirerContactPhoneNo, true), gettype($enquirerContactPhoneNo)), __LINE__);
        }
        if (is_null($enquirerContactPhoneNo) || (is_array($enquirerContactPhoneNo) && empty($enquirerContactPhoneNo))) {
            unset($this->EnquirerContactPhoneNo);
        } else {
            $this->EnquirerContactPhoneNo = $enquirerContactPhoneNo;
        }
        
        return $this;
    }
}
