<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetItemChecksResponse Models
 * @subpackage Structs
 */
class GetItemChecksResponse extends AbstractStructBase
{
    /**
     * The GetItemChecksResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckConfig|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckConfig $GetItemChecksResult = null;
    /**
     * Constructor method for GetItemChecksResponse
     * @uses GetItemChecksResponse::setGetItemChecksResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckConfig $getItemChecksResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalItemCheckConfig $getItemChecksResult = null)
    {
        $this
            ->setGetItemChecksResult($getItemChecksResult);
    }
    /**
     * Get GetItemChecksResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckConfig|null
     */
    public function getGetItemChecksResult(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckConfig
    {
        return isset($this->GetItemChecksResult) ? $this->GetItemChecksResult : null;
    }
    /**
     * Set GetItemChecksResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckConfig $getItemChecksResult
     * @return \ID3Global\Models\GetItemChecksResponse
     */
    public function setGetItemChecksResult(?\ID3Global\Arrays\ArrayOfGlobalItemCheckConfig $getItemChecksResult = null): self
    {
        if (is_null($getItemChecksResult) || (is_array($getItemChecksResult) && empty($getItemChecksResult))) {
            unset($this->GetItemChecksResult);
        } else {
            $this->GetItemChecksResult = $getItemChecksResult;
        }
        
        return $this;
    }
}
