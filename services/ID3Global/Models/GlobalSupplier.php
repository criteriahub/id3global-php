<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSupplier Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q619:GlobalSupplier
 * @subpackage Structs
 */
class GlobalSupplier extends AbstractStructBase
{
    /**
     * The SupplierID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $SupplierID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The IsRemote
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IsRemote = null;
    /**
     * The CanCreateAccounts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $CanCreateAccounts = null;
    /**
     * The Contact1st
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Contact1st = null;
    /**
     * The Email1st
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Email1st = null;
    /**
     * The Telephone1st
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Telephone1st = null;
    /**
     * The Hours1st
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Hours1st = null;
    /**
     * The Contact2nd
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Contact2nd = null;
    /**
     * The Email2nd
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Email2nd = null;
    /**
     * The Telephone2nd
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Telephone2nd = null;
    /**
     * The Hours2nd
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Hours2nd = null;
    /**
     * The ContactAccMan
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ContactAccMan = null;
    /**
     * The EmailAccMan
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $EmailAccMan = null;
    /**
     * The TelephoneAccMan
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $TelephoneAccMan = null;
    /**
     * The HoursAccMan
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $HoursAccMan = null;
    /**
     * The ResponseP1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ResponseP1 = null;
    /**
     * The ResponseP2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ResponseP2 = null;
    /**
     * The ResponseP3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ResponseP3 = null;
    /**
     * The ResponseP4
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ResponseP4 = null;
    /**
     * The PendingCount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PendingCount = null;
    /**
     * The ActiveCount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $ActiveCount = null;
    /**
     * The AutoPasswordChange
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $AutoPasswordChange = null;
    /**
     * Constructor method for GlobalSupplier
     * @uses GlobalSupplier::setSupplierID()
     * @uses GlobalSupplier::setName()
     * @uses GlobalSupplier::setIsRemote()
     * @uses GlobalSupplier::setCanCreateAccounts()
     * @uses GlobalSupplier::setContact1st()
     * @uses GlobalSupplier::setEmail1st()
     * @uses GlobalSupplier::setTelephone1st()
     * @uses GlobalSupplier::setHours1st()
     * @uses GlobalSupplier::setContact2nd()
     * @uses GlobalSupplier::setEmail2nd()
     * @uses GlobalSupplier::setTelephone2nd()
     * @uses GlobalSupplier::setHours2nd()
     * @uses GlobalSupplier::setContactAccMan()
     * @uses GlobalSupplier::setEmailAccMan()
     * @uses GlobalSupplier::setTelephoneAccMan()
     * @uses GlobalSupplier::setHoursAccMan()
     * @uses GlobalSupplier::setResponseP1()
     * @uses GlobalSupplier::setResponseP2()
     * @uses GlobalSupplier::setResponseP3()
     * @uses GlobalSupplier::setResponseP4()
     * @uses GlobalSupplier::setPendingCount()
     * @uses GlobalSupplier::setActiveCount()
     * @uses GlobalSupplier::setAutoPasswordChange()
     * @param string $supplierID
     * @param string $name
     * @param bool $isRemote
     * @param bool $canCreateAccounts
     * @param string $contact1st
     * @param string $email1st
     * @param string $telephone1st
     * @param string $hours1st
     * @param string $contact2nd
     * @param string $email2nd
     * @param string $telephone2nd
     * @param string $hours2nd
     * @param string $contactAccMan
     * @param string $emailAccMan
     * @param string $telephoneAccMan
     * @param string $hoursAccMan
     * @param string $responseP1
     * @param string $responseP2
     * @param string $responseP3
     * @param string $responseP4
     * @param int $pendingCount
     * @param int $activeCount
     * @param bool $autoPasswordChange
     */
    public function __construct(?string $supplierID = null, ?string $name = null, ?bool $isRemote = null, ?bool $canCreateAccounts = null, ?string $contact1st = null, ?string $email1st = null, ?string $telephone1st = null, ?string $hours1st = null, ?string $contact2nd = null, ?string $email2nd = null, ?string $telephone2nd = null, ?string $hours2nd = null, ?string $contactAccMan = null, ?string $emailAccMan = null, ?string $telephoneAccMan = null, ?string $hoursAccMan = null, ?string $responseP1 = null, ?string $responseP2 = null, ?string $responseP3 = null, ?string $responseP4 = null, ?int $pendingCount = null, ?int $activeCount = null, ?bool $autoPasswordChange = null)
    {
        $this
            ->setSupplierID($supplierID)
            ->setName($name)
            ->setIsRemote($isRemote)
            ->setCanCreateAccounts($canCreateAccounts)
            ->setContact1st($contact1st)
            ->setEmail1st($email1st)
            ->setTelephone1st($telephone1st)
            ->setHours1st($hours1st)
            ->setContact2nd($contact2nd)
            ->setEmail2nd($email2nd)
            ->setTelephone2nd($telephone2nd)
            ->setHours2nd($hours2nd)
            ->setContactAccMan($contactAccMan)
            ->setEmailAccMan($emailAccMan)
            ->setTelephoneAccMan($telephoneAccMan)
            ->setHoursAccMan($hoursAccMan)
            ->setResponseP1($responseP1)
            ->setResponseP2($responseP2)
            ->setResponseP3($responseP3)
            ->setResponseP4($responseP4)
            ->setPendingCount($pendingCount)
            ->setActiveCount($activeCount)
            ->setAutoPasswordChange($autoPasswordChange);
    }
    /**
     * Get SupplierID value
     * @return string|null
     */
    public function getSupplierID(): ?string
    {
        return $this->SupplierID;
    }
    /**
     * Set SupplierID value
     * @param string $supplierID
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setSupplierID(?string $supplierID = null): self
    {
        // validation for constraint: string
        if (!is_null($supplierID) && !is_string($supplierID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($supplierID, true), gettype($supplierID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($supplierID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $supplierID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($supplierID, true)), __LINE__);
        }
        $this->SupplierID = $supplierID;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get IsRemote value
     * @return bool|null
     */
    public function getIsRemote(): ?bool
    {
        return $this->IsRemote;
    }
    /**
     * Set IsRemote value
     * @param bool $isRemote
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setIsRemote(?bool $isRemote = null): self
    {
        // validation for constraint: boolean
        if (!is_null($isRemote) && !is_bool($isRemote)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isRemote, true), gettype($isRemote)), __LINE__);
        }
        $this->IsRemote = $isRemote;
        
        return $this;
    }
    /**
     * Get CanCreateAccounts value
     * @return bool|null
     */
    public function getCanCreateAccounts(): ?bool
    {
        return $this->CanCreateAccounts;
    }
    /**
     * Set CanCreateAccounts value
     * @param bool $canCreateAccounts
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setCanCreateAccounts(?bool $canCreateAccounts = null): self
    {
        // validation for constraint: boolean
        if (!is_null($canCreateAccounts) && !is_bool($canCreateAccounts)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($canCreateAccounts, true), gettype($canCreateAccounts)), __LINE__);
        }
        $this->CanCreateAccounts = $canCreateAccounts;
        
        return $this;
    }
    /**
     * Get Contact1st value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getContact1st(): ?string
    {
        return isset($this->Contact1st) ? $this->Contact1st : null;
    }
    /**
     * Set Contact1st value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $contact1st
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setContact1st(?string $contact1st = null): self
    {
        // validation for constraint: string
        if (!is_null($contact1st) && !is_string($contact1st)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($contact1st, true), gettype($contact1st)), __LINE__);
        }
        if (is_null($contact1st) || (is_array($contact1st) && empty($contact1st))) {
            unset($this->Contact1st);
        } else {
            $this->Contact1st = $contact1st;
        }
        
        return $this;
    }
    /**
     * Get Email1st value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail1st(): ?string
    {
        return isset($this->Email1st) ? $this->Email1st : null;
    }
    /**
     * Set Email1st value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email1st
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setEmail1st(?string $email1st = null): self
    {
        // validation for constraint: string
        if (!is_null($email1st) && !is_string($email1st)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email1st, true), gettype($email1st)), __LINE__);
        }
        if (is_null($email1st) || (is_array($email1st) && empty($email1st))) {
            unset($this->Email1st);
        } else {
            $this->Email1st = $email1st;
        }
        
        return $this;
    }
    /**
     * Get Telephone1st value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTelephone1st(): ?string
    {
        return isset($this->Telephone1st) ? $this->Telephone1st : null;
    }
    /**
     * Set Telephone1st value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $telephone1st
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setTelephone1st(?string $telephone1st = null): self
    {
        // validation for constraint: string
        if (!is_null($telephone1st) && !is_string($telephone1st)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($telephone1st, true), gettype($telephone1st)), __LINE__);
        }
        if (is_null($telephone1st) || (is_array($telephone1st) && empty($telephone1st))) {
            unset($this->Telephone1st);
        } else {
            $this->Telephone1st = $telephone1st;
        }
        
        return $this;
    }
    /**
     * Get Hours1st value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getHours1st(): ?string
    {
        return isset($this->Hours1st) ? $this->Hours1st : null;
    }
    /**
     * Set Hours1st value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $hours1st
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setHours1st(?string $hours1st = null): self
    {
        // validation for constraint: string
        if (!is_null($hours1st) && !is_string($hours1st)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hours1st, true), gettype($hours1st)), __LINE__);
        }
        if (is_null($hours1st) || (is_array($hours1st) && empty($hours1st))) {
            unset($this->Hours1st);
        } else {
            $this->Hours1st = $hours1st;
        }
        
        return $this;
    }
    /**
     * Get Contact2nd value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getContact2nd(): ?string
    {
        return isset($this->Contact2nd) ? $this->Contact2nd : null;
    }
    /**
     * Set Contact2nd value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $contact2nd
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setContact2nd(?string $contact2nd = null): self
    {
        // validation for constraint: string
        if (!is_null($contact2nd) && !is_string($contact2nd)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($contact2nd, true), gettype($contact2nd)), __LINE__);
        }
        if (is_null($contact2nd) || (is_array($contact2nd) && empty($contact2nd))) {
            unset($this->Contact2nd);
        } else {
            $this->Contact2nd = $contact2nd;
        }
        
        return $this;
    }
    /**
     * Get Email2nd value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail2nd(): ?string
    {
        return isset($this->Email2nd) ? $this->Email2nd : null;
    }
    /**
     * Set Email2nd value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email2nd
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setEmail2nd(?string $email2nd = null): self
    {
        // validation for constraint: string
        if (!is_null($email2nd) && !is_string($email2nd)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email2nd, true), gettype($email2nd)), __LINE__);
        }
        if (is_null($email2nd) || (is_array($email2nd) && empty($email2nd))) {
            unset($this->Email2nd);
        } else {
            $this->Email2nd = $email2nd;
        }
        
        return $this;
    }
    /**
     * Get Telephone2nd value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTelephone2nd(): ?string
    {
        return isset($this->Telephone2nd) ? $this->Telephone2nd : null;
    }
    /**
     * Set Telephone2nd value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $telephone2nd
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setTelephone2nd(?string $telephone2nd = null): self
    {
        // validation for constraint: string
        if (!is_null($telephone2nd) && !is_string($telephone2nd)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($telephone2nd, true), gettype($telephone2nd)), __LINE__);
        }
        if (is_null($telephone2nd) || (is_array($telephone2nd) && empty($telephone2nd))) {
            unset($this->Telephone2nd);
        } else {
            $this->Telephone2nd = $telephone2nd;
        }
        
        return $this;
    }
    /**
     * Get Hours2nd value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getHours2nd(): ?string
    {
        return isset($this->Hours2nd) ? $this->Hours2nd : null;
    }
    /**
     * Set Hours2nd value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $hours2nd
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setHours2nd(?string $hours2nd = null): self
    {
        // validation for constraint: string
        if (!is_null($hours2nd) && !is_string($hours2nd)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hours2nd, true), gettype($hours2nd)), __LINE__);
        }
        if (is_null($hours2nd) || (is_array($hours2nd) && empty($hours2nd))) {
            unset($this->Hours2nd);
        } else {
            $this->Hours2nd = $hours2nd;
        }
        
        return $this;
    }
    /**
     * Get ContactAccMan value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getContactAccMan(): ?string
    {
        return isset($this->ContactAccMan) ? $this->ContactAccMan : null;
    }
    /**
     * Set ContactAccMan value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $contactAccMan
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setContactAccMan(?string $contactAccMan = null): self
    {
        // validation for constraint: string
        if (!is_null($contactAccMan) && !is_string($contactAccMan)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($contactAccMan, true), gettype($contactAccMan)), __LINE__);
        }
        if (is_null($contactAccMan) || (is_array($contactAccMan) && empty($contactAccMan))) {
            unset($this->ContactAccMan);
        } else {
            $this->ContactAccMan = $contactAccMan;
        }
        
        return $this;
    }
    /**
     * Get EmailAccMan value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmailAccMan(): ?string
    {
        return isset($this->EmailAccMan) ? $this->EmailAccMan : null;
    }
    /**
     * Set EmailAccMan value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $emailAccMan
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setEmailAccMan(?string $emailAccMan = null): self
    {
        // validation for constraint: string
        if (!is_null($emailAccMan) && !is_string($emailAccMan)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($emailAccMan, true), gettype($emailAccMan)), __LINE__);
        }
        if (is_null($emailAccMan) || (is_array($emailAccMan) && empty($emailAccMan))) {
            unset($this->EmailAccMan);
        } else {
            $this->EmailAccMan = $emailAccMan;
        }
        
        return $this;
    }
    /**
     * Get TelephoneAccMan value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTelephoneAccMan(): ?string
    {
        return isset($this->TelephoneAccMan) ? $this->TelephoneAccMan : null;
    }
    /**
     * Set TelephoneAccMan value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $telephoneAccMan
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setTelephoneAccMan(?string $telephoneAccMan = null): self
    {
        // validation for constraint: string
        if (!is_null($telephoneAccMan) && !is_string($telephoneAccMan)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($telephoneAccMan, true), gettype($telephoneAccMan)), __LINE__);
        }
        if (is_null($telephoneAccMan) || (is_array($telephoneAccMan) && empty($telephoneAccMan))) {
            unset($this->TelephoneAccMan);
        } else {
            $this->TelephoneAccMan = $telephoneAccMan;
        }
        
        return $this;
    }
    /**
     * Get HoursAccMan value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getHoursAccMan(): ?string
    {
        return isset($this->HoursAccMan) ? $this->HoursAccMan : null;
    }
    /**
     * Set HoursAccMan value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $hoursAccMan
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setHoursAccMan(?string $hoursAccMan = null): self
    {
        // validation for constraint: string
        if (!is_null($hoursAccMan) && !is_string($hoursAccMan)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hoursAccMan, true), gettype($hoursAccMan)), __LINE__);
        }
        if (is_null($hoursAccMan) || (is_array($hoursAccMan) && empty($hoursAccMan))) {
            unset($this->HoursAccMan);
        } else {
            $this->HoursAccMan = $hoursAccMan;
        }
        
        return $this;
    }
    /**
     * Get ResponseP1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getResponseP1(): ?string
    {
        return isset($this->ResponseP1) ? $this->ResponseP1 : null;
    }
    /**
     * Set ResponseP1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $responseP1
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setResponseP1(?string $responseP1 = null): self
    {
        // validation for constraint: string
        if (!is_null($responseP1) && !is_string($responseP1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($responseP1, true), gettype($responseP1)), __LINE__);
        }
        if (is_null($responseP1) || (is_array($responseP1) && empty($responseP1))) {
            unset($this->ResponseP1);
        } else {
            $this->ResponseP1 = $responseP1;
        }
        
        return $this;
    }
    /**
     * Get ResponseP2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getResponseP2(): ?string
    {
        return isset($this->ResponseP2) ? $this->ResponseP2 : null;
    }
    /**
     * Set ResponseP2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $responseP2
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setResponseP2(?string $responseP2 = null): self
    {
        // validation for constraint: string
        if (!is_null($responseP2) && !is_string($responseP2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($responseP2, true), gettype($responseP2)), __LINE__);
        }
        if (is_null($responseP2) || (is_array($responseP2) && empty($responseP2))) {
            unset($this->ResponseP2);
        } else {
            $this->ResponseP2 = $responseP2;
        }
        
        return $this;
    }
    /**
     * Get ResponseP3 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getResponseP3(): ?string
    {
        return isset($this->ResponseP3) ? $this->ResponseP3 : null;
    }
    /**
     * Set ResponseP3 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $responseP3
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setResponseP3(?string $responseP3 = null): self
    {
        // validation for constraint: string
        if (!is_null($responseP3) && !is_string($responseP3)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($responseP3, true), gettype($responseP3)), __LINE__);
        }
        if (is_null($responseP3) || (is_array($responseP3) && empty($responseP3))) {
            unset($this->ResponseP3);
        } else {
            $this->ResponseP3 = $responseP3;
        }
        
        return $this;
    }
    /**
     * Get ResponseP4 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getResponseP4(): ?string
    {
        return isset($this->ResponseP4) ? $this->ResponseP4 : null;
    }
    /**
     * Set ResponseP4 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $responseP4
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setResponseP4(?string $responseP4 = null): self
    {
        // validation for constraint: string
        if (!is_null($responseP4) && !is_string($responseP4)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($responseP4, true), gettype($responseP4)), __LINE__);
        }
        if (is_null($responseP4) || (is_array($responseP4) && empty($responseP4))) {
            unset($this->ResponseP4);
        } else {
            $this->ResponseP4 = $responseP4;
        }
        
        return $this;
    }
    /**
     * Get PendingCount value
     * @return int|null
     */
    public function getPendingCount(): ?int
    {
        return $this->PendingCount;
    }
    /**
     * Set PendingCount value
     * @param int $pendingCount
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setPendingCount(?int $pendingCount = null): self
    {
        // validation for constraint: int
        if (!is_null($pendingCount) && !(is_int($pendingCount) || ctype_digit($pendingCount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pendingCount, true), gettype($pendingCount)), __LINE__);
        }
        $this->PendingCount = $pendingCount;
        
        return $this;
    }
    /**
     * Get ActiveCount value
     * @return int|null
     */
    public function getActiveCount(): ?int
    {
        return $this->ActiveCount;
    }
    /**
     * Set ActiveCount value
     * @param int $activeCount
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setActiveCount(?int $activeCount = null): self
    {
        // validation for constraint: int
        if (!is_null($activeCount) && !(is_int($activeCount) || ctype_digit($activeCount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($activeCount, true), gettype($activeCount)), __LINE__);
        }
        $this->ActiveCount = $activeCount;
        
        return $this;
    }
    /**
     * Get AutoPasswordChange value
     * @return bool|null
     */
    public function getAutoPasswordChange(): ?bool
    {
        return $this->AutoPasswordChange;
    }
    /**
     * Set AutoPasswordChange value
     * @param bool $autoPasswordChange
     * @return \ID3Global\Models\GlobalSupplier
     */
    public function setAutoPasswordChange(?bool $autoPasswordChange = null): self
    {
        // validation for constraint: boolean
        if (!is_null($autoPasswordChange) && !is_bool($autoPasswordChange)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($autoPasswordChange, true), gettype($autoPasswordChange)), __LINE__);
        }
        $this->AutoPasswordChange = $autoPasswordChange;
        
        return $this;
    }
}
