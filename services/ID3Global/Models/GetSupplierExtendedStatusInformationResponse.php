<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSupplierExtendedStatusInformationResponse Models
 * @subpackage Structs
 */
class GetSupplierExtendedStatusInformationResponse extends AbstractStructBase
{
    /**
     * The GetSupplierExtendedStatusInformationResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalStatus|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalStatus $GetSupplierExtendedStatusInformationResult = null;
    /**
     * Constructor method for GetSupplierExtendedStatusInformationResponse
     * @uses GetSupplierExtendedStatusInformationResponse::setGetSupplierExtendedStatusInformationResult()
     * @param \ID3Global\Arrays\ArrayOfGlobalStatus $getSupplierExtendedStatusInformationResult
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalStatus $getSupplierExtendedStatusInformationResult = null)
    {
        $this
            ->setGetSupplierExtendedStatusInformationResult($getSupplierExtendedStatusInformationResult);
    }
    /**
     * Get GetSupplierExtendedStatusInformationResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalStatus|null
     */
    public function getGetSupplierExtendedStatusInformationResult(): ?\ID3Global\Arrays\ArrayOfGlobalStatus
    {
        return isset($this->GetSupplierExtendedStatusInformationResult) ? $this->GetSupplierExtendedStatusInformationResult : null;
    }
    /**
     * Set GetSupplierExtendedStatusInformationResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalStatus $getSupplierExtendedStatusInformationResult
     * @return \ID3Global\Models\GetSupplierExtendedStatusInformationResponse
     */
    public function setGetSupplierExtendedStatusInformationResult(?\ID3Global\Arrays\ArrayOfGlobalStatus $getSupplierExtendedStatusInformationResult = null): self
    {
        if (is_null($getSupplierExtendedStatusInformationResult) || (is_array($getSupplierExtendedStatusInformationResult) && empty($getSupplierExtendedStatusInformationResult))) {
            unset($this->GetSupplierExtendedStatusInformationResult);
        } else {
            $this->GetSupplierExtendedStatusInformationResult = $getSupplierExtendedStatusInformationResult;
        }
        
        return $this;
    }
}
