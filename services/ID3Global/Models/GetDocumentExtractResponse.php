<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDocumentExtractResponse Models
 * @subpackage Structs
 */
class GetDocumentExtractResponse extends AbstractStructBase
{
    /**
     * The GetDocumentExtractResult
     * Meta information extracted from the WSDL
     * - base: xs:base64Binary
     * @var string|null
     */
    protected ?string $GetDocumentExtractResult = null;
    /**
     * Constructor method for GetDocumentExtractResponse
     * @uses GetDocumentExtractResponse::setGetDocumentExtractResult()
     * @param string $getDocumentExtractResult
     */
    public function __construct(?string $getDocumentExtractResult = null)
    {
        $this
            ->setGetDocumentExtractResult($getDocumentExtractResult);
    }
    /**
     * Get GetDocumentExtractResult value
     * @return string|null
     */
    public function getGetDocumentExtractResult(): ?string
    {
        return $this->GetDocumentExtractResult;
    }
    /**
     * Set GetDocumentExtractResult value
     * @param string $getDocumentExtractResult
     * @return \ID3Global\Models\GetDocumentExtractResponse
     */
    public function setGetDocumentExtractResult(?string $getDocumentExtractResult = null): self
    {
        // validation for constraint: string
        if (!is_null($getDocumentExtractResult) && !is_string($getDocumentExtractResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($getDocumentExtractResult, true), gettype($getDocumentExtractResult)), __LINE__);
        }
        $this->GetDocumentExtractResult = $getDocumentExtractResult;
        
        return $this;
    }
}
