<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Global3DSecureCheckResultCodes Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q55:Global3DSecureCheckResultCodes
 * @subpackage Structs
 */
class Global3DSecureCheckResultCodes extends GlobalItemCheckResultCodes
{
    /**
     * The MerchantData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $MerchantData = null;
    /**
     * The ThreeDSecureACSUrl
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ThreeDSecureACSUrl = null;
    /**
     * The ThreeDSecureRequest
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ThreeDSecureRequest = null;
    /**
     * Constructor method for Global3DSecureCheckResultCodes
     * @uses Global3DSecureCheckResultCodes::setMerchantData()
     * @uses Global3DSecureCheckResultCodes::setThreeDSecureACSUrl()
     * @uses Global3DSecureCheckResultCodes::setThreeDSecureRequest()
     * @param string $merchantData
     * @param string $threeDSecureACSUrl
     * @param string $threeDSecureRequest
     */
    public function __construct(?string $merchantData = null, ?string $threeDSecureACSUrl = null, ?string $threeDSecureRequest = null)
    {
        $this
            ->setMerchantData($merchantData)
            ->setThreeDSecureACSUrl($threeDSecureACSUrl)
            ->setThreeDSecureRequest($threeDSecureRequest);
    }
    /**
     * Get MerchantData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMerchantData(): ?string
    {
        return isset($this->MerchantData) ? $this->MerchantData : null;
    }
    /**
     * Set MerchantData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $merchantData
     * @return \ID3Global\Models\Global3DSecureCheckResultCodes
     */
    public function setMerchantData(?string $merchantData = null): self
    {
        // validation for constraint: string
        if (!is_null($merchantData) && !is_string($merchantData)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($merchantData, true), gettype($merchantData)), __LINE__);
        }
        if (is_null($merchantData) || (is_array($merchantData) && empty($merchantData))) {
            unset($this->MerchantData);
        } else {
            $this->MerchantData = $merchantData;
        }
        
        return $this;
    }
    /**
     * Get ThreeDSecureACSUrl value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getThreeDSecureACSUrl(): ?string
    {
        return isset($this->ThreeDSecureACSUrl) ? $this->ThreeDSecureACSUrl : null;
    }
    /**
     * Set ThreeDSecureACSUrl value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $threeDSecureACSUrl
     * @return \ID3Global\Models\Global3DSecureCheckResultCodes
     */
    public function setThreeDSecureACSUrl(?string $threeDSecureACSUrl = null): self
    {
        // validation for constraint: string
        if (!is_null($threeDSecureACSUrl) && !is_string($threeDSecureACSUrl)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($threeDSecureACSUrl, true), gettype($threeDSecureACSUrl)), __LINE__);
        }
        if (is_null($threeDSecureACSUrl) || (is_array($threeDSecureACSUrl) && empty($threeDSecureACSUrl))) {
            unset($this->ThreeDSecureACSUrl);
        } else {
            $this->ThreeDSecureACSUrl = $threeDSecureACSUrl;
        }
        
        return $this;
    }
    /**
     * Get ThreeDSecureRequest value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getThreeDSecureRequest(): ?string
    {
        return isset($this->ThreeDSecureRequest) ? $this->ThreeDSecureRequest : null;
    }
    /**
     * Set ThreeDSecureRequest value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $threeDSecureRequest
     * @return \ID3Global\Models\Global3DSecureCheckResultCodes
     */
    public function setThreeDSecureRequest(?string $threeDSecureRequest = null): self
    {
        // validation for constraint: string
        if (!is_null($threeDSecureRequest) && !is_string($threeDSecureRequest)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($threeDSecureRequest, true), gettype($threeDSecureRequest)), __LINE__);
        }
        if (is_null($threeDSecureRequest) || (is_array($threeDSecureRequest) && empty($threeDSecureRequest))) {
            unset($this->ThreeDSecureRequest);
        } else {
            $this->ThreeDSecureRequest = $threeDSecureRequest;
        }
        
        return $this;
    }
}
