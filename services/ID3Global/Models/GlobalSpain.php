<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSpain Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q394:GlobalSpain
 * @subpackage Structs
 */
class GlobalSpain extends AbstractStructBase
{
    /**
     * The TaxIDNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalSpainTaxIDNumber|null
     */
    protected ?\ID3Global\Models\GlobalSpainTaxIDNumber $TaxIDNumber = null;
    /**
     * Constructor method for GlobalSpain
     * @uses GlobalSpain::setTaxIDNumber()
     * @param \ID3Global\Models\GlobalSpainTaxIDNumber $taxIDNumber
     */
    public function __construct(?\ID3Global\Models\GlobalSpainTaxIDNumber $taxIDNumber = null)
    {
        $this
            ->setTaxIDNumber($taxIDNumber);
    }
    /**
     * Get TaxIDNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalSpainTaxIDNumber|null
     */
    public function getTaxIDNumber(): ?\ID3Global\Models\GlobalSpainTaxIDNumber
    {
        return isset($this->TaxIDNumber) ? $this->TaxIDNumber : null;
    }
    /**
     * Set TaxIDNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalSpainTaxIDNumber $taxIDNumber
     * @return \ID3Global\Models\GlobalSpain
     */
    public function setTaxIDNumber(?\ID3Global\Models\GlobalSpainTaxIDNumber $taxIDNumber = null): self
    {
        if (is_null($taxIDNumber) || (is_array($taxIDNumber) && empty($taxIDNumber))) {
            unset($this->TaxIDNumber);
        } else {
            $this->TaxIDNumber = $taxIDNumber;
        }
        
        return $this;
    }
}
