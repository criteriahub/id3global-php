<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DownloadExtractResponse Models
 * @subpackage Structs
 */
class DownloadExtractResponse extends AbstractStructBase
{
    /**
     * The DownloadExtractResult
     * Meta information extracted from the WSDL
     * - base: xs:base64Binary
     * @var string|null
     */
    protected ?string $DownloadExtractResult = null;
    /**
     * Constructor method for DownloadExtractResponse
     * @uses DownloadExtractResponse::setDownloadExtractResult()
     * @param string $downloadExtractResult
     */
    public function __construct(?string $downloadExtractResult = null)
    {
        $this
            ->setDownloadExtractResult($downloadExtractResult);
    }
    /**
     * Get DownloadExtractResult value
     * @return string|null
     */
    public function getDownloadExtractResult(): ?string
    {
        return $this->DownloadExtractResult;
    }
    /**
     * Set DownloadExtractResult value
     * @param string $downloadExtractResult
     * @return \ID3Global\Models\DownloadExtractResponse
     */
    public function setDownloadExtractResult(?string $downloadExtractResult = null): self
    {
        // validation for constraint: string
        if (!is_null($downloadExtractResult) && !is_string($downloadExtractResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($downloadExtractResult, true), gettype($downloadExtractResult)), __LINE__);
        }
        $this->DownloadExtractResult = $downloadExtractResult;
        
        return $this;
    }
}
