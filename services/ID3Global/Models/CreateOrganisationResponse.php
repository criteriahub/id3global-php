<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateOrganisationResponse Models
 * @subpackage Structs
 */
class CreateOrganisationResponse extends AbstractStructBase
{
    /**
     * The CreateOrganisationResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalOrganisation|null
     */
    protected ?\ID3Global\Models\GlobalOrganisation $CreateOrganisationResult = null;
    /**
     * Constructor method for CreateOrganisationResponse
     * @uses CreateOrganisationResponse::setCreateOrganisationResult()
     * @param \ID3Global\Models\GlobalOrganisation $createOrganisationResult
     */
    public function __construct(?\ID3Global\Models\GlobalOrganisation $createOrganisationResult = null)
    {
        $this
            ->setCreateOrganisationResult($createOrganisationResult);
    }
    /**
     * Get CreateOrganisationResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalOrganisation|null
     */
    public function getCreateOrganisationResult(): ?\ID3Global\Models\GlobalOrganisation
    {
        return isset($this->CreateOrganisationResult) ? $this->CreateOrganisationResult : null;
    }
    /**
     * Set CreateOrganisationResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalOrganisation $createOrganisationResult
     * @return \ID3Global\Models\CreateOrganisationResponse
     */
    public function setCreateOrganisationResult(?\ID3Global\Models\GlobalOrganisation $createOrganisationResult = null): self
    {
        if (is_null($createOrganisationResult) || (is_array($createOrganisationResult) && empty($createOrganisationResult))) {
            unset($this->CreateOrganisationResult);
        } else {
            $this->CreateOrganisationResult = $createOrganisationResult;
        }
        
        return $this;
    }
}
