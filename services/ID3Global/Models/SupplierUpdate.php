<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SupplierUpdate Models
 * @subpackage Structs
 */
class SupplierUpdate extends AbstractStructBase
{
    /**
     * The SupplierName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $SupplierName = null;
    /**
     * Constructor method for SupplierUpdate
     * @uses SupplierUpdate::setSupplierName()
     * @param string $supplierName
     */
    public function __construct(?string $supplierName = null)
    {
        $this
            ->setSupplierName($supplierName);
    }
    /**
     * Get SupplierName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSupplierName(): ?string
    {
        return isset($this->SupplierName) ? $this->SupplierName : null;
    }
    /**
     * Set SupplierName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $supplierName
     * @return \ID3Global\Models\SupplierUpdate
     */
    public function setSupplierName(?string $supplierName = null): self
    {
        // validation for constraint: string
        if (!is_null($supplierName) && !is_string($supplierName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($supplierName, true), gettype($supplierName)), __LINE__);
        }
        if (is_null($supplierName) || (is_array($supplierName) && empty($supplierName))) {
            unset($this->SupplierName);
        } else {
            $this->SupplierName = $supplierName;
        }
        
        return $this;
    }
}
