<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalSupportedField Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q194:GlobalSupportedField
 * @subpackage Structs
 */
class GlobalSupportedField extends AbstractStructBase
{
    /**
     * The FullName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $FullName = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Namespace
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Namespace = null;
    /**
     * The Required
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Required = null;
    /**
     * Constructor method for GlobalSupportedField
     * @uses GlobalSupportedField::setFullName()
     * @uses GlobalSupportedField::setName()
     * @uses GlobalSupportedField::setNamespace()
     * @uses GlobalSupportedField::setRequired()
     * @param string $fullName
     * @param string $name
     * @param string $namespace
     * @param bool $required
     */
    public function __construct(?string $fullName = null, ?string $name = null, ?string $namespace = null, ?bool $required = null)
    {
        $this
            ->setFullName($fullName)
            ->setName($name)
            ->setNamespace($namespace)
            ->setRequired($required);
    }
    /**
     * Get FullName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return isset($this->FullName) ? $this->FullName : null;
    }
    /**
     * Set FullName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fullName
     * @return \ID3Global\Models\GlobalSupportedField
     */
    public function setFullName(?string $fullName = null): self
    {
        // validation for constraint: string
        if (!is_null($fullName) && !is_string($fullName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fullName, true), gettype($fullName)), __LINE__);
        }
        if (is_null($fullName) || (is_array($fullName) && empty($fullName))) {
            unset($this->FullName);
        } else {
            $this->FullName = $fullName;
        }
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalSupportedField
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Namespace value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNamespace(): ?string
    {
        return isset($this->Namespace) ? $this->Namespace : null;
    }
    /**
     * Set Namespace value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $namespace
     * @return \ID3Global\Models\GlobalSupportedField
     */
    public function setNamespace(?string $namespace = null): self
    {
        // validation for constraint: string
        if (!is_null($namespace) && !is_string($namespace)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($namespace, true), gettype($namespace)), __LINE__);
        }
        if (is_null($namespace) || (is_array($namespace) && empty($namespace))) {
            unset($this->Namespace);
        } else {
            $this->Namespace = $namespace;
        }
        
        return $this;
    }
    /**
     * Get Required value
     * @return bool|null
     */
    public function getRequired(): ?bool
    {
        return $this->Required;
    }
    /**
     * Set Required value
     * @param bool $required
     * @return \ID3Global\Models\GlobalSupportedField
     */
    public function setRequired(?bool $required = null): self
    {
        // validation for constraint: boolean
        if (!is_null($required) && !is_bool($required)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($required, true), gettype($required)), __LINE__);
        }
        $this->Required = $required;
        
        return $this;
    }
}
