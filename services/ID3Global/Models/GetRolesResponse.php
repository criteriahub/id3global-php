<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetRolesResponse Models
 * @subpackage Structs
 */
class GetRolesResponse extends AbstractStructBase
{
    /**
     * The GetRolesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalRoles|null
     */
    protected ?\ID3Global\Models\GlobalRoles $GetRolesResult = null;
    /**
     * Constructor method for GetRolesResponse
     * @uses GetRolesResponse::setGetRolesResult()
     * @param \ID3Global\Models\GlobalRoles $getRolesResult
     */
    public function __construct(?\ID3Global\Models\GlobalRoles $getRolesResult = null)
    {
        $this
            ->setGetRolesResult($getRolesResult);
    }
    /**
     * Get GetRolesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalRoles|null
     */
    public function getGetRolesResult(): ?\ID3Global\Models\GlobalRoles
    {
        return isset($this->GetRolesResult) ? $this->GetRolesResult : null;
    }
    /**
     * Set GetRolesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalRoles $getRolesResult
     * @return \ID3Global\Models\GetRolesResponse
     */
    public function setGetRolesResult(?\ID3Global\Models\GlobalRoles $getRolesResult = null): self
    {
        if (is_null($getRolesResult) || (is_array($getRolesResult) && empty($getRolesResult))) {
            unset($this->GetRolesResult);
        } else {
            $this->GetRolesResult = $getRolesResult;
        }
        
        return $this;
    }
}
