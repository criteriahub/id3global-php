<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCases Models
 * @subpackage Structs
 */
class GetCases extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The Page
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Page = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The State
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $State = null;
    /**
     * The Verdict
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Verdict = null;
    /**
     * The FromDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $FromDate = null;
    /**
     * The ToDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $ToDate = null;
    /**
     * The IncludeDocuments
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $IncludeDocuments = null;
    /**
     * Constructor method for GetCases
     * @uses GetCases::setOrgID()
     * @uses GetCases::setPage()
     * @uses GetCases::setPageSize()
     * @uses GetCases::setName()
     * @uses GetCases::setState()
     * @uses GetCases::setVerdict()
     * @uses GetCases::setFromDate()
     * @uses GetCases::setToDate()
     * @uses GetCases::setIncludeDocuments()
     * @param string $orgID
     * @param int $page
     * @param int $pageSize
     * @param string $name
     * @param int $state
     * @param int $verdict
     * @param string $fromDate
     * @param string $toDate
     * @param bool $includeDocuments
     */
    public function __construct(?string $orgID = null, ?int $page = null, ?int $pageSize = null, ?string $name = null, ?int $state = null, ?int $verdict = null, ?string $fromDate = null, ?string $toDate = null, ?bool $includeDocuments = null)
    {
        $this
            ->setOrgID($orgID)
            ->setPage($page)
            ->setPageSize($pageSize)
            ->setName($name)
            ->setState($state)
            ->setVerdict($verdict)
            ->setFromDate($fromDate)
            ->setToDate($toDate)
            ->setIncludeDocuments($includeDocuments);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\GetCases
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get Page value
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->Page;
    }
    /**
     * Set Page value
     * @param int $page
     * @return \ID3Global\Models\GetCases
     */
    public function setPage(?int $page = null): self
    {
        // validation for constraint: int
        if (!is_null($page) && !(is_int($page) || ctype_digit($page))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($page, true), gettype($page)), __LINE__);
        }
        $this->Page = $page;
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GetCases
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GetCases
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get State value
     * @return int|null
     */
    public function getState(): ?int
    {
        return $this->State;
    }
    /**
     * Set State value
     * @param int $state
     * @return \ID3Global\Models\GetCases
     */
    public function setState(?int $state = null): self
    {
        // validation for constraint: int
        if (!is_null($state) && !(is_int($state) || ctype_digit($state))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($state, true), gettype($state)), __LINE__);
        }
        $this->State = $state;
        
        return $this;
    }
    /**
     * Get Verdict value
     * @return int|null
     */
    public function getVerdict(): ?int
    {
        return $this->Verdict;
    }
    /**
     * Set Verdict value
     * @param int $verdict
     * @return \ID3Global\Models\GetCases
     */
    public function setVerdict(?int $verdict = null): self
    {
        // validation for constraint: int
        if (!is_null($verdict) && !(is_int($verdict) || ctype_digit($verdict))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($verdict, true), gettype($verdict)), __LINE__);
        }
        $this->Verdict = $verdict;
        
        return $this;
    }
    /**
     * Get FromDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFromDate(): ?string
    {
        return isset($this->FromDate) ? $this->FromDate : null;
    }
    /**
     * Set FromDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fromDate
     * @return \ID3Global\Models\GetCases
     */
    public function setFromDate(?string $fromDate = null): self
    {
        // validation for constraint: string
        if (!is_null($fromDate) && !is_string($fromDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fromDate, true), gettype($fromDate)), __LINE__);
        }
        if (is_null($fromDate) || (is_array($fromDate) && empty($fromDate))) {
            unset($this->FromDate);
        } else {
            $this->FromDate = $fromDate;
        }
        
        return $this;
    }
    /**
     * Get ToDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getToDate(): ?string
    {
        return isset($this->ToDate) ? $this->ToDate : null;
    }
    /**
     * Set ToDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $toDate
     * @return \ID3Global\Models\GetCases
     */
    public function setToDate(?string $toDate = null): self
    {
        // validation for constraint: string
        if (!is_null($toDate) && !is_string($toDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($toDate, true), gettype($toDate)), __LINE__);
        }
        if (is_null($toDate) || (is_array($toDate) && empty($toDate))) {
            unset($this->ToDate);
        } else {
            $this->ToDate = $toDate;
        }
        
        return $this;
    }
    /**
     * Get IncludeDocuments value
     * @return bool|null
     */
    public function getIncludeDocuments(): ?bool
    {
        return $this->IncludeDocuments;
    }
    /**
     * Set IncludeDocuments value
     * @param bool $includeDocuments
     * @return \ID3Global\Models\GetCases
     */
    public function setIncludeDocuments(?bool $includeDocuments = null): self
    {
        // validation for constraint: boolean
        if (!is_null($includeDocuments) && !is_bool($includeDocuments)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($includeDocuments, true), gettype($includeDocuments)), __LINE__);
        }
        $this->IncludeDocuments = $includeDocuments;
        
        return $this;
    }
}
