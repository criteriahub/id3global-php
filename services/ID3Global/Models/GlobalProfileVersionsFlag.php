<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalProfileVersionsFlag Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q175:GlobalProfileVersionsFlag
 * @subpackage Structs
 */
class GlobalProfileVersionsFlag extends AbstractStructBase
{
    /**
     * The ProfileVersionIDs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfstring|null
     */
    protected ?\ID3Global\Arrays\ArrayOfstring $ProfileVersionIDs = null;
    /**
     * The FlagImageBase64
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $FlagImageBase64 = null;
    /**
     * Constructor method for GlobalProfileVersionsFlag
     * @uses GlobalProfileVersionsFlag::setProfileVersionIDs()
     * @uses GlobalProfileVersionsFlag::setFlagImageBase64()
     * @param \ID3Global\Arrays\ArrayOfstring $profileVersionIDs
     * @param string $flagImageBase64
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfstring $profileVersionIDs = null, ?string $flagImageBase64 = null)
    {
        $this
            ->setProfileVersionIDs($profileVersionIDs)
            ->setFlagImageBase64($flagImageBase64);
    }
    /**
     * Get ProfileVersionIDs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfstring|null
     */
    public function getProfileVersionIDs(): ?\ID3Global\Arrays\ArrayOfstring
    {
        return isset($this->ProfileVersionIDs) ? $this->ProfileVersionIDs : null;
    }
    /**
     * Set ProfileVersionIDs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfstring $profileVersionIDs
     * @return \ID3Global\Models\GlobalProfileVersionsFlag
     */
    public function setProfileVersionIDs(?\ID3Global\Arrays\ArrayOfstring $profileVersionIDs = null): self
    {
        if (is_null($profileVersionIDs) || (is_array($profileVersionIDs) && empty($profileVersionIDs))) {
            unset($this->ProfileVersionIDs);
        } else {
            $this->ProfileVersionIDs = $profileVersionIDs;
        }
        
        return $this;
    }
    /**
     * Get FlagImageBase64 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFlagImageBase64(): ?string
    {
        return isset($this->FlagImageBase64) ? $this->FlagImageBase64 : null;
    }
    /**
     * Set FlagImageBase64 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $flagImageBase64
     * @return \ID3Global\Models\GlobalProfileVersionsFlag
     */
    public function setFlagImageBase64(?string $flagImageBase64 = null): self
    {
        // validation for constraint: string
        if (!is_null($flagImageBase64) && !is_string($flagImageBase64)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($flagImageBase64, true), gettype($flagImageBase64)), __LINE__);
        }
        if (is_null($flagImageBase64) || (is_array($flagImageBase64) && empty($flagImageBase64))) {
            unset($this->FlagImageBase64);
        } else {
            $this->FlagImageBase64 = $flagImageBase64;
        }
        
        return $this;
    }
}
