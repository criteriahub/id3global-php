<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalCaseNotifications Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q734:GlobalCaseNotifications
 * @subpackage Structs
 */
class GlobalCaseNotifications extends AbstractStructBase
{
    /**
     * The Items
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalCaseNotification|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalCaseNotification $Items = null;
    /**
     * The PageSize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $PageSize = null;
    /**
     * The TotalPages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalPages = null;
    /**
     * The TotalNotifications
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $TotalNotifications = null;
    /**
     * Constructor method for GlobalCaseNotifications
     * @uses GlobalCaseNotifications::setItems()
     * @uses GlobalCaseNotifications::setPageSize()
     * @uses GlobalCaseNotifications::setTotalPages()
     * @uses GlobalCaseNotifications::setTotalNotifications()
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseNotification $items
     * @param int $pageSize
     * @param int $totalPages
     * @param int $totalNotifications
     */
    public function __construct(?\ID3Global\Arrays\ArrayOfGlobalCaseNotification $items = null, ?int $pageSize = null, ?int $totalPages = null, ?int $totalNotifications = null)
    {
        $this
            ->setItems($items)
            ->setPageSize($pageSize)
            ->setTotalPages($totalPages)
            ->setTotalNotifications($totalNotifications);
    }
    /**
     * Get Items value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalCaseNotification|null
     */
    public function getItems(): ?\ID3Global\Arrays\ArrayOfGlobalCaseNotification
    {
        return isset($this->Items) ? $this->Items : null;
    }
    /**
     * Set Items value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalCaseNotification $items
     * @return \ID3Global\Models\GlobalCaseNotifications
     */
    public function setItems(?\ID3Global\Arrays\ArrayOfGlobalCaseNotification $items = null): self
    {
        if (is_null($items) || (is_array($items) && empty($items))) {
            unset($this->Items);
        } else {
            $this->Items = $items;
        }
        
        return $this;
    }
    /**
     * Get PageSize value
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->PageSize;
    }
    /**
     * Set PageSize value
     * @param int $pageSize
     * @return \ID3Global\Models\GlobalCaseNotifications
     */
    public function setPageSize(?int $pageSize = null): self
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        $this->PageSize = $pageSize;
        
        return $this;
    }
    /**
     * Get TotalPages value
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->TotalPages;
    }
    /**
     * Set TotalPages value
     * @param int $totalPages
     * @return \ID3Global\Models\GlobalCaseNotifications
     */
    public function setTotalPages(?int $totalPages = null): self
    {
        // validation for constraint: int
        if (!is_null($totalPages) && !(is_int($totalPages) || ctype_digit($totalPages))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalPages, true), gettype($totalPages)), __LINE__);
        }
        $this->TotalPages = $totalPages;
        
        return $this;
    }
    /**
     * Get TotalNotifications value
     * @return int|null
     */
    public function getTotalNotifications(): ?int
    {
        return $this->TotalNotifications;
    }
    /**
     * Set TotalNotifications value
     * @param int $totalNotifications
     * @return \ID3Global\Models\GlobalCaseNotifications
     */
    public function setTotalNotifications(?int $totalNotifications = null): self
    {
        // validation for constraint: int
        if (!is_null($totalNotifications) && !(is_int($totalNotifications) || ctype_digit($totalNotifications))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalNotifications, true), gettype($totalNotifications)), __LINE__);
        }
        $this->TotalNotifications = $totalNotifications;
        
        return $this;
    }
}
