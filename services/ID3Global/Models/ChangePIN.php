<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ChangePIN Models
 * @subpackage Structs
 */
class ChangePIN extends AbstractStructBase
{
    /**
     * The AccountName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $AccountName = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * The PINSequence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $PINSequence = null;
    /**
     * The NewPIN
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $NewPIN = null;
    /**
     * Constructor method for ChangePIN
     * @uses ChangePIN::setAccountName()
     * @uses ChangePIN::setPassword()
     * @uses ChangePIN::setPINSequence()
     * @uses ChangePIN::setNewPIN()
     * @param string $accountName
     * @param string $password
     * @param string $pINSequence
     * @param string $newPIN
     */
    public function __construct(?string $accountName = null, ?string $password = null, ?string $pINSequence = null, ?string $newPIN = null)
    {
        $this
            ->setAccountName($accountName)
            ->setPassword($password)
            ->setPINSequence($pINSequence)
            ->setNewPIN($newPIN);
    }
    /**
     * Get AccountName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAccountName(): ?string
    {
        return isset($this->AccountName) ? $this->AccountName : null;
    }
    /**
     * Set AccountName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $accountName
     * @return \ID3Global\Models\ChangePIN
     */
    public function setAccountName(?string $accountName = null): self
    {
        // validation for constraint: string
        if (!is_null($accountName) && !is_string($accountName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($accountName, true), gettype($accountName)), __LINE__);
        }
        if (is_null($accountName) || (is_array($accountName) && empty($accountName))) {
            unset($this->AccountName);
        } else {
            $this->AccountName = $accountName;
        }
        
        return $this;
    }
    /**
     * Get Password value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return isset($this->Password) ? $this->Password : null;
    }
    /**
     * Set Password value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $password
     * @return \ID3Global\Models\ChangePIN
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        if (is_null($password) || (is_array($password) && empty($password))) {
            unset($this->Password);
        } else {
            $this->Password = $password;
        }
        
        return $this;
    }
    /**
     * Get PINSequence value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPINSequence(): ?string
    {
        return isset($this->PINSequence) ? $this->PINSequence : null;
    }
    /**
     * Set PINSequence value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $pINSequence
     * @return \ID3Global\Models\ChangePIN
     */
    public function setPINSequence(?string $pINSequence = null): self
    {
        // validation for constraint: string
        if (!is_null($pINSequence) && !is_string($pINSequence)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pINSequence, true), gettype($pINSequence)), __LINE__);
        }
        if (is_null($pINSequence) || (is_array($pINSequence) && empty($pINSequence))) {
            unset($this->PINSequence);
        } else {
            $this->PINSequence = $pINSequence;
        }
        
        return $this;
    }
    /**
     * Get NewPIN value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getNewPIN(): ?string
    {
        return isset($this->NewPIN) ? $this->NewPIN : null;
    }
    /**
     * Set NewPIN value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $newPIN
     * @return \ID3Global\Models\ChangePIN
     */
    public function setNewPIN(?string $newPIN = null): self
    {
        // validation for constraint: string
        if (!is_null($newPIN) && !is_string($newPIN)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($newPIN, true), gettype($newPIN)), __LINE__);
        }
        if (is_null($newPIN) || (is_array($newPIN) && empty($newPIN))) {
            unset($this->NewPIN);
        } else {
            $this->NewPIN = $newPIN;
        }
        
        return $this;
    }
}
