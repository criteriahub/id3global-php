<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalAuthenticationDetails Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q461:GlobalAuthenticationDetails
 * @subpackage Structs
 */
class GlobalAuthenticationDetails extends GlobalAuthentication
{
    /**
     * The InputData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalInputData|null
     */
    protected ?\ID3Global\Models\GlobalInputData $InputData = null;
    /**
     * The ResultCodes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes $ResultCodes = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The DomainName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $DomainName = null;
    /**
     * The UserBreakpoint
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $UserBreakpoint = null;
    /**
     * The NoRetry
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $NoRetry = null;
    /**
     * The DecisionBands
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands|null
     */
    protected ?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands $DecisionBands = null;
    /**
     * Constructor method for GlobalAuthenticationDetails
     * @uses GlobalAuthenticationDetails::setInputData()
     * @uses GlobalAuthenticationDetails::setResultCodes()
     * @uses GlobalAuthenticationDetails::setName()
     * @uses GlobalAuthenticationDetails::setUsername()
     * @uses GlobalAuthenticationDetails::setDomainName()
     * @uses GlobalAuthenticationDetails::setUserBreakpoint()
     * @uses GlobalAuthenticationDetails::setNoRetry()
     * @uses GlobalAuthenticationDetails::setDecisionBands()
     * @param \ID3Global\Models\GlobalInputData $inputData
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes $resultCodes
     * @param string $name
     * @param string $username
     * @param string $domainName
     * @param int $userBreakpoint
     * @param bool $noRetry
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands $decisionBands
     */
    public function __construct(?\ID3Global\Models\GlobalInputData $inputData = null, ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes $resultCodes = null, ?string $name = null, ?string $username = null, ?string $domainName = null, ?int $userBreakpoint = null, ?bool $noRetry = null, ?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands $decisionBands = null)
    {
        $this
            ->setInputData($inputData)
            ->setResultCodes($resultCodes)
            ->setName($name)
            ->setUsername($username)
            ->setDomainName($domainName)
            ->setUserBreakpoint($userBreakpoint)
            ->setNoRetry($noRetry)
            ->setDecisionBands($decisionBands);
    }
    /**
     * Get InputData value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalInputData|null
     */
    public function getInputData(): ?\ID3Global\Models\GlobalInputData
    {
        return isset($this->InputData) ? $this->InputData : null;
    }
    /**
     * Set InputData value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalInputData $inputData
     * @return \ID3Global\Models\GlobalAuthenticationDetails
     */
    public function setInputData(?\ID3Global\Models\GlobalInputData $inputData = null): self
    {
        if (is_null($inputData) || (is_array($inputData) && empty($inputData))) {
            unset($this->InputData);
        } else {
            $this->InputData = $inputData;
        }
        
        return $this;
    }
    /**
     * Get ResultCodes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes|null
     */
    public function getResultCodes(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes
    {
        return isset($this->ResultCodes) ? $this->ResultCodes : null;
    }
    /**
     * Set ResultCodes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes $resultCodes
     * @return \ID3Global\Models\GlobalAuthenticationDetails
     */
    public function setResultCodes(?\ID3Global\Arrays\ArrayOfGlobalItemCheckResultCodes $resultCodes = null): self
    {
        if (is_null($resultCodes) || (is_array($resultCodes) && empty($resultCodes))) {
            unset($this->ResultCodes);
        } else {
            $this->ResultCodes = $resultCodes;
        }
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\GlobalAuthenticationDetails
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Username value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return isset($this->Username) ? $this->Username : null;
    }
    /**
     * Set Username value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $username
     * @return \ID3Global\Models\GlobalAuthenticationDetails
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        if (is_null($username) || (is_array($username) && empty($username))) {
            unset($this->Username);
        } else {
            $this->Username = $username;
        }
        
        return $this;
    }
    /**
     * Get DomainName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDomainName(): ?string
    {
        return isset($this->DomainName) ? $this->DomainName : null;
    }
    /**
     * Set DomainName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $domainName
     * @return \ID3Global\Models\GlobalAuthenticationDetails
     */
    public function setDomainName(?string $domainName = null): self
    {
        // validation for constraint: string
        if (!is_null($domainName) && !is_string($domainName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domainName, true), gettype($domainName)), __LINE__);
        }
        if (is_null($domainName) || (is_array($domainName) && empty($domainName))) {
            unset($this->DomainName);
        } else {
            $this->DomainName = $domainName;
        }
        
        return $this;
    }
    /**
     * Get UserBreakpoint value
     * @return int|null
     */
    public function getUserBreakpoint(): ?int
    {
        return $this->UserBreakpoint;
    }
    /**
     * Set UserBreakpoint value
     * @param int $userBreakpoint
     * @return \ID3Global\Models\GlobalAuthenticationDetails
     */
    public function setUserBreakpoint(?int $userBreakpoint = null): self
    {
        // validation for constraint: int
        if (!is_null($userBreakpoint) && !(is_int($userBreakpoint) || ctype_digit($userBreakpoint))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($userBreakpoint, true), gettype($userBreakpoint)), __LINE__);
        }
        $this->UserBreakpoint = $userBreakpoint;
        
        return $this;
    }
    /**
     * Get NoRetry value
     * @return bool|null
     */
    public function getNoRetry(): ?bool
    {
        return $this->NoRetry;
    }
    /**
     * Set NoRetry value
     * @param bool $noRetry
     * @return \ID3Global\Models\GlobalAuthenticationDetails
     */
    public function setNoRetry(?bool $noRetry = null): self
    {
        // validation for constraint: boolean
        if (!is_null($noRetry) && !is_bool($noRetry)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($noRetry, true), gettype($noRetry)), __LINE__);
        }
        $this->NoRetry = $noRetry;
        
        return $this;
    }
    /**
     * Get DecisionBands value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands|null
     */
    public function getDecisionBands(): ?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands
    {
        return isset($this->DecisionBands) ? $this->DecisionBands : null;
    }
    /**
     * Set DecisionBands value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands $decisionBands
     * @return \ID3Global\Models\GlobalAuthenticationDetails
     */
    public function setDecisionBands(?\ID3Global\Arrays\ArrayOfGlobalItemCheckDecisionBands $decisionBands = null): self
    {
        if (is_null($decisionBands) || (is_array($decisionBands) && empty($decisionBands))) {
            unset($this->DecisionBands);
        } else {
            $this->DecisionBands = $decisionBands;
        }
        
        return $this;
    }
}
