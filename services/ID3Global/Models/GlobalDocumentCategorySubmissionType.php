<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalDocumentCategorySubmissionType Models
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q831:GlobalDocumentCategorySubmissionType
 * @subpackage Structs
 */
class GlobalDocumentCategorySubmissionType extends AbstractStructBase
{
    /**
     * The SubmissionType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $SubmissionType = null;
    /**
     * The Type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $Type = null;
    /**
     * Constructor method for GlobalDocumentCategorySubmissionType
     * @uses GlobalDocumentCategorySubmissionType::setSubmissionType()
     * @uses GlobalDocumentCategorySubmissionType::setType()
     * @param string $submissionType
     * @param int $type
     */
    public function __construct(?string $submissionType = null, ?int $type = null)
    {
        $this
            ->setSubmissionType($submissionType)
            ->setType($type);
    }
    /**
     * Get SubmissionType value
     * @return string|null
     */
    public function getSubmissionType(): ?string
    {
        return $this->SubmissionType;
    }
    /**
     * Set SubmissionType value
     * @uses \ID3Global\Enums\GlobalDocumentSubmissionType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalDocumentSubmissionType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $submissionType
     * @return \ID3Global\Models\GlobalDocumentCategorySubmissionType
     */
    public function setSubmissionType(?string $submissionType = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalDocumentSubmissionType::valueIsValid($submissionType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalDocumentSubmissionType', is_array($submissionType) ? implode(', ', $submissionType) : var_export($submissionType, true), implode(', ', \ID3Global\Enums\GlobalDocumentSubmissionType::getValidValues())), __LINE__);
        }
        $this->SubmissionType = $submissionType;
        
        return $this;
    }
    /**
     * Get Type value
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->Type;
    }
    /**
     * Set Type value
     * @param int $type
     * @return \ID3Global\Models\GlobalDocumentCategorySubmissionType
     */
    public function setType(?int $type = null): self
    {
        // validation for constraint: int
        if (!is_null($type) && !(is_int($type) || ctype_digit($type))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->Type = $type;
        
        return $this;
    }
}
