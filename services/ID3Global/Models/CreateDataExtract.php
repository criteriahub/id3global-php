<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateDataExtract Models
 * @subpackage Structs
 */
class CreateDataExtract extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The Name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Name = null;
    /**
     * The Type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Type = null;
    /**
     * The Start
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Start = null;
    /**
     * The End
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $End = null;
    /**
     * The Format
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $Format = null;
    /**
     * The Version
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string|null
     */
    protected ?string $Version = null;
    /**
     * The Repeat
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $Repeat = null;
    /**
     * Constructor method for CreateDataExtract
     * @uses CreateDataExtract::setOrgID()
     * @uses CreateDataExtract::setName()
     * @uses CreateDataExtract::setType()
     * @uses CreateDataExtract::setStart()
     * @uses CreateDataExtract::setEnd()
     * @uses CreateDataExtract::setFormat()
     * @uses CreateDataExtract::setVersion()
     * @uses CreateDataExtract::setRepeat()
     * @param string $orgID
     * @param string $name
     * @param string $type
     * @param string $start
     * @param string $end
     * @param string $format
     * @param string $version
     * @param bool $repeat
     */
    public function __construct(?string $orgID = null, ?string $name = null, ?string $type = null, ?string $start = null, ?string $end = null, ?string $format = null, ?string $version = null, ?bool $repeat = null)
    {
        $this
            ->setOrgID($orgID)
            ->setName($name)
            ->setType($type)
            ->setStart($start)
            ->setEnd($end)
            ->setFormat($format)
            ->setVersion($version)
            ->setRepeat($repeat);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\CreateDataExtract
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName(): ?string
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \ID3Global\Models\CreateDataExtract
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        
        return $this;
    }
    /**
     * Get Type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->Type;
    }
    /**
     * Set Type value
     * @uses \ID3Global\Enums\GlobalDataExtractType::valueIsValid()
     * @uses \ID3Global\Enums\GlobalDataExtractType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $type
     * @return \ID3Global\Models\CreateDataExtract
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalDataExtractType::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalDataExtractType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \ID3Global\Enums\GlobalDataExtractType::getValidValues())), __LINE__);
        }
        $this->Type = $type;
        
        return $this;
    }
    /**
     * Get Start value
     * @return string|null
     */
    public function getStart(): ?string
    {
        return $this->Start;
    }
    /**
     * Set Start value
     * @param string $start
     * @return \ID3Global\Models\CreateDataExtract
     */
    public function setStart(?string $start = null): self
    {
        // validation for constraint: string
        if (!is_null($start) && !is_string($start)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($start, true), gettype($start)), __LINE__);
        }
        $this->Start = $start;
        
        return $this;
    }
    /**
     * Get End value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEnd(): ?string
    {
        return isset($this->End) ? $this->End : null;
    }
    /**
     * Set End value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $end
     * @return \ID3Global\Models\CreateDataExtract
     */
    public function setEnd(?string $end = null): self
    {
        // validation for constraint: string
        if (!is_null($end) && !is_string($end)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($end, true), gettype($end)), __LINE__);
        }
        if (is_null($end) || (is_array($end) && empty($end))) {
            unset($this->End);
        } else {
            $this->End = $end;
        }
        
        return $this;
    }
    /**
     * Get Format value
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->Format;
    }
    /**
     * Set Format value
     * @uses \ID3Global\Enums\GlobalDataExtractFormat::valueIsValid()
     * @uses \ID3Global\Enums\GlobalDataExtractFormat::getValidValues()
     * @throws InvalidArgumentException
     * @param string $format
     * @return \ID3Global\Models\CreateDataExtract
     */
    public function setFormat(?string $format = null): self
    {
        // validation for constraint: enumeration
        if (!\ID3Global\Enums\GlobalDataExtractFormat::valueIsValid($format)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \ID3Global\Enums\GlobalDataExtractFormat', is_array($format) ? implode(', ', $format) : var_export($format, true), implode(', ', \ID3Global\Enums\GlobalDataExtractFormat::getValidValues())), __LINE__);
        }
        $this->Format = $format;
        
        return $this;
    }
    /**
     * Get Version value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getVersion(): ?string
    {
        return isset($this->Version) ? $this->Version : null;
    }
    /**
     * Set Version value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $version
     * @return \ID3Global\Models\CreateDataExtract
     */
    public function setVersion(?string $version = null): self
    {
        // validation for constraint: string
        if (!is_null($version) && !is_string($version)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($version, true), gettype($version)), __LINE__);
        }
        if (is_null($version) || (is_array($version) && empty($version))) {
            unset($this->Version);
        } else {
            $this->Version = $version;
        }
        
        return $this;
    }
    /**
     * Get Repeat value
     * @return bool|null
     */
    public function getRepeat(): ?bool
    {
        return $this->Repeat;
    }
    /**
     * Set Repeat value
     * @param bool $repeat
     * @return \ID3Global\Models\CreateDataExtract
     */
    public function setRepeat(?bool $repeat = null): self
    {
        // validation for constraint: boolean
        if (!is_null($repeat) && !is_bool($repeat)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($repeat, true), gettype($repeat)), __LINE__);
        }
        $this->Repeat = $repeat;
        
        return $this;
    }
}
