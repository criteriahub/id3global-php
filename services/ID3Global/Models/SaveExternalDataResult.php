<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SaveExternalDataResult Models
 * @subpackage Structs
 */
class SaveExternalDataResult extends AbstractStructBase
{
    /**
     * The OrgID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $OrgID = null;
    /**
     * The CaseID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string|null
     */
    protected ?string $CaseID = null;
    /**
     * The Param
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \ID3Global\Models\GlobalExternalDataResultParameters|null
     */
    protected ?\ID3Global\Models\GlobalExternalDataResultParameters $Param = null;
    /**
     * Constructor method for SaveExternalDataResult
     * @uses SaveExternalDataResult::setOrgID()
     * @uses SaveExternalDataResult::setCaseID()
     * @uses SaveExternalDataResult::setParam()
     * @param string $orgID
     * @param string $caseID
     * @param \ID3Global\Models\GlobalExternalDataResultParameters $param
     */
    public function __construct(?string $orgID = null, ?string $caseID = null, ?\ID3Global\Models\GlobalExternalDataResultParameters $param = null)
    {
        $this
            ->setOrgID($orgID)
            ->setCaseID($caseID)
            ->setParam($param);
    }
    /**
     * Get OrgID value
     * @return string|null
     */
    public function getOrgID(): ?string
    {
        return $this->OrgID;
    }
    /**
     * Set OrgID value
     * @param string $orgID
     * @return \ID3Global\Models\SaveExternalDataResult
     */
    public function setOrgID(?string $orgID = null): self
    {
        // validation for constraint: string
        if (!is_null($orgID) && !is_string($orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orgID, true), gettype($orgID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($orgID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $orgID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($orgID, true)), __LINE__);
        }
        $this->OrgID = $orgID;
        
        return $this;
    }
    /**
     * Get CaseID value
     * @return string|null
     */
    public function getCaseID(): ?string
    {
        return $this->CaseID;
    }
    /**
     * Set CaseID value
     * @param string $caseID
     * @return \ID3Global\Models\SaveExternalDataResult
     */
    public function setCaseID(?string $caseID = null): self
    {
        // validation for constraint: string
        if (!is_null($caseID) && !is_string($caseID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($caseID, true), gettype($caseID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($caseID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $caseID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', var_export($caseID, true)), __LINE__);
        }
        $this->CaseID = $caseID;
        
        return $this;
    }
    /**
     * Get Param value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \ID3Global\Models\GlobalExternalDataResultParameters|null
     */
    public function getParam(): ?\ID3Global\Models\GlobalExternalDataResultParameters
    {
        return isset($this->Param) ? $this->Param : null;
    }
    /**
     * Set Param value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \ID3Global\Models\GlobalExternalDataResultParameters $param
     * @return \ID3Global\Models\SaveExternalDataResult
     */
    public function setParam(?\ID3Global\Models\GlobalExternalDataResultParameters $param = null): self
    {
        if (is_null($param) || (is_array($param) && empty($param))) {
            unset($this->Param);
        } else {
            $this->Param = $param;
        }
        
        return $this;
    }
}
