<?php

declare(strict_types=1);

namespace ID3Global\Models;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DownloadResponse Models
 * @subpackage Structs
 */
class DownloadResponse extends AbstractStructBase
{
    /**
     * The DownloadResult
     * Meta information extracted from the WSDL
     * - base: xs:base64Binary
     * @var string|null
     */
    protected ?string $DownloadResult = null;
    /**
     * Constructor method for DownloadResponse
     * @uses DownloadResponse::setDownloadResult()
     * @param string $downloadResult
     */
    public function __construct(?string $downloadResult = null)
    {
        $this
            ->setDownloadResult($downloadResult);
    }
    /**
     * Get DownloadResult value
     * @return string|null
     */
    public function getDownloadResult(): ?string
    {
        return $this->DownloadResult;
    }
    /**
     * Set DownloadResult value
     * @param string $downloadResult
     * @return \ID3Global\Models\DownloadResponse
     */
    public function setDownloadResult(?string $downloadResult = null): self
    {
        // validation for constraint: string
        if (!is_null($downloadResult) && !is_string($downloadResult)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($downloadResult, true), gettype($downloadResult)), __LINE__);
        }
        $this->DownloadResult = $downloadResult;
        
        return $this;
    }
}
