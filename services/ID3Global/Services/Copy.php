<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Copy Services
 * @subpackage Services
 */
class Copy extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named CopyProfile
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CopyProfile $parameters
     * @return \ID3Global\Models\CopyProfileResponse|bool
     */
    public function CopyProfile(\ID3Global\Models\CopyProfile $parameters)
    {
        try {
            $this->setResult($resultCopyProfile = $this->getSoapClient()->__soapCall('CopyProfile', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCopyProfile;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\CopyProfileResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
