<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get Services
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named GetPINSequence
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetPINSequence $parameters
     * @return \ID3Global\Models\GetPINSequenceResponse|bool
     */
    public function GetPINSequence(\ID3Global\Models\GetPINSequence $parameters)
    {
        try {
            $this->setResult($resultGetPINSequence = $this->getSoapClient()->__soapCall('GetPINSequence', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPINSequence;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPasswordPolicyForUser
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetPasswordPolicyForUser $parameters
     * @return \ID3Global\Models\GetPasswordPolicyForUserResponse|bool
     */
    public function GetPasswordPolicyForUser(\ID3Global\Models\GetPasswordPolicyForUser $parameters)
    {
        try {
            $this->setResult($resultGetPasswordPolicyForUser = $this->getSoapClient()->__soapCall('GetPasswordPolicyForUser', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPasswordPolicyForUser;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDocImageMinSizeKBytes
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDocImageMinSizeKBytes $parameters
     * @return \ID3Global\Models\GetDocImageMinSizeKBytesResponse|bool
     */
    public function GetDocImageMinSizeKBytes(\ID3Global\Models\GetDocImageMinSizeKBytes $parameters)
    {
        try {
            $this->setResult($resultGetDocImageMinSizeKBytes = $this->getSoapClient()->__soapCall('GetDocImageMinSizeKBytes', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDocImageMinSizeKBytes;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDocImageMaxSizeKBytes
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDocImageMaxSizeKBytes $parameters
     * @return \ID3Global\Models\GetDocImageMaxSizeKBytesResponse|bool
     */
    public function GetDocImageMaxSizeKBytes(\ID3Global\Models\GetDocImageMaxSizeKBytes $parameters)
    {
        try {
            $this->setResult($resultGetDocImageMaxSizeKBytes = $this->getSoapClient()->__soapCall('GetDocImageMaxSizeKBytes', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDocImageMaxSizeKBytes;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDocImageMinWidthResolution
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDocImageMinWidthResolution $parameters
     * @return \ID3Global\Models\GetDocImageMinWidthResolutionResponse|bool
     */
    public function GetDocImageMinWidthResolution(\ID3Global\Models\GetDocImageMinWidthResolution $parameters)
    {
        try {
            $this->setResult($resultGetDocImageMinWidthResolution = $this->getSoapClient()->__soapCall('GetDocImageMinWidthResolution', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDocImageMinWidthResolution;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDocImageMinHeightResolution
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDocImageMinHeightResolution $parameters
     * @return \ID3Global\Models\GetDocImageMinHeightResolutionResponse|bool
     */
    public function GetDocImageMinHeightResolution(\ID3Global\Models\GetDocImageMinHeightResolution $parameters)
    {
        try {
            $this->setResult($resultGetDocImageMinHeightResolution = $this->getSoapClient()->__soapCall('GetDocImageMinHeightResolution', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDocImageMinHeightResolution;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDocImageSupportedFileTypes
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDocImageSupportedFileTypes $parameters
     * @return \ID3Global\Models\GetDocImageSupportedFileTypesResponse|bool
     */
    public function GetDocImageSupportedFileTypes(\ID3Global\Models\GetDocImageSupportedFileTypes $parameters)
    {
        try {
            $this->setResult($resultGetDocImageSupportedFileTypes = $this->getSoapClient()->__soapCall('GetDocImageSupportedFileTypes', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDocImageSupportedFileTypes;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPassportCountries
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetPassportCountries $parameters
     * @return \ID3Global\Models\GetPassportCountriesResponse|bool
     */
    public function GetPassportCountries(\ID3Global\Models\GetPassportCountries $parameters)
    {
        try {
            $this->setResult($resultGetPassportCountries = $this->getSoapClient()->__soapCall('GetPassportCountries', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPassportCountries;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetIdentityCardIssuingCountries
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetIdentityCardIssuingCountries $parameters
     * @return \ID3Global\Models\GetIdentityCardIssuingCountriesResponse|bool
     */
    public function GetIdentityCardIssuingCountries(\ID3Global\Models\GetIdentityCardIssuingCountries $parameters)
    {
        try {
            $this->setResult($resultGetIdentityCardIssuingCountries = $this->getSoapClient()->__soapCall('GetIdentityCardIssuingCountries', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetIdentityCardIssuingCountries;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named
     * GetIdentityCardNationalityCountries
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetIdentityCardNationalityCountries $parameters
     * @return \ID3Global\Models\GetIdentityCardNationalityCountriesResponse|bool
     */
    public function GetIdentityCardNationalityCountries(\ID3Global\Models\GetIdentityCardNationalityCountries $parameters)
    {
        try {
            $this->setResult($resultGetIdentityCardNationalityCountries = $this->getSoapClient()->__soapCall('GetIdentityCardNationalityCountries', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetIdentityCardNationalityCountries;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetLocationCountries
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetLocationCountries $parameters
     * @return \ID3Global\Models\GetLocationCountriesResponse|bool
     */
    public function GetLocationCountries(\ID3Global\Models\GetLocationCountries $parameters)
    {
        try {
            $this->setResult($resultGetLocationCountries = $this->getSoapClient()->__soapCall('GetLocationCountries', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetLocationCountries;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetLocationStates
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetLocationStates $parameters
     * @return \ID3Global\Models\GetLocationStatesResponse|bool
     */
    public function GetLocationStates(\ID3Global\Models\GetLocationStates $parameters)
    {
        try {
            $this->setResult($resultGetLocationStates = $this->getSoapClient()->__soapCall('GetLocationStates', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetLocationStates;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetItalyProvinceOfBirth
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetItalyProvinceOfBirth $parameters
     * @return \ID3Global\Models\GetItalyProvinceOfBirthResponse|bool
     */
    public function GetItalyProvinceOfBirth(\ID3Global\Models\GetItalyProvinceOfBirth $parameters)
    {
        try {
            $this->setResult($resultGetItalyProvinceOfBirth = $this->getSoapClient()->__soapCall('GetItalyProvinceOfBirth', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetItalyProvinceOfBirth;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetItemCheckResultCodes
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetItemCheckResultCodes $parameters
     * @return \ID3Global\Models\GetItemCheckResultCodesResponse|bool
     */
    public function GetItemCheckResultCodes(\ID3Global\Models\GetItemCheckResultCodes $parameters)
    {
        try {
            $this->setResult($resultGetItemCheckResultCodes = $this->getSoapClient()->__soapCall('GetItemCheckResultCodes', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetItemCheckResultCodes;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSanctionsIssuers
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetSanctionsIssuers $parameters
     * @return \ID3Global\Models\GetSanctionsIssuersResponse|bool
     */
    public function GetSanctionsIssuers(\ID3Global\Models\GetSanctionsIssuers $parameters)
    {
        try {
            $this->setResult($resultGetSanctionsIssuers = $this->getSoapClient()->__soapCall('GetSanctionsIssuers', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSanctionsIssuers;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSearchPurposes
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetSearchPurposes $parameters
     * @return \ID3Global\Models\GetSearchPurposesResponse|bool
     */
    public function GetSearchPurposes(\ID3Global\Models\GetSearchPurposes $parameters)
    {
        try {
            $this->setResult($resultGetSearchPurposes = $this->getSoapClient()->__soapCall('GetSearchPurposes', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSearchPurposes;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetFCRASanctionsIssuers
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetFCRASanctionsIssuers $parameters
     * @return \ID3Global\Models\GetFCRASanctionsIssuersResponse|bool
     */
    public function GetFCRASanctionsIssuers(\ID3Global\Models\GetFCRASanctionsIssuers $parameters)
    {
        try {
            $this->setResult($resultGetFCRASanctionsIssuers = $this->getSoapClient()->__soapCall('GetFCRASanctionsIssuers', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetFCRASanctionsIssuers;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCardName
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCardName $parameters
     * @return \ID3Global\Models\GetCardNameResponse|bool
     */
    public function GetCardName(\ID3Global\Models\GetCardName $parameters)
    {
        try {
            $this->setResult($resultGetCardName = $this->getSoapClient()->__soapCall('GetCardName', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCardName;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDocumentTypes
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDocumentTypes $parameters
     * @return \ID3Global\Models\GetDocumentTypesResponse|bool
     */
    public function GetDocumentTypes(\ID3Global\Models\GetDocumentTypes $parameters)
    {
        try {
            $this->setResult($resultGetDocumentTypes = $this->getSoapClient()->__soapCall('GetDocumentTypes', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDocumentTypes;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetExtractVersions
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetExtractVersions $parameters
     * @return \ID3Global\Models\GetExtractVersionsResponse|bool
     */
    public function GetExtractVersions(\ID3Global\Models\GetExtractVersions $parameters)
    {
        try {
            $this->setResult($resultGetExtractVersions = $this->getSoapClient()->__soapCall('GetExtractVersions', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetExtractVersions;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetExtractFormats
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetExtractFormats $parameters
     * @return \ID3Global\Models\GetExtractFormatsResponse|bool
     */
    public function GetExtractFormats(\ID3Global\Models\GetExtractFormats $parameters)
    {
        try {
            $this->setResult($resultGetExtractFormats = $this->getSoapClient()->__soapCall('GetExtractFormats', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetExtractFormats;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSouthAfricaCreditSuppliers
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetSouthAfricaCreditSuppliers $parameters
     * @return \ID3Global\Models\GetSouthAfricaCreditSuppliersResponse|bool
     */
    public function GetSouthAfricaCreditSuppliers(\ID3Global\Models\GetSouthAfricaCreditSuppliers $parameters)
    {
        try {
            $this->setResult($resultGetSouthAfricaCreditSuppliers = $this->getSoapClient()->__soapCall('GetSouthAfricaCreditSuppliers', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSouthAfricaCreditSuppliers;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDVLADrivingLicenceEndorsements
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDVLADrivingLicenceEndorsements $parameters
     * @return \ID3Global\Models\GetDVLADrivingLicenceEndorsementsResponse|bool
     */
    public function GetDVLADrivingLicenceEndorsements(\ID3Global\Models\GetDVLADrivingLicenceEndorsements $parameters)
    {
        try {
            $this->setResult($resultGetDVLADrivingLicenceEndorsements = $this->getSoapClient()->__soapCall('GetDVLADrivingLicenceEndorsements', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDVLADrivingLicenceEndorsements;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPEPTiers
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetPEPTiers $parameters
     * @return \ID3Global\Models\GetPEPTiersResponse|bool
     */
    public function GetPEPTiers(\ID3Global\Models\GetPEPTiers $parameters)
    {
        try {
            $this->setResult($resultGetPEPTiers = $this->getSoapClient()->__soapCall('GetPEPTiers', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPEPTiers;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCIFASProductCode
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCIFASProductCode $parameters
     * @return \ID3Global\Models\GetCIFASProductCodeResponse|bool
     */
    public function GetCIFASProductCode(\ID3Global\Models\GetCIFASProductCode $parameters)
    {
        try {
            $this->setResult($resultGetCIFASProductCode = $this->getSoapClient()->__soapCall('GetCIFASProductCode', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCIFASProductCode;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCreditHeaderAccountTypes
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCreditHeaderAccountTypes $parameters
     * @return \ID3Global\Models\GetCreditHeaderAccountTypesResponse|bool
     */
    public function GetCreditHeaderAccountTypes(\ID3Global\Models\GetCreditHeaderAccountTypes $parameters)
    {
        try {
            $this->setResult($resultGetCreditHeaderAccountTypes = $this->getSoapClient()->__soapCall('GetCreditHeaderAccountTypes', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCreditHeaderAccountTypes;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetImageResults
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetImageResults $parameters
     * @return \ID3Global\Models\GetImageResultsResponse|bool
     */
    public function GetImageResults(\ID3Global\Models\GetImageResults $parameters)
    {
        try {
            $this->setResult($resultGetImageResults = $this->getSoapClient()->__soapCall('GetImageResults', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetImageResults;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetImages
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetImages $parameters
     * @return \ID3Global\Models\GetImagesResponse|bool
     */
    public function GetImages(\ID3Global\Models\GetImages $parameters)
    {
        try {
            $this->setResult($resultGetImages = $this->getSoapClient()->__soapCall('GetImages', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetImages;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetImageDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetImageDetails $parameters
     * @return \ID3Global\Models\GetImageDetailsResponse|bool
     */
    public function GetImageDetails(\ID3Global\Models\GetImageDetails $parameters)
    {
        try {
            $this->setResult($resultGetImageDetails = $this->getSoapClient()->__soapCall('GetImageDetails', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetImageDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDocumentExtract
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDocumentExtract $parameters
     * @return \ID3Global\Models\GetDocumentExtractResponse|bool
     */
    public function GetDocumentExtract(\ID3Global\Models\GetDocumentExtract $parameters)
    {
        try {
            $this->setResult($resultGetDocumentExtract = $this->getSoapClient()->__soapCall('GetDocumentExtract', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDocumentExtract;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProfileVersions
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetProfileVersions $parameters
     * @return \ID3Global\Models\GetProfileVersionsResponse|bool
     */
    public function GetProfileVersions(\ID3Global\Models\GetProfileVersions $parameters)
    {
        try {
            $this->setResult($resultGetProfileVersions = $this->getSoapClient()->__soapCall('GetProfileVersions', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetProfileVersions;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProfileVersionFlags
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetProfileVersionFlags $parameters
     * @return \ID3Global\Models\GetProfileVersionFlagsResponse|bool
     */
    public function GetProfileVersionFlags(\ID3Global\Models\GetProfileVersionFlags $parameters)
    {
        try {
            $this->setResult($resultGetProfileVersionFlags = $this->getSoapClient()->__soapCall('GetProfileVersionFlags', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetProfileVersionFlags;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProfileVersionItemIDs
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetProfileVersionItemIDs $parameters
     * @return \ID3Global\Models\GetProfileVersionItemIDsResponse|bool
     */
    public function GetProfileVersionItemIDs(\ID3Global\Models\GetProfileVersionItemIDs $parameters)
    {
        try {
            $this->setResult($resultGetProfileVersionItemIDs = $this->getSoapClient()->__soapCall('GetProfileVersionItemIDs', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetProfileVersionItemIDs;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCardTypes
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCardTypes $parameters
     * @return \ID3Global\Models\GetCardTypesResponse|bool
     */
    public function GetCardTypes(\ID3Global\Models\GetCardTypes $parameters)
    {
        try {
            $this->setResult($resultGetCardTypes = $this->getSoapClient()->__soapCall('GetCardTypes', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCardTypes;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCardTypesMP
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCardTypesMP $parameters
     * @return \ID3Global\Models\GetCardTypesMPResponse|bool
     */
    public function GetCardTypesMP(\ID3Global\Models\GetCardTypesMP $parameters)
    {
        try {
            $this->setResult($resultGetCardTypesMP = $this->getSoapClient()->__soapCall('GetCardTypesMP', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCardTypesMP;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProfiles
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetProfiles $parameters
     * @return \ID3Global\Models\GetProfilesResponse|bool
     */
    public function GetProfiles(\ID3Global\Models\GetProfiles $parameters)
    {
        try {
            $this->setResult($resultGetProfiles = $this->getSoapClient()->__soapCall('GetProfiles', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetProfiles;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProfileChain
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetProfileChain $parameters
     * @return \ID3Global\Models\GetProfileChainResponse|bool
     */
    public function GetProfileChain(\ID3Global\Models\GetProfileChain $parameters)
    {
        try {
            $this->setResult($resultGetProfileChain = $this->getSoapClient()->__soapCall('GetProfileChain', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetProfileChain;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProfileDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetProfileDetails $parameters
     * @return \ID3Global\Models\GetProfileDetailsResponse|bool
     */
    public function GetProfileDetails(\ID3Global\Models\GetProfileDetails $parameters)
    {
        try {
            $this->setResult($resultGetProfileDetails = $this->getSoapClient()->__soapCall('GetProfileDetails', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetProfileDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named
     * GetProfileDetailsWithLatestRevision
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetProfileDetailsWithLatestRevision $parameters
     * @return \ID3Global\Models\GetProfileDetailsWithLatestRevisionResponse|bool
     */
    public function GetProfileDetailsWithLatestRevision(\ID3Global\Models\GetProfileDetailsWithLatestRevision $parameters)
    {
        try {
            $this->setResult($resultGetProfileDetailsWithLatestRevision = $this->getSoapClient()->__soapCall('GetProfileDetailsWithLatestRevision', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetProfileDetailsWithLatestRevision;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetLicence
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetLicence $parameters
     * @return \ID3Global\Models\GetLicenceResponse|bool
     */
    public function GetLicence(\ID3Global\Models\GetLicence $parameters)
    {
        try {
            $this->setResult($resultGetLicence = $this->getSoapClient()->__soapCall('GetLicence', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetLicence;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetItemCheckProperties
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetItemCheckProperties $parameters
     * @return \ID3Global\Models\GetItemCheckPropertiesResponse|bool
     */
    public function GetItemCheckProperties(\ID3Global\Models\GetItemCheckProperties $parameters)
    {
        try {
            $this->setResult($resultGetItemCheckProperties = $this->getSoapClient()->__soapCall('GetItemCheckProperties', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetItemCheckProperties;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetItemChecks
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetItemChecks $parameters
     * @return \ID3Global\Models\GetItemChecksResponse|bool
     */
    public function GetItemChecks(\ID3Global\Models\GetItemChecks $parameters)
    {
        try {
            $this->setResult($resultGetItemChecks = $this->getSoapClient()->__soapCall('GetItemChecks', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetItemChecks;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetInputFieldByItemCheckId
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetInputFieldByItemCheckId $parameters
     * @return \ID3Global\Models\GetInputFieldByItemCheckIdResponse|bool
     */
    public function GetInputFieldByItemCheckId(\ID3Global\Models\GetInputFieldByItemCheckId $parameters)
    {
        try {
            $this->setResult($resultGetInputFieldByItemCheckId = $this->getSoapClient()->__soapCall('GetInputFieldByItemCheckId', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetInputFieldByItemCheckId;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetRetiringProfiles
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetRetiringProfiles $parameters
     * @return \ID3Global\Models\GetRetiringProfilesResponse|bool
     */
    public function GetRetiringProfiles(\ID3Global\Models\GetRetiringProfiles $parameters)
    {
        try {
            $this->setResult($resultGetRetiringProfiles = $this->getSoapClient()->__soapCall('GetRetiringProfiles', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetRetiringProfiles;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAuthenticationDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetAuthenticationDetails $parameters
     * @return \ID3Global\Models\GetAuthenticationDetailsResponse|bool
     */
    public function GetAuthenticationDetails(\ID3Global\Models\GetAuthenticationDetails $parameters)
    {
        try {
            $this->setResult($resultGetAuthenticationDetails = $this->getSoapClient()->__soapCall('GetAuthenticationDetails', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAuthenticationDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetLatestAuthenticationDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetLatestAuthenticationDetails $parameters
     * @return \ID3Global\Models\GetLatestAuthenticationDetailsResponse|bool
     */
    public function GetLatestAuthenticationDetails(\ID3Global\Models\GetLatestAuthenticationDetails $parameters)
    {
        try {
            $this->setResult($resultGetLatestAuthenticationDetails = $this->getSoapClient()->__soapCall('GetLatestAuthenticationDetails', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetLatestAuthenticationDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAuthentications
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetAuthentications $parameters
     * @return \ID3Global\Models\GetAuthenticationsResponse|bool
     */
    public function GetAuthentications(\ID3Global\Models\GetAuthentications $parameters)
    {
        try {
            $this->setResult($resultGetAuthentications = $this->getSoapClient()->__soapCall('GetAuthentications', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAuthentications;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSanctionsEnforcementsData
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetSanctionsEnforcementsData $parameters
     * @return \ID3Global\Models\GetSanctionsEnforcementsDataResponse|bool
     */
    public function GetSanctionsEnforcementsData(\ID3Global\Models\GetSanctionsEnforcementsData $parameters)
    {
        try {
            $this->setResult($resultGetSanctionsEnforcementsData = $this->getSoapClient()->__soapCall('GetSanctionsEnforcementsData', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSanctionsEnforcementsData;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPEPIntelligenceData
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetPEPIntelligenceData $parameters
     * @return \ID3Global\Models\GetPEPIntelligenceDataResponse|bool
     */
    public function GetPEPIntelligenceData(\ID3Global\Models\GetPEPIntelligenceData $parameters)
    {
        try {
            $this->setResult($resultGetPEPIntelligenceData = $this->getSoapClient()->__soapCall('GetPEPIntelligenceData', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPEPIntelligenceData;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDataExtract
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDataExtract $parameters
     * @return \ID3Global\Models\GetDataExtractResponse|bool
     */
    public function GetDataExtract(\ID3Global\Models\GetDataExtract $parameters)
    {
        try {
            $this->setResult($resultGetDataExtract = $this->getSoapClient()->__soapCall('GetDataExtract', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDataExtract;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDataExtracts
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDataExtracts $parameters
     * @return \ID3Global\Models\GetDataExtractsResponse|bool
     */
    public function GetDataExtracts(\ID3Global\Models\GetDataExtracts $parameters)
    {
        try {
            $this->setResult($resultGetDataExtracts = $this->getSoapClient()->__soapCall('GetDataExtracts', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDataExtracts;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDataExtractHistory
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDataExtractHistory $parameters
     * @return \ID3Global\Models\GetDataExtractHistoryResponse|bool
     */
    public function GetDataExtractHistory(\ID3Global\Models\GetDataExtractHistory $parameters)
    {
        try {
            $this->setResult($resultGetDataExtractHistory = $this->getSoapClient()->__soapCall('GetDataExtractHistory', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDataExtractHistory;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetReports
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetReports $parameters
     * @return \ID3Global\Models\GetReportsResponse|bool
     */
    public function GetReports(\ID3Global\Models\GetReports $parameters)
    {
        try {
            $this->setResult($resultGetReports = $this->getSoapClient()->__soapCall('GetReports', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetReports;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetReportParameters
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetReportParameters $parameters
     * @return \ID3Global\Models\GetReportParametersResponse|bool
     */
    public function GetReportParameters(\ID3Global\Models\GetReportParameters $parameters)
    {
        try {
            $this->setResult($resultGetReportParameters = $this->getSoapClient()->__soapCall('GetReportParameters', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetReportParameters;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetReport
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetReport $parameters
     * @return \ID3Global\Models\GetReportResponse|bool
     */
    public function GetReport(\ID3Global\Models\GetReport $parameters)
    {
        try {
            $this->setResult($resultGetReport = $this->getSoapClient()->__soapCall('GetReport', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetReport;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetReportImage
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetReportImage $parameters
     * @return \ID3Global\Models\GetReportImageResponse|bool
     */
    public function GetReportImage(\ID3Global\Models\GetReportImage $parameters)
    {
        try {
            $this->setResult($resultGetReportImage = $this->getSoapClient()->__soapCall('GetReportImage', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetReportImage;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDetailedInvestigation
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDetailedInvestigation $parameters
     * @return \ID3Global\Models\GetDetailedInvestigationResponse|bool
     */
    public function GetDetailedInvestigation(\ID3Global\Models\GetDetailedInvestigation $parameters)
    {
        try {
            $this->setResult($resultGetDetailedInvestigation = $this->getSoapClient()->__soapCall('GetDetailedInvestigation', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDetailedInvestigation;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetFCRAMatchDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetFCRAMatchDetails $parameters
     * @return \ID3Global\Models\GetFCRAMatchDetailsResponse|bool
     */
    public function GetFCRAMatchDetails(\ID3Global\Models\GetFCRAMatchDetails $parameters)
    {
        try {
            $this->setResult($resultGetFCRAMatchDetails = $this->getSoapClient()->__soapCall('GetFCRAMatchDetails', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetFCRAMatchDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named
     * GetSupplierExtendedStatusInformation
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetSupplierExtendedStatusInformation $parameters
     * @return \ID3Global\Models\GetSupplierExtendedStatusInformationResponse|bool
     */
    public function GetSupplierExtendedStatusInformation(\ID3Global\Models\GetSupplierExtendedStatusInformation $parameters)
    {
        try {
            $this->setResult($resultGetSupplierExtendedStatusInformation = $this->getSoapClient()->__soapCall('GetSupplierExtendedStatusInformation', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSupplierExtendedStatusInformation;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAccountExtracts
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetAccountExtracts $parameters
     * @return \ID3Global\Models\GetAccountExtractsResponse|bool
     */
    public function GetAccountExtracts(\ID3Global\Models\GetAccountExtracts $parameters)
    {
        try {
            $this->setResult($resultGetAccountExtracts = $this->getSoapClient()->__soapCall('GetAccountExtracts', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAccountExtracts;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetUserAccountExtractHistory
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetUserAccountExtractHistory $parameters
     * @return \ID3Global\Models\GetUserAccountExtractHistoryResponse|bool
     */
    public function GetUserAccountExtractHistory(\ID3Global\Models\GetUserAccountExtractHistory $parameters)
    {
        try {
            $this->setResult($resultGetUserAccountExtractHistory = $this->getSoapClient()->__soapCall('GetUserAccountExtractHistory', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetUserAccountExtractHistory;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDetailedInvestigationPDF
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetDetailedInvestigationPDF $parameters
     * @return \ID3Global\Models\GetDetailedInvestigationPDFResponse|bool
     */
    public function GetDetailedInvestigationPDF(\ID3Global\Models\GetDetailedInvestigationPDF $parameters)
    {
        try {
            $this->setResult($resultGetDetailedInvestigationPDF = $this->getSoapClient()->__soapCall('GetDetailedInvestigationPDF', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDetailedInvestigationPDF;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetOrganisations
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetOrganisations $parameters
     * @return \ID3Global\Models\GetOrganisationsResponse|bool
     */
    public function GetOrganisations(\ID3Global\Models\GetOrganisations $parameters)
    {
        try {
            $this->setResult($resultGetOrganisations = $this->getSoapClient()->__soapCall('GetOrganisations', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetOrganisations;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetOrganisationDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetOrganisationDetails $parameters
     * @return \ID3Global\Models\GetOrganisationDetailsResponse|bool
     */
    public function GetOrganisationDetails(\ID3Global\Models\GetOrganisationDetails $parameters)
    {
        try {
            $this->setResult($resultGetOrganisationDetails = $this->getSoapClient()->__soapCall('GetOrganisationDetails', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetOrganisationDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetHome
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetHome $parameters
     * @return \ID3Global\Models\GetHomeResponse|bool
     */
    public function GetHome(\ID3Global\Models\GetHome $parameters)
    {
        try {
            $this->setResult($resultGetHome = $this->getSoapClient()->__soapCall('GetHome', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetHome;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAccounts
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetAccounts $parameters
     * @return \ID3Global\Models\GetAccountsResponse|bool
     */
    public function GetAccounts(\ID3Global\Models\GetAccounts $parameters)
    {
        try {
            $this->setResult($resultGetAccounts = $this->getSoapClient()->__soapCall('GetAccounts', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAccounts;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAccountDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetAccountDetails $parameters
     * @return \ID3Global\Models\GetAccountDetailsResponse|bool
     */
    public function GetAccountDetails(\ID3Global\Models\GetAccountDetails $parameters)
    {
        try {
            $this->setResult($resultGetAccountDetails = $this->getSoapClient()->__soapCall('GetAccountDetails', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAccountDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetOrgAdminAccountID
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetOrgAdminAccountID $parameters
     * @return \ID3Global\Models\GetOrgAdminAccountIDResponse|bool
     */
    public function GetOrgAdminAccountID(\ID3Global\Models\GetOrgAdminAccountID $parameters)
    {
        try {
            $this->setResult($resultGetOrgAdminAccountID = $this->getSoapClient()->__soapCall('GetOrgAdminAccountID', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetOrgAdminAccountID;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAccountRoles
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetAccountRoles $parameters
     * @return \ID3Global\Models\GetAccountRolesResponse|bool
     */
    public function GetAccountRoles(\ID3Global\Models\GetAccountRoles $parameters)
    {
        try {
            $this->setResult($resultGetAccountRoles = $this->getSoapClient()->__soapCall('GetAccountRoles', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAccountRoles;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAccountPermission
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetAccountPermission $parameters
     * @return \ID3Global\Models\GetAccountPermissionResponse|bool
     */
    public function GetAccountPermission(\ID3Global\Models\GetAccountPermission $parameters)
    {
        try {
            $this->setResult($resultGetAccountPermission = $this->getSoapClient()->__soapCall('GetAccountPermission', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAccountPermission;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetRoles
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetRoles $parameters
     * @return \ID3Global\Models\GetRolesResponse|bool
     */
    public function GetRoles(\ID3Global\Models\GetRoles $parameters)
    {
        try {
            $this->setResult($resultGetRoles = $this->getSoapClient()->__soapCall('GetRoles', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetRoles;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetRoleMembers
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetRoleMembers $parameters
     * @return \ID3Global\Models\GetRoleMembersResponse|bool
     */
    public function GetRoleMembers(\ID3Global\Models\GetRoleMembers $parameters)
    {
        try {
            $this->setResult($resultGetRoleMembers = $this->getSoapClient()->__soapCall('GetRoleMembers', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetRoleMembers;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetRolePermissions
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetRolePermissions $parameters
     * @return \ID3Global\Models\GetRolePermissionsResponse|bool
     */
    public function GetRolePermissions(\ID3Global\Models\GetRolePermissions $parameters)
    {
        try {
            $this->setResult($resultGetRolePermissions = $this->getSoapClient()->__soapCall('GetRolePermissions', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetRolePermissions;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPermissions
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetPermissions $parameters
     * @return \ID3Global\Models\GetPermissionsResponse|bool
     */
    public function GetPermissions(\ID3Global\Models\GetPermissions $parameters)
    {
        try {
            $this->setResult($resultGetPermissions = $this->getSoapClient()->__soapCall('GetPermissions', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPermissions;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSuppliers
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetSuppliers $parameters
     * @return \ID3Global\Models\GetSuppliersResponse|bool
     */
    public function GetSuppliers(\ID3Global\Models\GetSuppliers $parameters)
    {
        try {
            $this->setResult($resultGetSuppliers = $this->getSoapClient()->__soapCall('GetSuppliers', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSuppliers;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSupplierFlags
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetSupplierFlags $parameters
     * @return \ID3Global\Models\GetSupplierFlagsResponse|bool
     */
    public function GetSupplierFlags(\ID3Global\Models\GetSupplierFlags $parameters)
    {
        try {
            $this->setResult($resultGetSupplierFlags = $this->getSoapClient()->__soapCall('GetSupplierFlags', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSupplierFlags;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSupplierAccounts
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetSupplierAccounts $parameters
     * @return \ID3Global\Models\GetSupplierAccountsResponse|bool
     */
    public function GetSupplierAccounts(\ID3Global\Models\GetSupplierAccounts $parameters)
    {
        try {
            $this->setResult($resultGetSupplierAccounts = $this->getSoapClient()->__soapCall('GetSupplierAccounts', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSupplierAccounts;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSupplierAccount
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetSupplierAccount $parameters
     * @return \ID3Global\Models\GetSupplierAccountResponse|bool
     */
    public function GetSupplierAccount(\ID3Global\Models\GetSupplierAccount $parameters)
    {
        try {
            $this->setResult($resultGetSupplierAccount = $this->getSoapClient()->__soapCall('GetSupplierAccount', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSupplierAccount;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSupplierFieldsBySupplierId
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetSupplierFieldsBySupplierId $parameters
     * @return \ID3Global\Models\GetSupplierFieldsBySupplierIdResponse|bool
     */
    public function GetSupplierFieldsBySupplierId(\ID3Global\Models\GetSupplierFieldsBySupplierId $parameters)
    {
        try {
            $this->setResult($resultGetSupplierFieldsBySupplierId = $this->getSoapClient()->__soapCall('GetSupplierFieldsBySupplierId', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSupplierFieldsBySupplierId;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSupplierConfig
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetSupplierConfig $parameters
     * @return \ID3Global\Models\GetSupplierConfigResponse|bool
     */
    public function GetSupplierConfig(\ID3Global\Models\GetSupplierConfig $parameters)
    {
        try {
            $this->setResult($resultGetSupplierConfig = $this->getSoapClient()->__soapCall('GetSupplierConfig', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetSupplierConfig;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetGlobalCaseProfileVersions
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetGlobalCaseProfileVersions $parameters
     * @return \ID3Global\Models\GetGlobalCaseProfileVersionsResponse|bool
     */
    public function GetGlobalCaseProfileVersions(\ID3Global\Models\GetGlobalCaseProfileVersions $parameters)
    {
        try {
            $this->setResult($resultGetGlobalCaseProfileVersions = $this->getSoapClient()->__soapCall('GetGlobalCaseProfileVersions', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetGlobalCaseProfileVersions;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCaseNotifications
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCaseNotifications $parameters
     * @return \ID3Global\Models\GetCaseNotificationsResponse|bool
     */
    public function GetCaseNotifications(\ID3Global\Models\GetCaseNotifications $parameters)
    {
        try {
            $this->setResult($resultGetCaseNotifications = $this->getSoapClient()->__soapCall('GetCaseNotifications', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCaseNotifications;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCaseNotification
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCaseNotification $parameters
     * @return \ID3Global\Models\GetCaseNotificationResponse|bool
     */
    public function GetCaseNotification(\ID3Global\Models\GetCaseNotification $parameters)
    {
        try {
            $this->setResult($resultGetCaseNotification = $this->getSoapClient()->__soapCall('GetCaseNotification', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCaseNotification;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCustomerSettings
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCustomerSettings $parameters
     * @return \ID3Global\Models\GetCustomerSettingsResponse|bool
     */
    public function GetCustomerSettings(\ID3Global\Models\GetCustomerSettings $parameters)
    {
        try {
            $this->setResult($resultGetCustomerSettings = $this->getSoapClient()->__soapCall('GetCustomerSettings', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCustomerSettings;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCases
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCases $parameters
     * @return \ID3Global\Models\GetCasesResponse|bool
     */
    public function GetCases(\ID3Global\Models\GetCases $parameters)
    {
        try {
            $this->setResult($resultGetCases = $this->getSoapClient()->__soapCall('GetCases', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCases;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCaseDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCaseDetails $parameters
     * @return \ID3Global\Models\GetCaseDetailsResponse|bool
     */
    public function GetCaseDetails(\ID3Global\Models\GetCaseDetails $parameters)
    {
        try {
            $this->setResult($resultGetCaseDetails = $this->getSoapClient()->__soapCall('GetCaseDetails', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCaseDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCaseInputData
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCaseInputData $parameters
     * @return \ID3Global\Models\GetCaseInputDataResponse|bool
     */
    public function GetCaseInputData(\ID3Global\Models\GetCaseInputData $parameters)
    {
        try {
            $this->setResult($resultGetCaseInputData = $this->getSoapClient()->__soapCall('GetCaseInputData', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCaseInputData;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCaseAuditRecord
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\GetCaseAuditRecord $parameters
     * @return \ID3Global\Models\GetCaseAuditRecordResponse|bool
     */
    public function GetCaseAuditRecord(\ID3Global\Models\GetCaseAuditRecord $parameters)
    {
        try {
            $this->setResult($resultGetCaseAuditRecord = $this->getSoapClient()->__soapCall('GetCaseAuditRecord', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCaseAuditRecord;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\GetAccountDetailsResponse|\ID3Global\Models\GetAccountExtractsResponse|\ID3Global\Models\GetAccountPermissionResponse|\ID3Global\Models\GetAccountRolesResponse|\ID3Global\Models\GetAccountsResponse|\ID3Global\Models\GetAuthenticationDetailsResponse|\ID3Global\Models\GetAuthenticationsResponse|\ID3Global\Models\GetCardNameResponse|\ID3Global\Models\GetCardTypesMPResponse|\ID3Global\Models\GetCardTypesResponse|\ID3Global\Models\GetCaseAuditRecordResponse|\ID3Global\Models\GetCaseDetailsResponse|\ID3Global\Models\GetCaseInputDataResponse|\ID3Global\Models\GetCaseNotificationResponse|\ID3Global\Models\GetCaseNotificationsResponse|\ID3Global\Models\GetCasesResponse|\ID3Global\Models\GetCIFASProductCodeResponse|\ID3Global\Models\GetCreditHeaderAccountTypesResponse|\ID3Global\Models\GetCustomerSettingsResponse|\ID3Global\Models\GetDataExtractHistoryResponse|\ID3Global\Models\GetDataExtractResponse|\ID3Global\Models\GetDataExtractsResponse|\ID3Global\Models\GetDetailedInvestigationPDFResponse|\ID3Global\Models\GetDetailedInvestigationResponse|\ID3Global\Models\GetDocImageMaxSizeKBytesResponse|\ID3Global\Models\GetDocImageMinHeightResolutionResponse|\ID3Global\Models\GetDocImageMinSizeKBytesResponse|\ID3Global\Models\GetDocImageMinWidthResolutionResponse|\ID3Global\Models\GetDocImageSupportedFileTypesResponse|\ID3Global\Models\GetDocumentExtractResponse|\ID3Global\Models\GetDocumentTypesResponse|\ID3Global\Models\GetDVLADrivingLicenceEndorsementsResponse|\ID3Global\Models\GetExtractFormatsResponse|\ID3Global\Models\GetExtractVersionsResponse|\ID3Global\Models\GetFCRAMatchDetailsResponse|\ID3Global\Models\GetFCRASanctionsIssuersResponse|\ID3Global\Models\GetGlobalCaseProfileVersionsResponse|\ID3Global\Models\GetHomeResponse|\ID3Global\Models\GetIdentityCardIssuingCountriesResponse|\ID3Global\Models\GetIdentityCardNationalityCountriesResponse|\ID3Global\Models\GetImageDetailsResponse|\ID3Global\Models\GetImageResultsResponse|\ID3Global\Models\GetImagesResponse|\ID3Global\Models\GetInputFieldByItemCheckIdResponse|\ID3Global\Models\GetItalyProvinceOfBirthResponse|\ID3Global\Models\GetItemCheckPropertiesResponse|\ID3Global\Models\GetItemCheckResultCodesResponse|\ID3Global\Models\GetItemChecksResponse|\ID3Global\Models\GetLatestAuthenticationDetailsResponse|\ID3Global\Models\GetLicenceResponse|\ID3Global\Models\GetLocationCountriesResponse|\ID3Global\Models\GetLocationStatesResponse|\ID3Global\Models\GetOrgAdminAccountIDResponse|\ID3Global\Models\GetOrganisationDetailsResponse|\ID3Global\Models\GetOrganisationsResponse|\ID3Global\Models\GetPassportCountriesResponse|\ID3Global\Models\GetPasswordPolicyForUserResponse|\ID3Global\Models\GetPEPIntelligenceDataResponse|\ID3Global\Models\GetPEPTiersResponse|\ID3Global\Models\GetPermissionsResponse|\ID3Global\Models\GetPINSequenceResponse|\ID3Global\Models\GetProfileChainResponse|\ID3Global\Models\GetProfileDetailsResponse|\ID3Global\Models\GetProfileDetailsWithLatestRevisionResponse|\ID3Global\Models\GetProfilesResponse|\ID3Global\Models\GetProfileVersionFlagsResponse|\ID3Global\Models\GetProfileVersionItemIDsResponse|\ID3Global\Models\GetProfileVersionsResponse|\ID3Global\Models\GetReportImageResponse|\ID3Global\Models\GetReportParametersResponse|\ID3Global\Models\GetReportResponse|\ID3Global\Models\GetReportsResponse|\ID3Global\Models\GetRetiringProfilesResponse|\ID3Global\Models\GetRoleMembersResponse|\ID3Global\Models\GetRolePermissionsResponse|\ID3Global\Models\GetRolesResponse|\ID3Global\Models\GetSanctionsEnforcementsDataResponse|\ID3Global\Models\GetSanctionsIssuersResponse|\ID3Global\Models\GetSearchPurposesResponse|\ID3Global\Models\GetSouthAfricaCreditSuppliersResponse|\ID3Global\Models\GetSupplierAccountResponse|\ID3Global\Models\GetSupplierAccountsResponse|\ID3Global\Models\GetSupplierConfigResponse|\ID3Global\Models\GetSupplierExtendedStatusInformationResponse|\ID3Global\Models\GetSupplierFieldsBySupplierIdResponse|\ID3Global\Models\GetSupplierFlagsResponse|\ID3Global\Models\GetSuppliersResponse|\ID3Global\Models\GetUserAccountExtractHistoryResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
