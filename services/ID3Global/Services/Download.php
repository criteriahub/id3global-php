<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Download Services
 * @subpackage Services
 */
class Download extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Download
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\Download $parameters
     * @return \ID3Global\Models\DownloadResponse|bool
     */
    public function Download(\ID3Global\Models\Download $parameters)
    {
        try {
            $this->setResult($resultDownload = $this->getSoapClient()->__soapCall('Download', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultDownload;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named DownloadStoredImage
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\DownloadStoredImage $parameters
     * @return \ID3Global\Models\DownloadStoredImageResponse|bool
     */
    public function DownloadStoredImage(\ID3Global\Models\DownloadStoredImage $parameters)
    {
        try {
            $this->setResult($resultDownloadStoredImage = $this->getSoapClient()->__soapCall('DownloadStoredImage', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultDownloadStoredImage;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named DownloadDataExtract
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\DownloadDataExtract $parameters
     * @return \ID3Global\Models\DownloadDataExtractResponse|bool
     */
    public function DownloadDataExtract(\ID3Global\Models\DownloadDataExtract $parameters)
    {
        try {
            $this->setResult($resultDownloadDataExtract = $this->getSoapClient()->__soapCall('DownloadDataExtract', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultDownloadDataExtract;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named DownloadExtract
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\DownloadExtract $parameters
     * @return \ID3Global\Models\DownloadExtractResponse|bool
     */
    public function DownloadExtract(\ID3Global\Models\DownloadExtract $parameters)
    {
        try {
            $this->setResult($resultDownloadExtract = $this->getSoapClient()->__soapCall('DownloadExtract', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultDownloadExtract;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\DownloadDataExtractResponse|\ID3Global\Models\DownloadExtractResponse|\ID3Global\Models\DownloadResponse|\ID3Global\Models\DownloadStoredImageResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
