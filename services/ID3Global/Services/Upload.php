<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Upload Services
 * @subpackage Services
 */
class Upload extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Upload
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\Upload $parameters
     * @return \ID3Global\Models\UploadResponse|bool
     */
    public function Upload(\ID3Global\Models\Upload $parameters)
    {
        try {
            $this->setResult($resultUpload = $this->getSoapClient()->__soapCall('Upload', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpload;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UploadAndProcess
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UploadAndProcess $parameters
     * @return \ID3Global\Models\UploadAndProcessResponse|bool
     */
    public function UploadAndProcess(\ID3Global\Models\UploadAndProcess $parameters)
    {
        try {
            $this->setResult($resultUploadAndProcess = $this->getSoapClient()->__soapCall('UploadAndProcess', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUploadAndProcess;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named
     * UploadAndProcessByProfileProperties
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UploadAndProcessByProfileProperties $parameters
     * @return \ID3Global\Models\UploadAndProcessByProfilePropertiesResponse|bool
     */
    public function UploadAndProcessByProfileProperties(\ID3Global\Models\UploadAndProcessByProfileProperties $parameters)
    {
        try {
            $this->setResult($resultUploadAndProcessByProfileProperties = $this->getSoapClient()->__soapCall('UploadAndProcessByProfileProperties', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUploadAndProcessByProfileProperties;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\UploadAndProcessByProfilePropertiesResponse|\ID3Global\Models\UploadAndProcessResponse|\ID3Global\Models\UploadResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
