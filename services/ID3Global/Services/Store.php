<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Store Services
 * @subpackage Services
 */
class Store extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named StoreDocumentImage
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\StoreDocumentImage $parameters
     * @return \ID3Global\Models\StoreDocumentImageResponse|bool
     */
    public function StoreDocumentImage(\ID3Global\Models\StoreDocumentImage $parameters)
    {
        try {
            $this->setResult($resultStoreDocumentImage = $this->getSoapClient()->__soapCall('StoreDocumentImage', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultStoreDocumentImage;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\StoreDocumentImageResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
