<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Forgot Services
 * @subpackage Services
 */
class Forgot extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named ForgotAccount
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\ForgotAccount $parameters
     * @return \ID3Global\Models\ForgotAccountResponse|bool
     */
    public function ForgotAccount(\ID3Global\Models\ForgotAccount $parameters)
    {
        try {
            $this->setResult($resultForgotAccount = $this->getSoapClient()->__soapCall('ForgotAccount', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultForgotAccount;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named ForgotAccountForProduct
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\ForgotAccountForProduct $parameters
     * @return \ID3Global\Models\ForgotAccountForProductResponse|bool
     */
    public function ForgotAccountForProduct(\ID3Global\Models\ForgotAccountForProduct $parameters)
    {
        try {
            $this->setResult($resultForgotAccountForProduct = $this->getSoapClient()->__soapCall('ForgotAccountForProduct', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultForgotAccountForProduct;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named ForgotPassword
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\ForgotPassword $parameters
     * @return \ID3Global\Models\ForgotPasswordResponse|bool
     */
    public function ForgotPassword(\ID3Global\Models\ForgotPassword $parameters)
    {
        try {
            $this->setResult($resultForgotPassword = $this->getSoapClient()->__soapCall('ForgotPassword', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultForgotPassword;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\ForgotAccountForProductResponse|\ID3Global\Models\ForgotAccountResponse|\ID3Global\Models\ForgotPasswordResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
