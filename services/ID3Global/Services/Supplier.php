<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Supplier Services
 * @subpackage Services
 */
class Supplier extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SupplierUpdate
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\SupplierUpdate $parameters
     * @return \ID3Global\Models\SupplierUpdateResponse|bool
     */
    public function SupplierUpdate(\ID3Global\Models\SupplierUpdate $parameters)
    {
        try {
            $this->setResult($resultSupplierUpdate = $this->getSoapClient()->__soapCall('SupplierUpdate', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSupplierUpdate;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\SupplierUpdateResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
