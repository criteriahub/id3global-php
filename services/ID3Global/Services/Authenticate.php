<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Authenticate Services
 * @subpackage Services
 */
class Authenticate extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named AuthenticateSP
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\AuthenticateSP $parameters
     * @return \ID3Global\Models\AuthenticateSPResponse|bool
     */
    public function AuthenticateSP(\ID3Global\Models\AuthenticateSP $parameters)
    {
        try {
            $this->setResult($resultAuthenticateSP = $this->getSoapClient()->__soapCall('AuthenticateSP', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultAuthenticateSP;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named AuthenticateMP
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\AuthenticateMP $parameters
     * @return \ID3Global\Models\AuthenticateMPResponse|bool
     */
    public function AuthenticateMP(\ID3Global\Models\AuthenticateMP $parameters)
    {
        try {
            $this->setResult($resultAuthenticateMP = $this->getSoapClient()->__soapCall('AuthenticateMP', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultAuthenticateMP;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\AuthenticateMPResponse|\ID3Global\Models\AuthenticateSPResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
