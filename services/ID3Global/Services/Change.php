<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Change Services
 * @subpackage Services
 */
class Change extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named ChangePassword
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\ChangePassword $parameters
     * @return \ID3Global\Models\ChangePasswordResponse|bool
     */
    public function ChangePassword(\ID3Global\Models\ChangePassword $parameters)
    {
        try {
            $this->setResult($resultChangePassword = $this->getSoapClient()->__soapCall('ChangePassword', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultChangePassword;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named ChangePIN
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\ChangePIN $parameters
     * @return \ID3Global\Models\ChangePINResponse|bool
     */
    public function ChangePIN(\ID3Global\Models\ChangePIN $parameters)
    {
        try {
            $this->setResult($resultChangePIN = $this->getSoapClient()->__soapCall('ChangePIN', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultChangePIN;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named ChangePINWithReminder
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\ChangePINWithReminder $parameters
     * @return \ID3Global\Models\ChangePINWithReminderResponse|bool
     */
    public function ChangePINWithReminder(\ID3Global\Models\ChangePINWithReminder $parameters)
    {
        try {
            $this->setResult($resultChangePINWithReminder = $this->getSoapClient()->__soapCall('ChangePINWithReminder', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultChangePINWithReminder;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\ChangePasswordResponse|\ID3Global\Models\ChangePINResponse|\ID3Global\Models\ChangePINWithReminderResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
