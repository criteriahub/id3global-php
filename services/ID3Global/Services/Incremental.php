<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Incremental Services
 * @subpackage Services
 */
class Incremental extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named IncrementalVerification
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\IncrementalVerification $parameters
     * @return \ID3Global\Models\IncrementalVerificationResponse|bool
     */
    public function IncrementalVerification(\ID3Global\Models\IncrementalVerification $parameters)
    {
        try {
            $this->setResult($resultIncrementalVerification = $this->getSoapClient()->__soapCall('IncrementalVerification', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultIncrementalVerification;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\IncrementalVerificationResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
