<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Check Services
 * @subpackage Services
 */
class Check extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named CheckCredentials
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CheckCredentials $parameters
     * @return \ID3Global\Models\CheckCredentialsResponse|bool
     */
    public function CheckCredentials(\ID3Global\Models\CheckCredentials $parameters)
    {
        try {
            $this->setResult($resultCheckCredentials = $this->getSoapClient()->__soapCall('CheckCredentials', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCheckCredentials;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named CheckNonce
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CheckNonce $parameters
     * @return \ID3Global\Models\CheckNonceResponse|bool
     */
    public function CheckNonce(\ID3Global\Models\CheckNonce $parameters)
    {
        try {
            $this->setResult($resultCheckNonce = $this->getSoapClient()->__soapCall('CheckNonce', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCheckNonce;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named CheckCredentialsWithPIN
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CheckCredentialsWithPIN $parameters
     * @return \ID3Global\Models\CheckCredentialsWithPINResponse|bool
     */
    public function CheckCredentialsWithPIN(\ID3Global\Models\CheckCredentialsWithPIN $parameters)
    {
        try {
            $this->setResult($resultCheckCredentialsWithPIN = $this->getSoapClient()->__soapCall('CheckCredentialsWithPIN', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCheckCredentialsWithPIN;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named
     * CheckNewAccountAgainstPasswordPolicies
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CheckNewAccountAgainstPasswordPolicies $parameters
     * @return \ID3Global\Models\CheckNewAccountAgainstPasswordPoliciesResponse|bool
     */
    public function CheckNewAccountAgainstPasswordPolicies(\ID3Global\Models\CheckNewAccountAgainstPasswordPolicies $parameters)
    {
        try {
            $this->setResult($resultCheckNewAccountAgainstPasswordPolicies = $this->getSoapClient()->__soapCall('CheckNewAccountAgainstPasswordPolicies', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCheckNewAccountAgainstPasswordPolicies;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\CheckCredentialsResponse|\ID3Global\Models\CheckCredentialsWithPINResponse|\ID3Global\Models\CheckNewAccountAgainstPasswordPoliciesResponse|\ID3Global\Models\CheckNonceResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
