<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Process Services
 * @subpackage Services
 */
class Process extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named ProcessStoredImage
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\ProcessStoredImage $parameters
     * @return \ID3Global\Models\ProcessStoredImageResponse|bool
     */
    public function ProcessStoredImage(\ID3Global\Models\ProcessStoredImage $parameters)
    {
        try {
            $this->setResult($resultProcessStoredImage = $this->getSoapClient()->__soapCall('ProcessStoredImage', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultProcessStoredImage;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\ProcessStoredImageResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
