<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Force Services
 * @subpackage Services
 */
class Force extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named ForceOverride
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\ForceOverride $parameters
     * @return \ID3Global\Models\ForceOverrideResponse|bool
     */
    public function ForceOverride(\ID3Global\Models\ForceOverride $parameters)
    {
        try {
            $this->setResult($resultForceOverride = $this->getSoapClient()->__soapCall('ForceOverride', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultForceOverride;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\ForceOverrideResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
