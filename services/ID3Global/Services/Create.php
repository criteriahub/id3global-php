<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Create Services
 * @subpackage Services
 */
class Create extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named CreateProfile
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CreateProfile $parameters
     * @return \ID3Global\Models\CreateProfileResponse|bool
     */
    public function CreateProfile(\ID3Global\Models\CreateProfile $parameters)
    {
        try {
            $this->setResult($resultCreateProfile = $this->getSoapClient()->__soapCall('CreateProfile', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateProfile;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreateProfileWithScoringMethod
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CreateProfileWithScoringMethod $parameters
     * @return \ID3Global\Models\CreateProfileWithScoringMethodResponse|bool
     */
    public function CreateProfileWithScoringMethod(\ID3Global\Models\CreateProfileWithScoringMethod $parameters)
    {
        try {
            $this->setResult($resultCreateProfileWithScoringMethod = $this->getSoapClient()->__soapCall('CreateProfileWithScoringMethod', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateProfileWithScoringMethod;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreateDataExtract
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CreateDataExtract $parameters
     * @return \ID3Global\Models\CreateDataExtractResponse|bool
     */
    public function CreateDataExtract(\ID3Global\Models\CreateDataExtract $parameters)
    {
        try {
            $this->setResult($resultCreateDataExtract = $this->getSoapClient()->__soapCall('CreateDataExtract', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateDataExtract;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreateUserAccountExtract
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CreateUserAccountExtract $parameters
     * @return \ID3Global\Models\CreateUserAccountExtractResponse|bool
     */
    public function CreateUserAccountExtract(\ID3Global\Models\CreateUserAccountExtract $parameters)
    {
        try {
            $this->setResult($resultCreateUserAccountExtract = $this->getSoapClient()->__soapCall('CreateUserAccountExtract', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateUserAccountExtract;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreateOrganisation
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CreateOrganisation $parameters
     * @return \ID3Global\Models\CreateOrganisationResponse|bool
     */
    public function CreateOrganisation(\ID3Global\Models\CreateOrganisation $parameters)
    {
        try {
            $this->setResult($resultCreateOrganisation = $this->getSoapClient()->__soapCall('CreateOrganisation', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateOrganisation;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreateAccount
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CreateAccount $parameters
     * @return \ID3Global\Models\CreateAccountResponse|bool
     */
    public function CreateAccount(\ID3Global\Models\CreateAccount $parameters)
    {
        try {
            $this->setResult($resultCreateAccount = $this->getSoapClient()->__soapCall('CreateAccount', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateAccount;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreateRole
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CreateRole $parameters
     * @return \ID3Global\Models\CreateRoleResponse|bool
     */
    public function CreateRole(\ID3Global\Models\CreateRole $parameters)
    {
        try {
            $this->setResult($resultCreateRole = $this->getSoapClient()->__soapCall('CreateRole', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateRole;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreateCase
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CreateCase $parameters
     * @return \ID3Global\Models\CreateCaseResponse|bool
     */
    public function CreateCase(\ID3Global\Models\CreateCase $parameters)
    {
        try {
            $this->setResult($resultCreateCase = $this->getSoapClient()->__soapCall('CreateCase', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateCase;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreateFullCase
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CreateFullCase $parameters
     * @return \ID3Global\Models\CreateFullCaseResponse|bool
     */
    public function CreateFullCase(\ID3Global\Models\CreateFullCase $parameters)
    {
        try {
            $this->setResult($resultCreateFullCase = $this->getSoapClient()->__soapCall('CreateFullCase', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateFullCase;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\CreateAccountResponse|\ID3Global\Models\CreateCaseResponse|\ID3Global\Models\CreateDataExtractResponse|\ID3Global\Models\CreateFullCaseResponse|\ID3Global\Models\CreateOrganisationResponse|\ID3Global\Models\CreateProfileResponse|\ID3Global\Models\CreateProfileWithScoringMethodResponse|\ID3Global\Models\CreateRoleResponse|\ID3Global\Models\CreateUserAccountExtractResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
