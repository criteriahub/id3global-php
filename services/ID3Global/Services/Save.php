<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Save Services
 * @subpackage Services
 */
class Save extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SaveCaseDocuments
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\SaveCaseDocuments $parameters
     * @return \ID3Global\Models\SaveCaseDocumentsResponse|bool
     */
    public function SaveCaseDocuments(\ID3Global\Models\SaveCaseDocuments $parameters)
    {
        try {
            $this->setResult($resultSaveCaseDocuments = $this->getSoapClient()->__soapCall('SaveCaseDocuments', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSaveCaseDocuments;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named SaveDocumentResult
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\SaveDocumentResult $parameters
     * @return \ID3Global\Models\SaveDocumentResultResponse|bool
     */
    public function SaveDocumentResult(\ID3Global\Models\SaveDocumentResult $parameters)
    {
        try {
            $this->setResult($resultSaveDocumentResult = $this->getSoapClient()->__soapCall('SaveDocumentResult', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSaveDocumentResult;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named SaveExternalDataResult
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\SaveExternalDataResult $parameters
     * @return \ID3Global\Models\SaveExternalDataResultResponse|bool
     */
    public function SaveExternalDataResult(\ID3Global\Models\SaveExternalDataResult $parameters)
    {
        try {
            $this->setResult($resultSaveExternalDataResult = $this->getSoapClient()->__soapCall('SaveExternalDataResult', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSaveExternalDataResult;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named SaveResult
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\SaveResult $parameters
     * @return \ID3Global\Models\SaveResultResponse|bool
     */
    public function SaveResult(\ID3Global\Models\SaveResult $parameters)
    {
        try {
            $this->setResult($resultSaveResult = $this->getSoapClient()->__soapCall('SaveResult', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSaveResult;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named SaveManualDispatchResult
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\SaveManualDispatchResult $parameters
     * @return \ID3Global\Models\SaveManualDispatchResultResponse|bool
     */
    public function SaveManualDispatchResult(\ID3Global\Models\SaveManualDispatchResult $parameters)
    {
        try {
            $this->setResult($resultSaveManualDispatchResult = $this->getSoapClient()->__soapCall('SaveManualDispatchResult', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSaveManualDispatchResult;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\SaveCaseDocumentsResponse|\ID3Global\Models\SaveDocumentResultResponse|\ID3Global\Models\SaveExternalDataResultResponse|\ID3Global\Models\SaveManualDispatchResultResponse|\ID3Global\Models\SaveResultResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
