<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Request Services
 * @subpackage Services
 */
class Request extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named RequestMemorableWordReminder
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\RequestMemorableWordReminder $parameters
     * @return \ID3Global\Models\RequestMemorableWordReminderResponse|bool
     */
    public function RequestMemorableWordReminder(\ID3Global\Models\RequestMemorableWordReminder $parameters)
    {
        try {
            $this->setResult($resultRequestMemorableWordReminder = $this->getSoapClient()->__soapCall('RequestMemorableWordReminder', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultRequestMemorableWordReminder;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\RequestMemorableWordReminderResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
