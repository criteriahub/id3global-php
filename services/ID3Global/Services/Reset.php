<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Reset Services
 * @subpackage Services
 */
class Reset extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named ResetPassword
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\ResetPassword $parameters
     * @return \ID3Global\Models\ResetPasswordResponse|bool
     */
    public function ResetPassword(\ID3Global\Models\ResetPassword $parameters)
    {
        try {
            $this->setResult($resultResetPassword = $this->getSoapClient()->__soapCall('ResetPassword', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultResetPassword;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\ResetPasswordResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
