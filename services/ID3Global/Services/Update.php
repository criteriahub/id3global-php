<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Update Services
 * @subpackage Services
 */
class Update extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named UpdateProfile
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateProfile $parameters
     * @return \ID3Global\Models\UpdateProfileResponse|bool
     */
    public function UpdateProfile(\ID3Global\Models\UpdateProfile $parameters)
    {
        try {
            $this->setResult($resultUpdateProfile = $this->getSoapClient()->__soapCall('UpdateProfile', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateProfile;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateProfileState
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateProfileState $parameters
     * @return \ID3Global\Models\UpdateProfileStateResponse|bool
     */
    public function UpdateProfileState(\ID3Global\Models\UpdateProfileState $parameters)
    {
        try {
            $this->setResult($resultUpdateProfileState = $this->getSoapClient()->__soapCall('UpdateProfileState', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateProfileState;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateProfileDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateProfileDetails $parameters
     * @return \ID3Global\Models\UpdateProfileDetailsResponse|bool
     */
    public function UpdateProfileDetails(\ID3Global\Models\UpdateProfileDetails $parameters)
    {
        try {
            $this->setResult($resultUpdateProfileDetails = $this->getSoapClient()->__soapCall('UpdateProfileDetails', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateProfileDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateLicence
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateLicence $parameters
     * @return \ID3Global\Models\UpdateLicenceResponse|bool
     */
    public function UpdateLicence(\ID3Global\Models\UpdateLicence $parameters)
    {
        try {
            $this->setResult($resultUpdateLicence = $this->getSoapClient()->__soapCall('UpdateLicence', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateLicence;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateLicenceItemToPending
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateLicenceItemToPending $parameters
     * @return \ID3Global\Models\UpdateLicenceItemToPendingResponse|bool
     */
    public function UpdateLicenceItemToPending(\ID3Global\Models\UpdateLicenceItemToPending $parameters)
    {
        try {
            $this->setResult($resultUpdateLicenceItemToPending = $this->getSoapClient()->__soapCall('UpdateLicenceItemToPending', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateLicenceItemToPending;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateAuthenticationHelpdeskAccess
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateAuthenticationHelpdeskAccess $parameters
     * @return \ID3Global\Models\UpdateAuthenticationHelpdeskAccessResponse|bool
     */
    public function UpdateAuthenticationHelpdeskAccess(\ID3Global\Models\UpdateAuthenticationHelpdeskAccess $parameters)
    {
        try {
            $this->setResult($resultUpdateAuthenticationHelpdeskAccess = $this->getSoapClient()->__soapCall('UpdateAuthenticationHelpdeskAccess', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateAuthenticationHelpdeskAccess;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateAuthenticationDataDeletion
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateAuthenticationDataDeletion $parameters
     * @return \ID3Global\Models\UpdateAuthenticationDataDeletionResponse|bool
     */
    public function UpdateAuthenticationDataDeletion(\ID3Global\Models\UpdateAuthenticationDataDeletion $parameters)
    {
        try {
            $this->setResult($resultUpdateAuthenticationDataDeletion = $this->getSoapClient()->__soapCall('UpdateAuthenticationDataDeletion', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateAuthenticationDataDeletion;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateDIVDataDeletion
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateDIVDataDeletion $parameters
     * @return \ID3Global\Models\UpdateDIVDataDeletionResponse|bool
     */
    public function UpdateDIVDataDeletion(\ID3Global\Models\UpdateDIVDataDeletion $parameters)
    {
        try {
            $this->setResult($resultUpdateDIVDataDeletion = $this->getSoapClient()->__soapCall('UpdateDIVDataDeletion', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateDIVDataDeletion;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateOrganisationDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateOrganisationDetails $parameters
     * @return \ID3Global\Models\UpdateOrganisationDetailsResponse|bool
     */
    public function UpdateOrganisationDetails(\ID3Global\Models\UpdateOrganisationDetails $parameters)
    {
        try {
            $this->setResult($resultUpdateOrganisationDetails = $this->getSoapClient()->__soapCall('UpdateOrganisationDetails', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateOrganisationDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateAccountDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateAccountDetails $parameters
     * @return \ID3Global\Models\UpdateAccountDetailsResponse|bool
     */
    public function UpdateAccountDetails(\ID3Global\Models\UpdateAccountDetails $parameters)
    {
        try {
            $this->setResult($resultUpdateAccountDetails = $this->getSoapClient()->__soapCall('UpdateAccountDetails', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateAccountDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateRole
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateRole $parameters
     * @return \ID3Global\Models\UpdateRoleResponse|bool
     */
    public function UpdateRole(\ID3Global\Models\UpdateRole $parameters)
    {
        try {
            $this->setResult($resultUpdateRole = $this->getSoapClient()->__soapCall('UpdateRole', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateRole;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateRoleMembers
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateRoleMembers $parameters
     * @return \ID3Global\Models\UpdateRoleMembersResponse|bool
     */
    public function UpdateRoleMembers(\ID3Global\Models\UpdateRoleMembers $parameters)
    {
        try {
            $this->setResult($resultUpdateRoleMembers = $this->getSoapClient()->__soapCall('UpdateRoleMembers', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateRoleMembers;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdatePermissions
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdatePermissions $parameters
     * @return \ID3Global\Models\UpdatePermissionsResponse|bool
     */
    public function UpdatePermissions(\ID3Global\Models\UpdatePermissions $parameters)
    {
        try {
            $this->setResult($resultUpdatePermissions = $this->getSoapClient()->__soapCall('UpdatePermissions', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdatePermissions;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateSupplier
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateSupplier $parameters
     * @return \ID3Global\Models\UpdateSupplierResponse|bool
     */
    public function UpdateSupplier(\ID3Global\Models\UpdateSupplier $parameters)
    {
        try {
            $this->setResult($resultUpdateSupplier = $this->getSoapClient()->__soapCall('UpdateSupplier', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateSupplier;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateSupplierAccount
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateSupplierAccount $parameters
     * @return \ID3Global\Models\UpdateSupplierAccountResponse|bool
     */
    public function UpdateSupplierAccount(\ID3Global\Models\UpdateSupplierAccount $parameters)
    {
        try {
            $this->setResult($resultUpdateSupplierAccount = $this->getSoapClient()->__soapCall('UpdateSupplierAccount', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateSupplierAccount;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateCaseNotification
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateCaseNotification $parameters
     * @return \ID3Global\Models\UpdateCaseNotificationResponse|bool
     */
    public function UpdateCaseNotification(\ID3Global\Models\UpdateCaseNotification $parameters)
    {
        try {
            $this->setResult($resultUpdateCaseNotification = $this->getSoapClient()->__soapCall('UpdateCaseNotification', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateCaseNotification;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateCustomerSettings
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateCustomerSettings $parameters
     * @return \ID3Global\Models\UpdateCustomerSettingsResponse|bool
     */
    public function UpdateCustomerSettings(\ID3Global\Models\UpdateCustomerSettings $parameters)
    {
        try {
            $this->setResult($resultUpdateCustomerSettings = $this->getSoapClient()->__soapCall('UpdateCustomerSettings', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateCustomerSettings;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateState
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateState $parameters
     * @return \ID3Global\Models\UpdateStateResponse|bool
     */
    public function UpdateState(\ID3Global\Models\UpdateState $parameters)
    {
        try {
            $this->setResult($resultUpdateState = $this->getSoapClient()->__soapCall('UpdateState', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateState;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateCase
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateCase $parameters
     * @return \ID3Global\Models\UpdateCaseResponse|bool
     */
    public function UpdateCase(\ID3Global\Models\UpdateCase $parameters)
    {
        try {
            $this->setResult($resultUpdateCase = $this->getSoapClient()->__soapCall('UpdateCase', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateCase;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateCaseEmail
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateCaseEmail $parameters
     * @return \ID3Global\Models\UpdateCaseEmailResponse|bool
     */
    public function UpdateCaseEmail(\ID3Global\Models\UpdateCaseEmail $parameters)
    {
        try {
            $this->setResult($resultUpdateCaseEmail = $this->getSoapClient()->__soapCall('UpdateCaseEmail', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateCaseEmail;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named UpdateDispatchStatus
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\UpdateDispatchStatus $parameters
     * @return \ID3Global\Models\UpdateDispatchStatusResponse|bool
     */
    public function UpdateDispatchStatus(\ID3Global\Models\UpdateDispatchStatus $parameters)
    {
        try {
            $this->setResult($resultUpdateDispatchStatus = $this->getSoapClient()->__soapCall('UpdateDispatchStatus', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateDispatchStatus;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\UpdateAccountDetailsResponse|\ID3Global\Models\UpdateAuthenticationDataDeletionResponse|\ID3Global\Models\UpdateAuthenticationHelpdeskAccessResponse|\ID3Global\Models\UpdateCaseEmailResponse|\ID3Global\Models\UpdateCaseNotificationResponse|\ID3Global\Models\UpdateCaseResponse|\ID3Global\Models\UpdateCustomerSettingsResponse|\ID3Global\Models\UpdateDispatchStatusResponse|\ID3Global\Models\UpdateDIVDataDeletionResponse|\ID3Global\Models\UpdateLicenceItemToPendingResponse|\ID3Global\Models\UpdateLicenceResponse|\ID3Global\Models\UpdateOrganisationDetailsResponse|\ID3Global\Models\UpdatePermissionsResponse|\ID3Global\Models\UpdateProfileDetailsResponse|\ID3Global\Models\UpdateProfileResponse|\ID3Global\Models\UpdateProfileStateResponse|\ID3Global\Models\UpdateRoleMembersResponse|\ID3Global\Models\UpdateRoleResponse|\ID3Global\Models\UpdateStateResponse|\ID3Global\Models\UpdateSupplierAccountResponse|\ID3Global\Models\UpdateSupplierResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
