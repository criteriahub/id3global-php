<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Test Services
 * @subpackage Services
 */
class Test extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named TestSupplierAccountCredentials
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\TestSupplierAccountCredentials $parameters
     * @return \ID3Global\Models\TestSupplierAccountCredentialsResponse|bool
     */
    public function TestSupplierAccountCredentials(\ID3Global\Models\TestSupplierAccountCredentials $parameters)
    {
        try {
            $this->setResult($resultTestSupplierAccountCredentials = $this->getSoapClient()->__soapCall('TestSupplierAccountCredentials', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultTestSupplierAccountCredentials;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\TestSupplierAccountCredentialsResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
