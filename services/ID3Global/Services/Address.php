<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Address Services
 * @subpackage Services
 */
class Address extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named AddressLookup
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\AddressLookup $parameters
     * @return \ID3Global\Models\AddressLookupResponse|bool
     */
    public function AddressLookup(\ID3Global\Models\AddressLookup $parameters)
    {
        try {
            $this->setResult($resultAddressLookup = $this->getSoapClient()->__soapCall('AddressLookup', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultAddressLookup;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\AddressLookupResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
