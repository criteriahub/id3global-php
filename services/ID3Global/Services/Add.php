<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Add Services
 * @subpackage Services
 */
class Add extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named AddRemoveOGMRecord
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\AddRemoveOGMRecord $parameters
     * @return \ID3Global\Models\AddRemoveOGMRecordResponse|bool
     */
    public function AddRemoveOGMRecord(\ID3Global\Models\AddRemoveOGMRecord $parameters)
    {
        try {
            $this->setResult($resultAddRemoveOGMRecord = $this->getSoapClient()->__soapCall('AddRemoveOGMRecord', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultAddRemoveOGMRecord;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\AddRemoveOGMRecordResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
