<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Supported Services
 * @subpackage Services
 */
class Supported extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SupportedFields
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\SupportedFields $parameters
     * @return \ID3Global\Models\SupportedFieldsResponse|bool
     */
    public function SupportedFields(\ID3Global\Models\SupportedFields $parameters)
    {
        try {
            $this->setResult($resultSupportedFields = $this->getSoapClient()->__soapCall('SupportedFields', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSupportedFields;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\SupportedFieldsResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
