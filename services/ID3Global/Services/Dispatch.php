<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Dispatch Services
 * @subpackage Services
 */
class Dispatch extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named DispatchCase
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\DispatchCase $parameters
     * @return \ID3Global\Models\DispatchCaseResponse|bool
     */
    public function DispatchCase(\ID3Global\Models\DispatchCase $parameters)
    {
        try {
            $this->setResult($resultDispatchCase = $this->getSoapClient()->__soapCall('DispatchCase', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultDispatchCase;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\DispatchCaseResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
