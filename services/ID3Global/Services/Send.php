<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Send Services
 * @subpackage Services
 */
class Send extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SendWelcomeEmail
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\SendWelcomeEmail $parameters
     * @return \ID3Global\Models\SendWelcomeEmailResponse|bool
     */
    public function SendWelcomeEmail(\ID3Global\Models\SendWelcomeEmail $parameters)
    {
        try {
            $this->setResult($resultSendWelcomeEmail = $this->getSoapClient()->__soapCall('SendWelcomeEmail', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultSendWelcomeEmail;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\SendWelcomeEmailResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
