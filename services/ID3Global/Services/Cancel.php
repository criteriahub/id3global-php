<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Cancel Services
 * @subpackage Services
 */
class Cancel extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named CancelDataExtract
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CancelDataExtract $parameters
     * @return \ID3Global\Models\CancelDataExtractResponse|bool
     */
    public function CancelDataExtract(\ID3Global\Models\CancelDataExtract $parameters)
    {
        try {
            $this->setResult($resultCancelDataExtract = $this->getSoapClient()->__soapCall('CancelDataExtract', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCancelDataExtract;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named CancelExtract
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\CancelExtract $parameters
     * @return \ID3Global\Models\CancelExtractResponse|bool
     */
    public function CancelExtract(\ID3Global\Models\CancelExtract $parameters)
    {
        try {
            $this->setResult($resultCancelExtract = $this->getSoapClient()->__soapCall('CancelExtract', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCancelExtract;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\CancelDataExtractResponse|\ID3Global\Models\CancelExtractResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
