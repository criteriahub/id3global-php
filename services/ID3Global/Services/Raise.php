<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Raise Services
 * @subpackage Services
 */
class Raise extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named RaiseChargingPoint
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\RaiseChargingPoint $parameters
     * @return \ID3Global\Models\RaiseChargingPointResponse|bool
     */
    public function RaiseChargingPoint(\ID3Global\Models\RaiseChargingPoint $parameters)
    {
        try {
            $this->setResult($resultRaiseChargingPoint = $this->getSoapClient()->__soapCall('RaiseChargingPoint', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultRaiseChargingPoint;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\RaiseChargingPointResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
