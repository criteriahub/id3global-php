<?php

declare(strict_types=1);

namespace ID3Global\Services;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Retrieve Services
 * @subpackage Services
 */
class Retrieve extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named RetrieveDispatch
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \ID3Global\Models\RetrieveDispatch $parameters
     * @return \ID3Global\Models\RetrieveDispatchResponse|bool
     */
    public function RetrieveDispatch(\ID3Global\Models\RetrieveDispatch $parameters)
    {
        try {
            $this->setResult($resultRetrieveDispatch = $this->getSoapClient()->__soapCall('RetrieveDispatch', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultRetrieveDispatch;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \ID3Global\Models\RetrieveDispatchResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
