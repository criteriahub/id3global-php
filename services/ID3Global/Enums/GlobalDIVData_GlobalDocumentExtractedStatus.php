<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalDIVData.GlobalDocumentExtractedStatus Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q143:GlobalDIVData.GlobalDocumentExtractedStatus
 * @subpackage Enumerations
 */
class GlobalDIVData_GlobalDocumentExtractedStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'UnknownDocument'
     * @return string 'UnknownDocument'
     */
    const VALUE_UNKNOWN_DOCUMENT = 'UnknownDocument';
    /**
     * Constant for value 'LowQualityImage'
     * @return string 'LowQualityImage'
     */
    const VALUE_LOW_QUALITY_IMAGE = 'LowQualityImage';
    /**
     * Constant for value 'UnableToProcess'
     * @return string 'UnableToProcess'
     */
    const VALUE_UNABLE_TO_PROCESS = 'UnableToProcess';
    /**
     * Constant for value 'Completed'
     * @return string 'Completed'
     */
    const VALUE_COMPLETED = 'Completed';
    /**
     * Constant for value 'InProgress'
     * @return string 'InProgress'
     */
    const VALUE_IN_PROGRESS = 'InProgress';
    /**
     * Return allowed values
     * @uses self::VALUE_UNKNOWN_DOCUMENT
     * @uses self::VALUE_LOW_QUALITY_IMAGE
     * @uses self::VALUE_UNABLE_TO_PROCESS
     * @uses self::VALUE_COMPLETED
     * @uses self::VALUE_IN_PROGRESS
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_UNKNOWN_DOCUMENT,
            self::VALUE_LOW_QUALITY_IMAGE,
            self::VALUE_UNABLE_TO_PROCESS,
            self::VALUE_COMPLETED,
            self::VALUE_IN_PROGRESS,
        ];
    }
}
