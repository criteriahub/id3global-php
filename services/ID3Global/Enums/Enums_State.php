<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for Enums.State Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:Enums.State
 * @subpackage Enumerations
 */
class Enums_State extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Scheduled'
     * @return string 'Scheduled'
     */
    const VALUE_SCHEDULED = 'Scheduled';
    /**
     * Constant for value 'Ready'
     * @return string 'Ready'
     */
    const VALUE_READY = 'Ready';
    /**
     * Constant for value 'Failed'
     * @return string 'Failed'
     */
    const VALUE_FAILED = 'Failed';
    /**
     * Constant for value 'Expired'
     * @return string 'Expired'
     */
    const VALUE_EXPIRED = 'Expired';
    /**
     * Constant for value 'Cancelled'
     * @return string 'Cancelled'
     */
    const VALUE_CANCELLED = 'Cancelled';
    /**
     * Return allowed values
     * @uses self::VALUE_SCHEDULED
     * @uses self::VALUE_READY
     * @uses self::VALUE_FAILED
     * @uses self::VALUE_EXPIRED
     * @uses self::VALUE_CANCELLED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_SCHEDULED,
            self::VALUE_READY,
            self::VALUE_FAILED,
            self::VALUE_EXPIRED,
            self::VALUE_CANCELLED,
        ];
    }
}
