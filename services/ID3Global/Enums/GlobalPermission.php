<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalPermission Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q576:GlobalPermission
 * @subpackage Enumerations
 */
class GlobalPermission extends AbstractStructEnumBase
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'Read'
     * @return string 'Read'
     */
    const VALUE_READ = 'Read';
    /**
     * Constant for value 'Execute'
     * @return string 'Execute'
     */
    const VALUE_EXECUTE = 'Execute';
    /**
     * Constant for value 'Manage'
     * @return string 'Manage'
     */
    const VALUE_MANAGE = 'Manage';
    /**
     * Constant for value 'Full'
     * @return string 'Full'
     */
    const VALUE_FULL = 'Full';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_READ
     * @uses self::VALUE_EXECUTE
     * @uses self::VALUE_MANAGE
     * @uses self::VALUE_FULL
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NONE,
            self::VALUE_READ,
            self::VALUE_EXECUTE,
            self::VALUE_MANAGE,
            self::VALUE_FULL,
        ];
    }
}
