<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalConsentVerdict Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q853:GlobalConsentVerdict
 * @subpackage Enumerations
 */
class GlobalConsentVerdict extends AbstractStructEnumBase
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'Approved'
     * @return string 'Approved'
     */
    const VALUE_APPROVED = 'Approved';
    /**
     * Constant for value 'Reject'
     * @return string 'Reject'
     */
    const VALUE_REJECT = 'Reject';
    /**
     * Constant for value 'RequestInfo'
     * @return string 'RequestInfo'
     */
    const VALUE_REQUEST_INFO = 'RequestInfo';
    /**
     * Constant for value 'Cancel'
     * @return string 'Cancel'
     */
    const VALUE_CANCEL = 'Cancel';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_APPROVED
     * @uses self::VALUE_REJECT
     * @uses self::VALUE_REQUEST_INFO
     * @uses self::VALUE_CANCEL
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NONE,
            self::VALUE_APPROVED,
            self::VALUE_REJECT,
            self::VALUE_REQUEST_INFO,
            self::VALUE_CANCEL,
        ];
    }
}
