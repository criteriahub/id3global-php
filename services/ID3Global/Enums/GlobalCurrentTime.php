<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalCurrentTime Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q419:GlobalCurrentTime
 * @subpackage Enumerations
 */
class GlobalCurrentTime extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Years5Plus'
     * @return string 'Years5Plus'
     */
    const VALUE_YEARS_5_PLUS = 'Years5Plus';
    /**
     * Constant for value 'Years2to5'
     * @return string 'Years2to5'
     */
    const VALUE_YEARS_2_TO_5 = 'Years2to5';
    /**
     * Constant for value 'Years0to2'
     * @return string 'Years0to2'
     */
    const VALUE_YEARS_0_TO_2 = 'Years0to2';
    /**
     * Return allowed values
     * @uses self::VALUE_YEARS_5_PLUS
     * @uses self::VALUE_YEARS_2_TO_5
     * @uses self::VALUE_YEARS_0_TO_2
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_YEARS_5_PLUS,
            self::VALUE_YEARS_2_TO_5,
            self::VALUE_YEARS_0_TO_2,
        ];
    }
}
