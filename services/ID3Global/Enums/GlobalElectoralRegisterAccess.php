<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalElectoralRegisterAccess Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q711:GlobalElectoralRegisterAccess
 * @subpackage Enumerations
 */
class GlobalElectoralRegisterAccess extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Edited'
     * @return string 'Edited'
     */
    const VALUE_EDITED = 'Edited';
    /**
     * Constant for value 'Full'
     * @return string 'Full'
     */
    const VALUE_FULL = 'Full';
    /**
     * Return allowed values
     * @uses self::VALUE_EDITED
     * @uses self::VALUE_FULL
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_EDITED,
            self::VALUE_FULL,
        ];
    }
}
