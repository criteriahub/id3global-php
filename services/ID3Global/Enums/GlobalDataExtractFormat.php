<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalDataExtractFormat Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q500:GlobalDataExtractFormat
 * @subpackage Enumerations
 */
class GlobalDataExtractFormat extends AbstractStructEnumBase
{
    /**
     * Constant for value 'CSV'
     * @return string 'CSV'
     */
    const VALUE_CSV = 'CSV';
    /**
     * Constant for value 'TSV'
     * @return string 'TSV'
     */
    const VALUE_TSV = 'TSV';
    /**
     * Constant for value 'EXCEL'
     * @return string 'EXCEL'
     */
    const VALUE_EXCEL = 'EXCEL';
    /**
     * Return allowed values
     * @uses self::VALUE_CSV
     * @uses self::VALUE_TSV
     * @uses self::VALUE_EXCEL
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CSV,
            self::VALUE_TSV,
            self::VALUE_EXCEL,
        ];
    }
}
