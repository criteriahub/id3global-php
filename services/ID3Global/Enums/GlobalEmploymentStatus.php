<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalEmploymentStatus Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q418:GlobalEmploymentStatus
 * @subpackage Enumerations
 */
class GlobalEmploymentStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'FTPerm'
     * @return string 'FTPerm'
     */
    const VALUE_FTPERM = 'FTPerm';
    /**
     * Constant for value 'PTPerm'
     * @return string 'PTPerm'
     */
    const VALUE_PTPERM = 'PTPerm';
    /**
     * Constant for value 'SelfEmployed'
     * @return string 'SelfEmployed'
     */
    const VALUE_SELF_EMPLOYED = 'SelfEmployed';
    /**
     * Constant for value 'Retired'
     * @return string 'Retired'
     */
    const VALUE_RETIRED = 'Retired';
    /**
     * Constant for value 'Homemaker'
     * @return string 'Homemaker'
     */
    const VALUE_HOMEMAKER = 'Homemaker';
    /**
     * Constant for value 'Unemployed'
     * @return string 'Unemployed'
     */
    const VALUE_UNEMPLOYED = 'Unemployed';
    /**
     * Constant for value 'Student'
     * @return string 'Student'
     */
    const VALUE_STUDENT = 'Student';
    /**
     * Constant for value 'ArmedForces'
     * @return string 'ArmedForces'
     */
    const VALUE_ARMED_FORCES = 'ArmedForces';
    /**
     * Constant for value 'Employed'
     * @return string 'Employed'
     */
    const VALUE_EMPLOYED = 'Employed';
    /**
     * Constant for value 'ReceivesPension'
     * @return string 'ReceivesPension'
     */
    const VALUE_RECEIVES_PENSION = 'ReceivesPension';
    /**
     * Constant for value 'CarerChild'
     * @return string 'CarerChild'
     */
    const VALUE_CARER_CHILD = 'CarerChild';
    /**
     * Constant for value 'CarerOver16'
     * @return string 'CarerOver16'
     */
    const VALUE_CARER_OVER_16 = 'CarerOver16';
    /**
     * Constant for value 'FTEducation'
     * @return string 'FTEducation'
     */
    const VALUE_FTEDUCATION = 'FTEducation';
    /**
     * Constant for value 'Other'
     * @return string 'Other'
     */
    const VALUE_OTHER = 'Other';
    /**
     * Return allowed values
     * @uses self::VALUE_FTPERM
     * @uses self::VALUE_PTPERM
     * @uses self::VALUE_SELF_EMPLOYED
     * @uses self::VALUE_RETIRED
     * @uses self::VALUE_HOMEMAKER
     * @uses self::VALUE_UNEMPLOYED
     * @uses self::VALUE_STUDENT
     * @uses self::VALUE_ARMED_FORCES
     * @uses self::VALUE_EMPLOYED
     * @uses self::VALUE_RECEIVES_PENSION
     * @uses self::VALUE_CARER_CHILD
     * @uses self::VALUE_CARER_OVER_16
     * @uses self::VALUE_FTEDUCATION
     * @uses self::VALUE_OTHER
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_FTPERM,
            self::VALUE_PTPERM,
            self::VALUE_SELF_EMPLOYED,
            self::VALUE_RETIRED,
            self::VALUE_HOMEMAKER,
            self::VALUE_UNEMPLOYED,
            self::VALUE_STUDENT,
            self::VALUE_ARMED_FORCES,
            self::VALUE_EMPLOYED,
            self::VALUE_RECEIVES_PENSION,
            self::VALUE_CARER_CHILD,
            self::VALUE_CARER_OVER_16,
            self::VALUE_FTEDUCATION,
            self::VALUE_OTHER,
        ];
    }
}
