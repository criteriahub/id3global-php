<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalUKBirthsIndexCountry Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q321:GlobalUKBirthsIndexCountry
 * @subpackage Enumerations
 */
class GlobalUKBirthsIndexCountry extends AbstractStructEnumBase
{
    /**
     * Constant for value 'UNSPECIFIED'
     * @return string 'UNSPECIFIED'
     */
    const VALUE_UNSPECIFIED = 'UNSPECIFIED';
    /**
     * Constant for value 'ENGLANDWALES'
     * @return string 'ENGLANDWALES'
     */
    const VALUE_ENGLANDWALES = 'ENGLANDWALES';
    /**
     * Constant for value 'OTHER'
     * @return string 'OTHER'
     */
    const VALUE_OTHER = 'OTHER';
    /**
     * Return allowed values
     * @uses self::VALUE_UNSPECIFIED
     * @uses self::VALUE_ENGLANDWALES
     * @uses self::VALUE_OTHER
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_UNSPECIFIED,
            self::VALUE_ENGLANDWALES,
            self::VALUE_OTHER,
        ];
    }
}
