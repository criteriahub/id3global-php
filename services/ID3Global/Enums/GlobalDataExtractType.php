<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalDataExtractType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q499:GlobalDataExtractType
 * @subpackage Enumerations
 */
class GlobalDataExtractType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'UserDefined'
     * @return string 'UserDefined'
     */
    const VALUE_USER_DEFINED = 'UserDefined';
    /**
     * Constant for value 'Day'
     * @return string 'Day'
     */
    const VALUE_DAY = 'Day';
    /**
     * Constant for value 'Week'
     * @return string 'Week'
     */
    const VALUE_WEEK = 'Week';
    /**
     * Constant for value 'Fortnight'
     * @return string 'Fortnight'
     */
    const VALUE_FORTNIGHT = 'Fortnight';
    /**
     * Constant for value 'Month'
     * @return string 'Month'
     */
    const VALUE_MONTH = 'Month';
    /**
     * Return allowed values
     * @uses self::VALUE_USER_DEFINED
     * @uses self::VALUE_DAY
     * @uses self::VALUE_WEEK
     * @uses self::VALUE_FORTNIGHT
     * @uses self::VALUE_MONTH
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_USER_DEFINED,
            self::VALUE_DAY,
            self::VALUE_WEEK,
            self::VALUE_FORTNIGHT,
            self::VALUE_MONTH,
        ];
    }
}
