<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalResidenceType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q417:GlobalResidenceType
 * @subpackage Enumerations
 */
class GlobalResidenceType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'HomeOwnerOutright'
     * @return string 'HomeOwnerOutright'
     */
    const VALUE_HOME_OWNER_OUTRIGHT = 'HomeOwnerOutright';
    /**
     * Constant for value 'HomeOwnerMortgage'
     * @return string 'HomeOwnerMortgage'
     */
    const VALUE_HOME_OWNER_MORTGAGE = 'HomeOwnerMortgage';
    /**
     * Constant for value 'Tenant'
     * @return string 'Tenant'
     */
    const VALUE_TENANT = 'Tenant';
    /**
     * Constant for value 'LivingWithRelatives'
     * @return string 'LivingWithRelatives'
     */
    const VALUE_LIVING_WITH_RELATIVES = 'LivingWithRelatives';
    /**
     * Return allowed values
     * @uses self::VALUE_HOME_OWNER_OUTRIGHT
     * @uses self::VALUE_HOME_OWNER_MORTGAGE
     * @uses self::VALUE_TENANT
     * @uses self::VALUE_LIVING_WITH_RELATIVES
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_HOME_OWNER_OUTRIGHT,
            self::VALUE_HOME_OWNER_MORTGAGE,
            self::VALUE_TENANT,
            self::VALUE_LIVING_WITH_RELATIVES,
        ];
    }
}
