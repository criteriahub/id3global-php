<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalLicenseBillingOption Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q708:GlobalLicenseBillingOption
 * @subpackage Enumerations
 */
class GlobalLicenseBillingOption extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Enterprise'
     * @return string 'Enterprise'
     */
    const VALUE_ENTERPRISE = 'Enterprise';
    /**
     * Constant for value 'Evaluation'
     * @return string 'Evaluation'
     */
    const VALUE_EVALUATION = 'Evaluation';
    /**
     * Constant for value 'MidMarket'
     * @return string 'MidMarket'
     */
    const VALUE_MID_MARKET = 'MidMarket';
    /**
     * Constant for value 'NewStart'
     * @return string 'NewStart'
     */
    const VALUE_NEW_START = 'NewStart';
    /**
     * Constant for value 'TrialFee'
     * @return string 'TrialFee'
     */
    const VALUE_TRIAL_FEE = 'TrialFee';
    /**
     * Return allowed values
     * @uses self::VALUE_ENTERPRISE
     * @uses self::VALUE_EVALUATION
     * @uses self::VALUE_MID_MARKET
     * @uses self::VALUE_NEW_START
     * @uses self::VALUE_TRIAL_FEE
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_ENTERPRISE,
            self::VALUE_EVALUATION,
            self::VALUE_MID_MARKET,
            self::VALUE_NEW_START,
            self::VALUE_TRIAL_FEE,
        ];
    }
}
