<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalDocumentSubmissionType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q832:GlobalDocumentSubmissionType
 * @subpackage Enumerations
 */
class GlobalDocumentSubmissionType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'Author'
     * @return string 'Author'
     */
    const VALUE_AUTHOR = 'Author';
    /**
     * Constant for value 'Applicant'
     * @return string 'Applicant'
     */
    const VALUE_APPLICANT = 'Applicant';
    /**
     * Constant for value 'InPossesion'
     * @return string 'InPossesion'
     */
    const VALUE_IN_POSSESION = 'InPossesion';
    /**
     * Constant for value 'PostOffice'
     * @return string 'PostOffice'
     */
    const VALUE_POST_OFFICE = 'PostOffice';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_AUTHOR
     * @uses self::VALUE_APPLICANT
     * @uses self::VALUE_IN_POSSESION
     * @uses self::VALUE_POST_OFFICE
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NONE,
            self::VALUE_AUTHOR,
            self::VALUE_APPLICANT,
            self::VALUE_IN_POSSESION,
            self::VALUE_POST_OFFICE,
        ];
    }
}
