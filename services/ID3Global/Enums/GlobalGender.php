<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalGender Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q131:GlobalGender
 * @subpackage Enumerations
 */
class GlobalGender extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Unspecified'
     * @return string 'Unspecified'
     */
    const VALUE_UNSPECIFIED = 'Unspecified';
    /**
     * Constant for value 'Unknown'
     * @return string 'Unknown'
     */
    const VALUE_UNKNOWN = 'Unknown';
    /**
     * Constant for value 'Male'
     * @return string 'Male'
     */
    const VALUE_MALE = 'Male';
    /**
     * Constant for value 'Female'
     * @return string 'Female'
     */
    const VALUE_FEMALE = 'Female';
    /**
     * Return allowed values
     * @uses self::VALUE_UNSPECIFIED
     * @uses self::VALUE_UNKNOWN
     * @uses self::VALUE_MALE
     * @uses self::VALUE_FEMALE
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_UNSPECIFIED,
            self::VALUE_UNKNOWN,
            self::VALUE_MALE,
            self::VALUE_FEMALE,
        ];
    }
}
