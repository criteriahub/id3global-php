<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalSanctionsDateType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q485:GlobalSanctionsDateType
 * @subpackage Enumerations
 */
class GlobalSanctionsDateType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Unknown'
     * @return string 'Unknown'
     */
    const VALUE_UNKNOWN = 'Unknown';
    /**
     * Constant for value 'Birth'
     * @return string 'Birth'
     */
    const VALUE_BIRTH = 'Birth';
    /**
     * Constant for value 'Death'
     * @return string 'Death'
     */
    const VALUE_DEATH = 'Death';
    /**
     * Return allowed values
     * @uses self::VALUE_UNKNOWN
     * @uses self::VALUE_BIRTH
     * @uses self::VALUE_DEATH
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_UNKNOWN,
            self::VALUE_BIRTH,
            self::VALUE_DEATH,
        ];
    }
}
