<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalProfileState Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q168:GlobalProfileState
 * @subpackage Enumerations
 */
class GlobalProfileState extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Test'
     * @return string 'Test'
     */
    const VALUE_TEST = 'Test';
    /**
     * Constant for value 'PreEffective'
     * @return string 'PreEffective'
     */
    const VALUE_PRE_EFFECTIVE = 'PreEffective';
    /**
     * Constant for value 'Effective'
     * @return string 'Effective'
     */
    const VALUE_EFFECTIVE = 'Effective';
    /**
     * Constant for value 'Retired'
     * @return string 'Retired'
     */
    const VALUE_RETIRED = 'Retired';
    /**
     * Return allowed values
     * @uses self::VALUE_TEST
     * @uses self::VALUE_PRE_EFFECTIVE
     * @uses self::VALUE_EFFECTIVE
     * @uses self::VALUE_RETIRED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_TEST,
            self::VALUE_PRE_EFFECTIVE,
            self::VALUE_EFFECTIVE,
            self::VALUE_RETIRED,
        ];
    }
}
