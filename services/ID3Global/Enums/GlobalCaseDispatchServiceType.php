<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalCaseDispatchServiceType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q859:GlobalCaseDispatchServiceType
 * @subpackage Enumerations
 */
class GlobalCaseDispatchServiceType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'DVLA'
     * @return string 'DVLA'
     */
    const VALUE_DVLA = 'DVLA';
    /**
     * Constant for value 'DisclosureScotland'
     * @return string 'DisclosureScotland'
     */
    const VALUE_DISCLOSURE_SCOTLAND = 'DisclosureScotland';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_DVLA
     * @uses self::VALUE_DISCLOSURE_SCOTLAND
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NONE,
            self::VALUE_DVLA,
            self::VALUE_DISCLOSURE_SCOTLAND,
        ];
    }
}
