<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalScoringMethod Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q170:GlobalScoringMethod
 * @subpackage Enumerations
 */
class GlobalScoringMethod extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Standard'
     * @return string 'Standard'
     */
    const VALUE_STANDARD = 'Standard';
    /**
     * Constant for value 'Enhanced'
     * @return string 'Enhanced'
     */
    const VALUE_ENHANCED = 'Enhanced';
    /**
     * Return allowed values
     * @uses self::VALUE_STANDARD
     * @uses self::VALUE_ENHANCED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_STANDARD,
            self::VALUE_ENHANCED,
        ];
    }
}
