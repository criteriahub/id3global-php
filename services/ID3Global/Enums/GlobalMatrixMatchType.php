<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalMatrixMatchType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q812:GlobalMatrixMatchType
 * @subpackage Enumerations
 */
class GlobalMatrixMatchType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Unclassified'
     * @return string 'Unclassified'
     */
    const VALUE_UNCLASSIFIED = 'Unclassified';
    /**
     * Constant for value 'Pass'
     * @return string 'Pass'
     */
    const VALUE_PASS = 'Pass';
    /**
     * Constant for value 'Refer'
     * @return string 'Refer'
     */
    const VALUE_REFER = 'Refer';
    /**
     * Constant for value 'Alert'
     * @return string 'Alert'
     */
    const VALUE_ALERT = 'Alert';
    /**
     * Constant for value 'Fail'
     * @return string 'Fail'
     */
    const VALUE_FAIL = 'Fail';
    /**
     * Constant for value 'NA'
     * @return string 'NA'
     */
    const VALUE_NA = 'NA';
    /**
     * Constant for value 'Match'
     * @return string 'Match'
     */
    const VALUE_MATCH = 'Match';
    /**
     * Constant for value 'Mismatch'
     * @return string 'Mismatch'
     */
    const VALUE_MISMATCH = 'Mismatch';
    /**
     * Constant for value 'Nomatch'
     * @return string 'Nomatch'
     */
    const VALUE_NOMATCH = 'Nomatch';
    /**
     * Constant for value 'Multiple'
     * @return string 'Multiple'
     */
    const VALUE_MULTIPLE = 'Multiple';
    /**
     * Constant for value 'Pending'
     * @return string 'Pending'
     */
    const VALUE_PENDING = 'Pending';
    /**
     * Return allowed values
     * @uses self::VALUE_UNCLASSIFIED
     * @uses self::VALUE_PASS
     * @uses self::VALUE_REFER
     * @uses self::VALUE_ALERT
     * @uses self::VALUE_FAIL
     * @uses self::VALUE_NA
     * @uses self::VALUE_MATCH
     * @uses self::VALUE_MISMATCH
     * @uses self::VALUE_NOMATCH
     * @uses self::VALUE_MULTIPLE
     * @uses self::VALUE_PENDING
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_UNCLASSIFIED,
            self::VALUE_PASS,
            self::VALUE_REFER,
            self::VALUE_ALERT,
            self::VALUE_FAIL,
            self::VALUE_NA,
            self::VALUE_MATCH,
            self::VALUE_MISMATCH,
            self::VALUE_NOMATCH,
            self::VALUE_MULTIPLE,
            self::VALUE_PENDING,
        ];
    }
}
