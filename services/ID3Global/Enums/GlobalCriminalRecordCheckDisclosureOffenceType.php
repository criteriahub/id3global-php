<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalCriminalRecordCheckDisclosureOffenceType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q796:GlobalCriminalRecordCheckDisclosureOffenceType
 * @subpackage Enumerations
 */
class GlobalCriminalRecordCheckDisclosureOffenceType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Caution'
     * @return string 'Caution'
     */
    const VALUE_CAUTION = 'Caution';
    /**
     * Constant for value 'Conviction'
     * @return string 'Conviction'
     */
    const VALUE_CONVICTION = 'Conviction';
    /**
     * Return allowed values
     * @uses self::VALUE_CAUTION
     * @uses self::VALUE_CONVICTION
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CAUTION,
            self::VALUE_CONVICTION,
        ];
    }
}
