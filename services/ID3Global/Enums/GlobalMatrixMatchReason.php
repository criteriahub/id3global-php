<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalMatrixMatchReason Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q813:GlobalMatrixMatchReason
 * @subpackage Enumerations
 */
class GlobalMatrixMatchReason extends AbstractStructEnumBase
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'InsufficientDataMatches'
     * @return string 'InsufficientDataMatches'
     */
    const VALUE_INSUFFICIENT_DATA_MATCHES = 'InsufficientDataMatches';
    /**
     * Constant for value 'InvalidDetails'
     * @return string 'InvalidDetails'
     */
    const VALUE_INVALID_DETAILS = 'InvalidDetails';
    /**
     * Constant for value 'DataMatchAlert'
     * @return string 'DataMatchAlert'
     */
    const VALUE_DATA_MATCH_ALERT = 'DataMatchAlert';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_INSUFFICIENT_DATA_MATCHES
     * @uses self::VALUE_INVALID_DETAILS
     * @uses self::VALUE_DATA_MATCH_ALERT
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NONE,
            self::VALUE_INSUFFICIENT_DATA_MATCHES,
            self::VALUE_INVALID_DETAILS,
            self::VALUE_DATA_MATCH_ALERT,
        ];
    }
}
