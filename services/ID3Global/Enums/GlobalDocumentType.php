<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalDocumentType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q137:GlobalDocumentType
 * @subpackage Enumerations
 */
class GlobalDocumentType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Passport'
     * @return string 'Passport'
     */
    const VALUE_PASSPORT = 'Passport';
    /**
     * Constant for value 'DrivingLicence'
     * @return string 'DrivingLicence'
     */
    const VALUE_DRIVING_LICENCE = 'DrivingLicence';
    /**
     * Constant for value 'IDCard'
     * @return string 'IDCard'
     */
    const VALUE_IDCARD = 'IDCard';
    /**
     * Constant for value 'Unknown'
     * @return string 'Unknown'
     */
    const VALUE_UNKNOWN = 'Unknown';
    /**
     * Return allowed values
     * @uses self::VALUE_PASSPORT
     * @uses self::VALUE_DRIVING_LICENCE
     * @uses self::VALUE_IDCARD
     * @uses self::VALUE_UNKNOWN
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_PASSPORT,
            self::VALUE_DRIVING_LICENCE,
            self::VALUE_IDCARD,
            self::VALUE_UNKNOWN,
        ];
    }
}
