<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalMatchLevel Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q654:GlobalMatchLevel
 * @subpackage Enumerations
 */
class GlobalMatchLevel extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Basic'
     * @return string 'Basic'
     */
    const VALUE_BASIC = 'Basic';
    /**
     * Constant for value 'Level1'
     * @return string 'Level1'
     */
    const VALUE_LEVEL_1 = 'Level1';
    /**
     * Constant for value 'Enhanced'
     * @return string 'Enhanced'
     */
    const VALUE_ENHANCED = 'Enhanced';
    /**
     * Constant for value 'Advanced'
     * @return string 'Advanced'
     */
    const VALUE_ADVANCED = 'Advanced';
    /**
     * Return allowed values
     * @uses self::VALUE_BASIC
     * @uses self::VALUE_LEVEL_1
     * @uses self::VALUE_ENHANCED
     * @uses self::VALUE_ADVANCED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_BASIC,
            self::VALUE_LEVEL_1,
            self::VALUE_ENHANCED,
            self::VALUE_ADVANCED,
        ];
    }
}
