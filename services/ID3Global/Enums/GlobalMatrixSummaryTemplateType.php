<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalMatrixSummaryTemplateType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q824:GlobalMatrixSummaryTemplateType
 * @subpackage Enumerations
 */
class GlobalMatrixSummaryTemplateType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'Address'
     * @return string 'Address'
     */
    const VALUE_ADDRESS = 'Address';
    /**
     * Constant for value 'Cells'
     * @return string 'Cells'
     */
    const VALUE_CELLS = 'Cells';
    /**
     * Constant for value 'Filter'
     * @return string 'Filter'
     */
    const VALUE_FILTER = 'Filter';
    /**
     * Constant for value 'Disclosure'
     * @return string 'Disclosure'
     */
    const VALUE_DISCLOSURE = 'Disclosure';
    /**
     * Constant for value 'Licence'
     * @return string 'Licence'
     */
    const VALUE_LICENCE = 'Licence';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_ADDRESS
     * @uses self::VALUE_CELLS
     * @uses self::VALUE_FILTER
     * @uses self::VALUE_DISCLOSURE
     * @uses self::VALUE_LICENCE
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NONE,
            self::VALUE_ADDRESS,
            self::VALUE_CELLS,
            self::VALUE_FILTER,
            self::VALUE_DISCLOSURE,
            self::VALUE_LICENCE,
        ];
    }
}
