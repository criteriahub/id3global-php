<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalAddressFormat Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q169:GlobalAddressFormat
 * @subpackage Enumerations
 */
class GlobalAddressFormat extends AbstractStructEnumBase
{
    /**
     * Constant for value 'FixedOnly'
     * @return string 'FixedOnly'
     */
    const VALUE_FIXED_ONLY = 'FixedOnly';
    /**
     * Constant for value 'FreeOnly'
     * @return string 'FreeOnly'
     */
    const VALUE_FREE_ONLY = 'FreeOnly';
    /**
     * Constant for value 'FixedThenFree'
     * @return string 'FixedThenFree'
     */
    const VALUE_FIXED_THEN_FREE = 'FixedThenFree';
    /**
     * Constant for value 'FreeThenFixed'
     * @return string 'FreeThenFixed'
     */
    const VALUE_FREE_THEN_FIXED = 'FreeThenFixed';
    /**
     * Return allowed values
     * @uses self::VALUE_FIXED_ONLY
     * @uses self::VALUE_FREE_ONLY
     * @uses self::VALUE_FIXED_THEN_FREE
     * @uses self::VALUE_FREE_THEN_FIXED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_FIXED_ONLY,
            self::VALUE_FREE_ONLY,
            self::VALUE_FIXED_THEN_FREE,
            self::VALUE_FREE_THEN_FIXED,
        ];
    }
}
