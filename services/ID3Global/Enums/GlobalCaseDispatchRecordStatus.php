<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalCaseDispatchRecordStatus Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q860:GlobalCaseDispatchRecordStatus
 * @subpackage Enumerations
 */
class GlobalCaseDispatchRecordStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Sent'
     * @return string 'Sent'
     */
    const VALUE_SENT = 'Sent';
    /**
     * Constant for value 'ResendConfirmationRequired'
     * @return string 'ResendConfirmationRequired'
     */
    const VALUE_RESEND_CONFIRMATION_REQUIRED = 'ResendConfirmationRequired';
    /**
     * Constant for value 'ProcessResend'
     * @return string 'ProcessResend'
     */
    const VALUE_PROCESS_RESEND = 'ProcessResend';
    /**
     * Constant for value 'Resent'
     * @return string 'Resent'
     */
    const VALUE_RESENT = 'Resent';
    /**
     * Constant for value 'Complete'
     * @return string 'Complete'
     */
    const VALUE_COMPLETE = 'Complete';
    /**
     * Constant for value 'Rejected'
     * @return string 'Rejected'
     */
    const VALUE_REJECTED = 'Rejected';
    /**
     * Constant for value 'Failed'
     * @return string 'Failed'
     */
    const VALUE_FAILED = 'Failed';
    /**
     * Return allowed values
     * @uses self::VALUE_SENT
     * @uses self::VALUE_RESEND_CONFIRMATION_REQUIRED
     * @uses self::VALUE_PROCESS_RESEND
     * @uses self::VALUE_RESENT
     * @uses self::VALUE_COMPLETE
     * @uses self::VALUE_REJECTED
     * @uses self::VALUE_FAILED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_SENT,
            self::VALUE_RESEND_CONFIRMATION_REQUIRED,
            self::VALUE_PROCESS_RESEND,
            self::VALUE_RESENT,
            self::VALUE_COMPLETE,
            self::VALUE_REJECTED,
            self::VALUE_FAILED,
        ];
    }
}
