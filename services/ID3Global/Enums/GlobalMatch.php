<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalMatch Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q39:GlobalMatch
 * @subpackage Enumerations
 */
class GlobalMatch extends AbstractStructEnumBase
{
    /**
     * Constant for value 'NA'
     * @return string 'NA'
     */
    const VALUE_NA = 'NA';
    /**
     * Constant for value 'Match'
     * @return string 'Match'
     */
    const VALUE_MATCH = 'Match';
    /**
     * Constant for value 'Mismatch'
     * @return string 'Mismatch'
     */
    const VALUE_MISMATCH = 'Mismatch';
    /**
     * Constant for value 'Nomatch'
     * @return string 'Nomatch'
     */
    const VALUE_NOMATCH = 'Nomatch';
    /**
     * Return allowed values
     * @uses self::VALUE_NA
     * @uses self::VALUE_MATCH
     * @uses self::VALUE_MISMATCH
     * @uses self::VALUE_NOMATCH
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NA,
            self::VALUE_MATCH,
            self::VALUE_MISMATCH,
            self::VALUE_NOMATCH,
        ];
    }
}
