<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalCardType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q93:GlobalCardType
 * @subpackage Enumerations
 */
class GlobalCardType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'VISA'
     * @return string 'VISA'
     */
    const VALUE_VISA = 'VISA';
    /**
     * Constant for value 'MASTERCARD'
     * @return string 'MASTERCARD'
     */
    const VALUE_MASTERCARD = 'MASTERCARD';
    /**
     * Constant for value 'DELTA'
     * @return string 'DELTA'
     */
    const VALUE_DELTA = 'DELTA';
    /**
     * Constant for value 'SWITCH'
     * @return string 'SWITCH'
     */
    const VALUE_SWITCH = 'SWITCH';
    /**
     * Constant for value 'AMEX'
     * @return string 'AMEX'
     */
    const VALUE_AMEX = 'AMEX';
    /**
     * Constant for value 'JCB'
     * @return string 'JCB'
     */
    const VALUE_JCB = 'JCB';
    /**
     * Constant for value 'MAESTRO'
     * @return string 'MAESTRO'
     */
    const VALUE_MAESTRO = 'MAESTRO';
    /**
     * Constant for value 'DINERS'
     * @return string 'DINERS'
     */
    const VALUE_DINERS = 'DINERS';
    /**
     * Constant for value 'ELECTRON'
     * @return string 'ELECTRON'
     */
    const VALUE_ELECTRON = 'ELECTRON';
    /**
     * Constant for value 'SOLO'
     * @return string 'SOLO'
     */
    const VALUE_SOLO = 'SOLO';
    /**
     * Constant for value 'CARTEBANCAIRE'
     * @return string 'CARTEBANCAIRE'
     */
    const VALUE_CARTEBANCAIRE = 'CARTEBANCAIRE';
    /**
     * Constant for value 'CARTEBLEUE'
     * @return string 'CARTEBLEUE'
     */
    const VALUE_CARTEBLEUE = 'CARTEBLEUE';
    /**
     * Constant for value 'LASER'
     * @return string 'LASER'
     */
    const VALUE_LASER = 'LASER';
    /**
     * Constant for value 'DISCOVER'
     * @return string 'DISCOVER'
     */
    const VALUE_DISCOVER = 'DISCOVER';
    /**
     * Constant for value 'DANKORT'
     * @return string 'DANKORT'
     */
    const VALUE_DANKORT = 'DANKORT';
    /**
     * Return allowed values
     * @uses self::VALUE_VISA
     * @uses self::VALUE_MASTERCARD
     * @uses self::VALUE_DELTA
     * @uses self::VALUE_SWITCH
     * @uses self::VALUE_AMEX
     * @uses self::VALUE_JCB
     * @uses self::VALUE_MAESTRO
     * @uses self::VALUE_DINERS
     * @uses self::VALUE_ELECTRON
     * @uses self::VALUE_SOLO
     * @uses self::VALUE_CARTEBANCAIRE
     * @uses self::VALUE_CARTEBLEUE
     * @uses self::VALUE_LASER
     * @uses self::VALUE_DISCOVER
     * @uses self::VALUE_DANKORT
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_VISA,
            self::VALUE_MASTERCARD,
            self::VALUE_DELTA,
            self::VALUE_SWITCH,
            self::VALUE_AMEX,
            self::VALUE_JCB,
            self::VALUE_MAESTRO,
            self::VALUE_DINERS,
            self::VALUE_ELECTRON,
            self::VALUE_SOLO,
            self::VALUE_CARTEBANCAIRE,
            self::VALUE_CARTEBLEUE,
            self::VALUE_LASER,
            self::VALUE_DISCOVER,
            self::VALUE_DANKORT,
        ];
    }
}
