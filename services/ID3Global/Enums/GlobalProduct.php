<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalProduct Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q586:GlobalProduct
 * @subpackage Enumerations
 */
class GlobalProduct extends AbstractStructEnumBase
{
    /**
     * Constant for value 'ID3check'
     * @return string 'ID3check'
     */
    const VALUE_ID_3_CHECK = 'ID3check';
    /**
     * Constant for value 'URU'
     * @return string 'URU'
     */
    const VALUE_URU = 'URU';
    /**
     * Constant for value 'ID3global'
     * @return string 'ID3global'
     */
    const VALUE_ID_3_GLOBAL = 'ID3global';
    /**
     * Constant for value 'KYP'
     * @return string 'KYP'
     */
    const VALUE_KYP = 'KYP';
    /**
     * Constant for value 'LiteIDV'
     * @return string 'LiteIDV'
     */
    const VALUE_LITE_IDV = 'LiteIDV';
    /**
     * Return allowed values
     * @uses self::VALUE_ID_3_CHECK
     * @uses self::VALUE_URU
     * @uses self::VALUE_ID_3_GLOBAL
     * @uses self::VALUE_KYP
     * @uses self::VALUE_LITE_IDV
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_ID_3_CHECK,
            self::VALUE_URU,
            self::VALUE_ID_3_GLOBAL,
            self::VALUE_KYP,
            self::VALUE_LITE_IDV,
        ];
    }
}
