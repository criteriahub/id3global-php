<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalDVLADrivingLicenceReportCategoryType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q786:GlobalDVLADrivingLicenceReportCategoryType
 * @subpackage Enumerations
 */
class GlobalDVLADrivingLicenceReportCategoryType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Full'
     * @return string 'Full'
     */
    const VALUE_FULL = 'Full';
    /**
     * Constant for value 'Provisional'
     * @return string 'Provisional'
     */
    const VALUE_PROVISIONAL = 'Provisional';
    /**
     * Return allowed values
     * @uses self::VALUE_FULL
     * @uses self::VALUE_PROVISIONAL
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_FULL,
            self::VALUE_PROVISIONAL,
        ];
    }
}
