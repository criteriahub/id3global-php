<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for GlobalDispatchReportStatus Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q780:GlobalDispatchReportStatus
 * @subpackage Enumerations
 */
class GlobalDispatchReportStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'Clear'
     * @return string 'Clear'
     */
    const VALUE_CLEAR = 'Clear';
    /**
     * Constant for value 'Unclear'
     * @return string 'Unclear'
     */
    const VALUE_UNCLEAR = 'Unclear';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_CLEAR
     * @uses self::VALUE_UNCLEAR
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NONE,
            self::VALUE_CLEAR,
            self::VALUE_UNCLEAR,
        ];
    }
}
