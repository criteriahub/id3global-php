<?php

declare(strict_types=1);

namespace ID3Global\Enums;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for Enums.UserAccountType Enums
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:Enums.UserAccountType
 * @subpackage Enumerations
 */
class Enums_UserAccountType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'All'
     * @return string 'All'
     */
    const VALUE_ALL = 'All';
    /**
     * Constant for value 'Active'
     * @return string 'Active'
     */
    const VALUE_ACTIVE = 'Active';
    /**
     * Constant for value 'Expired'
     * @return string 'Expired'
     */
    const VALUE_EXPIRED = 'Expired';
    /**
     * Return allowed values
     * @uses self::VALUE_ALL
     * @uses self::VALUE_ACTIVE
     * @uses self::VALUE_EXPIRED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_ALL,
            self::VALUE_ACTIVE,
            self::VALUE_EXPIRED,
        ];
    }
}
