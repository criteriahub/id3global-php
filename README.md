## Dependencies

* php 8.0
* composer

## Setup

```bash
# install composer dependencies
composer install

# copy env example and add username/password to .env file ID3GLOBAL_* stuff
mv .env.example .env

# Install roadrunner server
vendor/bin/rr get-binary
```

## Run

Use the roadrunner server from larvel/octane to serve the app

```bash
php artisan octane:start
```

If you want hot reload on code changes then you need to install chokidar

```bash
npm install

php artisan octane:start --watch
```

Go to `localhost:$PORT/api/example` to get response from example controller

## ID3Global WSDL

The ID3Global wsdl has been generated into the `services/ID3Global` directory and all "Service" soap clients have been bound
to laravels service container for dependency injection. All the bound client implementations also have the WsSecurity header set based on the values set in `config/id3global.php`

There is an example route in `routes/api.php` that calls an example controller to check authentication on the credentials set either in the .env or directly in `config/id3global.php`. The controller at `app/Http/Controllers/ExampleController.php` dependency injects the `ID3Global\Services\Check` client into the controller and performs the `checkCredentials` method of the SOAP API.

### To regenerate the wsdl you can use the following docker command

```bash
docker run --rm -it --volume $PWD/services:/var/www mikaelcom/wsdltophp generate:package \
    --urlorpath="https://pilot.id3global.com/ID3gWS/ID3global.svc?wsdl" \
    --destination="/var/www" \
    --composer-name="mortgagebrain/id3global" \
    --force \
    --namespace="ID3Global" \
    --src-dirname="" \
    --services-folder="Services" \
    --enums-folder="Enums" \
    --arrays-folder="Arrays" \
    --structs-folder="Models"
```
